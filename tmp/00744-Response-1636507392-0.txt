RESPONSE URI FOR SEQUENCE ID 00744
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/query?minorversion=47

RESPONSE HEADERS
================
date: Wed, 10 Nov 2021 01:23:12 GMT
content-type: application/xml;charset=UTF-8
content-length: 3060
intuit_tid: 1-618b1f00-47894a63584794fe12a9ba3d
x-spanid: df200b41-377c-4691-84e8-61f69c6405b0
x-amzn-trace-id: Root=1-618b1f00-47894a63584794fe12a9ba3d
x-content-type-options: nosniff
qbo-version: 1969.129
service-time: total=278, db=46
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private
x-envoy-upstream-service-time: 355
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2021-11-09T17:23:12.496-08:00">
  <QueryResponse startPosition="1" maxResults="1" totalCount="1">
    <Invoice domain="QBO" sparse="false">
      <Id>126</Id>
      <SyncToken>0</SyncToken>
      <MetaData>
        <CreateTime>2021-11-09T17:00:09-08:00</CreateTime>
        <LastUpdatedTime>2021-11-09T17:00:09-08:00</LastUpdatedTime>
      </MetaData>
      <CustomField>
        <DefinitionId>2</DefinitionId>
        <Type>StringType</Type>
        <StringValue>Waiver &amp; Release of Lien Upon</StringValue>
      </CustomField>
      <CustomField>
        <DefinitionId>3</DefinitionId>
        <Type>StringType</Type>
        <StringValue>150 NE 8 STREET MIAMI FLORIDA</StringValue>
      </CustomField>
      <DocNumber>53_CYO_1636506009</DocNumber>
      <TxnDate>2021-11-09</TxnDate>
      <CurrencyRef name="United States Dollar">USD</CurrencyRef>
      <Line>
        <Id>1</Id>
        <LineNum>1</LineNum>
        <Description>Waiver &amp; Release of Lien Upon Final Payment
Contracted By - PROTON BUILDERS LLC
Parent Work Order Number - 29
09/08/2021</Description>
        <Amount>0</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="SERVICES:Waiver &amp; Release of Lien Upon Final Payment">40</ItemRef>
          <UnitPrice>0</UnitPrice>
          <Qty>1</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>NON</TaxCodeRef>
          <TaxClassificationRef>EUC-99990201-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Amount>0</Amount>
        <DetailType>SubTotalLineDetail</DetailType>
        <SubTotalLineDetail/>
      </Line>
      <TxnTaxDetail>
        <TxnTaxCodeRef>3</TxnTaxCodeRef>
        <TotalTax>0</TotalTax>
        <TaxLine>
          <Amount>0</Amount>
          <DetailType>TaxLineDetail</DetailType>
          <TaxLineDetail>
            <TaxRateRef>3</TaxRateRef>
            <PercentBased>true</PercentBased>
            <TaxPercent>6</TaxPercent>
            <NetAmountTaxable>0</NetAmountTaxable>
          </TaxLineDetail>
        </TaxLine>
        <TaxLine>
          <Amount>0</Amount>
          <DetailType>TaxLineDetail</DetailType>
          <TaxLineDetail>
            <TaxRateRef>4</TaxRateRef>
            <PercentBased>true</PercentBased>
            <TaxPercent>1</TaxPercent>
            <NetAmountTaxable>0</NetAmountTaxable>
          </TaxLineDetail>
        </TaxLine>
      </TxnTaxDetail>
      <CustomerRef name="AAA Construction Corp._1615909222">213</CustomerRef>
      <BillAddr>
        <Id>218</Id>
        <Line1>301 Hialeah Drive ste 118</Line1>
        <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
        <PostalCode>33010</PostalCode>
      </BillAddr>
      <ShipAddr>
        <Id>219</Id>
        <Line1>301 Hialeah Drive ste 118</Line1>
        <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
        <PostalCode>33010</PostalCode>
      </ShipAddr>
      <FreeFormAddress>true</FreeFormAddress>
      <ShipFromAddr>
        <Id>363</Id>
        <Line1>PO Box 22821</Line1>
        <Line2>Hialeah, FL  33002 US</Line2>
      </ShipFromAddr>
      <SalesTermRef name="Net 15">2</SalesTermRef>
      <DueDate>2021-11-24</DueDate>
      <TotalAmt>0</TotalAmt>
      <ApplyTaxAfterDiscount>false</ApplyTaxAfterDiscount>
      <PrintStatus>NeedToPrint</PrintStatus>
      <EmailStatus>NotSet</EmailStatus>
      <Balance>0</Balance>
      <TaxExemptionRef/>
      <AllowIPNPayment>false</AllowIPNPayment>
      <AllowOnlinePayment>false</AllowOnlinePayment>
      <AllowOnlineCreditCardPayment>false</AllowOnlineCreditCardPayment>
      <AllowOnlineACHPayment>false</AllowOnlineACHPayment>
    </Invoice>
  </QueryResponse>
</IntuitResponse>


