RESPONSE URI FOR SEQUENCE ID 06198
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

RESPONSE HEADERS
================
date: Wed, 23 Mar 2022 16:56:29 GMT
content-type: application/xml;charset=UTF-8
content-length: 3828
intuit_tid: 1-623b513c-3e7a56811ba7805d37aa12c7
x-spanid: 6dc20ffb-1349-43dc-90e7-e72c37156c06
x-amzn-trace-id: Root=1-623b513c-3e7a56811ba7805d37aa12c7
x-content-type-options: nosniff
x-xss-protection: 1; mode=block
qbo-version: 1977.177
service-time: total=641, db=252
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private
x-envoy-upstream-service-time: 667
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-03-23T09:56:28.405-07:00">
  <Invoice domain="QBO" sparse="false">
    <Id>677</Id>
    <SyncToken>0</SyncToken>
    <MetaData>
      <CreateTime>2022-03-23T09:56:28-07:00</CreateTime>
      <LastUpdatedTime>2022-03-23T09:56:28-07:00</LastUpdatedTime>
    </MetaData>
    <CustomField>
      <DefinitionId>2</DefinitionId>
      <Type>StringType</Type>
      <StringValue>Notice to Owner/Preliminary No</StringValue>
    </CustomField>
    <CustomField>
      <DefinitionId>3</DefinitionId>
      <Type>StringType</Type>
      <StringValue>8319 W Sunrise. Blvd Plantatio</StringValue>
    </CustomField>
    <DocNumber>390_1648054588</DocNumber>
    <TxnDate>2022-03-23</TxnDate>
    <CurrencyRef name="United States Dollar">USD</CurrencyRef>
    <Line>
      <Id>1</Id>
      <LineNum>1</LineNum>
      <Description>Contracted By - Kennedy Contractors, Inc.</Description>
      <Amount>45.00</Amount>
      <DetailType>SalesItemLineDetail</DetailType>
      <SalesItemLineDetail>
        <ItemRef name="SERVICES:NTO">32</ItemRef>
        <UnitPrice>45</UnitPrice>
        <Qty>1</Qty>
        <ItemAccountRef name="Services">1</ItemAccountRef>
        <TaxCodeRef>TAX</TaxCodeRef>
        <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
      </SalesItemLineDetail>
    </Line>
    <Line>
      <Id>2</Id>
      <LineNum>2</LineNum>
      <Description>Certified Mail</Description>
      <Amount>10.84</Amount>
      <DetailType>SalesItemLineDetail</DetailType>
      <SalesItemLineDetail>
        <ItemRef name="MAILING:Certified Mail USPS">17</ItemRef>
        <UnitPrice>5.42</UnitPrice>
        <Qty>2</Qty>
        <ItemAccountRef name="Services">1</ItemAccountRef>
        <TaxCodeRef>TAX</TaxCodeRef>
        <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
      </SalesItemLineDetail>
    </Line>
    <Line>
      <Id>3</Id>
      <LineNum>3</LineNum>
      <Description>Certified Mail with Electronic Return Receipt</Description>
      <Amount>14.44</Amount>
      <DetailType>SalesItemLineDetail</DetailType>
      <SalesItemLineDetail>
        <ItemRef name="MAILING:Certified Mail ESRR">15</ItemRef>
        <UnitPrice>7.22</UnitPrice>
        <Qty>2</Qty>
        <ItemAccountRef name="Services">1</ItemAccountRef>
        <TaxCodeRef>TAX</TaxCodeRef>
        <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
      </SalesItemLineDetail>
    </Line>
    <Line>
      <Amount>70.28</Amount>
      <DetailType>SubTotalLineDetail</DetailType>
      <SubTotalLineDetail/>
    </Line>
    <TxnTaxDetail>
      <TxnTaxCodeRef>3</TxnTaxCodeRef>
      <TotalTax>4.92</TotalTax>
      <TaxLine>
        <Amount>4.22</Amount>
        <DetailType>TaxLineDetail</DetailType>
        <TaxLineDetail>
          <TaxRateRef>3</TaxRateRef>
          <PercentBased>true</PercentBased>
          <TaxPercent>6</TaxPercent>
          <NetAmountTaxable>70.28</NetAmountTaxable>
        </TaxLineDetail>
      </TaxLine>
      <TaxLine>
        <Amount>0.70</Amount>
        <DetailType>TaxLineDetail</DetailType>
        <TaxLineDetail>
          <TaxRateRef>4</TaxRateRef>
          <PercentBased>true</PercentBased>
          <TaxPercent>1</TaxPercent>
          <NetAmountTaxable>70.28</NetAmountTaxable>
        </TaxLineDetail>
      </TaxLine>
    </TxnTaxDetail>
    <CustomerRef name="Alfaro Plumbing Contractor_1647609740">271</CustomerRef>
    <BillAddr>
      <Id>842</Id>
      <Line1>1751 W 30th PL Unit 1007A</Line1>
      <City>Hialeah</City>
      <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
      <PostalCode>33012</PostalCode>
    </BillAddr>
    <ShipAddr>
      <Id>843</Id>
      <Line1>1751 W 30th PL Unit 1007A</Line1>
      <City>Hialeah</City>
      <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
      <PostalCode>33012</PostalCode>
    </ShipAddr>
    <FreeFormAddress>true</FreeFormAddress>
    <ShipFromAddr>
      <Id>874</Id>
      <Line1>PO Box 22821</Line1>
      <Line2>Hialeah, FL  33002 US</Line2>
    </ShipFromAddr>
    <SalesTermRef name="Net 15">2</SalesTermRef>
    <DueDate>2022-04-07</DueDate>
    <TotalAmt>75.20</TotalAmt>
    <ApplyTaxAfterDiscount>false</ApplyTaxAfterDiscount>
    <PrintStatus>NeedToPrint</PrintStatus>
    <EmailStatus>NotSet</EmailStatus>
    <Balance>75.20</Balance>
    <TaxExemptionRef/>
    <AllowIPNPayment>false</AllowIPNPayment>
    <AllowOnlinePayment>false</AllowOnlinePayment>
    <AllowOnlineCreditCardPayment>false</AllowOnlineCreditCardPayment>
    <AllowOnlineACHPayment>false</AllowOnlineACHPayment>
  </Invoice>
</IntuitResponse>


