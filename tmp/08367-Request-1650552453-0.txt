REQUEST URI FOR SEQUENCE ID 08367
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..CHQYwg-lVri45N8BuSx_Tw.D-FDqRY7vBsEXsa80Gapiu4ol9Od2bjee73U390TQ2QQf07UZfSETR6_hkaGZl_plcWrqQsNQwEygLv4qTK-BzWhfIbtu2T4u_rCMh5trT8iudDggerRomsx5MBX9LhBeHdFrDxwtJRUi4cRxTFbimDrBDJfcn6pZHGL0Bp5iMYh1eckThtRU8Ham8iIuGk7gmlrK1lzT0ZK413yuT9FtxfRbxucMfKKN03Ph96k-HgFGb6SqJ9YeZ3bSLXNLofKN1N775RWIqaUXCvIwl3WWwO3etp7EkE6KX_Jh20ODnIJgfMbPkkRFo_-wHfpce7h_l5LaswE09s0dxgc3UsL0NDDnwBqFzoiGCOAzYDumlbFPzlOrWxBEH3ssERPs6Eol_iBM4k2PVrR1fq6q6spBvjKduOL9stcFsSbgADQw7SwKIHpU2BxHtfWU0T-plg_cg-Typ2dffhJkejpyq1nFx_DsGxrevQl9MWmUk1IDX4HJD3Mjf-QerGuZ3icDi1KkAm7HnHJ87a80e28HX1Xc5z7yBcKPEWr7qhxDfk7L6UQXqgznTivrsOD1hz_h8McrJW1wtBCDqb-AWLqusHHidjOqzKSk42-Y5jT3xrq3hy3ZaOM-1QLxxi3siXoUUNQnVPT0EzmcVrWD2KB25bE8296wytYNXmB0RW2FbgZM3PKm8rFSokXzWQ1Wc1HLUKUZ0eOl9HYuxeZuoCx3T0-QPn3M83oIX9_3QZD6ffA1LA.-2sYfj7LInpANhpydNgLog
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 2354

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>3130 PLAZA ST MIAMI FL  &#13;
</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>508_1650552453</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - STAR BRITE GROUP</ns0:Description>
    <ns0:Amount>45.00</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>45.00</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Additional address charges</ns0:Description>
    <ns0:Amount>20</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Additional Address">34</ns0:ItemRef>
      <ns0:UnitPrice>20</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Firm Mail</ns0:Description>
    <ns0:Amount>3.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Firm Mail">18</ns0:ItemRef>
      <ns0:UnitPrice>3.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail with Electronic Return Receipt</ns0:Description>
    <ns0:Amount>14.44</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Electronic Return Receipt">15</ns0:ItemRef>
      <ns0:UnitPrice>7.22</ns0:UnitPrice>
      <ns0:Qty>2</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>245</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


