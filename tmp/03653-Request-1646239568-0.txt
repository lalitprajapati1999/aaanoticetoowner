REQUEST URI FOR SEQUENCE ID 03653
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..hzNeGHi0MvyBE3oAg_AJnA.NzQ5ZAl4wbpbBY59FxGB-WBvMj3S91kgLuQ8Cd-VjGi1CCBL_Ru91BmcikM2WiA1VA9o9bdmtGfkr2yW7sPIe1KP3GIpzk5x_Zps37XZEOAmtZcKrMbUqkg04LRWHNKBBHw-G6pEP1BVMOHAe6AEISBtuLHKvwiZHGzJvo_phF4A44FdggVMnao0hb0XL5ETIIGLhIDJOOKXzmRmnuN2Gx2L3NUCHVSlERFbNed3yS_ptUbjvQDG00_WNOTRv0r9IsHiRhQFfAqJlqiwhfngqsJ8lrXV0DvzN0BLhz_28c3BMHwohQmwrDaa_ODDxBjpSVra2f9U4mpVz7DdTej59msISIQSS-6TpG8fKErFSlDo6VrVDjp8KKwcRsilzC-n4Lw9fOeucdC0eqVnKYWDBee-40rrtNRE_WSzlboBaA0UeoXxVDyf_c6VUb3Z7xYtLK2dv-fbx15yokCcKdYjJodViwVs_iJlfq6mQEt4W9g467EeSGxG4LlGVuAZoui4fhPPQ3clo4skkkQX7SVLwSYcFElpfp-PRe7DBxsKRGTCYS4VbPHOoh3vLC12QBstirrWi7GWKAQ98XOAxNQ5YSCxnRWycnjvZ7Fgc-ADg8pmwnmVw4y47wl8LEOzprJfBPUc0QCrQwwLR2gxkC8IYx5c6UqZEXFBQoY9pt3YI0lfN_sr6fUso8hHm3MLDiCZvcgI4JHVNkv3EDqWVEqCYd1TaYYNJeSNjC8UISBdgds.GSuX2EOqqF_Jvf_039ANHg
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 2712

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>7878 NW 103 ST  HIALEAH, FL 33</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>229_1646239568</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - FRONTIER BUILDING CORP.</ns0:Description>
    <ns0:Amount>45.00</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>45.00</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Firm Mail</ns0:Description>
    <ns0:Amount>3.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Firm Mail">18</ns0:ItemRef>
      <ns0:UnitPrice>3.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail</ns0:Description>
    <ns0:Amount>5.42</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Certified Mail">17</ns0:ItemRef>
      <ns0:UnitPrice>5.42</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail with Electronic Return Receipt</ns0:Description>
    <ns0:Amount>14.44</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Electronic Return Receipt">15</ns0:ItemRef>
      <ns0:UnitPrice>7.22</ns0:UnitPrice>
      <ns0:Qty>2</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>C70191120000063845461-229</ns0:Description>
    <ns0:Amount>6.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Manual">19</ns0:ItemRef>
      <ns0:UnitPrice>6.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>236</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


