RESPONSE URI FOR SEQUENCE ID 11101
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/query?minorversion=47

RESPONSE HEADERS
================
date: Tue, 05 Jul 2022 20:01:19 GMT
content-type: application/xml;charset=UTF-8
content-length: 2767
intuit_tid: 1-62c4988f-76b29c285a6c7e5e3aa6faae
x-spanid: d76d3b95-24d9-419c-afc8-26c72e56f336
x-amzn-trace-id: Root=1-62c4988f-76b29c285a6c7e5e3aa6faae
x-content-type-options: nosniff
x-xss-protection: 1; mode=block
qbo-version: 1983.200
service-time: total=229, db=134
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private
x-envoy-upstream-service-time: 275
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-07-05T13:01:19.158-07:00">
  <QueryResponse startPosition="1" maxResults="1" totalCount="1">
    <Invoice domain="QBO" sparse="false">
      <Id>1929</Id>
      <SyncToken>0</SyncToken>
      <MetaData>
        <CreateTime>2022-07-05T12:38:12-07:00</CreateTime>
        <LastUpdatedTime>2022-07-05T12:38:12-07:00</LastUpdatedTime>
      </MetaData>
      <DocNumber>1029</DocNumber>
      <TxnDate>2022-07-05</TxnDate>
      <CurrencyRef name="United States Dollar">USD</CurrencyRef>
      <Line>
        <Id>1</Id>
        <LineNum>1</LineNum>
        <Description>NTO# 707- 3585 PAM AMERICAN DRIVE</Description>
        <Amount>20.00</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="Services-Default">1</ItemRef>
          <UnitPrice>20</UnitPrice>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>NON</TaxCodeRef>
          <TaxClassificationRef>EUC-99990201-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Amount>20.00</Amount>
        <DetailType>SubTotalLineDetail</DetailType>
        <SubTotalLineDetail/>
      </Line>
      <TxnTaxDetail>
        <TxnTaxCodeRef>3</TxnTaxCodeRef>
        <TotalTax>0</TotalTax>
        <TaxLine>
          <Amount>0</Amount>
          <DetailType>TaxLineDetail</DetailType>
          <TaxLineDetail>
            <TaxRateRef>3</TaxRateRef>
            <PercentBased>true</PercentBased>
            <TaxPercent>6</TaxPercent>
            <NetAmountTaxable>0</NetAmountTaxable>
          </TaxLineDetail>
        </TaxLine>
        <TaxLine>
          <Amount>0</Amount>
          <DetailType>TaxLineDetail</DetailType>
          <TaxLineDetail>
            <TaxRateRef>4</TaxRateRef>
            <PercentBased>true</PercentBased>
            <TaxPercent>1</TaxPercent>
            <NetAmountTaxable>0</NetAmountTaxable>
          </TaxLineDetail>
        </TaxLine>
      </TxnTaxDetail>
      <CustomerRef name="ERNESTO HERNANDEZ">195</CustomerRef>
      <BillAddr>
        <Id>1277</Id>
        <Line1>6945 NW 53 TERRACE</Line1>
        <City>MIAMI</City>
        <CountrySubDivisionCode>FLOIRDA</CountrySubDivisionCode>
        <PostalCode>33166</PostalCode>
      </BillAddr>
      <ShipAddr>
        <Id>1416</Id>
        <Line1>ERNESTO HERNANDEZ</Line1>
        <Line2>SUPERIOR MIX CORPORATION</Line2>
        <Line3>6945 NW 53 TERRACE</Line3>
        <Line4>MIAMI, FLOIRDA  33166</Line4>
      </ShipAddr>
      <FreeFormAddress>true</FreeFormAddress>
      <ShipFromAddr>
        <Id>1417</Id>
        <Line1>PO Box 22821, Hialeah, FL, 33002, USA</Line1>
      </ShipFromAddr>
      <SalesTermRef name="Due Net 15 Days from Statement Date">6</SalesTermRef>
      <DueDate>2022-07-20</DueDate>
      <TotalAmt>20.00</TotalAmt>
      <ApplyTaxAfterDiscount>false</ApplyTaxAfterDiscount>
      <PrintStatus>NotSet</PrintStatus>
      <EmailStatus>NotSet</EmailStatus>
      <BillEmail>
        <Address>ernesto@superiormix.net</Address>
      </BillEmail>
      <Balance>20.00</Balance>
      <TaxExemptionRef name="service">99</TaxExemptionRef>
      <AllowIPNPayment>false</AllowIPNPayment>
      <AllowOnlinePayment>false</AllowOnlinePayment>
      <AllowOnlineCreditCardPayment>true</AllowOnlineCreditCardPayment>
      <AllowOnlineACHPayment>true</AllowOnlineACHPayment>
    </Invoice>
  </QueryResponse>
</IntuitResponse>


