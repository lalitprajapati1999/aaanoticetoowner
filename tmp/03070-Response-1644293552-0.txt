RESPONSE URI FOR SEQUENCE ID 03070
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/customer?minorversion=47

RESPONSE HEADERS
================
date: Tue, 08 Feb 2022 04:12:32 GMT
content-type: application/xml;charset=UTF-8
content-length: 1496
intuit_tid: 1-6201edaf-75eee52a0afce35f3269eef1
x-spanid: ef8cfeb1-cea7-4987-98f8-ba2cfb99e03c
x-amzn-trace-id: Root=1-6201edaf-75eee52a0afce35f3269eef1
x-content-type-options: nosniff
qbo-version: 1974.107
service-time: total=414, db=83
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private
x-envoy-upstream-service-time: 452
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-02-07T20:12:31.961-08:00">
  <Customer domain="QBO" sparse="false">
    <Id>255</Id>
    <SyncToken>0</SyncToken>
    <MetaData>
      <CreateTime>2022-02-07T20:12:32-08:00</CreateTime>
      <LastUpdatedTime>2022-02-07T20:12:32-08:00</LastUpdatedTime>
    </MetaData>
    <FullyQualifiedName>MAR B PLUMBING CORPORATION_1644293551</FullyQualifiedName>
    <CompanyName>MAR B PLUMBING CORPORATION</CompanyName>
    <DisplayName>MAR B PLUMBING CORPORATION_1644293551</DisplayName>
    <PrintOnCheckName>MAR B PLUMBING CORPORATION</PrintOnCheckName>
    <Active>true</Active>
    <PrimaryPhone>
      <FreeFormNumber>(305) 635-4554</FreeFormNumber>
    </PrimaryPhone>
    <PrimaryEmailAddr>
      <Address>marbplum@bellsouth.net</Address>
    </PrimaryEmailAddr>
    <DefaultTaxCodeRef>3</DefaultTaxCodeRef>
    <Taxable>true</Taxable>
    <BillAddr>
      <Id>566</Id>
      <Line1>3201 NW 24 STREET ROAD STE 206</Line1>
      <City>Miami</City>
      <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
      <PostalCode>33142</PostalCode>
    </BillAddr>
    <ShipAddr>
      <Id>567</Id>
      <Line1>3201 NW 24 STREET ROAD STE 206</Line1>
      <City>Miami</City>
      <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
      <PostalCode>33142</PostalCode>
    </ShipAddr>
    <Job>false</Job>
    <BillWithParent>false</BillWithParent>
    <Balance>0</Balance>
    <BalanceWithJobs>0</BalanceWithJobs>
    <CurrencyRef name="United States Dollar">USD</CurrencyRef>
    <PreferredDeliveryMethod>None</PreferredDeliveryMethod>
    <IsProject>false</IsProject>
  </Customer>
</IntuitResponse>


