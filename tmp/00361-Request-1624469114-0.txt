REQUEST URI FOR SEQUENCE ID 00361
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..mA9qugTOYMmp01Losxtgvg.sulc0mZTZq4mw6l5g5VMMykZFPZBdUyjoISHOOg4T91tuUmH0WCuBk5BkX-4IbzlZ0MQx1KJyD6Wk2gil-Z8ZuMJDUq0vd4M3SFgnY9v1forhhuT8f6E0HoRlkwBKfdsI7F6mLmLZD_IlwvHIYL8401HCEkH7UzkoVxHl8BAk_T7GJmOIwqKw3bnU3Lajaoyud27daQDLxD_Vd2BakmqIYR3Iz2pYUwRF4vEXiAhn6anTymkwicPevV8LPtL528dB_pmJYZtC8_K0PWUbY0h87Tp6WMfN4fa56nXDqP6bTbvqe6mHpsaGSdbMYlbJjOHX1ajs-lyFh6k5ilTuoLXRr9myAh3n23yEnVqpYAAjjr5gnyri2yblcfzE0PpvADN7nK9CLR90LZ6JZuRpzyTMDDINIjjubgnqar5WTavL_ykRAr2rnQCv0QQzHLsoz06_Jm6rnLsqc9Mx8nilfKOOVizYPwa0crLl1Hu7xOiXGPju3QFbCmlRskjXt_Cdoezfoodz8IA948vD0zf_qg8RdP023R5l6Cej0GzFfh-jrSCxBw1OeCwWcdsFL8MqwLf0F3z2BX8vP8OgW4dNByKxPlQ6m8qzdiT8IVfT4wDB83PmY3HqEj7vIFrPbPb0uKsiFtaEYnfjtUuaDTSjT4oQTds_5Zw6F68jGFCDMNPEJNBb0mtuDNgZBvLBUJt804rBan8TsjuMq4IW8xPIaClT6nQAX2Msu1ydVV_xJe0Nyc.TmofkazgqW90sc3SKfWI3w
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 2029

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>PO 6565</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Bond Claim of Lien</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>2000 PARK AVENUE MIAMI BEACH F</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>72_1624469114</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - ATLANTIC PACIFIC COMMUNITY BUILDERS, LLC
Parent Work Order Number - 71</ns0:Description>
    <ns0:Amount>325.00</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">39</ns0:ItemRef>
      <ns0:UnitPrice>325.00</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Rush Fee</ns0:Description>
    <ns0:Amount>12.50</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Rush Hour Charges">33</ns0:ItemRef>
      <ns0:UnitPrice>12.50</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Firm Mail</ns0:Description>
    <ns0:Amount>39.5</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Firm Mail">18</ns0:ItemRef>
      <ns0:UnitPrice>3.95</ns0:UnitPrice>
      <ns0:Qty>10</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>213</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


