RESPONSE URI FOR SEQUENCE ID 13019
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

RESPONSE HEADERS
================
date: Tue, 09 Aug 2022 15:20:19 GMT
content-type: application/xml
content-length: 509
intuit_tid: 1-62f27b33-0d4e7ae51a9c6a087c76dcd8
x-spanid: 80b63c0c-7d48-4487-aab4-d35ac542e6af
x-amzn-trace-id: Root=1-62f27b33-0d4e7ae51a9c6a087c76dcd8
x-content-type-options: nosniff
x-envoy-upstream-service-time: 421
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-08-09T08:20:19.466-07:00">
  <Fault type="ValidationFault">
    <Error code="6000" element="">
      <Message>A business validation error has occurred while processing your request</Message>
      <Detail>Business Validation Error: You cannot enter more than 31 characters in the sales_custom_1_val field. You tried entering CICC 7040-..., which is 40 characters.</Detail>
    </Error>
  </Fault>
</IntuitResponse>


