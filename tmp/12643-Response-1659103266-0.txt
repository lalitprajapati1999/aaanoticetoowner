RESPONSE URI FOR SEQUENCE ID 12643
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/customer?minorversion=47

RESPONSE HEADERS
================
date: Fri, 29 Jul 2022 14:01:07 GMT
content-type: application/xml;charset=UTF-8
content-length: 1476
intuit_tid: 1-62e3e822-7abfab335171f82f0e147a22
x-spanid: 87858d53-04bc-40ed-b036-4b4b7b39754e
x-amzn-trace-id: Root=1-62e3e822-7abfab335171f82f0e147a22
x-content-type-options: nosniff
x-xss-protection: 1; mode=block
qbo-version: 1985.167
service-time: total=546, db=154
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private
x-envoy-upstream-service-time: 610
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-07-29T07:01:07.121-07:00">
  <Customer domain="QBO" sparse="false">
    <Id>295</Id>
    <SyncToken>0</SyncToken>
    <MetaData>
      <CreateTime>2022-07-29T07:01:07-07:00</CreateTime>
      <LastUpdatedTime>2022-07-29T07:01:07-07:00</LastUpdatedTime>
    </MetaData>
    <FullyQualifiedName>ARCHON AIR MANAGEMENT CORP._1659103265</FullyQualifiedName>
    <CompanyName>ARCHON AIR MANAGEMENT CORP.</CompanyName>
    <DisplayName>ARCHON AIR MANAGEMENT CORP._1659103265</DisplayName>
    <PrintOnCheckName>ARCHON AIR MANAGEMENT CORP.</PrintOnCheckName>
    <Active>true</Active>
    <PrimaryPhone>
      <FreeFormNumber>(305) 592-8552</FreeFormNumber>
    </PrimaryPhone>
    <PrimaryEmailAddr>
      <Address>yaguilar@archonair.com</Address>
    </PrimaryEmailAddr>
    <DefaultTaxCodeRef>3</DefaultTaxCodeRef>
    <Taxable>true</Taxable>
    <BillAddr>
      <Id>1595</Id>
      <Line1>7264 NW 25 STREET</Line1>
      <City>Miami</City>
      <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
      <PostalCode>33122</PostalCode>
    </BillAddr>
    <ShipAddr>
      <Id>1596</Id>
      <Line1>7264 NW 25 STREET</Line1>
      <City>Miami</City>
      <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
      <PostalCode>33122</PostalCode>
    </ShipAddr>
    <Job>false</Job>
    <BillWithParent>false</BillWithParent>
    <Balance>0</Balance>
    <BalanceWithJobs>0</BalanceWithJobs>
    <CurrencyRef name="United States Dollar">USD</CurrencyRef>
    <PreferredDeliveryMethod>None</PreferredDeliveryMethod>
    <IsProject>false</IsProject>
  </Customer>
</IntuitResponse>


