RESPONSE URI FOR SEQUENCE ID 11015
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

RESPONSE HEADERS
================
date: Tue, 05 Jul 2022 16:12:08 GMT
content-type: application/xml
content-length: 509
intuit_tid: 1-62c462d7-776e87480b073cc2593e70a2
x-spanid: 1030ae38-0195-4c69-86c2-1fa2297086b3
x-amzn-trace-id: Root=1-62c462d7-776e87480b073cc2593e70a2
x-content-type-options: nosniff
x-envoy-upstream-service-time: 534
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-07-05T09:12:07.992-07:00">
  <Fault type="ValidationFault">
    <Error code="6000" element="">
      <Message>A business validation error has occurred while processing your request</Message>
      <Detail>Business Validation Error: You cannot enter more than 31 characters in the sales_custom_1_val field. You tried entering NTO 342- c..., which is 46 characters.</Detail>
    </Error>
  </Fault>
</IntuitResponse>


