REQUEST URI FOR SEQUENCE ID 05607
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..QST2SaQn-On99_0NLZZGsQ.QZ2WX550i4WnLlgi1se7L-Y7rywz_lzTzI0B9trP_k6HULnobYi3TGY5dyvY-OBHEfsG0TK3pvi84egVbb27rLVdoD7wJr9WKWFp1JlXzfvOiJjHsDUrSDunRBV3X3bqOWpBUgsViRmaCFshZqfOwLxd6R2efZC-cOjcTTPv2kcBM_Nic8fq-k_2VGGExI8M5z3RF6OZ2naVOI2n6vhxeeejTeaR2X70xThyo9Xsw95hRuDmdHs0IB5lCan5EKMGX91u5eD67V3JklJNlHbSg0njva48As6Nzzg-hd3qWmxnEvz1EdilboV1AjdRFTypheOPtwqYlDLlRQkcN_OpnWDev4KpTEk1dDikT3qM9uQhz96VNDDwbY6GGqTKH_u1lIOcR3DQKRYoPvxrGNrTC6TzreIqu8ys22PExG-m-hJzu8Btd0DUFGtNJ9d2B6-8Nq_UoGn3z4x2cpVgaUgyYKJ4H0wmjv-njRCjgV07e5l3IMKzDHmLLDAmNf11X1WIgqFkOCXAhdYwUXu0OvLp0gVoBivyr6Gy7o7oelT_SDag55nklblM_SCDg7Vs20pq7LvhskHKmsNkubkBCYy4QiWsGiMZKI3Ub8R0zvebkv4mVN5g1TXo7h4cdsuMYlYwDu6O1Hj_syQNCo2-HYBZF7IO0XfsbHtsclJhG8HOWQn4Q7XKj3LoVqDefoj7X9mdXIBokAZDYtTWD6CkkJTMsMR01oPdW9yxvum8lZEaZSE.DN_FZ75W8eikB1aZzB4Eqw
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 1254

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>123 sw 89th st</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>356_1646995682</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - APPLE GROUP HOLDINGS ATTN: EMILIO MEZA AND APPLE 1250 LLC</ns0:Description>
    <ns0:Amount>25.00</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>25.00</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>213</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


