RESPONSE URI FOR SEQUENCE ID 08960
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

RESPONSE HEADERS
================
date: Tue, 03 May 2022 10:28:18 GMT
content-type: application/xml
content-length: 509
intuit_tid: 1-627103c1-03f41c493973c63e2f081aa6
x-spanid: 6dcf9865-f1d4-4e4b-b85b-3ef3d09905fe
x-amzn-trace-id: Root=1-627103c1-03f41c493973c63e2f081aa6
x-content-type-options: nosniff
x-envoy-upstream-service-time: 727
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-05-03T03:28:17.948-07:00">
  <Fault type="ValidationFault">
    <Error code="6000" element="">
      <Message>A business validation error has occurred while processing your request</Message>
      <Detail>Business Validation Error: You cannot enter more than 31 characters in the sales_custom_1_val field. You tried entering CICC 7040-..., which is 40 characters.</Detail>
    </Error>
  </Fault>
</IntuitResponse>


