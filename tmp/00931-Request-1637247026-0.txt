REQUEST URI FOR SEQUENCE ID 00931
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/customer?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..OX7RQJ0EwDf1H6tGhJB6RQ.YsOMEBVrvUuulfISwCCWoBvVWPgOe49zih-z_T596eVESuTP7034K64CaK_C3NWZCkYWJZHrl3OA91yvo9N6knWhB6T8UXMS_giGaS9SYthF-h678ellAJmOYLac07XghMK_JcyaXZfLa_MQ2BA66XMEluMeRSRizMfhTzqoiwA8sExhFcSW7qTWDJoaBuRuegcgJYIiME_B350yflV2oSa8rEs2bwmN7jhek1bGemhWi2BqLYDC9ZQ30S2OviSeEV6cjSu9IeA79JH4h_WAdkSDSsf1yd4KdWzAy5eHsVF86z3uw2bhOUtVt2jeP6oZp6szZR2UBqmg4jZYPQY99hdWcKbBXmWb9ZtcDIfD_KfbOZfYLQ7UJxMWK6pXOIxXKFlRHJdIcu0ckIjO4hG-Z5VCgibaEOrDXFkvoZL-vs2NjFHcWP_sej2Mz47fhdZVWbPfMBB0eqtx3izIg91Tr8xz5vt5nicujiRYVdN6ylHLRrXJhL8oVLa_QF0PP6t5QaoiZ9ECJ4WZx3znBcPzW5BZC0fUlq5Fteo1dccChhkElE7O_icN0GWGGVixdlsrUTJeRr90ZQ2bg0hMiQ5g83ePjMdNaIRs9mWJvE09qmKQVChkkps5r4lRa-JlMQH6Yh_5qLWP1xbXl3IB2IX8qlg4IN_0SsyZbmEejF9jhrhFT9Uj7ac5yeTCknZkk7SHHlw8113pLJLZNS1Y998y-g9mrfdo4nFxl0i2V1ViKdw.41CPKkCSpkhHd21hRpK4TQ
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 955

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Customer xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:FullyQualifiedName>Test subscription</ns0:FullyQualifiedName>
  <ns0:CompanyName>Test subscription</ns0:CompanyName>
  <ns0:DisplayName>Test subscription_1637247026</ns0:DisplayName>
  <ns0:PrimaryPhone>
    <ns0:FreeFormNumber>(123) 456-7890</ns0:FreeFormNumber>
  </ns0:PrimaryPhone>
  <ns0:PrimaryEmailAddr>
    <ns0:Address>kajal.neosoft@gmail.com</ns0:Address>
  </ns0:PrimaryEmailAddr>
  <ns0:BillAddr>
    <ns0:Line1>Test address</ns0:Line1>
    <ns0:City>Fabens</ns0:City>
    <ns0:CountrySubDivisionCode>Florida</ns0:CountrySubDivisionCode>
    <ns0:PostalCode>31103</ns0:PostalCode>
  </ns0:BillAddr>
  <ns0:ShipAddr>
    <ns0:Line1>Test address</ns0:Line1>
    <ns0:City>Fabens</ns0:City>
    <ns0:CountrySubDivisionCode>Florida</ns0:CountrySubDivisionCode>
    <ns0:PostalCode>31103</ns0:PostalCode>
  </ns0:ShipAddr>
</ns0:Customer>


