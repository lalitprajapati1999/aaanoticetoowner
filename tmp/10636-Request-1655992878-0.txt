REQUEST URI FOR SEQUENCE ID 10636
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..XF3JgUyHnPNIMZ1CBsBf4g.Yr94luCF4XleJIIUsaRl8AmWaTXL30ugpRot_Jew2pFES0ggNoG82PhW9AvA1KwH_Yw-LyPvCIx7NV9TOoxvLSrrjkOlYNmYV4MegJ4izjyrCPb5XqJP3xvew_DgMv69P1j41eInyDoYJlHMjDR5-HNbIaVvln1hClq3Dp0wVB9wu2G2YKLX4LhjMCH7Px8n9y1MI0GNAbTx5omjDuovpyZsMPdHr3KgBLm4r3Afxlz3vrgQgahfD6FOR_B9Ne3Fr_GjA84GFa6LrJbNvIaBrkqNdSRLns0H5pBOHTQNlF5ptIsjKO1aTLGSJ0UysvOK8WwExRMoBYWKcT_d11E6SLxuqQ1Ynaj2eOOqBZXl241byrSbOtEfvM0bNJSkgfzR0Z4es4Np_GOsjrTMqVm0JN3b2gbqvlxW1RnqzQT_wp4vuuPiDYaGou5XqC8l3mRyu_ozbo4MdBATdFXd1fadh878fOvMp-H_-NXhwsfS1nprEMGDDRR6xziREJCyZKwfzlJdz9VeIkjXLo3P--0OLptDj8eTQwkix1meWzPnN7zKPcJLm6LZ1p8yrkAWnW_SccIN9ZguZSh9imkaWmOUjE0Uc18yYZyk5ss07vROUYFm73-tjzee8Fk2WQHIylq0fgI0ikjFBAFcZL2BYnTJuxWFdXktMvFEOAxtLzB9Mtg99cz7E4yIyZex1eRXrbDlydQIc-CxGhRt8UtGIfdG7HuLHFrqQ3JUHL7G2IRQVYs.-LdaAJ2MaMNU46Rc4Pw7jg
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 2016

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>992 NE 167 TH ST</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>738_1655992878</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - AP WELDING &amp; STEEL MANUFACTURE
Parent Work Order Number - 634</ns0:Description>
    <ns0:Amount>45.00</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>45.00</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Firm Mail</ns0:Description>
    <ns0:Amount>3.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Firm Mail">18</ns0:ItemRef>
      <ns0:UnitPrice>3.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail with Electronic Return Receipt</ns0:Description>
    <ns0:Amount>7.22</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Electronic Return Receipt">15</ns0:ItemRef>
      <ns0:UnitPrice>7.22</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>257</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


