RESPONSE URI FOR SEQUENCE ID 12669
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/query?minorversion=47

RESPONSE HEADERS
================
date: Sat, 30 Jul 2022 16:12:27 GMT
content-type: application/xml;charset=UTF-8
content-length: 3110
intuit_tid: 1-62e5586b-20415dbc286340933bdc8a27
x-spanid: 0248a6f0-adee-4962-aae3-49bb8297381e
x-amzn-trace-id: Root=1-62e5586b-20415dbc286340933bdc8a27
x-content-type-options: nosniff
x-xss-protection: 1; mode=block
qbo-version: 1985.167
service-time: total=411, db=163
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private
x-envoy-upstream-service-time: 438
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-07-30T09:12:27.585-07:00">
  <QueryResponse startPosition="1" maxResults="1" totalCount="1">
    <Invoice domain="QBO" sparse="false">
      <Id>2168</Id>
      <SyncToken>0</SyncToken>
      <MetaData>
        <CreateTime>2022-07-30T08:59:26-07:00</CreateTime>
        <LastUpdatedTime>2022-07-30T08:59:26-07:00</LastUpdatedTime>
      </MetaData>
      <DocNumber>1037</DocNumber>
      <TxnDate>2022-07-30</TxnDate>
      <CurrencyRef name="United States Dollar">USD</CurrencyRef>
      <Line>
        <Id>1</Id>
        <LineNum>1</LineNum>
        <Description>COL#21912 1737 NOTH BAY SHORE DR</Description>
        <Amount>575.00</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="CLAIM OF LIEN:Claim of Lien">38</ItemRef>
          <UnitPrice>575</UnitPrice>
          <Qty>1</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>NON</TaxCodeRef>
          <TaxClassificationRef>EUC-99990201-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Id>2</Id>
        <LineNum>2</LineNum>
        <Description>Firm mail USPS</Description>
        <Amount>49.50</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="MAILING:Certified Mail RR">16</ItemRef>
          <UnitPrice>8.25</UnitPrice>
          <Qty>6</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>NON</TaxCodeRef>
          <TaxClassificationRef>EUC-99990201-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Amount>624.50</Amount>
        <DetailType>SubTotalLineDetail</DetailType>
        <SubTotalLineDetail/>
      </Line>
      <TxnTaxDetail>
        <TxnTaxCodeRef>3</TxnTaxCodeRef>
        <TotalTax>0</TotalTax>
        <TaxLine>
          <Amount>0</Amount>
          <DetailType>TaxLineDetail</DetailType>
          <TaxLineDetail>
            <TaxRateRef>3</TaxRateRef>
            <PercentBased>true</PercentBased>
            <TaxPercent>6</TaxPercent>
            <NetAmountTaxable>0</NetAmountTaxable>
          </TaxLineDetail>
        </TaxLine>
        <TaxLine>
          <Amount>0</Amount>
          <DetailType>TaxLineDetail</DetailType>
          <TaxLineDetail>
            <TaxRateRef>4</TaxRateRef>
            <PercentBased>true</PercentBased>
            <TaxPercent>1</TaxPercent>
            <NetAmountTaxable>0</NetAmountTaxable>
          </TaxLineDetail>
        </TaxLine>
      </TxnTaxDetail>
      <CustomerRef name="SILVIO MARTINEZ">136</CustomerRef>
      <BillAddr>
        <Id>137</Id>
        <Line1>3201 NW 24 STREET ROAD STE 206</Line1>
        <City>MIAMI</City>
        <Country>FLORIDA</Country>
        <PostalCode>33142</PostalCode>
      </BillAddr>
      <ShipAddr>
        <Id>1600</Id>
        <Line1>SILVIO MARTINEZ</Line1>
        <Line2>MAR B PLUMBING CORPORATION</Line2>
        <Line3>3201 NW 24 STREET ROAD STE 206</Line3>
        <Line4>MIAMI  33142 FLORIDA</Line4>
      </ShipAddr>
      <FreeFormAddress>true</FreeFormAddress>
      <ShipFromAddr>
        <Id>1601</Id>
        <Line1>PO Box 22821, Hialeah, FL, 33002, USA</Line1>
      </ShipFromAddr>
      <DueDate>2022-07-30</DueDate>
      <TotalAmt>624.50</TotalAmt>
      <ApplyTaxAfterDiscount>false</ApplyTaxAfterDiscount>
      <PrintStatus>NotSet</PrintStatus>
      <EmailStatus>NotSet</EmailStatus>
      <BillEmail>
        <Address>marbplum@bellsouth.net</Address>
      </BillEmail>
      <Balance>624.50</Balance>
      <TaxExemptionRef/>
      <AllowIPNPayment>false</AllowIPNPayment>
      <AllowOnlinePayment>false</AllowOnlinePayment>
      <AllowOnlineCreditCardPayment>true</AllowOnlineCreditCardPayment>
      <AllowOnlineACHPayment>true</AllowOnlineACHPayment>
    </Invoice>
  </QueryResponse>
</IntuitResponse>


