REQUEST URI FOR SEQUENCE ID 10868
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/customer?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..jpFadli0J-GHw47_VszAmg.EDXMzNc86OQpaCuEWReBTPKMszKxr-k_Ah40CsWYs5tWGeVSPUvPus74uCSxzaerx9nJ9U-dqh9zyG4HVmLTHam_yU7FHaP9KqwIrzJ0__Lw720NEw5OQwLPOPs6F_qAMd3N2yRXpf0NzNAZULjz-gMJoC6GTA2i2ltis_T2RwXV85Qh0-K89eDjvfj9rCWLGEFuO1KA__U_99gA-BVmB35R4e_s_Z2J0-HppPFidUgpLGC73Hd7jCKjGf6s-hGLWwBvlRZaPfIR7iA0GgN6hS7aTfkxuLm_NCtSi153HrPnpfyeq3PVeA9glQf53__3xSJUFEiAxF4zjXblPP8zdJQ_yD6RNCJT1OvMAqTVXxP4pXlKPTyF_RS9OQY1nDyxK2yYV4cSFp-Q-ez71SEavn7MVjlly-GpfamAjtE85-rk_Tzg7EfofMSrdixqyj_SKS-7OEwOOFPQl6t8_9_2ftanzv38WB0H7Qr8_efZPezVZNFlYrIjo1JpdU9r6_Cz3xZDkx0iq6-4ESZu7fqC4O7icKZDiJS28Mz4pUJVtAQAw9DJVg-yZ--c1oOkJeQuCBlAIsdu8Ie1ZmtAtkJIFJHIN_iC0QK4FD9JXDFgqSBplt-80yvCW-whpWu1ytFrB-o3GDkLMyED3XqGp0tZ6ptCkXNBO6teNuiSAjJ21aKEJPzZV2I_dnyl3SB0Nbg7QYpZDLn-DaTciFD3oflHVziuoSVpbADIoeqkysjthwc.ujRGFQZ1pgdk_e6Tl9xi-g
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 991

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Customer xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:FullyQualifiedName>Pronto Waste Service Inc.</ns0:FullyQualifiedName>
  <ns0:CompanyName>Pronto Waste Service Inc.</ns0:CompanyName>
  <ns0:DisplayName>Pronto Waste Service Inc._1656692863</ns0:DisplayName>
  <ns0:PrimaryPhone>
    <ns0:FreeFormNumber>(305) 691-0020</ns0:FreeFormNumber>
  </ns0:PrimaryPhone>
  <ns0:PrimaryEmailAddr>
    <ns0:Address>accounting@prontowasteservice.com</ns0:Address>
  </ns0:PrimaryEmailAddr>
  <ns0:BillAddr>
    <ns0:Line1>7000 NW 35 AVE</ns0:Line1>
    <ns0:City>Miami</ns0:City>
    <ns0:CountrySubDivisionCode>Florida</ns0:CountrySubDivisionCode>
    <ns0:PostalCode>33147</ns0:PostalCode>
  </ns0:BillAddr>
  <ns0:ShipAddr>
    <ns0:Line1>7000 NW 35 AVE</ns0:Line1>
    <ns0:City>Miami</ns0:City>
    <ns0:CountrySubDivisionCode>Florida</ns0:CountrySubDivisionCode>
    <ns0:PostalCode>33147</ns0:PostalCode>
  </ns0:ShipAddr>
</ns0:Customer>


