REQUEST URI FOR SEQUENCE ID 05479
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..7rgyVbu7Q6XcwXyGTZx4Gw.QxMIQ0iqJa9qkF6hPavTFoXj46jY1XIM-iDzmrtBkHxnuJsq-drk2d1e7oX26XuUsd0WwY1xfOoIYAOIBDZpabgL18_Gbj76gxKQ_4Ce5jHrZvxCgC5kcHq9RpPgiQ_chPjXgabKq7m3eJtCZ-yibvflGJoIVXZ7w75MH3q8Kvk_xYILwWr2fwFzDBJ3FuxWrpENTqsyFAgd_hu65aC_waBcYigRutuUcNGgOjaF04IdNDrTlTRgfM9dF-5HDsUCrMHQdcRjYGhcyl1jbz79yL9tz5LuU3IIyrpohsIk7UUtpELEs9Hu6ZFoER4e8E3JiLTO4kBO3SATcxaglMVjRyMelfpjjmhIeXaNpeM7p5WV3MErK8C8rSpKeAO7Ww-aAikl852zKMa9Sa4Raum3Rs1omdBKMEQo9b4-duwbW4qA7CdqLpjP5PFw2eDFftte3uFk9_QF5LdTNsCEjOSL_Wa8u2axCwUME18MJhuBBVuwKYh3vqKxskqWisDzhAzEq1Rbr9ggUtbsXNp9-SdQCj7_LF6LUmaxRhQq-Wq8TqJB7XQpjVGl4ZtGHaE1tOdCirJS7O8429wkB4LN5HTg8C9bL5LJHTxzDcuyKOyfxoRFzTRbF6CguHIyqBtgacwmfkCspMUeHEEpbyq9acpOMOPq_-FzNbfJlp_1jkQnaUHkatT7gB1OZQIug8RVopyW9dENflzHPb3_pCALB_4lXAg76hv7KELnvFCqYgCkvHA.AsLJCO8sbobM9BjtlVb63A
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 2306

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>1780 SW 5 STREET MIAMI FL 3313</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>317_1646930923</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - ANGULAR STONE LLC</ns0:Description>
    <ns0:Amount>20.00</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>20.00</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Firm Mail</ns0:Description>
    <ns0:Amount>3.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Firm Mail">18</ns0:ItemRef>
      <ns0:UnitPrice>3.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail</ns0:Description>
    <ns0:Amount>5.42</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Certified Mail">17</ns0:ItemRef>
      <ns0:UnitPrice>5.42</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>CES-70191120000063845584-317</ns0:Description>
    <ns0:Amount>6.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Manual">19</ns0:ItemRef>
      <ns0:UnitPrice>6.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>195</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


