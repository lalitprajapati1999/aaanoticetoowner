RESPONSE URI FOR SEQUENCE ID 00005
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/customer?minorversion=47

RESPONSE HEADERS
================
date: Tue, 02 Feb 2021 13:33:44 GMT
content-type: application/xml;charset=UTF-8
content-length: 1354
server: nginx
strict-transport-security: max-age=15552000
intuit_tid: 1-601954b7-248342213dd9db4b3afdee8d
x-spanid: f8b88080-0755-4e42-9fa7-4ef58c88aad6
x-amzn-trace-id: Root=1-601954b7-248342213dd9db4b3afdee8d
set-cookie: JSESSIONID=C4DBAF5DFB8DD1AD283D55D1CF96EA85.c40-pprdc40uw2apv089017-stack-b; Domain=qbo.intuit.com; Path=/; Secure; HttpOnly
qbo-version: 1950.187
service-time: total=247, db=44
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private
vary: Accept-Encoding
x-xss-protection: 1; mode=block

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2021-02-02T05:33:44.149-08:00">
  <Customer domain="QBO" sparse="false">
    <Id>210</Id>
    <SyncToken>0</SyncToken>
    <MetaData>
      <CreateTime>2021-02-02T05:33:44-08:00</CreateTime>
      <LastUpdatedTime>2021-02-02T05:33:44-08:00</LastUpdatedTime>
    </MetaData>
    <FullyQualifiedName>Neo_1612272823</FullyQualifiedName>
    <CompanyName>Neo</CompanyName>
    <DisplayName>Neo_1612272823</DisplayName>
    <PrintOnCheckName>Neo</PrintOnCheckName>
    <Active>true</Active>
    <PrimaryPhone>
      <FreeFormNumber>(745) 896-3524</FreeFormNumber>
    </PrimaryPhone>
    <PrimaryEmailAddr>
      <Address>truptijdev@gmail.com</Address>
    </PrimaryEmailAddr>
    <DefaultTaxCodeRef>3</DefaultTaxCodeRef>
    <Taxable>true</Taxable>
    <BillAddr>
      <Id>211</Id>
      <Line1>truptijdev@gmail.com</Line1>
      <CountrySubDivisionCode>Maharashtra</CountrySubDivisionCode>
      <PostalCode>41215</PostalCode>
    </BillAddr>
    <ShipAddr>
      <Id>212</Id>
      <Line1>truptijdev@gmail.com</Line1>
      <CountrySubDivisionCode>Maharashtra</CountrySubDivisionCode>
      <PostalCode>41215</PostalCode>
    </ShipAddr>
    <Job>false</Job>
    <BillWithParent>false</BillWithParent>
    <Balance>0</Balance>
    <BalanceWithJobs>0</BalanceWithJobs>
    <CurrencyRef name="United States Dollar">USD</CurrencyRef>
    <PreferredDeliveryMethod>None</PreferredDeliveryMethod>
    <IsProject>false</IsProject>
  </Customer>
</IntuitResponse>


