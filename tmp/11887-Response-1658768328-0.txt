RESPONSE URI FOR SEQUENCE ID 11887
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

RESPONSE HEADERS
================
date: Mon, 25 Jul 2022 16:58:48 GMT
content-type: application/xml;charset=UTF-8
content-length: 4685
intuit_tid: 1-62decbc8-6a66076c4753405803c6a154
x-spanid: febb241d-0d0b-486d-bd21-315718caca9f
x-amzn-trace-id: Root=1-62decbc8-6a66076c4753405803c6a154
x-content-type-options: nosniff
x-xss-protection: 1; mode=block
qbo-version: 1985.162
service-time: total=392, db=120
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private
x-envoy-upstream-service-time: 430
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-07-25T09:58:48.608-07:00">
  <Invoice domain="QBO" sparse="false">
    <Id>2087</Id>
    <SyncToken>0</SyncToken>
    <MetaData>
      <CreateTime>2022-07-25T09:58:48-07:00</CreateTime>
      <LastUpdatedTime>2022-07-25T09:58:48-07:00</LastUpdatedTime>
    </MetaData>
    <CustomField>
      <DefinitionId>2</DefinitionId>
      <Name>sales2</Name>
      <Type>StringType</Type>
      <StringValue>Notice to Owner/Preliminary No</StringValue>
    </CustomField>
    <CustomField>
      <DefinitionId>3</DefinitionId>
      <Name>sales3</Name>
      <Type>StringType</Type>
      <StringValue>Block 3 Lot 1 – Permit Pendi</StringValue>
    </CustomField>
    <DocNumber>861_1658768327</DocNumber>
    <TxnDate>2022-07-25</TxnDate>
    <CurrencyRef name="United States Dollar">USD</CurrencyRef>
    <Line>
      <Id>1</Id>
      <LineNum>1</LineNum>
      <Description>Contracted By - ESTRELLA PLUMBING INC</Description>
      <Amount>35.00</Amount>
      <DetailType>SalesItemLineDetail</DetailType>
      <SalesItemLineDetail>
        <ItemRef name="SERVICES:NTO">32</ItemRef>
        <UnitPrice>35</UnitPrice>
        <Qty>1</Qty>
        <ItemAccountRef name="Services">1</ItemAccountRef>
        <TaxCodeRef>NON</TaxCodeRef>
        <TaxClassificationRef>EUC-99990201-V1-00020000</TaxClassificationRef>
      </SalesItemLineDetail>
    </Line>
    <Line>
      <Id>2</Id>
      <LineNum>2</LineNum>
      <Description>Additional address charges</Description>
      <Amount>285.00</Amount>
      <DetailType>SalesItemLineDetail</DetailType>
      <SalesItemLineDetail>
        <ItemRef name="SERVICES:NTO- Additional Address">34</ItemRef>
        <UnitPrice>15</UnitPrice>
        <Qty>19</Qty>
        <ItemAccountRef name="Services">1</ItemAccountRef>
        <TaxCodeRef>NON</TaxCodeRef>
        <TaxClassificationRef>EUC-99990201-V1-00020000</TaxClassificationRef>
      </SalesItemLineDetail>
    </Line>
    <Line>
      <Id>3</Id>
      <LineNum>3</LineNum>
      <Description>Rush Fee</Description>
      <Amount>12.50</Amount>
      <DetailType>SalesItemLineDetail</DetailType>
      <SalesItemLineDetail>
        <ItemRef name="SERVICES:Rush fee">33</ItemRef>
        <UnitPrice>12.5</UnitPrice>
        <Qty>1</Qty>
        <ItemAccountRef name="Services">1</ItemAccountRef>
        <TaxCodeRef>NON</TaxCodeRef>
        <TaxClassificationRef>EUC-99990201-V1-00020000</TaxClassificationRef>
      </SalesItemLineDetail>
    </Line>
    <Line>
      <Id>4</Id>
      <LineNum>4</LineNum>
      <Description>Firm Mail</Description>
      <Amount>3.95</Amount>
      <DetailType>SalesItemLineDetail</DetailType>
      <SalesItemLineDetail>
        <ItemRef name="MAILING:Firm Mail">18</ItemRef>
        <UnitPrice>3.95</UnitPrice>
        <Qty>1</Qty>
        <ItemAccountRef name="Services">1</ItemAccountRef>
        <TaxCodeRef>NON</TaxCodeRef>
        <TaxClassificationRef>EUC-99990201-V1-00020000</TaxClassificationRef>
      </SalesItemLineDetail>
    </Line>
    <Line>
      <Id>5</Id>
      <LineNum>5</LineNum>
      <Description>Certified Mail with Electronic Return Receipt</Description>
      <Amount>15.32</Amount>
      <DetailType>SalesItemLineDetail</DetailType>
      <SalesItemLineDetail>
        <ItemRef name="MAILING:Certified Mail ESRR">15</ItemRef>
        <UnitPrice>7.66</UnitPrice>
        <Qty>2</Qty>
        <ItemAccountRef name="Services">1</ItemAccountRef>
        <TaxCodeRef>NON</TaxCodeRef>
        <TaxClassificationRef>EUC-99990201-V1-00020000</TaxClassificationRef>
      </SalesItemLineDetail>
    </Line>
    <Line>
      <Amount>351.77</Amount>
      <DetailType>SubTotalLineDetail</DetailType>
      <SubTotalLineDetail/>
    </Line>
    <TxnTaxDetail>
      <TxnTaxCodeRef>3</TxnTaxCodeRef>
      <TotalTax>0</TotalTax>
      <TaxLine>
        <Amount>0</Amount>
        <DetailType>TaxLineDetail</DetailType>
        <TaxLineDetail>
          <TaxRateRef>3</TaxRateRef>
          <PercentBased>true</PercentBased>
          <TaxPercent>6</TaxPercent>
          <NetAmountTaxable>0</NetAmountTaxable>
        </TaxLineDetail>
      </TaxLine>
      <TaxLine>
        <Amount>0</Amount>
        <DetailType>TaxLineDetail</DetailType>
        <TaxLineDetail>
          <TaxRateRef>4</TaxRateRef>
          <PercentBased>true</PercentBased>
          <TaxPercent>1</TaxPercent>
          <NetAmountTaxable>0</NetAmountTaxable>
        </TaxLineDetail>
      </TaxLine>
    </TxnTaxDetail>
    <CustomerRef name="STEVE F BERGER">36</CustomerRef>
    <BillAddr>
      <Id>37</Id>
      <Line1>8131 NW 91 ST</Line1>
      <City>MIAMI</City>
      <CountrySubDivisionCode>FLORIDA</CountrySubDivisionCode>
      <PostalCode>33166</PostalCode>
    </BillAddr>
    <ShipAddr>
      <Id>37</Id>
      <Line1>8131 NW 91 ST</Line1>
      <City>MIAMI</City>
      <CountrySubDivisionCode>FLORIDA</CountrySubDivisionCode>
      <PostalCode>33166</PostalCode>
    </ShipAddr>
    <FreeFormAddress>true</FreeFormAddress>
    <ShipFromAddr>
      <Id>1518</Id>
      <Line1>PO Box 22821</Line1>
      <Line2>Hialeah, FL  33002 US</Line2>
    </ShipFromAddr>
    <SalesTermRef name="Net 15">2</SalesTermRef>
    <DueDate>2022-08-09</DueDate>
    <TotalAmt>351.77</TotalAmt>
    <ApplyTaxAfterDiscount>false</ApplyTaxAfterDiscount>
    <PrintStatus>NeedToPrint</PrintStatus>
    <EmailStatus>NotSet</EmailStatus>
    <Balance>351.77</Balance>
    <TaxExemptionRef name="service">99</TaxExemptionRef>
    <AllowIPNPayment>false</AllowIPNPayment>
    <AllowOnlinePayment>false</AllowOnlinePayment>
    <AllowOnlineCreditCardPayment>false</AllowOnlineCreditCardPayment>
    <AllowOnlineACHPayment>false</AllowOnlineACHPayment>
  </Invoice>
</IntuitResponse>


