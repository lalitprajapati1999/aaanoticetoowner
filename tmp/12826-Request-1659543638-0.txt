REQUEST URI FOR SEQUENCE ID 12826
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..axOWPax9Cm5Lgr9xJTjeEQ.lIajMRb49p9TEXSXK17dhCXS_-S6IokJBKeS2B_LiiOADcqh7gYYtWbcxVSKFT-3Ff82Ud8MLLHOVzxwaIqithezjw4tU9HLT_rWFRC0CZuRRVu3GDMkpQ-9wBoBw0M-8s55SA_M9unjBnu5YJsaln6rvLRMOcB03glsqR9aoCKpuQsdc_3cRC-OtDYoDMaLVfsP1AhRdAQ9muy2j6YafI6sb8LkU3jYCX_t5qYf6jtgWVKqqcC2ChpklBXd7Pm0EUHQvYqT7fuj6rWz4BKUMpXmzUX4VOkHNWkJuTredMfhw9qqSE0j2Q_vJUsEP8_U0iLRepnNk-jDr5oS9cSTjScVc42eK-uyOay-KzqMvFzOik0v_n4R9dntPu_ICBRaJLABRBrnafGKYGd8fvsCaRyjcjVXHglBOGergRvETRrvYUUMLQnzVNwmhPmlfoW1SHiuQQUaIf0pm21wBeVxFlkdGiT7eAnzBpTBPjKNzT87yoRTcRN6fCKB2tP7znX2pvP3LJ1gjOMSA9yg_QLPTTm3EStVN_Vgcm6wnwJaud1Jo0t67H4tSACzFVfqN8mYEWZrFnpVkhVKATZ7QL0ipNX_abOOcKEd7_QYUGh2Zucb4klRdTybbswrhcAwXCzzMnKL9nhPBs5NydpkKGCgKQedV0Ilt5IGPwVr4cDI5e5tbd15iiccZTwEBsxXJdO2qZn1ejsPPt6Q6j5Rfg1pOS6UcWUE7jWqFgFbZyfaBAs.aPobc14V0tHkjSB7stSTWQ
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 2778

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>NTO 342- correction  recipient: G7 Holding LLC</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>5860 N BAY ROAD MIAMI BEACH 33</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>380_1659543638</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - POWER PLUMBING</ns0:Description>
    <ns0:Amount>30</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>30</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Rush Fee</ns0:Description>
    <ns0:Amount>12.50</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Rush Hour Charges">33</ns0:ItemRef>
      <ns0:UnitPrice>12.50</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Firm Mail</ns0:Description>
    <ns0:Amount>3.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Firm Mail">18</ns0:ItemRef>
      <ns0:UnitPrice>3.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail</ns0:Description>
    <ns0:Amount>16.26</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Certified Mail">17</ns0:ItemRef>
      <ns0:UnitPrice>5.42</ns0:UnitPrice>
      <ns0:Qty>3</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail with Electronic Return Receipt</ns0:Description>
    <ns0:Amount>7.22</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Electronic Return Receipt">15</ns0:ItemRef>
      <ns0:UnitPrice>7.22</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>36</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


