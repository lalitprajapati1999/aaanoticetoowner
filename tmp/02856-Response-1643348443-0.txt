RESPONSE URI FOR SEQUENCE ID 02856
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/customer?minorversion=47

RESPONSE HEADERS
================
date: Fri, 28 Jan 2022 05:40:43 GMT
content-type: application/xml;charset=UTF-8
content-length: 1517
intuit_tid: 1-61f381da-3445b3c970d1a99b6ef117ac
x-spanid: 84dd5374-ef72-4538-9cb7-07cf484f1f94
x-amzn-trace-id: Root=1-61f381da-3445b3c970d1a99b6ef117ac
x-content-type-options: nosniff
qbo-version: 1973.097
service-time: total=338, db=61
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private
x-envoy-upstream-service-time: 379
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-01-27T21:40:42.944-08:00">
  <Customer domain="QBO" sparse="false">
    <Id>251</Id>
    <SyncToken>0</SyncToken>
    <MetaData>
      <CreateTime>2022-01-27T21:40:42-08:00</CreateTime>
      <LastUpdatedTime>2022-01-27T21:40:42-08:00</LastUpdatedTime>
    </MetaData>
    <FullyQualifiedName>B.C. ARVIZU CONSTRUCTION INCORPORATE_1643348442</FullyQualifiedName>
    <CompanyName>B.C. ARVIZU CONSTRUCTION INCORPORATE</CompanyName>
    <DisplayName>B.C. ARVIZU CONSTRUCTION INCORPORATE_1643348442</DisplayName>
    <PrintOnCheckName>B.C. ARVIZU CONSTRUCTION INCORPORATE</PrintOnCheckName>
    <Active>true</Active>
    <PrimaryPhone>
      <FreeFormNumber>(786) 229-0031</FreeFormNumber>
    </PrimaryPhone>
    <PrimaryEmailAddr>
      <Address>main@bcarvizu.com</Address>
    </PrimaryEmailAddr>
    <DefaultTaxCodeRef>3</DefaultTaxCodeRef>
    <Taxable>true</Taxable>
    <BillAddr>
      <Id>536</Id>
      <Line1>26820 SW 187 AVENUE</Line1>
      <City>Homestead</City>
      <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
      <PostalCode>33031</PostalCode>
    </BillAddr>
    <ShipAddr>
      <Id>537</Id>
      <Line1>26820 SW 187 AVENUE</Line1>
      <City>Homestead</City>
      <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
      <PostalCode>33031</PostalCode>
    </ShipAddr>
    <Job>false</Job>
    <BillWithParent>false</BillWithParent>
    <Balance>0</Balance>
    <BalanceWithJobs>0</BalanceWithJobs>
    <CurrencyRef name="United States Dollar">USD</CurrencyRef>
    <PreferredDeliveryMethod>None</PreferredDeliveryMethod>
    <IsProject>false</IsProject>
  </Customer>
</IntuitResponse>


