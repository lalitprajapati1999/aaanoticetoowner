REQUEST URI FOR SEQUENCE ID 02857
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/customer?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..CQB6tAHrM9a8zHdzTkrAkA.VHjcMax-2ZQSXXllOC4j5fbwkC5-1JdQ4uznFoIXOeiNDHXLbaGfejIdYeK30x6M-ary_OrSpZ9YH1MDmvCE7O8iPV5HCxl33Jyd3RTLw3MkljebcVEg07fgcMz8luDnuF1f8uT0yD2bEsnZKTEu3KYbuEzs82y6C3VeVqgIL9TmgytvtakoGTPShmLebLGj4Ch1PTRd7on8-GnUIImVWpsmuboQvn4n3QXIcjUMNqlh_XE-tY7g6FJwJJj3kBfnm_L5WvSQjTEgKLTNECExKimPBk9FnE_58-gPUIqkWnfZoqAGiOzWdXAadZgy-99QgS5BwHP0BQtTCpBLOsNjQ-MyGxVpX5PCSCTFspYIrwolbO3IfHbxsf02B-RZOvKsb4RBgXy2HJcn7wAoDniRQHRbL1JyJwLzjKw85qHC-p36sCyhoGCKRTYcpSRBPi--fwC-14Gka9-W1jzF6UxawpoocRBt6_weGq1UJIdyGx164zn7FnzLfplXW1MeKcBNuqueDTbGR85YMUvyUMwwiL1Uf22afNlnul95ELlQ681jZJgMKKTQEKH7CgPqitVxrmxgN-MVfHZpWctQrIhP6Iz1deOhT1ZJ-pBqKadygGWysB53p1LyLridyOqzarmK2k-6TSBMeZdZoNIVBcTzbSicTb6wrezKf0fFGyfNZNT4zTXdzAGKEos5GY0pXkLpA9xfDj4qUmwFLedTKTZZ5kC1drgbpv5iWzpStnVRNCc.h4QJ721fEr-g4pXLPKzz6w
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 983

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Customer xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:FullyQualifiedName>MARTINEZ TRUSS CO.</ns0:FullyQualifiedName>
  <ns0:CompanyName>MARTINEZ TRUSS CO.</ns0:CompanyName>
  <ns0:DisplayName>MARTINEZ TRUSS CO._1643435787</ns0:DisplayName>
  <ns0:PrimaryPhone>
    <ns0:FreeFormNumber>(305) 883-6261</ns0:FreeFormNumber>
  </ns0:PrimaryPhone>
  <ns0:PrimaryEmailAddr>
    <ns0:Address>carolina@martineztruss.net</ns0:Address>
  </ns0:PrimaryEmailAddr>
  <ns0:BillAddr>
    <ns0:Line1>9280 NW SOUTH RIVER DR.</ns0:Line1>
    <ns0:City>Medley</ns0:City>
    <ns0:CountrySubDivisionCode>Florida</ns0:CountrySubDivisionCode>
    <ns0:PostalCode>33166</ns0:PostalCode>
  </ns0:BillAddr>
  <ns0:ShipAddr>
    <ns0:Line1>9280 NW SOUTH RIVER DR.</ns0:Line1>
    <ns0:City>Medley</ns0:City>
    <ns0:CountrySubDivisionCode>Florida</ns0:CountrySubDivisionCode>
    <ns0:PostalCode>33166</ns0:PostalCode>
  </ns0:ShipAddr>
</ns0:Customer>


