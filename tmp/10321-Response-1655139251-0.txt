RESPONSE URI FOR SEQUENCE ID 10321
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

RESPONSE HEADERS
================
date: Mon, 13 Jun 2022 16:54:11 GMT
content-type: application/xml
content-length: 509
intuit_tid: 1-62a76bb2-126038fa18fbac854b218674
x-spanid: 2e4c3f54-2dd4-4a60-b201-8ab41045d3de
x-amzn-trace-id: Root=1-62a76bb2-126038fa18fbac854b218674
x-content-type-options: nosniff
x-envoy-upstream-service-time: 533
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-06-13T09:54:10.650-07:00">
  <Fault type="ValidationFault">
    <Error code="6000" element="">
      <Message>A business validation error has occurred while processing your request</Message>
      <Detail>Business Validation Error: You cannot enter more than 31 characters in the sales_custom_1_val field. You tried entering CICC 7040-..., which is 40 characters.</Detail>
    </Error>
  </Fault>
</IntuitResponse>


