REQUEST URI FOR SEQUENCE ID 06103
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..sq5Yf6oSoBTajCH_PRTy_Q.3XyVeVpS_C34dBJmJ47hNlpZZxYjYpxXqfhNIVY7JvcKdbWYKNEV0xSGFtT-2tVaZiExVklrWZeso2WVUXXgPBYcOJAt2W7N-kvchj4vNh7oeR5r8EyHckUbLVTfrk-C2FFz0FDpxt0D4_KTTawHT5wdLCVok-ATLs22H8FgLfFGxgZ9K3RBMaZQlnlJ59m9ivup0_0evcerWvV8i9agW_iAKaBdyKOLG4NHtPLN6J-e-a6ZmG7G7tffbIOb4hVMRgPU7ygT7-YRTre0cOa_dEgLdqCeqIqXqWruA8I3CUGEsS9O77AGfY9yT_vk2mRK9fuybo85XeERc0ao4ftr9gO7Nb4PNytcpxAcwFslByPZgi48P22p1Xdt1HmIh8LhOasfkOLIibXRhScZjk8FSGym-cWxSgfx5Ps62676muxhxkqjdMaHYFfJlLcKALjky8Z9NdkpXuhBsx6bVZ4HRzz19xhuVmm72BeXiZub9rxWh1Y5wPsLZqb91jHJ-vNhKy10nmwvDOWOJUR--guQazU7adpjHZbfZq2Jq8lt--zl037TCn1D9aGc0S7S7UdNfmAPWEXUMVD07EcKNXVjdDrC_JTuAaBXmnTX6jazlX_bBgY7gzlUaOF6wNLMFMUJjUounHmlGIu7rSZx9NheM7X5TOLbHLTmBQ_tFC62CPSVJVcvWhkq76TBZaUe4_QsZzRwS-UXvcdY4I6461E_6k23hWswSR-CKTUXW6X_OI0.ypk1ClIuVCPttohxRvx3Xg
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 2347

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>1420 NW N RIVER DRIVE STE 330 </ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>349_1647968363</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - FLO PRO SERVICE CORPORATION</ns0:Description>
    <ns0:Amount>35</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>35</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Firm Mail</ns0:Description>
    <ns0:Amount>3.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Firm Mail">18</ns0:ItemRef>
      <ns0:UnitPrice>3.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail</ns0:Description>
    <ns0:Amount>10.84</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Certified Mail">17</ns0:ItemRef>
      <ns0:UnitPrice>5.42</ns0:UnitPrice>
      <ns0:Qty>2</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail with Electronic Return Receipt</ns0:Description>
    <ns0:Amount>14.44</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Electronic Return Receipt">15</ns0:ItemRef>
      <ns0:UnitPrice>7.22</ns0:UnitPrice>
      <ns0:Qty>2</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>36</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


