RESPONSE URI FOR SEQUENCE ID 12093
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

RESPONSE HEADERS
================
date: Tue, 26 Jul 2022 20:07:52 GMT
content-type: application/xml
content-length: 509
intuit_tid: 1-62e04997-02ab1b5d280dcabe5a2abe6a
x-spanid: c40a3e27-e60c-4f91-8dfd-d0f310546a8a
x-amzn-trace-id: Root=1-62e04997-02ab1b5d280dcabe5a2abe6a
x-content-type-options: nosniff
x-envoy-upstream-service-time: 481
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-07-26T13:07:51.690-07:00">
  <Fault type="ValidationFault">
    <Error code="6000" element="">
      <Message>A business validation error has occurred while processing your request</Message>
      <Detail>Business Validation Error: You cannot enter more than 31 characters in the sales_custom_1_val field. You tried entering CICC 7040-..., which is 40 characters.</Detail>
    </Error>
  </Fault>
</IntuitResponse>


