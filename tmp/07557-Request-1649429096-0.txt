REQUEST URI FOR SEQUENCE ID 07557
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..RaN5pjVZD0kd3pPEWAmHBA.2o9VFf0Rav2QczWH9xxKd8mMFFkYbPX2iXY0LAqdgNk5hm9NfX7G2QGSTGXsTTz3kWr-k2NyqMR-GxIXQH0BtrdosO2LEoaYkvi47G-buvsZKDGt83UXbaakoW5Hk20YMKJjKsf5iIMpQjdmqAAU25z4wpfabyA-MKJRn6PyQ0mjN3RVXypJNXIa9baOeqs5XS0XNREG7j5GjN33nLFk85JquB8QyODqK63wWUKsJE3PWrXZhqlnbkoRUNnY_zEYKK1JUC3B5BqlaglgN1GzIurSouHwixIfHnM_SrVNppkvd2g6RvZmZhQxbEAHPg2Njoj-t-Zm2R_icvfXGu705q5KEGdLZ0P5qMLohiL9P-U2lJVshwQOqK8yJFmFpZJntvLRkFCBRO1vyxpTzuzu0CsXkSVWXIGdBB_M3nCo8N80JMX87ZDGpwWb9K3OaRUb_xv2uT_nDrfaJBMOzQ_FS0cAEPJvEm8nHugsAXgI7RSR2Uolza8P5fFGgu4sLJRpeZUPvAMH3cHOgzpDmunmIGqCJr3hJEv0FBOO60s7caL5beFd1G_aPKls1roP69Yw5qPiul2nTInnbU21rtpRM4cFYyZ-5o6Y-sNdHgeJN32mafoUxbEYyrSq_2hyVD-qnFVmOgAdLSnOejRWRox0f_tDi_EKZQcZUd5unoj_Yv8RVSVofE9HD8hxhBATa6Gh0r6ER17uZLpOs0FqxRbhhoyefZSyMMWSidZbYEHSQHk._cbq4mSdOgUs3pLMp-szYw
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 2353

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>3130 PLAZA ST MIAMI FL 33133</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>430_1649429096</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - N&amp;R CONSTRUCTION SERVICES.</ns0:Description>
    <ns0:Amount>20.00</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>20.00</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Firm Mail</ns0:Description>
    <ns0:Amount>3.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Firm Mail">18</ns0:ItemRef>
      <ns0:UnitPrice>3.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail</ns0:Description>
    <ns0:Amount>5.42</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Certified Mail">17</ns0:ItemRef>
      <ns0:UnitPrice>5.42</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail with Electronic Return Receipt</ns0:Description>
    <ns0:Amount>7.22</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Electronic Return Receipt">15</ns0:ItemRef>
      <ns0:UnitPrice>7.22</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>195</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


