RESPONSE URI FOR SEQUENCE ID 00444
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/query?minorversion=47

RESPONSE HEADERS
================
date: Thu, 19 Aug 2021 10:44:00 GMT
content-type: application/xml;charset=UTF-8
content-length: 3492
server: nginx
intuit_tid: 1-611e35f0-28ea0f9908c45b5e03b37103
x-spanid: b1477940-a033-4de1-a55b-323de3519c32
x-amzn-trace-id: Root=1-611e35f0-28ea0f9908c45b5e03b37103
x-content-type-options: nosniff
qbo-version: 1963.140
service-time: total=220, db=85
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private
strict-transport-security: max-age=31536000

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2021-08-19T03:44:00.462-07:00">
  <QueryResponse startPosition="1" maxResults="1" totalCount="1">
    <Invoice domain="QBO" sparse="false">
      <Id>72</Id>
      <SyncToken>0</SyncToken>
      <MetaData>
        <CreateTime>2021-08-19T03:40:59-07:00</CreateTime>
        <LastUpdatedTime>2021-08-19T03:40:59-07:00</LastUpdatedTime>
      </MetaData>
      <CustomField>
        <DefinitionId>1</DefinitionId>
        <Type>StringType</Type>
        <StringValue>32234</StringValue>
      </CustomField>
      <CustomField>
        <DefinitionId>2</DefinitionId>
        <Type>StringType</Type>
        <StringValue>Non Payment Notice</StringValue>
      </CustomField>
      <CustomField>
        <DefinitionId>3</DefinitionId>
        <Type>StringType</Type>
        <StringValue>223 E. Concord Street</StringValue>
      </CustomField>
      <DocNumber>22_CYO_1629369658</DocNumber>
      <TxnDate>2021-08-19</TxnDate>
      <CurrencyRef name="United States Dollar">USD</CurrencyRef>
      <Line>
        <Id>1</Id>
        <LineNum>1</LineNum>
        <Description>Non Payment Notice
Contracted By - Sukanya D</Description>
        <Amount>30.00</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="SERVICES:Non Payment Notice">30</ItemRef>
          <UnitPrice>30</UnitPrice>
          <Qty>1</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>TAX</TaxCodeRef>
          <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Id>2</Id>
        <LineNum>2</LineNum>
        <Description>Firm Mail</Description>
        <Amount>1.00</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="MAILING:Firm Mail">18</ItemRef>
          <UnitPrice>1</UnitPrice>
          <Qty>1</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>TAX</TaxCodeRef>
          <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Amount>31.00</Amount>
        <DetailType>SubTotalLineDetail</DetailType>
        <SubTotalLineDetail/>
      </Line>
      <TxnTaxDetail>
        <TxnTaxCodeRef>3</TxnTaxCodeRef>
        <TotalTax>2.17</TotalTax>
        <TaxLine>
          <Amount>1.86</Amount>
          <DetailType>TaxLineDetail</DetailType>
          <TaxLineDetail>
            <TaxRateRef>3</TaxRateRef>
            <PercentBased>true</PercentBased>
            <TaxPercent>6</TaxPercent>
            <NetAmountTaxable>31.00</NetAmountTaxable>
          </TaxLineDetail>
        </TaxLine>
        <TaxLine>
          <Amount>0.31</Amount>
          <DetailType>TaxLineDetail</DetailType>
          <TaxLineDetail>
            <TaxRateRef>4</TaxRateRef>
            <PercentBased>true</PercentBased>
            <TaxPercent>1</TaxPercent>
            <NetAmountTaxable>31.00</NetAmountTaxable>
          </TaxLineDetail>
        </TaxLine>
      </TxnTaxDetail>
      <CustomerRef name="Neosoft_1612276794">211</CustomerRef>
      <BillAddr>
        <Id>213</Id>
        <Line1>harshad.khiste@neosoftmail.com</Line1>
        <CountrySubDivisionCode>Maharashtra</CountrySubDivisionCode>
        <PostalCode>33112</PostalCode>
      </BillAddr>
      <ShipAddr>
        <Id>214</Id>
        <Line1>harshad.khiste@neosoftmail.com</Line1>
        <CountrySubDivisionCode>Maharashtra</CountrySubDivisionCode>
        <PostalCode>33112</PostalCode>
      </ShipAddr>
      <FreeFormAddress>true</FreeFormAddress>
      <ShipFromAddr>
        <Id>306</Id>
        <Line1>PO Box 22821</Line1>
        <Line2>Hialeah, FL  33002 US</Line2>
      </ShipFromAddr>
      <SalesTermRef name="Net 15">2</SalesTermRef>
      <DueDate>2021-09-03</DueDate>
      <TotalAmt>33.17</TotalAmt>
      <ApplyTaxAfterDiscount>false</ApplyTaxAfterDiscount>
      <PrintStatus>NeedToPrint</PrintStatus>
      <EmailStatus>NotSet</EmailStatus>
      <Balance>33.17</Balance>
      <TaxExemptionRef/>
      <AllowIPNPayment>false</AllowIPNPayment>
      <AllowOnlinePayment>false</AllowOnlinePayment>
      <AllowOnlineCreditCardPayment>false</AllowOnlineCreditCardPayment>
      <AllowOnlineACHPayment>false</AllowOnlineACHPayment>
    </Invoice>
  </QueryResponse>
</IntuitResponse>


