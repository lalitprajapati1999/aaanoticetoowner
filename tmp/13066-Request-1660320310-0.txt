REQUEST URI FOR SEQUENCE ID 13066
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..SeUT0jX3tvhV30wcduNNPA.wUwDVySQ6Bs5DnPgDmtZd0enyIX2h5A42ja9LZVvIUnogUxnoGQ9wtKD35i3694vFO9K1ZV1aG62kt1ARKrwURd7CJv47D8GxWMxI-NL5t_uuXnAlLQvSXSgZXVmj1FsR738pum6yYd9K-B71ZHLceI_3z7bf3gYGYX2x089zvkESQByzOK3eVc8mwamjsLFBvBVBFvWe472r8Gdpoq5mAt0RQRwD82EtMuB4tQ24TATNb-70k2HWaczj_E8SBtqKMVPgl-HN8QBCRlP8AnSG0zZP_C7Gj2vdTUjX9JOoq-8VhU942J_iuxw0cjhG6JVSTqCxNkHqAQMwkB_Kj1IMIyFnQzego2lJNhDTBdxT1uN2-Q__OoOtv8DifdrXbBeVtS9-vC2pwZi_QuvyrKhZOvqd8A0PgzdqfjEzcnixed7aEfUJaPgv7TkKPSEddjPHHMknEEFOXfGX0SpRHLhYgFsG4JQ69XrXe1DXymSczBN2FQ1BTLI9tT8r6hb53Xm8myv3PxVu73hNrIFEayN53esSnkIsWmSxcfs_wam67q6htM2tbZ82EyN3T_pVesaNtSVyzVo22qi0h05RhZVRoog_N_1pj4dWRtGZI5mY8_Kw5geWx7t2VXsUhWrUnVC39eFu6h0D0aFlt2c1YA1nUgBg3eiJoX_TCyUT2q4HNCRuwS5bZIFOVL8W2MMa4l2ih5DOxc_DPW213WSstTm_U6Tj7dLHKF7B2JZ8ofgTtY.-cfPdWIixfyG2Nmoqh3ebw
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 2343

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue> 131 NW 45 ST MIAMI FL 33127 </ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>929_1660320310</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - CAMELL BUILDERS INC</ns0:Description>
    <ns0:Amount>20.00</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>20.00</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Rush Fee</ns0:Description>
    <ns0:Amount>15.00</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Rush Hour Charges">33</ns0:ItemRef>
      <ns0:UnitPrice>15.00</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Firm Mail</ns0:Description>
    <ns0:Amount>3.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Firm Mail">18</ns0:ItemRef>
      <ns0:UnitPrice>3.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail with Electronic Return Receipt</ns0:Description>
    <ns0:Amount>15.32</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Electronic Return Receipt">15</ns0:ItemRef>
      <ns0:UnitPrice>7.66</ns0:UnitPrice>
      <ns0:Qty>2</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>195</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


