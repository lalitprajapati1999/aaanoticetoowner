REQUEST URI FOR SEQUENCE ID 06609
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..7bYgQH_5KuUXPhFjjGpR7Q.xvt0cEfeC6AkO4iL5pSq315w5m5HqJR69PJp91bsoVifw0gD7oklRilIdgcmlB8Y2467ipWr6vuxN98uvfGafdwpjBVaDERSN0tZfvaiMc1k6YM3sGSprzIXp_RJV7_-7yjJf_CgfG9D9bExAnSeuzY1GFD9gnY0s23W79Oziu-SRYhG_dS4FodW7A9I8R4fmCZuAgABqZj5T652ynPLH6vNFPnK8amPAs9yYrUrfn2ze7U1qoRdRCbZbcg2ixOkPBizIKHE78NFmTjrBoVkgxRS0WQPOw4ce3dW2bk9J-cFcvSzEJsxQ4PwbttPJYl4DPB5Tal432ZXvoEA799bhpJbpAlqoFnEmlsziBgoC5V7BY0zMGNBoAHgvQ9o7JeCIE2UX6euV-7XG525z5BE-S_x-3io6Vbf5VfPwOkqX2po94e5MmjU9MfQBtqtWWzT_F2kda_g4jYRBxRgbsEK7vjvYEPZ91q0p3QLNxPSS8QsK3gUC-bP5U5EtDTGYCK8O8u82ORwgTiH9PkVT8JdZ7T7P3pTCwTfRReQA0VAXXjVSWo_BhqGgPzrejGW9-vz6zGX1QPGVxttFvO3erdcUpgxz5ygVlZlv1kBkRcSlppDOJdd6DC77OFwi8spqlNHET-dOiqMnkuxo0pvn-GqmI0U5DQKs_ZRZYV0Usd0xR2Ld3kFtJv-GDNI14iYy6q5Ep4JH5PD8xueeefrbPf1p72ZVC59Nn21k49h-_Svzlg.354dwmHSIjgLBaV2-KqghA
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 2009

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>JDS Development Group</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>1300 Monad Terrace Unit 8F, Mi</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>330_1649089656</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - JDS Development Group</ns0:Description>
    <ns0:Amount>45.00</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>45.00</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Firm Mail</ns0:Description>
    <ns0:Amount>3.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Firm Mail">18</ns0:ItemRef>
      <ns0:UnitPrice>3.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>CES7019112000063845997-330</ns0:Description>
    <ns0:Amount>6.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Manual">19</ns0:ItemRef>
      <ns0:UnitPrice>6.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>266</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


