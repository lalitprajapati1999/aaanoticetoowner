REQUEST URI FOR SEQUENCE ID 03433
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..D9bmKIvB1pjNJ-Uamku1wg.Hw9YRy_15oHIpc6CPIJb7TepGYpKW2c_WeOZCSpYit4VkRQ2-Nr8gQonNPe83YRfrsj0K9Ww2VXrXYCORMar19PtO-nHQqFWxfjTMJd-6qrnX0nynKNO3j_LQAkVTwyZ1fNDTmxYiQEN3-_ekqXjuCBhRWwuXUxWOHNOT2GfheVkIhnPDFZu5BilSG2HesKUm93mJJ35IsBx9lXlPMvPk0xgiiwA9YPiIsQSQNjUYX5BD4relOZ0RWIyFOwCLKkZU821xWbWNThC3UaC3l_VSP_PZfxZi8u2dnPrxK7lCnjRvim8xNcBXDmw7ffrhxBj0a6EnJnO3e36c7iexLqXqvBWEKnFYiqjBFG4zkM8qvU3JkMhNQQyjcDmMruyR6vhZTQW3J1z6o36avCcN71_XrrQnHe8RD3X_15uFXDxplOImGioFvn0YmQ4rFF5APBoIsYC9j_b9hp2Ddv1ui69DX25zZB8tHF7AoA4gO7jbxGtucPCkg2Szu0uCA7A75uog7u06Uj1AQq_ReKIDgDzb1F7kF4Dhb5LV33dbndjqnlqR_SZqDPQUbcGm9yhAAavytc26a0jTkcV_8C6n0gvJveNK-Y7FlfbYL3ABPTYRiaXGCiGDFc8WAgfgGV1d5-WrykcpzuAPZUHuyU5SWoWannILWPtwvWXAosbvQ6kyFGkYY_fswDff9u0E4RkvKiCtItZP9m9984yDgtlQvVjq1wjZz3YsutPloOWYG9jaxM.-TTty0ROwPl6rbx143gQcw
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 2699

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>10065 CLEARY BOULEVARDPLANTATI</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>280_1645711477</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - CLEIN E &amp; N</ns0:Description>
    <ns0:Amount>45.00</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>45.00</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Rush Fee</ns0:Description>
    <ns0:Amount>15.00</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Rush Hour Charges">33</ns0:ItemRef>
      <ns0:UnitPrice>15.00</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Firm Mail</ns0:Description>
    <ns0:Amount>3.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Firm Mail">18</ns0:ItemRef>
      <ns0:UnitPrice>3.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail</ns0:Description>
    <ns0:Amount>5.42</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Certified Mail">17</ns0:ItemRef>
      <ns0:UnitPrice>5.42</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail with Electronic Return Receipt</ns0:Description>
    <ns0:Amount>7.22</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Electronic Return Receipt">15</ns0:ItemRef>
      <ns0:UnitPrice>7.22</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>244</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


