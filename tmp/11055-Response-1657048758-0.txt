RESPONSE URI FOR SEQUENCE ID 11055
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/query?minorversion=47

RESPONSE HEADERS
================
date: Tue, 05 Jul 2022 19:19:18 GMT
content-type: application/xml;charset=UTF-8
content-length: 2725
intuit_tid: 1-62c48eb6-539b5b371c2d6bd87c178b8f
x-spanid: d44fa72d-91ef-48fd-9673-43ca16aeea1c
x-amzn-trace-id: Root=1-62c48eb6-539b5b371c2d6bd87c178b8f
x-content-type-options: nosniff
x-xss-protection: 1; mode=block
qbo-version: 1983.200
service-time: total=208, db=114
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private
x-envoy-upstream-service-time: 233
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-07-05T12:19:18.223-07:00">
  <QueryResponse startPosition="1" maxResults="1" totalCount="1">
    <Invoice domain="QBO" sparse="false">
      <Id>1921</Id>
      <SyncToken>1</SyncToken>
      <MetaData>
        <CreateTime>2022-07-05T11:26:15-07:00</CreateTime>
        <LastUpdatedTime>2022-07-05T11:28:43-07:00</LastUpdatedTime>
      </MetaData>
      <DocNumber>1027</DocNumber>
      <TxnDate>2022-06-30</TxnDate>
      <CurrencyRef name="United States Dollar">USD</CurrencyRef>
      <Line>
        <Id>1</Id>
        <LineNum>1</LineNum>
        <Description>WORK ORDER#643 CANCELLED</Description>
        <Amount>20.00</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="CANCELLED">44</ItemRef>
          <UnitPrice>20</UnitPrice>
          <Qty>1</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>NON</TaxCodeRef>
          <TaxClassificationRef>EUC-99990101-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Amount>20.00</Amount>
        <DetailType>SubTotalLineDetail</DetailType>
        <SubTotalLineDetail/>
      </Line>
      <TxnTaxDetail>
        <TxnTaxCodeRef>3</TxnTaxCodeRef>
        <TotalTax>0</TotalTax>
        <TaxLine>
          <Amount>0</Amount>
          <DetailType>TaxLineDetail</DetailType>
          <TaxLineDetail>
            <TaxRateRef>3</TaxRateRef>
            <PercentBased>true</PercentBased>
            <TaxPercent>6</TaxPercent>
            <NetAmountTaxable>0</NetAmountTaxable>
          </TaxLineDetail>
        </TaxLine>
        <TaxLine>
          <Amount>0</Amount>
          <DetailType>TaxLineDetail</DetailType>
          <TaxLineDetail>
            <TaxRateRef>4</TaxRateRef>
            <PercentBased>true</PercentBased>
            <TaxPercent>1</TaxPercent>
            <NetAmountTaxable>0</NetAmountTaxable>
          </TaxLineDetail>
        </TaxLine>
      </TxnTaxDetail>
      <CustomerRef name="CONCRETE BY M&amp;J INCORPORATE_1644811819">258</CustomerRef>
      <BillAddr>
        <Id>592</Id>
        <Line1>30105 SW 143RD COURT</Line1>
        <City>Homestead</City>
        <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
        <PostalCode>33033</PostalCode>
      </BillAddr>
      <ShipAddr>
        <Id>593</Id>
        <Line1>30105 SW 143RD COURT</Line1>
        <City>Homestead</City>
        <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
        <PostalCode>33033</PostalCode>
      </ShipAddr>
      <FreeFormAddress>true</FreeFormAddress>
      <ShipFromAddr>
        <Id>1408</Id>
        <Line1>PO Box 22821, Hialeah, FL, 33002, USA</Line1>
      </ShipFromAddr>
      <DueDate>2022-06-30</DueDate>
      <TotalAmt>20.00</TotalAmt>
      <ApplyTaxAfterDiscount>false</ApplyTaxAfterDiscount>
      <PrintStatus>NotSet</PrintStatus>
      <EmailStatus>NotSet</EmailStatus>
      <BillEmail>
        <Address>concretebymandj@gmail.com</Address>
      </BillEmail>
      <Balance>20.00</Balance>
      <TaxExemptionRef name="SERVICE">99</TaxExemptionRef>
      <AllowIPNPayment>false</AllowIPNPayment>
      <AllowOnlinePayment>false</AllowOnlinePayment>
      <AllowOnlineCreditCardPayment>true</AllowOnlineCreditCardPayment>
      <AllowOnlineACHPayment>true</AllowOnlineACHPayment>
    </Invoice>
  </QueryResponse>
</IntuitResponse>


