RESPONSE URI FOR SEQUENCE ID 07368
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/query?minorversion=47

RESPONSE HEADERS
================
date: Wed, 06 Apr 2022 23:06:14 GMT
content-type: application/xml;charset=UTF-8
content-length: 3340
intuit_tid: 1-624e1ce6-4fcbcb1e40bc8dce7961c67b
x-spanid: cdcf60cb-3596-44cf-9a7d-625274137a53
x-amzn-trace-id: Root=1-624e1ce6-4fcbcb1e40bc8dce7961c67b
x-content-type-options: nosniff
x-xss-protection: 1; mode=block
qbo-version: 1978.180
service-time: total=260, db=169
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private
x-envoy-upstream-service-time: 289
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-04-06T16:06:14.145-07:00">
  <QueryResponse startPosition="1" maxResults="1" totalCount="1">
    <Invoice domain="QBO" sparse="false">
      <Id>772</Id>
      <SyncToken>1</SyncToken>
      <MetaData>
        <CreateTime>2022-04-04T15:59:12-07:00</CreateTime>
        <LastUpdatedTime>2022-04-06T12:25:25-07:00</LastUpdatedTime>
      </MetaData>
      <DocNumber>1011</DocNumber>
      <TxnDate>2022-03-31</TxnDate>
      <CurrencyRef name="United States Dollar">USD</CurrencyRef>
      <Line>
        <Id>1</Id>
        <LineNum>1</LineNum>
        <Description>Satisfaction of Lien</Description>
        <Amount>145.00</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="SATISFACTIONS:Satisfaction of Lien">23</ItemRef>
          <UnitPrice>145</UnitPrice>
          <Qty>1</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>NON</TaxCodeRef>
          <TaxClassificationRef>EUC-99990201-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Id>2</Id>
        <LineNum>2</LineNum>
        <Amount>11.85</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="MAILING:Firm Mail">18</ItemRef>
          <UnitPrice>3.95</UnitPrice>
          <Qty>3</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>TAX</TaxCodeRef>
          <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Amount>156.85</Amount>
        <DetailType>SubTotalLineDetail</DetailType>
        <SubTotalLineDetail/>
      </Line>
      <TxnTaxDetail>
        <TxnTaxCodeRef>3</TxnTaxCodeRef>
        <TotalTax>0</TotalTax>
        <TaxLine>
          <Amount>0</Amount>
          <DetailType>TaxLineDetail</DetailType>
          <TaxLineDetail>
            <TaxRateRef>3</TaxRateRef>
            <PercentBased>true</PercentBased>
            <TaxPercent>6</TaxPercent>
            <NetAmountTaxable>0</NetAmountTaxable>
          </TaxLineDetail>
        </TaxLine>
        <TaxLine>
          <Amount>0</Amount>
          <DetailType>TaxLineDetail</DetailType>
          <TaxLineDetail>
            <TaxRateRef>4</TaxRateRef>
            <PercentBased>true</PercentBased>
            <TaxPercent>1</TaxPercent>
            <NetAmountTaxable>0</NetAmountTaxable>
          </TaxLineDetail>
        </TaxLine>
      </TxnTaxDetail>
      <CustomerRef name="MITO PLUMBING CORPORATION_1641670449">228</CustomerRef>
      <BillAddr>
        <Id>432</Id>
        <Line1>7879 NW 173 STREET</Line1>
        <City>Hialeah</City>
        <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
        <PostalCode>33015</PostalCode>
      </BillAddr>
      <ShipAddr>
        <Id>433</Id>
        <Line1>7879 NW 173 STREET</Line1>
        <City>Hialeah</City>
        <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
        <PostalCode>33015</PostalCode>
      </ShipAddr>
      <FreeFormAddress>true</FreeFormAddress>
      <ShipFromAddr>
        <Id>957</Id>
        <Line1>PO Box 22821, Hialeah, FL, 33002, USA</Line1>
      </ShipFromAddr>
      <SalesTermRef name="Net 30">3</SalesTermRef>
      <DueDate>2022-04-30</DueDate>
      <TotalAmt>156.85</TotalAmt>
      <ApplyTaxAfterDiscount>false</ApplyTaxAfterDiscount>
      <PrintStatus>NotSet</PrintStatus>
      <EmailStatus>EmailSent</EmailStatus>
      <BillEmail>
        <Address>mito@mitoplumbing.com,info@mitoplumbing.com</Address>
      </BillEmail>
      <Balance>156.85</Balance>
      <DeliveryInfo>
        <DeliveryType>Email</DeliveryType>
        <DeliveryTime>2022-04-06T12:24:00-07:00</DeliveryTime>
      </DeliveryInfo>
      <TaxExemptionRef name="SERVICE">99</TaxExemptionRef>
      <AllowIPNPayment>false</AllowIPNPayment>
      <AllowOnlinePayment>false</AllowOnlinePayment>
      <AllowOnlineCreditCardPayment>true</AllowOnlineCreditCardPayment>
      <AllowOnlineACHPayment>true</AllowOnlineACHPayment>
      <EInvoiceStatus>Viewed</EInvoiceStatus>
    </Invoice>
  </QueryResponse>
</IntuitResponse>


