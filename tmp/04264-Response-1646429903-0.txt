RESPONSE URI FOR SEQUENCE ID 04264
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/query?minorversion=47

RESPONSE HEADERS
================
date: Fri, 04 Mar 2022 21:38:23 GMT
content-type: application/xml;charset=UTF-8
content-length: 2721
intuit_tid: 1-622286cf-31a505645cfbc12c0768c390
x-spanid: 2d459a04-0214-4b30-a307-1b628a8e769a
x-amzn-trace-id: Root=1-622286cf-31a505645cfbc12c0768c390
x-content-type-options: nosniff
x-xss-protection: 1; mode=block
qbo-version: 1975.230
service-time: total=242, db=91
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private
x-envoy-upstream-service-time: 295
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-03-04T13:38:23.508-08:00">
  <QueryResponse startPosition="1" maxResults="1" totalCount="1">
    <Invoice domain="QBO" sparse="false">
      <Id>426</Id>
      <SyncToken>1</SyncToken>
      <MetaData>
        <CreateTime>2022-03-04T13:31:48-08:00</CreateTime>
        <LastUpdatedTime>2022-03-04T13:35:22-08:00</LastUpdatedTime>
      </MetaData>
      <DocNumber>1001</DocNumber>
      <TxnDate>2022-02-28</TxnDate>
      <CurrencyRef name="United States Dollar">USD</CurrencyRef>
      <Line>
        <Id>1</Id>
        <LineNum>1</LineNum>
        <Description>Claim of Lien</Description>
        <Amount>590.00</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="CLAIM OF LIEN:Claim of Lien">38</ItemRef>
          <UnitPrice>590</UnitPrice>
          <Qty>1</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>NON</TaxCodeRef>
          <TaxClassificationRef>EUC-99990201-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Amount>590.00</Amount>
        <DetailType>SubTotalLineDetail</DetailType>
        <SubTotalLineDetail/>
      </Line>
      <TxnTaxDetail>
        <TxnTaxCodeRef>3</TxnTaxCodeRef>
        <TotalTax>0</TotalTax>
        <TaxLine>
          <Amount>0</Amount>
          <DetailType>TaxLineDetail</DetailType>
          <TaxLineDetail>
            <TaxRateRef>3</TaxRateRef>
            <PercentBased>true</PercentBased>
            <TaxPercent>6</TaxPercent>
            <NetAmountTaxable>0</NetAmountTaxable>
          </TaxLineDetail>
        </TaxLine>
        <TaxLine>
          <Amount>0</Amount>
          <DetailType>TaxLineDetail</DetailType>
          <TaxLineDetail>
            <TaxRateRef>4</TaxRateRef>
            <PercentBased>true</PercentBased>
            <TaxPercent>1</TaxPercent>
            <NetAmountTaxable>0</NetAmountTaxable>
          </TaxLineDetail>
        </TaxLine>
      </TxnTaxDetail>
      <CustomerRef name="ADRIANA RINALDI">189</CustomerRef>
      <BillAddr>
        <Id>190</Id>
        <Line1>5800 SW 177TH AVE # 101</Line1>
        <City>MIAMI</City>
        <CountrySubDivisionCode>FLORIDA</CountrySubDivisionCode>
        <PostalCode>33193</PostalCode>
      </BillAddr>
      <ShipAddr>
        <Id>190</Id>
        <Line1>5800 SW 177TH AVE # 101</Line1>
        <City>MIAMI</City>
        <CountrySubDivisionCode>FLORIDA</CountrySubDivisionCode>
        <PostalCode>33193</PostalCode>
      </ShipAddr>
      <FreeFormAddress>true</FreeFormAddress>
      <ShipFromAddr>
        <Id>746</Id>
        <Line1>PO Box 22821, Hialeah, FL, 33002, USA</Line1>
      </ShipFromAddr>
      <SalesTermRef name="Net 30">3</SalesTermRef>
      <DueDate>2022-03-30</DueDate>
      <TotalAmt>590.00</TotalAmt>
      <ApplyTaxAfterDiscount>false</ApplyTaxAfterDiscount>
      <PrintStatus>NotSet</PrintStatus>
      <EmailStatus>NotSet</EmailStatus>
      <BillEmail>
        <Address>southfloridaconc@bellsouth.net</Address>
      </BillEmail>
      <Balance>590.00</Balance>
      <TaxExemptionRef/>
      <AllowIPNPayment>false</AllowIPNPayment>
      <AllowOnlinePayment>false</AllowOnlinePayment>
      <AllowOnlineCreditCardPayment>true</AllowOnlineCreditCardPayment>
      <AllowOnlineACHPayment>true</AllowOnlineACHPayment>
    </Invoice>
  </QueryResponse>
</IntuitResponse>


