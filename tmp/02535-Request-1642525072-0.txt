REQUEST URI FOR SEQUENCE ID 02535
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..2K9TWWKqwZzRU2NFZYrPBA.GPiTENGOrMKD3BQVeFNqwPwYFF2-j0Ymq5AiTr6rPZmdgBUjFF0DUN8KbFz_WhrXQlRtck0HTcJmM2NqmeCiydgbEwhh-tYaoaFb4foN3R36DVwNczUoZW7bVDlEst2HEvTDULMWnhSIYiUKE0GQPGYceOJ7jalBHQLtIQjNsa5hWC6eYY4u_Ki6-6tih24ktwpXTmu6MCGfZb8Haa2a2Y1thX1_1qtaTPcIGk8qj41FuA1gmbqFz2CLq9jh1scR4ZcP_oMk5PfuYMWu9rU9SYAjKY2ZTdG6Bnc6PBO4otRwNQtobur2xRxR_Rl03Si4MWVfNpjHtqg71Uvabrycst5nOotGKOHrT7vl4H6-t4mWgk0MssihNL9pXWl_TjdLgOoLiUyXOl7t-tEQ12I2CJk-hPjfXfqb4IGZkLJ2I-FA0plZV1WRoTpfNwRdgpQ7LSJZn7ItQvzw-WZSjhR1IYdLpc4r59M-mDZHW0raYKtWNXwoP6HlPzVeS7a3xyQUlW9XXQCEPhBq9V3QUflt5QOkoLm3g1SVxrrFGRc7qT_C9xWOoeH7k1aKAOeQkw73yGy1FrGXahe3gLjctw6abUxrOyW8SEGw7vxiqPK0_F8BOGuXlOEciw1pQrYhPxawcaPEd_-83quTjZEfP30LGc6gde1xMmWeYVuCe5jOcFeftbmcUwUuVZxNlsdS7qGh_jxvCGKXKMtT9Ie1O8yUOcy1TS07LGW54q14ysOoREI.7FgOFDIvbZZ5CtWezTcx5g
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 2705

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>12200 NW 25th St Miami FLORIDA</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>178_1642525071</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - Madison Construction Group InC</ns0:Description>
    <ns0:Amount>45.00</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>45.00</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Rush Fee</ns0:Description>
    <ns0:Amount>8.50</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Rush Hour Charges">33</ns0:ItemRef>
      <ns0:UnitPrice>8.50</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Firm Mail</ns0:Description>
    <ns0:Amount>3.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Firm Mail">18</ns0:ItemRef>
      <ns0:UnitPrice>3.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail with Electronic Return Receipt</ns0:Description>
    <ns0:Amount>7.22</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Electronic Return Receipt">15</ns0:ItemRef>
      <ns0:UnitPrice>7.22</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>ED EX  775779697238</ns0:Description>
    <ns0:Amount>50</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Manual">19</ns0:ItemRef>
      <ns0:UnitPrice>50</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>237</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


