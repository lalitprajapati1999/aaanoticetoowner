RESPONSE URI FOR SEQUENCE ID 04632
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/query?minorversion=47

RESPONSE HEADERS
================
date: Mon, 07 Mar 2022 16:46:00 GMT
content-type: application/xml;charset=UTF-8
content-length: 3208
intuit_tid: 1-622636c7-6819039f5810ecb812acdc6d
x-spanid: 1683d458-5f9a-408a-85fe-6e0e7e9e0ede
x-amzn-trace-id: Root=1-622636c7-6819039f5810ecb812acdc6d
x-content-type-options: nosniff
x-xss-protection: 1; mode=block
qbo-version: 1975.230
service-time: total=197, db=74
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private
x-envoy-upstream-service-time: 237
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-03-07T08:45:59.943-08:00">
  <QueryResponse startPosition="1" maxResults="1" totalCount="1">
    <Invoice domain="QBO" sparse="false">
      <Id>339</Id>
      <SyncToken>1</SyncToken>
      <MetaData>
        <CreateTime>2022-02-08T04:03:14-08:00</CreateTime>
        <LastUpdatedTime>2022-03-07T08:42:43-08:00</LastUpdatedTime>
      </MetaData>
      <CustomField>
        <DefinitionId>2</DefinitionId>
        <Type>StringType</Type>
        <StringValue>Notice to Owner/Preliminary No</StringValue>
      </CustomField>
      <CustomField>
        <DefinitionId>3</DefinitionId>
        <Type>StringType</Type>
        <StringValue>300 NE 2 AVE &#13;
MIAMI FL 33132</StringValue>
      </CustomField>
      <DocNumber>233_1644321794</DocNumber>
      <TxnDate>2022-02-08</TxnDate>
      <CurrencyRef name="United States Dollar">USD</CurrencyRef>
      <Line>
        <Id>1</Id>
        <LineNum>1</LineNum>
        <Description>Contracted By - PMB CONSTRUCTION INC</Description>
        <Amount>20.00</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="SERVICES:NTO">32</ItemRef>
          <UnitPrice>20</UnitPrice>
          <Qty>1</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>TAX</TaxCodeRef>
          <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Amount>20.00</Amount>
        <DetailType>SubTotalLineDetail</DetailType>
        <SubTotalLineDetail/>
      </Line>
      <TxnTaxDetail>
        <TxnTaxCodeRef>3</TxnTaxCodeRef>
        <TotalTax>0</TotalTax>
        <TaxLine>
          <Amount>0</Amount>
          <DetailType>TaxLineDetail</DetailType>
          <TaxLineDetail>
            <TaxRateRef>3</TaxRateRef>
            <PercentBased>true</PercentBased>
            <TaxPercent>6</TaxPercent>
            <NetAmountTaxable>0</NetAmountTaxable>
          </TaxLineDetail>
        </TaxLine>
        <TaxLine>
          <Amount>0</Amount>
          <DetailType>TaxLineDetail</DetailType>
          <TaxLineDetail>
            <TaxRateRef>4</TaxRateRef>
            <PercentBased>true</PercentBased>
            <TaxPercent>1</TaxPercent>
            <NetAmountTaxable>0</NetAmountTaxable>
          </TaxLineDetail>
        </TaxLine>
      </TxnTaxDetail>
      <CustomerRef name="ERNESTO HERNANDEZ">195</CustomerRef>
      <BillAddr>
        <Id>196</Id>
        <Line1>6945 NW 53 TERRACE</Line1>
        <City>MIAMI</City>
        <CountrySubDivisionCode>FLOIRDA</CountrySubDivisionCode>
        <PostalCode>33166</PostalCode>
      </BillAddr>
      <ShipAddr>
        <Id>686</Id>
        <Line1>ERNESTO HERNANDEZ</Line1>
        <Line2>SUPERIOR MIX CORPORATION</Line2>
        <Line3>6945 NW 53 TERRACE</Line3>
        <Line4>MIAMI, FLOIRDA  33166</Line4>
      </ShipAddr>
      <FreeFormAddress>true</FreeFormAddress>
      <ShipFromAddr>
        <Id>570</Id>
        <Line1>PO Box 22821</Line1>
        <Line2>Hialeah, FL  33002 US</Line2>
      </ShipFromAddr>
      <SalesTermRef name="Net 15">2</SalesTermRef>
      <DueDate>2022-02-23</DueDate>
      <TotalAmt>20.00</TotalAmt>
      <ApplyTaxAfterDiscount>false</ApplyTaxAfterDiscount>
      <PrintStatus>NeedToPrint</PrintStatus>
      <EmailStatus>EmailSent</EmailStatus>
      <BillEmail>
        <Address>ernesto@superiormix.net</Address>
      </BillEmail>
      <Balance>20.00</Balance>
      <DeliveryInfo>
        <DeliveryType>Email</DeliveryType>
        <DeliveryTime>2022-03-07T08:42:41-08:00</DeliveryTime>
      </DeliveryInfo>
      <TaxExemptionRef name="service">99</TaxExemptionRef>
      <AllowIPNPayment>false</AllowIPNPayment>
      <AllowOnlinePayment>false</AllowOnlinePayment>
      <AllowOnlineCreditCardPayment>false</AllowOnlineCreditCardPayment>
      <AllowOnlineACHPayment>false</AllowOnlineACHPayment>
      <EInvoiceStatus>Sent</EInvoiceStatus>
    </Invoice>
  </QueryResponse>
</IntuitResponse>


