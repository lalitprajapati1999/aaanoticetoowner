RESPONSE URI FOR SEQUENCE ID 12087
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

RESPONSE HEADERS
================
date: Tue, 26 Jul 2022 20:07:22 GMT
content-type: application/xml
content-length: 509
intuit_tid: 1-62e0497a-2ebf0ff7793ec4c47e017233
x-spanid: 39de3f84-85bd-417b-b52f-9f5cb3eec68e
x-amzn-trace-id: Root=1-62e0497a-2ebf0ff7793ec4c47e017233
x-content-type-options: nosniff
x-envoy-upstream-service-time: 387
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-07-26T13:07:22.098-07:00">
  <Fault type="ValidationFault">
    <Error code="6000" element="">
      <Message>A business validation error has occurred while processing your request</Message>
      <Detail>Business Validation Error: You cannot enter more than 31 characters in the sales_custom_1_val field. You tried entering CICC 7040-..., which is 40 characters.</Detail>
    </Error>
  </Fault>
</IntuitResponse>


