RESPONSE URI FOR SEQUENCE ID 05958
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/customer?minorversion=47

RESPONSE HEADERS
================
date: Fri, 18 Mar 2022 13:22:22 GMT
content-type: application/xml;charset=UTF-8
content-length: 1491
intuit_tid: 1-6234878d-600cc7276480b8186687d26a
x-spanid: 73ccd6cd-fec6-4dc6-a30d-27e79ffa1b5c
x-amzn-trace-id: Root=1-6234878d-600cc7276480b8186687d26a
x-content-type-options: nosniff
x-xss-protection: 1; mode=block
qbo-version: 1976.152
service-time: total=319, db=68
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private
x-envoy-upstream-service-time: 356
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-03-18T06:22:22.003-07:00">
  <Customer domain="QBO" sparse="false">
    <Id>271</Id>
    <SyncToken>0</SyncToken>
    <MetaData>
      <CreateTime>2022-03-18T06:22:22-07:00</CreateTime>
      <LastUpdatedTime>2022-03-18T06:22:22-07:00</LastUpdatedTime>
    </MetaData>
    <FullyQualifiedName>Alfaro Plumbing Contractor_1647609740</FullyQualifiedName>
    <CompanyName>Alfaro Plumbing Contractor</CompanyName>
    <DisplayName>Alfaro Plumbing Contractor_1647609740</DisplayName>
    <PrintOnCheckName>Alfaro Plumbing Contractor</PrintOnCheckName>
    <Active>true</Active>
    <PrimaryPhone>
      <FreeFormNumber>(786) 546-6487</FreeFormNumber>
    </PrimaryPhone>
    <PrimaryEmailAddr>
      <Address>info@alfaroplumbing.com</Address>
    </PrimaryEmailAddr>
    <DefaultTaxCodeRef>3</DefaultTaxCodeRef>
    <Taxable>true</Taxable>
    <BillAddr>
      <Id>842</Id>
      <Line1>1751 W 30th PL Unit 1007A</Line1>
      <City>Hialeah</City>
      <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
      <PostalCode>33012</PostalCode>
    </BillAddr>
    <ShipAddr>
      <Id>843</Id>
      <Line1>1751 W 30th PL Unit 1007A</Line1>
      <City>Hialeah</City>
      <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
      <PostalCode>33012</PostalCode>
    </ShipAddr>
    <Job>false</Job>
    <BillWithParent>false</BillWithParent>
    <Balance>0</Balance>
    <BalanceWithJobs>0</BalanceWithJobs>
    <CurrencyRef name="United States Dollar">USD</CurrencyRef>
    <PreferredDeliveryMethod>None</PreferredDeliveryMethod>
    <IsProject>false</IsProject>
  </Customer>
</IntuitResponse>


