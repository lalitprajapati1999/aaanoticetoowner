RESPONSE URI FOR SEQUENCE ID 09335
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

RESPONSE HEADERS
================
date: Wed, 18 May 2022 17:00:35 GMT
content-type: application/xml
content-length: 509
intuit_tid: 1-62852632-5591efa25fbd553016abf70e
x-spanid: 9b5aea79-7759-4734-96f7-cee7ad64b538
x-amzn-trace-id: Root=1-62852632-5591efa25fbd553016abf70e
x-content-type-options: nosniff
x-envoy-upstream-service-time: 456
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-05-18T10:00:34.869-07:00">
  <Fault type="ValidationFault">
    <Error code="6000" element="">
      <Message>A business validation error has occurred while processing your request</Message>
      <Detail>Business Validation Error: You cannot enter more than 31 characters in the sales_custom_1_val field. You tried entering CICC 7040-..., which is 40 characters.</Detail>
    </Error>
  </Fault>
</IntuitResponse>


