RESPONSE URI FOR SEQUENCE ID 00068
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/query?minorversion=47

RESPONSE HEADERS
================
date: Tue, 11 May 2021 08:18:18 GMT
content-type: application/xml;charset=UTF-8
content-length: 3425
server: nginx
strict-transport-security: max-age=15552000
intuit_tid: 1-609a3dca-247a892b1319d5fc3df4c140
x-spanid: ed4b669b-3b85-47e4-9d32-4e81cc1a3635
x-amzn-trace-id: Root=1-609a3dca-247a892b1319d5fc3df4c140
qbo-version: 1957.141
service-time: total=73, db=26
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2021-05-11T01:18:18.324-07:00">
  <QueryResponse startPosition="1" maxResults="1" totalCount="1">
    <Invoice domain="QBO" sparse="false">
      <Id>1</Id>
      <SyncToken>1</SyncToken>
      <MetaData>
        <CreateTime>2021-03-11T04:09:14-08:00</CreateTime>
        <LastUpdatedTime>2021-05-11T00:55:11-07:00</LastUpdatedTime>
      </MetaData>
      <CustomField>
        <DefinitionId>2</DefinitionId>
        <Type>StringType</Type>
        <StringValue>Notice to Owner/Preliminary No</StringValue>
      </CustomField>
      <CustomField>
        <DefinitionId>3</DefinitionId>
        <Type>StringType</Type>
        <StringValue>Lake Buena Vista 12529 State R</StringValue>
      </CustomField>
      <DocNumber>1615464552_7_3713</DocNumber>
      <TxnDate>2021-03-11</TxnDate>
      <CurrencyRef name="United States Dollar">USD</CurrencyRef>
      <Line>
        <Id>1</Id>
        <LineNum>1</LineNum>
        <Description>Contracted By - Bhushan Mehta</Description>
        <Amount>30.00</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="SERVICES:NTO">32</ItemRef>
          <UnitPrice>30</UnitPrice>
          <Qty>1</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>TAX</TaxCodeRef>
          <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Id>2</Id>
        <LineNum>2</LineNum>
        <Description>Certified Mail with Return Receipt Requested</Description>
        <Amount>6.96</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="MAILING:Certified Mail RR">16</ItemRef>
          <UnitPrice>6.96</UnitPrice>
          <Qty>1</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>TAX</TaxCodeRef>
          <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Amount>36.96</Amount>
        <DetailType>SubTotalLineDetail</DetailType>
        <SubTotalLineDetail/>
      </Line>
      <TxnTaxDetail>
        <TxnTaxCodeRef>3</TxnTaxCodeRef>
        <TotalTax>2.59</TotalTax>
        <TaxLine>
          <Amount>2.22</Amount>
          <DetailType>TaxLineDetail</DetailType>
          <TaxLineDetail>
            <TaxRateRef>3</TaxRateRef>
            <PercentBased>true</PercentBased>
            <TaxPercent>6</TaxPercent>
            <NetAmountTaxable>36.96</NetAmountTaxable>
          </TaxLineDetail>
        </TaxLine>
        <TaxLine>
          <Amount>0.37</Amount>
          <DetailType>TaxLineDetail</DetailType>
          <TaxLineDetail>
            <TaxRateRef>4</TaxRateRef>
            <PercentBased>true</PercentBased>
            <TaxPercent>1</TaxPercent>
            <NetAmountTaxable>36.96</NetAmountTaxable>
          </TaxLineDetail>
        </TaxLine>
      </TxnTaxDetail>
      <CustomerRef name="Neosoft_1612276794">211</CustomerRef>
      <BillAddr>
        <Id>213</Id>
        <Line1>harshad.khiste@neosoftmail.com</Line1>
        <CountrySubDivisionCode>Maharashtra</CountrySubDivisionCode>
        <PostalCode>33112</PostalCode>
      </BillAddr>
      <ShipAddr>
        <Id>214</Id>
        <Line1>harshad.khiste@neosoftmail.com</Line1>
        <CountrySubDivisionCode>Maharashtra</CountrySubDivisionCode>
        <PostalCode>33112</PostalCode>
      </ShipAddr>
      <FreeFormAddress>true</FreeFormAddress>
      <ShipFromAddr>
        <Id>217</Id>
        <Line1>186 Westward Dr</Line1>
        <Line2>Miami Springs, FL  33166 US</Line2>
      </ShipFromAddr>
      <SalesTermRef name="Net 15">2</SalesTermRef>
      <DueDate>2021-03-26</DueDate>
      <TotalAmt>39.55</TotalAmt>
      <ApplyTaxAfterDiscount>false</ApplyTaxAfterDiscount>
      <PrintStatus>NeedToPrint</PrintStatus>
      <EmailStatus>NotSet</EmailStatus>
      <Balance>39.55</Balance>
      <TaxExemptionRef/>
      <AllowIPNPayment>false</AllowIPNPayment>
      <AllowOnlinePayment>false</AllowOnlinePayment>
      <AllowOnlineCreditCardPayment>false</AllowOnlineCreditCardPayment>
      <AllowOnlineACHPayment>false</AllowOnlineACHPayment>
    </Invoice>
  </QueryResponse>
</IntuitResponse>


