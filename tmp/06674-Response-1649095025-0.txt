RESPONSE URI FOR SEQUENCE ID 06674
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/query?minorversion=47

RESPONSE HEADERS
================
date: Mon, 04 Apr 2022 17:57:05 GMT
content-type: application/xml;charset=UTF-8
content-length: 3485
intuit_tid: 1-624b3171-285bbc19318561fa3e322f17
x-spanid: b02f4846-51f2-4d96-9ff2-445972dd3558
x-amzn-trace-id: Root=1-624b3171-285bbc19318561fa3e322f17
x-content-type-options: nosniff
x-xss-protection: 1; mode=block
qbo-version: 1977.183
service-time: total=149, db=30
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private
x-envoy-upstream-service-time: 192
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-04-04T10:57:05.582-07:00">
  <QueryResponse startPosition="1" maxResults="1" totalCount="1">
    <Invoice domain="QBO" sparse="false">
      <Id>481</Id>
      <SyncToken>3</SyncToken>
      <MetaData>
        <CreateTime>2022-03-07T10:23:27-08:00</CreateTime>
        <LastUpdatedTime>2022-04-04T10:54:04-07:00</LastUpdatedTime>
      </MetaData>
      <DocNumber>1007</DocNumber>
      <TxnDate>2022-03-07</TxnDate>
      <CurrencyRef name="United States Dollar">USD</CurrencyRef>
      <PrivateNote>Voided</PrivateNote>
      <Line>
        <Id>1</Id>
        <LineNum>1</LineNum>
        <Description>Notice to Owner-158/18575 S DIXIE HWY</Description>
        <Amount>0</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="SERVICES:NTO">32</ItemRef>
          <Qty>0</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>NON</TaxCodeRef>
          <TaxClassificationRef>EUC-99990201-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Id>2</Id>
        <LineNum>2</LineNum>
        <Description>Certified Mail ERR</Description>
        <Amount>0</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="MAILING:Certified Mail ESRR">15</ItemRef>
          <Qty>0</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>NON</TaxCodeRef>
          <TaxClassificationRef>EUC-99990201-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Id>3</Id>
        <LineNum>3</LineNum>
        <Amount>0</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="MAILING:Firm Mail">18</ItemRef>
          <Qty>0</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>NON</TaxCodeRef>
          <TaxClassificationRef>EUC-99990201-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Amount>0</Amount>
        <DetailType>SubTotalLineDetail</DetailType>
        <SubTotalLineDetail/>
      </Line>
      <TxnTaxDetail>
        <TxnTaxCodeRef>3</TxnTaxCodeRef>
        <TotalTax>0</TotalTax>
        <TaxLine>
          <Amount>0</Amount>
          <DetailType>TaxLineDetail</DetailType>
          <TaxLineDetail>
            <TaxRateRef>3</TaxRateRef>
            <PercentBased>true</PercentBased>
            <TaxPercent>6</TaxPercent>
            <NetAmountTaxable>0</NetAmountTaxable>
          </TaxLineDetail>
        </TaxLine>
        <TaxLine>
          <Amount>0</Amount>
          <DetailType>TaxLineDetail</DetailType>
          <TaxLineDetail>
            <TaxRateRef>4</TaxRateRef>
            <PercentBased>true</PercentBased>
            <TaxPercent>1</TaxPercent>
            <NetAmountTaxable>0</NetAmountTaxable>
          </TaxLineDetail>
        </TaxLine>
      </TxnTaxDetail>
      <CustomerRef name="GABRIEL  CARDENAS">98</CustomerRef>
      <BillAddr>
        <Id>99</Id>
        <Line1>2315 WEST 2ND AVENUE</Line1>
        <City>HIALEAH</City>
        <Country>FLORIDA</Country>
        <PostalCode>33010</PostalCode>
      </BillAddr>
      <ShipAddr>
        <Id>780</Id>
        <Line1>GABRIEL  CARDENAS</Line1>
        <Line2>GRAND PLUMBING CORPORATION</Line2>
        <Line3>2315 WEST 2ND AVENUE</Line3>
        <Line4>HIALEAH  33010 FLORIDA</Line4>
      </ShipAddr>
      <FreeFormAddress>true</FreeFormAddress>
      <ShipFromAddr>
        <Id>779</Id>
        <Line1>PO Box 22821, Hialeah, FL, 33002, USA</Line1>
      </ShipFromAddr>
      <SalesTermRef name="Net 30">3</SalesTermRef>
      <DueDate>2022-04-06</DueDate>
      <TotalAmt>0</TotalAmt>
      <ApplyTaxAfterDiscount>false</ApplyTaxAfterDiscount>
      <PrintStatus>NotSet</PrintStatus>
      <EmailStatus>NotSet</EmailStatus>
      <BillEmail>
        <Address>gabrielac@grandplumbing.net</Address>
      </BillEmail>
      <Balance>0</Balance>
      <TaxExemptionRef name="DOC SERVICE">99</TaxExemptionRef>
      <AllowIPNPayment>false</AllowIPNPayment>
      <AllowOnlinePayment>false</AllowOnlinePayment>
      <AllowOnlineCreditCardPayment>true</AllowOnlineCreditCardPayment>
      <AllowOnlineACHPayment>true</AllowOnlineACHPayment>
    </Invoice>
  </QueryResponse>
</IntuitResponse>


