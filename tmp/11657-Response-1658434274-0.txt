RESPONSE URI FOR SEQUENCE ID 11657
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

RESPONSE HEADERS
================
date: Thu, 21 Jul 2022 20:11:14 GMT
content-type: application/xml
content-length: 509
intuit_tid: 1-62d9b2e2-0b2e8f63664b28b83ad0f592
x-spanid: 3f43991f-8b47-4ea1-ac31-8be1924201f6
x-amzn-trace-id: Root=1-62d9b2e2-0b2e8f63664b28b83ad0f592
x-content-type-options: nosniff
x-envoy-upstream-service-time: 399
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-07-21T13:11:14.294-07:00">
  <Fault type="ValidationFault">
    <Error code="6000" element="">
      <Message>A business validation error has occurred while processing your request</Message>
      <Detail>Business Validation Error: You cannot enter more than 31 characters in the sales_custom_1_val field. You tried entering CICC 7040-..., which is 40 characters.</Detail>
    </Error>
  </Fault>
</IntuitResponse>


