RESPONSE URI FOR SEQUENCE ID 10335
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

RESPONSE HEADERS
================
date: Mon, 13 Jun 2022 16:55:15 GMT
content-type: application/xml;charset=UTF-8
content-length: 4746
intuit_tid: 1-62a76bf2-503dcb534f6df0146a94d5c6
x-spanid: 003d0135-8426-4963-96a7-b444a21fec29
x-amzn-trace-id: Root=1-62a76bf2-503dcb534f6df0146a94d5c6
x-content-type-options: nosniff
x-xss-protection: 1; mode=block
qbo-version: 1982.209
service-time: total=947, db=565
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private
x-envoy-upstream-service-time: 981
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-06-13T09:55:14.670-07:00">
  <Invoice domain="QBO" sparse="false">
    <Id>1818</Id>
    <SyncToken>0</SyncToken>
    <MetaData>
      <CreateTime>2022-06-13T09:55:15-07:00</CreateTime>
      <LastUpdatedTime>2022-06-13T09:55:15-07:00</LastUpdatedTime>
    </MetaData>
    <CustomField>
      <DefinitionId>2</DefinitionId>
      <Name>sales2</Name>
      <Type>StringType</Type>
      <StringValue>Notice to Owner/Preliminary No</StringValue>
    </CustomField>
    <CustomField>
      <DefinitionId>3</DefinitionId>
      <Name>sales3</Name>
      <Type>StringType</Type>
      <StringValue>8565 W 44TH AVE HIALEAH, FL 33</StringValue>
    </CustomField>
    <DocNumber>697_1655139314</DocNumber>
    <TxnDate>2022-06-13</TxnDate>
    <CurrencyRef name="United States Dollar">USD</CurrencyRef>
    <Line>
      <Id>1</Id>
      <LineNum>1</LineNum>
      <Description>Contracted By - RALPH MERRITT CONSTRUCTION CORP</Description>
      <Amount>45.00</Amount>
      <DetailType>SalesItemLineDetail</DetailType>
      <SalesItemLineDetail>
        <ItemRef name="SERVICES:NTO">32</ItemRef>
        <UnitPrice>45</UnitPrice>
        <Qty>1</Qty>
        <ItemAccountRef name="Services">1</ItemAccountRef>
        <TaxCodeRef>NON</TaxCodeRef>
        <TaxClassificationRef>EUC-99990201-V1-00020000</TaxClassificationRef>
      </SalesItemLineDetail>
    </Line>
    <Line>
      <Id>2</Id>
      <LineNum>2</LineNum>
      <Description>Additional address charges</Description>
      <Amount>225.00</Amount>
      <DetailType>SalesItemLineDetail</DetailType>
      <SalesItemLineDetail>
        <ItemRef name="SERVICES:NTO- Additional Address">34</ItemRef>
        <UnitPrice>15</UnitPrice>
        <Qty>15</Qty>
        <ItemAccountRef name="Services">1</ItemAccountRef>
        <TaxCodeRef>NON</TaxCodeRef>
        <TaxClassificationRef>EUC-99990201-V1-00020000</TaxClassificationRef>
      </SalesItemLineDetail>
    </Line>
    <Line>
      <Id>3</Id>
      <LineNum>3</LineNum>
      <Description>Firm Mail</Description>
      <Amount>3.95</Amount>
      <DetailType>SalesItemLineDetail</DetailType>
      <SalesItemLineDetail>
        <ItemRef name="MAILING:Firm Mail">18</ItemRef>
        <UnitPrice>3.95</UnitPrice>
        <Qty>1</Qty>
        <ItemAccountRef name="Services">1</ItemAccountRef>
        <TaxCodeRef>NON</TaxCodeRef>
        <TaxClassificationRef>EUC-99990201-V1-00020000</TaxClassificationRef>
      </SalesItemLineDetail>
    </Line>
    <Line>
      <Id>4</Id>
      <LineNum>4</LineNum>
      <Description>Certified Mail</Description>
      <Amount>5.42</Amount>
      <DetailType>SalesItemLineDetail</DetailType>
      <SalesItemLineDetail>
        <ItemRef name="MAILING:Certified Mail USPS">17</ItemRef>
        <UnitPrice>5.42</UnitPrice>
        <Qty>1</Qty>
        <ItemAccountRef name="Services">1</ItemAccountRef>
        <TaxCodeRef>NON</TaxCodeRef>
        <TaxClassificationRef>EUC-99990201-V1-00020000</TaxClassificationRef>
      </SalesItemLineDetail>
    </Line>
    <Line>
      <Id>5</Id>
      <LineNum>5</LineNum>
      <Description>Certified Mail with Electronic Return Receipt</Description>
      <Amount>14.44</Amount>
      <DetailType>SalesItemLineDetail</DetailType>
      <SalesItemLineDetail>
        <ItemRef name="MAILING:Certified Mail ESRR">15</ItemRef>
        <UnitPrice>7.22</UnitPrice>
        <Qty>2</Qty>
        <ItemAccountRef name="Services">1</ItemAccountRef>
        <TaxCodeRef>NON</TaxCodeRef>
        <TaxClassificationRef>EUC-99990201-V1-00020000</TaxClassificationRef>
      </SalesItemLineDetail>
    </Line>
    <Line>
      <Amount>293.81</Amount>
      <DetailType>SubTotalLineDetail</DetailType>
      <SubTotalLineDetail/>
    </Line>
    <TxnTaxDetail>
      <TxnTaxCodeRef>3</TxnTaxCodeRef>
      <TotalTax>0</TotalTax>
      <TaxLine>
        <Amount>0</Amount>
        <DetailType>TaxLineDetail</DetailType>
        <TaxLineDetail>
          <TaxRateRef>3</TaxRateRef>
          <PercentBased>true</PercentBased>
          <TaxPercent>6</TaxPercent>
          <NetAmountTaxable>0</NetAmountTaxable>
        </TaxLineDetail>
      </TaxLine>
      <TaxLine>
        <Amount>0</Amount>
        <DetailType>TaxLineDetail</DetailType>
        <TaxLineDetail>
          <TaxRateRef>4</TaxRateRef>
          <PercentBased>true</PercentBased>
          <TaxPercent>1</TaxPercent>
          <NetAmountTaxable>0</NetAmountTaxable>
        </TaxLineDetail>
      </TaxLine>
    </TxnTaxDetail>
    <CustomerRef name="PROFESSIONAL FINISH TILT CORPORATION_1638818204">224</CustomerRef>
    <BillAddr>
      <Id>423</Id>
      <Line1>PO BOX 473035</Line1>
      <City>Miami</City>
      <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
      <PostalCode>33147</PostalCode>
    </BillAddr>
    <ShipAddr>
      <Id>424</Id>
      <Line1>PO BOX 473035</Line1>
      <City>Miami</City>
      <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
      <PostalCode>33147</PostalCode>
    </ShipAddr>
    <FreeFormAddress>true</FreeFormAddress>
    <ShipFromAddr>
      <Id>1331</Id>
      <Line1>PO Box 22821</Line1>
      <Line2>Hialeah, FL  33002 US</Line2>
    </ShipFromAddr>
    <SalesTermRef name="Net 15">2</SalesTermRef>
    <DueDate>2022-06-28</DueDate>
    <TotalAmt>293.81</TotalAmt>
    <ApplyTaxAfterDiscount>false</ApplyTaxAfterDiscount>
    <PrintStatus>NeedToPrint</PrintStatus>
    <EmailStatus>NotSet</EmailStatus>
    <Balance>293.81</Balance>
    <TaxExemptionRef name="service">99</TaxExemptionRef>
    <AllowIPNPayment>false</AllowIPNPayment>
    <AllowOnlinePayment>false</AllowOnlinePayment>
    <AllowOnlineCreditCardPayment>false</AllowOnlineCreditCardPayment>
    <AllowOnlineACHPayment>false</AllowOnlineACHPayment>
  </Invoice>
</IntuitResponse>


