REQUEST URI FOR SEQUENCE ID 06347
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..4rqMu7fvS7HfSOmB9LTBvQ.-Eg1E4jRqmfUYZ5nUK1rXCbG6qAl8TEjy3nLKqNUTCQaWSOo4wocRV2MUJPJUsOH4qY2oinN3OeiqoTr6tt0Y5oXV9z9SNJVyeA6HIkZrEPZRm3Te0HXDRzTn-IawbaVseLbY3ex83DBKb4XQBwTAFUzt4xxEGcGrCf3HFLKyvQGRUHi3KniaKyY-6GomyK75zR5WJF76Y1FOoZvNysotU60SySf2etsXVIOF3RwR83m_RzWDWjI7CSrQnG_nRrm53Llbh75hCmTZi1wBfBhml21LcC3bZvfm2vdKAFyDcD7vKflMXN7t0kFivP-ls59BzZvaoerhYPxk85fB9f2OVt54y90UbBx84BYVUxLfab-Yu-q4W_K22oys9vcQ5jl8X8SfPiRtrWriBpLQcrQ2cEdEZ0MC5277Z9F8OqLm62QSs77cbSP567M1dPNzTkoLLnDEGqX5WKB9XO8CI5zmOHt2wiPkx6xRMFHC-86z10mkNqu1_MOJN-X4WZGuBUGtO1r_PEyZKrOfNT42p2bg2kkNgQKKYIbBmRw68AdNGtudH6swwAJrl5m0xTvTtP_jMenrqQrxHxOmA5gSdaNpdytynnJORQ8_7WDxWVzIuMkbi2TmZkH55tj210jjWIPOTuU-queNa9q5gxUvLk1sDFeRGDD6DbC_xjTQ1KA02dO9SjsykNAipJ0t-qswNqEUwpkZDGilkDMu0ZwvnebnhfSXjl1VnyxTnJKENllHN8.3Qlna8IfAd1KltjuOCe-Sg
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 2778

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>NTO 342- correction  recipient: G7 Holding LLC</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>5860 N BAY ROAD MIAMI BEACH 33</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>380_1648486528</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - POWER PLUMBING</ns0:Description>
    <ns0:Amount>35</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>35</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Rush Fee</ns0:Description>
    <ns0:Amount>12.50</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Rush Hour Charges">33</ns0:ItemRef>
      <ns0:UnitPrice>12.50</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Firm Mail</ns0:Description>
    <ns0:Amount>3.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Firm Mail">18</ns0:ItemRef>
      <ns0:UnitPrice>3.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail</ns0:Description>
    <ns0:Amount>16.26</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Certified Mail">17</ns0:ItemRef>
      <ns0:UnitPrice>5.42</ns0:UnitPrice>
      <ns0:Qty>3</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail with Electronic Return Receipt</ns0:Description>
    <ns0:Amount>7.22</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Electronic Return Receipt">15</ns0:ItemRef>
      <ns0:UnitPrice>7.22</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>36</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


