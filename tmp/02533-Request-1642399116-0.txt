REQUEST URI FOR SEQUENCE ID 02533
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/customer?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..NMBWw_Zo0Klqkk8jiPz6vg.SjIxAskYvgX_pgfsdqXMa_Nlqey10SYrF2OeLPGQxT_4NVxAmkeH8wykTmK67aMNyZZyqB-fwQeiPc6XJsOF2AhQ-6BdagEc3PlgwdsPAhauvPm6bZepWNDMi23K-md0OmmFaAGhDnZwxck1B1_eaCfaslI_BkHnySMLZ8k5A3NZKcNZtABeGR-ZJSsfvLWPml2spInbuv-w3cBLiLlhK7K9F3yNg_4AP_zVivHslxGI__YGSHlt28XtdLgDLsbl_1wRIe9Up6jWeZPPGoAI5w25xoY1Ci08F_dTZfKWfMXR3AglunGeKwR-_Q2pgTGkDw6UNyfIZcCXDDYVw7aEWsKWmUY_MHFy0DM0fhUYWa302fwkJA8wzGuMdznhUqyglEXOQzPYoFyMEOGZZ4LUMQY0XY4_mYTwy8qnV_DwtWDf-iaSEYmEXXWP-8YvlCpvk7e4tEsrXdN9xyI_aHh8zS1EZhQBTsiOx6MNEilAbldI_ojjaEfmSPVdtPOQt2EvJJvta5TEXZT5TiJpTvjmEU-RQit5taMTPl4ipLw1NU5l8-Ci8ao_ALsX6ROV4lb6plf1vXf33zRUUHL8798bTNcLD6fE7fSCKO93uF-90-mLm6RrJqtNJDQ5JTjA7CchHFU6ULW2ThsVwsv6xmv1MTK92b7TAzPDgrONIXyEISfWjoY4HG1Js_tG9htsPY03XNKNhG8xua3K-TczGkV0NEPEdmE7y4C3lyzY2LDvUDA.FmAv0E7z2Ue-7Xj72BiiBw
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 905

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Customer xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:FullyQualifiedName>Test</ns0:FullyQualifiedName>
  <ns0:CompanyName>Test</ns0:CompanyName>
  <ns0:DisplayName>Test_1642399116</ns0:DisplayName>
  <ns0:PrimaryPhone>
    <ns0:FreeFormNumber>(411) 047-6876</ns0:FreeFormNumber>
  </ns0:PrimaryPhone>
  <ns0:PrimaryEmailAddr>
    <ns0:Address>Test3011@gmail.com</ns0:Address>
  </ns0:PrimaryEmailAddr>
  <ns0:BillAddr>
    <ns0:Line1>TEst</ns0:Line1>
    <ns0:City>D''Iberville</ns0:City>
    <ns0:CountrySubDivisionCode>Alaska</ns0:CountrySubDivisionCode>
    <ns0:PostalCode>41104</ns0:PostalCode>
  </ns0:BillAddr>
  <ns0:ShipAddr>
    <ns0:Line1>TEst</ns0:Line1>
    <ns0:City>D''Iberville</ns0:City>
    <ns0:CountrySubDivisionCode>Alaska</ns0:CountrySubDivisionCode>
    <ns0:PostalCode>41104</ns0:PostalCode>
  </ns0:ShipAddr>
</ns0:Customer>


