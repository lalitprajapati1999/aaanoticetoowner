REQUEST URI FOR SEQUENCE ID 07923
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..s_ovgLm7b7K6sY6U1OLJCQ.HZ8F49jdMIpBKDlImcaN4BBgLukkOtGprOMd_ZXt1DYJvCU4S4u2TJcyaIgbDI6qfmsC3eBwLpgYUo2cx74iM0mdGQjoIEjdwUmSPp1IfexJ0YbRpRYFLUFHMUqauZjVLYxf932USEkH37huUbQgN6obajcvRrE3m7mBJ3pMfDnEbKYs79_C8uZ6DqUEiFpAZmO4h9_yZV4yV41XOkotB7DQShGez7QSUCckrIVZMLwLIE5pTqRiRxqqi09csV6y7juXzHdJ4Gyt_uibxKrDLoZdRUUlxpdLGtRAfg2vPaWDQZ5tnO7BBMVoyEHCN1pvgx3ITsq6qPlhrSv8CcRf-oP9HF--SscrxXJ8SixQ68WnyzCLGgKMsbCKkMHMsRFF1rIHEzAf3M2r5wuibfmANJ5uLcxWArxkuI1MTiJyy2-LOBtA2QzpWLc2xElFovCqvU3HKD4N9409HCFjKHxgwfNk5NHK5xaP0KWUVFKXmLbJo2mn7kVs8W6DHBkHuC3zNIM8MIxf5miSHaiKvtpcf5WE6UJ-KZOW4-TD0XzL9oE9hJPGMDkf04R7wEfMUwiNYJr-OsDUuCRa0RaemYfz_v-Tp_jtLlyc0D1bykO-soGtFT4uzyXM8ifKErATffZpcMS7pYnePZr7K9svhpenWBlfFKL3gGpdEJlTCu7hgL4jBoZSvaDR7rQ7QXQOa2RtH8W0yegTXK-zHGJC8jJkGGpZSSNr5iBkm7eYWFUys_4.rlSA59E05Naultcmx3G1bA
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 2701

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>2750 NW 58 ST MIAMI FL 33142</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>464_1649954084</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - AURORA SERVICES INC.</ns0:Description>
    <ns0:Amount>20.00</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>20.00</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Rush Fee</ns0:Description>
    <ns0:Amount>8.50</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Rush Hour Charges">33</ns0:ItemRef>
      <ns0:UnitPrice>8.50</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Firm Mail</ns0:Description>
    <ns0:Amount>3.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Firm Mail">18</ns0:ItemRef>
      <ns0:UnitPrice>3.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail</ns0:Description>
    <ns0:Amount>5.42</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Certified Mail">17</ns0:ItemRef>
      <ns0:UnitPrice>5.42</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail with Electronic Return Receipt</ns0:Description>
    <ns0:Amount>14.44</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Electronic Return Receipt">15</ns0:ItemRef>
      <ns0:UnitPrice>7.22</ns0:UnitPrice>
      <ns0:Qty>2</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>195</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


