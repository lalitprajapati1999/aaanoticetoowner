REQUEST URI FOR SEQUENCE ID 10768
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..2H0xm7aJ6YGxzncDHvFtAw.R_FP7QTDDgRNYCILQOzH5mxYiIU1OdpID9-0fP30GbqLkIVAUPLltYcUU-nM5ljyo7v15O3O85L1MdeqF5J0nA8jgPphy8FXFlZeNyUrgFWr7RLRIFowUfLL_WdMNV1vMG0ExXFIY7rOYwZKdyQD2-Hk7Y-0zqZXfgZp2kbMdeKYwwr57kJ2uDj3qmR2MgbnsRHsrGyAvGrfZj5I3rbxrKjYR_HDQpfgRUoGCQFuKPmzPDIqmcZXYyedKMQAhCTWM8s39oLlFyhKTBp7PIen6Lzq5KXOJqMJayW3oEZ_Cdxy0M5ZQTmOxRh5SNyos3R-Z0rQV0TKBJn5UkxdxX1R73vLDPL-HBMZJJW5GCHX2PNuTzdL4JkGQwTQYCP8z_oAI5V6uI5keLI_Z25dTBcqNF4O9Sxz5LX3KxyXTY4jQLuNvfnbE4DEpVqBIojL3Lx4Qe05BXuT12YOMzsevfNVT3KENdYeelULyS9hSvSo8DB_WhBdYIaOXL97cmlG9ifDSufH9RcVCSm_GifIMHkBWn8ExjYw-TdncYPWxKjegYXUONIrJA8Qc5Ik80hRDzZ_Pf429kx53qVA0qPMvQLsd-Ki5y7KWwkK7cyOSmfblfvIofHpg1n3y2g7yJnbX7Zs8gaD4H_AmbF8rh4Old7M-kiZMizEbwFRWsF6TZidu6HhNOTbpX78rWk6cbXH13sp0rmED93AFZ933-QmauKwsswg72ULokN6cZdN5Jm_HnM.C2Qi5lhmev9ZHbz8YFKUFw
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 1991

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>9910 S. Jog Road Boynton Beach</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>754_1656599452</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - Ibis Building Corporation</ns0:Description>
    <ns0:Amount>45.00</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>45.00</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Firm Mail</ns0:Description>
    <ns0:Amount>3.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Firm Mail">18</ns0:ItemRef>
      <ns0:UnitPrice>3.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail with Electronic Return Receipt</ns0:Description>
    <ns0:Amount>21.66</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Electronic Return Receipt">15</ns0:ItemRef>
      <ns0:UnitPrice>7.22</ns0:UnitPrice>
      <ns0:Qty>3</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>228</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


