REQUEST URI FOR SEQUENCE ID 06529
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..znu8blV3AXrSgVjaYPIMtw.5P3tzY45gBIyzLhv3VlDJUCBkYdoMjWSBHp3ixoLoalj3n_8YccJnG6U2TVH1cQ4k1beHP_KCz4s_-vnn3qgk_4QL4Li1M1YrThw23aP8f-lLXpazVPSEWwXG8gJp0DDO21oet2vVJ9HiZkDhY-E4Ms58XgiRXex-bkxjbZJ4MiTX8VMQBoocBh13hJkoz3OnszMfvlkQM0aAr5QZ2wWG33l9XkSDqLP4FV06WaagRoSLVFWiqXwaG_y9pUiH7T2xGRjolCE4Qx3pvblRpLITUJ3UdRqNyeqCcQCZ0jN32UPyoGJxc2PQ9okx55FxEI_2dqOjX6AhTnTweVMz5Ezu1EzzZjxZ3MYpxWE2eliKMIhJvOSyexD5-aha7YbtjA5nY6X772X6N6Y1h1ehP4fN5-t1gaygM9jecNXVR0VZQoTBYDlQ-6kDP26rJhr2PGd7kj1hSogu7yXM_PiEjStTimOI89wJkGd6FqvsBNICoM4dOoi4eBcTSr9G-8k5H2M1VJ8XLSJGmGrWfw1xm2dcmfhVqdijLbm8MuqtuufpzrS0JRDfc1fpS_hGK71O9TtW_-MjkkVMkOBaTZfdEhKJeXZbNXZFcT_HnDzFzWh71cPhTljOVVPZuHaKC7TRIYcXeHUmpWsAMF2bmbS1UPQJ5rOvLnDImWUBdlv6awcFE45KiK5m-3t4hNGF9HMsQpOSqdI2cdE3B4QvguOV6UOYGGYhjZJ1lqoZowK90IErZ4.kOhPY-Y1jWq8QjAQ9OJoMg
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 2778

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>NTO 342- correction  recipient: G7 Holding LLC</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>5860 N BAY ROAD MIAMI BEACH 33</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>380_1648743240</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - POWER PLUMBING</ns0:Description>
    <ns0:Amount>35</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>35</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Rush Fee</ns0:Description>
    <ns0:Amount>12.50</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Rush Hour Charges">33</ns0:ItemRef>
      <ns0:UnitPrice>12.50</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Firm Mail</ns0:Description>
    <ns0:Amount>3.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Firm Mail">18</ns0:ItemRef>
      <ns0:UnitPrice>3.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail</ns0:Description>
    <ns0:Amount>16.26</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Certified Mail">17</ns0:ItemRef>
      <ns0:UnitPrice>5.42</ns0:UnitPrice>
      <ns0:Qty>3</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail with Electronic Return Receipt</ns0:Description>
    <ns0:Amount>7.22</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Electronic Return Receipt">15</ns0:ItemRef>
      <ns0:UnitPrice>7.22</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>36</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


