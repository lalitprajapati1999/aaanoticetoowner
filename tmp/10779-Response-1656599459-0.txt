RESPONSE URI FOR SEQUENCE ID 10779
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

RESPONSE HEADERS
================
date: Thu, 30 Jun 2022 14:30:58 GMT
content-type: application/xml;charset=UTF-8
content-length: 4284
intuit_tid: 1-62bdb3a1-4307ef864c7b6f1f0f03de97
x-spanid: 5df1e3c2-2bb7-4dbf-90fd-13eab339233f
x-amzn-trace-id: Root=1-62bdb3a1-4307ef864c7b6f1f0f03de97
x-content-type-options: nosniff
x-xss-protection: 1; mode=block
qbo-version: 1983.200
service-time: total=687, db=373
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private
x-envoy-upstream-service-time: 717
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-06-30T07:30:57.789-07:00">
  <Invoice domain="QBO" sparse="false">
    <Id>1861</Id>
    <SyncToken>0</SyncToken>
    <MetaData>
      <CreateTime>2022-06-30T07:30:58-07:00</CreateTime>
      <LastUpdatedTime>2022-06-30T07:30:58-07:00</LastUpdatedTime>
    </MetaData>
    <CustomField>
      <DefinitionId>2</DefinitionId>
      <Name>sales2</Name>
      <Type>StringType</Type>
      <StringValue>Notice to Owner/Preliminary No</StringValue>
    </CustomField>
    <CustomField>
      <DefinitionId>3</DefinitionId>
      <Name>sales3</Name>
      <Type>StringType</Type>
      <StringValue>1780 W 49TH STREET HIALEAH, FL</StringValue>
    </CustomField>
    <DocNumber>729_1656599457</DocNumber>
    <TxnDate>2022-06-30</TxnDate>
    <CurrencyRef name="United States Dollar">USD</CurrencyRef>
    <Line>
      <Id>1</Id>
      <LineNum>1</LineNum>
      <Description>Contracted By - THORNTON CONSTRUCTION COMPANY, INC.</Description>
      <Amount>45.00</Amount>
      <DetailType>SalesItemLineDetail</DetailType>
      <SalesItemLineDetail>
        <ItemRef name="SERVICES:NTO">32</ItemRef>
        <UnitPrice>45</UnitPrice>
        <Qty>1</Qty>
        <ItemAccountRef name="Services">1</ItemAccountRef>
        <TaxCodeRef>NON</TaxCodeRef>
        <TaxClassificationRef>EUC-99990201-V1-00020000</TaxClassificationRef>
      </SalesItemLineDetail>
    </Line>
    <Line>
      <Id>2</Id>
      <LineNum>2</LineNum>
      <Description>Rush Fee</Description>
      <Amount>12.50</Amount>
      <DetailType>SalesItemLineDetail</DetailType>
      <SalesItemLineDetail>
        <ItemRef name="SERVICES:Rush fee">33</ItemRef>
        <UnitPrice>12.5</UnitPrice>
        <Qty>1</Qty>
        <ItemAccountRef name="Services">1</ItemAccountRef>
        <TaxCodeRef>NON</TaxCodeRef>
        <TaxClassificationRef>EUC-99990201-V1-00020000</TaxClassificationRef>
      </SalesItemLineDetail>
    </Line>
    <Line>
      <Id>3</Id>
      <LineNum>3</LineNum>
      <Description>Firm Mail</Description>
      <Amount>3.95</Amount>
      <DetailType>SalesItemLineDetail</DetailType>
      <SalesItemLineDetail>
        <ItemRef name="MAILING:Firm Mail">18</ItemRef>
        <UnitPrice>3.95</UnitPrice>
        <Qty>1</Qty>
        <ItemAccountRef name="Services">1</ItemAccountRef>
        <TaxCodeRef>NON</TaxCodeRef>
        <TaxClassificationRef>EUC-99990201-V1-00020000</TaxClassificationRef>
      </SalesItemLineDetail>
    </Line>
    <Line>
      <Id>4</Id>
      <LineNum>4</LineNum>
      <Description>Certified Mail with Electronic Return Receipt</Description>
      <Amount>14.44</Amount>
      <DetailType>SalesItemLineDetail</DetailType>
      <SalesItemLineDetail>
        <ItemRef name="MAILING:Certified Mail ESRR">15</ItemRef>
        <UnitPrice>7.22</UnitPrice>
        <Qty>2</Qty>
        <ItemAccountRef name="Services">1</ItemAccountRef>
        <TaxCodeRef>NON</TaxCodeRef>
        <TaxClassificationRef>EUC-99990201-V1-00020000</TaxClassificationRef>
      </SalesItemLineDetail>
    </Line>
    <Line>
      <Amount>75.89</Amount>
      <DetailType>SubTotalLineDetail</DetailType>
      <SubTotalLineDetail/>
    </Line>
    <TxnTaxDetail>
      <TxnTaxCodeRef>3</TxnTaxCodeRef>
      <TotalTax>0</TotalTax>
      <TaxLine>
        <Amount>0</Amount>
        <DetailType>TaxLineDetail</DetailType>
        <TaxLineDetail>
          <TaxRateRef>3</TaxRateRef>
          <PercentBased>true</PercentBased>
          <TaxPercent>6</TaxPercent>
          <NetAmountTaxable>0</NetAmountTaxable>
        </TaxLineDetail>
      </TaxLine>
      <TaxLine>
        <Amount>0</Amount>
        <DetailType>TaxLineDetail</DetailType>
        <TaxLineDetail>
          <TaxRateRef>4</TaxRateRef>
          <PercentBased>true</PercentBased>
          <TaxPercent>1</TaxPercent>
          <NetAmountTaxable>0</NetAmountTaxable>
        </TaxLineDetail>
      </TaxLine>
    </TxnTaxDetail>
    <CustomerRef name="DADE ELECTRIC SERVICE INC._1645596425">264</CustomerRef>
    <BillAddr>
      <Id>627</Id>
      <Line1>8191 NW 91 TERR. # 9</Line1>
      <City>Miami</City>
      <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
      <PostalCode>33166</PostalCode>
    </BillAddr>
    <ShipAddr>
      <Id>628</Id>
      <Line1>8191 NW 91 TERR. # 9</Line1>
      <City>Miami</City>
      <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
      <PostalCode>33166</PostalCode>
    </ShipAddr>
    <FreeFormAddress>true</FreeFormAddress>
    <ShipFromAddr>
      <Id>1376</Id>
      <Line1>PO Box 22821</Line1>
      <Line2>Hialeah, FL  33002 US</Line2>
    </ShipFromAddr>
    <SalesTermRef name="Net 15">2</SalesTermRef>
    <DueDate>2022-07-15</DueDate>
    <TotalAmt>75.89</TotalAmt>
    <ApplyTaxAfterDiscount>false</ApplyTaxAfterDiscount>
    <PrintStatus>NeedToPrint</PrintStatus>
    <EmailStatus>NotSet</EmailStatus>
    <Balance>75.89</Balance>
    <TaxExemptionRef name="service">99</TaxExemptionRef>
    <AllowIPNPayment>false</AllowIPNPayment>
    <AllowOnlinePayment>false</AllowOnlinePayment>
    <AllowOnlineCreditCardPayment>false</AllowOnlineCreditCardPayment>
    <AllowOnlineACHPayment>false</AllowOnlineACHPayment>
  </Invoice>
</IntuitResponse>


