RESPONSE URI FOR SEQUENCE ID 08602
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/query?minorversion=47

RESPONSE HEADERS
================
date: Fri, 22 Apr 2022 19:09:18 GMT
content-type: application/xml;charset=UTF-8
content-length: 4557
intuit_tid: 1-6262fd5d-77557f08512db892241eb28e
x-spanid: 2cb27f80-9b7d-4854-8234-7e7a40536b65
x-amzn-trace-id: Root=1-6262fd5d-77557f08512db892241eb28e
x-content-type-options: nosniff
x-xss-protection: 1; mode=block
qbo-version: 1979.193
service-time: total=202, db=110
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private
x-envoy-upstream-service-time: 228
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-04-22T12:09:18.011-07:00">
  <QueryResponse startPosition="1" maxResults="1" totalCount="1">
    <Invoice domain="QBO" sparse="false">
      <Id>723</Id>
      <SyncToken>0</SyncToken>
      <MetaData>
        <CreateTime>2022-04-04T09:01:26-07:00</CreateTime>
        <LastUpdatedTime>2022-04-22T10:58:42-07:00</LastUpdatedTime>
      </MetaData>
      <CustomField>
        <DefinitionId>2</DefinitionId>
        <Type>StringType</Type>
        <StringValue>Claim of Lien</StringValue>
      </CustomField>
      <CustomField>
        <DefinitionId>3</DefinitionId>
        <Type>StringType</Type>
        <StringValue>7390 ARTHUR ST HOLLYWOOD FL 33</StringValue>
      </CustomField>
      <DocNumber>357_1649088085</DocNumber>
      <TxnDate>2022-04-04</TxnDate>
      <CurrencyRef name="United States Dollar">USD</CurrencyRef>
      <Line>
        <Id>1</Id>
        <LineNum>1</LineNum>
        <Description>Contracted By - CASTRILLON SERVICES, INC.
Parent Work Order Number - 245</Description>
        <Amount>185.00</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="CLAIM OF LIEN:Claim of Lien">38</ItemRef>
          <UnitPrice>185</UnitPrice>
          <Qty>1</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>NON</TaxCodeRef>
          <TaxClassificationRef>EUC-99990201-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Id>2</Id>
        <LineNum>2</LineNum>
        <Description>CRR-70191120000063845829-COL357</Description>
        <Amount>7.25</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="MAILING:Manual">19</ItemRef>
          <UnitPrice>7.25</UnitPrice>
          <Qty>1</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>TAX</TaxCodeRef>
          <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Id>3</Id>
        <LineNum>3</LineNum>
        <Description>CRR-70191120000063845836-COL357</Description>
        <Amount>7.25</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="MAILING:Manual">19</ItemRef>
          <UnitPrice>7.25</UnitPrice>
          <Qty>1</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>TAX</TaxCodeRef>
          <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Id>4</Id>
        <LineNum>4</LineNum>
        <Description>CRR-70191120000063845843-COL357</Description>
        <Amount>7.25</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="MAILING:Manual">19</ItemRef>
          <UnitPrice>7.25</UnitPrice>
          <Qty>1</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>TAX</TaxCodeRef>
          <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Amount>206.75</Amount>
        <DetailType>SubTotalLineDetail</DetailType>
        <SubTotalLineDetail/>
      </Line>
      <TxnTaxDetail>
        <TxnTaxCodeRef>3</TxnTaxCodeRef>
        <TotalTax>0</TotalTax>
        <TaxLine>
          <Amount>0</Amount>
          <DetailType>TaxLineDetail</DetailType>
          <TaxLineDetail>
            <TaxRateRef>3</TaxRateRef>
            <PercentBased>true</PercentBased>
            <TaxPercent>6</TaxPercent>
            <NetAmountTaxable>0</NetAmountTaxable>
          </TaxLineDetail>
        </TaxLine>
        <TaxLine>
          <Amount>0</Amount>
          <DetailType>TaxLineDetail</DetailType>
          <TaxLineDetail>
            <TaxRateRef>4</TaxRateRef>
            <PercentBased>true</PercentBased>
            <TaxPercent>1</TaxPercent>
            <NetAmountTaxable>0</NetAmountTaxable>
          </TaxLineDetail>
        </TaxLine>
      </TxnTaxDetail>
      <CustomerRef name="ERNESTO HERNANDEZ">195</CustomerRef>
      <BillAddr>
        <Id>196</Id>
        <Line1>6945 NW 53 TERRACE</Line1>
        <City>MIAMI</City>
        <CountrySubDivisionCode>FLOIRDA</CountrySubDivisionCode>
        <PostalCode>33166</PostalCode>
      </BillAddr>
      <ShipAddr>
        <Id>196</Id>
        <Line1>6945 NW 53 TERRACE</Line1>
        <City>MIAMI</City>
        <CountrySubDivisionCode>FLOIRDA</CountrySubDivisionCode>
        <PostalCode>33166</PostalCode>
      </ShipAddr>
      <FreeFormAddress>true</FreeFormAddress>
      <ShipFromAddr>
        <Id>929</Id>
        <Line1>PO Box 22821</Line1>
        <Line2>Hialeah, FL  33002 US</Line2>
      </ShipFromAddr>
      <SalesTermRef name="Net 15">2</SalesTermRef>
      <DueDate>2022-04-19</DueDate>
      <TotalAmt>206.75</TotalAmt>
      <ApplyTaxAfterDiscount>false</ApplyTaxAfterDiscount>
      <PrintStatus>NeedToPrint</PrintStatus>
      <EmailStatus>EmailSent</EmailStatus>
      <BillEmail>
        <Address>ernesto@superiormix.net</Address>
      </BillEmail>
      <Balance>206.75</Balance>
      <DeliveryInfo>
        <DeliveryType>Email</DeliveryType>
        <DeliveryTime>2022-04-22T10:26:03-07:00</DeliveryTime>
      </DeliveryInfo>
      <TaxExemptionRef name="service">99</TaxExemptionRef>
      <AllowIPNPayment>false</AllowIPNPayment>
      <AllowOnlinePayment>false</AllowOnlinePayment>
      <AllowOnlineCreditCardPayment>false</AllowOnlineCreditCardPayment>
      <AllowOnlineACHPayment>false</AllowOnlineACHPayment>
      <EInvoiceStatus>Viewed</EInvoiceStatus>
    </Invoice>
  </QueryResponse>
</IntuitResponse>


