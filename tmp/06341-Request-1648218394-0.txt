REQUEST URI FOR SEQUENCE ID 06341
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/customer?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..dBImFu0n-dWXMba887RBMw.VpEdshiBeRe988_rkZJeZ4BnG-Yn8dn16ACZrbu3dwR5XkFiu_YLIdmOXnVYvqMVDg0YNE6fWHmwKUh85o8KHcrfdN4Np33iP-h4Uox96mv8oS23SvV5v5eDcBPH_gwjcWo1ws1sIgDVqLzP2JjpwFwqDcd92lNCDcrCIIf8-tEakMBlGLY5rKwuoKyJt1OGXMDZxLyaHJUSUYaiScXYzPxiLFOJybzpYj9uMlGShu9Biy2sxMfGVTh7SdSavlcoOw_CwqhATdeXyZHW8tFUVMGID_0W1r-obkv9SpdbgyKqC8RUrHZ4ZJO5OtjYlJnQe-Q-TZDKW6bQqt_JJLSy89znchY9BwKILZUXpYvnpJaPUoMRm7UjftJzBKGxZb-oRIKczAUFhuR6opSdvXGOUunCqWsIOXb_5gv4BIDqHco2AAx7-iKZ2QY1f4fSH0dq0lHAMq4LqtzfizSWX4IJ0hPRT8qiWY0L0BopAKPQzsQR6g9JW2jRMpjfUxOShcF637ml3zbRRpvyGWH5R8cCeucLCQna4aw94jIj_qt1mjcgmvrQOhxmunW4m7jR0XADlUwO-JMxUZTKGy3XKUnsygDXohddFD6pqVDYKJ1EQ1kM-uS4Kr4eu149BmqHbWsGrMLpKnbAijazLVPSkPJuBgFSTN-6Uu61nY3xDOzOSfegwap4HPHtJ7V7j_xbjmp7UDxfMyF2LSz2_Vi2pXR5usFdFYUZwyTJSh6PMmn7YWo.g1pXFQjn8xPLbzoH_ggweA
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 978

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Customer xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:FullyQualifiedName>REYES FC SERVICE CORP</ns0:FullyQualifiedName>
  <ns0:CompanyName>REYES FC SERVICE CORP</ns0:CompanyName>
  <ns0:DisplayName>REYES FC SERVICE CORP_1648218393</ns0:DisplayName>
  <ns0:PrimaryPhone>
    <ns0:FreeFormNumber>(305) 600-9484</ns0:FreeFormNumber>
  </ns0:PrimaryPhone>
  <ns0:PrimaryEmailAddr>
    <ns0:Address>fidelcid60@yahoo.com</ns0:Address>
  </ns0:PrimaryEmailAddr>
  <ns0:BillAddr>
    <ns0:Line1>6742 NW 199TH STREET</ns0:Line1>
    <ns0:City>Miami</ns0:City>
    <ns0:CountrySubDivisionCode>Florida</ns0:CountrySubDivisionCode>
    <ns0:PostalCode>33015</ns0:PostalCode>
  </ns0:BillAddr>
  <ns0:ShipAddr>
    <ns0:Line1>6742 NW 199TH STREET</ns0:Line1>
    <ns0:City>Miami</ns0:City>
    <ns0:CountrySubDivisionCode>Florida</ns0:CountrySubDivisionCode>
    <ns0:PostalCode>33015</ns0:PostalCode>
  </ns0:ShipAddr>
</ns0:Customer>


