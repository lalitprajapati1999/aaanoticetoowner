RESPONSE URI FOR SEQUENCE ID 09050
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

RESPONSE HEADERS
================
date: Thu, 05 May 2022 16:30:47 GMT
content-type: application/xml
content-length: 509
intuit_tid: 1-6273fbb6-7b9ec11e76b3f7d9613551eb
x-spanid: 4edc71fd-ef5b-47f2-acbd-ceec6ceb626d
x-amzn-trace-id: Root=1-6273fbb6-7b9ec11e76b3f7d9613551eb
x-content-type-options: nosniff
x-envoy-upstream-service-time: 560
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-05-05T09:30:46.598-07:00">
  <Fault type="ValidationFault">
    <Error code="6000" element="">
      <Message>A business validation error has occurred while processing your request</Message>
      <Detail>Business Validation Error: You cannot enter more than 31 characters in the sales_custom_1_val field. You tried entering NTO 342- c..., which is 46 characters.</Detail>
    </Error>
  </Fault>
</IntuitResponse>


