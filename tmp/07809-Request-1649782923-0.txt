REQUEST URI FOR SEQUENCE ID 07809
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..Cyqx46hljurnEOuQi59yaQ.fKfGhNrOk7SZPvyZNNY9W0tfpf-NFhdtDNW5L6fCj6EbHXnBjzA8NZjOGRILPTJk0AU75eTk8rS4VYdpU-mPjc85WOo7Z6l0rH62t4w4VXghc1MUYx_RcuaXjAWMKRU3ugSDPxiFTAydJQRPWcfuS-1bqwhRRg9a1FAPvrAHYos4TEJRNtcXJa7D9Jzfib647jHU5diNi0xQLuthOxtoFVpfP5CPU9_CqHzqzz4cB-4LZUGMkOk-Bc0zfGLWLkPvObtWa3FG5hRuVai9x7BNQm7DoD2RRDtGaxA8cFPLsQl80D5Ma436wyUrDRb8dNy5X-JmR0-pjclOcp9ygAAgodIOS4ZhSWj8-1onM_qt7aXcFwmZMttPRgRv6Iba6r9zyx0LjrXlQIY9r1rsKy2ktWZCSUdsaBb-TIl0H2gHg2-paUGt3ebv_Z2B497g1g-euS0wtMaYAw1t27ZmDBz3IqR-SWW0YMyrK-cQ6d7xLZ4PRcz1ovUKqbGgeu82h3Amb4JX6ICLeo_cLpF-b5f3Y5M1m1B3qZJVmMOee5FUdYZJ5fVeX7ttGin6yIHbLNUn3f8mCURVYFspV_Reapt8aMJnlCjeEm1ivULYn8OuNiQ0F2o066zjooLu7asSP2X2QurLjre_2_nPFR8zJbZhOuUoOa-QHvDI4zSwv04Qcu3J3Pq-WUy-KHew7ADdfQmJeA9oSFDr_4T2iSauRyqT4fX7kePEiraykVPX-SHB1pg.m6T_rVyHpPjFb5OyDlSXjQ
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 2298

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>7500 SW 87TH AVE #200</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>449_1649782923</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - MARLIN PLUMBING OF MIAMI INC</ns0:Description>
    <ns0:Amount>35</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>35</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Firm Mail</ns0:Description>
    <ns0:Amount>3.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Firm Mail">18</ns0:ItemRef>
      <ns0:UnitPrice>3.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail</ns0:Description>
    <ns0:Amount>5.42</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Certified Mail">17</ns0:ItemRef>
      <ns0:UnitPrice>5.42</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>C70191120000063846154-449</ns0:Description>
    <ns0:Amount>6.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Manual">19</ns0:ItemRef>
      <ns0:UnitPrice>6.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>36</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


