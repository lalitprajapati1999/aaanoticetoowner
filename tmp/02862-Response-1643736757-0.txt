RESPONSE URI FOR SEQUENCE ID 02862
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

RESPONSE HEADERS
================
date: Tue, 01 Feb 2022 17:32:37 GMT
content-type: application/xml;charset=UTF-8
content-length: 2810
intuit_tid: 1-61f96eb5-544dd646474d349e733854db
x-spanid: b1285d9b-4cad-418c-8d3f-8610b633ed65
x-amzn-trace-id: Root=1-61f96eb5-544dd646474d349e733854db
x-content-type-options: nosniff
qbo-version: 1973.097
service-time: total=687, db=168
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private
x-envoy-upstream-service-time: 768
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-02-01T09:32:37.247-08:00">
  <Invoice domain="QBO" sparse="false">
    <Id>245</Id>
    <SyncToken>0</SyncToken>
    <MetaData>
      <CreateTime>2022-02-01T09:32:37-08:00</CreateTime>
      <LastUpdatedTime>2022-02-01T09:32:37-08:00</LastUpdatedTime>
    </MetaData>
    <CustomField>
      <DefinitionId>2</DefinitionId>
      <Type>StringType</Type>
      <StringValue>Satisfaction of Lien</StringValue>
    </CustomField>
    <CustomField>
      <DefinitionId>3</DefinitionId>
      <Type>StringType</Type>
      <StringValue>71 la gorce circle miami beach</StringValue>
    </CustomField>
    <DocNumber>213_1643736756</DocNumber>
    <TxnDate>2022-02-01</TxnDate>
    <CurrencyRef name="United States Dollar">USD</CurrencyRef>
    <Line>
      <Id>1</Id>
      <LineNum>1</LineNum>
      <Amount>80.00</Amount>
      <DetailType>SalesItemLineDetail</DetailType>
      <SalesItemLineDetail>
        <ItemRef name="SATISFACTIONS:Satisfaction of Lien">23</ItemRef>
        <UnitPrice>80</UnitPrice>
        <Qty>1</Qty>
        <ItemAccountRef name="Services">1</ItemAccountRef>
        <TaxCodeRef>NON</TaxCodeRef>
        <TaxClassificationRef>EUC-99990201-V1-00020000</TaxClassificationRef>
      </SalesItemLineDetail>
    </Line>
    <Line>
      <Amount>80.00</Amount>
      <DetailType>SubTotalLineDetail</DetailType>
      <SubTotalLineDetail/>
    </Line>
    <TxnTaxDetail>
      <TxnTaxCodeRef>3</TxnTaxCodeRef>
      <TotalTax>0</TotalTax>
      <TaxLine>
        <Amount>0</Amount>
        <DetailType>TaxLineDetail</DetailType>
        <TaxLineDetail>
          <TaxRateRef>3</TaxRateRef>
          <PercentBased>true</PercentBased>
          <TaxPercent>6</TaxPercent>
          <NetAmountTaxable>0</NetAmountTaxable>
        </TaxLineDetail>
      </TaxLine>
      <TaxLine>
        <Amount>0</Amount>
        <DetailType>TaxLineDetail</DetailType>
        <TaxLineDetail>
          <TaxRateRef>4</TaxRateRef>
          <PercentBased>true</PercentBased>
          <TaxPercent>1</TaxPercent>
          <NetAmountTaxable>0</NetAmountTaxable>
        </TaxLineDetail>
      </TaxLine>
    </TxnTaxDetail>
    <CustomerRef name="AAA Construction Corp._1615909222">213</CustomerRef>
    <BillAddr>
      <Id>218</Id>
      <Line1>301 Hialeah Drive ste 118</Line1>
      <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
      <PostalCode>33010</PostalCode>
    </BillAddr>
    <ShipAddr>
      <Id>219</Id>
      <Line1>301 Hialeah Drive ste 118</Line1>
      <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
      <PostalCode>33010</PostalCode>
    </ShipAddr>
    <FreeFormAddress>true</FreeFormAddress>
    <ShipFromAddr>
      <Id>541</Id>
      <Line1>PO Box 22821</Line1>
      <Line2>Hialeah, FL  33002 US</Line2>
    </ShipFromAddr>
    <SalesTermRef name="Net 15">2</SalesTermRef>
    <DueDate>2022-02-16</DueDate>
    <TotalAmt>80.00</TotalAmt>
    <ApplyTaxAfterDiscount>false</ApplyTaxAfterDiscount>
    <PrintStatus>NeedToPrint</PrintStatus>
    <EmailStatus>NotSet</EmailStatus>
    <Balance>80.00</Balance>
    <TaxExemptionRef/>
    <AllowIPNPayment>false</AllowIPNPayment>
    <AllowOnlinePayment>false</AllowOnlinePayment>
    <AllowOnlineCreditCardPayment>false</AllowOnlineCreditCardPayment>
    <AllowOnlineACHPayment>false</AllowOnlineACHPayment>
  </Invoice>
</IntuitResponse>


