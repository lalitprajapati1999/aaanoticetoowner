RESPONSE URI FOR SEQUENCE ID 00048
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/query?minorversion=47

RESPONSE HEADERS
================
date: Tue, 11 May 2021 07:58:16 GMT
content-type: application/xml;charset=UTF-8
content-length: 2912
server: nginx
strict-transport-security: max-age=15552000
intuit_tid: 1-609a3918-4b5c02015c6d10ba7fcc4e0a
x-spanid: 9b43316f-406d-4f20-a5e1-979f4e917626
x-amzn-trace-id: Root=1-609a3918-4b5c02015c6d10ba7fcc4e0a
qbo-version: 1957.141
service-time: total=107, db=49
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2021-05-11T00:58:16.458-07:00">
  <QueryResponse startPosition="1" maxResults="1" totalCount="1">
    <Invoice domain="QBO" sparse="false">
      <Id>7</Id>
      <SyncToken>1</SyncToken>
      <MetaData>
        <CreateTime>2021-04-12T06:00:06-07:00</CreateTime>
        <LastUpdatedTime>2021-05-11T00:57:43-07:00</LastUpdatedTime>
      </MetaData>
      <CustomField>
        <DefinitionId>2</DefinitionId>
        <Type>StringType</Type>
        <StringValue>NOTICE TO OWNER</StringValue>
      </CustomField>
      <CustomField>
        <DefinitionId>3</DefinitionId>
        <Type>StringType</Type>
        <StringValue>101 INDUSTRY WAY SATNUNTON VIR</StringValue>
      </CustomField>
      <DocNumber>1618232405_18_6660</DocNumber>
      <TxnDate>2021-04-12</TxnDate>
      <CurrencyRef name="United States Dollar">USD</CurrencyRef>
      <Line>
        <Id>1</Id>
        <LineNum>1</LineNum>
        <Description>Contracted By - PROCON INC</Description>
        <Amount>25.00</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="SERVICES:NTO">32</ItemRef>
          <UnitPrice>25</UnitPrice>
          <Qty>1</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>TAX</TaxCodeRef>
          <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Amount>25.00</Amount>
        <DetailType>SubTotalLineDetail</DetailType>
        <SubTotalLineDetail/>
      </Line>
      <TxnTaxDetail>
        <TxnTaxCodeRef>4</TxnTaxCodeRef>
        <TotalTax>1.75</TotalTax>
        <TaxLine>
          <Amount>0.25</Amount>
          <DetailType>TaxLineDetail</DetailType>
          <TaxLineDetail>
            <TaxRateRef>5</TaxRateRef>
            <PercentBased>true</PercentBased>
            <TaxPercent>1</TaxPercent>
            <NetAmountTaxable>25.00</NetAmountTaxable>
          </TaxLineDetail>
        </TaxLine>
        <TaxLine>
          <Amount>1.50</Amount>
          <DetailType>TaxLineDetail</DetailType>
          <TaxLineDetail>
            <TaxRateRef>3</TaxRateRef>
            <PercentBased>true</PercentBased>
            <TaxPercent>6</TaxPercent>
            <NetAmountTaxable>25.00</NetAmountTaxable>
          </TaxLineDetail>
        </TaxLine>
      </TxnTaxDetail>
      <CustomerRef name="K &amp; S PATCHING, INC._1618227073">214</CustomerRef>
      <BillAddr>
        <Id>233</Id>
        <Line1>K &amp; S PATCHING, INC.</Line1>
        <Line2>3164 SW 120TH TER</Line2>
        <Line3>Florida  33025.</Line3>
      </BillAddr>
      <ShipAddr>
        <Id>226</Id>
        <Line1>3164 SW 120TH TER</Line1>
        <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
        <PostalCode>33025</PostalCode>
      </ShipAddr>
      <FreeFormAddress>true</FreeFormAddress>
      <ShipFromAddr>
        <Id>227</Id>
        <Line1>186 Westward Dr</Line1>
        <Line2>Miami Springs, FL  33166 US</Line2>
      </ShipFromAddr>
      <SalesTermRef name="Net 15">2</SalesTermRef>
      <DueDate>2021-04-27</DueDate>
      <TotalAmt>26.75</TotalAmt>
      <ApplyTaxAfterDiscount>false</ApplyTaxAfterDiscount>
      <PrintStatus>NeedToPrint</PrintStatus>
      <EmailStatus>NotSet</EmailStatus>
      <Balance>26.75</Balance>
      <TaxExemptionRef/>
      <AllowIPNPayment>false</AllowIPNPayment>
      <AllowOnlinePayment>false</AllowOnlinePayment>
      <AllowOnlineCreditCardPayment>false</AllowOnlineCreditCardPayment>
      <AllowOnlineACHPayment>false</AllowOnlineACHPayment>
    </Invoice>
  </QueryResponse>
</IntuitResponse>


