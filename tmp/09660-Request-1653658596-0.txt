REQUEST URI FOR SEQUENCE ID 09660
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..NN4lgy3iHbF4VhE-cR9lkg.IUAj1LQw7E_0pGAaX7X12xqVpBBMhlXkVoiBiQdMdVHvWHNcElPbRHeZCrfsBgaIsM_D-XaBnnzMnVmCyoJct-te56pdgVMRB7spWisqpDD8whwjPiqVLdHloG0iHOXWuWv6ngm2y3DVcKl3ATKITEdQJ1XiNMIgXb3x3n5i1mllVcqeYfQvLWe19S2y8aqE2dAJaYGvql81GlJG4x2kNMDiokYHP_jodMBl-h5kDIDXkBONiFKXkMlElYfAlP8IBkQQ3lycpFbnh4FOSTkAyl18zy5wQrT8cGpgMV9kecusW1GtaEDzKu2cx4_OHskN6ZIkXBn_FDt0d8ZRdh-pzQfaYDyVGkCoKVcArwbKcwDhnCbB0nMg7vBG8wx27o0u0JnE7kMEBgsAYdRj1qNMlccUYZfZw3QZqrsLeKqa4sB4pqCmxhqsD3VUllHZbZuJXxKuIJThYSYSWo__6mQezNSR0OiAUMooMkkxE77CPHt5WYxUo9Wtvo33XXl3hmPYC45v0kljB-rJ73j3aqrmjnIJ5hKxI5C8Te-Qt_GA6lE_3f1Pw29fwyGO1X_0hzMuUUlwqLWUgLe0CLOnrD17kallUCtelBfY7a9E8W8yiRE_kQAVyoQjaYQ0u3J5iIzmiCbU8LRKIWN8rHjpMUSOSVEi4FaZxQE5FCdmWW2WyoZ7p2E3QZGJAlHEEhaI18qxrkdrnpXR-FSldrp8z6aN4ZAAFO48GMd5xtUFLJsWVBE.kDLWXtmEx1mcjxvRDtUGuw
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 2700

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>15031 SW 152 TERR MIAMI FL 331</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>621_1653658596</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - MSL BUILDERS CORP.</ns0:Description>
    <ns0:Amount>20.00</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>20.00</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Rush Fee</ns0:Description>
    <ns0:Amount>8.50</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Rush Hour Charges">33</ns0:ItemRef>
      <ns0:UnitPrice>8.50</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Firm Mail</ns0:Description>
    <ns0:Amount>3.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Firm Mail">18</ns0:ItemRef>
      <ns0:UnitPrice>3.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail</ns0:Description>
    <ns0:Amount>5.42</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Certified Mail">17</ns0:ItemRef>
      <ns0:UnitPrice>5.42</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail with Electronic Return Receipt</ns0:Description>
    <ns0:Amount>7.22</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Electronic Return Receipt">15</ns0:ItemRef>
      <ns0:UnitPrice>7.22</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>195</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


