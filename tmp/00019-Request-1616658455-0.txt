REQUEST URI FOR SEQUENCE ID 00019
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..yp9L62Qx75yzaI6oYTWDbA.QReDfxZaQtsK-SJBQtjPKw0APE9QyOHc8TPmlFTGgO3XM-ceTrr7AuxygjAZW4GUEZLw6b9wvMkbHXWYR3Fga4qvW1daKQmZR2KWIpEZHHR3cwqqTuVMvz-2seX4wJfykG76YMLtJFMvTRtcDPrKlmDTj5abClILsB_k9Gq3FrYJPVSXgsKICD2xk1d74mDCGbIx51SpjD_sn3yXW6BMZsffD_xXyR8eTRomsK0nyRn2-zj5M-LcRegNKv8qWGv_Cl5qllnwuVLZj8-z8SQTW5qOTxHK2k2nzwftuvtS84WIKHnQaiKLcQj3P_t8xHw1pGe54g76tIV0m6Ig_Zzfhk06azh5Pf7jfVKBaL2iWLRZtc_x4k8LN6cze9_FRnm_rvZsZfrQFkVfVVPZWThv91whRcCMvW8b1tSNEf6PRfBz9T9TGgn7R_GM9IlAAD45FwJsHaYkyYJM1lMhSGYtaoRSgLH2SFoZRGeP9NvwXMroGYyUcoF3O1l-kzZULw995sPupRpUZ3p6STmxUp_bVQT8zFjm5noJa4ReMhm-3TDb_XvDWs09bSYqej9OcReDSfoaLCbvLwRoTf4l-gRfAYFIvWn8Zejj0yqnthcHSRGm3rmTcOAf_v88yNkFl3YibXY8pQCawWS18rLni9joANUm4edgayw7A4OWpKltho0y_4GpCy1A8JJnrNCy06cVR8NOR34ceWkWdSSL4GllKNMhjo8RyzM-n5mUgqkYztc._vCWW54VRkub_VpoTDYA4g
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 1609

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>0333</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Intent to Lien</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Lake Buena Vista 12529 State R</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>1616658455_10_8562</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - Sukanya D
Parent Work Order Number - 7</ns0:Description>
    <ns0:Amount>45.00</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>45.00</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail with Electronic Return Receipt</ns0:Description>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Electronic Return Receipt">15</ns0:ItemRef>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>211</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


