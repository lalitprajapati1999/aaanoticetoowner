RESPONSE URI FOR SEQUENCE ID 06582
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/query?minorversion=47

RESPONSE HEADERS
================
date: Mon, 04 Apr 2022 16:23:50 GMT
content-type: application/xml;charset=UTF-8
content-length: 4333
intuit_tid: 1-624b1b96-4b6c7c3d652d6a7b56e696c8
x-spanid: c2d17ccb-d28c-4a01-8fee-9224a5b4fecc
x-amzn-trace-id: Root=1-624b1b96-4b6c7c3d652d6a7b56e696c8
x-content-type-options: nosniff
x-xss-protection: 1; mode=block
qbo-version: 1977.183
service-time: total=289, db=48
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private
x-envoy-upstream-service-time: 327
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-04-04T09:23:50.749-07:00">
  <QueryResponse startPosition="1" maxResults="1" totalCount="1">
    <Invoice domain="QBO" sparse="false">
      <Id>723</Id>
      <SyncToken>0</SyncToken>
      <MetaData>
        <CreateTime>2022-04-04T09:01:26-07:00</CreateTime>
        <LastUpdatedTime>2022-04-04T09:01:26-07:00</LastUpdatedTime>
      </MetaData>
      <CustomField>
        <DefinitionId>2</DefinitionId>
        <Type>StringType</Type>
        <StringValue>Claim of Lien</StringValue>
      </CustomField>
      <CustomField>
        <DefinitionId>3</DefinitionId>
        <Type>StringType</Type>
        <StringValue>7390 ARTHUR ST HOLLYWOOD FL 33</StringValue>
      </CustomField>
      <DocNumber>357_1649088085</DocNumber>
      <TxnDate>2022-04-04</TxnDate>
      <CurrencyRef name="United States Dollar">USD</CurrencyRef>
      <Line>
        <Id>1</Id>
        <LineNum>1</LineNum>
        <Description>Contracted By - CASTRILLON SERVICES, INC.
Parent Work Order Number - 245</Description>
        <Amount>185.00</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="CLAIM OF LIEN:Claim of Lien">38</ItemRef>
          <UnitPrice>185</UnitPrice>
          <Qty>1</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>NON</TaxCodeRef>
          <TaxClassificationRef>EUC-99990201-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Id>2</Id>
        <LineNum>2</LineNum>
        <Description>CRR-70191120000063845829-COL357</Description>
        <Amount>7.25</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="MAILING:Manual">19</ItemRef>
          <UnitPrice>7.25</UnitPrice>
          <Qty>1</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>TAX</TaxCodeRef>
          <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Id>3</Id>
        <LineNum>3</LineNum>
        <Description>CRR-70191120000063845836-COL357</Description>
        <Amount>7.25</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="MAILING:Manual">19</ItemRef>
          <UnitPrice>7.25</UnitPrice>
          <Qty>1</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>TAX</TaxCodeRef>
          <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Id>4</Id>
        <LineNum>4</LineNum>
        <Description>CRR-70191120000063845843-COL357</Description>
        <Amount>7.25</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="MAILING:Manual">19</ItemRef>
          <UnitPrice>7.25</UnitPrice>
          <Qty>1</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>TAX</TaxCodeRef>
          <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Amount>206.75</Amount>
        <DetailType>SubTotalLineDetail</DetailType>
        <SubTotalLineDetail/>
      </Line>
      <TxnTaxDetail>
        <TxnTaxCodeRef>3</TxnTaxCodeRef>
        <TotalTax>0</TotalTax>
        <TaxLine>
          <Amount>0</Amount>
          <DetailType>TaxLineDetail</DetailType>
          <TaxLineDetail>
            <TaxRateRef>3</TaxRateRef>
            <PercentBased>true</PercentBased>
            <TaxPercent>6</TaxPercent>
            <NetAmountTaxable>0</NetAmountTaxable>
          </TaxLineDetail>
        </TaxLine>
        <TaxLine>
          <Amount>0</Amount>
          <DetailType>TaxLineDetail</DetailType>
          <TaxLineDetail>
            <TaxRateRef>4</TaxRateRef>
            <PercentBased>true</PercentBased>
            <TaxPercent>1</TaxPercent>
            <NetAmountTaxable>0</NetAmountTaxable>
          </TaxLineDetail>
        </TaxLine>
      </TxnTaxDetail>
      <CustomerRef name="ERNESTO HERNANDEZ">195</CustomerRef>
      <BillAddr>
        <Id>196</Id>
        <Line1>6945 NW 53 TERRACE</Line1>
        <City>MIAMI</City>
        <CountrySubDivisionCode>FLOIRDA</CountrySubDivisionCode>
        <PostalCode>33166</PostalCode>
      </BillAddr>
      <ShipAddr>
        <Id>196</Id>
        <Line1>6945 NW 53 TERRACE</Line1>
        <City>MIAMI</City>
        <CountrySubDivisionCode>FLOIRDA</CountrySubDivisionCode>
        <PostalCode>33166</PostalCode>
      </ShipAddr>
      <FreeFormAddress>true</FreeFormAddress>
      <ShipFromAddr>
        <Id>929</Id>
        <Line1>PO Box 22821</Line1>
        <Line2>Hialeah, FL  33002 US</Line2>
      </ShipFromAddr>
      <SalesTermRef name="Net 15">2</SalesTermRef>
      <DueDate>2022-04-19</DueDate>
      <TotalAmt>206.75</TotalAmt>
      <ApplyTaxAfterDiscount>false</ApplyTaxAfterDiscount>
      <PrintStatus>NeedToPrint</PrintStatus>
      <EmailStatus>NotSet</EmailStatus>
      <Balance>206.75</Balance>
      <TaxExemptionRef name="service">99</TaxExemptionRef>
      <AllowIPNPayment>false</AllowIPNPayment>
      <AllowOnlinePayment>false</AllowOnlinePayment>
      <AllowOnlineCreditCardPayment>false</AllowOnlineCreditCardPayment>
      <AllowOnlineACHPayment>false</AllowOnlineACHPayment>
    </Invoice>
  </QueryResponse>
</IntuitResponse>


