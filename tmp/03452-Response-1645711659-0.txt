RESPONSE URI FOR SEQUENCE ID 03452
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/query?minorversion=47

RESPONSE HEADERS
================
date: Thu, 24 Feb 2022 14:07:39 GMT
content-type: application/xml;charset=UTF-8
content-length: 4286
intuit_tid: 1-6217912b-2cbc2ce66ff5679676f49beb
x-spanid: 3e361eab-57d3-48ec-a5e8-8594f592f953
x-amzn-trace-id: Root=1-6217912b-2cbc2ce66ff5679676f49beb
x-content-type-options: nosniff
x-xss-protection: 1; mode=block
qbo-version: 1975.230
service-time: total=159, db=37
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private
x-envoy-upstream-service-time: 183
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-02-24T06:07:39.135-08:00">
  <QueryResponse startPosition="1" maxResults="1" totalCount="1">
    <Invoice domain="QBO" sparse="false">
      <Id>380</Id>
      <SyncToken>0</SyncToken>
      <MetaData>
        <CreateTime>2022-02-24T06:04:34-08:00</CreateTime>
        <LastUpdatedTime>2022-02-24T06:04:34-08:00</LastUpdatedTime>
      </MetaData>
      <CustomField>
        <DefinitionId>2</DefinitionId>
        <Type>StringType</Type>
        <StringValue>Notice to Owner/Preliminary No</StringValue>
      </CustomField>
      <CustomField>
        <DefinitionId>3</DefinitionId>
        <Type>StringType</Type>
        <StringValue>2414 DE SOTO BOULEVARD CORAL G</StringValue>
      </CustomField>
      <DocNumber>297_1645711473</DocNumber>
      <TxnDate>2022-02-24</TxnDate>
      <CurrencyRef name="United States Dollar">USD</CurrencyRef>
      <Line>
        <Id>1</Id>
        <LineNum>1</LineNum>
        <Description>Contracted By - RBG CARIB CONSTRUCTION CORP.</Description>
        <Amount>45.00</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="SERVICES:NTO">32</ItemRef>
          <UnitPrice>45</UnitPrice>
          <Qty>1</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>TAX</TaxCodeRef>
          <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Id>2</Id>
        <LineNum>2</LineNum>
        <Description>Rush Fee</Description>
        <Amount>15.00</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="SERVICES:Rush fee">33</ItemRef>
          <UnitPrice>15</UnitPrice>
          <Qty>1</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>TAX</TaxCodeRef>
          <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Id>3</Id>
        <LineNum>3</LineNum>
        <Description>Firm Mail</Description>
        <Amount>3.95</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="MAILING:Firm Mail">18</ItemRef>
          <UnitPrice>3.95</UnitPrice>
          <Qty>1</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>TAX</TaxCodeRef>
          <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Id>4</Id>
        <LineNum>4</LineNum>
        <Description>FEDEX-776117933899-297</Description>
        <Amount>55.00</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="MAILING:Manual">19</ItemRef>
          <UnitPrice>55</UnitPrice>
          <Qty>1</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>TAX</TaxCodeRef>
          <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Amount>118.95</Amount>
        <DetailType>SubTotalLineDetail</DetailType>
        <SubTotalLineDetail/>
      </Line>
      <TxnTaxDetail>
        <TxnTaxCodeRef>3</TxnTaxCodeRef>
        <TotalTax>8.33</TotalTax>
        <TaxLine>
          <Amount>7.14</Amount>
          <DetailType>TaxLineDetail</DetailType>
          <TaxLineDetail>
            <TaxRateRef>3</TaxRateRef>
            <PercentBased>true</PercentBased>
            <TaxPercent>6</TaxPercent>
            <NetAmountTaxable>118.95</NetAmountTaxable>
          </TaxLineDetail>
        </TaxLine>
        <TaxLine>
          <Amount>1.19</Amount>
          <DetailType>TaxLineDetail</DetailType>
          <TaxLineDetail>
            <TaxRateRef>4</TaxRateRef>
            <PercentBased>true</PercentBased>
            <TaxPercent>1</TaxPercent>
            <NetAmountTaxable>118.95</NetAmountTaxable>
          </TaxLineDetail>
        </TaxLine>
      </TxnTaxDetail>
      <CustomerRef name="JIREH MEGA BUILDERS STRUCTURAL INC_1642567647">241</CustomerRef>
      <BillAddr>
        <Id>495</Id>
        <Line1>18495 S DIXIE HWY STE # 131</Line1>
        <City>Miami</City>
        <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
        <PostalCode>33189</PostalCode>
      </BillAddr>
      <ShipAddr>
        <Id>496</Id>
        <Line1>18495 S DIXIE HWY STE # 131</Line1>
        <City>Miami</City>
        <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
        <PostalCode>33189</PostalCode>
      </ShipAddr>
      <FreeFormAddress>true</FreeFormAddress>
      <ShipFromAddr>
        <Id>629</Id>
        <Line1>PO Box 22821</Line1>
        <Line2>Hialeah, FL  33002 US</Line2>
      </ShipFromAddr>
      <SalesTermRef name="Net 15">2</SalesTermRef>
      <DueDate>2022-03-11</DueDate>
      <TotalAmt>127.28</TotalAmt>
      <ApplyTaxAfterDiscount>false</ApplyTaxAfterDiscount>
      <PrintStatus>NeedToPrint</PrintStatus>
      <EmailStatus>NotSet</EmailStatus>
      <Balance>127.28</Balance>
      <TaxExemptionRef/>
      <AllowIPNPayment>false</AllowIPNPayment>
      <AllowOnlinePayment>false</AllowOnlinePayment>
      <AllowOnlineCreditCardPayment>false</AllowOnlineCreditCardPayment>
      <AllowOnlineACHPayment>false</AllowOnlineACHPayment>
    </Invoice>
  </QueryResponse>
</IntuitResponse>


