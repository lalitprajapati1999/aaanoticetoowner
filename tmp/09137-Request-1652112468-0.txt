REQUEST URI FOR SEQUENCE ID 09137
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..7jUNt_-u47vcBhNb3y_6LQ.ZXRTK33WyXD1JH1pTY3IgKk70i0jFB0TjHveUsShv1LKLAWoSF9chBkzrKye_VxKzvjbVcMIG7z8Fej9LOCXH2Wnj5zKn2ZX6Ti1eMsO_bWtYcOzANmmJWnTs0bx-xesbU8HByLnrfagMWu7nO3vvIob1o35SOsiJZY85O7qfX5hxesG8fsRqlhq1q9863vrOSB2C6ErahzkYGHUfpSwX_QxixdLYZ6OAn8ZZ9o2x-DSZ5B6xjRlyb4XmGSBcDgcIZwsZn9cyNBfFPp7JkCcXwx_2d8M3p6Fima8xvmSLnCsFDKqFBNU0RWC6HGK2Jg-gbDLrKRMm8n8-NvRX8ckV_yD_RLhCfB8UR5Fn3SfNIMgRamzBo7Z1vr9-GaOM93tjmSaLSf2yACJ2PsLbYGC9Xg-I7jE09uxzQnpEwerVyZHkQvwLZ7pE-5csrtp1yy1W-8s1Sr-OIH60U7EoayHIK8_tB5uec_axxV8k4dKkfoYzD0e7wWKI-mm-Zqg6yMTyahDb68jYnFoFq_utx6hZ5R_EVwrbtfpQNe5NIFUg0pX2IgBjoyo2ZFAQXbrSdVfQ87MASbH99stK6-D5sAqnXMw7EyIum9pmBoBeuGqVQuENHfgXbR9gvbDBMK6YsdTS1tD7gVqqcSEW4HRrpyK1r-doF2QkrRVvr73Ir82c6xXi3duMBOx1ey70mEO92HxM1DebFdsLLyehX4fthwAFyFl-ctFf-PI4Bpozu-HVX4.xHxZSgpEtosl6U_LoqpyJA
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 2702

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>321 NW 4 AVE. MIAMI FL 33128</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>568_1652112468</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - 748 DEVELOPMENT LLC</ns0:Description>
    <ns0:Amount>20.00</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>20.00</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Rush Fee</ns0:Description>
    <ns0:Amount>8.50</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Rush Hour Charges">33</ns0:ItemRef>
      <ns0:UnitPrice>8.50</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Firm Mail</ns0:Description>
    <ns0:Amount>3.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Firm Mail">18</ns0:ItemRef>
      <ns0:UnitPrice>3.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail with Electronic Return Receipt</ns0:Description>
    <ns0:Amount>7.22</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Electronic Return Receipt">15</ns0:ItemRef>
      <ns0:UnitPrice>7.22</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>C70191120000063846611-568</ns0:Description>
    <ns0:Amount>6.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Manual">19</ns0:ItemRef>
      <ns0:UnitPrice>6.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>195</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


