REQUEST URI FOR SEQUENCE ID 12424
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..a7Sc8jEZ479AECpnNgYxMA.zq0sZFZZNVFwkpvnHyb1oIg-fdL20br563EG7IM4blzrELmvkwSgrn62366eq2iZJca9gja9uzxQrJt8GGykVUUOdWAwWV4FPpxW_QWBeW6ZLaSBjLxy593sIdV4tAVU411o-RXDOTaOHovmv6M9N6wb3Vh4IM0MR9iWWEW1ReCzOWxaNSav5ynmtHP1Habi0XbvNcBfQO1Vhgrk9THyHw2Y1L38kX4NQZtbwqTY3lWTV0jp-DvvUFas1yqKHIvJ5APRE3tiWxd5dcZkh7dmzg8anHaE-B60AYNurmGyrPjyB-v5jx3ZCFRuTbzy2gS5RNQ3-cbI0OniyMO2XGwhNRoSCMmN-q1WP_LVnuIv6RLfAilTF893hlzVQkxFzY9lcJkqCqfudKNRarpPbuOLpwO9xrZ-PTmaz3ItPsaKJ51lPhMdJyEwLfliBJU41bgVp2Dg14rfLTeMzdrkPix2OtnrerTUg1T9XOxRep7d9pxfPc65E3CDhf1ULf-_VlLY2FpzCBGgF1shKNOfPcvOwX-yo-dlrDCbtV3i6udRaA5buuWl3l6i-N5Rh-zjvUBEnwnov_Pssutrv48nRQhHjgT5NczZORB5fYpYAq1wg74cWTosrKWg1HnIKGIXwC2qA7OjyDCbOzN90SUwQP2jX7rGRhEybV3gPYP47Dnb3lwBoE_lTrNJurNa3dHKiTFpmrMcohmsYkHiuRlkTArxBPsUtKvTGkqpL-OEvz_zN9c.qLWRvBdvulACxfFIJjpsIg
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 1239

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue> 3600 North Federal Highway. F</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>852_1658940369</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - Vision General Contractors</ns0:Description>
    <ns0:Amount>45.00</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>45.00</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>228</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


