RESPONSE URI FOR SEQUENCE ID 00100
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

RESPONSE HEADERS
================
date: Thu, 13 May 2021 14:16:18 GMT
content-type: application/xml;charset=UTF-8
content-length: 3887
server: nginx
strict-transport-security: max-age=15552000
intuit_tid: 1-609d34b1-796e12bb60994150394a5beb
x-spanid: 3169232e-b98a-4422-b087-f6e3b77c83a9
x-amzn-trace-id: Root=1-609d34b1-796e12bb60994150394a5beb
qbo-version: 1957.141
service-time: total=837, db=211
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2021-05-13T07:16:17.921-07:00">
  <Invoice domain="QBO" sparse="false">
    <Id>12</Id>
    <SyncToken>0</SyncToken>
    <MetaData>
      <CreateTime>2021-05-13T07:16:18-07:00</CreateTime>
      <LastUpdatedTime>2021-05-13T07:16:18-07:00</LastUpdatedTime>
    </MetaData>
    <CustomField>
      <DefinitionId>1</DefinitionId>
      <Type>StringType</Type>
      <StringValue>PO NTO# 17710-113</StringValue>
    </CustomField>
    <CustomField>
      <DefinitionId>2</DefinitionId>
      <Type>StringType</Type>
      <StringValue>Claim of Lien</StringValue>
    </CustomField>
    <CustomField>
      <DefinitionId>3</DefinitionId>
      <Type>StringType</Type>
      <StringValue>10471 BOCA WOOD LANE BOCA RATO</StringValue>
    </CustomField>
    <DocNumber>1620915376_27_5811</DocNumber>
    <TxnDate>2021-05-13</TxnDate>
    <CurrencyRef name="United States Dollar">USD</CurrencyRef>
    <Line>
      <Id>1</Id>
      <LineNum>1</LineNum>
      <Description>Contracted By - AG CONTRACTORSLLC
Parent Work Order Number - 11</Description>
      <Amount>185.00</Amount>
      <DetailType>SalesItemLineDetail</DetailType>
      <SalesItemLineDetail>
        <ItemRef name="SERVICES:NTO">32</ItemRef>
        <UnitPrice>185</UnitPrice>
        <Qty>1</Qty>
        <ItemAccountRef name="Services">1</ItemAccountRef>
        <TaxCodeRef>TAX</TaxCodeRef>
        <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
      </SalesItemLineDetail>
    </Line>
    <Line>
      <Id>2</Id>
      <LineNum>2</LineNum>
      <Description>these are rush hour charges</Description>
      <Amount>9.00</Amount>
      <DetailType>SalesItemLineDetail</DetailType>
      <SalesItemLineDetail>
        <ItemRef name="SERVICES:NTO Rush fee">33</ItemRef>
        <UnitPrice>9</UnitPrice>
        <Qty>1</Qty>
        <ItemAccountRef name="Services">1</ItemAccountRef>
        <TaxCodeRef>TAX</TaxCodeRef>
        <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
      </SalesItemLineDetail>
    </Line>
    <Line>
      <Id>3</Id>
      <LineNum>3</LineNum>
      <Description>firm mail</Description>
      <Amount>8.00</Amount>
      <DetailType>SalesItemLineDetail</DetailType>
      <SalesItemLineDetail>
        <ItemRef name="MAILING:Firm Mail">18</ItemRef>
        <UnitPrice>1</UnitPrice>
        <Qty>8</Qty>
        <ItemAccountRef name="Services">1</ItemAccountRef>
        <TaxCodeRef>TAX</TaxCodeRef>
        <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
      </SalesItemLineDetail>
    </Line>
    <Line>
      <Amount>202.00</Amount>
      <DetailType>SubTotalLineDetail</DetailType>
      <SubTotalLineDetail/>
    </Line>
    <TxnTaxDetail>
      <TxnTaxCodeRef>3</TxnTaxCodeRef>
      <TotalTax>14.14</TotalTax>
      <TaxLine>
        <Amount>12.12</Amount>
        <DetailType>TaxLineDetail</DetailType>
        <TaxLineDetail>
          <TaxRateRef>3</TaxRateRef>
          <PercentBased>true</PercentBased>
          <TaxPercent>6</TaxPercent>
          <NetAmountTaxable>202.00</NetAmountTaxable>
        </TaxLineDetail>
      </TaxLine>
      <TaxLine>
        <Amount>2.02</Amount>
        <DetailType>TaxLineDetail</DetailType>
        <TaxLineDetail>
          <TaxRateRef>4</TaxRateRef>
          <PercentBased>true</PercentBased>
          <TaxPercent>1</TaxPercent>
          <NetAmountTaxable>202.00</NetAmountTaxable>
        </TaxLineDetail>
      </TaxLine>
    </TxnTaxDetail>
    <CustomerRef name="AAA Construction Corp._1615909222">213</CustomerRef>
    <BillAddr>
      <Id>218</Id>
      <Line1>301 Hialeah Drive ste 118</Line1>
      <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
      <PostalCode>33010</PostalCode>
    </BillAddr>
    <ShipAddr>
      <Id>219</Id>
      <Line1>301 Hialeah Drive ste 118</Line1>
      <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
      <PostalCode>33010</PostalCode>
    </ShipAddr>
    <FreeFormAddress>true</FreeFormAddress>
    <ShipFromAddr>
      <Id>238</Id>
      <Line1>186 Westward Dr</Line1>
      <Line2>Miami Springs, FL  33166 US</Line2>
    </ShipFromAddr>
    <SalesTermRef name="Net 15">2</SalesTermRef>
    <DueDate>2021-05-28</DueDate>
    <TotalAmt>216.14</TotalAmt>
    <ApplyTaxAfterDiscount>false</ApplyTaxAfterDiscount>
    <PrintStatus>NeedToPrint</PrintStatus>
    <EmailStatus>NotSet</EmailStatus>
    <Balance>216.14</Balance>
    <TaxExemptionRef/>
    <AllowIPNPayment>false</AllowIPNPayment>
    <AllowOnlinePayment>false</AllowOnlinePayment>
    <AllowOnlineCreditCardPayment>false</AllowOnlineCreditCardPayment>
    <AllowOnlineACHPayment>false</AllowOnlineACHPayment>
  </Invoice>
</IntuitResponse>


