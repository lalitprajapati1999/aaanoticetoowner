REQUEST URI FOR SEQUENCE ID 10046
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..O7fgHTNjoctyjJ2nLIuIrQ.fa4OPxSW7vOOQVi0mZG27snqXovFZoTopxTvyuirO7-xwUynd1gPuz4rR2Q2I_CBRln1bJ9m5dMhnpqEDTl2QWw3NZMab7fnKIiqG5Y1-xHO9f-XFwWez_Ljx0sa7J-kk7xx5HAYoNEza8Wydspu31xgBS7cLttvg2Pu0I0R5yMW_3hK8NSm38zvv5zbhh7wBmRjjS_unLU6zxA2v0PpdobgKLBBZgbseRRi_3LKuqBiAodyWgSB1oz-ETyzM7RocXYRz9fu-mBr5gElHtUdj9-5-0qwFgBfoHcKhIDBqbNlOd-wC9oqwCLNJqvTcT_AWXf6iYUUvfV_xCg_H2a5tBZ0G2imsVTIsmU9efEPVyAZGeCh-U28dAY-jCdoEo7ybS2oLULN8dpOAuAlRdvGWGxtkxh09PPit0X3QzuE2EGMD0kcXzQcO96l382IEA2geeBVynI1LQt2y3MPendq1B7XFUSI3EXHHfuG0mKb63xQKqwit7a91pfezs-o3R4wkruMLppGXEw70XzeIb8KSKyb_4cOyiIOvYBuhHxTlbOPQYTEqnhqRhQ6La6zozs2A6jXreFV5z8RU4B6nPSC59Ig1clZMMKcXdCXbWtBm9nMLFHn_QexzLV5n2Aq_4Dpo83dhbTDr3h-ZLRxs_ElECXtrmcY06TEe4CDvmAZPFPe5_UgEjdoJP0WFtxbXaRpIPCXV2f1-0VdwgXBiKbJEGmimNzZReFgsED6ww6aUKc.Q0hjDQqPY10MGyOZqeguEQ
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 2346

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>176 ORANGE BLOSSOM Rd, TAVERNI</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>682_1654617587</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - FARFAN DEVELOPMENT INC</ns0:Description>
    <ns0:Amount>45.00</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>45.00</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Rush Fee</ns0:Description>
    <ns0:Amount>15.00</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Rush Hour Charges">33</ns0:ItemRef>
      <ns0:UnitPrice>15.00</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Firm Mail</ns0:Description>
    <ns0:Amount>3.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Firm Mail">18</ns0:ItemRef>
      <ns0:UnitPrice>3.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail with Electronic Return Receipt</ns0:Description>
    <ns0:Amount>7.22</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Electronic Return Receipt">15</ns0:ItemRef>
      <ns0:UnitPrice>7.22</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>269</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


