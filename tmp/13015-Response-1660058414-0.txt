RESPONSE URI FOR SEQUENCE ID 13015
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

RESPONSE HEADERS
================
date: Tue, 09 Aug 2022 15:20:16 GMT
content-type: application/xml
content-length: 509
intuit_tid: 1-62f27b30-2bd9ab1860aae5647fe35c4b
x-spanid: ac32a264-11c4-4338-b045-7608c170233b
x-amzn-trace-id: Root=1-62f27b30-2bd9ab1860aae5647fe35c4b
x-content-type-options: nosniff
x-envoy-upstream-service-time: 593
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-08-09T08:20:16.259-07:00">
  <Fault type="ValidationFault">
    <Error code="6000" element="">
      <Message>A business validation error has occurred while processing your request</Message>
      <Detail>Business Validation Error: You cannot enter more than 31 characters in the sales_custom_1_val field. You tried entering CICC 7040-..., which is 40 characters.</Detail>
    </Error>
  </Fault>
</IntuitResponse>


