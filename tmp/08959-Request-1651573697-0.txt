REQUEST URI FOR SEQUENCE ID 08959
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..Q0GW3Szc_lGso9qJ88hPXA.Qh1aipyoalgD93AXu80tMau6VDHZ8VfYpMWH1z6w1qC5fg9E505ubevI6Fzzp9IL4YWhrn5lcqK_pVnYnfC4ajIM0E3CWWvrN-YWWVmeqfr0N0hcz-ika1odjEXb08EWwQCIkV8_zxvyz9fovS3zWX4PBWnOi9QJQMlqJJXtDQ24WQSJwDm2gzXKA5MDt8RDoYEqIbvti5h0U59uyXGq1kgWgX2IJ8_kTSMgwbIqxOFwmLi6X2EUk85_obBrJd8YpEJlaJnG6hNC_vfSw5cQmEp1AwFCu7sreczUzOONzr2JPlR2oX_aCDSIkA9jopRpABSzNTh0Y-yWKxCctyXZwcmIFeQLUdIdzDRwGD4_x1sjMd_x7qfaz4shmpQVxWt6qKQ2fmYgN3JuW2QtOg0VI69SY8rgw7vUk9WlMjj5GAPEkEXgOGEd8LOvn0kmkIJy4azqUJpUjYDi3tN7AdxXGp0D53HWK3iPbAE8gBpWaKh2AppJWEWqxIzhTwNXm0jQCXrFsnt7ItzktHv9u1hfOMkyaYV6PLxpcw-_p0Ugo-Ho8hnap6WBXJ7J4W72NXlfw255VYEXOTqqoAprlO3J2j8j-rPK_VyTtE5iAO97mIqjJq61S_LivPk9qb2Jo3YgAuHn8uWLpPeAFx57Rn3xrGUsyBFZ66q8BcI8aKaABg4S3Y_S8edsd5YWAQ7per0fm1eLRWWDHcSEudYzB1gAc-07zJKc8ESWl58NEqy2z8g.hSMG0D2Qk9MrWGqshjN_TA
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 2798

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>CICC 7040-0/07 Contract- RPQ NO: X127A-R</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>4250 NW 20 ST MIAMI FL 33126</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>513_1651573696</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - QUALITY CONSTRUCTION PERFORMANCE , INC.</ns0:Description>
    <ns0:Amount>30</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>30</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Rush Fee</ns0:Description>
    <ns0:Amount>8.50</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Rush Hour Charges">33</ns0:ItemRef>
      <ns0:UnitPrice>8.50</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Firm Mail</ns0:Description>
    <ns0:Amount>3.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Firm Mail">18</ns0:ItemRef>
      <ns0:UnitPrice>3.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail with Electronic Return Receipt</ns0:Description>
    <ns0:Amount>7.22</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Electronic Return Receipt">15</ns0:ItemRef>
      <ns0:UnitPrice>7.22</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>CES70191120000063846345-513</ns0:Description>
    <ns0:Amount>6.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Manual">19</ns0:ItemRef>
      <ns0:UnitPrice>6.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>195</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


