RESPONSE URI FOR SEQUENCE ID 12591
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

RESPONSE HEADERS
================
date: Thu, 28 Jul 2022 15:27:25 GMT
content-type: application/xml
content-length: 509
intuit_tid: 1-62e2aadd-2f20b2887d25027b53489e0f
x-spanid: b108463e-d206-4ca9-8a88-a2cfd77703b7
x-amzn-trace-id: Root=1-62e2aadd-2f20b2887d25027b53489e0f
x-content-type-options: nosniff
x-envoy-upstream-service-time: 540
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-07-28T08:27:25.180-07:00">
  <Fault type="ValidationFault">
    <Error code="6000" element="">
      <Message>A business validation error has occurred while processing your request</Message>
      <Detail>Business Validation Error: You cannot enter more than 31 characters in the sales_custom_1_val field. You tried entering NTO 342- c..., which is 46 characters.</Detail>
    </Error>
  </Fault>
</IntuitResponse>


