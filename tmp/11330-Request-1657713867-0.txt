REQUEST URI FOR SEQUENCE ID 11330
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..3AUe8eKclKtNuhhcnPqt6g.kbiraQ7-zTVL8A1saQxUnakLxR7GKPE2SoWhagnRJ-5Bh6vPityp0W0UDdMbk-BQwdZiNUUxecJTt96GHsCS1nWoOP8zAZ9dIBjGPJWNDTpTyfgsbQWAfcmI5LQL7x7qYrkwDkWsGETS_aaeCtne1di3YCjz5JKkTlahY1Nn4OSE0qvCu08amLnN2crukkdTX0xb142sXGs-XZiwsXGPRaM8UPyBdsN_fc38HqdZk2pRUGz5SrCPVTsqv6L4mln8Fn98rK0ofQu79CX2lrQEjv3IyinQ2z03YvnHXI1P86HVMpq4Mu_MQb8sOo_lUgr-yuo1VuafdrD-PAPRgKBQ4DnoVbJqxUEGKfFbQuo_qRIyHZH27gWz4E-grIt0_P_MxZEL-DuXw3P2_NGnWxT9_Nht2EM3e3oz4ypwzGgT4AeRtCNVZW1FcjX0t6BtDaSL0zk9SO4in5aWWntM6ZEXlxJ6ik_wL6SXSXfz8_z0UaNVhYBI_7jom4aldK6i9jmQk-NU8HfbUrY68iLPmumvvukOiHWcLjJUFBt47ehucn9vCpv0kTk7GKGW8Hw29iqmZzW6V33HePBui679OIjzPXA6HNHETSMYSb9qWwKVe3pUPLnh5oGbdEozagt-qQFDe8W-xcZfyQPfhQKXBDZKbYcJtWLnG1Hwc6do-avrg8QxwEDwFO_E1oS8xmY0sLdgveqs0zEq3o6geR5thaf6h6NXenxHYf_NOB9V8VPqBuk.lrIbv5-3A3t2CXFIC1xOkg
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 2798

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>CICC 7040-0/07 Contract- RPQ NO: X127A-R</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>4250 NW 20 ST MIAMI FL 33126</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>513_1657713867</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - QUALITY CONSTRUCTION PERFORMANCE , INC.</ns0:Description>
    <ns0:Amount>30</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>30</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Rush Fee</ns0:Description>
    <ns0:Amount>8.50</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Rush Hour Charges">33</ns0:ItemRef>
      <ns0:UnitPrice>8.50</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Firm Mail</ns0:Description>
    <ns0:Amount>3.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Firm Mail">18</ns0:ItemRef>
      <ns0:UnitPrice>3.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail with Electronic Return Receipt</ns0:Description>
    <ns0:Amount>7.22</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Electronic Return Receipt">15</ns0:ItemRef>
      <ns0:UnitPrice>7.22</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>CES70191120000063846345-513</ns0:Description>
    <ns0:Amount>6.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Manual">19</ns0:ItemRef>
      <ns0:UnitPrice>6.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>195</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


