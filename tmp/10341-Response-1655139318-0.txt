RESPONSE URI FOR SEQUENCE ID 10341
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

RESPONSE HEADERS
================
date: Mon, 13 Jun 2022 16:55:18 GMT
content-type: application/xml
content-length: 509
intuit_tid: 1-62a76bf6-5618b8c15083ee901fe679ea
x-spanid: a0db5654-31f1-44aa-be16-282f1ad4e889
x-amzn-trace-id: Root=1-62a76bf6-5618b8c15083ee901fe679ea
x-content-type-options: nosniff
x-envoy-upstream-service-time: 589
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-06-13T09:55:18.310-07:00">
  <Fault type="ValidationFault">
    <Error code="6000" element="">
      <Message>A business validation error has occurred while processing your request</Message>
      <Detail>Business Validation Error: You cannot enter more than 31 characters in the sales_custom_1_val field. You tried entering NTO 342- c..., which is 46 characters.</Detail>
    </Error>
  </Fault>
</IntuitResponse>


