RESPONSE URI FOR SEQUENCE ID 05484
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

RESPONSE HEADERS
================
date: Thu, 10 Mar 2022 16:48:46 GMT
content-type: application/xml;charset=UTF-8
content-length: 3797
intuit_tid: 1-622a2bed-28e411892351a58f1901e9e7
x-spanid: 5dff9684-e2d5-4d11-8a66-a6c41148a884
x-amzn-trace-id: Root=1-622a2bed-28e411892351a58f1901e9e7
x-content-type-options: nosniff
x-xss-protection: 1; mode=block
qbo-version: 1976.152
service-time: total=636, db=264
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private
x-envoy-upstream-service-time: 666
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-03-10T08:48:45.523-08:00">
  <Invoice domain="QBO" sparse="false">
    <Id>618</Id>
    <SyncToken>0</SyncToken>
    <MetaData>
      <CreateTime>2022-03-10T08:48:46-08:00</CreateTime>
      <LastUpdatedTime>2022-03-10T08:48:46-08:00</LastUpdatedTime>
    </MetaData>
    <CustomField>
      <DefinitionId>2</DefinitionId>
      <Type>StringType</Type>
      <StringValue>Notice to Owner/Preliminary No</StringValue>
    </CustomField>
    <CustomField>
      <DefinitionId>3</DefinitionId>
      <Type>StringType</Type>
      <StringValue>12999 OLD CUTLER ROAD PINECRES</StringValue>
    </CustomField>
    <DocNumber>306_1646930925</DocNumber>
    <TxnDate>2022-03-10</TxnDate>
    <CurrencyRef name="United States Dollar">USD</CurrencyRef>
    <Line>
      <Id>1</Id>
      <LineNum>1</LineNum>
      <Description>Contracted By - LUIS SOMOANO</Description>
      <Amount>45.00</Amount>
      <DetailType>SalesItemLineDetail</DetailType>
      <SalesItemLineDetail>
        <ItemRef name="SERVICES:NTO">32</ItemRef>
        <UnitPrice>45</UnitPrice>
        <Qty>1</Qty>
        <ItemAccountRef name="Services">1</ItemAccountRef>
        <TaxCodeRef>TAX</TaxCodeRef>
        <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
      </SalesItemLineDetail>
    </Line>
    <Line>
      <Id>2</Id>
      <LineNum>2</LineNum>
      <Description>Firm Mail</Description>
      <Amount>3.95</Amount>
      <DetailType>SalesItemLineDetail</DetailType>
      <SalesItemLineDetail>
        <ItemRef name="MAILING:Firm Mail">18</ItemRef>
        <UnitPrice>3.95</UnitPrice>
        <Qty>1</Qty>
        <ItemAccountRef name="Services">1</ItemAccountRef>
        <TaxCodeRef>TAX</TaxCodeRef>
        <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
      </SalesItemLineDetail>
    </Line>
    <Line>
      <Id>3</Id>
      <LineNum>3</LineNum>
      <Description>Certified Mail with Electronic Return Receipt</Description>
      <Amount>7.22</Amount>
      <DetailType>SalesItemLineDetail</DetailType>
      <SalesItemLineDetail>
        <ItemRef name="MAILING:Certified Mail ESRR">15</ItemRef>
        <UnitPrice>7.22</UnitPrice>
        <Qty>1</Qty>
        <ItemAccountRef name="Services">1</ItemAccountRef>
        <TaxCodeRef>TAX</TaxCodeRef>
        <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
      </SalesItemLineDetail>
    </Line>
    <Line>
      <Amount>56.17</Amount>
      <DetailType>SubTotalLineDetail</DetailType>
      <SubTotalLineDetail/>
    </Line>
    <TxnTaxDetail>
      <TxnTaxCodeRef>3</TxnTaxCodeRef>
      <TotalTax>3.93</TotalTax>
      <TaxLine>
        <Amount>3.37</Amount>
        <DetailType>TaxLineDetail</DetailType>
        <TaxLineDetail>
          <TaxRateRef>3</TaxRateRef>
          <PercentBased>true</PercentBased>
          <TaxPercent>6</TaxPercent>
          <NetAmountTaxable>56.17</NetAmountTaxable>
        </TaxLineDetail>
      </TaxLine>
      <TaxLine>
        <Amount>0.56</Amount>
        <DetailType>TaxLineDetail</DetailType>
        <TaxLineDetail>
          <TaxRateRef>4</TaxRateRef>
          <PercentBased>true</PercentBased>
          <TaxPercent>1</TaxPercent>
          <NetAmountTaxable>56.17</NetAmountTaxable>
        </TaxLineDetail>
      </TaxLine>
    </TxnTaxDetail>
    <CustomerRef name="CLN GROUP LLC_1645988421">265</CustomerRef>
    <BillAddr>
      <Id>650</Id>
      <Line1>11767 S DIXIE HIGHWAY STE 224</Line1>
      <City>Pinecrest</City>
      <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
      <PostalCode>33156</PostalCode>
    </BillAddr>
    <ShipAddr>
      <Id>651</Id>
      <Line1>11767 S DIXIE HIGHWAY STE 224</Line1>
      <City>Pinecrest</City>
      <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
      <PostalCode>33156</PostalCode>
    </ShipAddr>
    <FreeFormAddress>true</FreeFormAddress>
    <ShipFromAddr>
      <Id>813</Id>
      <Line1>PO Box 22821</Line1>
      <Line2>Hialeah, FL  33002 US</Line2>
    </ShipFromAddr>
    <SalesTermRef name="Net 15">2</SalesTermRef>
    <DueDate>2022-03-25</DueDate>
    <TotalAmt>60.10</TotalAmt>
    <ApplyTaxAfterDiscount>false</ApplyTaxAfterDiscount>
    <PrintStatus>NeedToPrint</PrintStatus>
    <EmailStatus>NotSet</EmailStatus>
    <Balance>60.10</Balance>
    <TaxExemptionRef/>
    <AllowIPNPayment>false</AllowIPNPayment>
    <AllowOnlinePayment>false</AllowOnlinePayment>
    <AllowOnlineCreditCardPayment>false</AllowOnlineCreditCardPayment>
    <AllowOnlineACHPayment>false</AllowOnlineACHPayment>
  </Invoice>
</IntuitResponse>


