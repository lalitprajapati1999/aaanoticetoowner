RESPONSE URI FOR SEQUENCE ID 03520
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

RESPONSE HEADERS
================
date: Thu, 24 Feb 2022 17:45:37 GMT
content-type: application/xml;charset=UTF-8
content-length: 3777
intuit_tid: 1-6217c440-635fe7df1feb619c4e2458c3
x-spanid: 87eb81ad-6541-41ef-841c-2e5b0d1e76e4
x-amzn-trace-id: Root=1-6217c440-635fe7df1feb619c4e2458c3
x-content-type-options: nosniff
x-xss-protection: 1; mode=block
qbo-version: 1975.230
service-time: total=376, db=115
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private
x-envoy-upstream-service-time: 404
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-02-24T09:45:36.819-08:00">
  <Invoice domain="QBO" sparse="false">
    <Id>394</Id>
    <SyncToken>0</SyncToken>
    <MetaData>
      <CreateTime>2022-02-24T09:45:37-08:00</CreateTime>
      <LastUpdatedTime>2022-02-24T09:45:37-08:00</LastUpdatedTime>
    </MetaData>
    <CustomField>
      <DefinitionId>2</DefinitionId>
      <Type>StringType</Type>
      <StringValue>Notice to Owner/Preliminary No</StringValue>
    </CustomField>
    <CustomField>
      <DefinitionId>3</DefinitionId>
      <Type>StringType</Type>
      <StringValue>2730 SW 90 AVENUE MIAMI FL</StringValue>
    </CustomField>
    <DocNumber>275_1645724736</DocNumber>
    <TxnDate>2022-02-24</TxnDate>
    <CurrencyRef name="United States Dollar">USD</CurrencyRef>
    <Line>
      <Id>1</Id>
      <LineNum>1</LineNum>
      <Description>Contracted By - DAMIAN FERNANDEZ</Description>
      <Amount>45.00</Amount>
      <DetailType>SalesItemLineDetail</DetailType>
      <SalesItemLineDetail>
        <ItemRef name="SERVICES:NTO">32</ItemRef>
        <UnitPrice>45</UnitPrice>
        <Qty>1</Qty>
        <ItemAccountRef name="Services">1</ItemAccountRef>
        <TaxCodeRef>TAX</TaxCodeRef>
        <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
      </SalesItemLineDetail>
    </Line>
    <Line>
      <Id>2</Id>
      <LineNum>2</LineNum>
      <Description>Firm Mail</Description>
      <Amount>3.95</Amount>
      <DetailType>SalesItemLineDetail</DetailType>
      <SalesItemLineDetail>
        <ItemRef name="MAILING:Firm Mail">18</ItemRef>
        <UnitPrice>3.95</UnitPrice>
        <Qty>1</Qty>
        <ItemAccountRef name="Services">1</ItemAccountRef>
        <TaxCodeRef>TAX</TaxCodeRef>
        <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
      </SalesItemLineDetail>
    </Line>
    <Line>
      <Id>3</Id>
      <LineNum>3</LineNum>
      <Description>Certified Mail with Electronic Return Receipt</Description>
      <Amount>7.22</Amount>
      <DetailType>SalesItemLineDetail</DetailType>
      <SalesItemLineDetail>
        <ItemRef name="MAILING:Certified Mail ESRR">15</ItemRef>
        <UnitPrice>7.22</UnitPrice>
        <Qty>1</Qty>
        <ItemAccountRef name="Services">1</ItemAccountRef>
        <TaxCodeRef>TAX</TaxCodeRef>
        <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
      </SalesItemLineDetail>
    </Line>
    <Line>
      <Amount>56.17</Amount>
      <DetailType>SubTotalLineDetail</DetailType>
      <SubTotalLineDetail/>
    </Line>
    <TxnTaxDetail>
      <TxnTaxCodeRef>3</TxnTaxCodeRef>
      <TotalTax>3.93</TotalTax>
      <TaxLine>
        <Amount>3.37</Amount>
        <DetailType>TaxLineDetail</DetailType>
        <TaxLineDetail>
          <TaxRateRef>3</TaxRateRef>
          <PercentBased>true</PercentBased>
          <TaxPercent>6</TaxPercent>
          <NetAmountTaxable>56.17</NetAmountTaxable>
        </TaxLineDetail>
      </TaxLine>
      <TaxLine>
        <Amount>0.56</Amount>
        <DetailType>TaxLineDetail</DetailType>
        <TaxLineDetail>
          <TaxRateRef>4</TaxRateRef>
          <PercentBased>true</PercentBased>
          <TaxPercent>1</TaxPercent>
          <NetAmountTaxable>56.17</NetAmountTaxable>
        </TaxLineDetail>
      </TaxLine>
    </TxnTaxDetail>
    <CustomerRef name="GLOBAL ELECTRIC SERVICES, LLC_1644967258">260</CustomerRef>
    <BillAddr>
      <Id>601</Id>
      <Line1>15905 SW 105 CT</Line1>
      <City>Miami</City>
      <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
      <PostalCode>33157</PostalCode>
    </BillAddr>
    <ShipAddr>
      <Id>602</Id>
      <Line1>15905 SW 105 CT</Line1>
      <City>Miami</City>
      <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
      <PostalCode>33157</PostalCode>
    </ShipAddr>
    <FreeFormAddress>true</FreeFormAddress>
    <ShipFromAddr>
      <Id>643</Id>
      <Line1>PO Box 22821</Line1>
      <Line2>Hialeah, FL  33002 US</Line2>
    </ShipFromAddr>
    <SalesTermRef name="Net 15">2</SalesTermRef>
    <DueDate>2022-03-11</DueDate>
    <TotalAmt>60.10</TotalAmt>
    <ApplyTaxAfterDiscount>false</ApplyTaxAfterDiscount>
    <PrintStatus>NeedToPrint</PrintStatus>
    <EmailStatus>NotSet</EmailStatus>
    <Balance>60.10</Balance>
    <TaxExemptionRef/>
    <AllowIPNPayment>false</AllowIPNPayment>
    <AllowOnlinePayment>false</AllowOnlinePayment>
    <AllowOnlineCreditCardPayment>false</AllowOnlineCreditCardPayment>
    <AllowOnlineACHPayment>false</AllowOnlineACHPayment>
  </Invoice>
</IntuitResponse>


