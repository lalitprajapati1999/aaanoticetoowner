RESPONSE URI FOR SEQUENCE ID 00934
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/customer?minorversion=47

RESPONSE HEADERS
================
date: Thu, 18 Nov 2021 15:06:09 GMT
content-type: application/xml;charset=UTF-8
content-length: 1374
intuit_tid: 1-61966be1-3c2baf63371621b23ef15acd
x-spanid: 97c9e77d-3fb5-44ea-9c09-12e54bd9e4c3
x-amzn-trace-id: Root=1-61966be1-3c2baf63371621b23ef15acd
x-content-type-options: nosniff
qbo-version: 1969.129
service-time: total=246, db=60
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private
x-envoy-upstream-service-time: 283
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2021-11-18T07:06:09.167-08:00">
  <Customer domain="QBO" sparse="false">
    <Id>222</Id>
    <SyncToken>0</SyncToken>
    <MetaData>
      <CreateTime>2021-11-18T07:06:09-08:00</CreateTime>
      <LastUpdatedTime>2021-11-18T07:06:09-08:00</LastUpdatedTime>
    </MetaData>
    <FullyQualifiedName>Test_1637247968</FullyQualifiedName>
    <CompanyName>Test</CompanyName>
    <DisplayName>Test_1637247968</DisplayName>
    <PrintOnCheckName>Test</PrintOnCheckName>
    <Active>true</Active>
    <PrimaryPhone>
      <FreeFormNumber>(123) 435-4545</FreeFormNumber>
    </PrimaryPhone>
    <PrimaryEmailAddr>
      <Address>kajalwaykole@gmail.com</Address>
    </PrimaryEmailAddr>
    <DefaultTaxCodeRef>3</DefaultTaxCodeRef>
    <Taxable>true</Taxable>
    <BillAddr>
      <Id>409</Id>
      <Line1>Test address</Line1>
      <City>Fabius</City>
      <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
      <PostalCode>31103</PostalCode>
    </BillAddr>
    <ShipAddr>
      <Id>410</Id>
      <Line1>Test address</Line1>
      <City>Fabius</City>
      <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
      <PostalCode>31103</PostalCode>
    </ShipAddr>
    <Job>false</Job>
    <BillWithParent>false</BillWithParent>
    <Balance>0</Balance>
    <BalanceWithJobs>0</BalanceWithJobs>
    <CurrencyRef name="United States Dollar">USD</CurrencyRef>
    <PreferredDeliveryMethod>None</PreferredDeliveryMethod>
    <IsProject>false</IsProject>
  </Customer>
</IntuitResponse>


