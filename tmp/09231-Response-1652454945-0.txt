RESPONSE URI FOR SEQUENCE ID 09231
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/query?minorversion=47

RESPONSE HEADERS
================
date: Fri, 13 May 2022 15:15:45 GMT
content-type: application/xml;charset=UTF-8
content-length: 3924
intuit_tid: 1-627e7621-493f87931b7da13d41d9052b
x-spanid: 5157ae4c-8714-43d1-b300-40edaffe561e
x-amzn-trace-id: Root=1-627e7621-493f87931b7da13d41d9052b
x-content-type-options: nosniff
x-xss-protection: 1; mode=block
qbo-version: 1980.233
service-time: total=379, db=283
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private
x-envoy-upstream-service-time: 415
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-05-13T08:15:45.566-07:00">
  <QueryResponse startPosition="1" maxResults="1" totalCount="1">
    <Invoice domain="QBO" sparse="false">
      <Id>1596</Id>
      <SyncToken>0</SyncToken>
      <MetaData>
        <CreateTime>2022-05-13T08:12:44-07:00</CreateTime>
        <LastUpdatedTime>2022-05-13T08:12:44-07:00</LastUpdatedTime>
      </MetaData>
      <CustomField>
        <DefinitionId>2</DefinitionId>
        <Name>sales2</Name>
        <Type>StringType</Type>
        <StringValue>Notice to Owner/Preliminary No</StringValue>
      </CustomField>
      <CustomField>
        <DefinitionId>3</DefinitionId>
        <Name>sales3</Name>
        <Type>StringType</Type>
        <StringValue>7145 ABBOTT AVE MIAMI BEACH FL</StringValue>
      </CustomField>
      <DocNumber>583_1652454763</DocNumber>
      <TxnDate>2022-05-13</TxnDate>
      <CurrencyRef name="United States Dollar">USD</CurrencyRef>
      <Line>
        <Id>1</Id>
        <LineNum>1</LineNum>
        <Description>Contracted By - PORTAL CONSTRUCTION INC</Description>
        <Amount>45.00</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="SERVICES:NTO">32</ItemRef>
          <UnitPrice>45</UnitPrice>
          <Qty>1</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>NON</TaxCodeRef>
          <TaxClassificationRef>EUC-99990201-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Id>2</Id>
        <LineNum>2</LineNum>
        <Description>Firm Mail</Description>
        <Amount>3.95</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="MAILING:Firm Mail">18</ItemRef>
          <UnitPrice>3.95</UnitPrice>
          <Qty>1</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>TAX</TaxCodeRef>
          <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Id>3</Id>
        <LineNum>3</LineNum>
        <Description>Certified Mail with Electronic Return Receipt</Description>
        <Amount>14.44</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="MAILING:Certified Mail ESRR">15</ItemRef>
          <UnitPrice>7.22</UnitPrice>
          <Qty>2</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>TAX</TaxCodeRef>
          <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Amount>63.39</Amount>
        <DetailType>SubTotalLineDetail</DetailType>
        <SubTotalLineDetail/>
      </Line>
      <TxnTaxDetail>
        <TxnTaxCodeRef>3</TxnTaxCodeRef>
        <TotalTax>1.29</TotalTax>
        <TaxLine>
          <Amount>1.11</Amount>
          <DetailType>TaxLineDetail</DetailType>
          <TaxLineDetail>
            <TaxRateRef>3</TaxRateRef>
            <PercentBased>true</PercentBased>
            <TaxPercent>6</TaxPercent>
            <NetAmountTaxable>18.39</NetAmountTaxable>
          </TaxLineDetail>
        </TaxLine>
        <TaxLine>
          <Amount>0.18</Amount>
          <DetailType>TaxLineDetail</DetailType>
          <TaxLineDetail>
            <TaxRateRef>4</TaxRateRef>
            <PercentBased>true</PercentBased>
            <TaxPercent>1</TaxPercent>
            <NetAmountTaxable>18.39</NetAmountTaxable>
          </TaxLineDetail>
        </TaxLine>
      </TxnTaxDetail>
      <CustomerRef name="DR ELECTRIC SERVICES CORP_1652365449">283</CustomerRef>
      <BillAddr>
        <Id>1138</Id>
        <Line1>8355 W FLAGER ST STE 326</Line1>
        <City>Miami</City>
        <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
        <PostalCode>33144</PostalCode>
      </BillAddr>
      <ShipAddr>
        <Id>1139</Id>
        <Line1>8355 W FLAGER ST STE 326</Line1>
        <City>Miami</City>
        <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
        <PostalCode>33144</PostalCode>
      </ShipAddr>
      <FreeFormAddress>true</FreeFormAddress>
      <ShipFromAddr>
        <Id>1145</Id>
        <Line1>PO Box 22821</Line1>
        <Line2>Hialeah, FL  33002 US</Line2>
      </ShipFromAddr>
      <SalesTermRef name="Net 15">2</SalesTermRef>
      <DueDate>2022-05-28</DueDate>
      <TotalAmt>64.68</TotalAmt>
      <ApplyTaxAfterDiscount>false</ApplyTaxAfterDiscount>
      <PrintStatus>NeedToPrint</PrintStatus>
      <EmailStatus>NotSet</EmailStatus>
      <Balance>64.68</Balance>
      <TaxExemptionRef/>
      <AllowIPNPayment>false</AllowIPNPayment>
      <AllowOnlinePayment>false</AllowOnlinePayment>
      <AllowOnlineCreditCardPayment>false</AllowOnlineCreditCardPayment>
      <AllowOnlineACHPayment>false</AllowOnlineACHPayment>
    </Invoice>
  </QueryResponse>
</IntuitResponse>


