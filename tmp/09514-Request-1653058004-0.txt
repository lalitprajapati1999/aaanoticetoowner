REQUEST URI FOR SEQUENCE ID 09514
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0.._otTLQOGKVpeQbQOBanCtQ.bRBhkkGKRBQXybuXdWj8if4VaYCpZ2w0YpfZ32a4zSmqGTIC2ODSicd1beJtmdLeusAYSWZr4EKKtNACyI67zvyTswhzIrDhaDYgeCiR28iO1UxgVtcCOTqzIKGOssmmOnEKfxlRdGH2nbX6NPHKt5bsnGQ6l03lJeB3R0ayv5yURW5NvgLeBW2Zblp9Ic3dgrtqjCLUlOSr5RyV29aju6o__2oRGvYOMIljmBEMr-bs9EOZcX3BPpGXpZJryBqlaVHD0chWFvYoTVTLNdSodaC2qMpUXkxccs64tUwN2AvycZzNvGGx498lWfTVSbHL_QTt92YE-sZmyUFJPglBD6xWWFoOzdVyM0I6AjJw9m6O_no6yAefg6wHqPXF7yWFq_XNew65RcMcjyFB31uQ8FWIH3BkxHRCmk11wECfvHlnvvzBP9-qzBHEaN9Q1CMYLNRJDnlG_63wmRUVtAftm8xVjWR8KETez1kNlhIfLsYzah5aMS5zaakTm1naq5vICjAzv7jRS092dRBSCWOtq-Oj985JhbFVXlbn16mnebrRZ7-q9yYdWT-nVs8e5StRAZ8L9s7adjPflt98ByjgCr9GYbZzZfZXAN0lSj6FM1H6fyvoxwyt38XcsU71sWOnJ_LM7GhF-wxlalyCI7CwCGszZP25iFJp1_cTw2NLyynK8F5DUR-Q31c-KGqjNNfYQCImfw3aDAOHqLi-WL-7F_K5P4Jdv-Xj5AvlWYvUexw.kNQoDfENGxemCfmdgWC3VQ
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 2703

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>11251 NW 20 ST  UNIT#114  DORA</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>584_1653058004</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - PMB CONSTRUCTION INC.</ns0:Description>
    <ns0:Amount>20.00</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>20.00</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Rush Fee</ns0:Description>
    <ns0:Amount>8.50</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Rush Hour Charges">33</ns0:ItemRef>
      <ns0:UnitPrice>8.50</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Firm Mail</ns0:Description>
    <ns0:Amount>3.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Firm Mail">18</ns0:ItemRef>
      <ns0:UnitPrice>3.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail</ns0:Description>
    <ns0:Amount>5.42</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Certified Mail">17</ns0:ItemRef>
      <ns0:UnitPrice>5.42</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail with Electronic Return Receipt</ns0:Description>
    <ns0:Amount>7.22</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Electronic Return Receipt">15</ns0:ItemRef>
      <ns0:UnitPrice>7.22</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>195</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


