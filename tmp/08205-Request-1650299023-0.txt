REQUEST URI FOR SEQUENCE ID 08205
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..OIUDy1CbkaI_vtlYvaZTXQ.i-j51zPlIqd9NoTD7N5vxxyGHe9ow4UctG6hz7dchG8Zi1_OPUgsKPoNWogN1DD_Rzs8tyv5lxUffAIqCgWqTJoRU3Z8dWV3xqtoeGr2N3MjRbJJ3Bwuw-M6PraIo3aN0gAzZ4h_cOrgqJ0HcjIwTY0f8ReJQbIsUjKDP1HI8YbJw0MFAkwQMpvEwp24nDLMYwAXr0EOQ6rS4euxYu7IkV-qV6497a4XZ9y9-el2j-LeBxNyuXVbIEBK9Iw17OfXLkW9zb8Yz-larqj7fnfSsZh5C5ON-TP55faUxVS0SDAhgXUk9LgvstUQf6RISBJAxEbwkrs6MJ1NOBjMNc6q2U9f7_K2ZPtkOrG74XCU1os0ZQ9uxraXz_x8RlKDkvBa2eJCDRC5pifGwx45lIX90o1s2mUMf_HxxBGfBgTBvrIDS8eEb1UXDuBr8hzi1QEYp5aJKMeAd1ZW4YOooLtZ5tkn2OvUr6I3mKEkhYgUOs09f_SitiChTGseYz661PGwMKF7BxED0ZW8XidhDYznVqzQD8BYPNftpilFAdyykYn4NkAYuygBxD7zTBWoCfJik11vpzYW9M6iug4fme2dhlch23ANh09HyCd0fv387TMpou6vSdj-9CinXNo9q7cWE_lU8AJRi2C_Znf_0rjnDR2FOstE-lASCYmA2Q9TGpaRUk-rMJwC42NNctcokzUQYLmyL8xNY8c_O_Z8xM3AIe46ieDQkAjBINzv86PAKNo.UJqLdpn53DanbAxLzDdIEg
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 2778

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>NTO 342- correction  recipient: G7 Holding LLC</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>5860 N BAY ROAD MIAMI BEACH 33</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>380_1650299023</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - POWER PLUMBING</ns0:Description>
    <ns0:Amount>35</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>35</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Rush Fee</ns0:Description>
    <ns0:Amount>12.50</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Rush Hour Charges">33</ns0:ItemRef>
      <ns0:UnitPrice>12.50</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Firm Mail</ns0:Description>
    <ns0:Amount>3.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Firm Mail">18</ns0:ItemRef>
      <ns0:UnitPrice>3.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail</ns0:Description>
    <ns0:Amount>16.26</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Certified Mail">17</ns0:ItemRef>
      <ns0:UnitPrice>5.42</ns0:UnitPrice>
      <ns0:Qty>3</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail with Electronic Return Receipt</ns0:Description>
    <ns0:Amount>7.22</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Electronic Return Receipt">15</ns0:ItemRef>
      <ns0:UnitPrice>7.22</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>36</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


