REQUEST URI FOR SEQUENCE ID 03711
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/customer?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..WLG8jxz_RqzXGPyyN9Q5Zg.mjOtVQRpho1CxGUK1WLRuhitOFoME0Ylbg-oCdbf0CR464xwn0m9KgcwwYcJ_Z7A59jAkCAVXGHsTKG6KweEaY4fV-Cv5ipM01T_YKlXI_uaSoKssiqES4D3R2TpzYpbm9h1PWL6mEOlo8Du_vYa2bW33EnfiJzoc9Gdg8SPx7U9gPHbJDLE3rm-HXQEW-ZjPXi8ONrSJ1MJufn88ieIOD7Js0nMvDn9sK234zTeh-OWABw1tzBMqeDP7EkKQt3ItlstVYDLrkZsUdZgx7k5DmWgL2LasNJpIvrMMgDD8Jhkaj-105aSv630w_ZvNJyiI_vAiJiPPcnIgvTS7so03LMFA6gHi3VoXpTAip3W1AtTJhRfvmZUouuTPfQnjLiVX50Y40lPNxrYRw7DQdttvrurDkzK2PVlzejMdDgsD1-pjFuRi82ppl10eHYXQ09JzNSfr-CGZqz0lC72Sk3niYxFBhw3XHtKv-C3FUFQTX9dJftxAmANbJrvM87xUHFBMCO8ZqbTtMlByCQ5FkRwbk_OhdflqZ70pdSiJDwvVV66bcv9KApOhwoK5FwlWUD-RhkGksKo8LVtxCgu8O553Vm7kjx_07w_Q6pgIYla29pREy2XzmI5-Kg-wQSr2XtQEE2Ibj5KumYkTC2u4-ubg-EMmC7SixDV_QPEIvMtpNcTDHg7s7Zucm9qIrJyCVkx8goY5RbsRQQyoV0jWNNrgzBay1261QycdOLdGOiWAQI.RWVlQdC2l75mhCxbzaSxag
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 1007

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Customer xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:FullyQualifiedName>Mr Wood Custom Floors, Inc</ns0:FullyQualifiedName>
  <ns0:CompanyName>Mr Wood Custom Floors, Inc</ns0:CompanyName>
  <ns0:DisplayName>Mr Wood Custom Floors, Inc_1646324085</ns0:DisplayName>
  <ns0:PrimaryPhone>
    <ns0:FreeFormNumber>(305) 917-5858</ns0:FreeFormNumber>
  </ns0:PrimaryPhone>
  <ns0:PrimaryEmailAddr>
    <ns0:Address>mrwoodfloors@yahoo.com</ns0:Address>
  </ns0:PrimaryEmailAddr>
  <ns0:BillAddr>
    <ns0:Line1>1925 NE 150th Street</ns0:Line1>
    <ns0:City>North miami</ns0:City>
    <ns0:CountrySubDivisionCode>Florida</ns0:CountrySubDivisionCode>
    <ns0:PostalCode>33181</ns0:PostalCode>
  </ns0:BillAddr>
  <ns0:ShipAddr>
    <ns0:Line1>1925 NE 150th Street</ns0:Line1>
    <ns0:City>North miami</ns0:City>
    <ns0:CountrySubDivisionCode>Florida</ns0:CountrySubDivisionCode>
    <ns0:PostalCode>33181</ns0:PostalCode>
  </ns0:ShipAddr>
</ns0:Customer>


