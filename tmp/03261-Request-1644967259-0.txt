REQUEST URI FOR SEQUENCE ID 03261
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/customer?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..HvJu66eN5M7Y3lrWUcJddg.UpfeBkrM7YUG8j7F0D5WwoSSo8NBQAnPsAIy0Ght_OvduxQouRX-17tc3yxHvkEDyi0DsOMRZhuxs1BsgbmZqI0Qzx8rx8AOg8yulEsYDi1GAp8GsQOZoIe32_H2VdGOpjauIHDlMfcyDDxJI8STFFkIGchP5J34mplTCPQdbIEvAQm21PNY1rIKQHqzN45PYAj-kKL-I1zfjU1uplOjTTu_G6I_57nbvar4kOdUmIZaONbBYVC8kDHPNiJn9qzJL-NR1aWVzXcg_7EfY7d9qYYBWjNREOsI17i02faYOz_LlekvkIAi1SctcTsxUKaCu0t7uAevecnInOj3XqHFRa76Xx0ECrciST1VxMazb9bDln4y9ONyDpCS1L6LYIWcRh0Hr9zmY7bwO_O5QceQpF3TL01FNWGCY8ZdsYm0kWKc7KyDyLyX0_KOW48CJrub1rk5lSnJ-4h-WHh9ph69Z3b0_N6U1OvHhOQE3SLxt6NFF1jwkRCthOxNMIsJfv_eE9-scKk4CC1DOSWJHjukOFB-a2fo0lKxnWJOav5s28HPtM_PQhe07jKPqrRRQq8RhANM8tKsIY7ZiHEGVOYCeuJjTaSSfZRz9igcFBvgbUlvNXEUzQT9pbn3BCZI6LBVFFhWZ6Pt_zHjS1uh95aUXLynyg7UIgKqKLBMJaCj5CQ-M8OaVEk8IrbZvGqcHat2_NWk-5bz26kvRERDm2R4o13j2thY5EoKHW6NaUPfq4U.hW7wSJSUMth-rEgoP8JkRQ
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 990

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Customer xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:FullyQualifiedName>GLOBAL ELECTRIC SERVICES, LLC</ns0:FullyQualifiedName>
  <ns0:CompanyName>GLOBAL ELECTRIC SERVICES, LLC</ns0:CompanyName>
  <ns0:DisplayName>GLOBAL ELECTRIC SERVICES, LLC_1644967258</ns0:DisplayName>
  <ns0:PrimaryPhone>
    <ns0:FreeFormNumber>(305) 219-0028</ns0:FreeFormNumber>
  </ns0:PrimaryPhone>
  <ns0:PrimaryEmailAddr>
    <ns0:Address>osmanigm@yahoo.com</ns0:Address>
  </ns0:PrimaryEmailAddr>
  <ns0:BillAddr>
    <ns0:Line1>15905 SW 105 CT</ns0:Line1>
    <ns0:City>Miami</ns0:City>
    <ns0:CountrySubDivisionCode>Florida</ns0:CountrySubDivisionCode>
    <ns0:PostalCode>33157</ns0:PostalCode>
  </ns0:BillAddr>
  <ns0:ShipAddr>
    <ns0:Line1>15905 SW 105 CT</ns0:Line1>
    <ns0:City>Miami</ns0:City>
    <ns0:CountrySubDivisionCode>Florida</ns0:CountrySubDivisionCode>
    <ns0:PostalCode>33157</ns0:PostalCode>
  </ns0:ShipAddr>
</ns0:Customer>


