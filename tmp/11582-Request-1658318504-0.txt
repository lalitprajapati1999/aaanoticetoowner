REQUEST URI FOR SEQUENCE ID 11582
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..O9h68yf_lGl79QFORqtA9g.-ATxAfP9j5KnUBqRGKem2iNmJYIytHnXLxBssovPE-pKQRlgPtgiRnU6SBmao1xv2eOzLCokJGVo32nJ3ISG--VaPy11FrhSOGVxWEwv7-n_boOnAG6F2IZBDjABtc8m0uKgVaDqvq-oPl5-HR3mHLKPTAzxhqumg2XNg_XCpsXwmPWmrhjSSdon_l8rKfGh7xQ50spdSJwWmXo1qHxI7hiUFm_C0TKrvkgzPXwk-kYfQVmuRtg_ghtJKCRWG_qnxRdqZy1iAy_sXbNZ8b1tvpLKk0928rRLilW5OD7hj3YDUBAGyBVnPHDeSaj0dA8Kjg3cTeqNceFOdUHxiGq3pGlTXWfYkbZia4SwSmbuZD7sktEAEp1F-wDxh9YoqgfdJrimLhMIhToaWTlvHIAlOhUDvUR6Pb51Vz9IjsWwRvIMDyHTwg_MCmQMwf-7MzKgNrWgbUu5ZNU-xL1imPkX4lNSDa9yqFIN24eqmumCZa7RJ5QGMsB19a9t6LNAEvAD1yxVxTCFYAI9Clld00sAD5_5_Et9SKw7wBQesqNulMZ3D4t4dAkxKdRoYNdonXqWVy9GHmsZ933r_J6oDda3GP0RJMjJNaRaEDi77nRufXWk5c0SSOwkxcwGeegekUZJi6HEjwNPG3j_81p1t398Awh6o0UJNAh8rcqna7UsaOZEBFUtsPkWqI2rNk_9mJ7nXvK6Mw-Qq2-Q4j-AoUgaXUUXCy1eIkzcYOOwfDKyrEY.7MiX1HUCUz-Y1teDMIPWrQ
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 1627

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue> 2124lkjghgg </ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>127_CYO_1658318504</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Notice to Owner/Preliminary Notice
Contracted By - GARY GLENEWINKEL CONSTRUCTION CORP
07/11/2022</ns0:Description>
    <ns0:Amount>.01</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>.01</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Firm Mail</ns0:Description>
    <ns0:Amount>11.85</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Firm Mail">18</ns0:ItemRef>
      <ns0:UnitPrice>3.95</ns0:UnitPrice>
      <ns0:Qty>3</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>213</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


