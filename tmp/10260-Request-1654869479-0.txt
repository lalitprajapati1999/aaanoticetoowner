REQUEST URI FOR SEQUENCE ID 10260
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..ugUfm91Ulu3-k-FIW5Fjaw.T1OLUyZe5Jn1Q_u_qDP5s-AJAAEff1AbIRTmx2Kw-ciUkQaC7zHy1lC1jeEFHZCqYHK2FBSyhSQEKh7vzIau67SQA-Klkh-T2lpYgHGPOJ8NyMFP1lktvx8uLBAiZir2ZcRehOpRHlT_ejgV9HAb3pcl-QI6rzGR3LEOvPP7q8IEdhcvChKFOf3PBsBo0mjW7cvcHq1FvGG2XeoaQr0JSkl704Ejihiq5d4DGwTbhT-2cgya4J6VHThBZVr9l5kVS_pu_f7F518dm4naUssufO1PsWrcXtBKq28wwVLX6LXYbwBAGcNUN_FJXwQbNM_2n9c3LHIMeT34I48B5-6ZRhLs0bMIFPSwmxnqezjFkcyKvn6rpz0onqpQyz1TKCQAwxHadYfM4LbeTsHClESlZ4pKEFfBNL2VSCW9czdzSJXkxk5BHI1y87b3_tA5wvVlYjLUullqw9S_0ORChd7t0WPMWE5AgEjtS4LNpBGHgM7_tHfVs4J9I7DpHfXGHGsxCnja64ikCDHxUeHekh-u4LkpxkLMRjoWuofHhmQ4hzGeD_SDR9YKKKYsIWjktjWF8jgh14pUs9uXBvYAmwFdI5rrh6_j_3AU3B3t3y3iMLiVyOaqxA8wd6FVcfjT9GIqCB1CW5-eDYDBqc--hSUnJ_SdvBhg3qTUsq5EIm2rycWzIukuNk6JFb12N6f-VRG4YF2kLp-RcfYyhJNM54qmPgHYnbgn1IJbqXb8FLf0UDE.MJYbMR5jwJoS6Z3zLeSceQ
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 2804

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>CICC 7040-0/07 Contract- RPQ NO: X127A-R</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>4250 NW 20 ST MIAMI FL 33126</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>513_1654869479</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - QUALITY CONSTRUCTION PERFORMANCE , INC.</ns0:Description>
    <ns0:Amount>20.00</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>20.00</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Rush Fee</ns0:Description>
    <ns0:Amount>8.50</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Rush Hour Charges">33</ns0:ItemRef>
      <ns0:UnitPrice>8.50</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Firm Mail</ns0:Description>
    <ns0:Amount>3.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Firm Mail">18</ns0:ItemRef>
      <ns0:UnitPrice>3.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail with Electronic Return Receipt</ns0:Description>
    <ns0:Amount>7.22</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Electronic Return Receipt">15</ns0:ItemRef>
      <ns0:UnitPrice>7.22</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>CES70191120000063846345-513</ns0:Description>
    <ns0:Amount>6.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Manual">19</ns0:ItemRef>
      <ns0:UnitPrice>6.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>195</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


