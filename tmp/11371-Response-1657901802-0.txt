RESPONSE URI FOR SEQUENCE ID 11371
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

RESPONSE HEADERS
================
date: Fri, 15 Jul 2022 16:16:42 GMT
content-type: application/xml
content-length: 509
intuit_tid: 1-62d192e9-60085c4926a152a70a10bf59
x-spanid: fa5c9654-92ea-4caa-b179-82b217f3c1bb
x-amzn-trace-id: Root=1-62d192e9-60085c4926a152a70a10bf59
x-content-type-options: nosniff
x-envoy-upstream-service-time: 537
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-07-15T09:16:41.569-07:00">
  <Fault type="ValidationFault">
    <Error code="6000" element="">
      <Message>A business validation error has occurred while processing your request</Message>
      <Detail>Business Validation Error: You cannot enter more than 31 characters in the sales_custom_1_val field. You tried entering CICC 7040-..., which is 40 characters.</Detail>
    </Error>
  </Fault>
</IntuitResponse>


