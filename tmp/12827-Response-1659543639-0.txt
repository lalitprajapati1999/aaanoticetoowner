RESPONSE URI FOR SEQUENCE ID 12827
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

RESPONSE HEADERS
================
date: Wed, 03 Aug 2022 16:20:40 GMT
content-type: application/xml
content-length: 509
intuit_tid: 1-62eaa057-5ce90761522a6a9d2be8cdad
x-spanid: c986e21a-39d6-4dc0-94ed-4922add51260
x-amzn-trace-id: Root=1-62eaa057-5ce90761522a6a9d2be8cdad
x-content-type-options: nosniff
x-envoy-upstream-service-time: 395
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-08-03T09:20:39.832-07:00">
  <Fault type="ValidationFault">
    <Error code="6000" element="">
      <Message>A business validation error has occurred while processing your request</Message>
      <Detail>Business Validation Error: You cannot enter more than 31 characters in the sales_custom_1_val field. You tried entering NTO 342- c..., which is 46 characters.</Detail>
    </Error>
  </Fault>
</IntuitResponse>


