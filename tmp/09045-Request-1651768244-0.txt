REQUEST URI FOR SEQUENCE ID 09045
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..ODwBgfvYT83WVuRkb6BdBA.cG5XIhGwHX00FdZadPRYNbL3UF71GHMzFqxN1XmDPkD_du1tm7Y11BisRPua_m6v5IRqNeqP5auIrLcwwrKTP1S54TMr85k2MY0_RIj8E_0PNyjlARloZkqW_pInE7JMdlh90El4gkIQJkAxQvYHkxodeOYz50tIFk-dPdky0oOtUDTH_Fe6Gf43M-cCc4wGge7-KtpJq8PrUtsx388fAleXKzT96QSgdxJfeZzsMN4JnAPlh_NWg6E6e9uUYFyT1ZSE_oUcfktioGKXVY4rdSwa2611G5C96XIiJlQ_6y1AhaR8QZLnG2Zg-WIGkstL8M9cvrcu5PF9N14UnJLt-Y_Z2An2HQoVjntqvOpHWmUphyDrVb7q1HaUpMt-THpZvdC5Hs2wGJ1joESFlvozz309gDL5g31rzhNGXBTlKT9xwfCjGveFnR6fdgJdCYqAxXu7S4c0pQO1jTWcL_XOttWY9PYjimNB5yMsIzdfGHfRLmxlMi6hCUncz_tZsm-arXHQNnrcl6-hOM8IPzTR_1kQhQS5F_b7q4JRxiMQD3vfB1x_N09jExr6jYLSIiZj9Uo1z8A0TNvY1O23UMAhToP4Ts1JzhA3xIO5dduUHF9-8ygMDyC6uOtvLkdaiyPruPL89xXKpjNOt4M4RCGNb7sGazaTlnIdw_lDE_plwg15NVU3CWoSedhqBN6suoqrqyCV_sPycJwSkQfAvJ4aIyGcosxNsjR1LxMzFnLO2Zk.-56pcUibxLiA6xydC1HUHQ
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 2350

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>7930 SW 54TH CT MIAMI FL 33143</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>551_1651768244</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - NEMANJU CONSTRUCTION CORP</ns0:Description>
    <ns0:Amount>45.00</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>45.00</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Firm Mail</ns0:Description>
    <ns0:Amount>3.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Firm Mail">18</ns0:ItemRef>
      <ns0:UnitPrice>3.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail</ns0:Description>
    <ns0:Amount>5.42</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Certified Mail">17</ns0:ItemRef>
      <ns0:UnitPrice>5.42</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail with Electronic Return Receipt</ns0:Description>
    <ns0:Amount>7.22</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Electronic Return Receipt">15</ns0:ItemRef>
      <ns0:UnitPrice>7.22</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>235</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


