RESPONSE URI FOR SEQUENCE ID 08052
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

RESPONSE HEADERS
================
date: Thu, 14 Apr 2022 17:57:05 GMT
content-type: application/xml
content-length: 509
intuit_tid: 1-62586070-4496f4227c4a25ec75f9b692
x-spanid: 1c435cf6-7fc8-4f4c-bcc2-a85a821ffbbe
x-amzn-trace-id: Root=1-62586070-4496f4227c4a25ec75f9b692
x-content-type-options: nosniff
x-envoy-upstream-service-time: 364
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-04-14T10:57:04.710-07:00">
  <Fault type="ValidationFault">
    <Error code="6000" element="">
      <Message>A business validation error has occurred while processing your request</Message>
      <Detail>Business Validation Error: You cannot enter more than 31 characters in the sales_custom_1_val field. You tried entering NTO 342- c..., which is 46 characters.</Detail>
    </Error>
  </Fault>
</IntuitResponse>


