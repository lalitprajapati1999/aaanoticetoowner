RESPONSE URI FOR SEQUENCE ID 00536
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/query?minorversion=47

RESPONSE HEADERS
================
date: Tue, 09 Nov 2021 13:03:13 GMT
content-type: application/xml;charset=UTF-8
content-length: 3543
intuit_tid: 1-618a7191-057c95cc4a49434c0ac00a8f
x-spanid: 8b4b8292-83d3-4875-942a-0398b9b9667a
x-amzn-trace-id: Root=1-618a7191-057c95cc4a49434c0ac00a8f
x-content-type-options: nosniff
qbo-version: 1969.129
service-time: total=151, db=43
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private
x-envoy-upstream-service-time: 177
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2021-11-09T05:03:13.783-08:00">
  <QueryResponse startPosition="1" maxResults="1" totalCount="1">
    <Invoice domain="QBO" sparse="false">
      <Id>107</Id>
      <SyncToken>0</SyncToken>
      <MetaData>
        <CreateTime>2021-11-09T05:00:23-08:00</CreateTime>
        <LastUpdatedTime>2021-11-09T05:00:23-08:00</LastUpdatedTime>
      </MetaData>
      <CustomField>
        <DefinitionId>1</DefinitionId>
        <Type>StringType</Type>
        <StringValue>PO 9999</StringValue>
      </CustomField>
      <CustomField>
        <DefinitionId>2</DefinitionId>
        <Type>StringType</Type>
        <StringValue>Intent to Lien</StringValue>
      </CustomField>
      <CustomField>
        <DefinitionId>3</DefinitionId>
        <Type>StringType</Type>
        <StringValue>1250 NW 62TH STREET MIAMI FLOR</StringValue>
      </CustomField>
      <DocNumber>8_CYO_1636462822</DocNumber>
      <TxnDate>2021-11-09</TxnDate>
      <CurrencyRef name="United States Dollar">USD</CurrencyRef>
      <Line>
        <Id>1</Id>
        <LineNum>1</LineNum>
        <Description>Intent to Lien
Contracted By - A&amp;Y SERVICE AND REPAIR
Parent Work Order Number - 4
06/07/2021</Description>
        <Amount>30.00</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="SERVICES:Intent to Lien">37</ItemRef>
          <UnitPrice>30</UnitPrice>
          <Qty>1</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>NON</TaxCodeRef>
          <TaxClassificationRef>EUC-99990201-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Id>2</Id>
        <LineNum>2</LineNum>
        <Description>Firm Mail</Description>
        <Amount>8.00</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="MAILING:Firm Mail">18</ItemRef>
          <UnitPrice>1</UnitPrice>
          <Qty>8</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>TAX</TaxCodeRef>
          <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Amount>38.00</Amount>
        <DetailType>SubTotalLineDetail</DetailType>
        <SubTotalLineDetail/>
      </Line>
      <TxnTaxDetail>
        <TxnTaxCodeRef>3</TxnTaxCodeRef>
        <TotalTax>0.56</TotalTax>
        <TaxLine>
          <Amount>0.48</Amount>
          <DetailType>TaxLineDetail</DetailType>
          <TaxLineDetail>
            <TaxRateRef>3</TaxRateRef>
            <PercentBased>true</PercentBased>
            <TaxPercent>6</TaxPercent>
            <NetAmountTaxable>8.00</NetAmountTaxable>
          </TaxLineDetail>
        </TaxLine>
        <TaxLine>
          <Amount>0.08</Amount>
          <DetailType>TaxLineDetail</DetailType>
          <TaxLineDetail>
            <TaxRateRef>4</TaxRateRef>
            <PercentBased>true</PercentBased>
            <TaxPercent>1</TaxPercent>
            <NetAmountTaxable>8.00</NetAmountTaxable>
          </TaxLineDetail>
        </TaxLine>
      </TxnTaxDetail>
      <CustomerRef name="AAA Construction Corp._1615909222">213</CustomerRef>
      <BillAddr>
        <Id>218</Id>
        <Line1>301 Hialeah Drive ste 118</Line1>
        <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
        <PostalCode>33010</PostalCode>
      </BillAddr>
      <ShipAddr>
        <Id>219</Id>
        <Line1>301 Hialeah Drive ste 118</Line1>
        <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
        <PostalCode>33010</PostalCode>
      </ShipAddr>
      <FreeFormAddress>true</FreeFormAddress>
      <ShipFromAddr>
        <Id>344</Id>
        <Line1>PO Box 22821</Line1>
        <Line2>Hialeah, FL  33002 US</Line2>
      </ShipFromAddr>
      <SalesTermRef name="Net 15">2</SalesTermRef>
      <DueDate>2021-11-24</DueDate>
      <TotalAmt>38.56</TotalAmt>
      <ApplyTaxAfterDiscount>false</ApplyTaxAfterDiscount>
      <PrintStatus>NeedToPrint</PrintStatus>
      <EmailStatus>NotSet</EmailStatus>
      <Balance>38.56</Balance>
      <TaxExemptionRef/>
      <AllowIPNPayment>false</AllowIPNPayment>
      <AllowOnlinePayment>false</AllowOnlinePayment>
      <AllowOnlineCreditCardPayment>false</AllowOnlineCreditCardPayment>
      <AllowOnlineACHPayment>false</AllowOnlineACHPayment>
    </Invoice>
  </QueryResponse>
</IntuitResponse>


