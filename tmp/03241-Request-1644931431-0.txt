REQUEST URI FOR SEQUENCE ID 03241
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/customer?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..EPG7L1_V0HKVgJiDtXrXJQ.eJCxHNV2QZPtS64viSl_bV4H0eyPn35j0lig9MIHjQpWDATb3ZsjBd4CX4fnHSSk-MQeYa-TFJn_5VMguUd6yequsZiGz-4_Q9zEspc0umL5WGOETeYjlAl50gzp2G6NU1_dW-sd2KjcvVpVXq_8D3wOWO3XTyZ4nvPJZfYlIceckEwtkfYZBj4qm-k0W2i7xtwo9HZ58BX9f9UkfT1TLkc65MGMMq7OyBWwU-lI6X27rAou4RcUFm1_Ik65bgS5TMskJ1ofvpYEsWN0Ov4Ij20qD_x4pMlL7sgD_aw4uMsFNCeQC1fZu_1yH__GpsXyjegwrlWjSy7TIw_nbJ1phxPpFjI9Qxjaq4PmMQdpOPAKpo7kjC1vOraGXAksewBBsv5NYR-GBpKOaFFldInijHzNFSQxbIi-cqc01hHXsF2ps6UQS5o8Li3Uk5njPdr_I-eqFkk5zZ9spKjJmcvvSSHEXvFzmj074vCgVrWzwBjQuyC3Sm_VdKJOKi2775kca88_WVJN74-Raqw5SYNjR4ecQR6y7yJ2wXNGbmF252gJQ3Yf09SjoJqaqEHbWEsBUJUmHrfvvsP8UBkRHSfrbt87XIlWXlRNg6359Db4sRcT-zMdnLT0tPkQuDLEpibTGOFa8TSBCzMbS2pP_U4MX_DDQcyTyUivTr7XKm3FDfnWdk7Tj5JA_IBPn-gwGmEaorq2MU6VRnMSu0t61t9htP-UtBuf14fUYuoKbIgRCbo.YCxJgt6N5bjWEgRZSzDr9A
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 1038

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Customer xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:FullyQualifiedName>ARCHITECTURAL DOORS AND FRAMES INCORPORATE</ns0:FullyQualifiedName>
  <ns0:CompanyName>ARCHITECTURAL DOORS AND FRAMES INCORPORATE</ns0:CompanyName>
  <ns0:DisplayName>ARCHITECTURAL DOORS AND FRAMES INCORPORATE_1644931431</ns0:DisplayName>
  <ns0:PrimaryPhone>
    <ns0:FreeFormNumber>(305) 805-1444</ns0:FreeFormNumber>
  </ns0:PrimaryPhone>
  <ns0:PrimaryEmailAddr>
    <ns0:Address>Olga@adfdoors.net</ns0:Address>
  </ns0:PrimaryEmailAddr>
  <ns0:BillAddr>
    <ns0:Line1>166 WEST 25 STREET</ns0:Line1>
    <ns0:City>Hialeah</ns0:City>
    <ns0:CountrySubDivisionCode>Florida</ns0:CountrySubDivisionCode>
    <ns0:PostalCode>33010</ns0:PostalCode>
  </ns0:BillAddr>
  <ns0:ShipAddr>
    <ns0:Line1>166 WEST 25 STREET</ns0:Line1>
    <ns0:City>Hialeah</ns0:City>
    <ns0:CountrySubDivisionCode>Florida</ns0:CountrySubDivisionCode>
    <ns0:PostalCode>33010</ns0:PostalCode>
  </ns0:ShipAddr>
</ns0:Customer>


