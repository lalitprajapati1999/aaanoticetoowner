RESPONSE URI FOR SEQUENCE ID 03712
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/customer?minorversion=47

RESPONSE HEADERS
================
date: Thu, 03 Mar 2022 16:14:47 GMT
content-type: application/xml;charset=UTF-8
content-length: 1488
intuit_tid: 1-6220e976-3846299d386c4b2404b45de8
x-spanid: c0720ee5-e0e0-442f-ba82-47964316d3a2
x-amzn-trace-id: Root=1-6220e976-3846299d386c4b2404b45de8
x-content-type-options: nosniff
x-xss-protection: 1; mode=block
qbo-version: 1975.230
service-time: total=329, db=62
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private
x-envoy-upstream-service-time: 366
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-03-03T08:14:46.803-08:00">
  <Customer domain="QBO" sparse="false">
    <Id>266</Id>
    <SyncToken>0</SyncToken>
    <MetaData>
      <CreateTime>2022-03-03T08:14:46-08:00</CreateTime>
      <LastUpdatedTime>2022-03-03T08:14:46-08:00</LastUpdatedTime>
    </MetaData>
    <FullyQualifiedName>Mr Wood Custom Floors, Inc_1646324085</FullyQualifiedName>
    <CompanyName>Mr Wood Custom Floors, Inc</CompanyName>
    <DisplayName>Mr Wood Custom Floors, Inc_1646324085</DisplayName>
    <PrintOnCheckName>Mr Wood Custom Floors, Inc</PrintOnCheckName>
    <Active>true</Active>
    <PrimaryPhone>
      <FreeFormNumber>(305) 917-5858</FreeFormNumber>
    </PrimaryPhone>
    <PrimaryEmailAddr>
      <Address>mrwoodfloors@yahoo.com</Address>
    </PrimaryEmailAddr>
    <DefaultTaxCodeRef>3</DefaultTaxCodeRef>
    <Taxable>true</Taxable>
    <BillAddr>
      <Id>662</Id>
      <Line1>1925 NE 150th Street</Line1>
      <City>North miami</City>
      <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
      <PostalCode>33181</PostalCode>
    </BillAddr>
    <ShipAddr>
      <Id>663</Id>
      <Line1>1925 NE 150th Street</Line1>
      <City>North miami</City>
      <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
      <PostalCode>33181</PostalCode>
    </ShipAddr>
    <Job>false</Job>
    <BillWithParent>false</BillWithParent>
    <Balance>0</Balance>
    <BalanceWithJobs>0</BalanceWithJobs>
    <CurrencyRef name="United States Dollar">USD</CurrencyRef>
    <PreferredDeliveryMethod>None</PreferredDeliveryMethod>
    <IsProject>false</IsProject>
  </Customer>
</IntuitResponse>


