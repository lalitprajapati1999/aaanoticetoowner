RESPONSE URI FOR SEQUENCE ID 09787
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

RESPONSE HEADERS
================
date: Fri, 27 May 2022 14:30:24 GMT
content-type: application/xml
content-length: 509
intuit_tid: 1-6290e080-5948ca082455afbc01e3c325
x-spanid: ae2cc16b-2e51-4137-874f-7a944962ff67
x-amzn-trace-id: Root=1-6290e080-5948ca082455afbc01e3c325
x-content-type-options: nosniff
x-envoy-upstream-service-time: 550
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-05-27T07:30:24.413-07:00">
  <Fault type="ValidationFault">
    <Error code="6000" element="">
      <Message>A business validation error has occurred while processing your request</Message>
      <Detail>Business Validation Error: You cannot enter more than 31 characters in the sales_custom_1_val field. You tried entering NTO 342- c..., which is 46 characters.</Detail>
    </Error>
  </Fault>
</IntuitResponse>


