RESPONSE URI FOR SEQUENCE ID 00456
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/query?minorversion=47

RESPONSE HEADERS
================
date: Mon, 23 Aug 2021 08:49:39 GMT
content-type: application/xml;charset=UTF-8
content-length: 2585
server: nginx
intuit_tid: 1-61236123-434b353575b081a54ba1270d
x-spanid: 58cbef48-d0a8-40a4-b845-3a20d7b6200e
x-amzn-trace-id: Root=1-61236123-434b353575b081a54ba1270d
x-content-type-options: nosniff
qbo-version: 1963.140
service-time: total=174, db=72
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private
strict-transport-security: max-age=31536000

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2021-08-23T01:49:39.464-07:00">
  <QueryResponse startPosition="1" maxResults="1" totalCount="1">
    <Invoice domain="QBO" sparse="false">
      <Id>78</Id>
      <SyncToken>0</SyncToken>
      <MetaData>
        <CreateTime>2021-08-23T01:46:38-07:00</CreateTime>
        <LastUpdatedTime>2021-08-23T01:46:38-07:00</LastUpdatedTime>
      </MetaData>
      <CustomField>
        <DefinitionId>1</DefinitionId>
        <Type>StringType</Type>
        <StringValue>45</StringValue>
      </CustomField>
      <CustomField>
        <DefinitionId>2</DefinitionId>
        <Type>StringType</Type>
        <StringValue>Claim of Lien</StringValue>
      </CustomField>
      <CustomField>
        <DefinitionId>3</DefinitionId>
        <Type>StringType</Type>
        <StringValue>1250 NW 62TH STREET MIAMI FLOR</StringValue>
      </CustomField>
      <DocNumber>21_CYO_1629708397</DocNumber>
      <TxnDate>2021-08-23</TxnDate>
      <CurrencyRef name="United States Dollar">USD</CurrencyRef>
      <Line>
        <Id>1</Id>
        <LineNum>1</LineNum>
        <Description>Claim of Lien
Contracted By - Bhushan Mehta
07/21/2021</Description>
        <Amount>100.00</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="CLAIM OF LIEN:Claim of Lien">38</ItemRef>
          <UnitPrice>100</UnitPrice>
          <Qty>1</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>NON</TaxCodeRef>
          <TaxClassificationRef>EUC-99990201-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Amount>100.00</Amount>
        <DetailType>SubTotalLineDetail</DetailType>
        <SubTotalLineDetail/>
      </Line>
      <TxnTaxDetail>
        <TotalTax>0</TotalTax>
      </TxnTaxDetail>
      <CustomerRef name="Neosoft_1612276794">211</CustomerRef>
      <BillAddr>
        <Id>213</Id>
        <Line1>harshad.khiste@neosoftmail.com</Line1>
        <CountrySubDivisionCode>Maharashtra</CountrySubDivisionCode>
        <PostalCode>33112</PostalCode>
      </BillAddr>
      <ShipAddr>
        <Id>214</Id>
        <Line1>harshad.khiste@neosoftmail.com</Line1>
        <CountrySubDivisionCode>Maharashtra</CountrySubDivisionCode>
        <PostalCode>33112</PostalCode>
      </ShipAddr>
      <FreeFormAddress>true</FreeFormAddress>
      <ShipFromAddr>
        <Id>312</Id>
        <Line1>PO Box 22821</Line1>
        <Line2>Hialeah, FL  33002 US</Line2>
      </ShipFromAddr>
      <SalesTermRef name="Net 15">2</SalesTermRef>
      <DueDate>2021-09-07</DueDate>
      <TotalAmt>100.00</TotalAmt>
      <ApplyTaxAfterDiscount>false</ApplyTaxAfterDiscount>
      <PrintStatus>NeedToPrint</PrintStatus>
      <EmailStatus>NotSet</EmailStatus>
      <Balance>100.00</Balance>
      <TaxExemptionRef/>
      <AllowIPNPayment>false</AllowIPNPayment>
      <AllowOnlinePayment>false</AllowOnlinePayment>
      <AllowOnlineCreditCardPayment>false</AllowOnlineCreditCardPayment>
      <AllowOnlineACHPayment>false</AllowOnlineACHPayment>
    </Invoice>
  </QueryResponse>
</IntuitResponse>


