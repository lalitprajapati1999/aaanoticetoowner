REQUEST URI FOR SEQUENCE ID 11890
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..wSpzOestWjFYvhF34dtm_Q.8kCBtiGHLElWp_c2WPagnHR4ghAjfnP5VxqPuLi-xMu-f0y0O3b5bFIDFT4o6skwf3mRjuNdezKz-L4QSTf_tzgL1f7lerzql_9NKvcykHq5R2aOX3kVpr0Z9k4MhGj8FbN99qexq3i0x-F-oVQvQg7y50i-e4ZIibVyQMkHsKHZ0aCuTLet5dwIxXNzKewFWLUZKBPiQUQSZWUfXonTOyuM52vfaaKQt51QnSvPrh82UNpC9U770sSC3iDQxfDWuSXax0UC3QIhrLY80_7FrljZPQ06sNbuDKgMQASfJ61DDeytNxeMb2krdr5_i7Alu1QGz8rFe2rhmIq97vdITfUtzioH21z2DuCPYZnnPtn_1rAAi5PuWGFfXodYCjhO9IfOEeuQagC2wghu9XJMbACJ0rjbHKmPGWajn2QP1k62un9JEpavBQLivtteRwx9YU5TGvOWufRxrB3XUY-puTmecFfDkjrZIoPMGCn-NQq9PeDXMbDU0X05ZsPN8jPhpIdAXolr0xooenu6YWGiMZHQ5M4xR4zjDcEDNDzSFGbD8Hxvk5u4IEPPXAYeqwiOeg1i4e3AGbeG8-ZpXyLtjDIvBCRgG_O9svc7_KaniUmhb5Z9xxXev0202XQgWQ6Z2AE7HWt9nNVLu2afQgVduHQ4sl_4rgOqhIvXvVR1_Zo-fbeSXBCj8ix9WoX_YKVU_MYsmZ7Z6FFLWT7ZZ1FzwoV0Wd76-HX1OEwD41vCpk0.qGLzWawzm36RRPCMySyr1A
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 2712

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>1100 West Palm Drive, Florida </ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>804_1658768329</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - ESTRELLA PLUMBING INC</ns0:Description>
    <ns0:Amount>35</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>35</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Additional address charges</ns0:Description>
    <ns0:Amount>140</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Additional Address">34</ns0:ItemRef>
      <ns0:UnitPrice>20</ns0:UnitPrice>
      <ns0:Qty>7</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Rush Fee</ns0:Description>
    <ns0:Amount>12.50</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Rush Hour Charges">33</ns0:ItemRef>
      <ns0:UnitPrice>12.50</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Firm Mail</ns0:Description>
    <ns0:Amount>3.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Firm Mail">18</ns0:ItemRef>
      <ns0:UnitPrice>3.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail with Electronic Return Receipt</ns0:Description>
    <ns0:Amount>15.32</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Electronic Return Receipt">15</ns0:ItemRef>
      <ns0:UnitPrice>7.66</ns0:UnitPrice>
      <ns0:Qty>2</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>36</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


