REQUEST URI FOR SEQUENCE ID 00233
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..ky-s1txrDHWKHr6onW-KJQ.v1q8F4Mot5TEj_BRDj-CT3Jsil61gPqKBwOA1xAUbfMH2KouCG6GTwkjNHhlWfdonTcm1hsMAnOJ7t9r6nztRVarc8F3rEm0jMYtBoBr2va-oGQTBcxjbVg687DRgS4DyzORnoLvHeyTHxuWoLGqyPOaF-vdTU11CSGpCHx4sJ2aZxDFKFMmV90dNHACTjAub3Nye4xQXM9uxCkXCOyDgOUDEzRrRero1kWOzJCpTHAtma1Vu0bFEdGbqNkof8I55caXHP6-nPzPBLjG_8-ylwYk7XSGca5uRyn9CPK0jGaB6KuebszWDeIwyhm6asmj0Sgryx32d-u6PS0qBCJk7knRssOHfTmcmcu2gUXuYZtr8yZfD7ypdWTWdPVtBZFuevJ0VYfa1SJefAZ2jsI037g3wH-VpW4-cLgm_Bpk6p4d6tdQ5qivBn9NdZseMfkgDYPYTWnN8xpxIg5DNICP-8bUfB7yDL_zQem7JiQ_CC3O5znCTVj5oMyvZ7f8Phk304rtAAyTB2Ul1oTtJb2vv6P5Gfm6jUvqzQObpmsH4naEaMiU5evRIAqZEY7rQskoJYWXAZKG5evweP3U8YXOvhE5t5B3W84rr_W_Ct9sW7LkBrL1P7v7_nImkSUi5YsxvaKdwibAZPpn61GEl1jMCiPWnhorO4SbhT05AJ3gVRUhez0YAgR1An-YkofwHIS4hXN2DAV8qZp083nC7YGIlxbXgiHe6qIUpHBQlsLZ_yU.2TTrvTS06RkCjsHZ9RPX7Q
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 2370

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>PO#1245</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>771 NW 24TH COURT MIAMI FLORID</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>50_1624458997</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - GOMEZ CONSTRUCTION COMPANY</ns0:Description>
    <ns0:Amount>45.00</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>45.00</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Additional address charges</ns0:Description>
    <ns0:Amount>11</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Additional Address">34</ns0:ItemRef>
      <ns0:UnitPrice>1.00</ns0:UnitPrice>
      <ns0:Qty>11</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Rush Fee</ns0:Description>
    <ns0:Amount>12.50</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Rush Hour Charges">33</ns0:ItemRef>
      <ns0:UnitPrice>12.50</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Firm Mail</ns0:Description>
    <ns0:Amount>47.4</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Firm Mail">18</ns0:ItemRef>
      <ns0:UnitPrice>3.95</ns0:UnitPrice>
      <ns0:Qty>12</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>213</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


