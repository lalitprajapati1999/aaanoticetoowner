RESPONSE URI FOR SEQUENCE ID 11325
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

RESPONSE HEADERS
================
date: Tue, 12 Jul 2022 20:07:37 GMT
content-type: application/xml
content-length: 509
intuit_tid: 1-62cdd488-1fafea9769be13811ab117d7
x-spanid: 2a375cd4-bec5-4448-8026-b912549a64dd
x-amzn-trace-id: Root=1-62cdd488-1fafea9769be13811ab117d7
x-content-type-options: nosniff
x-envoy-upstream-service-time: 666
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-07-12T13:07:36.786-07:00">
  <Fault type="ValidationFault">
    <Error code="6000" element="">
      <Message>A business validation error has occurred while processing your request</Message>
      <Detail>Business Validation Error: You cannot enter more than 31 characters in the sales_custom_1_val field. You tried entering CICC 7040-..., which is 40 characters.</Detail>
    </Error>
  </Fault>
</IntuitResponse>


