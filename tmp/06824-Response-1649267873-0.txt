RESPONSE URI FOR SEQUENCE ID 06824
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

RESPONSE HEADERS
================
date: Wed, 06 Apr 2022 17:57:53 GMT
content-type: application/xml
content-length: 509
intuit_tid: 1-624dd4a1-57b55eb969997aa67804b2fd
x-spanid: 16e9759f-d1ec-4f10-a59a-e18cad3aa4f9
x-amzn-trace-id: Root=1-624dd4a1-57b55eb969997aa67804b2fd
x-content-type-options: nosniff
x-envoy-upstream-service-time: 356
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-04-06T10:57:53.156-07:00">
  <Fault type="ValidationFault">
    <Error code="6000" element="">
      <Message>A business validation error has occurred while processing your request</Message>
      <Detail>Business Validation Error: You cannot enter more than 31 characters in the sales_custom_1_val field. You tried entering NTO 342- c..., which is 46 characters.</Detail>
    </Error>
  </Fault>
</IntuitResponse>


