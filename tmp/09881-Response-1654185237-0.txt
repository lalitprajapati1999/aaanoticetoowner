RESPONSE URI FOR SEQUENCE ID 09881
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

RESPONSE HEADERS
================
date: Thu, 02 Jun 2022 15:53:57 GMT
content-type: application/xml
content-length: 509
intuit_tid: 1-6298dd14-19933461650a908a7eb258f2
x-spanid: d85d9164-8966-4ed6-b946-7f46a6433c44
x-amzn-trace-id: Root=1-6298dd14-19933461650a908a7eb258f2
x-content-type-options: nosniff
x-envoy-upstream-service-time: 881
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-06-02T08:53:56.941-07:00">
  <Fault type="ValidationFault">
    <Error code="6000" element="">
      <Message>A business validation error has occurred while processing your request</Message>
      <Detail>Business Validation Error: You cannot enter more than 31 characters in the sales_custom_1_val field. You tried entering CICC 7040-..., which is 40 characters.</Detail>
    </Error>
  </Fault>
</IntuitResponse>


