RESPONSE URI FOR SEQUENCE ID 07188
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/query?minorversion=47

RESPONSE HEADERS
================
date: Wed, 06 Apr 2022 22:16:27 GMT
content-type: application/xml;charset=UTF-8
content-length: 4136
intuit_tid: 1-624e113b-57cf1cae776419ff0371b90d
x-spanid: 63c37a75-c335-4c30-8ff0-75be39762244
x-amzn-trace-id: Root=1-624e113b-57cf1cae776419ff0371b90d
x-content-type-options: nosniff
x-xss-protection: 1; mode=block
qbo-version: 1978.180
service-time: total=143, db=53
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private
x-envoy-upstream-service-time: 174
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-04-06T15:16:27.658-07:00">
  <QueryResponse startPosition="1" maxResults="1" totalCount="1">
    <Invoice domain="QBO" sparse="false">
      <Id>240</Id>
      <SyncToken>0</SyncToken>
      <MetaData>
        <CreateTime>2022-01-26T05:21:37-08:00</CreateTime>
        <LastUpdatedTime>2022-04-06T12:56:49-07:00</LastUpdatedTime>
      </MetaData>
      <CustomField>
        <DefinitionId>2</DefinitionId>
        <Type>StringType</Type>
        <StringValue>Notice to Owner/Preliminary No</StringValue>
      </CustomField>
      <CustomField>
        <DefinitionId>3</DefinitionId>
        <Type>StringType</Type>
        <StringValue>2733 LAKE RIDGE LANE WESTON FL</StringValue>
      </CustomField>
      <DocNumber>193_1643203296</DocNumber>
      <TxnDate>2022-01-26</TxnDate>
      <CurrencyRef name="United States Dollar">USD</CurrencyRef>
      <Line>
        <Id>1</Id>
        <LineNum>1</LineNum>
        <Description>Contracted By - BUILDINGWORKS CONSTRUCTION CORP.</Description>
        <Amount>45.00</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="SERVICES:NTO">32</ItemRef>
          <UnitPrice>45</UnitPrice>
          <Qty>1</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>TAX</TaxCodeRef>
          <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Id>2</Id>
        <LineNum>2</LineNum>
        <Description>Certified Mail</Description>
        <Amount>10.84</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="MAILING:Certified Mail USPS">17</ItemRef>
          <UnitPrice>5.42</UnitPrice>
          <Qty>2</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>TAX</TaxCodeRef>
          <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Id>3</Id>
        <LineNum>3</LineNum>
        <Description>Certified Mail with Electronic Return Receipt</Description>
        <Amount>7.22</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="MAILING:Certified Mail ESRR">15</ItemRef>
          <UnitPrice>7.22</UnitPrice>
          <Qty>1</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>TAX</TaxCodeRef>
          <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Amount>63.06</Amount>
        <DetailType>SubTotalLineDetail</DetailType>
        <SubTotalLineDetail/>
      </Line>
      <TxnTaxDetail>
        <TxnTaxCodeRef>3</TxnTaxCodeRef>
        <TotalTax>4.41</TotalTax>
        <TaxLine>
          <Amount>3.78</Amount>
          <DetailType>TaxLineDetail</DetailType>
          <TaxLineDetail>
            <TaxRateRef>3</TaxRateRef>
            <PercentBased>true</PercentBased>
            <TaxPercent>6</TaxPercent>
            <NetAmountTaxable>63.06</NetAmountTaxable>
          </TaxLineDetail>
        </TaxLine>
        <TaxLine>
          <Amount>0.63</Amount>
          <DetailType>TaxLineDetail</DetailType>
          <TaxLineDetail>
            <TaxRateRef>4</TaxRateRef>
            <PercentBased>true</PercentBased>
            <TaxPercent>1</TaxPercent>
            <NetAmountTaxable>63.06</NetAmountTaxable>
          </TaxLineDetail>
        </TaxLine>
      </TxnTaxDetail>
      <CustomerRef name="DESMAR PC INC_1641471298">226</CustomerRef>
      <BillAddr>
        <Id>428</Id>
        <Line1>6405 NW 36 ST # 124</Line1>
        <City>Miami</City>
        <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
        <PostalCode>33166</PostalCode>
      </BillAddr>
      <ShipAddr>
        <Id>429</Id>
        <Line1>6405 NW 36 ST # 124</Line1>
        <City>Miami</City>
        <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
        <PostalCode>33166</PostalCode>
      </ShipAddr>
      <FreeFormAddress>true</FreeFormAddress>
      <ShipFromAddr>
        <Id>528</Id>
        <Line1>PO Box 22821</Line1>
        <Line2>Hialeah, FL  33002 US</Line2>
      </ShipFromAddr>
      <SalesTermRef name="Net 15">2</SalesTermRef>
      <DueDate>2022-02-10</DueDate>
      <TotalAmt>67.47</TotalAmt>
      <ApplyTaxAfterDiscount>false</ApplyTaxAfterDiscount>
      <PrintStatus>NeedToPrint</PrintStatus>
      <EmailStatus>EmailSent</EmailStatus>
      <BillEmail>
        <Address>info@desmarpc.com</Address>
      </BillEmail>
      <Balance>67.47</Balance>
      <DeliveryInfo>
        <DeliveryType>Email</DeliveryType>
        <DeliveryTime>2022-04-06T12:23:32-07:00</DeliveryTime>
      </DeliveryInfo>
      <TaxExemptionRef name="service">99</TaxExemptionRef>
      <AllowIPNPayment>false</AllowIPNPayment>
      <AllowOnlinePayment>false</AllowOnlinePayment>
      <AllowOnlineCreditCardPayment>false</AllowOnlineCreditCardPayment>
      <AllowOnlineACHPayment>false</AllowOnlineACHPayment>
      <EInvoiceStatus>Viewed</EInvoiceStatus>
    </Invoice>
  </QueryResponse>
</IntuitResponse>


