RESPONSE URI FOR SEQUENCE ID 11453
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

RESPONSE HEADERS
================
date: Mon, 18 Jul 2022 11:44:48 GMT
content-type: application/xml
content-length: 509
intuit_tid: 1-62d547b0-7f45e7a35986691011b21449
x-spanid: 5e08f7ab-3717-4295-98d1-1bc40181dad9
x-amzn-trace-id: Root=1-62d547b0-7f45e7a35986691011b21449
x-content-type-options: nosniff
x-envoy-upstream-service-time: 370
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-07-18T04:44:48.208-07:00">
  <Fault type="ValidationFault">
    <Error code="6000" element="">
      <Message>A business validation error has occurred while processing your request</Message>
      <Detail>Business Validation Error: You cannot enter more than 31 characters in the sales_custom_1_val field. You tried entering NTO 342- c..., which is 46 characters.</Detail>
    </Error>
  </Fault>
</IntuitResponse>


