REQUEST URI FOR SEQUENCE ID 10968
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..rSguZphyfoanOP6zqcKkQQ.xhhofhZEq-OXjEr_pT_hdBwUfkTuLNMFYGXKkFTHw0Z_c2grQMoHuwq56pgCVpNHGp-vWKK8xNrXVuS8s5gVMPiEQ1TvUYYyWrzx-CWjWgRA6BU4zYEFsziK01jaYgQoUdCuVu3_6SZG5Lcf9KebmNaf8WTmV062xsrXNro4fjhjdTItoLW6PPT2dm6igiIcH49vcWZApUvX6tDXdRut-4X5VprpVR3oe6Cv3fHo2mC-Ps9lRg9DLEhGM1Dm6_epp-xtWfOAmE2D_XHHmxLwkDuZmrDlvjjI04JCR2AtG_0aiOIaJB4miFynrKwzYlYq3xsRdii6hBETtiN9Q6Y1xtBBFilCtje-CGa1h8qDKIFleeoJ8dp0myH_N8FWK6Wb7mqSabjjwPO0_iIYvm_jmLkwX7eO-ljmlJ900F8Lie4WTjvMfkupgDsvCBuvruAO5IoIfPG9f_loZqUp84hSi4DBPjSnN2unMAUBOGZwsNCjtFpEkDn0eQWvBjHFZYFXxkt69TTy5uYlHEU2j5NtNnp4hjLVaWyR_kJ0mR5Eh_CCFc4TO-1m7ewdrgx7hareNCNjWcJaF8FpvrYLJQjXuDnW27rj6Tj4Q8Rsc8o7_mZjQi_X0M7MNzWqQRkxpz-CIwxYhlaGhyRjzJrT568mWhX2nUdGMo58CvIZ6mAXuUQUYgQQGPfqVoIkdUjZIux-8yNC0UwIBSXRuysiPOzmL9VSTbX44UvSvmmlk0k9tXM.nFs8W691zJO7K_0RR2HT2w
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 1610

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>123 sw 87 st miami fl</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>108_CYO_1657029537</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Notice to Owner/Preliminary Notice
Contracted By - Frank mesa
06/28/2022</ns0:Description>
    <ns0:Amount>.01</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>.01</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Firm Mail</ns0:Description>
    <ns0:Amount>15.8</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Firm Mail">18</ns0:ItemRef>
      <ns0:UnitPrice>3.95</ns0:UnitPrice>
      <ns0:Qty>4</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>213</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


