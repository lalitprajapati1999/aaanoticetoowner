RESPONSE URI FOR SEQUENCE ID 02850
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/customer?minorversion=47

RESPONSE HEADERS
================
date: Thu, 27 Jan 2022 04:31:57 GMT
content-type: application/xml;charset=UTF-8
content-length: 1532
intuit_tid: 1-61f2203d-42a1965425ec95f256bdea5a
x-spanid: 7cd34318-3572-4735-aea8-b6738739d775
x-amzn-trace-id: Root=1-61f2203d-42a1965425ec95f256bdea5a
x-content-type-options: nosniff
qbo-version: 1973.097
service-time: total=426, db=80
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private
x-envoy-upstream-service-time: 484
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-01-26T20:31:57.637-08:00">
  <Customer domain="QBO" sparse="false">
    <Id>250</Id>
    <SyncToken>0</SyncToken>
    <MetaData>
      <CreateTime>2022-01-26T20:31:57-08:00</CreateTime>
      <LastUpdatedTime>2022-01-26T20:31:57-08:00</LastUpdatedTime>
    </MetaData>
    <FullyQualifiedName>EMANUEL SMOOTH FINISH AND PLASTERING, LLC_1643257916</FullyQualifiedName>
    <CompanyName>EMANUEL SMOOTH FINISH AND PLASTERING, LLC</CompanyName>
    <DisplayName>EMANUEL SMOOTH FINISH AND PLASTERING, LLC_1643257916</DisplayName>
    <PrintOnCheckName>EMANUEL SMOOTH FINISH AND PLASTERING, LLC</PrintOnCheckName>
    <Active>true</Active>
    <PrimaryPhone>
      <FreeFormNumber>(786) 718-6569</FreeFormNumber>
    </PrimaryPhone>
    <PrimaryEmailAddr>
      <Address>plastero1215@gmail.com</Address>
    </PrimaryEmailAddr>
    <DefaultTaxCodeRef>3</DefaultTaxCodeRef>
    <Taxable>true</Taxable>
    <BillAddr>
      <Id>533</Id>
      <Line1>1930 NW 68 TERRACE</Line1>
      <City>Miami</City>
      <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
      <PostalCode>33147</PostalCode>
    </BillAddr>
    <ShipAddr>
      <Id>534</Id>
      <Line1>1930 NW 68 TERRACE</Line1>
      <City>Miami</City>
      <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
      <PostalCode>33147</PostalCode>
    </ShipAddr>
    <Job>false</Job>
    <BillWithParent>false</BillWithParent>
    <Balance>0</Balance>
    <BalanceWithJobs>0</BalanceWithJobs>
    <CurrencyRef name="United States Dollar">USD</CurrencyRef>
    <PreferredDeliveryMethod>None</PreferredDeliveryMethod>
    <IsProject>false</IsProject>
  </Customer>
</IntuitResponse>


