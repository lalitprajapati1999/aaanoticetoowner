REQUEST URI FOR SEQUENCE ID 02443
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..5q4guv3C1nLoqWKCzTOBJA.6qBnbPUfS2_FXmDk26Rbj5TDrcmlVmrEx4BuE970jQYodvn-Y6vg81nPkV4JA3hdzX_6DLBiDEOmWlVLvFNk2CC0MTVaOKsAT15_xouwSI_BaS63oLUC1SdRjZ14f_6a4rBhVKaTb3IXeAPmGvSt_d1v_OXiP-RTbAw-jPK11OvlpavIFEuh78rBge2p-TeyV3EO9YmbNime7pf5I5AQykuVjrlgDK-g6ToDqILgoIil4JhAbWkKRtZpBC4R9jw-jyQzaixGIdWRZF66G-uAxIlaPJyPpvmV6NIt2OuOj6Fi5Smx-pn6LcKC1P8e57LNGkN_vzAR3iZKk3vnKpSGmFGoWgggAJXrw3D2ZsUfhVrW-lnihHIXwJO73cSPXPoP5nTOnpvY5egKBhgN9S2Cvx5Y6eLnLaS-1-p9KgpsqbGa8HncImdpIy98yYvEAmVXPvWhMG7tl-dWiWIcLwvt_RaFNGIKnI3SfgPe8-MQmFeL0gpJ5_-Rio9P1jEkDagq_ybOdD4GgQeMbA-VOAhY-u5hkWw2Pkhcy_WI7I5Gx0-IZc7kPpyV3UirjfGQFG3jPCmD9ZBXO0BAJRTqVRt2hSpxeXnm5thwEtA5Mdxx8YU1HCDszI418UYXk2YNI2bZhYrHVzfjgpehdvW-7BkY_HWJyKI-q2rkq336bldfjQwVl2oMyOJL8yItDgTL0bxFEuUFoqtk_6adi32zcAUgogDmRlLTn3S13fZacPsnfpQ.gES2ARcDrqT6RlB1P4WMXg
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 2348

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>8778 SW 136 Street, Miami, Fl.</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>156_1642082579</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - J. Raymond Construction</ns0:Description>
    <ns0:Amount>45.00</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>45.00</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Firm Mail</ns0:Description>
    <ns0:Amount>3.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Firm Mail">18</ns0:ItemRef>
      <ns0:UnitPrice>3.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail</ns0:Description>
    <ns0:Amount>5.42</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Certified Mail">17</ns0:ItemRef>
      <ns0:UnitPrice>5.42</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail with Electronic Return Receipt</ns0:Description>
    <ns0:Amount>7.22</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Electronic Return Receipt">15</ns0:ItemRef>
      <ns0:UnitPrice>7.22</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>228</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


