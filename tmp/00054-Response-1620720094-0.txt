RESPONSE URI FOR SEQUENCE ID 00054
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/query?minorversion=47

RESPONSE HEADERS
================
date: Tue, 11 May 2021 08:01:34 GMT
content-type: application/xml;charset=UTF-8
content-length: 3524
server: nginx
strict-transport-security: max-age=15552000
intuit_tid: 1-609a39de-39ac08346fd6590c760257b7
x-spanid: e430a717-aa39-4257-b474-e5a9e29373fb
x-amzn-trace-id: Root=1-609a39de-39ac08346fd6590c760257b7
qbo-version: 1957.141
service-time: total=110, db=52
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2021-05-11T01:01:34.463-07:00">
  <QueryResponse startPosition="1" maxResults="1" totalCount="1">
    <Invoice domain="QBO" sparse="false">
      <Id>10</Id>
      <SyncToken>1</SyncToken>
      <MetaData>
        <CreateTime>2021-04-28T06:03:12-07:00</CreateTime>
        <LastUpdatedTime>2021-05-11T00:59:50-07:00</LastUpdatedTime>
      </MetaData>
      <CustomField>
        <DefinitionId>1</DefinitionId>
        <Type>StringType</Type>
        <StringValue>PO NTO# 17710-113</StringValue>
      </CustomField>
      <CustomField>
        <DefinitionId>2</DefinitionId>
        <Type>StringType</Type>
        <StringValue>Non Payment Notice</StringValue>
      </CustomField>
      <CustomField>
        <DefinitionId>3</DefinitionId>
        <Type>StringType</Type>
        <StringValue>10471 BOCA WOOD LANE BOCA RATO</StringValue>
      </CustomField>
      <DocNumber>1619614991_21_1306</DocNumber>
      <TxnDate>2021-04-28</TxnDate>
      <CurrencyRef name="United States Dollar">USD</CurrencyRef>
      <Line>
        <Id>1</Id>
        <LineNum>1</LineNum>
        <Description>Contracted By - AG CONTRACTORSLLC
Parent Work Order Number - 11</Description>
        <Amount>45.00</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="SERVICES:NTO">32</ItemRef>
          <UnitPrice>45</UnitPrice>
          <Qty>1</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>TAX</TaxCodeRef>
          <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Id>2</Id>
        <LineNum>2</LineNum>
        <Description>firm mail</Description>
        <Amount>8.00</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="MAILING:Firm Mail">18</ItemRef>
          <UnitPrice>1</UnitPrice>
          <Qty>8</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>TAX</TaxCodeRef>
          <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Amount>53.00</Amount>
        <DetailType>SubTotalLineDetail</DetailType>
        <SubTotalLineDetail/>
      </Line>
      <TxnTaxDetail>
        <TxnTaxCodeRef>3</TxnTaxCodeRef>
        <TotalTax>3.71</TotalTax>
        <TaxLine>
          <Amount>3.18</Amount>
          <DetailType>TaxLineDetail</DetailType>
          <TaxLineDetail>
            <TaxRateRef>3</TaxRateRef>
            <PercentBased>true</PercentBased>
            <TaxPercent>6</TaxPercent>
            <NetAmountTaxable>53.00</NetAmountTaxable>
          </TaxLineDetail>
        </TaxLine>
        <TaxLine>
          <Amount>0.53</Amount>
          <DetailType>TaxLineDetail</DetailType>
          <TaxLineDetail>
            <TaxRateRef>4</TaxRateRef>
            <PercentBased>true</PercentBased>
            <TaxPercent>1</TaxPercent>
            <NetAmountTaxable>53.00</NetAmountTaxable>
          </TaxLineDetail>
        </TaxLine>
      </TxnTaxDetail>
      <CustomerRef name="AAA Construction Corp._1615909222">213</CustomerRef>
      <BillAddr>
        <Id>218</Id>
        <Line1>301 Hialeah Drive ste 118</Line1>
        <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
        <PostalCode>33010</PostalCode>
      </BillAddr>
      <ShipAddr>
        <Id>219</Id>
        <Line1>301 Hialeah Drive ste 118</Line1>
        <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
        <PostalCode>33010</PostalCode>
      </ShipAddr>
      <FreeFormAddress>true</FreeFormAddress>
      <ShipFromAddr>
        <Id>230</Id>
        <Line1>186 Westward Dr</Line1>
        <Line2>Miami Springs, FL  33166 US</Line2>
      </ShipFromAddr>
      <SalesTermRef name="Net 15">2</SalesTermRef>
      <DueDate>2021-05-13</DueDate>
      <TotalAmt>56.71</TotalAmt>
      <ApplyTaxAfterDiscount>false</ApplyTaxAfterDiscount>
      <PrintStatus>NeedToPrint</PrintStatus>
      <EmailStatus>NotSet</EmailStatus>
      <Balance>56.71</Balance>
      <TaxExemptionRef/>
      <AllowIPNPayment>false</AllowIPNPayment>
      <AllowOnlinePayment>false</AllowOnlinePayment>
      <AllowOnlineCreditCardPayment>false</AllowOnlineCreditCardPayment>
      <AllowOnlineACHPayment>false</AllowOnlineACHPayment>
    </Invoice>
  </QueryResponse>
</IntuitResponse>


