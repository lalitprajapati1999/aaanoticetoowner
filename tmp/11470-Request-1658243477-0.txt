REQUEST URI FOR SEQUENCE ID 11470
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..pHF5upRS0KBunFW7GeQG5A.Z4dQ8vBsGhGYEIxUStSYG1keH7_SF7q0QyR7naLQvUqIYUwUzfOW3zCkfnbD41B6JsX20cyg7xDgYNm7WM0cco8hb2Pgj1ptY91zT1LgQWCvcYP1av_JRmFCyI8ejXxyB1MxGKMEkLGqA4zt5n9hFSsiY9Humv-nS85jDWVWm5BoWBV2tgNqqu8s3n_g_AdNzGVpHAHIjr85EBLzH8tUdez_Fp8mkCWZAEvPBMESZvdca41qMro4C7kIjRiksoAPwX1_ACZUFuiogC0QhjgZ3roIB78S-8EzH8xI8vmp9Vef6DCPxYvx4-gnlHeNiuCPJ95qBNqqvu6xpn9HGGqc3XBG1QpiEtx2df8cHkrPWA7WWWoa_8lCd7rcRhHgoYk_6THaEQKuucLGFJjgNPe3yySlG4clsMXGvfaNqyqh35No7xLn3_1PnK0evsaW3ayCrWZDKXe0RUbWyBZamH7XKzKQGGAIJ_yTyJ6ST_7reHuy0_UzItW1InDgljYojbpClI54M9zUWzhiNpW2JsgId_0aHFdljcA8YPUGlj_sqX9n-IaR7_EcWaMKzGL0BoaxhMTmiZZoO24bT8-8sKnqqteTjyE7nmFkLWHW6A9SuuMSdLFAeQTtvfydg8X-4EHoLz8GUDaQH0A7Maj2iKPzf5jfnv8quDadzmlHioCTXK2oSQo02ir2gg65wtKZD686MdEXH52qRyME8LCtfT5-Kqg0Dd_tJxiHyi3TU78U_sc.nIbFQc0gfN9u7VVjNQoKEQ
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 1989

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>2600 N MILITARY TRAIL/ BOCA FL</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>777_1658243477</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - JONES &amp; JIRIK PLUMBING INC</ns0:Description>
    <ns0:Amount>35</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>35</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Firm Mail</ns0:Description>
    <ns0:Amount>3.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Firm Mail">18</ns0:ItemRef>
      <ns0:UnitPrice>3.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail with Electronic Return Receipt</ns0:Description>
    <ns0:Amount>22.98</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Electronic Return Receipt">15</ns0:ItemRef>
      <ns0:UnitPrice>7.66</ns0:UnitPrice>
      <ns0:Qty>3</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>36</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


