RESPONSE URI FOR SEQUENCE ID 04814
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/query?minorversion=47

RESPONSE HEADERS
================
date: Mon, 07 Mar 2022 17:06:10 GMT
content-type: application/xml;charset=UTF-8
content-length: 3669
intuit_tid: 1-62263b81-02d36b091eb7ec954812f40c
x-spanid: 9cee1393-c30f-4abc-9f39-0131d6e56ae5
x-amzn-trace-id: Root=1-62263b81-02d36b091eb7ec954812f40c
x-content-type-options: nosniff
x-xss-protection: 1; mode=block
qbo-version: 1975.230
service-time: total=242, db=119
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private
x-envoy-upstream-service-time: 269
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-03-07T09:06:09.953-08:00">
  <QueryResponse startPosition="1" maxResults="1" totalCount="1">
    <Invoice domain="QBO" sparse="false">
      <Id>345</Id>
      <SyncToken>2</SyncToken>
      <MetaData>
        <CreateTime>2022-02-10T02:48:26-08:00</CreateTime>
        <LastUpdatedTime>2022-03-07T08:42:45-08:00</LastUpdatedTime>
      </MetaData>
      <CustomField>
        <DefinitionId>2</DefinitionId>
        <Type>StringType</Type>
        <StringValue>Notice to Owner/Preliminary No</StringValue>
      </CustomField>
      <CustomField>
        <DefinitionId>3</DefinitionId>
        <Type>StringType</Type>
        <StringValue>3630 PALMETTO AVENUE MIAMI FLO</StringValue>
      </CustomField>
      <DocNumber>222_1644490105</DocNumber>
      <TxnDate>2022-02-10</TxnDate>
      <CurrencyRef name="United States Dollar">USD</CurrencyRef>
      <Line>
        <Id>1</Id>
        <LineNum>1</LineNum>
        <Description>Contracted By - Frank construction o octogonal construction corp</Description>
        <Amount>20.00</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="SERVICES:NTO">32</ItemRef>
          <UnitPrice>20</UnitPrice>
          <Qty>1</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>TAX</TaxCodeRef>
          <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Id>2</Id>
        <LineNum>2</LineNum>
        <Description>Firm Mail</Description>
        <Amount>0</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="MAILING:Firm Mail">18</ItemRef>
          <UnitPrice>3.95</UnitPrice>
          <Qty>0</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>TAX</TaxCodeRef>
          <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Amount>20.00</Amount>
        <DetailType>SubTotalLineDetail</DetailType>
        <SubTotalLineDetail/>
      </Line>
      <TxnTaxDetail>
        <TxnTaxCodeRef>3</TxnTaxCodeRef>
        <TotalTax>0</TotalTax>
        <TaxLine>
          <Amount>0</Amount>
          <DetailType>TaxLineDetail</DetailType>
          <TaxLineDetail>
            <TaxRateRef>3</TaxRateRef>
            <PercentBased>true</PercentBased>
            <TaxPercent>6</TaxPercent>
            <NetAmountTaxable>0</NetAmountTaxable>
          </TaxLineDetail>
        </TaxLine>
        <TaxLine>
          <Amount>0</Amount>
          <DetailType>TaxLineDetail</DetailType>
          <TaxLineDetail>
            <TaxRateRef>4</TaxRateRef>
            <PercentBased>true</PercentBased>
            <TaxPercent>1</TaxPercent>
            <NetAmountTaxable>0</NetAmountTaxable>
          </TaxLineDetail>
        </TaxLine>
      </TxnTaxDetail>
      <CustomerRef name="DAFRAN CONSTRUCTIONS CORP_1641675651">231</CustomerRef>
      <BillAddr>
        <Id>438</Id>
        <Line1>11921 WALSH BLVD</Line1>
        <City>Miami</City>
        <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
        <PostalCode>33184</PostalCode>
      </BillAddr>
      <ShipAddr>
        <Id>439</Id>
        <Line1>11921 WALSH BLVD</Line1>
        <City>Miami</City>
        <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
        <PostalCode>33184</PostalCode>
      </ShipAddr>
      <FreeFormAddress>true</FreeFormAddress>
      <ShipFromAddr>
        <Id>578</Id>
        <Line1>PO Box 22821</Line1>
        <Line2>Hialeah, FL  33002 US</Line2>
      </ShipFromAddr>
      <SalesTermRef name="Net 15">2</SalesTermRef>
      <DueDate>2022-02-25</DueDate>
      <TotalAmt>20.00</TotalAmt>
      <ApplyTaxAfterDiscount>false</ApplyTaxAfterDiscount>
      <PrintStatus>NeedToPrint</PrintStatus>
      <EmailStatus>EmailSent</EmailStatus>
      <BillEmail>
        <Address>dafranconstructions@hotmail.com</Address>
      </BillEmail>
      <Balance>20.00</Balance>
      <DeliveryInfo>
        <DeliveryType>Email</DeliveryType>
        <DeliveryTime>2022-03-07T08:42:42-08:00</DeliveryTime>
      </DeliveryInfo>
      <TaxExemptionRef name="SERVICE">99</TaxExemptionRef>
      <AllowIPNPayment>false</AllowIPNPayment>
      <AllowOnlinePayment>false</AllowOnlinePayment>
      <AllowOnlineCreditCardPayment>false</AllowOnlineCreditCardPayment>
      <AllowOnlineACHPayment>false</AllowOnlineACHPayment>
      <EInvoiceStatus>Sent</EInvoiceStatus>
    </Invoice>
  </QueryResponse>
</IntuitResponse>


