REQUEST URI FOR SEQUENCE ID 06361
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/customer?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..Ocxj8PW5oa1SqZmbv8QKdw.S4QTD4xcNwxTsv4QRKaIDisGnwEsfA6yrGBZ89oKMquYSBpdpZIVpZSv7HMV2wXBZsKvEcWJvKJuXNwns1oDckPG1xDgcZ88nSrPnsolPMaQNl9DZs-laUw8Sm6v2BWcFzxk5HHBebtVtIDquHieA_jc9Mi3ym_Iot5q0lUY58fA4rDMLk-ck65fgJM9LD4epTb8zFKucZOYFhs2HchzGeTUCwGTgYrbtekg84BXjgfP8NMRabIxj9N8Vuzi94S5dVL0WWIl6JqhIpepjqOPG3lORm7wYDVq3H0YXMiuMyTCj2W21V-cp3VnY6PfRpkuLcaCdRzOx9W0qP0V7BcBIFapg6watQzwapyobnOBcPc3sk2plDgh3MwYczP0rbf8b_ld689ogOPzjpR1V0j8weL6q9z9fv4vXNApgtYvrCxsEORRjv92SSbODuIKM93lCZsbOEziKDeYuzaPaGK0IFzxp87qBC8Jw0Pc3ZxkpXr00wnSZRnDty6PVKZNarm6mUHdwDv8Vw9_adNhDo6awcFL7dic3PBpNjXGETlRyEpcAKFH3P3nMjSd9FWu_fTfjR2OMZM5ZtFw4-xOm19LfnwV9Q6h9i2QMTp-kg8f0eUaHoNUFj-zYRmF3naEF7vNv-mAOATp0-TZRF2XkCeNjlkzHbNivfwN01je8vw4vFwCIm-yUJQsmfED4jA7rHjOyTDX0YRNtcnqyREnc7ICToamuFlukGy20MzB27kR0J8.Q3Kns_4-uzdH_LHcMo_jrw
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 979

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Customer xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:FullyQualifiedName>JH &amp; RJ SERVICE CORP</ns0:FullyQualifiedName>
  <ns0:CompanyName>JH &amp; RJ SERVICE CORP</ns0:CompanyName>
  <ns0:DisplayName>JH &amp; RJ SERVICE CORP_1648557019</ns0:DisplayName>
  <ns0:PrimaryPhone>
    <ns0:FreeFormNumber>(305) 699-7450</ns0:FreeFormNumber>
  </ns0:PrimaryPhone>
  <ns0:PrimaryEmailAddr>
    <ns0:Address>hrservicescorp1985@gmail.com</ns0:Address>
  </ns0:PrimaryEmailAddr>
  <ns0:BillAddr>
    <ns0:Line1>600 nw 32 pl</ns0:Line1>
    <ns0:City>Miami</ns0:City>
    <ns0:CountrySubDivisionCode>Florida</ns0:CountrySubDivisionCode>
    <ns0:PostalCode>33125</ns0:PostalCode>
  </ns0:BillAddr>
  <ns0:ShipAddr>
    <ns0:Line1>600 nw 32 pl</ns0:Line1>
    <ns0:City>Miami</ns0:City>
    <ns0:CountrySubDivisionCode>Florida</ns0:CountrySubDivisionCode>
    <ns0:PostalCode>33125</ns0:PostalCode>
  </ns0:ShipAddr>
</ns0:Customer>


