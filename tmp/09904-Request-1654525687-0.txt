REQUEST URI FOR SEQUENCE ID 09904
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..lLfyGPKWNx5cTxHvEo3vvA.4izwtt-Ll4FavhuUxFdlgFoyZqhCQMqGEqABeBuk1JZguyEwkZArCstTRrU07xwT7iv2idC_bjkGgPTvuyL987nu_-UxP5KKeoK8fG2m-wABaZiQfLBwGpD4bMOkWnOfzc9qeSdStd5Rm99oh-Qa_4o9A9qCXeXgbr5eDIe6hblV1FgCXJYFPpybqyTdJ4iqDHxAIIngtq6gmKTJGogKTE_VPk1JdOfoBNc2JtBWOVgWygXa-elG0DQk6Qrms7AbfDnp_8w4LskEheXwo06dV8H12JXghMpJ5TZMhUyx-F707CAelFhJCvxkcr5npb3hp45xhpk2QCbAkjMXveL_p6HNRl1SUqXvIg8p3NsLavS_lR9-aXsFxA_8cM7yr_oPkRVHWyxZ6gW4-LbZnFw6hjJFpjnRO8epqrgT-VBBlFzdXixcaed1Min4BN2uuwUJqSX6eWMrMdaVyXyWlNdGktFelVEcUuxoCN7VsuOOJz_gC0JRkjx5Nar6kVh9Jir1qiegKtdp9HdZi-idPQrWOAMCcIYkyS1uVv-M3xh7H7JoLOyPGgLbmikHyj-MDTBYbWdZJRKpSFvy5I5MbNMndy2XFEKKi42TCxFEb8PzYCb-6W3YT-Q3ea45WbwovPm5fELa40ATUZytRPUwndGi0SFLhmq4ED_yPxe6gARluN7Zx4a2V1klsWOFjKVWPn5ra11Fyt_zUp_3diaLlCO_zjTdO6McCmStQIVnvnY3q8o.Jy_tfBEj7HcSQtSnCiOzjw
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 2778

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>NTO 342- correction  recipient: G7 Holding LLC</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>5860 N BAY ROAD MIAMI BEACH 33</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>380_1654525687</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - POWER PLUMBING</ns0:Description>
    <ns0:Amount>30</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>30</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Rush Fee</ns0:Description>
    <ns0:Amount>12.50</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Rush Hour Charges">33</ns0:ItemRef>
      <ns0:UnitPrice>12.50</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Firm Mail</ns0:Description>
    <ns0:Amount>3.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Firm Mail">18</ns0:ItemRef>
      <ns0:UnitPrice>3.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail</ns0:Description>
    <ns0:Amount>16.26</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Certified Mail">17</ns0:ItemRef>
      <ns0:UnitPrice>5.42</ns0:UnitPrice>
      <ns0:Qty>3</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail with Electronic Return Receipt</ns0:Description>
    <ns0:Amount>7.22</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Electronic Return Receipt">15</ns0:ItemRef>
      <ns0:UnitPrice>7.22</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>36</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


