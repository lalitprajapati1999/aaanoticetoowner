RESPONSE URI FOR SEQUENCE ID 11031
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

RESPONSE HEADERS
================
date: Tue, 05 Jul 2022 17:00:12 GMT
content-type: application/xml
content-length: 509
intuit_tid: 1-62c46e1c-7f835cb436cdf4c6678fa28e
x-spanid: 1f6a273f-3fa5-4221-a639-78ed3828f05f
x-amzn-trace-id: Root=1-62c46e1c-7f835cb436cdf4c6678fa28e
x-content-type-options: nosniff
x-envoy-upstream-service-time: 377
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-07-05T10:00:12.622-07:00">
  <Fault type="ValidationFault">
    <Error code="6000" element="">
      <Message>A business validation error has occurred while processing your request</Message>
      <Detail>Business Validation Error: You cannot enter more than 31 characters in the sales_custom_1_val field. You tried entering CICC 7040-..., which is 40 characters.</Detail>
    </Error>
  </Fault>
</IntuitResponse>


