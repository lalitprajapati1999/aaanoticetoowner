RESPONSE URI FOR SEQUENCE ID 06090
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

RESPONSE HEADERS
================
date: Tue, 22 Mar 2022 16:59:17 GMT
content-type: application/xml;charset=UTF-8
content-length: 3860
intuit_tid: 1-623a0064-7e2d7fa73c127e283e12513e
x-spanid: 4d8110d1-1d23-42ca-879f-892674165c23
x-amzn-trace-id: Root=1-623a0064-7e2d7fa73c127e283e12513e
x-content-type-options: nosniff
x-xss-protection: 1; mode=block
qbo-version: 1976.152
service-time: total=611, db=264
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private
x-envoy-upstream-service-time: 639
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-03-22T09:59:16.753-07:00">
  <Invoice domain="QBO" sparse="false">
    <Id>666</Id>
    <SyncToken>0</SyncToken>
    <MetaData>
      <CreateTime>2022-03-22T09:59:17-07:00</CreateTime>
      <LastUpdatedTime>2022-03-22T09:59:17-07:00</LastUpdatedTime>
    </MetaData>
    <CustomField>
      <DefinitionId>2</DefinitionId>
      <Type>StringType</Type>
      <StringValue>Notice to Owner/Preliminary No</StringValue>
    </CustomField>
    <CustomField>
      <DefinitionId>3</DefinitionId>
      <Type>StringType</Type>
      <StringValue>7218 W 4 AVENUE HIALEAH FLORID</StringValue>
    </CustomField>
    <DocNumber>383_1647968356</DocNumber>
    <TxnDate>2022-03-22</TxnDate>
    <CurrencyRef name="United States Dollar">USD</CurrencyRef>
    <Line>
      <Id>1</Id>
      <LineNum>1</LineNum>
      <Description>Contracted By - FORTUNE CONSTRUCTION COMPANY LLP.</Description>
      <Amount>45.00</Amount>
      <DetailType>SalesItemLineDetail</DetailType>
      <SalesItemLineDetail>
        <ItemRef name="SERVICES:NTO">32</ItemRef>
        <UnitPrice>45</UnitPrice>
        <Qty>1</Qty>
        <ItemAccountRef name="Services">1</ItemAccountRef>
        <TaxCodeRef>TAX</TaxCodeRef>
        <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
      </SalesItemLineDetail>
    </Line>
    <Line>
      <Id>2</Id>
      <LineNum>2</LineNum>
      <Description>Certified Mail</Description>
      <Amount>5.42</Amount>
      <DetailType>SalesItemLineDetail</DetailType>
      <SalesItemLineDetail>
        <ItemRef name="MAILING:Certified Mail USPS">17</ItemRef>
        <UnitPrice>5.42</UnitPrice>
        <Qty>1</Qty>
        <ItemAccountRef name="Services">1</ItemAccountRef>
        <TaxCodeRef>TAX</TaxCodeRef>
        <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
      </SalesItemLineDetail>
    </Line>
    <Line>
      <Id>3</Id>
      <LineNum>3</LineNum>
      <Description>Certified Mail with Electronic Return Receipt</Description>
      <Amount>7.22</Amount>
      <DetailType>SalesItemLineDetail</DetailType>
      <SalesItemLineDetail>
        <ItemRef name="MAILING:Certified Mail ESRR">15</ItemRef>
        <UnitPrice>7.22</UnitPrice>
        <Qty>1</Qty>
        <ItemAccountRef name="Services">1</ItemAccountRef>
        <TaxCodeRef>TAX</TaxCodeRef>
        <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
      </SalesItemLineDetail>
    </Line>
    <Line>
      <Amount>57.64</Amount>
      <DetailType>SubTotalLineDetail</DetailType>
      <SubTotalLineDetail/>
    </Line>
    <TxnTaxDetail>
      <TxnTaxCodeRef>4</TxnTaxCodeRef>
      <TotalTax>4.03</TotalTax>
      <TaxLine>
        <Amount>0.57</Amount>
        <DetailType>TaxLineDetail</DetailType>
        <TaxLineDetail>
          <TaxRateRef>5</TaxRateRef>
          <PercentBased>true</PercentBased>
          <TaxPercent>1</TaxPercent>
          <NetAmountTaxable>57.64</NetAmountTaxable>
        </TaxLineDetail>
      </TaxLine>
      <TaxLine>
        <Amount>3.46</Amount>
        <DetailType>TaxLineDetail</DetailType>
        <TaxLineDetail>
          <TaxRateRef>3</TaxRateRef>
          <PercentBased>true</PercentBased>
          <TaxPercent>6</TaxPercent>
          <NetAmountTaxable>57.64</NetAmountTaxable>
        </TaxLineDetail>
      </TaxLine>
    </TxnTaxDetail>
    <CustomerRef name="COASTLINE PAINTING  SYSTEM INCORPORATE_1647490242">270</CustomerRef>
    <BillAddr>
      <Id>837</Id>
      <Line1>9810 S GRAND DUKE CIRCLE</Line1>
      <City>Fort lauderdale</City>
      <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
      <PostalCode>33321</PostalCode>
    </BillAddr>
    <ShipAddr>
      <Id>838</Id>
      <Line1>9810 S GRAND DUKE CIRCLE</Line1>
      <City>Fort lauderdale</City>
      <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
      <PostalCode>33321</PostalCode>
    </ShipAddr>
    <FreeFormAddress>true</FreeFormAddress>
    <ShipFromAddr>
      <Id>863</Id>
      <Line1>PO Box 22821</Line1>
      <Line2>Hialeah, FL  33002 US</Line2>
    </ShipFromAddr>
    <SalesTermRef name="Net 15">2</SalesTermRef>
    <DueDate>2022-04-06</DueDate>
    <TotalAmt>61.67</TotalAmt>
    <ApplyTaxAfterDiscount>false</ApplyTaxAfterDiscount>
    <PrintStatus>NeedToPrint</PrintStatus>
    <EmailStatus>NotSet</EmailStatus>
    <Balance>61.67</Balance>
    <TaxExemptionRef/>
    <AllowIPNPayment>false</AllowIPNPayment>
    <AllowOnlinePayment>false</AllowOnlinePayment>
    <AllowOnlineCreditCardPayment>false</AllowOnlineCreditCardPayment>
    <AllowOnlineACHPayment>false</AllowOnlineACHPayment>
  </Invoice>
</IntuitResponse>


