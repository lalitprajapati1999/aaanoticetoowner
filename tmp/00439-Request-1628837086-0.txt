REQUEST URI FOR SEQUENCE ID 00439
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/customer?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..2tT7oZyCcKagjFjPrI0_KA.iV3u71gOu1O-7Q8zuUYd88ddsPfg9n212n5LzAYx-HwHgvp5hMkpxar1_w-j4jnSNlyVLH7z4RS1nZvU6HxB2HtWzcGJv5McPTImyqR13jUX178afhcYk9rPLwaFV68sHzEoUy11bLD1TUw_Jj9h_RUhZM7198-Z8Q_WlLBoOE7-KPXQ1yGJRo2seg-wr0eqVBIKWT3pvlNQgfCPWk-3ny34DMOU6s2iUIxIS_SvUQYcq0nMx8WCFD9-h-AdO7GFOb9VUohLzYrThYIeHk-QdOl7IewY-7NmtDZ6Rt5hHDFmVcJB5uykAz0q6QiAtuNP7rUhI2VufjoNjVnmPqsg6ra__SXrRzh6HA-MicTU8HWcrxTo7Z6AB2pG4VVYg2xDDb-_eMuA4mz_Tfee7zlWBXAOwhD4Jzg1uRCUnOYHIHFjus5W5TRTQP03UNbC6NKnstLQZzGqbTcAz8XY3_-K4izVNJz3__RiBv-UZ9hYw5KnHICXxVPHwmQTZMvy2ZWb_2KBHAuiAqkZCu906v9rdXvYYp92CN2hmq7Q8aCE1S-R5uja_lio9kmsaE6a__kwFJ9ms1plFAKjX8Qz2t5h2MvJeQE9qrLYw2vLaUVYP-2pJ4jK9iqs--5QBzTXhvQ4UBEhEn1Cbx03zfVrxq1sCdxkhQDUgx8n6nbk8Qpj-s-12ZzSIk471qkjaOnOHbMh44LIEEH5LrXvp1zJSimdEOHbacjBdx3MtOm_qzjWMfA.1BHqHJuALPdqLd87cxVatA
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 923

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Customer xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:FullyQualifiedName>Test company</ns0:FullyQualifiedName>
  <ns0:CompanyName>Test company</ns0:CompanyName>
  <ns0:DisplayName>Test company_1628837085</ns0:DisplayName>
  <ns0:PrimaryPhone>
    <ns0:FreeFormNumber>(305) 555-5555</ns0:FreeFormNumber>
  </ns0:PrimaryPhone>
  <ns0:PrimaryEmailAddr>
    <ns0:Address>kajalwaykole@gmail.com</ns0:Address>
  </ns0:PrimaryEmailAddr>
  <ns0:BillAddr>
    <ns0:Line1>teet</ns0:Line1>
    <ns0:City>Añasco</ns0:City>
    <ns0:CountrySubDivisionCode>Alaska</ns0:CountrySubDivisionCode>
    <ns0:PostalCode>42500</ns0:PostalCode>
  </ns0:BillAddr>
  <ns0:ShipAddr>
    <ns0:Line1>teet</ns0:Line1>
    <ns0:City>Añasco</ns0:City>
    <ns0:CountrySubDivisionCode>Alaska</ns0:CountrySubDivisionCode>
    <ns0:PostalCode>42500</ns0:PostalCode>
  </ns0:ShipAddr>
</ns0:Customer>


