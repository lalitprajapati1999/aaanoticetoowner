RESPONSE URI FOR SEQUENCE ID 08972
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/query?minorversion=47

RESPONSE HEADERS
================
date: Tue, 03 May 2022 11:11:05 GMT
content-type: application/xml;charset=UTF-8
content-length: 2746
intuit_tid: 1-62710dc9-007734773a2b1e0430b8dff4
x-spanid: 996bd0f5-56a4-4b90-94e3-99e859f111c4
x-amzn-trace-id: Root=1-62710dc9-007734773a2b1e0430b8dff4
x-content-type-options: nosniff
x-xss-protection: 1; mode=block
qbo-version: 1979.197
service-time: total=118, db=28
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private
x-envoy-upstream-service-time: 155
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-05-03T04:11:05.513-07:00">
  <QueryResponse startPosition="1" maxResults="1" totalCount="1">
    <Invoice domain="QBO" sparse="false">
      <Id>1495</Id>
      <SyncToken>1</SyncToken>
      <MetaData>
        <CreateTime>2022-05-03T04:02:44-07:00</CreateTime>
        <LastUpdatedTime>2022-05-03T04:09:56-07:00</LastUpdatedTime>
      </MetaData>
      <DocNumber>1015</DocNumber>
      <TxnDate>2022-05-03</TxnDate>
      <CurrencyRef name="United States Dollar">USD</CurrencyRef>
      <Line>
        <Id>1</Id>
        <LineNum>1</LineNum>
        <Description>459-6150 SW 118 ST -M&amp;N CONSTRUCTION</Description>
        <Amount>20.00</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="CANCELLED">44</ItemRef>
          <UnitPrice>20</UnitPrice>
          <Qty>1</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>NON</TaxCodeRef>
          <TaxClassificationRef>EUC-99990101-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Amount>20.00</Amount>
        <DetailType>SubTotalLineDetail</DetailType>
        <SubTotalLineDetail/>
      </Line>
      <TxnTaxDetail>
        <TxnTaxCodeRef>3</TxnTaxCodeRef>
        <TotalTax>0</TotalTax>
        <TaxLine>
          <Amount>0</Amount>
          <DetailType>TaxLineDetail</DetailType>
          <TaxLineDetail>
            <TaxRateRef>3</TaxRateRef>
            <PercentBased>true</PercentBased>
            <TaxPercent>6</TaxPercent>
            <NetAmountTaxable>0</NetAmountTaxable>
          </TaxLineDetail>
        </TaxLine>
        <TaxLine>
          <Amount>0</Amount>
          <DetailType>TaxLineDetail</DetailType>
          <TaxLineDetail>
            <TaxRateRef>4</TaxRateRef>
            <PercentBased>true</PercentBased>
            <TaxPercent>1</TaxPercent>
            <NetAmountTaxable>0</NetAmountTaxable>
          </TaxLineDetail>
        </TaxLine>
      </TxnTaxDetail>
      <CustomerRef name="ERNESTO HERNANDEZ">195</CustomerRef>
      <BillAddr>
        <Id>196</Id>
        <Line1>6945 NW 53 TERRACE</Line1>
        <City>MIAMI</City>
        <CountrySubDivisionCode>FLOIRDA</CountrySubDivisionCode>
        <PostalCode>33166</PostalCode>
      </BillAddr>
      <ShipAddr>
        <Id>196</Id>
        <Line1>6945 NW 53 TERRACE</Line1>
        <City>MIAMI</City>
        <CountrySubDivisionCode>FLOIRDA</CountrySubDivisionCode>
        <PostalCode>33166</PostalCode>
      </ShipAddr>
      <FreeFormAddress>true</FreeFormAddress>
      <ShipFromAddr>
        <Id>1101</Id>
        <Line1>PO Box 22821, Hialeah, FL, 33002, USA</Line1>
      </ShipFromAddr>
      <SalesTermRef name="Net 30">3</SalesTermRef>
      <DueDate>2022-06-02</DueDate>
      <TotalAmt>20.00</TotalAmt>
      <ApplyTaxAfterDiscount>false</ApplyTaxAfterDiscount>
      <PrintStatus>NotSet</PrintStatus>
      <EmailStatus>NotSet</EmailStatus>
      <BillEmail>
        <Address>ernesto@superiormix.net</Address>
      </BillEmail>
      <Balance>20.00</Balance>
      <TaxExemptionRef name="service">99</TaxExemptionRef>
      <AllowIPNPayment>false</AllowIPNPayment>
      <AllowOnlinePayment>false</AllowOnlinePayment>
      <AllowOnlineCreditCardPayment>true</AllowOnlineCreditCardPayment>
      <AllowOnlineACHPayment>true</AllowOnlineACHPayment>
    </Invoice>
  </QueryResponse>
</IntuitResponse>


