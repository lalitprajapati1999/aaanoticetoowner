RESPONSE URI FOR SEQUENCE ID 02728
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/query?minorversion=47

RESPONSE HEADERS
================
date: Wed, 19 Jan 2022 12:49:20 GMT
content-type: application/xml;charset=UTF-8
content-length: 4288
intuit_tid: 1-61e808d0-626a0efe48bbcca44f263988
x-spanid: c16cd5fe-2492-47b0-9af3-b53b82ab3357
x-amzn-trace-id: Root=1-61e808d0-626a0efe48bbcca44f263988
x-content-type-options: nosniff
qbo-version: 1972.171
service-time: total=143, db=31
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private
x-envoy-upstream-service-time: 167
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-01-19T04:49:20.351-08:00">
  <QueryResponse startPosition="1" maxResults="1" totalCount="1">
    <Invoice domain="QBO" sparse="false">
      <Id>224</Id>
      <SyncToken>0</SyncToken>
      <MetaData>
        <CreateTime>2022-01-19T04:26:18-08:00</CreateTime>
        <LastUpdatedTime>2022-01-19T04:26:18-08:00</LastUpdatedTime>
      </MetaData>
      <CustomField>
        <DefinitionId>2</DefinitionId>
        <Type>StringType</Type>
        <StringValue>Notice to Owner/Preliminary No</StringValue>
      </CustomField>
      <CustomField>
        <DefinitionId>3</DefinitionId>
        <Type>StringType</Type>
        <StringValue>2495 TRAPP AVENUE MIAMI FLORID</StringValue>
      </CustomField>
      <DocNumber>180_1642595178</DocNumber>
      <TxnDate>2022-01-19</TxnDate>
      <CurrencyRef name="United States Dollar">USD</CurrencyRef>
      <Line>
        <Id>1</Id>
        <LineNum>1</LineNum>
        <Description>Contracted By - STRUCTURA LLC</Description>
        <Amount>45.00</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="SERVICES:NTO">32</ItemRef>
          <UnitPrice>45</UnitPrice>
          <Qty>1</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>TAX</TaxCodeRef>
          <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Id>2</Id>
        <LineNum>2</LineNum>
        <Description>Rush Fee</Description>
        <Amount>8.50</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="SERVICES:Rush fee">33</ItemRef>
          <UnitPrice>8.5</UnitPrice>
          <Qty>1</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>TAX</TaxCodeRef>
          <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Id>3</Id>
        <LineNum>3</LineNum>
        <Description>Certified Mail</Description>
        <Amount>10.84</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="MAILING:Certified Mail USPS">17</ItemRef>
          <UnitPrice>5.42</UnitPrice>
          <Qty>2</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>TAX</TaxCodeRef>
          <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Id>4</Id>
        <LineNum>4</LineNum>
        <Description>Certified Mail with Electronic Return Receipt</Description>
        <Amount>7.22</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="MAILING:Certified Mail ESRR">15</ItemRef>
          <UnitPrice>7.22</UnitPrice>
          <Qty>1</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>TAX</TaxCodeRef>
          <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Amount>71.56</Amount>
        <DetailType>SubTotalLineDetail</DetailType>
        <SubTotalLineDetail/>
      </Line>
      <TxnTaxDetail>
        <TxnTaxCodeRef>3</TxnTaxCodeRef>
        <TotalTax>5.01</TotalTax>
        <TaxLine>
          <Amount>4.29</Amount>
          <DetailType>TaxLineDetail</DetailType>
          <TaxLineDetail>
            <TaxRateRef>3</TaxRateRef>
            <PercentBased>true</PercentBased>
            <TaxPercent>6</TaxPercent>
            <NetAmountTaxable>71.56</NetAmountTaxable>
          </TaxLineDetail>
        </TaxLine>
        <TaxLine>
          <Amount>0.72</Amount>
          <DetailType>TaxLineDetail</DetailType>
          <TaxLineDetail>
            <TaxRateRef>4</TaxRateRef>
            <PercentBased>true</PercentBased>
            <TaxPercent>1</TaxPercent>
            <NetAmountTaxable>71.56</NetAmountTaxable>
          </TaxLineDetail>
        </TaxLine>
      </TxnTaxDetail>
      <CustomerRef name="DAFRAN CONSTRUCTIONS CORP_1641675651">231</CustomerRef>
      <BillAddr>
        <Id>438</Id>
        <Line1>11921 WALSH BLVD</Line1>
        <City>Miami</City>
        <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
        <PostalCode>33184</PostalCode>
      </BillAddr>
      <ShipAddr>
        <Id>439</Id>
        <Line1>11921 WALSH BLVD</Line1>
        <City>Miami</City>
        <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
        <PostalCode>33184</PostalCode>
      </ShipAddr>
      <FreeFormAddress>true</FreeFormAddress>
      <ShipFromAddr>
        <Id>501</Id>
        <Line1>PO Box 22821</Line1>
        <Line2>Hialeah, FL  33002 US</Line2>
      </ShipFromAddr>
      <SalesTermRef name="Net 15">2</SalesTermRef>
      <DueDate>2022-02-03</DueDate>
      <TotalAmt>76.57</TotalAmt>
      <ApplyTaxAfterDiscount>false</ApplyTaxAfterDiscount>
      <PrintStatus>NeedToPrint</PrintStatus>
      <EmailStatus>NotSet</EmailStatus>
      <Balance>76.57</Balance>
      <TaxExemptionRef/>
      <AllowIPNPayment>false</AllowIPNPayment>
      <AllowOnlinePayment>false</AllowOnlinePayment>
      <AllowOnlineCreditCardPayment>false</AllowOnlineCreditCardPayment>
      <AllowOnlineACHPayment>false</AllowOnlineACHPayment>
    </Invoice>
  </QueryResponse>
</IntuitResponse>


