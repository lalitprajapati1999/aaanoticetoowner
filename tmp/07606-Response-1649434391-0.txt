RESPONSE URI FOR SEQUENCE ID 07606
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

RESPONSE HEADERS
================
date: Fri, 08 Apr 2022 16:13:11 GMT
content-type: application/xml
content-length: 509
intuit_tid: 1-62505f17-23ad2f927f70c37209a21848
x-spanid: 50f3cca7-b523-4a3b-8e13-12c77a104d79
x-amzn-trace-id: Root=1-62505f17-23ad2f927f70c37209a21848
x-content-type-options: nosniff
x-envoy-upstream-service-time: 591
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-04-08T09:13:11.252-07:00">
  <Fault type="ValidationFault">
    <Error code="6000" element="">
      <Message>A business validation error has occurred while processing your request</Message>
      <Detail>Business Validation Error: You cannot enter more than 31 characters in the sales_custom_1_val field. You tried entering NTO 342- c..., which is 46 characters.</Detail>
    </Error>
  </Fault>
</IntuitResponse>


