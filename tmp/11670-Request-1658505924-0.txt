REQUEST URI FOR SEQUENCE ID 11670
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..TRz2nLgVMu0PU4yrR0VoLQ.0NachxTcfVjp6UCXCqzG3iUVw6YcNcxStJc_qikUn9K3CC5s9xmdSkZt-vC-n9cvv4cJrSF6B9TDxIg1PFWbBeONKgmvXe846uROx4vqkkqfuzPlzVKmB9fPh5SARs0Fv6is-9BlJIb3loLrgJ4DwOVwwJZkx3wf--V8bXs4b2A655J2aUfPuyhxBrknA2Ifcfo2fHypEcKaSFB3K7W_mbXfzHQWtUMF_nHXsKUZMxpTNnu7wFXugmKKfhXGammjqQpaGOaY7sufBfj5V7BcKH4pRyu4YTiEr3To5MK4-qV7U4l3jAR4HLoAY3gTszaugnwlwEZA00fWK_UsWDml-qL13iiRP_cVnRg4UDG3zT9fkN3ZFLvA-l2s1hwbKwPu9VaVK2S5ZgigI_GKQ6vhdOwDcYgdHqfvuh7WNgcqXbZhN29hhnqhD_74WKFeaidSX89QAszbjuifUGvT9_Kyx0lNdVBkX7MpLPIDdCReghE2VYU3yEhKddKpZYW4uBxXKdKa4UlE7gbyynD8VrrtLJtEfNdjSq5Miz8mdS3wPki0HtofcQ61Mn44el3T8YyT7nOGNUBYZF45E8im1APwkPKst8fppREK0c8oSK2KPA9K_4iEBVgekNdN7TwPLkXgUZ_hVuxomeJpW3Uu2sNiLZgpjt9ynlFrkX77RYPuBLOQDMj6jtCkKwzlStVVdlbvXpxgYy98tHUaFHvpHR8KYvBMAbCmy_7UIzsuIhhAxUU.tG7rLoU9kpOvmWKEm1ndzw
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 2338

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>130 HAMPTON LANE KEY BISCAYNE </ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>840_1658505924</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - GILMAR BUILDERS</ns0:Description>
    <ns0:Amount>20.00</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>20.00</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Rush Fee</ns0:Description>
    <ns0:Amount>8.50</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Rush Hour Charges">33</ns0:ItemRef>
      <ns0:UnitPrice>8.50</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Firm Mail</ns0:Description>
    <ns0:Amount>3.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Firm Mail">18</ns0:ItemRef>
      <ns0:UnitPrice>3.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail with Electronic Return Receipt</ns0:Description>
    <ns0:Amount>15.32</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Electronic Return Receipt">15</ns0:ItemRef>
      <ns0:UnitPrice>7.66</ns0:UnitPrice>
      <ns0:Qty>2</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>195</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


