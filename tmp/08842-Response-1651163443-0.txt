RESPONSE URI FOR SEQUENCE ID 08842
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

RESPONSE HEADERS
================
date: Thu, 28 Apr 2022 16:30:43 GMT
content-type: application/xml
content-length: 509
intuit_tid: 1-626ac133-563b13a620a763f17cca6835
x-spanid: 7919b0d2-33a5-4cac-9a7e-aca734792845
x-amzn-trace-id: Root=1-626ac133-563b13a620a763f17cca6835
x-content-type-options: nosniff
x-envoy-upstream-service-time: 462
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-04-28T09:30:43.339-07:00">
  <Fault type="ValidationFault">
    <Error code="6000" element="">
      <Message>A business validation error has occurred while processing your request</Message>
      <Detail>Business Validation Error: You cannot enter more than 31 characters in the sales_custom_1_val field. You tried entering CICC 7040-..., which is 40 characters.</Detail>
    </Error>
  </Fault>
</IntuitResponse>


