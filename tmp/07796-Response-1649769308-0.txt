RESPONSE URI FOR SEQUENCE ID 07796
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

RESPONSE HEADERS
================
date: Tue, 12 Apr 2022 13:15:08 GMT
content-type: application/xml
content-length: 509
intuit_tid: 1-62557b5b-7c5ff320155822560a9986d3
x-spanid: a614002f-8b8f-4088-9ded-b7a65da932b1
x-amzn-trace-id: Root=1-62557b5b-7c5ff320155822560a9986d3
x-content-type-options: nosniff
x-envoy-upstream-service-time: 756
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-04-12T06:15:07.840-07:00">
  <Fault type="ValidationFault">
    <Error code="6000" element="">
      <Message>A business validation error has occurred while processing your request</Message>
      <Detail>Business Validation Error: You cannot enter more than 31 characters in the sales_custom_1_val field. You tried entering NTO 342- c..., which is 46 characters.</Detail>
    </Error>
  </Fault>
</IntuitResponse>


