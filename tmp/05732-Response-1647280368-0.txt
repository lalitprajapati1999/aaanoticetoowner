RESPONSE URI FOR SEQUENCE ID 05732
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/query?minorversion=47

RESPONSE HEADERS
================
date: Mon, 14 Mar 2022 17:52:48 GMT
content-type: application/xml;charset=UTF-8
content-length: 4529
intuit_tid: 1-622f80f0-7f608f5239adad8904c32b15
x-spanid: 74597049-6ec1-4290-9152-9a534dd20fcc
x-amzn-trace-id: Root=1-622f80f0-7f608f5239adad8904c32b15
x-content-type-options: nosniff
x-xss-protection: 1; mode=block
qbo-version: 1976.152
service-time: total=232, db=84
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private
x-envoy-upstream-service-time: 254
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-03-14T10:52:48.372-07:00">
  <QueryResponse startPosition="1" maxResults="1" totalCount="1">
    <Invoice domain="QBO" sparse="false">
      <Id>381</Id>
      <SyncToken>2</SyncToken>
      <MetaData>
        <CreateTime>2022-02-24T06:04:35-08:00</CreateTime>
        <LastUpdatedTime>2022-03-14T09:59:20-07:00</LastUpdatedTime>
      </MetaData>
      <CustomField>
        <DefinitionId>2</DefinitionId>
        <Type>StringType</Type>
        <StringValue>Notice to Owner/Preliminary No</StringValue>
      </CustomField>
      <CustomField>
        <DefinitionId>3</DefinitionId>
        <Type>StringType</Type>
        <StringValue>3801 COLLINS AVENUE MIAMI BEAC</StringValue>
      </CustomField>
      <DocNumber>293_1645711475</DocNumber>
      <TxnDate>2022-02-24</TxnDate>
      <CurrencyRef name="United States Dollar">USD</CurrencyRef>
      <Line>
        <Id>1</Id>
        <LineNum>1</LineNum>
        <Description>Contracted By - CRUZ CONSTRUCTION GROUP, INC.</Description>
        <Amount>45.00</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="SERVICES:NTO">32</ItemRef>
          <UnitPrice>45</UnitPrice>
          <Qty>1</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>NON</TaxCodeRef>
          <TaxClassificationRef>EUC-99990201-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Id>2</Id>
        <LineNum>2</LineNum>
        <Description>Rush Fee</Description>
        <Amount>15.00</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="SERVICES:Rush fee">33</ItemRef>
          <UnitPrice>15</UnitPrice>
          <Qty>1</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>NON</TaxCodeRef>
          <TaxClassificationRef>EUC-99990201-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Id>3</Id>
        <LineNum>3</LineNum>
        <Description>Firm Mail</Description>
        <Amount>3.95</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="MAILING:Firm Mail">18</ItemRef>
          <UnitPrice>3.95</UnitPrice>
          <Qty>1</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>NON</TaxCodeRef>
          <TaxClassificationRef>EUC-99990201-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Id>4</Id>
        <LineNum>4</LineNum>
        <Description>Certified Mail with Electronic Return Receipt</Description>
        <Amount>7.22</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="MAILING:Certified Mail ESRR">15</ItemRef>
          <UnitPrice>7.22</UnitPrice>
          <Qty>1</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>NON</TaxCodeRef>
          <TaxClassificationRef>EUC-99990201-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Amount>71.17</Amount>
        <DetailType>SubTotalLineDetail</DetailType>
        <SubTotalLineDetail/>
      </Line>
      <TxnTaxDetail>
        <TxnTaxCodeRef>3</TxnTaxCodeRef>
        <TotalTax>0</TotalTax>
        <TaxLine>
          <Amount>0</Amount>
          <DetailType>TaxLineDetail</DetailType>
          <TaxLineDetail>
            <TaxRateRef>3</TaxRateRef>
            <PercentBased>true</PercentBased>
            <TaxPercent>6</TaxPercent>
            <NetAmountTaxable>0</NetAmountTaxable>
          </TaxLineDetail>
        </TaxLine>
        <TaxLine>
          <Amount>0</Amount>
          <DetailType>TaxLineDetail</DetailType>
          <TaxLineDetail>
            <TaxRateRef>4</TaxRateRef>
            <PercentBased>true</PercentBased>
            <TaxPercent>1</TaxPercent>
            <NetAmountTaxable>0</NetAmountTaxable>
          </TaxLineDetail>
        </TaxLine>
      </TxnTaxDetail>
      <CustomerRef name="J&amp;S CONTRACTOR PAINTERS CORP_1645545632">263</CustomerRef>
      <BillAddr>
        <Id>625</Id>
        <Line1>19951 NW 77 CT</Line1>
        <City>Hialeah</City>
        <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
        <PostalCode>33015</PostalCode>
      </BillAddr>
      <ShipAddr>
        <Id>626</Id>
        <Line1>19951 NW 77 CT</Line1>
        <City>Hialeah</City>
        <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
        <PostalCode>33015</PostalCode>
      </ShipAddr>
      <FreeFormAddress>true</FreeFormAddress>
      <ShipFromAddr>
        <Id>630</Id>
        <Line1>PO Box 22821</Line1>
        <Line2>Hialeah, FL  33002 US</Line2>
      </ShipFromAddr>
      <SalesTermRef name="Net 15">2</SalesTermRef>
      <DueDate>2022-03-11</DueDate>
      <TotalAmt>71.17</TotalAmt>
      <ApplyTaxAfterDiscount>false</ApplyTaxAfterDiscount>
      <PrintStatus>NeedToPrint</PrintStatus>
      <EmailStatus>EmailSent</EmailStatus>
      <BillEmail>
        <Address>scardonas1@aol.com</Address>
      </BillEmail>
      <Balance>71.17</Balance>
      <DeliveryInfo>
        <DeliveryType>Email</DeliveryType>
        <DeliveryTime>2022-03-14T09:59:20-07:00</DeliveryTime>
      </DeliveryInfo>
      <TaxExemptionRef name="SERVICE">99</TaxExemptionRef>
      <AllowIPNPayment>false</AllowIPNPayment>
      <AllowOnlinePayment>false</AllowOnlinePayment>
      <AllowOnlineCreditCardPayment>false</AllowOnlineCreditCardPayment>
      <AllowOnlineACHPayment>false</AllowOnlineACHPayment>
      <EInvoiceStatus>Sent</EInvoiceStatus>
    </Invoice>
  </QueryResponse>
</IntuitResponse>


