RESPONSE URI FOR SEQUENCE ID 02480
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/customer?minorversion=47

RESPONSE HEADERS
================
date: Thu, 13 Jan 2022 19:47:01 GMT
content-type: application/xml;charset=UTF-8
content-length: 1431
intuit_tid: 1-61e081b4-6a3f2ee85bad35d52aa2d25e
x-spanid: e8591783-75a6-4b86-9965-cded5d96dfaf
x-amzn-trace-id: Root=1-61e081b4-6a3f2ee85bad35d52aa2d25e
x-content-type-options: nosniff
qbo-version: 1971.195
service-time: total=249, db=42
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private
x-envoy-upstream-service-time: 289
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-01-13T11:47:00.825-08:00">
  <Customer domain="QBO" sparse="false">
    <Id>236</Id>
    <SyncToken>0</SyncToken>
    <MetaData>
      <CreateTime>2022-01-13T11:47:00-08:00</CreateTime>
      <LastUpdatedTime>2022-01-13T11:47:00-08:00</LastUpdatedTime>
    </MetaData>
    <FullyQualifiedName>ARTISTIC STONES INC_1642103220</FullyQualifiedName>
    <CompanyName>ARTISTIC STONES INC</CompanyName>
    <DisplayName>ARTISTIC STONES INC_1642103220</DisplayName>
    <PrintOnCheckName>ARTISTIC STONES INC</PrintOnCheckName>
    <Active>true</Active>
    <PrimaryPhone>
      <FreeFormNumber>(305) 836-0449</FreeFormNumber>
    </PrimaryPhone>
    <PrimaryEmailAddr>
      <Address>Artstones@aol.com</Address>
    </PrimaryEmailAddr>
    <DefaultTaxCodeRef>3</DefaultTaxCodeRef>
    <Taxable>true</Taxable>
    <BillAddr>
      <Id>464</Id>
      <Line1>6990 NW 35 AVE</Line1>
      <City>Miami</City>
      <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
      <PostalCode>33147</PostalCode>
    </BillAddr>
    <ShipAddr>
      <Id>465</Id>
      <Line1>6990 NW 35 AVE</Line1>
      <City>Miami</City>
      <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
      <PostalCode>33147</PostalCode>
    </ShipAddr>
    <Job>false</Job>
    <BillWithParent>false</BillWithParent>
    <Balance>0</Balance>
    <BalanceWithJobs>0</BalanceWithJobs>
    <CurrencyRef name="United States Dollar">USD</CurrencyRef>
    <PreferredDeliveryMethod>None</PreferredDeliveryMethod>
    <IsProject>false</IsProject>
  </Customer>
</IntuitResponse>


