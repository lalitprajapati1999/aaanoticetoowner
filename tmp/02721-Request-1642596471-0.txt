REQUEST URI FOR SEQUENCE ID 02721
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..j36rzxrpTUAR9fvUIdq74g.FoDyek2XHKmXyqdoHuzaDFs9cqbIeUlZl-4NAU9e7_TaQD-4LNkVECK7nVOTv2EuCrrCkOy-WIZonLTHTMDs_iMPXYoHQDtJansq5hQbeUdur99LmyEJ-maHCqN34EQ4ap2BGlGUo5QRULwhEclMgf_dFcMJomD_R9F0XfFmwjWHAGPUjhpSjaJhh0uZuXAye2SOIs4fQj7yYKV_R0aRvXOAVKD7vn9J68m-74ZsPDXig9_OgfrKXXKYAE9ufqLvKfqHlYgXrU33LLlExmjiy4RH9e7U9DdLKBZdn_FJxeqW2-41wsv41diFYP3ERZPgqD982EeVM_nTU8NVKUqG7CngRrx0qi3fe5xqejdvnRCJ3ge8zPOkLmfXZXDQrsiX2IiyD_7ruf4EU1mQfUPIGYH__23II6ZkgH2IB8zKFs91TUixA7NSWYebLqI6Rl-6isP6LrlV_jBmeOtBnm2Wxvu1W0_4gj1pcf1TLOZSAIJ6YDqm6UCUNaxTdlCAwd7N4ozEjlREwgmAaFxoa8M-h9MOz67CNwuH2Q7KoHfRVSSUrfRVRdtFM_3N7xEdDJ76zL8kVsYPNaoWiPFe_ChWSKpKbqVIJLxemtX143i2k6fPpjdRuBjedybiDdcotHX6LKxf8prv3FpnXw-o2LwRiavXB6jW2MoGqR6Z8ooV7gRKi-MOAugxiyr1GvDrPvMB2EFYU45c1qbiyKWDJsSbauc8umuCS9JWm1WDSsEHlac.2nLE1fAgGkk7kIhgl6JGVQ
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 2311

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>7855 NW 41 STREET DORAL FLORID</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>188_1642596471</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - GO TILT CONSTRUCTION LLC</ns0:Description>
    <ns0:Amount>45.00</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>45.00</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Rush Fee</ns0:Description>
    <ns0:Amount>8.50</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Rush Hour Charges">33</ns0:ItemRef>
      <ns0:UnitPrice>8.50</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail</ns0:Description>
    <ns0:Amount>27.1</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Certified Mail">17</ns0:ItemRef>
      <ns0:UnitPrice>5.42</ns0:UnitPrice>
      <ns0:Qty>5</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>C70202450000167261731-188</ns0:Description>
    <ns0:Amount>0</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Manual">19</ns0:ItemRef>
      <ns0:UnitPrice>0</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>224</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


