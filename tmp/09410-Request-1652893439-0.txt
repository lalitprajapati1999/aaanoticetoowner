REQUEST URI FOR SEQUENCE ID 09410
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..rLwYRuxjbAAXGeWIHNktfg.aichx2iQbzmOjPM__anC56uTPy79tEsevudqsPJ-IpEkCsU9hVv-YZnOg3EGCzTTHAdw5e7wiM9CEaCTSgKkuiYWmN7O9ByDlLB4AjohsUKpaMkmj8koItQuO5mlDtiubxHevZUf8NNaXT_M6QNwVyfM_o928Ls1Pvfk95xMdF4v3wd9jAyR1c7jEJXPZcN-Tqd_NoRfAHbIdMDiOWp_rqsAiF7CkIOwtrEbZSimCYL9_LBvjcXQOT2xgToDCc_PVraN_cEnqKwUlGyXuBjOK6wqVip4QtBeRjDKBoVxF-VzOkXYs0HghVKfdxS9rRCiDW2yBtSF5UBK6kwgqwIzCmS2KJUAQ2ZwCw1Kqxpr-DC9zlvf0Tok73g-Y_raTHJkGF_e1x171w4hgRTrq_W--akFdnQ_StVoDCJKA1kpchfhLyUNepHX_3GFcU8OxOzrOtJmmdL873fIh5R5xAlCGDb0CD0x6NPnCoX9GOicZzUqYjEYjZ7pokflpuBEx8wdulIXIYWXMFctX-bJeScExc_xU6EtEXvqn-k3qqSpQLMKTemncO76Wa8XrjIYhin9qhUPb1D7op4O8ttAYSQL13IhPXIRrgwXNhD4_eZ6x-fzwSe9fvjcPzSjlxW1GN_qLbX-G-6sefp6iY1WOSzvUgvOOCVQBb7tYWOiUFtnwnJ_vBb5n4HtxMFUFmpoG0nJLZk4KZP_ZmJ8mkqqFEfWgCzWMnJsAe400TwzcAaBfAI.Ltcj56kXy1e2ZCUDR66pPg
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 2005

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>PO 9999</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>1420 NW 15 AVENUE MIAMI FLORID</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>101_1652893439</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - PROTON BUILDERS LLC</ns0:Description>
    <ns0:Amount>25.00</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>25.00</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Additional address charges</ns0:Description>
    <ns0:Amount>60</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Additional Address">34</ns0:ItemRef>
      <ns0:UnitPrice>10.00</ns0:UnitPrice>
      <ns0:Qty>6</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Firm Mail</ns0:Description>
    <ns0:Amount>11.85</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Firm Mail">18</ns0:ItemRef>
      <ns0:UnitPrice>3.95</ns0:UnitPrice>
      <ns0:Qty>3</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>213</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


