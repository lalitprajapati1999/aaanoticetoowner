RESPONSE URI FOR SEQUENCE ID 10211
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

RESPONSE HEADERS
================
date: Fri, 10 Jun 2022 13:08:35 GMT
content-type: application/xml
content-length: 509
intuit_tid: 1-62a34253-075afd591060913d453857df
x-spanid: baf31450-c9cc-4f21-825a-341265c7d8f7
x-amzn-trace-id: Root=1-62a34253-075afd591060913d453857df
x-content-type-options: nosniff
x-envoy-upstream-service-time: 565
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-06-10T06:08:35.343-07:00">
  <Fault type="ValidationFault">
    <Error code="6000" element="">
      <Message>A business validation error has occurred while processing your request</Message>
      <Detail>Business Validation Error: You cannot enter more than 31 characters in the sales_custom_1_val field. You tried entering CICC 7040-..., which is 40 characters.</Detail>
    </Error>
  </Fault>
</IntuitResponse>


