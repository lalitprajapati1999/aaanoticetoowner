RESPONSE URI FOR SEQUENCE ID 09307
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

RESPONSE HEADERS
================
date: Wed, 18 May 2022 15:59:42 GMT
content-type: application/xml
content-length: 509
intuit_tid: 1-628517ee-0e5124f77d991c2455b60be4
x-spanid: 16dcf7d0-7ae8-42e9-b3bb-7aaf0e838210
x-amzn-trace-id: Root=1-628517ee-0e5124f77d991c2455b60be4
x-content-type-options: nosniff
x-envoy-upstream-service-time: 433
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-05-18T08:59:42.160-07:00">
  <Fault type="ValidationFault">
    <Error code="6000" element="">
      <Message>A business validation error has occurred while processing your request</Message>
      <Detail>Business Validation Error: You cannot enter more than 31 characters in the sales_custom_1_val field. You tried entering CICC 7040-..., which is 40 characters.</Detail>
    </Error>
  </Fault>
</IntuitResponse>


