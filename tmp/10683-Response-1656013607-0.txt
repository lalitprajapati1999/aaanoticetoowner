RESPONSE URI FOR SEQUENCE ID 10683
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

RESPONSE HEADERS
================
date: Thu, 23 Jun 2022 19:46:47 GMT
content-type: application/xml
content-length: 509
intuit_tid: 1-62b4c327-7ad2dcee77bc229829f03094
x-spanid: f215ab3f-3471-4853-882c-c9bfd10aaace
x-amzn-trace-id: Root=1-62b4c327-7ad2dcee77bc229829f03094
x-content-type-options: nosniff
x-envoy-upstream-service-time: 544
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-06-23T12:46:47.378-07:00">
  <Fault type="ValidationFault">
    <Error code="6000" element="">
      <Message>A business validation error has occurred while processing your request</Message>
      <Detail>Business Validation Error: You cannot enter more than 31 characters in the sales_custom_1_val field. You tried entering NTO 342- c..., which is 46 characters.</Detail>
    </Error>
  </Fault>
</IntuitResponse>


