REQUEST URI FOR SEQUENCE ID 00499
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/customer?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..QmXUU1LKz1qdIvWbGg7krQ.F-3BhAKfWxrY7hE3oEZ7MtrXKEd5shkHDsFFRyb3s-QCihpuoOfXzYei6JsEIuPC2VWhuDrxxPbjlDYp7InWVdLSO_FGln_3EuEeBcND_qzRPQwH7Caw-FsRjY4imGLNo3bAUg9pxl4IXPCZ4i_iYjwRW844GPiSyPCBgqMwAzpSARWGy0hoNp8V6T42A1VFUtXcpjNhnIcoOA-6y00nNOX-3AbuxKOuSfsA-AUU0ykzwPvfHIQGEVPZH6vIQTRcaPrSAPL7I3pdEsNOMr9zFiuznf_okYzoem_c9AKsqyzuj6wtbf4JpF32qpCByRWiyqS9POOlO5MKXE1L1FQwwZy0vzvNzAvTRlG7Av35NNCqI2_1KyZiEeVpXuvOfy72Zu0UNkFylKALgOH64L_lMbXJxPSiPmJcARnezeYCcaQUn4B-Y8FzwZW0BZ9fXWXmJoqXoR5MYrgN3sfXPEfiMjbP9n1wmjhxiAvDzZ9zDdDISp-Ef9O2VPzKmI7pOBCb79ufB3XkIIiTbJEjzxgpV7xgsw3Mf9_pdISNVZhE76u-Vne9nxHKVOdKWRGTcfEEkEjLLYRoczykz5RHB52hu9YOtYCRuxepLMQkz-B_F_XR74Ng8wHSkZfpkcGgnEomsPWfdlZoA6B6lw58Kjx4SG1fNEfA7JJxpTq6kedemJgSQbRnGhUJFRK6CLAlGPWQOZ3EGlq35K10jSAbYGg6ZsjOXwCNVOFk26-mJqEkPOc.T9T9xtaV_eFGoOnCl7RmEw
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 898

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Customer xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:FullyQualifiedName>Test</ns0:FullyQualifiedName>
  <ns0:CompanyName>Test</ns0:CompanyName>
  <ns0:DisplayName>Test_1634826584</ns0:DisplayName>
  <ns0:PrimaryPhone>
    <ns0:FreeFormNumber>(456) 768-6897</ns0:FreeFormNumber>
  </ns0:PrimaryPhone>
  <ns0:PrimaryEmailAddr>
    <ns0:Address>kajal@gmail.com</ns0:Address>
  </ns0:PrimaryEmailAddr>
  <ns0:BillAddr>
    <ns0:Line1>ewrreg</ns0:Line1>
    <ns0:City>Añasco</ns0:City>
    <ns0:CountrySubDivisionCode>Alabama</ns0:CountrySubDivisionCode>
    <ns0:PostalCode>41101</ns0:PostalCode>
  </ns0:BillAddr>
  <ns0:ShipAddr>
    <ns0:Line1>ewrreg</ns0:Line1>
    <ns0:City>Añasco</ns0:City>
    <ns0:CountrySubDivisionCode>Alabama</ns0:CountrySubDivisionCode>
    <ns0:PostalCode>41101</ns0:PostalCode>
  </ns0:ShipAddr>
</ns0:Customer>


