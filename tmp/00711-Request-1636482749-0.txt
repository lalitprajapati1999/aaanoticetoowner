REQUEST URI FOR SEQUENCE ID 00711
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..z4ZBCYz9698BqNLh5DWjpw.Hty43iPwg9fDBrVCzT7uyCDUtuPrgIcHG78KvzndZ20GQkARi-6eKy_kJocBXNsmXiQC9y53zDvnj-lMsI3sYap3GAJ6Ch3oCQlYqmLo1qG_4iVjRl6iyLof_LqWuS41rhfNiRNMvZskY4FOcOevM_bmFYJ6IT3JyW5-t3LLNYAWWbsmRESCThvsGhKUKjM1Ij74BTlBTo-6eHeRSFC7fUkpcD_BwyPmLJcTqFjIBpwpSghEJUE6-hhOBlDR3T6vKNmFjYdy5yFAUx2D_5k1BdCZEYM1To2MxxRs9G8VF8Vi045fy_cmlxCy1iIXwK2Xap8DcuieLkeP6Jd7yk2uYpiJ6I8KihaTjT2W19rDbJNOg4p-Jq0Xf0VJQ-zyIHC7r6cod00rd_R0sKTvEtPO6dGUI4u6GQIzpnJlKY4fRseTedaTj6yXGm_jijhH6V_qW1UcQMGVhKzDFjMkN3xQ1bP_jXo9Y63vdS_iN75URRT8OFVv99pJvlkLFBe1n_ioulClSUS2lzOsbSYYBGDXvPRiNVmiUEYKCWj8McODYWPX9EEgi4qN-Ymla1i8jL5tG0DmbfqCe5vnhn6y_7tHReIALw3woUl492UC2pOYc9ViZiDvLJOboltPmUg0E513S1LpCwS9rC2sRITNtqacg5RWunShnaG4x0al0kLwfsO5cFkWbdeKMbPbmn7s4ieXyLOSSyiw5fp94md2-h72UutI9V_8vyboP1ORtHB3pRg.WH0qI8V9C-jJ0Rfp51K19Q
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 3081

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>725 S OCEAN BOULEVARD BOCA RAT</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>132_1636482749</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - SUNTECH PLUMBING CORP</ns0:Description>
    <ns0:Amount>25.00</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>25.00</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Additional address charges</ns0:Description>
    <ns0:Amount>30</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Additional Address">34</ns0:ItemRef>
      <ns0:UnitPrice>10.00</ns0:UnitPrice>
      <ns0:Qty>3</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Rush Fee</ns0:Description>
    <ns0:Amount>15.00</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Rush Hour Charges">33</ns0:ItemRef>
      <ns0:UnitPrice>15.00</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Firm Mail</ns0:Description>
    <ns0:Amount>11.85</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Firm Mail">18</ns0:ItemRef>
      <ns0:UnitPrice>3.95</ns0:UnitPrice>
      <ns0:Qty>3</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail</ns0:Description>
    <ns0:Amount>5.42</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Certified Mail">17</ns0:ItemRef>
      <ns0:UnitPrice>5.42</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail with Electronic Return Receipt</ns0:Description>
    <ns0:Amount>7.22</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Electronic Return Receipt">15</ns0:ItemRef>
      <ns0:UnitPrice>7.22</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>213</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


