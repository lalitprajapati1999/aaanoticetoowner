RESPONSE URI FOR SEQUENCE ID 09317
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

RESPONSE HEADERS
================
date: Wed, 18 May 2022 16:36:36 GMT
content-type: application/xml
content-length: 509
intuit_tid: 1-62852094-3c42f0e50e3f62fb3b44690a
x-spanid: e585ad13-b4d9-4a59-a9f8-fddfa38fea81
x-amzn-trace-id: Root=1-62852094-3c42f0e50e3f62fb3b44690a
x-content-type-options: nosniff
x-envoy-upstream-service-time: 384
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-05-18T09:36:36.278-07:00">
  <Fault type="ValidationFault">
    <Error code="6000" element="">
      <Message>A business validation error has occurred while processing your request</Message>
      <Detail>Business Validation Error: You cannot enter more than 31 characters in the sales_custom_1_val field. You tried entering CICC 7040-..., which is 40 characters.</Detail>
    </Error>
  </Fault>
</IntuitResponse>


