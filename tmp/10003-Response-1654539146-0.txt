RESPONSE URI FOR SEQUENCE ID 10003
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

RESPONSE HEADERS
================
date: Mon, 06 Jun 2022 18:12:26 GMT
content-type: application/xml
content-length: 509
intuit_tid: 1-629e4389-2ab1cb7b06f1bc2d16b5385f
x-spanid: 7c055af0-7104-4eb5-b7d7-bf73750c3896
x-amzn-trace-id: Root=1-629e4389-2ab1cb7b06f1bc2d16b5385f
x-content-type-options: nosniff
x-envoy-upstream-service-time: 597
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-06-06T11:12:25.563-07:00">
  <Fault type="ValidationFault">
    <Error code="6000" element="">
      <Message>A business validation error has occurred while processing your request</Message>
      <Detail>Business Validation Error: You cannot enter more than 31 characters in the sales_custom_1_val field. You tried entering CICC 7040-..., which is 40 characters.</Detail>
    </Error>
  </Fault>
</IntuitResponse>


