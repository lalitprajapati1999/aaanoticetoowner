RESPONSE URI FOR SEQUENCE ID 01036
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/customer?minorversion=47

RESPONSE HEADERS
================
date: Mon, 06 Dec 2021 19:16:45 GMT
content-type: application/xml;charset=UTF-8
content-length: 1503
intuit_tid: 1-61ae619d-70f99bb57d8ca690245c8565
x-spanid: 78203b01-17d9-41d5-ab4a-f8be81fcb2c3
x-amzn-trace-id: Root=1-61ae619d-70f99bb57d8ca690245c8565
x-content-type-options: nosniff
qbo-version: 1970.152
service-time: total=319, db=86
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private
x-envoy-upstream-service-time: 360
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2021-12-06T11:16:45.580-08:00">
  <Customer domain="QBO" sparse="false">
    <Id>224</Id>
    <SyncToken>0</SyncToken>
    <MetaData>
      <CreateTime>2021-12-06T11:16:45-08:00</CreateTime>
      <LastUpdatedTime>2021-12-06T11:16:45-08:00</LastUpdatedTime>
    </MetaData>
    <FullyQualifiedName>PROFESSIONAL FINISH TILT CORPORATION_1638818204</FullyQualifiedName>
    <CompanyName>PROFESSIONAL FINISH TILT CORPORATION</CompanyName>
    <DisplayName>PROFESSIONAL FINISH TILT CORPORATION_1638818204</DisplayName>
    <PrintOnCheckName>PROFESSIONAL FINISH TILT CORPORATION</PrintOnCheckName>
    <Active>true</Active>
    <PrimaryPhone>
      <FreeFormNumber>(786) 859-4134</FreeFormNumber>
    </PrimaryPhone>
    <PrimaryEmailAddr>
      <Address>profinishtilt@gmail.com</Address>
    </PrimaryEmailAddr>
    <DefaultTaxCodeRef>3</DefaultTaxCodeRef>
    <Taxable>true</Taxable>
    <BillAddr>
      <Id>423</Id>
      <Line1>PO BOX 473035</Line1>
      <City>Miami</City>
      <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
      <PostalCode>33147</PostalCode>
    </BillAddr>
    <ShipAddr>
      <Id>424</Id>
      <Line1>PO BOX 473035</Line1>
      <City>Miami</City>
      <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
      <PostalCode>33147</PostalCode>
    </ShipAddr>
    <Job>false</Job>
    <BillWithParent>false</BillWithParent>
    <Balance>0</Balance>
    <BalanceWithJobs>0</BalanceWithJobs>
    <CurrencyRef name="United States Dollar">USD</CurrencyRef>
    <PreferredDeliveryMethod>None</PreferredDeliveryMethod>
    <IsProject>false</IsProject>
  </Customer>
</IntuitResponse>


