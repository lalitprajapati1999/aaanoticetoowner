REQUEST URI FOR SEQUENCE ID 08649
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..2bGhqi4YqsbRATMYzheMag.Nyha7fQi--nQmt0waD9jSQR2WqXNPFXSAyeFZGecAd0V0M_mmoUcyYySSToc9LlsudRG2-RjuvLaEEJIsZDxU1I9x_xbCOrydERztoXft4v9jZ_el1ggxxchHinrzCpNfEM8H99VNd6eizfuXRTHmjX314FRoeqpUfcFVNg_t_BJu_RZX9iZXjU6U8eKz8zKd_MoRnvlnoGSCFtnhym1rk8kXH-O06WSkGnTOQI-dt8GjfcFB0jHAun338zEwcEl0WjGEFe_oxx9fNFIitrzoNQl_3O1XbHft8AbSSB5U-K-pJ_-2voeKAmAFUIyd3afRb9_qlTE8ac8CVRXHvMlULPJ8ElBMZ85Ahc3SPdnBpJE0Ipt-4NdzCmdgPomeLl_F1yrOqfi__5s-JMYgG7hsPugcbIg1ns9nXW_7LU53fOri-yzkOGJh--VoHPs12F35PtECJ8hCrWaYWmMfL96ryweSOm56TYIaul4xYO67sHvET2GUTd_44-D_8VAE__Js29nulTUwjDT7nFPe_v77AnIqtvQgHIt2yRsUTqFEDpyBP_fETeOimPYWuFBMIENaDT1_ZkPCNEJIIinxZx6paAYU3Vq1OrvykT_4wI-GMAZUuwU2K8nn6xRKycu9SDEXFCD3UhtZt2qW86YPzdEcGDFO8-oTZSp0LSreKRQwo4xDDCC7N_1SAi4zXULNRyaPxGzZoHj8YtLMRyYZj6ZkrkPBvFWkR0mBJsYBwOlMHI.MEF_elL3J2doXgfLtRC9sg
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 2697

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>7487 SW 104 ST PINECREST FL 33</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>533_1651076719</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - MACO GROUP LLC</ns0:Description>
    <ns0:Amount>20.00</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>20.00</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Rush Fee</ns0:Description>
    <ns0:Amount>8.50</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Rush Hour Charges">33</ns0:ItemRef>
      <ns0:UnitPrice>8.50</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Firm Mail</ns0:Description>
    <ns0:Amount>3.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Firm Mail">18</ns0:ItemRef>
      <ns0:UnitPrice>3.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail</ns0:Description>
    <ns0:Amount>10.84</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Certified Mail">17</ns0:ItemRef>
      <ns0:UnitPrice>5.42</ns0:UnitPrice>
      <ns0:Qty>2</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail with Electronic Return Receipt</ns0:Description>
    <ns0:Amount>7.22</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Electronic Return Receipt">15</ns0:ItemRef>
      <ns0:UnitPrice>7.22</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>195</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


