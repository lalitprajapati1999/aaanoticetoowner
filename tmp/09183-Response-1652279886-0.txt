RESPONSE URI FOR SEQUENCE ID 09183
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

RESPONSE HEADERS
================
date: Wed, 11 May 2022 14:38:06 GMT
content-type: application/xml
content-length: 509
intuit_tid: 1-627bca4e-1ee5abb27c848c9f4f3a46bb
x-spanid: 741627d0-7882-4ded-9287-5eef78b68f6f
x-amzn-trace-id: Root=1-627bca4e-1ee5abb27c848c9f4f3a46bb
x-content-type-options: nosniff
x-envoy-upstream-service-time: 468
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-05-11T07:38:06.257-07:00">
  <Fault type="ValidationFault">
    <Error code="6000" element="">
      <Message>A business validation error has occurred while processing your request</Message>
      <Detail>Business Validation Error: You cannot enter more than 31 characters in the sales_custom_1_val field. You tried entering NTO 342- c..., which is 46 characters.</Detail>
    </Error>
  </Fault>
</IntuitResponse>


