RESPONSE URI FOR SEQUENCE ID 00442
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/customer?minorversion=47

RESPONSE HEADERS
================
date: Tue, 17 Aug 2021 11:11:13 GMT
content-type: application/xml;charset=UTF-8
content-length: 1459
server: nginx
intuit_tid: 1-611b9951-78cf57fa2c0df54e2cd38fab
x-spanid: 6f5f57ad-c5cc-46a5-a25a-d1fab82d3367
x-amzn-trace-id: Root=1-611b9951-78cf57fa2c0df54e2cd38fab
x-content-type-options: nosniff
qbo-version: 1962.172
service-time: total=299, db=67
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private
strict-transport-security: max-age=31536000

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2021-08-17T04:11:13.188-07:00">
  <Customer domain="QBO" sparse="false">
    <Id>217</Id>
    <SyncToken>0</SyncToken>
    <MetaData>
      <CreateTime>2021-08-17T04:11:13-07:00</CreateTime>
      <LastUpdatedTime>2021-08-17T04:11:13-07:00</LastUpdatedTime>
    </MetaData>
    <FullyQualifiedName>Mar Construction LLC_1629198672</FullyQualifiedName>
    <CompanyName>Mar Construction LLC</CompanyName>
    <DisplayName>Mar Construction LLC_1629198672</DisplayName>
    <PrintOnCheckName>Mar Construction LLC</PrintOnCheckName>
    <Active>true</Active>
    <PrimaryPhone>
      <FreeFormNumber>(305) 305-6969</FreeFormNumber>
    </PrimaryPhone>
    <PrimaryEmailAddr>
      <Address>propertyllcmgmt@gmail.com</Address>
    </PrimaryEmailAddr>
    <DefaultTaxCodeRef>3</DefaultTaxCodeRef>
    <Taxable>true</Taxable>
    <BillAddr>
      <Id>304</Id>
      <Line1>1520 SW 5TH ST STE 201</Line1>
      <City>Miami</City>
      <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
      <PostalCode>33135</PostalCode>
    </BillAddr>
    <ShipAddr>
      <Id>305</Id>
      <Line1>1520 SW 5TH ST STE 201</Line1>
      <City>Miami</City>
      <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
      <PostalCode>33135</PostalCode>
    </ShipAddr>
    <Job>false</Job>
    <BillWithParent>false</BillWithParent>
    <Balance>0</Balance>
    <BalanceWithJobs>0</BalanceWithJobs>
    <CurrencyRef name="United States Dollar">USD</CurrencyRef>
    <PreferredDeliveryMethod>None</PreferredDeliveryMethod>
    <IsProject>false</IsProject>
  </Customer>
</IntuitResponse>


