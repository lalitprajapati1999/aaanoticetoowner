RESPONSE URI FOR SEQUENCE ID 11847
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

RESPONSE HEADERS
================
date: Mon, 25 Jul 2022 13:17:26 GMT
content-type: application/xml;charset=UTF-8
content-length: 3909
intuit_tid: 1-62de97e5-2fa4710b29b0f66a289717a5
x-spanid: 8bb6bf2b-2eb4-4b16-97fd-6b696e3fdac2
x-amzn-trace-id: Root=1-62de97e5-2fa4710b29b0f66a289717a5
x-content-type-options: nosniff
x-xss-protection: 1; mode=block
qbo-version: 1985.162
service-time: total=610, db=279
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private
x-envoy-upstream-service-time: 639
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-07-25T06:17:26.009-07:00">
  <Invoice domain="QBO" sparse="false">
    <Id>2078</Id>
    <SyncToken>0</SyncToken>
    <MetaData>
      <CreateTime>2022-07-25T06:17:26-07:00</CreateTime>
      <LastUpdatedTime>2022-07-25T06:17:26-07:00</LastUpdatedTime>
    </MetaData>
    <CustomField>
      <DefinitionId>2</DefinitionId>
      <Name>sales2</Name>
      <Type>StringType</Type>
      <StringValue>Notice to Owner/Preliminary No</StringValue>
    </CustomField>
    <CustomField>
      <DefinitionId>3</DefinitionId>
      <Name>sales3</Name>
      <Type>StringType</Type>
      <StringValue>160 Isles of Venice Drive Fort</StringValue>
    </CustomField>
    <DocNumber>116_CYO_1658755045</DocNumber>
    <TxnDate>2022-07-25</TxnDate>
    <CurrencyRef name="United States Dollar">USD</CurrencyRef>
    <Line>
      <Id>1</Id>
      <LineNum>1</LineNum>
      <Description>Notice to Owner/Preliminary Notice
Contracted By - GARY GLENEWINKEL CONSTRUCTION CORP
Parent Work Order Number - 95
04/21/2022</Description>
      <Amount>0.01</Amount>
      <DetailType>SalesItemLineDetail</DetailType>
      <SalesItemLineDetail>
        <ItemRef name="SERVICES:NTO">32</ItemRef>
        <UnitPrice>0.01</UnitPrice>
        <Qty>1</Qty>
        <ItemAccountRef name="Services">1</ItemAccountRef>
        <TaxCodeRef>NON</TaxCodeRef>
        <TaxClassificationRef>EUC-99990201-V1-00020000</TaxClassificationRef>
      </SalesItemLineDetail>
    </Line>
    <Line>
      <Id>2</Id>
      <LineNum>2</LineNum>
      <Description>this are additional address charges</Description>
      <Amount>6.00</Amount>
      <DetailType>SalesItemLineDetail</DetailType>
      <SalesItemLineDetail>
        <ItemRef name="SERVICES:NTO- Additional Address">34</ItemRef>
        <UnitPrice>1</UnitPrice>
        <Qty>6</Qty>
        <ItemAccountRef name="Services">1</ItemAccountRef>
        <TaxCodeRef>NON</TaxCodeRef>
        <TaxClassificationRef>EUC-99990201-V1-00020000</TaxClassificationRef>
      </SalesItemLineDetail>
    </Line>
    <Line>
      <Id>3</Id>
      <LineNum>3</LineNum>
      <Description>Firm Mail</Description>
      <Amount>27.65</Amount>
      <DetailType>SalesItemLineDetail</DetailType>
      <SalesItemLineDetail>
        <ItemRef name="MAILING:Firm Mail">18</ItemRef>
        <UnitPrice>3.95</UnitPrice>
        <Qty>7</Qty>
        <ItemAccountRef name="Services">1</ItemAccountRef>
        <TaxCodeRef>NON</TaxCodeRef>
        <TaxClassificationRef>EUC-99990201-V1-00020000</TaxClassificationRef>
      </SalesItemLineDetail>
    </Line>
    <Line>
      <Amount>33.66</Amount>
      <DetailType>SubTotalLineDetail</DetailType>
      <SubTotalLineDetail/>
    </Line>
    <TxnTaxDetail>
      <TxnTaxCodeRef>3</TxnTaxCodeRef>
      <TotalTax>0</TotalTax>
      <TaxLine>
        <Amount>0</Amount>
        <DetailType>TaxLineDetail</DetailType>
        <TaxLineDetail>
          <TaxRateRef>3</TaxRateRef>
          <PercentBased>true</PercentBased>
          <TaxPercent>6</TaxPercent>
          <NetAmountTaxable>0</NetAmountTaxable>
        </TaxLineDetail>
      </TaxLine>
      <TaxLine>
        <Amount>0</Amount>
        <DetailType>TaxLineDetail</DetailType>
        <TaxLineDetail>
          <TaxRateRef>4</TaxRateRef>
          <PercentBased>true</PercentBased>
          <TaxPercent>1</TaxPercent>
          <NetAmountTaxable>0</NetAmountTaxable>
        </TaxLineDetail>
      </TaxLine>
    </TxnTaxDetail>
    <CustomerRef name="AAA Construction Corp._1615909222">213</CustomerRef>
    <BillAddr>
      <Id>1204</Id>
      <Line1>301 Hialeah Drive ste 118</Line1>
      <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
      <PostalCode>33010</PostalCode>
    </BillAddr>
    <ShipAddr>
      <Id>1204</Id>
      <Line1>301 Hialeah Drive ste 118</Line1>
      <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
      <PostalCode>33010</PostalCode>
    </ShipAddr>
    <FreeFormAddress>true</FreeFormAddress>
    <ShipFromAddr>
      <Id>1509</Id>
      <Line1>PO Box 22821</Line1>
      <Line2>Hialeah, FL  33002 US</Line2>
    </ShipFromAddr>
    <SalesTermRef name="Net 15">2</SalesTermRef>
    <DueDate>2022-08-09</DueDate>
    <TotalAmt>33.66</TotalAmt>
    <ApplyTaxAfterDiscount>false</ApplyTaxAfterDiscount>
    <PrintStatus>NeedToPrint</PrintStatus>
    <EmailStatus>NotSet</EmailStatus>
    <Balance>33.66</Balance>
    <TaxExemptionRef name="service">99</TaxExemptionRef>
    <AllowIPNPayment>false</AllowIPNPayment>
    <AllowOnlinePayment>false</AllowOnlinePayment>
    <AllowOnlineCreditCardPayment>false</AllowOnlineCreditCardPayment>
    <AllowOnlineACHPayment>false</AllowOnlineACHPayment>
  </Invoice>
</IntuitResponse>


