REQUEST URI FOR SEQUENCE ID 11324
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..1up5_GitxrODaU56-qW8Ng.KYhv7ow2mOvlSdDmYUGW-RbjIpXkIHKV_H-iziqJSXzroQfMJk2ozXzlgaoMdyqZdahe5Ih_yCdS0E2ssDVLXFy0vPF7mJ-PQ_7oXUZQe0uKQose2uQCjFC3RiQ61I_h0tSEAaUSq20LIIb5rLIopg16qHUY6jFKoddUdRPymJPJ0yg5H0Nh6evuqQQp8u14Jd26etRjaJFmTq--og2s4cnNUy06jA3wOVDFIeHha1DI_Ya5wCxBn9aKzvS3iZp0erFc038HYVGr-tqgPj0F6H5-Ft6ti9ujzCvMTwuhXnBChAPNDJlG5YChNPZVUBq-bVDZvx-L-Cg8fWMQ5MQyRCO8bz5oUNKMr285Y60eVyYx19azBeDIHiHYrynjoE-mmlq2t96JvkOS_rs0Shbx5kmJQcEGsd_gymGZOYKaJBY2-qyj5fuG9-FeoD7G0cMfTiIBoQ6zHcyrfq1Ta00vU4iOD5xoQGaJxlMhtGaWg69csh8cfjtEJXOHT7u3epKkuiJeczqNmf7Ym7pRllVrBS2gXnECdvtsnsO0XF5SOip7wLw1fyXEX9gjqgJ226wE_arQxj34Ewz-6OxEbHNG9KnNK8G2PSSwDXaBDbkm1NSDmtoaFpEV67YKkcOEtWf6t6XvBkJyy0jF0kWYvzkjFz9T4hGwW8zbBOxRkT1cMLEN_lMv_WZeed9ZzA8dzN1van8OOaymE04NBagoLWWp1OU9M2DhlY81wIc7U3CvHIE.8xBpADzC8iIOk3FrbD-YXA
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 2798

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>CICC 7040-0/07 Contract- RPQ NO: X127A-R</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>4250 NW 20 ST MIAMI FL 33126</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>513_1657656456</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - QUALITY CONSTRUCTION PERFORMANCE , INC.</ns0:Description>
    <ns0:Amount>30</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>30</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Rush Fee</ns0:Description>
    <ns0:Amount>8.50</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Rush Hour Charges">33</ns0:ItemRef>
      <ns0:UnitPrice>8.50</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Firm Mail</ns0:Description>
    <ns0:Amount>3.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Firm Mail">18</ns0:ItemRef>
      <ns0:UnitPrice>3.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail with Electronic Return Receipt</ns0:Description>
    <ns0:Amount>7.22</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Electronic Return Receipt">15</ns0:ItemRef>
      <ns0:UnitPrice>7.22</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>CES70191120000063846345-513</ns0:Description>
    <ns0:Amount>6.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Manual">19</ns0:ItemRef>
      <ns0:UnitPrice>6.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>195</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


