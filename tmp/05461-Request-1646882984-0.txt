REQUEST URI FOR SEQUENCE ID 05461
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/customer?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..gRDfI51zgUXtLbD13ibtZQ.aFax3RTkj2gZkWdQmsPsiRf0jGambpR1nRuLLXT6AmnFIYQ3g1_ptEoiHJp2GZkunIcGVLpUvm2Mh_Fki9cGNVvcg4FEHQFOS2jinVbjiqLKiHtzBdd1_Bd7LtLw1X-aAestt9GgZR_OqHswzD78DPLjKcjgO0Q2JAUcc1Z5Wbimg16amU_uhab4a_pmyngLk9q81p3AGjsoTFVKk-pdZSBiSd9J4cF34Oky5v8lKhtHHY0v7e0G4ZBGhwLNSXjyB0h1fSxQysftpAToY0U8Pj6wGF3Y7utFjJEbVu1t6bBBF8pJJjdWqGxSMcCeNi71yW_R0jmnqGKKwHpGsv79YW8Vw0IV-a862wrXRkpiw0eGBLkYzoH7ZQWkJaQgTEPPUHhjPktvvKn9qLN-mSvU1xNeOVl20x7Y5DU-9Trhk6h6TXdf3VkZ3vUhT6QZZBde0FlqgmI5YT4J3bBhHmC5szcmZH6bjFiZXSGaMEyUsRi5IQBc4wbgnGobAPSRdfANrByOYkP11grWozlqiLdHu2nIBLBBxw4nchOBV1hHVmgtMZUgGhx6jMMfyzCrL-w8-gOD41ZG4z2yxwXVFUpJKJipPNb6z2yzhz3eZ03DrF9gMQ8RqUDXyKIIMi8CWqwYAtd9VkqIZSjSugJR2LMmmihBPR3G2oHf-zk8HOhtos37sLLzKWH1AcLdt5g7VrplgN3QUjeNSIfqCIH_9_4qcQyTdug5q-UfEBShGaJe6h0.W16qwPz8Zdtmq7kXZ_tkFA
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 1027

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Customer xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:FullyQualifiedName>SOUTH FLORIDA CONCRETE BLOCK LLC</ns0:FullyQualifiedName>
  <ns0:CompanyName>SOUTH FLORIDA CONCRETE BLOCK LLC</ns0:CompanyName>
  <ns0:DisplayName>SOUTH FLORIDA CONCRETE BLOCK LLC_1646882984</ns0:DisplayName>
  <ns0:PrimaryPhone>
    <ns0:FreeFormNumber>(305) 408-3444</ns0:FreeFormNumber>
  </ns0:PrimaryPhone>
  <ns0:PrimaryEmailAddr>
    <ns0:Address>southfloridaconc@bellsouth.net</ns0:Address>
  </ns0:PrimaryEmailAddr>
  <ns0:BillAddr>
    <ns0:Line1>5800 SW 177TH AVE # 101</ns0:Line1>
    <ns0:City>Miami</ns0:City>
    <ns0:CountrySubDivisionCode>Florida</ns0:CountrySubDivisionCode>
    <ns0:PostalCode>33193</ns0:PostalCode>
  </ns0:BillAddr>
  <ns0:ShipAddr>
    <ns0:Line1>5800 SW 177TH AVE # 101</ns0:Line1>
    <ns0:City>Miami</ns0:City>
    <ns0:CountrySubDivisionCode>Florida</ns0:CountrySubDivisionCode>
    <ns0:PostalCode>33193</ns0:PostalCode>
  </ns0:ShipAddr>
</ns0:Customer>


