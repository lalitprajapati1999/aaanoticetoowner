REQUEST URI FOR SEQUENCE ID 12598
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..jPZYDrn2ST808fFWBezmNQ.7RwsMdrXrBzVd6u8HZw7vMA1PpRL2H5dzkQxQeeE8bpBDpK9LQQbESllttUBrmp9zCReh35SjwsPExAZ1VOgy_GmzvOkIVU90hoQMc4ZsE9kaqsAG4ftv8YZyVHSi1wZsiRACnIM1236kuskZCzeSCmDvt17TjkhqelAic2w6Crh_unfm-JCrtFN6Hg3Ne6CxzlB1ln8-5666xRjY17gu7NTHiT4nxMWfSnj_-cS0MIPBy3CrEXAt_nFv15H5EZ33J6x5nCbW7HO2cRCfB02vGf2k-by-Cg6SUeT0d2LZa3zqDPISJauoo2HkjX8y_F6xSlwBcJsmctAbPbhT6he2eiQSJjPPZ2ygLKWyHSGuITpiPJsI1AFH8vMO7dVac58V8tVzGL4fqTXrnQAepuQuIh_maiu89upZ1-vcMW8Hxz78svobW7LH4sm7hn88eykYvEzOzsFd4lMLtsZwqdQxzhpLUoOEKoMnW5E2UUenqqIvmvf46bYIB-jS0N50fhfEm_EoddHx34ksR387n9da1LmdI4qu4O8zmNwA_qJyW4fLgjb42EiMifAQRw9eYJ9GzisHTmN10DTqPJgY-qtPcunm09zKh7-K4XTACyGO8qUS4Y2-J1fsOkchf7C1e_cDKOPEc6m0f-Qw7EDSaDcjeTATPHZ_5HtmQmY2lVCOBDns0tn6QkaOlIvTt3k333EBbGDm7LBfgl9btQsE7O3npPZqRLTvNiZ5G1TwvyOpVg.MAxDpJJi1lbeDzAemMdAqw
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 2357

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>13619 S DIXIE HWY UNIT #128 MI</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>882_1659022796</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - WEST CENTRAL DESING BUILD LLC</ns0:Description>
    <ns0:Amount>45.00</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>45.00</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Firm Mail</ns0:Description>
    <ns0:Amount>3.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Firm Mail">18</ns0:ItemRef>
      <ns0:UnitPrice>3.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail with Electronic Return Receipt</ns0:Description>
    <ns0:Amount>15.32</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Electronic Return Receipt">15</ns0:ItemRef>
      <ns0:UnitPrice>7.66</ns0:UnitPrice>
      <ns0:Qty>2</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>CES70220410000129073965-882</ns0:Description>
    <ns0:Amount>6.6</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Manual">19</ns0:ItemRef>
      <ns0:UnitPrice>6.6</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>98</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


