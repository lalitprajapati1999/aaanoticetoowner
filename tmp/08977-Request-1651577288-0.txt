REQUEST URI FOR SEQUENCE ID 08977
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..lmMe9GKFI8fUvvvse1d9Gg.WY5jpumPw-wcO8XGcn-nPdxYRP9uQtjajAz3s969oMLWNb5WDPEadWg6aAQIAW9VhWvbCxTsLhwICidOBTejB4z_gDJiSnauBOXZXj_puR12a50_pm4tXHbjUn_htRko4nhzndWNFqMOPrJYaBKQwugSnTPq8ZwZliJAC84E3MxaKlQUnbx8BsR2TYLDnpfDnxrnTFz90tgOzlMadHsK6aFS4EPDXlN_JRRYcb2e104UuzYq82qz5-NleFhCrXl95fpB-AT155cLQoiRPkx0H1JOCPFMgjbPibQHzKdMh4EjFxrhLS4_LnDXuXC3U2bNXo5OFZAs_3omvs3Ey9hKPhlcGbaPs2zmM9yKcO2Hq7jo2EZNjzq-oU0pD01GneDNDtg3kQE6pOfQUFce_5SoDCAhUg4gIa-j2YA0t_VMxCK0ocQ5x_KDSWpG-iMAi4UpKNz7B05qpkOSXBsPpCh6O5gzsPZPPrrNQYpJh6OY8sWGdrxwIkpYRjn3PB22DxmI6PWkR8297zcW0ZnvrUx4DWQVvjvp0UEOcWJ2X1iEKm_p9hqJ5Ad0DKWilbCexrBLx5J2LVPvMYl9znHmyQGuU27W3cgEfn6p3JZG2uOWW6QQ3amTTtQc_qqbHnM5JRGJqaU7dtQqBTz1WgEGirpBy9844_qeSOLZY52P_UIUfx7ikZB44dT_Sks7eLQn55PIiSj-7yBZ7gL0Kl58kJEK7_3TehlObXNSJPZ8ry1Dk1s.asVkIHCKuIxjfJVn6Uq1DQ
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 2804

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>CICC 7040-0/07 Contract- RPQ NO: X127A-R</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>4250 NW 20 ST MIAMI FL 33126</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>513_1651577288</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - QUALITY CONSTRUCTION PERFORMANCE , INC.</ns0:Description>
    <ns0:Amount>20.00</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>20.00</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Rush Fee</ns0:Description>
    <ns0:Amount>8.50</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Rush Hour Charges">33</ns0:ItemRef>
      <ns0:UnitPrice>8.50</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Firm Mail</ns0:Description>
    <ns0:Amount>3.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Firm Mail">18</ns0:ItemRef>
      <ns0:UnitPrice>3.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail with Electronic Return Receipt</ns0:Description>
    <ns0:Amount>7.22</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Electronic Return Receipt">15</ns0:ItemRef>
      <ns0:UnitPrice>7.22</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>CES70191120000063846345-513</ns0:Description>
    <ns0:Amount>6.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Manual">19</ns0:ItemRef>
      <ns0:UnitPrice>6.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>195</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


