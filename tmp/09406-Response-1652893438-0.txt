RESPONSE URI FOR SEQUENCE ID 09406
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

RESPONSE HEADERS
================
date: Wed, 18 May 2022 17:03:58 GMT
content-type: application/xml;charset=UTF-8
content-length: 4770
intuit_tid: 1-628526fe-6bc5df24588e752b66b15cb1
x-spanid: 941f56ad-2e82-4a89-9c23-1151e2ff19e3
x-amzn-trace-id: Root=1-628526fe-6bc5df24588e752b66b15cb1
x-content-type-options: nosniff
x-xss-protection: 1; mode=block
qbo-version: 1980.242
service-time: total=724, db=343
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private
x-envoy-upstream-service-time: 751
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-05-18T10:03:58.072-07:00">
  <Invoice domain="QBO" sparse="false">
    <Id>1615</Id>
    <SyncToken>0</SyncToken>
    <MetaData>
      <CreateTime>2022-05-18T10:03:58-07:00</CreateTime>
      <LastUpdatedTime>2022-05-18T10:03:58-07:00</LastUpdatedTime>
    </MetaData>
    <CustomField>
      <DefinitionId>1</DefinitionId>
      <Name>sales1</Name>
      <Type>StringType</Type>
      <StringValue>PO 91021</StringValue>
    </CustomField>
    <CustomField>
      <DefinitionId>2</DefinitionId>
      <Name>sales2</Name>
      <Type>StringType</Type>
      <StringValue>Intent to Lien</StringValue>
    </CustomField>
    <CustomField>
      <DefinitionId>3</DefinitionId>
      <Name>sales3</Name>
      <Type>StringType</Type>
      <StringValue>2000 PARK AVENUE MIAMI BEACH F</StringValue>
    </CustomField>
    <DocNumber>93_1652893437</DocNumber>
    <TxnDate>2022-05-18</TxnDate>
    <CurrencyRef name="United States Dollar">USD</CurrencyRef>
    <Line>
      <Id>1</Id>
      <LineNum>1</LineNum>
      <Description>Contracted By - ATLANTIC PACIFIC COMMUNITY BUILDERS, LLC
Parent Work Order Number - 92</Description>
      <Amount>25.00</Amount>
      <DetailType>SalesItemLineDetail</DetailType>
      <SalesItemLineDetail>
        <ItemRef name="SERVICES:Intent to Lien">37</ItemRef>
        <UnitPrice>25</UnitPrice>
        <Qty>1</Qty>
        <ItemAccountRef name="Services">1</ItemAccountRef>
        <TaxCodeRef>NON</TaxCodeRef>
        <TaxClassificationRef>EUC-99990201-V1-00020000</TaxClassificationRef>
      </SalesItemLineDetail>
    </Line>
    <Line>
      <Id>2</Id>
      <LineNum>2</LineNum>
      <Description>Rush Fee</Description>
      <Amount>12.50</Amount>
      <DetailType>SalesItemLineDetail</DetailType>
      <SalesItemLineDetail>
        <ItemRef name="SERVICES:Rush fee">33</ItemRef>
        <UnitPrice>12.5</UnitPrice>
        <Qty>1</Qty>
        <ItemAccountRef name="Services">1</ItemAccountRef>
        <TaxCodeRef>TAX</TaxCodeRef>
        <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
      </SalesItemLineDetail>
    </Line>
    <Line>
      <Id>3</Id>
      <LineNum>3</LineNum>
      <Description>Firm Mail</Description>
      <Amount>35.55</Amount>
      <DetailType>SalesItemLineDetail</DetailType>
      <SalesItemLineDetail>
        <ItemRef name="MAILING:Firm Mail">18</ItemRef>
        <UnitPrice>3.95</UnitPrice>
        <Qty>9</Qty>
        <ItemAccountRef name="Services">1</ItemAccountRef>
        <TaxCodeRef>TAX</TaxCodeRef>
        <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
      </SalesItemLineDetail>
    </Line>
    <Line>
      <Id>4</Id>
      <LineNum>4</LineNum>
      <Description>FED EX ND</Description>
      <Amount>45.00</Amount>
      <DetailType>SalesItemLineDetail</DetailType>
      <SalesItemLineDetail>
        <ItemRef name="MAILING:Manual">19</ItemRef>
        <UnitPrice>45</UnitPrice>
        <Qty>1</Qty>
        <ItemAccountRef name="Services">1</ItemAccountRef>
        <TaxCodeRef>TAX</TaxCodeRef>
        <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
      </SalesItemLineDetail>
    </Line>
    <Line>
      <Id>5</Id>
      <LineNum>5</LineNum>
      <Description>FED EX</Description>
      <Amount>65.00</Amount>
      <DetailType>SalesItemLineDetail</DetailType>
      <SalesItemLineDetail>
        <ItemRef name="MAILING:Manual">19</ItemRef>
        <UnitPrice>65</UnitPrice>
        <Qty>1</Qty>
        <ItemAccountRef name="Services">1</ItemAccountRef>
        <TaxCodeRef>TAX</TaxCodeRef>
        <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
      </SalesItemLineDetail>
    </Line>
    <Line>
      <Amount>183.05</Amount>
      <DetailType>SubTotalLineDetail</DetailType>
      <SubTotalLineDetail/>
    </Line>
    <TxnTaxDetail>
      <TxnTaxCodeRef>3</TxnTaxCodeRef>
      <TotalTax>11.06</TotalTax>
      <TaxLine>
        <Amount>9.47</Amount>
        <DetailType>TaxLineDetail</DetailType>
        <TaxLineDetail>
          <TaxRateRef>3</TaxRateRef>
          <PercentBased>true</PercentBased>
          <TaxPercent>6</TaxPercent>
          <NetAmountTaxable>158.05</NetAmountTaxable>
        </TaxLineDetail>
      </TaxLine>
      <TaxLine>
        <Amount>1.59</Amount>
        <DetailType>TaxLineDetail</DetailType>
        <TaxLineDetail>
          <TaxRateRef>4</TaxRateRef>
          <PercentBased>true</PercentBased>
          <TaxPercent>1</TaxPercent>
          <NetAmountTaxable>158.05</NetAmountTaxable>
        </TaxLineDetail>
      </TaxLine>
    </TxnTaxDetail>
    <CustomerRef name="AAA Construction Corp._1615909222">213</CustomerRef>
    <BillAddr>
      <Id>1117</Id>
      <Line1>301 Hialeah Drive ste 118</Line1>
      <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
      <PostalCode>33010</PostalCode>
    </BillAddr>
    <ShipAddr>
      <Id>1117</Id>
      <Line1>301 Hialeah Drive ste 118</Line1>
      <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
      <PostalCode>33010</PostalCode>
    </ShipAddr>
    <FreeFormAddress>true</FreeFormAddress>
    <ShipFromAddr>
      <Id>1166</Id>
      <Line1>PO Box 22821</Line1>
      <Line2>Hialeah, FL  33002 US</Line2>
    </ShipFromAddr>
    <SalesTermRef name="Net 15">2</SalesTermRef>
    <DueDate>2022-06-02</DueDate>
    <TotalAmt>194.11</TotalAmt>
    <ApplyTaxAfterDiscount>false</ApplyTaxAfterDiscount>
    <PrintStatus>NeedToPrint</PrintStatus>
    <EmailStatus>NotSet</EmailStatus>
    <Balance>194.11</Balance>
    <TaxExemptionRef/>
    <AllowIPNPayment>false</AllowIPNPayment>
    <AllowOnlinePayment>false</AllowOnlinePayment>
    <AllowOnlineCreditCardPayment>false</AllowOnlineCreditCardPayment>
    <AllowOnlineACHPayment>false</AllowOnlineACHPayment>
  </Invoice>
</IntuitResponse>


