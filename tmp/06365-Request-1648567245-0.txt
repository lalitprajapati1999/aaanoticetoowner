REQUEST URI FOR SEQUENCE ID 06365
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..wOAcxiZFAV6yxWMIP1xwVw.SKS-NgobeZ6h55LnWCIkw118y9IDn0UC9yDMrxDn5dC2aIwzWn_ovn05QuSu6OcCLnKZto_7zvRYBtaWHjpYlrph0CNMbSQgw0K1OhRclkxQWsLOHYOW7MJ6TQAcz3hua-cnN3tP-iJxKrL4J1KwD6o-SuVxMmfF7TvINtPP0vxiXNdZtO5fbBt9zdSSh5ImfmCp1NCQwo5TLhBMd9grtryqDETntn1vxfpEeclQmAMXloHub1xFQ04C6T4iRLXSbzdCpd-zwTwB-RPkklziXaRl0rVjKbtwSvV4GCLYgIYYCWgTt56BHoCrmJNjj4LE62_EbYWTATdmafQVQT92CXX7oobrdz9LNJBn1fBfyKC6UkfNHf74kjVcWEo_KtLacGMuWeFYmU_uUMeLD9fYgPbROinOSLafpo8EUeCjEQiDpEg_yp1fypY_1ZNBj_-4hfSQKH2ZcJrzfTzirkOHVjutmJA7xQYd6w1RurUqhMb9tsj2unjP_xYtDf_TEq5WHbpWDEG3vowJITWxXYerAd-P2WN-inWiT0_g1bRJVasRmCwlOkyKdqG7-tbnSNz0CEvM6bvx73nGNoj4Xi0e8Ek9gSZmvdbvb6e6W4ep9TuAbHmwqmdS5HG8kmGo4My66EN2s9fdEY7T_cjnAcdcYS3FS_5KB_egbrMOmQwMZeomNQQG5Uuvtoubt2sQU04kAz7fYAy3x2ntQvtEjIpG7xTJvpxl24KOM_H8tC0gjWQ.c_7zoGlL5EaSj-lmJMTWaQ
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 2354

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>12310 Miramar Blvd Suite 1 Mir</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>414_1648567245</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - GD Construction LLC</ns0:Description>
    <ns0:Amount>45.00</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>45.00</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Rush Fee</ns0:Description>
    <ns0:Amount>12.50</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Rush Hour Charges">33</ns0:ItemRef>
      <ns0:UnitPrice>12.50</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail</ns0:Description>
    <ns0:Amount>5.42</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Certified Mail">17</ns0:ItemRef>
      <ns0:UnitPrice>5.42</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail with Electronic Return Receipt</ns0:Description>
    <ns0:Amount>14.44</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Electronic Return Receipt">15</ns0:ItemRef>
      <ns0:UnitPrice>7.22</ns0:UnitPrice>
      <ns0:Qty>2</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>273</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


