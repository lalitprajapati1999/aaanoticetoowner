REQUEST URI FOR SEQUENCE ID 12022
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..Us-XuH3uTItEE126EVDZJg.0U3ekb0hMG2qr8t_XDWZkTfzslem-XPVotzuVQqFiz3WTqdttnvBAqm2Pj3nBp9K_DJur1BF_Uw3WdH4lhaiWcDl7s5j5E_x5rF1k3U7iwZDw4Qyu-5lIW8XOKV8-HHWQQWdXlcpbCQ0nDBj6n2rqXOzimzqpW8KqNRZ4lyTjyBi56G3lu8BFV-YjmjLr0A5CaS1RZwzuW12KMe2o2rS0rHiUt6b2uS5bJjx_LQ86DEjDXnd0TZe89aM-Y72xBCa9voI0hY_ZxZZD8pNf3e7quhnmUPvo7W20fQi71Ibiv3cYoVvVl3ak6DdvSZNFCCGgdsvTK9q6a814uAi9AToDQrRTpEeaEjTBA2CGy1ll5K48set_i0TFRAKSBFKHFMCOwo_EDUjBsvxh709-jrejI_FK1Dz-T-hQvoQt_AQHvtqKzHcFO2bLVRRkQ9f5ig9hyiLLJltRJ9tfZkZuftKGx-Y2UITTGxag6rg_WBZA1PFbYZmOqn9Q83BjD6xFdBBInKd40O2wiOnzDlbFkM0CYQGv-WtiQtusj6y9CTuee2WeNUTG1CR0Im7o7GxD7caZ6pQboSWzhmKg5UsdhciIHkwM_6WluhY-IluhG63fw6Db79TShrm08QKjNNx93M6uIDmoEel-9Oh00m8IDkemN6L6OCOm4BrGe0FLE2s6rMlxyggI_Sqo8ukdRkFHO37ASdih6WvRBx-UTDlx1zOh8OEyXnAMl__6cOuKBG8ENw.1D8eR5Bpz1J_BqPFwFqxQQ
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 1990

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>12008 SW 88TH STREET/ MIAMI FL</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>779_1658853157</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - ALFARO PLUMBING CONTRACTORS LLC</ns0:Description>
    <ns0:Amount>35</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>35</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Firm Mail</ns0:Description>
    <ns0:Amount>3.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Firm Mail">18</ns0:ItemRef>
      <ns0:UnitPrice>3.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail with Electronic Return Receipt</ns0:Description>
    <ns0:Amount>30.64</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Electronic Return Receipt">15</ns0:ItemRef>
      <ns0:UnitPrice>7.66</ns0:UnitPrice>
      <ns0:Qty>4</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>36</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


