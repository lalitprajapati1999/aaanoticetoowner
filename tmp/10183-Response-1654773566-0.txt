RESPONSE URI FOR SEQUENCE ID 10183
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

RESPONSE HEADERS
================
date: Thu, 09 Jun 2022 11:19:26 GMT
content-type: application/xml
content-length: 509
intuit_tid: 1-62a1d73d-34a2f6590c81f89d0bfda1ce
x-spanid: 8a20b570-c2b8-431a-b58c-454ef7502f07
x-amzn-trace-id: Root=1-62a1d73d-34a2f6590c81f89d0bfda1ce
x-content-type-options: nosniff
x-envoy-upstream-service-time: 520
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-06-09T04:19:25.985-07:00">
  <Fault type="ValidationFault">
    <Error code="6000" element="">
      <Message>A business validation error has occurred while processing your request</Message>
      <Detail>Business Validation Error: You cannot enter more than 31 characters in the sales_custom_1_val field. You tried entering CICC 7040-..., which is 40 characters.</Detail>
    </Error>
  </Fault>
</IntuitResponse>


