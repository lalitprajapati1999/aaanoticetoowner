REQUEST URI FOR SEQUENCE ID 03339
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..97XENNz_JYXacVExIijrTA.6wWnjmfg5UkxTM48YOLPzKv5eaWOhUwblTGOeiphfE0qNlkWKitPxz2bhjZH-H8Kh8osBlmzyrsIQLYzhEglHBCH-P9bCgnv2s9J6s2slakt9VztAef8LCRRLmQfHSee5uk2taLXqxG-KY2ChXx_jwoSJgMBA8Yw4CF8nQ_qJaNNd5j7B9i32lT9qQyFoI3H_NtIaE63giZeExsjDRQxpoVn4wQJwjqOTdQNeR_neQYO3G4mCOfQgSysRnbSHRzOr8cdFPYYQwGpEbI7234zSu-7wEKu8d_PsU4PvWFKWRog0Oiatk4CRSFqLFqcU5q1ZrrHtkqK6JM5VKnxc_UFQMJRD63jYioCUABIycZRc0jMU9taRy_WPZmi8O-i-j1swEKWZJqoMjCh1wVMthlc4ZlJPAMLUlzBzZw5iW4HdQ-sW1ud4HMHXLcYlxpypYN9lFiDkgXQtRSyra2K1e4RAtFMvkycEquo2U6T5AV4fduBnuRgdbkhpVIFANzx763zileRtsDjdVZLFLj41-5scx4WdDfqRt9OUE-UJ6TsJ-jBOlN94qQZVi8hmcgWl4vq1i2hdA0QDVcK6L8S_drLUFJE6uFo7NWplOEZ96aA6O3L-dCjR8oeU9pLUgH9foKqAjkQWf0tk9DYh9-5Sejuf6_t25NgLREP19y_cTt42-DIWi4jfLPajRuXic75oNznWQsAZPW4x0LBn9FpQuVFrwCfhdFBnytCQv6VVRu8BlM.noJtcSwQlyFnh6f_tfgA_g
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 2714

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>9520 WEST BROADVIEW DRIVE BAY </ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>269_1645213002</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - JBS PREMIER SERVICES CORP.</ns0:Description>
    <ns0:Amount>20.00</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>20.00</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Firm Mail</ns0:Description>
    <ns0:Amount>3.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Firm Mail">18</ns0:ItemRef>
      <ns0:UnitPrice>3.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail</ns0:Description>
    <ns0:Amount>5.42</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Certified Mail">17</ns0:ItemRef>
      <ns0:UnitPrice>5.42</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail with Electronic Return Receipt</ns0:Description>
    <ns0:Amount>7.22</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Electronic Return Receipt">15</ns0:ItemRef>
      <ns0:UnitPrice>7.22</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>C70191120000063845294-269</ns0:Description>
    <ns0:Amount>6.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Manual">19</ns0:ItemRef>
      <ns0:UnitPrice>6.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>195</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


