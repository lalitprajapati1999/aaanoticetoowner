RESPONSE URI FOR SEQUENCE ID 08378
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

RESPONSE HEADERS
================
date: Thu, 21 Apr 2022 14:47:39 GMT
content-type: application/xml;charset=UTF-8
content-length: 4237
intuit_tid: 1-62616e8a-7069a6115d8f1b893f8ca792
x-spanid: 73645b31-b005-4441-ba5c-a9a7b3080544
x-amzn-trace-id: Root=1-62616e8a-7069a6115d8f1b893f8ca792
x-content-type-options: nosniff
x-xss-protection: 1; mode=block
qbo-version: 1979.193
service-time: total=554, db=164
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private
x-envoy-upstream-service-time: 585
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-04-21T07:47:38.839-07:00">
  <Invoice domain="QBO" sparse="false">
    <Id>1326</Id>
    <SyncToken>0</SyncToken>
    <MetaData>
      <CreateTime>2022-04-21T07:47:39-07:00</CreateTime>
      <LastUpdatedTime>2022-04-21T07:47:39-07:00</LastUpdatedTime>
    </MetaData>
    <CustomField>
      <DefinitionId>2</DefinitionId>
      <Type>StringType</Type>
      <StringValue>Notice to Owner/Preliminary No</StringValue>
    </CustomField>
    <CustomField>
      <DefinitionId>3</DefinitionId>
      <Type>StringType</Type>
      <StringValue>6447 NE 7 AVE MIAMI FL 33138</StringValue>
    </CustomField>
    <DocNumber>496_1650552458</DocNumber>
    <TxnDate>2022-04-21</TxnDate>
    <CurrencyRef name="United States Dollar">USD</CurrencyRef>
    <Line>
      <Id>1</Id>
      <LineNum>1</LineNum>
      <Description>Contracted By - M&amp;N CONSTRUCTION ENTERPRICES INC.</Description>
      <Amount>20.00</Amount>
      <DetailType>SalesItemLineDetail</DetailType>
      <SalesItemLineDetail>
        <ItemRef name="SERVICES:NTO">32</ItemRef>
        <UnitPrice>20</UnitPrice>
        <Qty>1</Qty>
        <ItemAccountRef name="Services">1</ItemAccountRef>
        <TaxCodeRef>TAX</TaxCodeRef>
        <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
      </SalesItemLineDetail>
    </Line>
    <Line>
      <Id>2</Id>
      <LineNum>2</LineNum>
      <Description>Firm Mail</Description>
      <Amount>3.95</Amount>
      <DetailType>SalesItemLineDetail</DetailType>
      <SalesItemLineDetail>
        <ItemRef name="MAILING:Firm Mail">18</ItemRef>
        <UnitPrice>3.95</UnitPrice>
        <Qty>1</Qty>
        <ItemAccountRef name="Services">1</ItemAccountRef>
        <TaxCodeRef>TAX</TaxCodeRef>
        <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
      </SalesItemLineDetail>
    </Line>
    <Line>
      <Id>3</Id>
      <LineNum>3</LineNum>
      <Description>Certified Mail</Description>
      <Amount>5.42</Amount>
      <DetailType>SalesItemLineDetail</DetailType>
      <SalesItemLineDetail>
        <ItemRef name="MAILING:Certified Mail USPS">17</ItemRef>
        <UnitPrice>5.42</UnitPrice>
        <Qty>1</Qty>
        <ItemAccountRef name="Services">1</ItemAccountRef>
        <TaxCodeRef>TAX</TaxCodeRef>
        <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
      </SalesItemLineDetail>
    </Line>
    <Line>
      <Id>4</Id>
      <LineNum>4</LineNum>
      <Description>Certified Mail with Electronic Return Receipt</Description>
      <Amount>14.44</Amount>
      <DetailType>SalesItemLineDetail</DetailType>
      <SalesItemLineDetail>
        <ItemRef name="MAILING:Certified Mail ESRR">15</ItemRef>
        <UnitPrice>7.22</UnitPrice>
        <Qty>2</Qty>
        <ItemAccountRef name="Services">1</ItemAccountRef>
        <TaxCodeRef>TAX</TaxCodeRef>
        <TaxClassificationRef>EUC-99990202-V1-00020000</TaxClassificationRef>
      </SalesItemLineDetail>
    </Line>
    <Line>
      <Amount>43.81</Amount>
      <DetailType>SubTotalLineDetail</DetailType>
      <SubTotalLineDetail/>
    </Line>
    <TxnTaxDetail>
      <TxnTaxCodeRef>3</TxnTaxCodeRef>
      <TotalTax>0</TotalTax>
      <TaxLine>
        <Amount>0</Amount>
        <DetailType>TaxLineDetail</DetailType>
        <TaxLineDetail>
          <TaxRateRef>3</TaxRateRef>
          <PercentBased>true</PercentBased>
          <TaxPercent>6</TaxPercent>
          <NetAmountTaxable>0</NetAmountTaxable>
        </TaxLineDetail>
      </TaxLine>
      <TaxLine>
        <Amount>0</Amount>
        <DetailType>TaxLineDetail</DetailType>
        <TaxLineDetail>
          <TaxRateRef>4</TaxRateRef>
          <PercentBased>true</PercentBased>
          <TaxPercent>1</TaxPercent>
          <NetAmountTaxable>0</NetAmountTaxable>
        </TaxLineDetail>
      </TaxLine>
    </TxnTaxDetail>
    <CustomerRef name="ERNESTO HERNANDEZ">195</CustomerRef>
    <BillAddr>
      <Id>196</Id>
      <Line1>6945 NW 53 TERRACE</Line1>
      <City>MIAMI</City>
      <CountrySubDivisionCode>FLOIRDA</CountrySubDivisionCode>
      <PostalCode>33166</PostalCode>
    </BillAddr>
    <ShipAddr>
      <Id>196</Id>
      <Line1>6945 NW 53 TERRACE</Line1>
      <City>MIAMI</City>
      <CountrySubDivisionCode>FLOIRDA</CountrySubDivisionCode>
      <PostalCode>33166</PostalCode>
    </ShipAddr>
    <FreeFormAddress>true</FreeFormAddress>
    <ShipFromAddr>
      <Id>1049</Id>
      <Line1>PO Box 22821</Line1>
      <Line2>Hialeah, FL  33002 US</Line2>
    </ShipFromAddr>
    <SalesTermRef name="Net 15">2</SalesTermRef>
    <DueDate>2022-05-06</DueDate>
    <TotalAmt>43.81</TotalAmt>
    <ApplyTaxAfterDiscount>false</ApplyTaxAfterDiscount>
    <PrintStatus>NeedToPrint</PrintStatus>
    <EmailStatus>NotSet</EmailStatus>
    <Balance>43.81</Balance>
    <TaxExemptionRef name="service">99</TaxExemptionRef>
    <AllowIPNPayment>false</AllowIPNPayment>
    <AllowOnlinePayment>false</AllowOnlinePayment>
    <AllowOnlineCreditCardPayment>false</AllowOnlineCreditCardPayment>
    <AllowOnlineACHPayment>false</AllowOnlineACHPayment>
  </Invoice>
</IntuitResponse>


