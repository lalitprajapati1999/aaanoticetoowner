REQUEST URI FOR SEQUENCE ID 00033
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..3mzM43L_t4Hx0tzr23ZsmQ.Gw50NkhHNOeMW2qpuvjG7OySb5HpATBimR1kGPptjsEQfZWvkTN0IA7VrV-CdohK0b1sr3JkJ8o3bqAGZ5TpsYQicZI7DmZI_hC7ZwkPDRr07FRbWZjKYYQIQRURDUtceytphbopnuc7UlAfFMVuIs_J2NY8cGsOUJcGnikA9A-cAIYtkN3dP9Yv_rIz0DjjtEjWh_R96OBfVX-rVFEroQYyg9Qmt3DJB5s78gKZmgxuebYm-qzvv5whl8qOZ9u0YE_yliSyCnq3ocJLpE3AzNFmZrB9uoFsYjGhBn4-6NDYfyKfDOAWq6Fgy1wPEbUaS7GXe9X1BAAlRp7TaL1UNLpniDsROCQJ6LkjyznWMiCMUoTXPc4b7mWClAy_1ppUZre2A4sLCvimAQS0PgBNxRJNbVSpqNVEFdDlkq5XlHM0DRV3qv6UfhydISIQH-_-7MGFHIsCPtTK84oXanfej7I07S_WP8_-KpUk_hDZOc1wmaxe9nKLXuwNTsS-AptCZXoPd9vkjzd5S2uwuVt-RZ5ddzN_N5suEb7tSzIl2ciO-3pQPV1y1--yXTj7yxcy71hfi881ZkAGEqdmmEpVjQwTkZQoNmwVt55rBixirUrpXoiWF_C3HGejAz8p6FroqwFL_Z_WK8PN9GL1TJyM-oKEP_fmNxCuQlw5WsOKbZrVr5e6Qk8OOheireW_UzEftTOcwTog291gUaD7CwJ4aNmwMzn6zrkWpWUpFJc68RI.FwNCjWQC2t34NFK36pMm-w
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 1653

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>PO NTO# 17710-113</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Non Payment Notice</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>10471 BOCA WOOD LANE BOCA RATO</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>1619614991_21_1306</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - AG CONTRACTORSLLC
Parent Work Order Number - 11</ns0:Description>
    <ns0:Amount>45.00</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>45.00</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>firm mail</ns0:Description>
    <ns0:Amount>8</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Firm Mail">18</ns0:ItemRef>
      <ns0:UnitPrice>1</ns0:UnitPrice>
      <ns0:Qty>8</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>213</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


