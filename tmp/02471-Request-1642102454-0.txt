REQUEST URI FOR SEQUENCE ID 02471
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..UhqmK1UeR0cvJMFxnllZDg.WgX3DC-ACWiqocTKED6U_G9GkkZ2pt6DziMdHCVB3Ej_-BI75ntYtHwWC7XA4FnbzdyHAoZE8bsuI6oh8p2bmkmkTuPIRADeZRlBa-yaE1cOQS2mopvxmCh_Iz3fQmbT3GOnNgMPRewUA3E-2yCHduGcbkZQCjj65X06BbFZLcsWQSs0mDv1QO4UPPcyxga5O9az1xiK2uOrngEaclQKGlRmGt7YciOfFkbdGN9U5H03oq_JRFUPtSUCGw81GlBkSvHq1fo4COGGd1sVd8QhN2fagEoLAD0DCgNXsoxAz24FQAFqrdRLibVxDB8726qBKSWp7Yhu1xYB1532HoLpb1G8Q0oepgpoJXeSWE1QKvKk1TIFfpV_cGOtQKwCO4Gh5H-LkwHLADeNZr55C8e_AnmwPyG4mtWlE_kk3qwvfyOLKLZtPU9zMkbXsSF4CRawvnnh6kQXqKlAIQ74O0ZGhgmmNLc0E0vqlRAgvNCJkQNfCGyZUbRrB-omJntSz5rdsEYBTjK5lOYU1Q0hmKpd90Uz4BEXzfa9OgbBofUTjjZCVpausI0_I_0Kf5phYaAvO4sfuwCVZuxr0uP4rOuXpPIwdxHfTxyi7P7gQ7FIg3PgeFR-nRdkhriNVhDQdrHSt2RRdtJtJOvHiDv3uIIP5mziqZKsNlr6QFy8lwvOjPPGv2NgJ4oejsublAUjyH62ntpkJNEA1QF9bSibLG20Pz0znb0xnhsZJ2DfjeKOFVg.7s2PS5LY7Xikpo7X8o9YDg
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 5443

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>PO 91021</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Satisfaction of Lien</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>2000 PARK AVENUE MIAMI BEACH F</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>96_1642102454</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - MASTER CONST. OF S.FLA
Parent Work Order Number - 92</ns0:Description>
    <ns0:Amount>145.00</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">23</ns0:ItemRef>
      <ns0:UnitPrice>145.00</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Rush Fee</ns0:Description>
    <ns0:Amount>90.00</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Rush Hour Charges">33</ns0:ItemRef>
      <ns0:UnitPrice>90.00</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>FED EX</ns0:Description>
    <ns0:Amount>65</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Manual">19</ns0:ItemRef>
      <ns0:UnitPrice>65</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>FED ND</ns0:Description>
    <ns0:Amount>45</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Manual">19</ns0:ItemRef>
      <ns0:UnitPrice>45</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>FED EX</ns0:Description>
    <ns0:Amount>65</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Manual">19</ns0:ItemRef>
      <ns0:UnitPrice>65</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>CERT RR</ns0:Description>
    <ns0:Amount>7.25</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Manual">19</ns0:ItemRef>
      <ns0:UnitPrice>7.25</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>CERT RR</ns0:Description>
    <ns0:Amount>7.25</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Manual">19</ns0:ItemRef>
      <ns0:UnitPrice>7.25</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>CERT RR</ns0:Description>
    <ns0:Amount>7.25</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Manual">19</ns0:ItemRef>
      <ns0:UnitPrice>7.25</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>CERT RR</ns0:Description>
    <ns0:Amount>7.25</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Manual">19</ns0:ItemRef>
      <ns0:UnitPrice>7.25</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>CERT RR</ns0:Description>
    <ns0:Amount>7.25</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Manual">19</ns0:ItemRef>
      <ns0:UnitPrice>7.25</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>CERT RR</ns0:Description>
    <ns0:Amount>7.25</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Manual">19</ns0:ItemRef>
      <ns0:UnitPrice>7.25</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>CERT RR</ns0:Description>
    <ns0:Amount>7.25</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Manual">19</ns0:ItemRef>
      <ns0:UnitPrice>7.25</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>CERT RR</ns0:Description>
    <ns0:Amount>7.25</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Manual">19</ns0:ItemRef>
      <ns0:UnitPrice>7.25</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>213</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


