REQUEST URI FOR SEQUENCE ID 03253
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..-tGyguN1JJrPaCd_DsniTA.9Ik0DyXwGqu_pzMYKfiejKpJI1QGTXlryfJMK4KLmOz0E3IKXc5Cy991xjbGR5jYLeCMWpRkI0WGaPwL-PPbP28PI7hmYCWdbqtdmDEh3R3itu6OvadQbLVoPo5Sq1CyY5swBd14pmKvgS2bzPbiU8tEq-EMGWkbjXbjoScrQqzNtfcaysC2zJ1nK2sWpqJz0wUxY9Z_Z8u9fRU8PloU98Vo2peh_Ehe7bT8dFqZEClFQ7PwIAd_aZ2OvreY_11KUTm6u0XUMSk_v6KOXUWoteMjxU447nbTFQm3cyYOqWkiuHhn3r1K4MifuuLp4kdmAJoKGUS_vJYGs_kTV8GLFtHXmnU6hAbSImxOwUyrDtM-PisRAvVytAcbjfx1dNRqFldmJkwLJi_VmIw7x1Qo3R4K0Y38Q9qM5wu4wU7yTNXkzTqoF9c3mZ6PstUppdLTYTSFsSyngYRls4ynRVXw1TpUimq8LfMfTSJK0hUM_hQp9QO8N_-9QdaJpXFEFIKL5jqm_u3vAZyESnYInzcJLCWNWeJBFapbyFKWXMwm07airVEx_wCm4AEX2kpKJkNDjLQhWluCR5xwhcqD9arcc4jJXPCLStJ8uqVVf9JdCk4qk3C5XVi5DGfoA9mE9JLfXH7R7PsXAheuY0j2shi3Q19mEJXNYHeAKUrPOSu1VgalqyWmemEZb8-fxpcGsHxNoWTW1DjGwAhcw8QpVJV-sp7GEtu7dnJOYtXZFVla4GI.5pz7qPSYGProSUbcRCNnPQ
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 1990

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>2345 TEQUESTA LANE MIAMI FLORI</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>217_1644946856</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - TRU-LINE CONSTRUCTION LLC</ns0:Description>
    <ns0:Amount>45.00</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>45.00</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Firm Mail</ns0:Description>
    <ns0:Amount>3.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Firm Mail">18</ns0:ItemRef>
      <ns0:UnitPrice>3.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail with Electronic Return Receipt</ns0:Description>
    <ns0:Amount>7.22</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Electronic Return Receipt">15</ns0:ItemRef>
      <ns0:UnitPrice>7.22</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>251</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


