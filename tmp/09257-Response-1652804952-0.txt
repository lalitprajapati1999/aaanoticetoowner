RESPONSE URI FOR SEQUENCE ID 09257
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

RESPONSE HEADERS
================
date: Tue, 17 May 2022 16:29:12 GMT
content-type: application/xml
content-length: 509
intuit_tid: 1-6283cd57-04f14fbe2656db7a258b4dc9
x-spanid: 01503c31-0c49-4dea-86aa-9ff131e5f3b3
x-amzn-trace-id: Root=1-6283cd57-04f14fbe2656db7a258b4dc9
x-content-type-options: nosniff
x-envoy-upstream-service-time: 1347
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-05-17T09:29:11.174-07:00">
  <Fault type="ValidationFault">
    <Error code="6000" element="">
      <Message>A business validation error has occurred while processing your request</Message>
      <Detail>Business Validation Error: You cannot enter more than 31 characters in the sales_custom_1_val field. You tried entering CICC 7040-..., which is 40 characters.</Detail>
    </Error>
  </Fault>
</IntuitResponse>


