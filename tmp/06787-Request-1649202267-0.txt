REQUEST URI FOR SEQUENCE ID 06787
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/customer?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..Tn9xKvuEo77wuaQTmJK4Vg.71CKwu1wpCQiuizC0KU3HV94rdshQ-u5Hzv5Y3K2gLPlQ2a5sf2WuxArivGae5Vq22cQMlQ8-8xznXXCwWk55duX9XxfyXom5EXLci0lnVG3oymZPLQAtYUVYb7mKMkzxpt1LYbpHm_pGp3Hb4EbsqseAaC14S2nm35VZhRVQBumL-_mHIMiiUf6gy0PLntYMM8rVPiMUqsI6UDVbDj81z7x5d5ovnAjcKisHZaDWxw2X6lzHF4NUReDmMKYl2eIXOXa4Uu1uSA85oeOevO2JJqPYbZHaQHpnwikc9FSYu7ZpIIpaOK4OjFQZdIMCObnBZHXRAVpna0YKR8zlwWYhjS__yq5bz6xhm6CovOAJnB23hBmkwGjSPHIR2dtdxkHZgIUITYn1omqInSx5KNPjcyDrYnHsC702w5kBjnUKWOrlLhRQtDx9fNb3glUSg-IZKV2ZrVHaDB_A8Zw70lwun2yok_SvgyBzoGot-0GZWO00tXpr8Uq6nDGd55VzAkBOEcBZuDaTAUvXzc0jQjWYXg84dgyALOQwPkawBSksdIqqT71lnlfWo0X8oqXz1lVMfZWL3r5VmTLIEATxO9Bj5tTLAZIVa4Bfl1vE_fWujGQ0dP_OlnrciV3XyvZfff1bizjvVm3XyK2Q9ic-isBmH53_XUo-4Fuui0Ouam_yAgLkbeOIxBBAt8VXNMd3uZ-d6nDlvX8-O95OM2UsZeplfv16SHA52iDNXeBIkNlnYQ.vfy9UvE69CY6VI9DFHV8Sw
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 997

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Customer xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:FullyQualifiedName>B&amp;B DEVELOPMENT INC.</ns0:FullyQualifiedName>
  <ns0:CompanyName>B&amp;B DEVELOPMENT INC.</ns0:CompanyName>
  <ns0:DisplayName>B&amp;B DEVELOPMENT INC._1649202266</ns0:DisplayName>
  <ns0:PrimaryPhone>
    <ns0:FreeFormNumber>(305) 609-4074</ns0:FreeFormNumber>
  </ns0:PrimaryPhone>
  <ns0:PrimaryEmailAddr>
    <ns0:Address>humbertobustamante46@gmail.com</ns0:Address>
  </ns0:PrimaryEmailAddr>
  <ns0:BillAddr>
    <ns0:Line1>14464 SW 293 TER</ns0:Line1>
    <ns0:City>HOMESTAED</ns0:City>
    <ns0:CountrySubDivisionCode>Florida</ns0:CountrySubDivisionCode>
    <ns0:PostalCode>33033</ns0:PostalCode>
  </ns0:BillAddr>
  <ns0:ShipAddr>
    <ns0:Line1>14464 SW 293 TER</ns0:Line1>
    <ns0:City>HOMESTAED</ns0:City>
    <ns0:CountrySubDivisionCode>Florida</ns0:CountrySubDivisionCode>
    <ns0:PostalCode>33033</ns0:PostalCode>
  </ns0:ShipAddr>
</ns0:Customer>


