RESPONSE URI FOR SEQUENCE ID 03242
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/customer?minorversion=47

RESPONSE HEADERS
================
date: Tue, 15 Feb 2022 13:23:52 GMT
content-type: application/xml;charset=UTF-8
content-length: 1535
intuit_tid: 1-620ba967-0b22c9026485b738358ef8e7
x-spanid: f5752b30-73c7-420e-ad90-7a1a604812bb
x-amzn-trace-id: Root=1-620ba967-0b22c9026485b738358ef8e7
x-content-type-options: nosniff
qbo-version: 1974.107
service-time: total=509, db=138
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private
x-envoy-upstream-service-time: 580
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-02-15T05:23:51.849-08:00">
  <Customer domain="QBO" sparse="false">
    <Id>259</Id>
    <SyncToken>0</SyncToken>
    <MetaData>
      <CreateTime>2022-02-15T05:23:51-08:00</CreateTime>
      <LastUpdatedTime>2022-02-15T05:23:51-08:00</LastUpdatedTime>
    </MetaData>
    <FullyQualifiedName>ARCHITECTURAL DOORS AND FRAMES INCORPORATE_1644931431</FullyQualifiedName>
    <CompanyName>ARCHITECTURAL DOORS AND FRAMES INCORPORATE</CompanyName>
    <DisplayName>ARCHITECTURAL DOORS AND FRAMES INCORPORATE_1644931431</DisplayName>
    <PrintOnCheckName>ARCHITECTURAL DOORS AND FRAMES INCORPORATE</PrintOnCheckName>
    <Active>true</Active>
    <PrimaryPhone>
      <FreeFormNumber>(305) 805-1444</FreeFormNumber>
    </PrimaryPhone>
    <PrimaryEmailAddr>
      <Address>Olga@adfdoors.net</Address>
    </PrimaryEmailAddr>
    <DefaultTaxCodeRef>3</DefaultTaxCodeRef>
    <Taxable>true</Taxable>
    <BillAddr>
      <Id>594</Id>
      <Line1>166 WEST 25 STREET</Line1>
      <City>Hialeah</City>
      <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
      <PostalCode>33010</PostalCode>
    </BillAddr>
    <ShipAddr>
      <Id>595</Id>
      <Line1>166 WEST 25 STREET</Line1>
      <City>Hialeah</City>
      <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
      <PostalCode>33010</PostalCode>
    </ShipAddr>
    <Job>false</Job>
    <BillWithParent>false</BillWithParent>
    <Balance>0</Balance>
    <BalanceWithJobs>0</BalanceWithJobs>
    <CurrencyRef name="United States Dollar">USD</CurrencyRef>
    <PreferredDeliveryMethod>None</PreferredDeliveryMethod>
    <IsProject>false</IsProject>
  </Customer>
</IntuitResponse>


