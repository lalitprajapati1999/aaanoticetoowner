REQUEST URI FOR SEQUENCE ID 00627
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..fJT-aYkIH8Ipeqfai5R4Kw.npUPpcOFC53WqOJQLbKLbUaM_qDeHyrgiDdQk0xnj54om1O6sEv6MkBtVOeRnprlMu0Fdt59Hul3NE066AviPReq-gY1pqhVPzl32Le9hDkjkPPVZs-1PGKq2rz-6-P9JqnAWnSUIIyIEkGTxFenNJi4NSIbqFV3V2B9mWgdglab6eRBuDSrg_eJpG-UXBfjTLL-5w2dXq_ooZiEOSFEIXXmX3LORz8gZvqmoibTiWFIoJcMjBN_yAwguRvKRaxz9whYn9yeeEgOzkTpPhQ7TKPkZQ8G31N2mJoSzUVt8aQ0N-uNolz6QQrEsLMNaaO1mL7K1avlrxTc-Q0D383jRPO3Y0ETG_gV_EEEQvgFfDo4u77zk2e-cxM8MSSDwmfSflgxva1jhS5Mv8n80ZaH3vppsTfSVTczqOBglv_O0C7Ier4WHBSc-Kq-UbzvmuJLjzZ2ZZtLcuUUcgZmV6C71qHHzwGfLkrB9znR_gCWDg6o6CpHoFL2bkPXvEygzfiax10586EbTWgSF8XUXqC8QsAaRDopJPJ3iHNHSMVsGxB9jHOiUhKOtNfPRM3uvRhi6xXNWIhE0yqNqM57ajNvrAXQjFAqe_qSO572EcboQkY-FtiTdAHoS7j78bf8clD5G-Z174F8BDzMg0JEm_D35sg1J41PXwlACNpWN1Iw51xXmPUTnZA-BgXfl9B5IbWihkqbhYsOTFpr4rXsYJshmE-Zcz4kqfwbyorfn-osBnQ.F-i20-fFcWXJoRHQLfPcsg
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 2752

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>123</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>725 S OCEAN BOULEVARD BOCA RAT</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>123_1636467436</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - ATLANTIC PACIFIC COMMUNITY BUILDERS, LLC
Parent Work Order Number - 122</ns0:Description>
    <ns0:Amount>45.00</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>45.00</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Additional address charges</ns0:Description>
    <ns0:Amount>50</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Additional Address">34</ns0:ItemRef>
      <ns0:UnitPrice>10.00</ns0:UnitPrice>
      <ns0:Qty>5</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Firm Mail</ns0:Description>
    <ns0:Amount>19.75</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Firm Mail">18</ns0:ItemRef>
      <ns0:UnitPrice>3.95</ns0:UnitPrice>
      <ns0:Qty>5</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>FED EX 12456987</ns0:Description>
    <ns0:Amount>45</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Manual">19</ns0:ItemRef>
      <ns0:UnitPrice>45</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>FED EX 123456789</ns0:Description>
    <ns0:Amount>45</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Manual">19</ns0:ItemRef>
      <ns0:UnitPrice>45</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>213</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


