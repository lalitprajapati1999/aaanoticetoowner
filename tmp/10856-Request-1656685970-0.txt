REQUEST URI FOR SEQUENCE ID 10856
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..ihlRAl38YHGz3pbEA7ezfw.dpR0DtxhcvS9au4UpmHGN5pXnkQTTWCCj7MujUUW_yCKzRuVlb9u4NyBfRXbNZHgAr2FE5SxSdupAXVBB6xbAhsWQ3r08Dg3VXpvchQ26l0Gj7H22IkokExbZWGXda7inzOcfrNS3WPLgkMuSwLoTnxPdmNvgJ7kRqObnbdJ_l4T2C_ihwcB981J-rmUnaRUF4W9nDK8puFq0sNqSpafu5_alj5DMHq37eWUMfxhMPstJGlhb_cYtWHg7Uup52xHMtsY7bN5aLCez0CE7jv2lwKmLlRnOnb-YAX0FIdnTn3vg3_o_jnNNUqCOeTKyQn-YI4CgmXMax2Xe8Q72TPojYD43qsbsEas6RNmAaLIc37zjicLnpyA-3ddknu9vua9Gf7El1aDE5osqYGwYncPADwCHI05V06sdH98Uipn6L7_RIcaQwYzJhdVCSr9MYuEpyFz7MmlWABNnPTf14UoZu3txyYzX7Uy4BaKTmgQ73Mhe509_b9h1Mlz3tFj_4Uno1NawGR6XMD2VkNgZFSpRObm_QZac6SZ2UimM8X37OTUi0v6rVYRX9xb3oMiE--AD618syJ_rxNpjN7kDdBXQnw0J3O7wUt5_ApkC9ztkog7ZIqBDVLIZ26d2lHK_NFRjJw41St-dGZtowjQvIYhd-mcfj4jD-JqG0q3c0hMeAdVr2Thamxrj2QepeckEOC_8PFsWtX6XDN9dDDVgG4K4FPhpMaGbU2xrzRtFCRgGwA.VDnyt6wiwA37ii7Zph6dpw
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 1245

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Claim of Lien</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>123456 sw 454 stretfgdfg dfgfd</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>692_1656685970</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - koko construionc
Parent Work Order Number - 686</ns0:Description>
    <ns0:Amount>185.00</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">38</ns0:ItemRef>
      <ns0:UnitPrice>185.00</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>213</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


