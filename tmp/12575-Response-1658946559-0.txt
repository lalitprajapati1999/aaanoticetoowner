RESPONSE URI FOR SEQUENCE ID 12575
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/query?minorversion=47

RESPONSE HEADERS
================
date: Wed, 27 Jul 2022 18:29:19 GMT
content-type: application/xml;charset=UTF-8
content-length: 3059
intuit_tid: 1-62e183ff-7067320062992c110787a671
x-spanid: 2de7e078-6dc0-4d0d-a089-5c26e2aab7f9
x-amzn-trace-id: Root=1-62e183ff-7067320062992c110787a671
x-content-type-options: nosniff
x-xss-protection: 1; mode=block
qbo-version: 1985.166
service-time: total=240, db=136
expires: 0
cache-control: max-age=0, no-cache, no-store, must-revalidate, private
x-envoy-upstream-service-time: 274
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-07-27T11:29:19.342-07:00">
  <QueryResponse startPosition="1" maxResults="1" totalCount="1">
    <Invoice domain="QBO" sparse="false">
      <Id>2152</Id>
      <SyncToken>0</SyncToken>
      <MetaData>
        <CreateTime>2022-07-27T09:48:05-07:00</CreateTime>
        <LastUpdatedTime>2022-07-27T09:48:05-07:00</LastUpdatedTime>
      </MetaData>
      <CustomField>
        <DefinitionId>2</DefinitionId>
        <Name>sales2</Name>
        <Type>StringType</Type>
        <StringValue>Claim of Lien</StringValue>
      </CustomField>
      <CustomField>
        <DefinitionId>3</DefinitionId>
        <Name>sales3</Name>
        <Type>StringType</Type>
        <StringValue>12255 MIRAMAR PARKWAY, MIRAMA</StringValue>
      </CustomField>
      <DocNumber>845_1658940483</DocNumber>
      <TxnDate>2022-07-27</TxnDate>
      <CurrencyRef name="United States Dollar">USD</CurrencyRef>
      <Line>
        <Id>1</Id>
        <LineNum>1</LineNum>
        <Description>Contracted By - CRAFT CONSTRUCTION COMPANY LLC
Parent Work Order Number - 844</Description>
        <Amount>185.00</Amount>
        <DetailType>SalesItemLineDetail</DetailType>
        <SalesItemLineDetail>
          <ItemRef name="CLAIM OF LIEN:Claim of Lien">38</ItemRef>
          <UnitPrice>185</UnitPrice>
          <Qty>1</Qty>
          <ItemAccountRef name="Services">1</ItemAccountRef>
          <TaxCodeRef>NON</TaxCodeRef>
          <TaxClassificationRef>EUC-99990201-V1-00020000</TaxClassificationRef>
        </SalesItemLineDetail>
      </Line>
      <Line>
        <Amount>185.00</Amount>
        <DetailType>SubTotalLineDetail</DetailType>
        <SubTotalLineDetail/>
      </Line>
      <TxnTaxDetail>
        <TxnTaxCodeRef>3</TxnTaxCodeRef>
        <TotalTax>0</TotalTax>
        <TaxLine>
          <Amount>0</Amount>
          <DetailType>TaxLineDetail</DetailType>
          <TaxLineDetail>
            <TaxRateRef>3</TaxRateRef>
            <PercentBased>true</PercentBased>
            <TaxPercent>6</TaxPercent>
            <NetAmountTaxable>0</NetAmountTaxable>
          </TaxLineDetail>
        </TaxLine>
        <TaxLine>
          <Amount>0</Amount>
          <DetailType>TaxLineDetail</DetailType>
          <TaxLineDetail>
            <TaxRateRef>4</TaxRateRef>
            <PercentBased>true</PercentBased>
            <TaxPercent>1</TaxPercent>
            <NetAmountTaxable>0</NetAmountTaxable>
          </TaxLineDetail>
        </TaxLine>
      </TxnTaxDetail>
      <CustomerRef name="AAA Construction Corp._1615909222">213</CustomerRef>
      <BillAddr>
        <Id>1204</Id>
        <Line1>301 Hialeah Drive ste 118</Line1>
        <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
        <PostalCode>33010</PostalCode>
      </BillAddr>
      <ShipAddr>
        <Id>1204</Id>
        <Line1>301 Hialeah Drive ste 118</Line1>
        <CountrySubDivisionCode>Florida</CountrySubDivisionCode>
        <PostalCode>33010</PostalCode>
      </ShipAddr>
      <FreeFormAddress>true</FreeFormAddress>
      <ShipFromAddr>
        <Id>1583</Id>
        <Line1>PO Box 22821</Line1>
        <Line2>Hialeah, FL  33002 US</Line2>
      </ShipFromAddr>
      <SalesTermRef name="Net 15">2</SalesTermRef>
      <DueDate>2022-08-11</DueDate>
      <TotalAmt>185.00</TotalAmt>
      <ApplyTaxAfterDiscount>false</ApplyTaxAfterDiscount>
      <PrintStatus>NeedToPrint</PrintStatus>
      <EmailStatus>NotSet</EmailStatus>
      <Balance>185.00</Balance>
      <TaxExemptionRef name="service">99</TaxExemptionRef>
      <AllowIPNPayment>false</AllowIPNPayment>
      <AllowOnlinePayment>false</AllowOnlinePayment>
      <AllowOnlineCreditCardPayment>false</AllowOnlineCreditCardPayment>
      <AllowOnlineACHPayment>false</AllowOnlineACHPayment>
    </Invoice>
  </QueryResponse>
</IntuitResponse>


