REQUEST URI FOR SEQUENCE ID 10704
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..Ov6vLXn8ZS0hWGCa12vYhw.VsSOIMBxZ02dXlzzXdM1bhT2SHYCGMrdqI9iBUzzChkREi6d4pqZX7hYzDdYs_0UtRnIJQwpF8fjpQIm-TOdxrRBMUaGYUdmsvRvKZdoT1K5VQ5zgoVuMdAW79OeGZFZe0623IpoXyb8MsuARee6W3kdjco6vhVCj71N8brIXKu7W5AiVzSvRu89BwW4_Orud7wTHIOe2zxtUhUK25KYunYM7qLYEjcjwNOAPwgjWM9cfw8-OBDIjkYVh1u08Oa-4y1_rkTwEi61DEGzNc0C0HKIsZJvCByu7mvzKeImgbLWoDGF-SKvZ6vVnzCjcUGRDOf5cg3ZXeJX1DqenkC0vo4RGwSFhpFBpQiZlX0uzTq_NwcHzcfGdRiukbx8eU7n-zfeLRAtVQpf_t_w4ctrM7OI2H0N_SdVmwTNN8pcdmyxa-ZJ_1q3zD2KETO4Yaz8gfI2V4ku6_vyZKKdJI7haBVwLgS88en6K9CAXPUQd-uKm8xDn81unPZPv9-6jCMp12yaFHKB5nyWOEU2cxcv5FSktusRj_wKLN8m3moIRsBoVqF23N1pGb3abGmWG1K72C_XB6jOKEvvMXAySe9lHAwdKv5KxQhg-OSovEmYL68NnBBetOtxP3xw2W30Lq-3k2kNk5Sg0kcWJqEBtDRz38ryNp_o8xgeum1yW4lxW7XM4hWwpvlIHbz13w-FTv9sa-lSKIx75pXxkF0mqc5WLTzePdIGrrM8L2OwQT5Nw2k.aWCiG7FaNE_y4ovN_PdGIA
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 2351

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>8188 Glades Road Boca Raton, F</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>743_1656346564</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - Hawkins Construction, Inc.</ns0:Description>
    <ns0:Amount>45.00</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>45.00</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Rush Fee</ns0:Description>
    <ns0:Amount>12.50</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Rush Hour Charges">33</ns0:ItemRef>
      <ns0:UnitPrice>12.50</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Firm Mail</ns0:Description>
    <ns0:Amount>3.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Firm Mail">18</ns0:ItemRef>
      <ns0:UnitPrice>3.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail with Electronic Return Receipt</ns0:Description>
    <ns0:Amount>14.44</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Electronic Return Receipt">15</ns0:ItemRef>
      <ns0:UnitPrice>7.22</ns0:UnitPrice>
      <ns0:Qty>2</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>228</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


