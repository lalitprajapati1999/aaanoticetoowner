REQUEST URI FOR SEQUENCE ID 03267
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

REQUEST HEADERS
================
Authorization: Bearer eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..N42be664VaNagEMDv7RnoA.LnloJ3kafOzajHtnKqVsqahHyUl3fph8IsRlVKaFO-ZpUY7jR2ecvvBgLKEPfHEdBB8XLjRSIx6eWY76RUav4n8M-ZQ7xANlceChukPWc3lCsY3rQbGR7_IwiJr0TzhrqKzuQr1hw0-KQcZ06AbHyOHJYXbMtfTtqAi8ffstgE6_di52sVPXxElp_qmXHga6kb2VGJLoOPRdApJzDT5jlChfIHnRRBrTobm11v4OHOBGlqb6OhpvnG-ZKVPvhFnglR9rYxQPg0In9VkS8whAfEeZ9Hf2LOi3-030MVZdjnOC7KB-pWaJ25riuREojIsQpqm6XN-3_3NwBueZ5XtSkgwSyRrGxH2EFAcb7jz3_7PVJ0RlCKgmqLkM2qlXN7-F6k5iRBAC_hMgGtIk9yqzjDhibHn0ULvkbIIGoXjowsv2E7NFySGyN8saJ81k005B57dLYHMOfYmIqnbFEJXm2K__BMSm7qc6B8i79bXzLvis-5RG96NhWRvKX0qUB9w1VPFNOLEk-pf4YYv5nL5kUg3Zc03ZwzcTCZTYKgDWdQsOn3Jx4xHoSV_witzPrpfmsMkvrQUTC-nkhTsS8qx4lqGsT75xR2w9rbAZ1jAX3o0fwDsKv-CEbX07r_eR7sYd_7VDo2sGTnKjiRdlRZuVpPhaeCaAtudwxDNiXJdDmvt1v8GX-x8-eyrdVz4Dv4zYshnbtS018tmR-iy-v_TqKYyrjQB9sjF2NMbg3AQSldI.bO-DUxhRlFDYVvnrZN-YcA
host: quickbooks.api.intuit.com
user-agent: V3PHPSDK5.3.6
accept: application/xml
connection: close
content-type: application/xml
content-length: 2321

REQUEST BODY
=============
<?xml version="1.0" encoding="UTF-8"?>
<ns0:Invoice xmlns:ns0="http://schema.intuit.com/finance/v3">
  <ns0:CustomField>
    <ns0:DefinitionId>1</ns0:DefinitionId>
    <ns0:Name>Job Ref No</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>2</ns0:DefinitionId>
    <ns0:Name>Notice Type</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>Notice to Owner/Preliminary No</ns0:StringValue>
  </ns0:CustomField>
  <ns0:CustomField>
    <ns0:DefinitionId>3</ns0:DefinitionId>
    <ns0:Name>Job Location</ns0:Name>
    <ns0:Type>StringType</ns0:Type>
    <ns0:StringValue>111 BRINY AVENUE POMPANO BEACH</ns0:StringValue>
  </ns0:CustomField>
  <ns0:DocNumber>196_1645041394</ns0:DocNumber>
  <ns0:Line>
    <ns0:Description>Contracted By - FLORIDA BUILDING &amp; SUPPLY INC.</ns0:Description>
    <ns0:Amount>45.00</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Notice">32</ns0:ItemRef>
      <ns0:UnitPrice>45.00</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Firm Mail</ns0:Description>
    <ns0:Amount>3.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Firm Mail">18</ns0:ItemRef>
      <ns0:UnitPrice>3.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>Certified Mail</ns0:Description>
    <ns0:Amount>5.42</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Certified Mail">17</ns0:ItemRef>
      <ns0:UnitPrice>5.42</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:Line>
    <ns0:Description>C 70191120000063845232-196</ns0:Description>
    <ns0:Amount>6.95</ns0:Amount>
    <ns0:DetailType>SalesItemLineDetail</ns0:DetailType>
    <ns0:SalesItemLineDetail>
      <ns0:ItemRef name="Manual">19</ns0:ItemRef>
      <ns0:UnitPrice>6.95</ns0:UnitPrice>
      <ns0:Qty>1</ns0:Qty>
    </ns0:SalesItemLineDetail>
  </ns0:Line>
  <ns0:CustomerRef>247</ns0:CustomerRef>
  <ns0:SalesTermRef>2</ns0:SalesTermRef>
</ns0:Invoice>


