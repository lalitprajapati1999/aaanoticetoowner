RESPONSE URI FOR SEQUENCE ID 09615
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

RESPONSE HEADERS
================
date: Tue, 24 May 2022 14:13:23 GMT
content-type: application/xml
content-length: 509
intuit_tid: 1-628ce802-49c289520a9ca23468f92e0b
x-spanid: 13737726-640e-4bc8-9d4d-8944621614f5
x-amzn-trace-id: Root=1-628ce802-49c289520a9ca23468f92e0b
x-content-type-options: nosniff
x-envoy-upstream-service-time: 977
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-05-24T07:13:22.476-07:00">
  <Fault type="ValidationFault">
    <Error code="6000" element="">
      <Message>A business validation error has occurred while processing your request</Message>
      <Detail>Business Validation Error: You cannot enter more than 31 characters in the sales_custom_1_val field. You tried entering CICC 7040-..., which is 40 characters.</Detail>
    </Error>
  </Fault>
</IntuitResponse>


