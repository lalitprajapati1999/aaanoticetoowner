RESPONSE URI FOR SEQUENCE ID 09929
==================================
https://quickbooks.api.intuit.com/v3/company/193514782950659/invoice?minorversion=47

RESPONSE HEADERS
================
date: Mon, 06 Jun 2022 14:46:31 GMT
content-type: application/xml
content-length: 509
intuit_tid: 1-629e1347-0beabe190b102bcb1788bd20
x-spanid: 8d7a594d-4688-4654-8a86-1acf9b82b382
x-amzn-trace-id: Root=1-629e1347-0beabe190b102bcb1788bd20
x-content-type-options: nosniff
x-envoy-upstream-service-time: 526
strict-transport-security: max-age=31536000
server: envoy

RESPONSE BODY
=============
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<IntuitResponse xmlns="http://schema.intuit.com/finance/v3" time="2022-06-06T07:46:31.299-07:00">
  <Fault type="ValidationFault">
    <Error code="6000" element="">
      <Message>A business validation error has occurred while processing your request</Message>
      <Detail>Business Validation Error: You cannot enter more than 31 characters in the sales_custom_1_val field. You tried entering NTO 342- c..., which is 46 characters.</Detail>
    </Error>
  </Fault>
</IntuitResponse>


