<?php

return [
// Registration User Fields Lables
    'full_name'                   => 'Full Name',
    'email'                       => 'Email',
    'password'                    => 'Password',
    'retype_password'             => 'Retype password',
    'remember_me'                 => 'Remember Me',
    'register'                    => 'Register',
    'register_a_new_membership'   => 'Register a new membership',
    'i_forgot_my_password'        => 'I forgot my password',
    'i_already_have_a_membership' => 'I already have a membership',
    'sign_in'                     => 'Sign In',
    'log_out'                     => 'Log Out',
    'toggle_navigation'           => 'Toggle navigation',
    'login_message'               => 'Sign in to start your session',
    'register_message'            => 'Register a new membership',
    'password_reset_message'      => 'Reset Password',
    'reset_password'              => 'Reset Password',
    'send_password_reset_link'    => 'Send Password Reset Link',

 // Registration Customer Fields Lables

    'company_name'                => 'Your Company Name',
    'contact_person'              => 'Contact Person',
    'company_physical_address'    => 'Company Physical Address',
    'email'                       => 'Email',
    'username'                    => 'Username',
    'password_confirmation'       => 'Password Confirmation',
    'fax'                         => 'Fax Number',
    'office_number'               => 'Office Number',
    'mobile_number'               => 'Mobile Number',
    'additional_phone_number'     => 'Additional Phone Number',
    'payment_terms'               => 'Payment terms',
    'mailing_address'             => 'Company Mailing Address',
    'mailing_city'                => 'Mailing City',
    'mailing_state'               => 'Mailing State',
    'physical_city'               => 'Physical Address City',
    'physical_state'              => 'Physical Address State',
    'company_email'               => 'Company Email',
    'hear_about'                  => 'How did you hear about us?',
    'no_of_offices'               => 'No of offices',
    'agent_first_name'            => 'First Name',
    'agent_last_name'             => 'Last Name',
    'agent_title'                 => 'Title',
    'repres_company_branch_name'  => 'Position',
    'repres_email'                => 'Email Address',
    'branch_name'                 => 'Branch Name',
    'branch_phone'                => 'Phone Number',
    'branch_address'              => 'Address',
    'branch_country'              => 'County',
    'branch_zip'                  =>  'ZipCode',
    //Address-book field lables
    'addr_book_company_name'      => 'Company Name',
    'company_address'             => 'Company Address',
    'zip'                         => 'Zip Code',
    "sales_person"                => 'Sales Person',
    "business_phone"              => 'Business Phone',
    "name"                        => 'Name',
    "mobile"                      => 'Mobile',
    'emailLable'                  => 'Email',
    'contact_name'                => 'Enter Name',
    'contact_email'               => 'Enter Email',
    'contact_fax'                 => 'Fax Number',
    'contact_mobile'              => 'Cell Number',
    'sales_person_name'           => 'Enter Name',
    'sales_person_email'          => 'Enter Email',
    'sales_person_fax'            => 'Fax Number',
    'sales_person_mobile'         => 'Cell Number',

    //new work order labels
    'data_request'                => 'Data Request',
    'parent_work_order'           => 'Parent Work Order',
    'project_name'                => 'Project Name',
    'project_address'             => 'Project Address',
    'amount_due'                  => 'Amount Due',
    'agreement_date'              => 'Agreement Date',
    'labor_furnished'             => 'Labor/Service',
    'contracted_by'               => 'Contracted By',
    'document_type'               => 'Document Type',
    
    //Secondary Document field labels
    'project_address'            => 'Project Address',
    'customer_name'              => 'Customer Name'

];

//address-book field lables
