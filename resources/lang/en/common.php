<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Common label Language File
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the common label on pages.
    |
    */

    'add' => 'Add',
    'edit' => 'Edit',
    'save' => 'Save',
    'update' => 'Update',
    'delete' => 'Delete',
    'name' => 'Name',
    'type' => 'Type',
    'status' => 'Status',
    'options' => 'Options',
    'type' => 'Type',
    'type' => 'Type',
    'sn' => 'SN',
    'make' => 'Make',
    'not_records' => 'No records found',
    //dynamic forms
    'data_request' => "Data Request",
    'section'       => "Section",
    'sort_order'     => "Sort Order",
    'allow_cyo'       => 'Allow Cyo',
    'state'     => 'State',
    'allow_deadline_calculator' => 'Allow Deadline Calculator',
    'is_claim_of_lien' => 'Is claim of lien'
];
