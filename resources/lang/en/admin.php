<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Admin Section Language File
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the admin section pages.
    |
    */

    'welcome' => 'Welcome',
    'profile' => 'Profile',

    // Notice
    'notice' => 'Notice',
    'notices' => 'Notices',
    'hard_doc' => 'Hard Document',
    'soft_doc' => 'Soft Document',
    'secondary_doc' => 'Secondary Document',
    'manage_forms' => 'Manage Forms',
    'state' => 'State',
    'states' => 'States',
    'select_states_notice' => 'Select state from below list to manage form for :notice notice',
    'manage_state_notice_form' => 'Manage form fields for :notice notice in :state state',
    'form' => 'Form',
    'add_field' => 'Add Field',
    'edit_field' => 'Edit Field',
    'manage_form' => 'Manage Form',
    'validation' => 'Validation',
    'is_required' => 'Is this field required ?',
    'allow_cyo_enable'  => 'Enable',
    'allow_cyo_disable' => 'Disable',
    'allow_deadline_enable'  => 'Enable',
    'allow_deadline_disable' => 'Disable',
     'is_claim_of_lien_enable'  => 'Enable',
    'is_claim_of_lien_disable' => 'Disable',
];
