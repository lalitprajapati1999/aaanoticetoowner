@extends('layouts.page')
@section('banner')
<section class="sec-banner deadline-banner"></section>
@stop
@section('content')

<section class="section-wrapper pricing">
    <div class="main-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="section-heading">
                    <h1 class="section-title text-center">We get it done, you get paid</h1>
                </div>
            </div>
        </div>

        <div class="price-body">
            <div class="row">
                @if(!empty($pricings))
                @foreach($pricings as $pricing)
                @if($pricing->additional_address!="additional_address")
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="price-box">
                        <div class="price-top">
                            <h1>${{number_format($pricing->charge, 0, ".", "")}}</h1>
                            <!-- <h4>{{$pricing->description}}</h4> -->
                        </div>
                        <div class="price-middle">
                            <ul class="list-style">
                                <li>
                                    <h4>{{$pricing->name}}</h4>
                                    <h2>{{$pricing->lower_limit}} - @if($pricing->upper_limit=="99999999") Above @else {{$pricing->upper_limit}} @endif</h2>
                                    @if(Auth::check())
                                    <div class="price-button">
                                        <a href="{{ url('home') }}" class="btn btn-primary custom-btn2"><span>Start Today</span></a>
                                    </div>
                                    @else
                                    <div class="price-button">
                                        <a href="{{ url('register') }}" class="btn btn-primary custom-btn2"><span>Start Today</span></a>
                                    </div>
                                    @endif
                                </li>
                                <li class="listing-group text-center">
                                    <h3 class="pricem-title">One Price all Recipients</h3>
                                    <ul class="list-group">
                                        <li>Unlimited Recipients</li>
                                        <li>Unlimited Projects</li>
                                        <li>Unlimited Database</li>
                                        <li>Free Partial Release</li>
                                        <li>Free Final Release</li>
                                        <li>Deadline Reminder</li>
                                        <li>Chat Support</li>
                                    </ul>
                                </li>
                            </ul>
                        </div>                            
                    </div>
                </div>  
                @endif
                @endforeach
                @endif    
            </div>
            <!--  <div class="row">
                  @if(!empty($pricings))
                  @foreach($pricings as $pricing)
                  <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                      <div class="optional-text">
                          <p>{{($pricing->description)?html_entity_decode($pricing->description):''}}</p>
                      </div>
                  </div>
                  @endforeach
                  @endif 
              </div>-->
            <div class="row" id="showsubscribe">
                <div class="col-md-12">
                    <div class="section-heading">
                        <h1 class="section-title text-center">you create it we mail it.</h1>
                    </div>
                </div>
            </div>
            @if (Session::get('warning'))
            <div class="row">
                <div class="col-md-3">

                </div>
                <div class="col-md-6">
                    <div class="alert alert-warning">
                        {{Session::get('warning')}}
                    </div>
                </div>
                <div class="col-md-3">

                </div>
            </div>
            @endif
            <div class="row">
                @if(!empty($packages))
                @php($cnt = 0)
                @foreach($packages as $package)
                <div class="col-lg-3 col-sm-6 col-xs-12 {{ ($cnt == 0)?'col-lg-offset-3 col-md-3 col-md-offset-3':''}}">
                    <div class="price-box">
                        <div class="price-top">
                            <h1>${{number_format($package->charge, 0, ".", "")}}</h1>
                            <h4>{{$package->name}}</h4>
                        </div>
                        <div class="price-middle">
                            <div class="listing-group text-center">
                                <h3 class="pricem-title">{{$package->description}}</h3>
                                <ul class="list-group full-width">
                                    <li>Unlimited Recipients</li>
                                    <li>Unlimited Projects</li>
                                    <li>Unlimited Database</li>
                                    <li>Free Partial Release</li>
                                    <li>Free Final Release</li>
                                    <li>Deadline Reminder</li>
                                    <li>Chat Support</li>
                                    <li>You Create it We Mail it</li>
                                </ul>
                            </div>
                        </div>
                        <div class="price-button">
                            @if(isset($subscription))
                                @if(in_array($package->id,$subscription,true))
                                <a href="{{url('pricing/'.$package->id.'/subscribe')}}" class="btn btn-primary custom-btn2" disabled><span>Already Subscribe</span></a>
                                @else
                                 <a href="{{url('pricing/'.$package->id.'/subscribe')}}" class="btn btn-primary custom-btn2"><span>Subscribe</span></a>
                                @endif
                            @else
                            <a href="{{url('pricing/'.$package->id.'/subscribe')}}" class="btn btn-primary custom-btn2"><span>Subscribe</span></a>
                            @endif
                        </div>
                    </div>
                </div>
                @php ($cnt++)
                @endforeach
                @endif

            </div>
            <!--        <div class="pricing-list">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <ul class="list-group">
                                    <li>Notice to owner</li>
                                    <li>Preliminary Notice</li>
                                    <li>Partial Release</li>
                                    <li>Final Release</li>
                                </ul>
                                 <h4>$ 30 Including all recipients</h4> 
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <ul class="list-group">
                                    <li>Waivers</li>
                                    <li>Notice of Non-Payment</li>
                                    <li>Bond Claim</li>
                                    <li>Intent To Lien</li>
                                    
                                </ul>
                                 <h4>$ 30 Including all recipients</h4> 
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <ul class="list-group">
                                    <li>Claim Of lien</li>
                                    <li>Satisfaction Of  Lien</li>                        
                                </ul>
                                 <h4><span>Call Us Today</span> Lowest Price Guaranteed</h4> 
                            </div>
                        </div>
                    </div>-->
        </div>
</section>
@stop