@extends('layouts.main')

@section('frontend_css')

	@yield('css')
@stop

@section('body')
  
    <!-- ./wrapper -->
<!--------------------header starts-------------------------->
 @include('layouts.inc.header')
 
<!--------------------banner starts--------------------------> @yield('banner') 
<!--------------------banner ends-------------------------->

@yield('content') 


<!-------------copyrights starts-------------------->
@include('layouts.inc.footer')
 
<a id="back-to-top" href="#" class="back-to-top" role="button" data-toggle="tooltip" data-placement="left">Top</a>

@stop

@section('frontend_js')

<script>
 new WOW().init();
</script>
<script type="text/javascript">

$(document).ready(function(){
  
     $(window).scroll(function () {
        if ($(this).scrollTop() > 50) {
            $('#back-to-top').fadeIn();
        } else {
            $('#back-to-top').fadeOut();
        }
    });
     if($('#back-to-top').length){
    // scroll body to 0px on click
    $('#back-to-top').click(function () {
        $('#back-to-top').tooltip('hide');
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });
    
    $('#back-to-top').tooltip('show');
}
    $(window).scroll(function () {
        if ($(this).scrollTop() > 50) {
            $('.navbar-brand').addClass('fixed-brand');
        } else {
            $('.navbar-brand').removeClass('fixed-brand');
        }
    });
      window.ParsleyConfig = {
         //   errorsWrapper: '<div></div>',
            errorTemplate: '<span class="alert alert-danger parsley"></span>',
            classHandler: function (el) {
                return el.$element.closest('input');
            },
            successClass: 'has-success',
            errorClass: 'has-error'
        };
        
        //set nope string to autocomplete to disable autocomplete
    $('.datepicker').on('click', function(e) {
        e.preventDefault();
        $(this).attr("autocomplete", "nope");  
    });
});

 </script>

@yield('js')
@stop
