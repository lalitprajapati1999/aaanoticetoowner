<!DOCTYPE html>
<html>
<head>
    <title>
      {{ isset($title) ? $title.' :: '.config('adminlte.title', 'AdminLTE 2') : config('adminlte.title', 'AdminLTE 2') }}
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

@yield('before_styles')
    <link rel="stylesheet" type="text/css" href="{{asset('css/select2.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('css/hover-min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/owl.carousel.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/owl.theme.default.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-select.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/parsley.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/jquery.mCustomScrollbar.css')}}">    
    <link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/jquery.ui.autocomplete.css')}}">

    <!------------date 29 nov---------------------->
     @if(config('adminlte.plugins.select2'))
        <!-- Select2 -->
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css">
        @endif

        <style type="text/css">
        @media screen and (-ms-high-contrast: active), (-ms-high-contrast: none) {
            .services > li .polygon-shape {
                -webkit-clip-path: none;
                -o-clip-path: none;
                -ms-clip-path: none;
                clip-path: none;
                background-color: transparent !important;
                width: 100%;
                
            }
            .services > li{
                display: inline-block;
                width: 25%;
                float: left;
                height: 670px;
                background-position: center !important;
                background-repeat: no-repeat !important;
                background-size: 100% 100% !important;
            }
            .services > li:first-child{
                background: url({{asset('images/left1.png')}}) ;
            }
            .services > li:nth-child(2){
                background: url({{asset('images/left2.png')}}) ;
            }
            .services > li:nth-child(3){
                background: url({{asset('images/left3.png')}}) ;
            }
            .services > li:last-child{
                background: url({{asset('images/left4.png')}}) ;
                
            }
        }
           
        </style>
    <!------------date 29 nov---------------------->

    @yield('frontend_css')

    @yield('after_styles')
</head>
<body>

@yield('body')

@yield('before_scripts')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
<script  type="text/javascript" src="{{ asset('js/select2.js') }}"></script>
<script src="{{ asset('js/wow.js') }}"></script>
<script src="{{asset('js/bootstrap-select.js')}}"></script>
<script src="{{ asset('js/owl.carousel.min.js') }}"></script>
<script src="{{asset('js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('js/parsley.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.mCustomScrollbar.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/custom.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.mask.js') }}"></script>

@yield('frontend_js')

</body>
</html>