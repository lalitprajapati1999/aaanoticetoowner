<!DOCTYPE html>
<html>
<head>
   <title>NTO</title>
  
   
   
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('css/hover-min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/owl.carousel.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-select.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap-select.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
    <script type="text/javascript" src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>

@yield('dashboard-body')
<!-- @yield('before_scripts')
 --><script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>

<script  type="text/javascript" src="{{ asset('js/bootstrap-select.js') }}"></script>
<script  type="text/javascript" src="{{ asset('js/wow.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/custom.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/moment.min.js') }}"></script>

<script type="text/javascript" src="{{asset('js/bootstrap-datepicker.js')}}"></script>
@yield('frontend_js')
</body>
</html>