@extends('layouts.dashboard.main')
@section('dashboard-body')
<body class="dashboard-body">
	<section class="dashboard-main">
		<div class="container-fluid">
			<div class="row">
				<button type="button" id="sidebarCollapse" class="btn btn-info">
			       <i class="fa fa-bars" aria-hidden="true"></i><span>Sidebar</span>
			    </button>
				@include('layouts.inc.sidebar')
			
				<div class="" id="content">
					<div class="dashboard-content">
						@include('layouts.inc.dashboard_header')
						
						 <div class="main-content">
                            <div class="tab-content">
                        @yield('content_header')
						@yield('content')
					</div>
				</div>

						
					</div>
				</div>
			</div>	
		</div>
	</section>
	<!-------------copyrights starts-------------------->
	<!-- <footer class="copyrights text-center dashboard-footer">
		<div class="main-wrapper">
			<div class="row">
				<div class="col-md-12">
					<p>2018 All Rights Reserved by AAA Business Association Corp DBA AAA Notice to Owner</p>
				</div>
			</div>
		</div>
	</footer>	 -->
@stop

<!-------------copyrights starts-------------------->

@section('frontend_js')
 <script>
 	 new WOW().init();
 </script>
 <script type="text/javascript">
 	$(document).ready(function(){
     $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
     if($('#back-to-top').length){
        // scroll body to 0px on click
        $('#back-to-top').click(function () {
            $('#back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });
        $('#back-to-top').tooltip('show');
    }
        $('.datepicker').datepicker({
	      uiLibrary: 'bootstrap'
	    });
	    $('#sidebarCollapse').on('click', function () {
	        $('#sidebar, #content').toggleClass('active');
	    });
	    $(function() {
	    	if($('.red-tooltip').length){
			$('.red-tooltip').tooltip()
			$(".red-tooltip").tooltip();
		}
		});
	});
 </script>
 @stop	
