
					<div class="" id="sidebar">
					<div class="left-sidebar">
						  <div class="sidebar-logo">
						  		<a href="index.html"><img src="{{url('images/logo.png')}}" class="img-responsive center-block"></a>
						  </div>

						  <ul class="nav nav-tabs" >
						    <li role="presentation" class="active">
                                                        <a href="{{url('customer/statement-invoices')}}" aria-controls="home" >
						    	<div class="left-icon statement-icon"></div>
                                                        <span>Statement</span>
                                                        </a>
                                                    </li>

						    <li>
                                                        <a href="{{url('customer/contacts')}}" aria-controls="profile" >
						    	<div class="left-icon address-icon"></div>
                                                        <span>Address Book</span>
                                                        </a>
                                                    </li>

						    <li><a href="{{url('customer/new-work-order')}}" aria-controls="messages">
						    	<div class="left-icon work-icon"></div><span>New Work Order</span></a></li>

						    <li><a href="{{url('customer/secondary-document')}}" aria-controls="settings">
						    	<div class="left-icon document-icon"></div><span>Secondary Document</span></a></li>

						    <li role="presentation"><a href="#vieworder" aria-controls="settings" role="tab" data-toggle="tab">
						    	<div class="left-icon order-icon"></div><span>View Work Order</span></a></li>

						    <li role="presentation"><a href="#createown" aria-controls="settings" role="tab" data-toggle="tab">
						    	<div class="left-icon create-icon"></div><span>Create Your Own</span></a></li>

						    <li><a href="{{url('admin/change-password')}}" aria-controls="settings">
						    	<div class="left-icon setting-icon"></div><span>Account Setting</span></a></li>

						    	<li><a href={{url('admin/edit-account-info')}} aria-controls="settings">
						    	<div class="left-icon setting-icon"></div><span>Profile</span></a></li>

						    	<li><a href={{url('admin/change-password')}} aria-controls="settings">
						    	<div class="left-icon setting-icon"></div><span>Change Password</span></a></li>
						  </ul>
					</div>
				</div>
