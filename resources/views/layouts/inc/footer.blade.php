<footer class="copyrights text-center">
	<div class="main-wrapper">
		<div class="row">
			<div class="col-md-12">
				<p>{{ date('Y').' '.config('adminlte.footer_note') }}</p>
			</div>
		</div>
	</div>
</footer>