<div class="dashboard-header full-width">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
							</div>
							<!-- Collect the nav links, forms, and other content for toggling -->
							<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">      
								<form class="search-bar navbar-left" method="GET" role="search">
									<div class="form-group search-box">
							            <input type="text" class="form-control" placeholder="Search">
							            <span class="form-group-btn">
							            <button class="btn btn-search" type="button" data-original-title="" title=""><span class="icon-search"></span></button>
							            </span>
							        </div>
								</form>
								<ul class="nav navbar-nav navbar-right">
									<li><a href="tel:1866 222-5479"><img src="{{url('images/phone-icon-w.png')}}"><span>1866 222-5479</span></a></li>
									<li class="dropdown acc-user">
									<a href="#" class="dropdown-toggle full-width" data-toggle="dropdown" role="button" aria-expanded="false">
										<img src="{{url('images/user-dummy.png')}}"><span>Welcome, {{Auth::User()->name}}</span>
										<span class="caret"></span></a>
										<ul class="dropdown-menu" role="menu">
											{{-- <li class=""><a href="#">Other Link</a></li>
											<li class=""><a href="#">Other Link</a></li>
											<li class=""><a href="#">Other Link</a></li>
											<li class=""><a href="#">Other Link</a></li> 
											<li class="divider"></li> --}}
											<li>  <a class="btn btn-default btn-flat" href="javascript::void(0);" 
                                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                                        >
                                            <i class="fa fa-fw fa-power-off"></i> {{ trans('adminlte::adminlte.log_out') }}
                                        </a>
                                        <form id="logout-form" action="{{ url(config('adminlte.logout_url', 'auth/logout')) }}" method="POST" style="display: none;">
                                            @if(config('adminlte.logout_method'))
                                                {{ method_field(config('adminlte.logout_method')) }}
                                            @endif
                                         {{ csrf_field() }}
                                        </form>
                                    </li>
										</ul>
									</li>
								</ul>
							</div><!-- /.navbar-collapse -->
						</div>
