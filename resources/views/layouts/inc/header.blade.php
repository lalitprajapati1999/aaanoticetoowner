<header>
	<div class="">
		<div class="header">
			<nav class="navbar navbar-default main-nav navbar-fixed-top" role="navigation">
			  <!-- Brand and toggle get grouped for better mobile display -->
			  <div class="main-wrapper">
			  <div class="navbar-header">
			    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
			      <span class="sr-only">Toggle navigation</span>
			      <span class="icon-bar"></span>
			      <span class="icon-bar"></span>
			      <span class="icon-bar"></span>
			    </button>
			    <a class="navbar-brand" href="{{ url('/') }}"><img src="{{ url('images/logo.png')}}" class="logo"></a>
			  </div>
			  <!-- Collect the nav links, forms, and other content for toggling -->
			  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			  	<div class="menu-wrapper"  data-wow-duration="1s" data-wow-delay="0.5s">
			  		<ul class="nav navbar-nav navbar-right">
				       	<li class="menu-item"><a href="{{ url('/') }}" class="hvr-underline-from-center">Home</a></li>
				     	<li class="menu-item"><a href="{{url('lien-laws')}}" class="hvr-underline-from-center">Lien blog</a></li>
				     	<li class="menu-item"><a href="{{url('/deadline-calculator')}}" class="hvr-underline-from-center">Deadline Calculator</a></li>
				     	<li class="menu-item"><a href="{{url('pricing')}}" class="hvr-underline-from-center">Pricing</a></li>    
				     	<li class="menu-item contact-no"><a href="tel:1866 222-5479" class="hvr-underline-from-center"><img src="{{ url('images/phone-icon.png')}}">1866 222-5479</a></li>

            			@if (Route::has('login'))  
				     	<li class="menu-item">
				     		<ul>
                    		@auth
								<li class="login-item"><a href="{{ url('/login') }}" class="hvr-underline-from-center">Dashboard</a></li>
		                    @else
                        		@if (config('adminlte.register_url', 'register'))
				     			<li class="login-item"><a href="{{ route(config('adminlte.register_url', 'register')) }}" class="hvr-underline-from-center">Register</a></li>
		                        @endif
		                        @if (config('adminlte.login_url', 'login'))
				     			<li class="login-item"><a href="{{ route(config('adminlte.login_url', 'login')) }}" class="hvr-underline-from-center">Login</a></li>
		                        @endif
		                    @endauth
				     		</ul>
				     	</li>
            			@endif       
				    </ul>
			  	</div>
			  </div><!-- /.navbar-collapse -->
			</div>
			</nav>
		</div>
	</div>
</header>
<!--------------------header ends-------------------------->