@extends('adminlte::page')
@section('content')
<div>
    @if (Session::get('success'))
    <div class="alert alert-success">
        {!! Session::get('success') !!}
    </div>
    @endif
    <section class="create-own">
        <div class="dashboard-wrapper">
            <div class="dashboard-heading">
                <!-- <ol class="breadcrumb">
<li><a href="{{ backpack_url() }}">{{ config('adminlte.title', 'AdminLTE 2') }}</a></li>
<li class="active">{{ trans('backpack::base.dashboard') }}</li>
</ol> -->
                <h1><span>Dashboard</span></h1>
            </div> 
            <div class="dashboard-inner-body">
                <div class="createown-wrapper">
                    
                    <div class="grid">
                        <figure class="effect-sadie">
                            <figcaption>
                                <a href="{{url('customer/new-work-order')}}"><h2>New Work Order</h2></a>
                                <a href="{{url('customer/new-work-order')}}"><p>New Work Order</p></a>
                            </figcaption>			
                        </figure>
                    </div>
                    <div class="grid">
                        <figure class="effect-sadie">
                            <figcaption>
                                <a href="{{url('customer/secondary-document')}}"><h2>Secondary Document</h2></a>
                                <a href="{{url('customer/secondary-document')}}"><p>Secondary Document</p></a>
                            </figcaption>			
                        </figure>
                    </div>
                    <div class="grid">
                        <figure class="effect-sadie">
                            <figcaption>
                                <a href="{{url('customer/contacts')}}"><h2>Address Book</h2></a>
                                <a href="{{url('customer/contacts')}}"><p>Address Book</p></a>
                            </figcaption>			
                        </figure>
                    </div>
                     <div class="grid">
                        <figure class="effect-sadie">
                            <figcaption>
                                <a href="{{url('customer/view-work-order')}}"><h2>View Work Order</h2></a>
                                <a href="{{url('customer/view-work-order')}}"><p>View Work Order</p></a>
                            </figcaption>			
                        </figure>
                    </div>
                   <!--   <div class="grid">
                        <figure class="effect-sadie">
                            <figcaption>
                                <a href="{{url('customer/statement-invoices')}}"><h2>Statement</h2></a>
                                <a href="{{url('customer/statement-invoices')}}"><p>Statement</p></a>
                            </figcaption>			
                        </figure>
                    </div> -->
                     <div class="grid">
                        <figure class="effect-sadie">
                            <figcaption>
                                <a href="{{url('customer/invoice')}}"><h2>Invoice</h2></a>
                                <a href="{{url('customer/invoice')}}"><p>Invoice</p></a>
                            </figcaption>           
                        </figure>
                    </div>
                    <div class="grid">
                        <figure class="effect-sadie">
                            <figcaption>
                                <a href="{{url('admin/edit-account-info')}}"><h2>Account Setting</h2></a>
                                <a href="{{url('admin/edit-account-info')}}"><p>Account Setting</p></a>
                            </figcaption>			
                        </figure>
                    </div>
                   
                    <div class="grid">
                        <figure class="effect-sadie">
                            <figcaption>
                                <a href="{{url('customer/create-your-own')}}"><h2>Create Your Own</h2></a>
                                <a href="{{url('customer/create-your-own')}}"><p>Create Your Own</p></a>
                            </figcaption>			
                        </figure>
                    </div>
                   

                </div>
            </div>
            
            <div>
                <h3><span><b>Important: It is your responsibility</b></span><h3>
                <span style="font-size:18px;">Your item is eligible for USPS Tracking Plus. This feature allows you to buy extended access to your tracking history and receive a statement via email upon request. It is Important that if you have a project that its over one year of length on receiving payment, you must save your Certified mail with usps.com by select quick tools enter your tracking # on NTO located above recipients and select USPS tracking plus. </span>
            </div>
        </div>
    </section>
</div>

@stop