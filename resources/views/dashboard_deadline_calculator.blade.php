@extends('layouts.page')

@section('banner')
<section class="sec-banner deadline-banner"></section>
@stop

@section('content')

<!--<section class="section-wrapper deadline-calculator">-->
<section class="register-sec deadline-calc">

    <div class="main-wrapper">
         <div class="row">
            <div class="col-md-12">
                <div class="register-form">
                    <div class="register-sub">
                        <div class="section-heading text-center">
                            <h2 class="section-title">Don't wait get paid<br/></h2>
                                <h3>Protect your payment by sending notice to your jobs</h3>
                        </div>
                        @if (Session::get('success'))
                        <div class="alert alert-success alert-dismissible">
                            <?php echo Session::get('success'); ?>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
                        </div>
                        @endif
                            <form class="form-horizontal" action="{{url('store/deadline/contact-us')}}" method="post" data-parsley-validate="">
                            {!! csrf_field() !!}
                            <h3>Your Information:</h3>
                             <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                            <input type="text" name="company_name" class="form-control" data-parsley-required="true" data-parsley-required-message="Company Name is required" value="{{old('company_name')}}"/>
                                            <label>Company Name<span class="mandatory-field">*</span></label>
                                            <span></span>
                                            @if ($errors->has('company_name'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('company_name') }}</strong>
                                            </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                            <input type="text" name="name" class="form-control" data-parsley-required="true" data-parsley-required-message="Name is required" value="{{old('name')}}" />
                                            <label>Your Name<span class="mandatory-field">*</span></label>
                                            <span></span>
                                            @if ($errors->has('name'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                            <input type="email" name="email" class="form-control"  data-parsley-required="true" data-parsley-required-message="Email is required" value="{{old('email')}}"/>
                                            <label>Email<span class="mandatory-field">*</span></label>
                                            <span></span>
                                            @if ($errors->has('email'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                            <input type="text" name="phone" class="form-control" id="phone" data-parsley-required="true" data-parsley-required-message="Phone is required" data-parsley-trigger="change focusout"  id="fax_number" data-parsley-pattern="(\(\d{3})\)\s\d{3}\-\d{4}$" value="{{old('phone')}}"/>
                                            <label>Phone<span class="mandatory-field">*</span></label>
                                            <span></span>
                                            @if ($errors->has('phone'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <div class="row">
                               <div class="col-md-6 col-sm-6">
                                   <div class="input-wrapper full-width">
                                          <label for="ReCaptcha">Recaptcha:</label>
                                        <div class="styled-input">
                                {!! NoCaptcha::renderJs() !!}
                                {!! NoCaptcha::display() !!}
                                
                                  @if ($errors->has('g-recaptcha-response'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                            </p>
                                            @endif
                                              </div>
                                   </div>
                               </div>
                              </div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="form-bottom-btn">
                                        <a href="{{url('/')}}" class="btn btn-primary custom-btn customb-btn"><span>Cancel</span></a>
                                        <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>Send</span></button>
                                    </div>
                                </div>
                            </div>
                        
                        </form>
                    </div>
                </div>
            </div>
        </div>
<!--        <div class="row">
            <div class="col-md-12">
                <div class="section-heading">
                    <h1 class="section-title">Deadline Calculator</h1>
                    <h2 class="section-subpara">If you want to calculate deadline calculator please join us.</h2>
                     <div class="col-md-12 col-sm-12 col-xs-12 wow bounceIn">
                        <a href="{{ route(config('adminlte.register_url', 'register')) }}" type="button" class="btn btn-primary custom-btn"><span>Join Us</span></a>
                    </div>
                </div>
            </div>
        </div>-->
        <div class="deadline-body">
            
        </div>
    </div>
</section>
<!-----------------page body ends------------------>

@stop
@section('frontend_js')
<script>
    new WOW().init();
    $('#state').on('change',function(){
         $('#notices').find('option').remove().end().append('<option value="">Select</option>');
         
        $("#notices").selectpicker("refresh");
            $.ajax({
            data: {'state': $(this).val()},
            type: "post",
            url: "{{url('deadline-calculator/notices') }}",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
//             var result = $.parseJSON(response);
           //  console.log(result);
                
               $.each(response,function(e,val) {
                    $("#notices").append("<option value='"+val.id+"'>"+val.name+"</option>");
                    $("#notices").selectpicker("refresh");
                });
               
            }
        });
    });
</script>
<script type="text/javascript">
 
    $(document).ready(function () {
        $('input,textarea,select').on('focus', function(e) {
            e.preventDefault();
            $(this).attr("autocomplete", "nope");  
        });

        $("#phone").mask("(999) 999-9999");
        $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('#back-to-top').click(function () {
            $('#back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });

        $('#back-to-top').tooltip('show');
        $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('.navbar-brand').addClass('fixed-brand').css('transition', '0.3 all ease-in-out 0s');
            } else {
                $('.navbar-brand').removeClass('fixed-brand');
            }
        });
        
        $('#notices').on('change',function(){
            
            if($('#notices').val()== '9'){
                $('.firstjob').html('First Day On The Job*');
            }else if($('#notices').val()== '3'){
                $('.firstjob').html('Last Day On The Job*');
            }else if($('#notices').val()== ''){
                $('.firstjob').html('');
            }
        })
    });
</script>
@stop