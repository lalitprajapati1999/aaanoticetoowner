<!-- <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    </head>
    <body>
       
    </body>
</html> -->
<!DOCTYPE html>
<html>
  <head>
      <meta http-equiv="X-UA-Compatible" content="IE=Edge">
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <style type="text/css">
      </style>
  </head>
<body style="margin:0; padding:0; font-family: 'Open Sans', sans-serif;font-size: 14px;text-align: center;height: 100vh;background-color: #f1f1f1;">
    <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td>
          <!--[if (gte mso 9)|(IE)]>
        <table width="630" align="center" cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td>
        <![endif]-->  
          <table width="100%" align="center" class="welcome" cellpadding="0" cellspacing="0" bgcolor="#ffffff" style="max-width: 600px;margin-top: 20px;vertical-align: middle;margin-top: 80px;">
            <tr>
              <td>
                <table width="100%" align="center" cellpadding="0" cellspacing="0" background="box-shadow-img.jpg" style="background-repeat: no-repeat; background-position: center 10px;background-size: cover;">
                  <tr>
                    <td>
                      <table width="100%" align="center" cellpadding="0" cellspacing="10">
                        <tr>
                          <td width="100%" align="center">
                            <img src="{{ $message->embed(public_path() . '/images/logo.png') }}" />
                               

                          </td>
                        </tr>
                        <tr>
                          
                        </tr>
                        <!-- <tr>
                          <td height="10"></td>
                        </tr> -->
                      </table>
                    </td>
                  </tr>
                </table>
              </td>  
            </tr>
            <tr>
              <td>
                <table width="100%" align="center" cellpadding="0" cellspacing="10">
                  <tr>
                    
                    <td width="100%" align="left" style="padding: 0 50px 30px 50px;">
                     <p style="font-family: 'Open Sans', sans-serif;font-size: 17px;color: #696969">
                      @if(isset($name))<span style="padding-bottom: 10px;display:block;">
                     Dear Admin,
                     </span></br>
                     @endif
                      <p>New payment received by check on NTO by-</p><br/>
                      Name: {{$name}}<br/>
                      {{$email}}<br/>
                      Subscription Package:{{$package_name}}<br/>
                      To update chaque status 
                      <a href="{{$stopnotificationurl}}">Click here</a>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
            <tr>
              <td>
                  <table width="100%" bgcolor= "#f5f5f5" border="0" cellpadding="20" cellspacing="0">
                    <tr>
                      <td style="text-align: center;font-family: 'Open Sans', sans-serif;color: #898989;">Copyright © {{date('Y')}} AAA Notice Owner. All rights reserved.</td>
                    </tr>
                  </table>
              </td>
            </tr>
          </table>
         
        </td>
      </tr>
    </table>
</body>
</html>