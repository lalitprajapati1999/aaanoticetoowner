<style>
 .bootstrap-select :focus{
    outline: 0;
    border-bottom : 1px solid #032c61 !important;
 }
  
 
 .select2class :focus{
    outline: 0;
    height: 47px !important;
    border-bottom : 1px solid #032c61 !important;
}
.select2-container--default .select2-selection--single, .select2-selection .select2-selection--single {
     border-radius: 0;
     padding: 6px 12px;
     height: 34px;
     border: none !important;; 
     border-bottom: 1px solid #aaa !important;
 }
 
 .select2-container--default .select2-selection--single {
     background: none !important;
     border: 1px solid #aaa;
     border-radius: 4px;
 }
</style>
@extends('adminlte::page')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<div  class="tab-pane active" id="workorder" role="tabpanel">

<div class="tab-content">
<section class="work-order address-book-add-readonly view-order">
   <div class="dashboard-wrapper">
   @if (Session::get('success'))
   <div class="alert alert-success">
      <?php echo Session::get('success'); ?>
   </div>
   @endif
   <div class="dashboard-heading">
      <h1><span>REQUEST FOR {{strtoupper($notice['name'])}}</span></h1>
   </div>
   <div class="dashboard-inner-body">
      <div class="notice-tabs">
         <div class="full-width">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs display-ib pull-left" role="tablist">
               @if($tab=="project")
               <li role="presentation" id="project_tab" class="active"><a href="#project" aria-controls="project" role="tab" data-toggle="tab">Project</a></li>
               <!-- <li role="presentation"><a href="#recepients" aria-controls="recepients" role="tab" data-toggle="tab">Recipients</a></li>-->
               @else 
               <li role="presentation"><a href="#project" aria-controls="project" role="tab" data-toggle="tab">Project</a></li>
               <!-- <li role="presentation" class="active"><a href="#recepients" aria-controls="recepients" role="tab" data-toggle="tab">Recipients</a></li>-->
               @endif
               <!--<li role="presentation" id="address_tab" class="{{ ($tab == 'address_verify')?'active':''}}"><a href="#verify_address" aria-controls="verify_address" role="tab" data-toggle="tab" >Address Verification</a></li>-->
            </ul>
         </div>
         <!--                        <div class="correction-div">
            <div class="display-ib pull-right">
                <a href="#" class="btn btn-default corr-btn" disabled="">Corrections</a>
                <a href="#" class="btn btn-default notice-btn" disabled="">Notes</a>
            
            </div>
            </div>-->
         <!-- Tab panes -->
         <div class="tab-content">
            @if($tab=="project")
            <div role="tabpanel" class="tab-pane active" id="project">
               @elseif($tab=='recipients')
               <div role="tabpanel" class="tab-pane" id="project">
                  @endif
                  <div class="project-wrapper">
                     <form id="secondary_doc_project" action="{{url('customer/partial-document')}}" enctype="multipart/form-data" method="post" date-parsley-validate="">
                        <div class="row">
                          <div class="col-md-5"> </div> 
                            <div class="col-md-7 save_btn_rec pull-right text-right">
                          
                              <a href="{{url('customer/secondary-document')}}" class="btn btn-primary custom-btn customb-btn"><span>Back</span></a>
                              <a href="{{url('home')}}" class="btn btn-primary custom-btn customb-btn"><span>Cancel</span></a>
                              <button type="submit" class="btn btn-primary custom-btn customc-btn save_for_later" name="submit" value="save_for_later" id="save_for_later"><span>Save For Later</span></button>
                              <button type="submit" name="submit" class="btn btn-primary custom-btn customc-btn" value="continue"><span>Continue</span></button>
                           </div>
                        </div>
                        <div class="dash-subform mt-0">
                           <!--action="{{url('customer/partial-document')}}"-->
                           <meta name="_token" content="{{ csrf_token() }}" />
                           {!! csrf_field() !!}
                            <input type="text" name="notice_id" value="{{$notice['id']}}" hidden="">
                           <div class="row">
                              <!--                                                <div class="col-md-6 col-sm-6">
                                 <div class="date-box full-width">
                                     <div class="input-wrapper full-width">
                                         <div class="styled-input">
                                             <label>Date</label>
                                             <input class="form-control" name="date" type="text" readonly="" value="{{date('Y-m-d')}}">
                                             <span></span>
                                         </div>
                                     </div>
                                 </div>
                                 </div>-->

                              @if(Auth::user()->hasrole('account-manager')||Auth::user()->hasrole('admin'))
                              <div class="col-md-12 col-sm-12">
                                 <div class="select-opt">
                                    <label class="col-md-12 control-label" for="textinput">Customers<span class="mandatory-field">*</span></label>
                                    <select name="customer_id" id="customer_id" class="selectpicker" data-parsley-required="true" data-parsley-required-message="Customer  is required" data-parsley-trigger="change focusout">
                                       <option value="">Select</option>
                                       @foreach($customers as $customer)
                                       <option value="{{$customer->id}}" @if (old('customer_id') == $customer->id) selected="selected" @endif > {{ucfirst(trans($customer->company_name))}}</option>
                                       @endforeach
                                    </select>
                                    @if ($errors->has('customer_id'))
                                    <p class="help-block">
                                       <strong>{{ $errors->first('customer_id') }}</strong>
                                    </p>
                                    @endif
                                 </div>
                              </div>
                              @endif

                              @if(isset($notice_fields))
                                 @foreach($notice_fields as $fields)
                                     @if($fields->type==6)
                                       <div class="col-md-6 col-sm-6">
                                          <div class="date-box full-width">
                                             <div class="input-wrapper full-width">
                                                <div class="styled-input">                                                               <?php $label = str_replace("_", " ", $fields->name); ?>
                                                <input class="form-control datepicker"  type="text" name="{{$fields->id.' '.$fields->name}}" id="{{$fields->name}}" data-parsley-required="true" data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout">
                                                <label>{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>
                                                <span></span>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       @elseif($fields->type==1 && $fields->name == 'date_request' )
                                          <div class="col-md-6 col-sm-6">
                                             <div class="date-box full-width">
                                                <div class="input-wrapper full-width">
                                                   <div class="styled-input">
                                                      <input class="form-control"  type="text" name="{{$fields->id.' '.$fields->name}}" value="{{date('m-d-Y')}}" data-parsley-required="false" data-parsley-required-message="{{ucfirst($fields->name)}} is required" data-parsley-trigger="change focusout" readonly="" >
                                                      <label class="full-width">Date</label>
                                                      <span></span>
                                                      </div>
                                                   </div>
                                                </div>
                                          </div>
                                       @elseif($fields->type==1 && $fields->name == 'contracted_by')
                                         <div class="col-md-6 col-sm-6 clear-b">
                                             <div class="select-opt select2-container select2class">
                                                <input type="hidden" value="1" name="contracted_by_exist">
                                                <?php $label = str_replace("_", " ", $fields->name); ?>
                                                <label class="col-md-12 control-label" for="textinput">{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>
                                                <select class="js-example-tags form-control" name="{{$fields->id.' '.$fields->name}}" id="contracted_by_select" data-parsley-required="true" data-parsley-required-message="{{ucfirst(trans($label))}} is required">
                                                <option value="">Select</option>
                                                @if(isset($names) && !empty($names))
                                                @foreach($names as $each_name)
                                                <option value="{{$each_name->company_name}}">{{$each_name->company_name}}</option>
                                                 @endforeach
                                                @endif
                                                </select>
                                             <span></span>
                                          </div>
                                       </div>
                                        @elseif($fields->type==1 && $fields->name == 'parent_work_order' )
                                            <?php $label = str_replace("_", " ", $fields->name); ?>
                                            <div class="col-md-6 col-sm-6">
                                                    <div class="select-opt">
                                                    <?php $label = str_replace("_", " ", $fields->name); ?>
                                                    <label class="col-md-12 control-label" for="textinput">{{ucfirst(trans($label))}}</label>
                                                    <select class="form-control" name="{{$fields->id.' '.$fields->name}}" data-show-subtext="true" data-live-search="true" id="work_order_no" >
                                                        <?php if(!empty($fields->value)){ ?>
                                                            <option value="{{$fields->value}}" selected="selected">{{$fields->value}}</option>   
                                                        <?php }else{ ?>
                                                            <option value=" ">Select</option>
                                                        <?php } ?>      
                                                    </select>
                                            </div>
                                            <input type="text" value=""  id="parent_work_order_no" name="work_order_no" hidden="">
                                          </div>
                                       @elseif($fields->type==1 && $fields->name == 'project_name' )
                                          <?php $label = str_replace("_", " ", $fields->name); ?>
                                       <div class="col-md-6 col-sm-6">
                                          <div class="input-wrapper full-width">
                                             <div class="styled-input">
                                                <?php $label = str_replace("_", " ", $fields->name); ?>
                                                <input class="form-control" type="text" name="{{$fields->id.' '.$fields->name}}" id="{{$fields->name}}">
                                                <label>{{ucfirst(trans($label))}}</label>
                                                <span></span>
                                                </div>
                                             </div>
                                       </div>
                                       @elseif($fields->type==1 && $fields->name == 'project_owner' )
                                          <?php $label = str_replace("_", " ", $fields->name); ?>
                                       <div class="col-md-6 col-sm-6">
                                          <div class="input-wrapper full-width">
                                             <div class="styled-input">
                                                <?php $label = str_replace("_", " ", $fields->name); ?>
                                                <input class="form-control" type="text" name="{{$fields->id.' '.$fields->name}}" id="{{$fields->name}}">
                                                <label>{{ucfirst(trans($label))}}</label>
                                                <span></span>
                                                </div>
                                             </div>
                                       </div>
                                       @elseif($fields->type==1 && $fields->name == 'county')
                                          <?php $label = str_replace("_", " ", $fields->name); ?>
                                             <div class="col-md-6 col-sm-6">
                                               <div class="input-wrapper full-width">
                                                            <div class="styled-input">
                                                                <input class="form-control"  type="text" name="parent_country" id="parent_country" data-parsley-required="true" data-parsley-required-message="Select County from the list" data-parsley-trigger="change focusout">
                                                                <input type="hidden" name="{{$fields->id.' '.$fields->name}}"  id="county_id" />
                                                                <label class="col-md-12 control-label" for="textinput">{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label> 
                                                                <span></span>     
                                                            </div>
                                                            <div id="recipientcountyloader" style="display: none">
                                                                <img width="150px" src="{{asset('images/loader.gif')}}"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                       @elseif($fields->type==1 && $fields->name == 'comment')
                                                    <?php $label = str_replace("_", " ", $fields->name); ?>
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="input-wrapper full-width">
                                                            <div class="styled-input">
                                                                <?php $label = str_replace("_", " ", $fields->name); ?>
                                                                <input class="form-control"  type="text" name="{{$fields->id.' '.$fields->name}}" id="{{$fields->name}}"  data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout">
                                                                <label>{{ucfirst(trans($label))}}</span></label>
                                                                <span></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                       @elseif($fields->type==1)
                                                    <?php $label = str_replace("_", " ", $fields->name); ?>
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="input-wrapper full-width">
                                                            <div class="styled-input">
                                                                <?php $label = str_replace("_", " ", $fields->name); ?>
                                                                <input class="form-control"  type="text" name="{{$fields->id.' '.$fields->name}}" id="{{$fields->name}}" data-parsley-required="true" data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout">
                                                                <label>{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>
                                                                <span></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                   @elseif($fields->type==2 && $fields->name=="project_address")
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="input-wrapper full-width">
                                                            <label class="col-md-12 control-label">Project Address<span class="mandatory-field">*</span></label>
                                                            <div class="styled-input">
                                                                <?php $label = str_replace("_", " ", $fields->name); ?>
                                                                <textarea id="{{$fields->name}}" class="form-control projects_address" name="{{$fields->id.' '.$fields->name}}" data-parsley-required="true" data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout"  rows="5"></textarea>
                                                                <span></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                 @elseif($fields->type==2)
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="input-wrapper full-width">
                                                            <div class="styled-input">
                                                                <?php $label = str_replace("_", " ", $fields->name); ?>
                                                                <textarea id="{{$fields->name}}" class="form-control" name="{{$fields->id.' '.$fields->name}}"  data-parsley-required="true" data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout"></textarea>
                                                                <label>{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>
                                                                <span></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                              @elseif($fields->type==3 && $fields->name =='authorise_agent')
                                             
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="select-opt select-custom-opt">
                                                            <?php $label = str_replace("_", " ", $fields->name); ?>
                                                            <label class="col-md-12 control-label" for="textinput"  >{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>  
                                                            <select name="{{$fields->id.' '.$fields->name}}" class="selectpicker" id="officers_directors" data-parsley-required="true" data-parsley-required-message="Authorise signature is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true">
                                                            <option value="">Select</option>
                                                            @if(isset($officers_directors) && count($officers_directors)>0 )
                                                            @foreach($officers_directors as $officer)
                                                            <option value="{{$officer->first_name.' '.$officer->last_name}}" @if (old('signature') == $officer->first_name.' '.$officer->last_name) selected="selected" @endif > {{ucfirst(trans($officer->first_name.' '.$officer->last_name))}}</option>
                                                            @endforeach
                                                            @endif
                                                         </select>
                                                        </div>
                                                    </div>
                                             
                                                @elseif($fields->type==3 && $fields->name =='title')
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="select-opt">
                                                            <?php $label = str_replace("_", " ", $fields->name); ?>
                                                            <label class="col-md-12 control-label" for="textinput">{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>  

                                                            <select class="selectpicker" name="{{$fields->id.' '.$fields->name}}" id="officers_directors_title" class="selectpicker"  data-parsley-required="true" data-parsley-required-message="Authorise signature is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true">
                                                                <option value="">Select</option>
                                                                @if(isset($officers_directors) && count($officers_directors)>0)
                                                                @foreach($officers_directors as $officer)
                                                                <option value="{{$officer->title}}" @if (old($fields->id.' '.$fields->name) == $officer->id) selected="selected" @endif > {{ucfirst(trans($officer->title))}}</option>
                                                                @endforeach
                                                                @endif
                                                            </select>
                                                        </div>
                                                    </div>  
                                                @elseif($fields->type==3 && $fields->name =='city')

                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="select-opt">
                                                            <?php $label = str_replace("_", " ", $fields->name); ?>
                                                            <label class="col-md-12 control-label" for="textinput">{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>  
                                                            <select class="selectpicker" name="{{$fields->id.' '.$fields->name}}" id="hard_doc_city_value" data-parsley-required="true" data-parsley-required-message="City is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true">
                                                                <option value="">Select</option>
                                                                @foreach($cities as $project_city)
                                                                <option value="{{$project_city->id}}"> {{ucfirst(trans($project_city->name))}}</option>
                                                                @endforeach
                                                            </select>

                                                        </div>
                                                    </div>
                                                    @elseif($fields->type==3 && $fields->name =='state')
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="select-opt">
                                                            <?php $label = str_replace("_", " ", $fields->name); ?>
                                                            <label class="col-md-12 control-label" for="textinput"  >{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>  
                                                            <select class="selectpicker" name="{{$fields->id.' '.$fields->name}}" id="hard_doc_state_value" data-parsley-required="true" data-parsley-required-message="State is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true">
                                                                <option value="">Select</option>
                                                                @foreach($states as $state)
                                                                <option value="{{$state->id}}" > {{ucfirst(trans($state->name))}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    
                                                  @elseif($fields->type==4 && $fields->name =='notary_seal')
                                                    <?php $label = str_replace("_", " ", $fields->name); ?>
                                                    <div class="col-md-6 col-sm-6 clear-b">
                                                        <div class="input-wrapper full-width work-status">
                                                            <ul class="checkbox-holder">
                                                                <li><a href="#"><input  class="regular-checkbox not-empty"  id="notary_seal" name="{{$fields->id.' '.$fields->name}}" value="1" type="checkbox"><label for="{{ $fields->name}}">{{ucfirst(trans($label))}}</label></a></li>
                                                                <input name="{{$fields->id.' '.$fields->name}}" value="0" id="notary_seal_hidden" class="checkbox " type="hidden">
                                                            </ul>
                                                        </div>
                                                    </div>
                                                @elseif($fields->type==4 && $fields->name =='check_clear')
                                                    <?php $label = str_replace("_", " ", $fields->name); ?>
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="input-wrapper full-width work-status">
                                                            <ul class="checkbox-holder">
                                                                <li><a href="#"><input  class="regular-checkbox not-empty"  id="{{ $fields->name}}" name="{{$fields->id.' '.$fields->name}}" value="1" type="checkbox"><label for="{{ $fields->name}}">{{ucfirst(trans($label))}}</label></a></li>

                                                                <input name="{{$fields->id.' '.$fields->name}}" value="0" id="check_clear_hidden" class="checkbox " type="hidden">
                                                            </ul>
                                                        </div>
                                                    </div>
                                           @elseif($fields->type==5)
                                                    <?php $label = str_replace("_", " ", $fields->name); ?>
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="date-box full-width">
                                                            <div class="input-wrapper full-width">
                                                                <div class="styled-input">
                                                                    <input class="form-control" required=""  type="radio" name="{{$fields->id.' '.$fields->name}}" id="{{$fields->name}}" data-parsley-required="true" data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout">
                                                                    <label>{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>
                                                                    <span></span>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                       @endif
                                 @endforeach
                              @endif

                              
                              <!--  <div class="col-md-12 col-sm-12">
                                 <div class="row">
                                     <div class="col-md-6 col-sm-6">
                                         <div class="input-wrapper full-width">
                                             <div class="form-group">
                                                 <label class="col-md-12 control-label">Comment</label>
                                                 <textarea class="form-control" name="comment"  type="text"  data-parsley-required-message="Comment is required" data-parsley-trigger="change focusout" id="comment"></textarea>
                                                 @if ($errors->has('comment'))
                                                 <span class="help-block">
                                                     <strong>{{ $errors->first('comment') }}</strong>
                                                 </span>
                                                 @endif
                                             </div>
                                         </div>
                                     </div>
                                 </div>
                                 </div>-->
                            
                                                    <?php
                                                    $document_types = getDocumentTypes();
                                                    ?>

                                                    <div class="col-md-12 col-sm-12">
                                                        <div class="row">
                                                            <div class="col-lg-6 col-md-6 col-sm-12">
                                                                <div class="select-opt mt-top">
                                                                    <label class="col-md-12 control-label" for="textinput">Document Type</label>  
                                                                    <select class="selectpicker" name="type" id="select_document_type">
                                                                        <option value="">-Select-</option>
                                                                        @foreach ($document_types as $key => $value) 
                                                                        <option value="{{$key}}">{{$value}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    @if ($errors->has('type'))
                                                                    <span class="help-block">
                                                                        <strong>{{ $errors->first('type') }}</strong>
                                                                    </span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12 col-sm-12 doc_field">
                                                                <div class="row">
                                                                    <div class="col-md-12 col-sm-12">
                                                                        <div class="input-wrapper full-width">
                                                                            <div class="styled-input">
                                                                                <input class="form-control" name="doc_input"  type="text" id="doc_input_id">
                                                                                <label id="doc_field"></label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <input type="hidden" name="edit_document_name" id="edit_document_name">
                                                            <input type="hidden" name="edit_doc_id" id="edit_doc_id">
                                                             <input type="hidden" id="row_id">
                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                <div class="form-btn-div doc_file">
                                                                    <span class="btn btn-default btn-file upload-btn btn btn-primary form-btn" id="temp">
                                                                        Choose File to Upload  <input type="file" name="file_name" id="result_file">
                                                                    </span>
                                                                    <span id="errormessage" style="display: none"></span>
                                                                </div>
                                                            </div>
                                                             <div class="form-btn-div doc_file col-md-12 col-sm-12 col-xs-12" >
                                                                <div style="float: left;width: 10"> 
                                                                <a href="javascript:;" onClick="openTab(this)" id="open_doc" name=""><span  id="preview" style="display: none;" ></span></a></div>
                                                                <div style="float: left ;width: 10;padding-left: 20px"> 
                                                                <a  href="javascript:;" ><span id="remove_icon" style="display: none;"  >Delete</span></a></div>
                                                               
                                                                <input type="hidden" name="remove_file" id="remove_file" value="0">
                                                            </div>
                                                            <div class="col-md-12 col-sm-12 col-xs-12">

                                                                <div class="add-new-btn inner-dash-btn full-width doc_file">
                                                                    <button  name="submit_doc" type="button" value="Add" id="submit_document" class="btn btn-primary custom-btn">
                                                                        <span class="update_doc">Add</span>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12 col-sm-12">
                                                                <div class="row">
                                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                                        <div class="dahboard-table doc-table full-width table-responsive">
                                                                            <table class="table table-striped" id="document_table">
                                                                                <thead class="thead-dark">
                                                                                    <tr>
                                                                                        <th scope="col">Document Type</th>
                                                                                        <th scope="col">Document Description</th>
                                                                                        <th scope="col">Name</th>
                                                                                        <th scope="col">Date & Time</th>
                                                                                        <th scope="col">Type of Attachment</th>
                                                                                        <th scope="col">Options</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                <td class="text-center norecords" colspan="6">No record found</td>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-bottom-btn save_btn1">
                                                        <input type="hidden" name="secondary_doc_attachment[]" id="secondary_doc_attachment"/>
                                                         <input type="hidden" name="work_order_attachment_id[]" id="work_order_attachment_id"/>
                                                        <input type="hidden" name="remove_doc[]" id="remove_doc"/>
                                                        <input type="hidden" name="partial_final_type" value="{{$partial_final_type}}"/>
                                                        <a href="{{url('customer/secondary-document')}}" class="btn btn-primary custom-btn customb-btn"><span>Back</span></a>
                                                        <a href="{{url('home')}}" class="btn btn-primary custom-btn customb-btn"><span>Cancel</span></a>
                                                        <button type="submit" class="btn btn-primary custom-btn customc-btn save_for_later" name="submit" value="save_for_later" id="save_for_later"><span>Save For Later</span></button>
                                                        <button type="submit" name="submit" class="btn btn-primary custom-btn customc-btn" value="continue"><span>Continue</span></button>
                                                    </div>
                                                </div>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                                @if($tab=="project")
                                <div role="tabpanel" class="tab-pane" id="recepients">
                                    @elseif($tab=='recipients')
                                    <div role="tabpanel" class="tab-pane active" id="recepients">
                                        @endif

                                        <div class="project-wrapper"> 
                                            <form method="post" action="{{ url('customer/secondary-recipients/'.$secondary_document_id) }}" data-parsley-validate="" >
                                                {!! csrf_field() !!}
                                                <div class="dash-subform mt-0">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="sub-section-title">
                                                                        <h2>Recipients</h2>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="dahboard-table table-responsive">
                                                                        <table class="table table-striped">
                                                                            <thead class="thead-dark">
                                                                                <tr>
                                                                                    <th scope="col">Category</th>
                                                                                    <th scope="col">Recipients</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                @if(isset($count) && !empty($count))
                                                                                @foreach($count as $k => $count)
                                                                                <tr>
                                                                                    <td class="checkbox-holder">
                                                                                        <input type="checkbox" id="checkbox-3-{{$k}}" class="regular-checkbox">
                                                                                        <label for="checkbox-3-{{$k}}">{{$count->name}}</label>
                                                                                    </td>
                                                                                    <td>{{$count->total}}</td>
                                                                                </tr>
                                                                                @endforeach  
                                                                                @else
                                                                            <td class="text-center" colspan="3">No records found</td>

                                                                            @endif
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="sub-section-title">
                                                                        <h2>Selected Recipients</h2>
                                                                    </div>
                                                                </div>
        <!--                                                        <span>Note : - Customer will be able to edit the details of recipient selected</span>-->
                                                                <div class="col-md-12">
                                                                    <div class="dahboard-table table-responsive">
                                                                        <table class="table table-striped">
                                                                            <thead class="thead-dark">
                                                                                <tr>
                                                                                    <th scope="col">Role</th>
                                                                                    <th scope="col">Company Name</th>
<!--                                                                                        <th scope="col">Contact Person</th>-->
<!--                                                                                        <th scope="col">Phone Number</th>-->
                                                                                    <th scope="col">Address</th>
                                                                                    <th scope="col">City</th>
                                                                                    <th scope="col">State</th>
                                                                                    <th scope="col">Zip Code</th>
                                                                                    <th scope="col">Action</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                @if(isset($recipients) && !empty($recipients))
                                                                                @foreach($recipients  as $recipient)
                                                                                <tr>
                                                                                    <td class="cl-blue">{{$recipient->name}}</td>
<!--                                                                                        <td>{{$recipient->contact}}</td>-->
<!--                                                                                        <td>{{$recipient->mobile}}</td>-->
                                                                                    <td>{{$recipient->address}}</td>
                                                                                    <td>{{$recipient->city->name}}</td>

                                                                                    <td>{{$recipient->state->name}}</td>
                                                                                    <td>{{$recipient->zip}}</td>
                                                                                    <td><a href="{{url('customer/secondary-recipient/delete/'.$recipient->id.'/'.$recipient->secondary_document_id.'/'.$partial_final_type)}}"><span class="icon-cross-remove-sign"></span></a></td>
                                                                                </tr>
                                                                                @endforeach
                                                                                @else
                                                                            <td class="text-center" colspan="5">No records found</td>

                                                                            @endif
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="dash-subform">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="sub-section-title">
                                                                <h2>Recipients</h2>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <div class="row">
                                                                <div class="col-md-6 col-sm-6">
                                                                    <input type="text" name="partial_final_type" value={{$partial_final_type}} hidden="">
                                                                    <input type="text" name="secondary_document_id" id="sec_doc_id" value={{$secondary_document_id}} hidden="">
                                                                    <div class="select-opt">

                                                                        <label class="col-md-12 control-label" for="textinput">Type of Recipient<span class="mandatory-field">*</span></label>  
                                                                        <select class="selectpicker" name="category_id" data-parsley-required="true" data-parsley-required-message="Type of recipient is required" data-parsley-trigger="change focusout">
                                                                            <option value="">select</option>
                                                                            @foreach($categories as $category)
                                                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                                                            @endforeach

                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6">

                                                            <div class="select-opt">
                                                                <label class="col-md-12 control-label" for="textinput">Name<span class="mandatory-field">*</span></label>  
                                                                <select class="selectpicker"  name="name" id="recipient_name" data-parsley-required="true" data-parsley-required-message="Name is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true">
                                                                    <option value="">Select</option>
                                                                    @if(isset($names) && !empty($names))
                                                                    @foreach($names as $each_name)
                                                                    <option value="{{$each_name->company_name}}">{{$each_name->company_name}}</option>
                                                                    @endforeach
                                                                    @endif
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-bottom-btn pd-25">
                                                                @if($tab == 'project')
                                                                <button type="button" name="add_contact_submit" class="btn btn-primary custom-btn customc-btn" value="contacts" disabled=""><span>Add Address Book</span></button>
                                                                <button type="button" name="add_contact_submit" class="btn btn-primary custom-btn customc-btn" value="contacts" disabled=""><span>Update Address Book</span></button>
                                                                @elseif($tab == 'recipients')
                                                                <button type="button" name="add_contact_submit" class="btn btn-primary custom-btn customc-btn" value="contacts" id="add_contact" data-toggle="modal" data-target="#contactModal"><span>Add Address Book</span></button>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6">
                                                            <div class="input-wrapper full-width">
                                                                <label class="col-md-12 control-label" for="textarea">Attn/Additional Recipient Name<span class="mandatory-field"></span></label>
                                                                <div class="styled-input">
                                                                    <textarea class="form-control recipient_attns"  type="text" name="attn" id="recipient_attn" data-parsley-trigger="change focusout" ></textarea>
                                                                    <span></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6">
                                                            <div class="input-wrapper full-width">
                                                                <div class="styled-input">
                                                                    <input class="form-control"  type="text" name="contact" id="contact"  data-parsley-required-message="Telephone is required">
                                                                    <label>Telephone</label>
                                                                    <span></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--                                                            <div class="col-md-6 col-sm-6 clear-b">
                                                                                                                        <div class="input-wrapper full-width">
                                                                                                                            <div class="styled-input">
                                                                                                                                <input class="form-control" type="text" name="mobile"  id="mobile" data-parsley-required-message="Mobile number is required" data-parsley-trigger="change focusout">
                                                                                                                                <label>Mobile Number</label>
                                                                                                                                <span></span>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>-->
                                                        <div class="col-md-6 col-sm-6">
                                                            <div class="input-wrapper full-width">
                                                                <div class="styled-input">
                                                                    <input class="form-control" type="text" name="email" id="email"  data-parsley-required-message="Email is required" data-parsley-trigger="change focusout">
                                                                    <label>Email</label>
                                                                    <span></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-sm-12">
                                                            <div class="input-wrapper full-width">
                                                                <div class="styled-input">
                                                                    <input class="form-control"  type="text" name="address" id="address" data-parsley-required="true" data-parsley-required-message="Address is required" data-parsley-trigger="change focusout">
                                                                    <label>Address<span class="mandatory-field">*</span></label>
                                                                    <span></span>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="col-md-6 col-sm-6 clear-b">
                                                            <div class="input-wrapper full-width">
                                                                <div class="styled-input">
                                                                    <input class="form-control"  type="text" name="city_id" id="city" data-parsley-required="true" data-parsley-required-message="Select City from the list" data-parsley-trigger="change focusout">
                                                                    <input type="hidden" name="recipient_city_id" id="recipient_city_id"/>
                                                                    <label class="col-md-12 control-label" for="textinput">City<span class="mandatory-field">*</span></label> 
                                                                    <span></span>                                                                
                                                                </div>
                                                                <div id="recipientloader" style="display: none">
                                                                    <img width="150px" src="{{asset('images/loader.gif')}}"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <input type="text" name="state_id" value="" class="state" hidden="">
                                                        <div class="col-md-6 col-sm-6">
                                                            <div class="select-opt">
                                                                <label class="col-md-12 control-label" for="textinput">State* </label>  
                                                                <select class="selectpicker" id="state" name="state_id" data-parsley-required="true" data-parsley-required-message="State is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true">
                                                                    <option value="">Select</option>
                                                                    @foreach($states as $state)
                                                                    <option value="{{$state->id}}">{{$state->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 clear-b">
                                                            <div class="input-wrapper full-width">
                                                                <div class="styled-input">
                                                                    <input class="form-control" type="text" name="zip" id="zip" data-parsley-required="true" data-parsley-required-message="Zip Code is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true">
                                                                    <label>Zip Code<span class="mandatory-field">*</span></label>
                                                                    <span></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--                                                            <div class="col-md-6 col-sm-6">
                                                                                                                        <div class="select-opt">
                                                                                                                            <label class="col-md-12 control-label" for="textinput">County</label>  
                                                                                                                            <select class="selectpicker" id="country" name="country_id"  data-parsley-required-message="Country is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true">
                                                                                                                                <option value="">Select</option>
                                                                                                                                @foreach($countries as $country)
                                                                                                                                <option value="{{$country->id}}">{{$country->name}}</option>
                                                                                                                                @endforeach
                                                                                                                            </select>
                                                                                                                        </div>
                                                                                                                    </div>-->
                                                        <div class="col-md-12">
                                                            <div class="form-bottom-btn pd-25">


                                                                @if($tab=="project")
<!--                                                                    <input type="hidden" name="verified_addr" id="verified_addr" value="false">-->
                                                                <button type="submit" name="continue" class="btn btn-primary custom-btn customc-btn" disabled=""><span>Add Recipient</span></button>
                                                                <button  type="submit" name="continue"  class="btn btn-primary custom-btn customc-btn update_recipient" value="update" disabled=""><span>Update Recipient</span></button>

                                                                <div id="ajax_loader"></div>
                                                                @elseif($tab=='recipients')
<!--                                                                    <input type="hidden" name="verified_addr" id="verified_addr" value="false">-->
                                                                <input type="hidden" name="add_in_address_book" id="add_in_address_book">
                                                                <button  type="submit" name="continue"  class="btn btn-primary custom-btn customc-btn"><span>Add Recipient</span></button>
                                                                <button  type="submit" name="continue"  class="btn btn-primary custom-btn customc-btn update_recipient" value="update" disabled=""><span>Update Recipient</span></button>
                                                                <div id="ajax_loader"></div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-bottom-btn">
                                                            <a href="#" class="btn btn-primary custom-btn customb-btn" id="createBack"><span>Back</span></a>
                                                            <a href="{{url('home')}}" class="btn btn-primary custom-btn customb-btn"><span>Cancel</span></a>
                                                            <a href="#" class="btn btn-primary custom-btn customb-btn" disabled="" title="Please Fill all mandetory project information"><span>Save for Later</span></a>
                                                            <input type="button" id="createLabel" name="create_label_submit" class="btn btn-primary custom-btn customc-btn" value="Continue" disabled="" title="Please Fill all mandetory project information"/>
                                                        </div>
                                                    </div>

                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <!-- add new contacts-->

                                    <div id="contactModal" class="modal fade register-modal addcon-modal">


                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <div id="success-msg"class="alert alert-success" style="display: none"></div>
                                                    <h4 class="modal-title">Add Contact</h4>	
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                </div>
                                                <div class="modal-body">
                                                    <form method="POST" id="add_recipient_contact">
                                                        {{csrf_field()}}
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="input-wrapper full-width">
                                                                <div class="styled-input">

                                                                    <input class="form-control"  type="text" name="contact_name" value="{{old('contact_name')}}" id="contact_name">
                                                                    <label>Name<span class="mandatory-field">*</span></label>
                                                                    <span></span>
                                                                    <p class="help-block">
                                                                        <strong id="name-error"></strong>
                                                                        </span>  
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-xs-12 ">
                                                            <div class="input-wrapper full-width">
                                                                 <label class="col-md-12 control-label" for="textarea">Attn/Additional Recipient Name<span class="mandatory-field"></span></label>
                                                                <div class="styled-input">
                                                                    <textarea class="form-control attn recipient_attns"   name="attn" value="{{old('attn')}}" id="attn" ></textarea>
                                                                    <p class="help-block">
                                                                        <strong id="attn-error"></strong>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="input-wrapper full-width">
                                                                <div class="styled-input">
                                                                    <input class="form-control"  type="text" name="contact_no" value="{{old('contact_no')}}" id="contact_no">
                                                                    <label>Telephone</label>
                                                                    <span></span>
                                                                    <p class="help-block">
                                                                        <strong id="contact-error"></strong>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="input-wrapper full-width">
                                                                <div class="styled-input">
                                                                    <input class="form-control"  type="email" name="contact_email" value="{{old('contact_email')}}" id="contact_email">
                                                                    <label>Email</label>
                                                                    <span></span>
                                                                    <p class="help-block">
                                                                        <strong id="email-error"></strong>
                                                                        </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="input-wrapper full-width">
                                                                <div class="styled-input"> 
                                                                    <input class="form-control"  type="text" name="contact_address" value="{{old('contact_address')}}" id="contact_address">
                                                                    <label>Address<span class="mandatory-field">*</span></label>
                                                                    <span></span>
                                                                    <p class="help-block">
                                                                        <strong id="address-error"></strong>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="input-wrapper full-width">
                                                                <div class="styled-input">
                                                                    <input class="form-control"  type="text" name="city_id" id="city" data-parsley-required="true" data-parsley-required-message="Select City from the list" data-parsley-trigger="change focusout">
                                                                    <input type="hidden" name="recipient_city_id" id="recipient_city_id"/>
                                                                    <label class="col-md-12 control-label" for="textinput">City<span class="mandatory-field">*</span></label> 
                                                                    <span></span>                                                                
                                                                </div>
                                                                <div id="recipientloader" style="display: none">
                                                                    <img width="150px" src="{{asset('images/loader.gif')}}"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="select-opt">
                                                                <label class="col-md-12 control-label">State<span class="mandatory-field">*</span></label>
                                                                <select class="selectpicker"  name="contact_state_id" data-parsley-required="true" data-parsley-required-message="State is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true" id="contact_state_id">
                                                                    <option value="">Select</option>
                                                                    @foreach($states as $state)
                                                                    <option value="{{$state->id}}" @if(old('contact_state_id')) selected="selected" @endif>{{$state->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                                <p class="help-block">
                                                                    <strong id="state-error"></strong>
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="input-wrapper full-width">
                                                                <div class="styled-input">

                                                                    <input class="form-control"  type="text" name="contact_zip" value="{{old('contact_zip')}}" id="contact_zip">
                                                                    <label>Zip Code<span class="mandatory-field">*</span></label>
                                                                    <p class="help-block">
                                                                        <strong id="zip-error"></strong>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="select-opt">
                                                                <label class="col-md-12 control-label">Country</label>
                                                                <select class="selectpicker" name="contact_country" data-show-subtext="true" data-live-search="true" id="contact_country">
                                                                    <option value="">Select</option>
                                                                    @foreach($countries as $country)
                                                                    <option value="{{$country->id}}" @if(old('contact_country')) selected="selected" @endif>{{$country->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                                <p class="help-block">
                                                                    <strong id="country-error"></strong>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>

                                                <div class="modal-footer">
                                                    <div class="form-bottom-btn">
                                                        <input type="hidden" name="edit_secondary_doc_id" value="{{$secondary_document_id}}" id="doc_id_recipient"/>
                                                        <button type="submit" class="btn btn-primary custom-btn customc-btn" id="submit_contact"><span>Add Contact</span></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>  

                                    </div>
                                </div>
                            </div></div>
                        </section>	
                    </div>
                </div>
                <div id="address_verification_modal" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Verified Addresses</h4>
                            </div>
                            <div class="modal-body">
                                <div id="alternate_addresses"></div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" id="choose_address">Use chosen address</button>
                            </div>
                        </div>
                    </div>
                </div>
                <form action="" method="POST" class="remove-attachment-model"  id="remove-attachment-model">
                    {!! csrf_field() !!}
                    <div id="attachment-confirmation-modal" class="modal fade">
                        <div class="modal-dialog modal-confirm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div class="icon-box">
                                        <i class="fa fa-close"></i>
                                    </div>				
                                    <h4 class="modal-title">Are you sure?</h4>	
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                </div>
                                <div class="modal-body">
                                    <p>Do you really want to delete these records? This process cannot be undone.</p>
                                </div>
                                <div class="modal-footer">
                                    <div class="form-bottom-btn">
                                        <button type="button" class="btn btn-primary custom-btn customb-btn remove-attachement-from-delete-form" data-dismiss="modal"><span>Cancel</span></button>
                                        <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>Confirm</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </form>


                @stop
@section('frontend_js'))
<script type="text/javascript" src="{{ asset('js/jquery-ui.min.js') }}"></script>

<script src="{{ asset('vendor/adminlte/plugins/datepicker/bootstrap-datepicker.js') }}" type="text/javascript"></script>

<script type="text/javascript">
$(document).on('click', '#notary_seal', function (e) {
        if ($(this).is(':checked')) {
            $('#notary_seal_hidden').val('1');
        } else {
            $('#notary_seal_hidden').val('0');
        }

    });

    $(document).on('click', '#check_clear', function (e) {
        if ($(this).is(':checked')) {
            $('#check_clear_hidden').val('1');
        } else {
            $('#check_clear_hidden').val('0');
        }

    });
$('#secondary_doc_note').parsley();
$('#secondary_doc_project').parsley();

//Start to submit note
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$('.notice-btn').click(function (e) {

    $('input[name=note]').val('');
    $('input[name=email]').val('');
    $('.note-success').empty();
    $('.note-success').css('display', 'none');
    $('.note-error').empty();
    $('.note-error').css('display', 'none');
});
$(".submitNote").click(function (e) {
    e.preventDefault();
    var note = $("input[name=note]").val();
    var email = $("input[name=email]").val();
    var secondary_doc_id = $('#secondary_doc_id').val();
    $.ajax({
        type: 'POST',
        url: '{{url("customer/secondary-document/store_note")}}',
        data: {note: note, email: email, doc_id: secondary_doc_id},
        success: function (data) {
            if (data.success) {
                $('.note-error').empty();
                $('.note-error').css('display', 'none');
                $('.note-success').show();
                $('.note-success').append('<p>' + data.success + '</p>');
                $('.correction_div').append('<li>' + data.correction.note + '<br>' + data.correction.email + '</li>');

            } else if (data.errors) {
                $.each(data.errors, function (key, value) {
                    $('.note-error').show();
                    $('.note-error').append('<p>' + value + '</p>');
                });
            }
        }
    });
});
//End to submit note
//back to project tab
$('#createBack').click(function () {
    $('.display-ib li').removeClass('active');
    $('#project_tab').addClass('active');
    $('.notice-tabs .tab-pane').removeClass('active');
    $('#project').addClass('active');
})
//Start to fetch data of related work order

$('#work_order_no').on('change', function () {
    $(".delete_parent_attachment").remove();
     $('#parent_work_order_no').val($('#work_order_no').val());
    $.ajax({
        data: {'work_order_no': $('#work_order_no').val()},
        type: 'post',
        url: "{{url('customer/secondary-document/work_order_details')}}",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            if ($('#work_order_no').val() == "") {
                location.reload();
            }
            if (response.success) {
                var stateid = "";
                $.each(response.success, function (key, val) {

                    if (val.name == 'state') {
                        $('#soft_doc_state_value').val(val.value);
                        $("#soft_doc_state_value").selectpicker("refresh");
                    } else if (val.name == 'city') {
                        $('#soft_doc_city_value').val(val.value);
                        $("#soft_doc_city_value").selectpicker("refresh");

                    } else if (val.name == 'county') {
                        $('#county_id').val(val.value);
                        $("#parent_country").val(val.county_name);
                    } else if (val.name == 'project_type') {

                        $('#project_type').val(val.value);
                        $("#project_type").selectpicker("refresh");

                    } else if (val.name == 'your_role') {

                        $('#your_role').val(val.value);
                        $("#your_role").selectpicker("refresh");

                    } else if (val.name == 'contracted_by') {
                        $("#contracted_by_select").val(val.value).trigger('change');
                        $("#your_customer").val(val.value);
                        // $('#contracted_by_select').val(val.value);
                        //$("#contracted_by_select").selectpicker("refresh");

                    } else if (val.name == 'job_start_date') {
                        $('#job_start_date').val(val.value);
                        $('#enter_into_agreement').val(val.value);


                    } else if (val.name == 'last_date_on_the_job') {
                        $('#last_date_of_labor_service_furnished').val(val.value);
                        $('#last_date_on_the_job').val(val.value);
                    } else {
                        var field_name = val.name;
                        var vars = val.value;
                        let result = vars.replace(/<br \/>/gi, "<p>");
                        $("#" + field_name).val(result);
                    }


                   
                    /* else 

                    {
                        var data_name = $("<br />").html(val.value).text();
                        var regex = /<br\s*[\/]?>/gi;

                        $("#legal_description").val(data_name.replace(regex, ' '));

                    }*/

//                    if (val.name == 'state') {
//                        $('select[name="project_state"]').val(val.value);
//                        $('select[name="project_state"]').selectpicker('refresh');
//
//
//                    }
//                    if (val.name == 'city') {
//                        $('select[name="project_city"]').val(val.value);
//                    }
//                    if (val.name == 'zip') {
//                        $('input[name=project_zip]').val(val.value);
//                    }


                });
            }

            if (response.parent_attachment) {
                // console.log(response.parent_attachment);
               // $('#document_table tbody').empty();
                $.each(response.parent_attachment, function (k, v) {

                    var f1 = v.db_arr[2];
                    var f2 = f1.split('.')[0];
                    var row = '<tr class="delete_parent_attachment" id="refreshdelete' + v.db_arr[4] + '">';
                    $.each((v.ajax_arr), function (i, d) {

                        $('.norecords').closest('tr').remove();
                        var display_url = '{{asset("attachment/secondary_document/parent_document")}}/' + v.db_arr[2];
                        /*if (i == 2) {
                            row += '<td><a target="_blank" href="' + display_url + '">' + d + '</a></td>';
                        } else {*/
                            row += '<td>' + d + '</td>';
                        //}
                    });
                    var display_url = '{{asset("attachment/secondary_document/parent_document")}}/' + v.db_arr[2];
                    var attachment_url = '{{url("customer/parent_document/removeattachment")}}';
                   /* row += "<td><a class='btn btn-secondary item red-tooltip remove-parent-attachment' data-toggle='modal' data-url='" + attachment_url + "'  data-file='" + v.db_arr[2] + "' data-target='#attachment-confirmation-modal' data-placement='bottom' title='Remove'><span class='icon-cross-remove-sign'></span></a></td>";*/
                if(v.db_arr[2].length>0){
                        row += "<td><a target='_blank' href='" + display_url + "' class='btn btn-secondary item red-tooltip'><i class='fa fa-eye'></i></a></td>";
                            }else{
                                row += "<td></td>";
                            }
                row += '</tr>';
                    $('#document_table tbody').append(row);

                });

            }

            $('#project_owner').val("");
            var owner = '';
            if (response.owner_recipient) {
                $.each(response.owner_recipient, function (key, val) {
             
                  owner += val.name + ' and ';

                });
                //console.log(owner.length);
                $('#project_owner').val(owner.slice(0,-4));
                $('#property_owner').val(owner.slice(0,-4));

            }
            $('input,textarea').each(function () {
                if ($(this).val().length > 0) {
                    $(this).addClass('not-empty');
                } else {
                    $(this).removeClass('not-empty');
                }
            });

        }
    });
});
//End to fetch data of related work order



$(document).ready(function () {


    $('textarea').each(function () {
        if ($(this).val().length > 0) {
            $(this).addClass('not-empty');
        } else {
            $(this).removeClass('not-empty');
        }
    });
    $('textarea').keyup(function () {
        if ($(this).val().length > 0) {
            $(this).addClass('not-empty');
        } else {
            $(this).removeClass('not-empty');
        }
    });
//address varificvation tab
    $('#createLabel').click(function () {
        $('.display-ib li').removeClass('active');
        $('#address_tab').addClass('active');
        $('.notice-tabs .tab-pane').removeClass('active');
        $('#verify_address').addClass('active');
    })
//save for later validation detroy
    $(".save_for_later").on('click', function () {
        $('#secondary_doc_project').parsley().destroy();
    });

//Start for document section
    $('.doc_field').hide();
    $('.doc_file').hide();
    $('.datepicker').datepicker({
        //  autoclose: true
    });
    $('#select_document_type').change(function () {

        var selected_doc_type = $(this).val();
        $('#doc_input_id').val("");
        if (selected_doc_type == 1) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('#doc_field').text('Bond Number');
        } else if (selected_doc_type == 2) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('#doc_field').text('NOC Book/Page');
        } else if (selected_doc_type == 3) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('#doc_field').text('Permit No');
        } else if (selected_doc_type == 5) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('#doc_field').text('Invoice No.');
        } else if (selected_doc_type == 4 || selected_doc_type == 6 ||
                selected_doc_type == 7 || selected_doc_type == 8
                || selected_doc_type == 9 || selected_doc_type == 13) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('#doc_field').text('Description');
        } else if (selected_doc_type == 10) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('#doc_field').text('Folio No.');
        } else if (!selected_doc_type) {
            $('.doc_field').hide();
            $('.doc_file').hide();
        }

    });
//End of document section
    $('#name').on('change', function () {
        var name = $('#name').val();
        var email = $('#email').val();
        var mobile = $('#mobile').val();
        var contact = $('#contact').val();
        var address = $('#address').val();
        var fax = $('#fax').val();
        var city = $('.city').val();
        var state = $('.state').val();
        $.ajax({
            data: {'name': name, },
            type: "post",
            url: "{{ url('customer/get-contacts') }}",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (res) {
                // $('').empty();
                $.each(res, function (key, value)
                {

                    $('#name').val(value.company_name);
                    $('#email').val(value.email);
                    $('#mobile').val(value.mobile);
                    $('#contact').val(value.phone);
                    $('#zip').val(value.zip);
                    $('#address').val(value.mailing_address);
                    $('#fax').val(value.fax);
                    $('#state').val(value.state_id);
                    $('#state').selectpicker('refresh');
                    $('select[name="city_id"]').val(value.city_id);
                    $('#city').selectpicker('refresh');
                    $('#country').val(value.country_id);
                    $('#recipient_attn').val(res.attn);
                    $('#country').selectpicker('refresh');
                });
                $('input').each(function () {
                    if ($(this).val().length > 0) {
                        $(this).addClass('not-empty');
                    } else {
                        $(this).removeClass('not-empty');
                    }
                });
            },
        });

    });
//Start to upload document
    var files;
    var attachment_id_arr = [];
    var result_arr = [];
    $("#submit_document").on("click", function (e) {
        if($('.update_doc').html()=='Add')  {
        e.preventDefault();
        var extension = $('#result_file').val().split('.').pop().toLowerCase();

        if ($('#result_file').val() != "" && $.inArray(extension, ['csv', 'xls', 'xlsx', 'pdf', 'doc', 'docx', 'jpg', 'jpeg']) == -1) {
            $('#errormessage').html('Please Select Valid File... ');
            $('#errormessage').css('display', 'block');
        } else {
            var file_data = $('#result_file').prop('files')[0];
            var selected_document_type = $('#select_document_type').val();
            var doc_input = $('#doc_input_id').val();
            var form_data = new FormData();

            form_data.append('file', file_data);
            form_data.append('document_type', selected_document_type);
            form_data.append('doc_value', doc_input)
            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': $('meta[name=_token]').attr('content')
                }
            });
            $.ajax({
                url: "{{url('customer/secondary-document/attachments')}}", // point to server-side PHP script
                data: form_data,
                type: 'POST',
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                processData: false,
                success: function (response) {
                    var data = jQuery.parseJSON(response);
                    var ran_no = Math.random() * (100 - 1) + 1;
                    ran_no = String(ran_no).replace('.','_');
                    data.db_arr.push('refreshdelete'+ran_no);   
                    result_arr.push(data.db_arr);
                    //console.log(data.db_arr[2]);
                    result_arr.push('|');
                       
                    $('#secondary_doc_attachment').val(result_arr);
                    var f1 = data.db_arr[2];
                    var f2 = f1.split('.')[0];
                            
                    var row = '<tr id="refreshdelete' + ran_no + '">';
                    $.each((data.ajax_arr), function (i, d) {
                        $('.norecords').closest('tr').remove();
                        var display_url = '{{asset("attachment/secondary_document")}}/' + data.db_arr[2];
                        /*if (i == 1) {
                            row += '<td><a target="_blank" href="' + display_url + '">' + d + '</a></td>';
                        } else {*/
                            row += '<td>' + d + '</td>';
                        //}
                        $('.doc_field').hide();
                        $('.doc_file').hide();
                        //$('.doc_visibility').hide();
                        $('#select_document_type').val('');
                        $('#select_document_type').selectpicker('refresh');
                        $('#result_file').val('');
                        $('#edit_document_name').val('');
                        $('#doc_input_id').val('');
                        $('#remove_file').val('0');
                        $('#preview').css('display','none');
                        $('#remove_icon').css('display','none');
                    });
                    var display_url = '{{asset("attachment/secondary_document")}}/' + data.db_arr[2];
                    var attachment_url = '{{url("customer/removeattachment")}}';
                    row += "<td><a class='btn btn-secondary item red-tooltip remove-attachment' data-toggle='modal' data-url='" + attachment_url + "'  data-file='" + data.db_arr[2] + "' data-target='#attachment-confirmation-modal' data-placement='bottom' title='Remove'><span class='icon-cross-remove-sign'></span></a><a class='btn btn-secondary item red-tooltip edit-attachment' data-file='" + data.db_arr[2] + "' data-type='" + data.db_arr[0] + "'data-title='" + data.db_arr[1] +"'data-file-original-name='" + data.db_arr[3] + "'data-file-extension='" + data.ajax_arr[4] +"'><span class='icon-pencil-edit-button'></span></a>";
                    if(data.db_arr[2].length>0){
                                    row += "<a target='_blank' href='" + display_url + "' class='btn btn-secondary item red-tooltip'><i class='fa fa-eye'></i></a>";
                                    }
                                    row += '</td></tr>';
                    $('#document_table tbody').append(row);
                }
            });
        }
    }else{
        e.preventDefault();
        var extension = $('#result_file').val().split('.').pop().toLowerCase();

        if ($('#result_file').val() != "" && $.inArray(extension, ['csv', 'xls', 'xlsx', 'pdf', 'doc', 'docx', 'jpg', 'jpeg']) == -1) {
            $('#errormessage').html('Please Select Valid File... ');
            $('#errormessage').css('display', 'block');
        } else {
            var file_data = $('#result_file').prop('files')[0];
            var selected_document_type = $('#select_document_type').val();
            var doc_input = $('#doc_input_id').val();
            var doc_id = $('#edit_doc_id').val();
                      //  var doc_visibile = $("input[name='visibility']:checked").val();
            var secondary_document_id = null;
            var remove_file = $('#remove_file').val();
            var edit_document_name = $('#edit_document_name').val();
            var edit_document_original_name = $('#edit_document_name').attr('data-file-original-name');
            var edit_document_extension = $('#edit_document_name').attr('data-file-extension');
            var row_id = $('#row_id').val();
            var form_data = new FormData();
            var result;
                        for( var i = 0, len = result_arr.length; i < len; i++ ) {
                            for( var j = 0, len1 = result_arr[i].length; j < len1; j++ ){
                                if( result_arr[i][j] === row_id ) {
                                    result = i;
                                    break;
                                    }
                            }

                        }
                       
                      result_arr.splice(result, 1);
                      
                        if(result_arr.length==1){
                            result_arr.splice(0, 1);
                        }
                      //console.log(result_arr);
                        $('#secondary_doc_attachment').val(result_arr);
                       
                        if(doc_id==''){
                           var doc_id = 0;
                        }
                        if(file_data)
                        {
                            form_data.append('file', file_data);
                        }else{
                            form_data.append('file_name',edit_document_name);
                            form_data.append('file_original_name',edit_document_original_name);
                            form_data.append('extension',edit_document_extension);
                        }
            form_data.append('document_type', selected_document_type);
            form_data.append('doc_value', doc_input)
            form_data.append('doc_id', doc_id);
            form_data.append('secondary_document_id',secondary_document_id);
            form_data.append('remove_file',remove_file);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': $('meta[name=_token]').attr('content')
                }
            });
            $.ajax({
                url: "{{url('customer/secondary-document/edit/attachments')}}", // point to server-side PHP script
                data: form_data,
                type: 'POST',
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                processData: false,
                success: function (response) {
                    var data = jQuery.parseJSON(response);
                    //result_arr.push(data.db_arr);
                    //result_arr.push('|');
                    var del_var = $('#row_id').val();
                    $("#" + del_var).remove();
                    if(data.db_arr[2] != null){
                        var f1 = data.db_arr[2];//console.log(f1);
                        var f2 = f1.split('.')[0];
                     }

                    if(data.ajax_arr[2] == null){
                        data.db_arr[2] = '';
                    }
                    var row = '<tr id="refreshdelete' + data.db_arr[4] + '">';
                    attachment_id_arr.push(data.db_arr[4]);
                    $('#work_order_attachment_id').val(attachment_id_arr);
                    $.each((data.ajax_arr), function (i, d) {
                        $('.norecords').closest('tr').remove();
                        var display_url = '{{asset("attachment/secondary_document")}}/' + data.db_arr[2];
                            if(d==null){
                                        row += '<td>' + " " + '</td>';
                                        }else{
                                        row += '<td>' + d + '</td>';
                                    }
                        //}
                        $('.doc_field').hide();
                        $('.doc_file').hide();
                         if($('#submit_document span').html()=="Update"){
                            $('#submit_document span').html('Add');
                       }
                        //$('.doc_visibility').hide();
                        $('#select_document_type').val('');
                        $('#select_document_type').selectpicker('refresh');
                        $('#result_file').val('');
                        $('#edit_document_name').val('');
                        $('#doc_input_id').val('');
                        $('#remove_file').val('0');
                        $('#preview').css('display','none');
                        $('#remove_icon').css('display','none');
                    });//console.log(data.db_arr,'shamal');
                    var display_url = '{{asset("attachment/secondary_document")}}/' + data.db_arr[2];
                    var attachment_url = '{{url("customer/edit/removeattachment")}}';
                    row += "<td><a class='btn btn-secondary item red-tooltip remove-attachment' data-toggle='modal' data-url='" + attachment_url + "' data-id='" + data.db_arr[4]+"' data-file='" + data.db_arr[2] + "' data-target='#attachment-confirmation-modal' data-placement='bottom' title='Remove'><span class='icon-cross-remove-sign'></span></a><a class='btn btn-secondary item red-tooltip edit-attachment' data-file='" + data.db_arr[2] + "' data-type='" + data.db_arr[0] + "'data-title='" + data.db_arr[1] +"'data-id='" + data.db_arr[4]+ "'data-file-original-name='" + data.db_arr[3] + "'data-file-extension='" + data.db_arr[6] +"'><span class='icon-pencil-edit-button'></span></a>";
                    if(data.db_arr[2].length>0){
                                    row += "<a target='_blank' href='" + display_url + "' class='btn btn-secondary item red-tooltip'><i class='fa fa-eye'></i></a>";
                                }
                                row += '</td></tr>';
                    $('#document_table tbody').append(row);
                }
            });
        }
    }
    });
//End of upload document

//Start to fetch work order number of customers
    //$('#work_order_no').find('option').remove().end().append('<option value="">Select</option>');

    $('#work_order_no').select2({
        minimumInputLength: 1,
        ajax: {
            url : "{{ url('customer/secondary-document/autocomplete_V1?work_order_id='.$secondary_document_id) }}",
            dataType: 'json',
            
            delay: 250,
            type: "get",
            processResults: function (data) {
            return {
                results:  $.map(data, function (item) {
                    return {
                        text: item,
                        id: item
                    }
                })
            };
            },
            cache: true
        }
    });
//End to fetch work order number of customers

//Add recipient contact dynamic
    $('#add_contact').click(function () {
        var all_val = 1;
        if ($('#name').val() != "") {
            $('input[name=contact_name]').val($('#name').val());
            // $('input[name=contact_name]').addClass('not-empty');
            $('input[name=contact_no]').val($('#contact').val());
            $('input[name=contact_email]').val($('#email').val());
            $('input[name=contact_address]').val($('#address').val());
            $('input[name=contact_zip]').val($('#zip').val());
            $("#contact_state_id").val($('#state').val());
            $("#contact_state_id").selectpicker('refresh');
            $("#contact_city_id").val($('#city').val());
            $('#autocomplete_city_id').val($('#recipient_city_id').val());
            $("#contact_city_id").selectpicker('refresh');
            $('input').addClass('not-empty');
            all_val = 0;
        } else {
            $('input[name=contact_name]').val('');
        }
        if (all_val) {
            $('input[name=contact_name]').val('');
            $('input[name=contact_no]').val('');
            $('input[name=contact_email]').val('');
            $('input[name=contact_address]').val('');
            $('input[name=contact_zip]').val('');
            $("#contact_state_id").val('');
            $("#contact_state_id").selectpicker('refresh');
            $("#contact_city_id").val('');
            $("#contact_city_id").selectpicker('refresh');
            $("#contact_country").val('');
            $("#contact_country").selectpicker('refresh');
        }
        $('#success-msg').css('display', 'none');

//            $('#contactModal').modal('show');
    });

    $('body').on('click', '#submit_contact', function () {
        var recipientForm = $('#add_recipient_contact');
        //    var formData = recipientForm.serialize();

        $('#name-error').html("");
        $('#contact-error').html("");
        $('#email-error').html("");
        $('#address-error').html("");
        $('#city-error').html("");
        $('#state-error').html("");
        $('#zip-error').html("");
        $('#country-error').html("");
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name=_token]').attr('content')
            }
        });
        $.ajax({
            url: '{{url("customer/contact-recipient/store")}}',
            type: 'POST',
            data: {
                doc_id: $('#doc_id_recipient').val(),
                contact_name: $('#contact_name').val(),
                contact_no: $('#contact_no').val(),
                contact_email: $('#contact_email').val(),
                contact_address: $('#contact_address').val(),
                contact_state_id: $('#contact_state_id').val(),
                contact_zip: $('#contact_zip').val(),
                contact_city_id: $('#contact_city_id').val(),
                contact_country: $('#contact_country').val(),
                attn: $('#attn').val()
            },
            success: function (data) {

                if (data.errors) {
                    if (data.errors.contact_name) {
                        $('#name-error').html(data.errors.contact_name[0]);
                    }
                    if (data.errors.contact_address) {
                        $('#address-error').html(data.errors.contact_address[0]);
                    }
                    if (data.errors.contact_city_id) {
                        $('#city-error').html(data.errors.contact_city_id[0]);
                    }
                    if (data.errors.contact_state_id) {
                        $('#state-error').html(data.errors.contact_state_id[0]);
                    }
                    if (data.errors.contact_country) {
                        $('#country-error').html(data.errors.contact_country[0]);
                    }
                    if (data.errors.contact_zip) {
                        $('#zip-error').html(data.errors.contact_zip[0]);
                    }

                }
                if (data.success) {
                    // console.log(data.contact_data)
                    $('#success-msg').css('display', 'block');
                    $('#success-msg').html(data.success);

                    // console.log($('#doc_id_recipient').val());
                    $.ajax({
                        data: {"doc_id": $('#doc_id_recipient').val()},
                        type: 'POST',
                        url: "{{url('customer/getAllContacts')}}", // point to server-side PHP script
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function (response) {
                            $("#name").empty("");
                            $("#name").selectpicker("refresh");
                            $.each(response.contacts, function (i, obj)
                            {
                                var div_data = "<option value=" + obj.company_name + ">" + obj.company_name + "</option>";

                                $(div_data).appendTo('#name');
                            });
                            $("#name").selectpicker("refresh");
                        }
                    });
                    $('#name').val(data.contact_data.company_name);
                    $('#name').selectpicker('refresh');
                    $('#contact').val(data.contact_data.phone);
                    $('#email').val(data.contact_data.email);
                    $('#address').val(data.contact_data.mailing_address);
                    $('#city').val(data.contact_data.city_id);
                    $('#city').selectpicker('refresh');
                    $('#state').val(data.contact_data.state_id);
                    $('#state').selectpicker('refresh');
                    $('#zip').val(data.contact_data.zip);
                    $('#country').val(data.contact_data.country_id);
                    $('#country').selectpicker('refresh');
                }
                $('input').each(function () {
                    if ($(this).val().length > 0) {
                        $(this).addClass('not-empty');
                    } else {
                        $(this).removeClass('not-empty');
                    }
                });
            },
        });
    });

    /**************Autocomplete County********************/
    /*county */
    src_county = "{{ url('customer/city/autocompletecountyname') }}";

    $("#parent_country").autocomplete({

        source: function (request, response) {
            $.ajax({
                url: src_county,
                dataType: "json",
                data: {
                    term: request.term
                },
                beforeSend: function () {
                    $('#countyloader').show();
                },
                complete: function () {
                    $('#countyloader').hide();
                },
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            value: item.value,
                            id: item.id     // EDIT
                        }
                    }));

                },

            });
        },
        select: function (event, ui) {

            $("#county_id").val(ui.item.id);
            $("#parent_country").val(ui.item.value);
        },
        change: function (event, ui) {
            if (ui.item == null) {
                $(this).val("");
                $(this).focusout();
                $(this).focus();
            }
        }
    });

    src = "{{ url('customer/city/autocompletecityname') }}";

    $("#city").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: src,
                dataType: "json",
                data: {
                    term: request.term
                },
                beforeSend: function () {
                    $('#recipientloader').show();
                },
                complete: function () {
                    $('#recipientloader').hide();
                },
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            value: item.value,
                            id: item.id     // EDIT
                        }
                    }));

                },

            });
        },
        select: function (event, ui) {

            $("#recipient_city_id").val(ui.item.id);
            $("#city").val(ui.item.value);
        },
        change: function (event, ui) {
            if (ui.item == null) {
                $(this).val("");
                $(this).focusout();
                $(this).focus();
            }
        }
    });


    $("#contact_city_id").autocomplete({

        source: function (request, response) {
            $.ajax({
                url: src,
                dataType: "json",
                data: {
                    term: request.term
                },
                beforeSend: function () {
                    $('#recipientpopuploader').show();
                },
                complete: function () {
                    $('#recipientpopuploader').hide();
                },
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            value: item.value,
                            id: item.id     // EDIT
                        }
                    }));

                },

            });
        },
        select: function (event, ui) {

            $("#autocomplete_city_id").val(ui.item.id);
            $("#contact_city_id").val(ui.item.value);
        },
        change: function (event, ui) {
            if (ui.item == null) {
                $(this).val("");
                $(this).focusout();
                $(this).focus();
            }
        }
    });
    /*************Autocomplete County*****************/
});

$('#choose_address').click(function () {
    var selected_addr = '';
    if ($('input:radio[name="select_addr"]:checked').length > 0) {
        selected_addr = $('input:radio[name="select_addr"]:checked').val();
        selected_addr = selected_addr.split('**');
        console.log(selected_addr);
        var city = selected_addr[1];
        //var city = 'Boca Raton';
        var state = selected_addr[2];
        //var state = 'Florida';
        $('#address').val(selected_addr[0]);
        $('#zip').val(selected_addr[3]);
        $("#state option").each(function () {
            if ($(this).text() == state) {
                $(this).attr('selected', 'selected');
            }
        });
        $("#state").selectpicker("refresh");
        $("#city option").each(function () {
            if ($(this).text() == city) {
                $(this).attr('selected', 'selected');
            }
        });
        $("#city").selectpicker("refresh");
        $('#address_verification_modal').modal('hide');
    } else {
        alert("Please select one address");
    }
})

$(document).on('click', '.remove-attachment', function () {

    var url = $(this).attr('data-url');
    var file = $(this).attr('data-file');
    var id = $(this).attr('data-id');
    var row_id = $(this).closest('tr').attr('id');
    $(".remove-attachment-model").attr("action", url);
    $("input[name='file']").remove();
    $("input[name='id']").remove();
    $("input[name='row_id']").remove();
    $('body').find('.remove-attachment-model').append('<input name="file"  type="hidden" value="' + file + '">');
    $('body').find('.remove-attachment-model').append('<input name="_method" type="hidden" value="POST">');
    $('body').find('.remove-attachment-model').append('<input name="row_id" type="hidden" value="' + row_id + '">');
    $('body').find('.remove-attachment-model').append('<input name="id" type="hidden" value="' + id + '">');
});
function openTab(th)
            {
                window.open(th.name,'_blank');
            }
        $('#remove_icon').click(function(){
        $('#preview').css('display','none');
        $('#remove_icon').css('display','none');
        $('#remove_file').val('1');
        $('#result_file').val('');

    })
     $('#result_file').change(function(e){
            var fileName = e.target.files[0].name;
            doc_icon = fileName.split('.')[1];
             if(doc_icon == 'pdf'){
             $('#preview').html('<i class="fa fa-file-pdf-o" style="font-size:36px"></i>');
                }else if(doc_icon == 'doc' || doc_icon == 'docx'){
                    $('#preview').html('<i class="fa fa-file-word-o" style="font-size:36px"></i>');
                }else if(doc_icon == 'xls' || doc_icon == 'xlsx'){
                    $('#preview').html('<i class="fa fa-file-excel-o" style="font-size:36px"></i>');
                }else if(doc_icon == 'jpg' || doc_icon == 'jpegs'){
                    $('#preview').html('<i class="fa fa-file-image-o" style="font-size:36px"></i>');
                }else{
                     $('#preview').html('<i class="fa fa-file" style="font-size:36px"></i>');
                }
           // $('#preview').html(fileName);
            $('#preview').css('display','block');
            $('#remove_icon').css('display','block');
            //alert('The file "' + fileName +  '" has been selected.');
        });
     $(document).on('click', '.edit-attachment', function () {
        var row_id = $(this).closest('tr').attr('id');
       // $('#submit_document').removeClass('old_class').addClass('new_class');
        var type = $(this).attr('data-type');
        var file = $(this).attr('data-file');
        var title = $(this).attr('data-title');
        var id = $(this).attr('data-id');
        $('#edit_document_name').val(file);
        $('#edit_document_name').attr('data-file-original-name',$(this).attr('data-file-original-name'));
        $('#edit_document_name').attr('data-file-extension',$(this).attr('data-file-extension'));
        $('#doc_input_id').val(title);
        $('#edit_doc_id').val(id);
        $('#row_id').val(row_id);
        $('#preview').css('display','block');
        $('#remove_icon').css('display','block');

        //console.log(file);
        var APP_URL = {!! json_encode(url('/')) !!}
       // console.log(APP_URL);
        $("#open_doc").attr('name',APP_URL+'/attachment/work_order_document/'+file);
        doc_icon = file.split('.')[1];
        if(doc_icon == 'pdf'){
             $('#preview').html('<i class="fa fa-file-pdf-o" style="font-size:36px"></i>');
        }else if(doc_icon == 'doc' || doc_icon == 'docx'){
            $('#preview').html('<i class="fa fa-file-word-o" style="font-size:36px"></i>');
        }else if(doc_icon == 'xls' || doc_icon == 'xlsx'){
            $('#preview').html('<i class="fa fa-file-excel-o" style="font-size:36px"></i>');
        }else if(doc_icon == 'jpg' || doc_icon == 'jpegs'){
            $('#preview').html('<i class="fa fa-file-image-o" style="font-size:36px"></i>');
        }else if (doc_icon == 'csv'){
            $('#preview').html('<i class="fa fa-file" style="font-size:36px"></i>');
        }else{
            $('#preview').html('');
            $('#preview').css('display','none');
            $('#remove_icon').css('display','none');
        }
       
        //$('#edit_result_file').val(APP_URL+'/attachment/work_order_document/'+file);
        var id1 = file.split('.')[0];

        var id = $(this).attr('data-id');
      
        $('.doc_field').show();
        $('.doc_file').show();
       // $('.doc_visibility').show();
        $('.update_doc').text('Update');
        $('#select_document_type').val(type);
        $('#select_document_type').selectpicker('refresh');
       /* $('#errormessage').text(file);
        $('#errormessage').css('display', 'block');*/
       var selected_doc_type = type;
      //  $('#work_order_doc_input').val("");
        if (selected_doc_type == 1) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('#doc_field').text('Bond Number');
        } else if (selected_doc_type == 2) {
           $('.doc_field').show();
            $('.doc_file').show();
            $('#doc_field').text('NOC Book/Page');
        } else if (selected_doc_type == 3) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('#doc_field').text('Permit No');
        } else if (selected_doc_type == 5) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('#doc_field').text('Invoice No.');
        } else if (selected_doc_type == 4 || selected_doc_type == 6 ||
                selected_doc_type == 7 || selected_doc_type == 8
                || selected_doc_type == 9) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('#doc_field').text('Description');
        } else if (selected_doc_type == 10) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('#doc_field').text('Folio No.');
        } else if (!selected_doc_type) {
            $('.doc_field').hide();
            $('.doc_file').hide();
        }
           /* if($('#submit_document span').html()=="Update"){
                                           // var file = data.db_arr[2];
                                            var id = file.split('.')[0];
                                            remove_arr.push(file);
                                            $('#remove_doc').val(remove_arr);
                                            var del_var = "refreshdelete" + id;
                         console.log(del_var);
                                            $("#" + del_var).remove();
                                        }*/
        /*$(".remove-attachment-model").attr("action", url);
        $("input[name='file']").remove();
        $('body').find('.remove-attachment-model').append('<input name="file" type="hidden" value="' + file + '">');
        $('body').find('.remove-attachment-model').append('<input name="id" type="hidden" value="' + id + '">');
        $('body').find('.remove-attachment-model').append('<input name="_method" type="hidden" value="POST">');*/
        /*$.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name=_token]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url: '{{url("customer/edit/work-ordwer/removeattachment")}}',
            data: {'file':file,'id':id},
            success: function (response) {
                if (response.success) {
                    //$('#attachment-confirmation-modal').modal('hide');
                    remove_arr.push(file);
                    $('#remove_doc').val(remove_arr);
                    var del_var = "refreshdelete" + id1;

                    $("#" + del_var).remove();
                }
            }
        });*/
    });
//For account manager login parent work according to customer
$(document).on('click', '.remove-parent-attachment', function () {
    var file = $(this).attr('data-file');
    var url = $(this).attr('data-url');
    $(".remove-attachment-model").attr("action", url);
    $("input[name='file']").remove();
    $('body').find('.remove-attachment-model').append('<input name="file" type="hidden" value="' + file + '">');
    $('body').find('.remove-attachment-model').append('<input name="_method" type="hidden" value="POST">');
});
$('#customer_id').on('change', function () {

    $.ajax({
        type: "post",
        url: "{{url('customer/account-manager/secondary-document/autocomplete')}}",
        data: {customer_id: $('#customer_id').val()},
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            $("#work_order_no").empty();
            $("#work_order_no").selectpicker("refresh");
            //  console.log(response);
            $('#work_order_no').find('option').remove().end().append('<option value="">Select</option>');

            $.each(response, function (e, val) {

                $("#work_order_no").append("<option value='" + val.id + "'>" + val.id + "</option>");
                $("#work_order_no").selectpicker("refresh");
            });

        }
    });
});

$('#customer_id').on('change', function () {

    $.ajax({
        type: "post",
        url: "{{url('customer/account-manager/secondary-document/authoriseagent')}}",
        data: {customer_id: $('#customer_id').val()},
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            $("#officers_directors").empty();
            $("#officers_directors").selectpicker("refresh");

            $("#officers_directors_title").empty();
            $("#officers_directors_title").selectpicker("refresh");
            //  console.log(response);
            $('#officers_directors').find('option').remove().end().append('<option value="">Select</option>');
            $('#officers_directors_title').find('option').remove().end().append('<option value="">Select</option>');
            $.each(response, function (e, val) {
                $.each(val, function (e1, v1) {
                    var total_name = v1.first_name + ' ' + v1.last_name;

                    $("#officers_directors").append("<option value='" + total_name + "'>" + total_name + "</option>");
                    $("#officers_directors").selectpicker("refresh");
                    $("#officers_directors_title").append("<option value='" + v1.title + "'>" + v1.title + "</option>");
                    $("#officers_directors_title").selectpicker("refresh");
                });
            });
        }
    });
});
var remove_arr = [];
$('.remove-attachment-model').submit(function (event) {
    event.preventDefault();
    var file = $('input[name=file]').val();
    var id = file.split('.')[0];
    var row_id = $('input[name=row_id]').val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name=_token]').attr('content')
        }
    });
    var submit_url = $(this).attr("action");
    $.ajax({
        type: 'POST',
        url: submit_url,
        data: $('#remove-attachment-model').serialize(),
        success: function (response) {

            if (response.success) {
                $('#attachment-confirmation-modal').modal('hide');
                remove_arr.push(row_id);
                $('#remove_doc').val(remove_arr);

                var del_var = "refreshdelete" + id;

                $("#" + row_id).remove();
            }
        }
    });
});
                </script>
                @stop