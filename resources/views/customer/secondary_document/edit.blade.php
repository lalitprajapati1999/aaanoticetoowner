<style>
 .bootstrap-select :focus{
    outline: 0;
    border-bottom : 1px solid #032c61 !important;
 }
  
 
 .select2class :focus{
    outline: 0;
    height: 47px !important;
    border-bottom : 1px solid #032c61 !important;
}
.min-height{
    min-height: 98px!important;
  }
  .select2-container--default .select2-selection--single, .select2-selection .select2-selection--single {
     border-radius: 0;
     padding: 6px 12px;
     height: 34px;
     border: none !important;; 
     border-bottom: 1px solid #aaa !important;
 }
 
 .select2-container--default .select2-selection--single {
     background: none !important;
     border: 1px solid #aaa;
     border-radius: 4px;
 }
</style>
@extends('adminlte::page')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<div  class="tab-pane active" id="workorder" role="tabpanel">
    <div class="tab-content">
        <section class="work-order address-book-add-readonly view-order">
            <div class="dashboard-wrapper">
                @if (Session::get('success'))
                <div class="alert alert-success msg-success">
                    <?php echo Session::get('success'); ?>
                </div>
                @endif
                <div class="dashboard-heading">
                  <h1><span>REQUEST FOR {{strtoupper($notice['name'])}}</span></h1>
               </div>

                <div class="dashboard-inner-body">
                    <div class="notice-tabs">
                        <div class="full-width">
                            <ul class="nav nav-tabs display-ib pull-left" role="tablist">
                                @if($tab=="project")
                                <li role="presentation" id="project_tab"  class="active"><a href="#project" aria-controls="project" role="tab" data-toggle="tab">Project</a></li>
                                <!--  <li role="presentation"><a href="#recepients" aria-controls="recepients" role="tab" data-toggle="tab">Recipients</a></li>-->
                                @else 
                                <li role="presentation"><a href="#project" aria-controls="project" role="tab" data-toggle="tab">Project</a></li>
                                <!--<li role="presentation" class="active"><a href="#recepients" aria-controls="recepients" role="tab" data-toggle="tab">Recipients</a></li>-->

                                @endif
                                <!--                                  <li role="presentation" id="address_tab" class="{{ ($tab == 'address_verify')?'active':''}}"><a href="#verify_address" aria-controls="verify_address" role="tab" data-toggle="tab" >Address Verification</a></li>-->
                            </ul>

                            <!---------------new html starts date:9 oct 2018------------------>
                            <div class="correction-div">
                                <div class="display-ib pull-right">
                                    <a href="#" class="btn btn-default corr-btn">Corrections</a>
                                    <a href="#" class="btn btn-default notice-btn">Notes</a>
                                    <div class="corr-block notice-block mCustomScrollbar" data-mcs-theme="light">
                                        <div class="corr-block-wrapper" >
                                            <h4>Corrections</h4>
                                            <form id="edit_secondary_document_correction">
                                                <div class="correction-success alert no-margin alert-success" style="display: none"></div>
                                                <div class="correction-error no-margin alert alert-danger" style="display:none"></div>
                                                <div class="row">
                                                    <input type="text" name="secondary_doc_id" value="{{$secondary_doc_id}}" id="edit_secondary_doc_id" hidden="">                                           
                                                    <input type="hidden" name="customer_id" value="{{getSecondaryDocumentCustomerId($secondary_doc_id)}}" id="customer_id"/>
                                                    <div class="col-md-12 col-sm-12">
                                                        <div class="col-md-12 col-sm-12">
                                                            <div class="input-wrapper full-width">
                                                                <div class="styled-input">
                                                                    <input class="form-control"  type="text" value="{{old('correction')}}" name="correction" data-parlsey-required="true" data-parsley-required-message="Correction is required">
                                                                    <label>Correction<span class="mandatory-field">*</span></label>
                                                                    <span></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-sm-12">
                                                            <div class="select-opt">
                                                                <label class="col-md-12 control-label" for="textinput"  name="Email*"></label> 
                                                                <select class="selectpicker" name="email" id="correction_email">
                                                                    <option value="">Select</option>
                                                                    @foreach($note_emails as $email)
                                                                    <option value="{{$email}}" @if (old('email') == $email) selected="selected" @endif > {{$email}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div id="correctionloader" style="display: none">
                                                            <img width="150px" src="{{asset('images/loader.gif')}}"/>
                                                        </div>
                                                        <div class="form-bottom-btn">
                                                            <a href="#" class="btn btn-primary custom-btn customc-btn submitCorrection"><span>Submit</span></a>
                                                        </div>
                                                    </div>
                                            </form>
                                            <ul class="correction_div">
                                                @if(isset($corrections) && !empty($corrections))
                                                @foreach($corrections AS $each_correction)
                                                <li>
                                                    {{$each_correction->correction}}<br>
                                                    Added By:{{$each_correction->name}}<br>
                                                    Created at:{{date('m-d-Y',strtotime($each_correction->created_at))}}
                                                </li>
                                                @endforeach
                                                @endif
                                            </ul>
                                            <a class="closebtn">&times;</a>
                                        </div>
                                    </div>
                                    <div class="notes-block notice-block mCustomScrollbar" data-mcs-theme="light">
                                        <div class="notes-block-wrapper">
                                            <form id="edit_work_order_note">
                                                <div class="note-success no-margin alert alert-success" style="display: none"></div>
                                                <div class="note-error no-margin alert alert-danger" style="display:none"></div>
                                                <h4>Notes</h4>
                                                <p>Order Number: {{$secondary_doc_id}}</p>
                                                <div class="row">
                                                    @if(!Auth::user()->hasRole('customer'))
                                                    <div class="col-md-12">
                                                        <ul class="nav nav-tabs radio-tabs button-holder full-width public-tabs" role="tablist">
                                                            <li class="active col-md-3"><a href="#note-public"><input type="radio" id="radio-2-4" name="visibility" class="regular-radio" value="0" checked /><label for="radio-2-4">Public</label></a></li>
                                                            <li class="col-md-3"><a href="#note-private"><input type="radio" id="radio-2-5" name="visibility" class="regular-radio" value="1"/><label for="radio-2-5">Private</label></a></li>
                                                        </ul>
                                                    </div>
                                                    @endif
                                                    <input type="text" name="secondary_doc_id" value="{{$secondary_doc_id}}" id="edit_secondary_doc_id" hidden="">                                            <div class="col-md-12 col-sm-12">
                                                        <div class="input-wrapper full-width">
                                                            <div class="styled-input">
                                                                <input class="form-control"  type="text" value="{{old('note')}}" name="note" data-parlsey-required="true" data-parsley-required-message="Note is required">
                                                                <label>Note<span class="mandatory-field">*</span></label>
                                                                <span></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 col-sm-12">
                                                        <div class="select-opt">
                                                            <label class="col-md-12 control-label" for="textinput"  name="Email*"></label> 
                                                            <select class="selectpicker" name="email" id="note_email">
                                                                <option value="">Select</option>
                                                                @foreach($note_emails as $email)
                                                                <option value="{{$email}}" @if (old('email') == $email) selected="selected" @endif > {{$email}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div id="noteloader" style="display: none">
                                                        <img width="150px" src="{{asset('images/loader.gif')}}"/>
                                                    </div>
                                                    <div class="form-bottom-btn">
                                                        <a href="#" class="btn btn-primary custom-btn customc-btn submitNote"><span>Submit</span></a>
                                                    </div>
                                                </div>
                                            </form>
                                            <ul class="notes_div">
                                                @if(isset($notes) && !empty($notes))
                                                @foreach($notes AS $each_note)
                                                <li>
                                                    {{$each_note->note}}<br>
                                                    Added By:{{$each_note->name}}<br>
                                                    Created at:{{date('m-d-Y',strtotime($each_note->created_at))}}
                                                </li>
                                                @endforeach
                                                @endif
                                            </ul>
                                            <a class="closebtn1">&times;</a>
                                        </div>
                                    </div>
                                </div>
                                <!--                                <div id="loaderDiv">
                                                                <img id="loading-image" src="{{ asset('loader.gif')}}" style="display:none;"/>
                                                            </div>-->
                            </div>
                            <!---------------new html ends date:9 oct 2018------------------>
                        </div>
                        <div class="tab-content">
                            @if($tab=="project")
                            <div role="tabpanel" class="tab-pane active" id="project">
                                @else
                                <div role="tabpanel" class="tab-pane" id="project">
                                    @endif
                                    <div class="project-wrapper">

                                        <!--action="{{url('customer/partial-document')}}"-->
                                        <form id="edit_secondary_doc_project" action="{{url('customer/secondary-document/edit/'.$secondary_doc_id)}}" enctype="multipart/form-data" method="post" date-parsley-validate="">
                                            <div class="dash-subform mt-0">
                                            <div class="row">
                                                <div class="col-md-7"> </div> 
                                                <div class="col-md-5 save_btn pull-right text-right">
                                                    <a href="{{url('customer/secondary-document')}}" class="btn btn-primary custom-btn customb-btn" style="margin-left: 5%;"><span>Back</span></a>
                                                    {{--<a href="{{url('home')}}" class="btn btn-primary custom-btn customb-btn"><span>Cancel</span></a>--}}
                                                    <button type="submit" name="submit" value="save_for_later" class="btn btn-primary custom-btn customc-btn save_for_later" id="save_for_later"><span>Save For Later</span></button>
                                                    <button type="button" name="submit" value="continue" class="btn btn-primary custom-btn customc-btn generate_pdf"><span>Generate PDF</span></button>
                                                   <!--  <a  href="{{url('customer/secondary-document/printSecondaryDocument/'.$secondary_doc_id.'/pdf')}}" name="submit" class="btn btn-primary custom-btn customc-btn" value="submit" target="_blank"><span>Generate PDF</span></a> -->
                                                </div>
                                            </div>
                                            <!--div class="dash-subform mt-0"-->
                                                <input type="hidden" name="edit_secondary_doc_id" value="{{$secondary_doc_id}}"/>
                                                <input type="text" name="notice_id" value="{{$notice['id']}}" hidden="">
                                                <input type="text" name="edit_flag" value="{{$edit_flag}}" hidden="">
                                                <meta name="_token" content="{{ csrf_token() }}" />
                                                {!! csrf_field() !!}
                                                <div class="row">
                                                    <!--                                                    <div class="col-md-6 col-sm-6">
                                                                                                            <div class="date-box full-width">
                                                                                                                <div class="input-wrapper full-width">
                                                                                                                    <div class="styled-input">
                                                                                                                        <label>Date</label>
                                                                                                                        <input class="form-control" name="date" type="text" readonly="" value="{{($secondary_document->date)?($secondary_document->date):date('Y-m-d')}}">
                                                                                                                        <span></span>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>-->
                                             @if(isset($notice_fields))
                                             @foreach($notice_fields as $fields)
                                                 @if($fields->type==6)
                                                  <div class="col-md-6 col-sm-6">
                                                      <div class="date-box full-width">
                                                         <div class="input-wrapper full-width">
                                                            <div class="styled-input">                            
                                                                <?php $label = str_replace("_", " ", $fields->name); ?>
                                                            <input class="form-control datepicker"  type="text" name="{{$fields->notice_field_id.' '.$fields->name}}" id="{{$fields->name}}" data-parsley-required="true" data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout" value="{{$fields->value}}">
                                                            <label>{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>
                                                            <span></span>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                    @elseif($fields->type==1 && $fields->name == 'date_request' )
                                                      <div class="col-md-6 col-sm-6">
                                                         <div class="date-box full-width">
                                                            <div class="input-wrapper full-width">
                                                               <div class="styled-input">
                                                                  <input class="form-control"  type="text" name="{{$fields->notice_field_id.' '.$fields->name}}" value="{{date('m-d-Y')}}" data-parsley-required="false" data-parsley-required-message="{{ucfirst($fields->name)}} is required" data-parsley-trigger="change focusout" readonly="" >
                                                                  <label class="full-width">Date</label>
                                                                  <span></span>
                                                                  </div>
                                                               </div>
                                                            </div>
                                                      </div>
                                                     @elseif($fields->type==1 && $fields->name == 'contracted_by')
                                                     <div class="col-md-6 col-sm-6 clear-b">
                                                         <div class="select-opt select2-container select2class">
                                                            <input type="hidden" value="1" name="contracted_by_exist">
                                                            <?php $label = str_replace("_", " ", $fields->name); ?>
                                                            <label class="col-md-12 control-label" for="textinput">{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>
                                                            <select class="js-example-tags form-control" name="{{$fields->notice_field_id.' '.$fields->name}}" id="contracted_by_select" data-parsley-required="true" data-parsley-required-message="{{ucfirst(trans($label))}} is required"> 
                                                            <option value="">Select</option>
                                                            @if(isset($names) && !empty($names))
                                                            @foreach($names as $each_name)
                                                            <option value="{{$each_name->company_name}}"  @if ($fields->value == $each_name->company_name) selected="selected" @endif>{{$each_name->company_name}}</option>
                                                             @endforeach
                                                            @endif
                                                            </select>
                                                         <span></span>
                                                      </div>
                                                   </div>
                                                    @elseif($fields->type==1 && $fields->name == 'parent_work_order' )
                                                    <?php $label = str_replace("_", " ", $fields->name); ?>
                                                   <div class="col-md-6 col-sm-6">
                                                         <div class="select-opt">
                                                            <?php $label = str_replace("_", " ", $fields->name); ?>
                                                            <label class="col-md-12 control-label" for="textinput">{{ucfirst(trans($label))}}</label>
                                                            <select class="form-control" name="{{$fields->notice_field_id.' '.$fields->name}}" data-show-subtext="true" data-live-search="true" id="edit_work_order_no" >
                                                            <?php if(!empty($fields->value)){ ?>
                                                             <option value="{{$fields->value}}" selected="selected">{{$fields->value}}</option>   
                                                        <?php }else{ ?>
                                                            <option value=" ">Select</option>
                                                        <?php } ?>      
                                                         </select>
                                          </div>

                                          <input type="text"  id="parent_work_order_no" name="work_order_no" hidden="" value="{{$secondary_document->work_order_no}}">
                                          </div>
                                        

                                       @elseif($fields->type==1 && $fields->name == 'project_name' )
                                          <?php $label = str_replace("_", " ", $fields->name); ?>
                                       <div class="col-md-6 col-sm-6">
                                          <div class="input-wrapper full-width">
                                             <div class="styled-input">
                                                <?php $label = str_replace("_", " ", $fields->name); ?>
                                                <input class="form-control" type="text" name="{{$fields->notice_field_id.' '.$fields->name}}" id="{{$fields->name}}">
                                                <label>{{ucfirst(trans($label))}}</label>
                                                <span></span>
                                                </div>
                                             </div>
                                       </div>
                                       @elseif($fields->type==1 && $fields->name == 'project_owner' )
                                          <?php $label = str_replace("_", " ", $fields->name); ?>
                                       <div class="col-md-6 col-sm-6">
                                          <div class="input-wrapper full-width">
                                             <div class="styled-input">
                                                <?php $label = str_replace("_", " ", $fields->name); ?>
                                                <input class="form-control" type="text" name="{{$fields->notice_field_id.' '.$fields->name}}" id="{{$fields->name}}" value="{{$fields->value}}">
                                                <label>{{ucfirst(trans($label))}}</label>
                                                <span></span>
                                                </div>
                                             </div>
                                       </div>
                                       @elseif($fields->type==1 && $fields->name == 'county')

                                          <?php $label = str_replace("_", " ", $fields->name); ?>
                                             <div class="col-md-6 col-sm-6">
                                               <div class="input-wrapper full-width">
                                                            <div class="styled-input">
                                                                <input class="form-control"  type="text" name="parent_country" id="parent_country" data-parsley-required="true" data-parsley-required-message="Select County from the list" data-parsley-trigger="change focusout" value="{{getCounty($fields->value)}}"> 
                                                                <input type="hidden" name="{{$fields->notice_field_id.' '.$fields->name}}"  id="county_id" value="{{$fields->value}}"/>
                                                                <label class="col-md-12 control-label" for="textinput">{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label> 
                                                                <span></span>     
                                                            </div>
                                                            <div id="recipientcountyloader" style="display: none">
                                                                <img width="150px" src="{{asset('images/loader.gif')}}"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @elseif($fields->type==1 && $fields->name == 'comment')
                                                    <?php $label = str_replace("_", " ", $fields->name); ?>
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="input-wrapper full-width">
                                                            <div class="styled-input">
                                                                <?php $label = str_replace("_", " ", $fields->name); ?>
                                                                <input class="form-control"  type="text" name="{{$fields->notice_field_id.' '.$fields->name}}" id="{{$fields->name}}"  data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout" value="{{$fields->value}}">
                                                                <label>{{ucfirst(trans($label))}}</span></label>
                                                                <span></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @elseif($fields->type==1)
                                                    <?php $label = str_replace("_", " ", $fields->name); ?>
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="input-wrapper full-width">
                                                            <div class="styled-input">
                                                                <?php $label = str_replace("_", " ", $fields->name); ?>
                                                                <input class="form-control"  type="text" name="{{$fields->notice_field_id.' '.$fields->name}}" id="{{$fields->name}}" data-parsley-required="true" data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout" value="{{$fields->value}}">
                                                                <label>{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>
                                                                <span></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                   @elseif($fields->type==2 && $fields->name=="project_address")

                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="input-wrapper full-width">
                                                           <label class="col-md-12 control-label">Project Address<span class="mandatory-field">*</span></label>
                                                            <div class="styled-input">
                                                                <?php $label = str_replace("_", " ", $fields->name); 
                                                                   $project_address = preg_replace("/<br \/>/", "<p>", $fields->value);
                                                                 ?>
                                                                <textarea id="{{$fields->name}}" class="form-control projects_address val" name="{{$fields->notice_field_id.' '.$fields->name}}" data-parsley-required="true" data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout"  rows="5">{{$project_address}}</textarea>
                                                              <span></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                 @elseif($fields->type==2)
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="input-wrapper full-width">
                                                            <div class="styled-input">
                                                                <?php $label = str_replace("_", " ", $fields->name); ?>
                                                                <textarea id="{{$fields->name}}" class="form-control" name="{{$fields->notice_field_id.' '.$fields->name}}"  data-parsley-required="true" data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout">{{$fields->value}}</textarea>
                                                                <label>{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>
                                                                <span></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                 @elseif($fields->type==3 && $fields->name =='authorise_agent')
                                            
                                                    <div class="col-md-6 col-sm-6 min-height">
                                                        <div class="select-opt"@if(strtoupper($notice['name']) =="PARTIAL UNCONDITIONAL BOND WAIVER
")
                                                        style="position: absolute;"
                                                    @endif >
                                                            <?php $label = str_replace("_", " ", $fields->name); ?>
                                                            <label class="col-md-12 control-label" for="textinput"  >{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>  
                                                            <select name="{{$fields->notice_field_id.' '.$fields->name}}" class="selectpicker" id="officers_directors" data-parsley-required="true" data-parsley-required-message="Authorise signature is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true">
                                                            <option value="">Select</option>
                                                            @if(isset($officers_directors) && count($officers_directors)>0 )
                                                            @foreach($officers_directors as $officer)
                                                            <option value="{{$officer->first_name.' '.$officer->last_name}}" @if ($fields->value == $officer->first_name.' '.$officer->last_name) selected="selected" @endif > {{ucfirst(trans($officer->first_name.' '.$officer->last_name))}}</option>
                                                            @endforeach
                                                            @endif
                                                         </select>
                                                        </div>
                                                    </div>
                                                
                                                @elseif($fields->type==3 && $fields->name =='title')
                                                    <div class="col-md-6 col-sm-6 min-height">
                                                        <div class="select-opt">
                                                            <?php $label = str_replace("_", " ", $fields->name); ?>
                                                            <label class="col-md-12 control-label" for="textinput">{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>  

                                                            <select class="selectpicker" name="{{$fields->notice_field_id.' '.$fields->name}}" id="officers_directors_title" class="selectpicker"  data-parsley-required="true" data-parsley-required-message="Authorise signature is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true">
                                                                <option value="">Select</option>
                                                                @if(isset($officers_directors) && count($officers_directors)>0)
                                                                @foreach($officers_directors as $officer)
                                                                <option value="{{$officer->title}}" @if ($fields->value == $officer->title) selected="selected" @endif > {{ucfirst(trans($officer->title))}}</option>
                                                                @endforeach
                                                                @endif
                                                            </select>
                                                        </div>
                                                    </div>  
                                                @elseif($fields->type==3 && $fields->name =='city')

                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="select-opt">
                                                            <?php $label = str_replace("_", " ", $fields->name); ?>
                                                            <label class="col-md-12 control-label" for="textinput">{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>  
                                                            <select class="selectpicker" name="{{$fields->notice_field_id.' '.$fields->name}}" id="hard_doc_city_value" data-parsley-required="true" data-parsley-required-message="City is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true">
                                                                <option value="">Select</option>
                                                                @foreach($cities as $project_city)
                                                                <option value="{{$project_city->id}}"> {{ucfirst(trans($project_city->name))}}</option>
                                                                @endforeach
                                                            </select>

                                                        </div>
                                                    </div>
                                                    @elseif($fields->type==3 && $fields->name =='state')
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="select-opt">
                                                            <?php $label = str_replace("_", " ", $fields->name); ?>
                                                            <label class="col-md-12 control-label" for="textinput"  >{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>  
                                                            <select class="selectpicker" name="{{$fields->notice_field_id.' '.$fields->name}}" id="hard_doc_state_value" data-parsley-required="true" data-parsley-required-message="State is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true">
                                                                <option value="">Select</option>
                                                                @foreach($states as $state)
                                                                <option value="{{$state->id}}" > {{ucfirst(trans($state->name))}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    
                                                    @elseif($fields->type==4 && $fields->name =='notary_seal')
                                                    <?php $label = str_replace("_", " ", $fields->name); ?>
                                                    
                                                    <div class="col-md-6 col-sm-6 clear-b">
                                                        <div class="input-wrapper full-width work-status">
                                                            <ul class="checkbox-holder">
                                                                <li><a href="#"><input  class="regular-checkbox not-empty"  id="notary_seal" <?php echo ($fields->value) ? "checked=checked" : "" ?> name="{{$fields->notice_field_id.' '.$fields->name}}" value="1" type="checkbox"><label for="{{ $fields->name}}">{{ucfirst(trans($label))}}</label></a></li>
                                                                <input name="{{$fields->notice_field_id.' '.$fields->name}}" value="{{$fields->value}}" id="notary_seal_hidden" class="checkbox " type="hidden">
                                                            </ul>
                                                        </div>
                                                    </div>
                                                @elseif($fields->type==4 && $fields->name =='check_clear')
                                                    <?php $label = str_replace("_", " ", $fields->name); ?>
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="input-wrapper full-width work-status">
                                                            <ul class="checkbox-holder">
                                                                <li><a href="#"><input  class="regular-checkbox not-empty"  id="{{ $fields->name}}" <?php echo ($fields->value) ? "checked=checked" : "" ?> name="{{$fields->notice_field_id.' '.$fields->name}}" value="1" type="checkbox"><label for="{{ $fields->name}}">{{ucfirst(trans($label))}}</label></a></li>

                                                                <input name="{{$fields->notice_field_id.' '.$fields->name}}" value="{{$fields->value}}" id="check_clear_hidden" class="checkbox " type="hidden">
                                                            </ul>
                                                        </div>
                                                    </div>
                                           @elseif($fields->type==5)
                                                    <?php $label = str_replace("_", " ", $fields->name); ?>
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="date-box full-width">
                                                            <div class="input-wrapper full-width">
                                                                <div class="styled-input">
                                                                    <input class="form-control" required=""  type="radio" name="{{$fields->notice_field_id.' '.$fields->name}}" id="{{$fields->name}}" data-parsley-required="true" data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout">
                                                                    <label>{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>
                                                                    <span></span>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                       @endif
                                 @endforeach
                              @endif
                                                   
                                                 

                                                    <?php
                                                    $document_types = getDocumentTypes();
                                                    ?>
                                                    <div class="col-md-12 col-sm-12">
                                                        <div class="row">
                                                            <div class="col-lg-6 col-md-6 col-sm-12">
                                                                <div class="select-opt mt-top">
                                                                    <label class="col-md-12 control-label" for="textinput">Document Type</label>  
                                                                    <select class="selectpicker" name="type" id="edit_select_document_type" >
                                                                        <option value="">-Select-</option>
                                                                        @foreach ($document_types as $key => $value) 
                                                                        <option value="{{$key}}">{{$value}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    @if ($errors->has('type'))
                                                                    <p class="help-block">
                                                                        <strong>{{ $errors->first('type') }}</strong>
                                                                    </p>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12 col-sm-12 doc_field">
                                                                <div class="row">
                                                                    <div class="col-md-12 col-sm-12">
                                                                        <div class="input-wrapper full-width">
                                                                            <div class="styled-input">
                                                                                <input class="form-control" name="doc_input"  type="text" id="edit_doc_input_id">
                                                                                <label id="doc_field"></label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <input type="hidden" name="edit_document_name" id="edit_document_name">
                                                            <input type="hidden" name="edit_doc_id" id="edit_doc_id">
                                                            <input type="hidden" id="row_id">
                                                            <div class="col-md-12 col-sm-12 col-xs-12">

                                                                <div class="form-btn-div doc_file">
                                                                    <span class="btn btn-default btn-file upload-btn btn btn-primary form-btn" id="temp">
                                                                        Choose File to Upload  <input type="file" name="file_name" id="edit_result_file">
                                                                    </span>
                                                                    <span id="errormessage" style="display: none"></span>
                                                                </div>
                                                            </div>
                                                            <div class="form-btn-div doc_file col-md-12 col-sm-12 col-xs-12" >
                                                            <div style="float: left;width: 10">
                                                                <a href="javascript:;" onClick="openTab(this)" id="open_doc" name=""><span  id="preview" style="display: none;" ></span></a>
                                                            </div>
                                                            <div style="float: left ;width: 10;padding-left: 20px">
                                                                <a  href="javascript:;" ><span id="remove_icon" style="display: none;"  >Delete</span></a>
                                                            </div>
                                                        </div>
                                                         <input type="hidden" name="remove_file" id="remove_file" value="0">
                                                            <div class="col-md-12 col-sm-12 col-xs-12">

                                                                <div class="add-new-btn inner-dash-btn full-width doc_file">
                                                                    <button  name="submit_doc" type="button" value="Add" id="submit_document" class="btn btn-primary custom-btn">
                                                                         <span class="update_doc">Add</span>
                                                                    </button>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-12 col-sm-12">
                                                                <div class="row">
                                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                                        <div class="dahboard-table doc-table full-width table-responsive">
                                                                            <table class="table table-striped" id="edit_document_table">
                                                                                <thead class="thead-dark">
                                                                                    <tr>
                                                                                        <th scope="col">Document Type</th>
                                                                                        <th scope="col">Document Description</th>
                                                                                        <th scope="col">Name</th>
                                                                                        <th scope="col">Date & Time</th>
                                                                                        <th scope="col">Type of Attachment</th>
                                                                                        <th scope="col">Options</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                    @if(isset($attachment) && !empty($attachment) && count($attachment)>0)
                                                                                    @foreach($attachment AS $each_attachment)
                                                                                    <?php $file_name = explode('.', $each_attachment->file_name) ?>
                                                                                    <tr id="refreshdelete{{$each_attachment->id}}">
                                                                                        <td>
                                                                                            @foreach ($document_types as $key => $value) 
                                                                                            @if ($each_attachment->type == $key) 
                                                                                            {{$value}}
                                                                                            @endif
                                                                                            @endforeach
                                                                                        </td>
                                                                                        <td>{{$each_attachment->title}}</td>

                                                                                        <td><a href="{{asset('attachment/secondary_document/'.$each_attachment->file_name)}}" target="_blank">{{$each_attachment->original_file_name}}</a></td>
                                                                                        <td>{{date('d M y h:i A',strtotime($each_attachment->created_at))}}</td>
                                                                                        <td>
                                                                                            <?php
                                                                                            $extension = explode('.', $each_attachment->original_file_name);
                                                                                            echo $ext=$each_attachment->original_file_name != "" ? $extension[1] : "";
                                                                                            ?>

                                                                                        </td>
                                                                                        <td><a class='btn btn-secondary item red-tooltip remove-attachment' data-toggle='modal' 
                                                                                               data-url='{{url('customer/edit/removeattachment')}}'  data-id="{{$each_attachment->id}}" data-file='{{$each_attachment->file_name}}' 
                                                                                               data-target='#attachment-confirmation-modal' data-placement='bottom' title='Remove'>
                                                                                                <span class='icon-cross-remove-sign'></span></a>
                                                                                         <a class='btn btn-secondary item red-tooltip edit-attachment'
                                                                                                   data-id="{{$each_attachment->id}}" data-file='{{$each_attachment->file_name}}' data-type="{{$each_attachment->type}}" data-title="{{$each_attachment->title}}" data-file-original-name="{{$each_attachment->original_file_name}}" data-file-extension="{{$ext}}">
                                                                                                <span class='icon-pencil-edit-button'></span>
                                                                                            </a>

                                                                                        @if(!empty(trim($each_attachment->file_name)))
                                                                                           <a href="{{asset('attachment/secondary_document/'.$each_attachment->file_name)}}" target="_blank" class="btn btn-secondary item red-tooltip"><i class="fa fa-eye" style=""></i></a>
                                                                                        @endif
                                                                                        </td>
                                                                                    </tr>
                                                                                    @endforeach
                                                                                    @else
                                                                                <td class="text-center norecords" colspan="6">No record found</td>
                                                                                @endif
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-bottom-btn">
                                                        <input type="hidden" name="secondary_doc_attachment[]" id="edit_secondary_doc_attachment"/>
                                                        <input type="hidden" name="remove_doc[]" id="remove_doc"/>
                                                        <a href="{{url('customer/secondary-document')}}" class="btn btn-primary custom-btn customb-btn"><span>Back</span></a>
                                                        {{--<a href="{{url('home')}}" class="btn btn-primary custom-btn customb-btn"><span>Cancel</span></a>--}}
                                                        <button type="submit" name="submit" value="save_for_later" class="btn btn-primary custom-btn customc-btn save_for_later" id="save_for_later"><span>Save For Later</span></button>
                                                        <button type="button" name="submit" value="continue" class="btn btn-primary custom-btn customc-btn generate_pdf" id="continue"><span>Generate PDF</span></button>
                                                      <!--   <a  href="#" name="submit" class="btn btn-primary custom-btn customc-btn" value="submit" id="generate" target="_blank"><span>Generate PDF</span></a> -->

                                                    </div>
                                                </div>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                                @if($tab=="project")
                                <div role="tabpanel" class="tab-pane" id="recepients">
                                    @else
                                    <div role="tabpanel" class="tab-pane active" id="recepients">

                                        @endif

                                        <div class="project-wrapper"> 
                                            <form method="post" action="{{ url('customer/secondary-recipients/'.$secondary_doc_id) }}" data-parsley-validate="" id="update_recipient_data">
                                                {!! csrf_field() !!}
                                                <div class="dash-subform mt-0">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="sub-section-title">
                                                                        <h2>Recipients</h2>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="dahboard-table table-responsive">
                                                                        <table class="table table-striped">
                                                                            <thead class="thead-dark">
                                                                                <tr>
                                                                                    <th scope="col">Category</th>
                                                                                    <th scope="col">Recipients</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>

                                                                                @if(isset($count) && !empty($count) && count($count)>0) 
                                                                                @foreach($count as $k => $count)
                                                                                <tr>
                                                                                    <td class="checkbox-holder">
                                                                                        <input type="checkbox" id="checkbox-3-{{$k}}" class="regular-checkbox">
                                                                                        <label for="checkbox-3-{{$k}}">{{$count->name}}</label>
                                                                                    </td>
                                                                                    <td>{{$count->total}}</td>
                                                                                </tr>
                                                                                @endforeach   
                                                                                @else
                                                                            <td class="text-center" colspan="3">No records found</td>
                                                                            @endif
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="sub-section-title">
                                                                        <h2>Selected Recipients</h2>
                                                                    </div>
                                                                </div>
                                <!--                                                        <span>Note : - Customer will be able to edit the details of recipient selected</span>-->
                                                                <div class="col-md-12">
                                                                    <div class="dahboard-table table-responsive">
                                                                        <table class="table table-striped">
                                                                            <thead class="thead-dark">
                                                                                <tr>
                                                                                    <th scope="col">Role</th>
                                                                                    <th scope="col">Company Name</th>
                                <!--                                                    <th scope="col">Contact Person</th>-->
                                <!--                                                    <th scope="col">Phone Number</th>-->
                                                                                    <th scope="col">Address</th>
                                                                                    <th scope="col">City</th>
                                                                                    <th scope="col">State</th>
                                                                                    <th scope="col">Zip Code</th>
                                                                                    <th scope="col">Action</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                @if(isset($recipients) && !empty($recipients) && count($recipients)>0)
                                                                                @foreach($recipients  as $recipient)
                                                                                <tr>
                                                                                    <td class="cl-blue">{{$recipient->category->name}}</td>
                                                                                    <td>{{$recipient->name}}</td>
<!--                                                                                        <td>{{$recipient->contact}}</td>-->
<!--                                                                                        <td>{{$recipient->mobile}}</td>-->
                                                                                    <td>

                                                                                        @if (strlen($recipient->address) > 10)
                                                                                        {{ substr($recipient->address,0, 10) }}
                                                                                        <a href="#" data-toggle="tooltip" title='{{$recipient->address}}'>...</a>
                                                                                        @else
                                                                                        {{$recipient->address}}
                                                                                        @endif
                                                                                    </td>
                                                                                    <td>{{$recipient->city->name}}</td>

                                                                                    <td>{{$recipient->state->name}}</td>
                                                                                    <td>{{$recipient->zip}}</td>
                                                                                    <td>
                                                                                        <a href="#" data-id="{{$recipient->id}}" class="edit_recipient"><span class="icon-pencil-edit-button"></span></a>
<!--                                                                                        <a href="{{url('customer/secondary-recipient/delete/'.$recipient->id.'/'.$recipient->secondary_document_id.'/'.$partial_final_type)}}"><span class="icon-cross-remove-sign"></span></a>-->
                                                                                        <a data-id="{{$recipient->id}}" data-secondary-doc-id="{{$recipient->secondary_document_id}}" data-type="{{$partial_final_type}}" href="#" class="remove-record" data-toggle='modal' data-toggle="tooltip" data-target='#contact-confirmation-modal' data-placement="bottom" title="Remove"><span class="icon-cross-remove-sign"></span></a>

                                                                                    </td>
                                                                                </tr>
                                                                                @endforeach
                                                                                @else
                                                                            <td class="text-center" colspan="5">No records found</td>

                                                                            @endif
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="dash-subform">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="sub-section-title">
                                                                <h2>Recipients</h2>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <div class="row">

                                                                <div class="col-md-6 col-sm-6">
                                                                    <input type="text" name="partial_final_type" value={{$partial_final_type}} hidden="">
                                                                    <input type="text" name="secondary_doc_id" value={{$secondary_doc_id}} hidden="">
                                                                    <input type="hidden" name="recipient_id" id="recipient_id">
                                                                    <div class="select-opt">

                                                                        <label class="col-md-12 control-label" for="textinput">Type of Recipient<span class="mandatory-field">*</span></label>  
                                                                        <select class="selectpicker" name="category_id" id="category_id" data-parsley-required="true" data-parsley-required-message="Type of recipient is required">
                                                                            <option value="">select</option>
                                                                            @foreach($categories as $category)
                                                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="col-md-6 col-sm-6">
                                                            <div class="select-opt select2-container select2class">
                                                                <label class="col-md-12 control-label" for="textinput">Name<span class="mandatory-field">*</span></label>  

                                                                <select class="js-example-tags form-control"  name="name" id="name" data-parsley-required="true" data-parsley-required-message="Name is required"  data-show-subtext="true" data-live-search="true">
                                                                    <option value="">Select</option>
                                                                    @if(isset($names) && !empty($names))
                                                                    @foreach($names as $each_name)
                                                                    <option value="{{$each_name->company_name}}">{{$each_name->company_name}}</option>
                                                                    @endforeach
                                                                    @endif
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-bottom-btn pd-25">

                                                                <button type="button" name="add_contact_submit" class="btn btn-primary custom-btn customc-btn add_contact_details" value="contacts" id="add_contact" data-toggle="modal" data-target="#contactModal"><span>Add Address Book</span></button>
                                                                <button type="button" name="add_contact_submit" class="btn btn-primary custom-btn customc-btn update_contact" value="contacts" id="update_contact" disabled=""><span>Update Address Book</span></button>

                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6">
                                                            <div class="input-wrapper full-width">
                                                                 <label class="col-md-12 control-label" for="textarea">Attn/Additional Recipient Name<span class="mandatory-field"></span></label>
                                                                <div class="styled-input">
                                                                    <textarea class="form-control recipient_attns"  type="text" name="attn" id="recipient_attn" data-parsley-trigger="change focusout"></textarea>
                                                                    <span></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6">
                                                            <div class="input-wrapper full-width">
                                                                <div class="styled-input">
                                                                    <input class="form-control business_phone_validation" type="text" name="contact" id="contact" data-parsley-trigger="change focusout" data-parsley-pattern="(\(\d{3})\)\s\d{3}\-\d{4}$">
                                                                    <label>Telephone</label>
                                                                    <span></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--                        <div class="col-md-6 col-sm-6 clear-b">
                                                                                    <div class="input-wrapper full-width">
                                                                                        <div class="styled-input">
                                                                                            <input class="form-control" type="text" name="mobile"  id="mobile"  >
                                                                                            <label>Mobile Number</label>
                                                                                            <span></span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>-->
                                                        <div class="col-md-6 col-sm-6">
                                                            <div class="input-wrapper full-width">
                                                                <div class="styled-input">
                                                                    <input class="form-control"  type="text" name="email" id="email" >
                                                                    <label>Email</label>
                                                                    <span></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-sm-12">
                                                            <div class="input-wrapper full-width">
                                                                <div class="styled-input">
                                                                    <input class="form-control"  type="text" name="address" id="address" data-parsley-required="true" data-parsley-required-message="Address is required">
                                                                    <label>Address<span class="mandatory-field">*</span></label>
                                                                    <span></span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6 col-sm-6">

                                                            <div class="input-wrapper full-width">
                                                                <div class="styled-input">
                                                                    <input class="form-control"  type="text" name="city_id" id="city" data-parsley-required="true" data-parsley-required-message="Select City from the list" data-parsley-trigger="change focusout">
                                                                    <input type="hidden" name="recipient_city_id" id="recipient_city_id"/>
                                                                    <label class="col-md-12 control-label" for="textinput">City<span class="mandatory-field">*</span></label> 
                                                                    <span></span>                                                                
                                                                </div>
                                                                <div id="recipientloader" style="display: none">
                                                                    <img width="150px" src="{{asset('images/loader.gif')}}"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <input type="text" name="state_id" value="" class="state" hidden="">
                                                        <div class="col-md-6 col-sm-6">
                                                            <div class="select-opt">
                                                                <label class="col-md-12 control-label" for="textinput">State* </label>  
                                                                <select class="selectpicker" id="state" name="state_id" data-parsley-required="true" data-parsley-required-message="State is required" data-show-subtext="true" data-live-search="true">
                                                                    <option value="">Select</option>
                                                                    @foreach($states as $state)
                                                                    <option value="{{$state->id}}">{{$state->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6 col-sm-6 clear-b">
                                                            <div class="input-wrapper full-width">
                                                                <div class="styled-input">
                                                                    <input class="form-control"  type="text" name="zip" id="zip" data-parsley-required="true" data-parsley-required-message="Zip code is required">
                                                                    <label>Zip Code<span class="mandatory-field">*</span></label>
                                                                    <span></span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="form-bottom-btn pd-25">



                                                                <button  type="submit" name="continue"   class="btn btn-primary custom-btn customc-btn add_recipient" value="add"><span>Add Recipient</span></button>
                                                                <button  type="submit" name="continue"  class="btn btn-primary custom-btn customc-btn update_recipient" value="update" disabled=""><span>Update Recipient</span></button>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-bottom-btn">
<!--                                                                <a href="{{URL::previous()}}" class="btn btn-primary custom-btn customb-btn"><span>Back</span></a>-->
                                                            <!--<input type="button" id="editBack" name="editBack" class="btn btn-primary custom-btn customb-btn" value="Back"/>-->
                                                            <a type="button" id="editBack" name="editBack" class="btn btn-primary custom-btn customb-btn not-empty" value="Back"><span>back</span></a>


                                                            <a href="{{url('home')}}" class="btn btn-primary custom-btn customb-btn"><span>Cancel</span></a>
                                                            <a href="{{url('customer/secondary-document/updateStatus/'.$secondary_doc_id)}}" class="btn btn-primary custom-btn customb-btn"><span>Save for Later</span></a>
                                                            @if($status=='request')
                                                            @if($partial_final_type ==0)
                                                            <a  href="{{url('customer/secondary-document/printSecondaryDocument/'.$secondary_doc_id.'/Partial Release')}}" name="submit" class="btn btn-primary custom-btn customc-btn" value="submit" target="_blank"><span>Submit</span></a>
                                                            @else
                                                            <a  href="{{url('customer/secondary-document/printSecondaryDocument/'.$secondary_doc_id.'/Final Release')}}" name="submit" class="btn btn-primary custom-btn customc-btn" value="submit" target="_blank"><span>Submit</span></a>
                                                            @endif 
                                                            @elseif($status=='draft')
                                                            <input type="button" name="create_label_submit" class="btn btn-primary custom-btn customc-btn" value="Submit" title="Please fill all mandetor project information" disabled="" />

                                                            @endif

                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                    <!-- add new contacts-->

                                    <div id="contactModal" class="modal fade register-modal addcon-modal">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <div id="success-msg"class="alert alert-success" style="display: none"></div>
                                                    <div id="error-msg"class="alert alert-error" style="display: none"></div>
                                                    <h4 class="modal-title">Add Address Book</h4>	
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                </div>
                                                <div class="modal-body">
                                                    <form method="POST" id="add_recipient_contact">
                                                        {{csrf_field()}}
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="input-wrapper full-width">
                                                                <div class="styled-input">
                                                                    <input class="form-control"  type="text" name="contact_name" value="{{old('contact_name')}}" id="contact_name">
                                                                    <label>Name<span class="mandatory-field">*</span></label>
                                                                    <span></span>
                                                                    <p class="help-block">
                                                                        <strong id="name-error"></strong>
                                                                    </p>  
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-xs-12 ">
                                                            <div class="input-wrapper full-width">
                                                                <label class="col-md-12 control-label" for="textarea">Attn/Additional Recipient Name<span class="mandatory-field"></span></label>
                                                                <div class="styled-input">
                                                                    <textarea class="form-control attn recipient_attns"   name="attn" value="{{old('attn')}}" id="attn"></textarea>
                                                                    <p class="help-block">
                                                                        <strong id="attn-error"></strong>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="input-wrapper full-width">
                                                                <div class="styled-input">
                                                                    <input class="form-control business_phone_validation"  type="text" name="contact_no" value="{{old('contact_no')}}" id="contact_no">
                                                                    <label>Telephone</label>
                                                                    <span></span>
                                                                    <p class="help-block">
                                                                        <strong id="contact-error"></strong>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="input-wrapper full-width">
                                                                <div class="styled-input">
                                                                    <input class="form-control email_validation"  type="email" name="contact_email" value="{{old('contact_email')}}" id="contact_email">
                                                                    <label>Email</label>
                                                                    <span></span>
                                                                    <p class="help-block">
                                                                        <strong id="email-error"></strong>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="input-wrapper full-width">
                                                                <div class="styled-input"> 
                                                                    <input class="form-control"  type="text" name="contact_address" value="{{old('contact_address')}}" id="contact_address">
                                                                    <label>Address<span class="mandatory-field">*</span></label>
                                                                    <span></span>
                                                                    <p class="help-block">
                                                                        <strong id="address-error"></strong>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="input-wrapper full-width">
                                                                <div class="styled-input">
                                                                    <input class="form-control"  type="text" name="contact_city_id" id="contact_city_id" data-parsley-required="true" data-parsley-required-message="Select City from the list" data-parsley-trigger="change focusout">
                                                                    <input type="hidden" name="autocomplete_city_id" id="autocomplete_city_id"/>
                                                                    <label class="col-md-12 control-label" for="textinput">City<span class="mandatory-field">*</span></label> 
                                                                    <span></span>
                                                                    <p class="help-block">
                                                                        <strong id="city-error"></strong>
                                                                    </p>
                                                                </div>
                                                                <div id="recipientpopuploader" style="display: none">
                                                                    <img width="150px" src="{{asset('images/loader.gif')}}"/>
                                                                </div>
                                                                </select>


                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="select-opt">
                                                                <label class="col-md-12 control-label">State<span class="mandatory-field">*</span></label>
                                                                <select class="selectpicker"  name="contact_state_id" data-parsley-required="true" data-parsley-required-message="State is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true" id="contact_state_id">
                                                                    <option value="">Select</option>
                                                                    @foreach($states as $state)
                                                                    <option value="{{$state->id}}" @if(old('contact_state_id')) selected="selected" @endif>{{$state->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                                <p class="help-block">
                                                                    <strong id="state-error"></strong>
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="input-wrapper full-width">
                                                                <div class="styled-input">
                                                                    <input class="form-control zip_validation"  type="text" name="contact_zip" value="{{old('contact_zip')}}" id="contact_zip">
                                                                    <label>Zip Code<span class="mandatory-field">*</span></label>
                                                                    <span></span>
                                                                    <p class="help-block">
                                                                        <strong id="zip-error"></strong>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </form>
                                                </div>

                                                <div class="modal-footer">
                                                    <div class="form-bottom-btn">
                                                        <input type="hidden" name="edit_secondary_doc_id" value="{{$secondary_doc_id}}" id="doc_id_recipient"/>
                                                        <button type="submit" class="btn btn-primary custom-btn customc-btn" id="submit_contact"><span>Add Address Book</span></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>  

                                    </div>
                                </div>
                            </div></div>
                        <form action="" method="POST" class="remove-attachment-model" id="remove-attachment-model">
                            {!! csrf_field() !!}
                            <div id="attachment-confirmation-modal" class="modal fade">
                                <div class="modal-dialog modal-confirm">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <div class="icon-box">
                                                <i class="fa fa-close"></i>
                                            </div>				
                                            <h4 class="modal-title">Are you sure?</h4>	
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        </div>
                                        <div class="modal-body">
                                            <p>Do you really want to delete these records? This process cannot be undone.</p>
                                        </div>
                                        <div class="modal-footer">
                                            <div class="form-bottom-btn">
                                                <button type="button" class="btn btn-primary custom-btn customb-btn remove-attachement-from-delete-form" data-dismiss="modal"><span>Cancel</span></button>
                                                <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>Confirm</span></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <form action="" method="GET" class="remove-record-model">
                            {!! csrf_field() !!}
                            <div id="contact-confirmation-modal" class="modal fade">
                                <div class="modal-dialog modal-confirm">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <div class="icon-box">
                                                <i class="fa fa-close"></i>
                                            </div>				
                                            <h4 class="modal-title">Are you sure?</h4>	
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        </div>
                                        <div class="modal-body">
                                            <input type="text" name="contact_id" id="contact_id" hidden="">              
                                            <p>Do you really want to delete these records? This process cannot be undone.</p>
                                        </div>
                                        <div class="modal-footer">
                                            <div class="form-bottom-btn">
                                                <button type="button" class="btn btn-primary custom-btn customb-btn remove-data-from-delete-form" data-dismiss="modal"><span>Cancel</span></button>
                                                <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>Confirm</span></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        @stop
                        @section('frontend_js')
                        <script type="text/javascript" src="{{ asset('js/jquery-ui.min.js') }}"></script>

                        <script src="{{ asset('vendor/adminlte/plugins/datepicker/bootstrap-datepicker.js') }}" type="text/javascript"></script>
                        <script type="text/javascript">
$(document).on('click', '#notary_seal', function (e) {
        if ($(this).is(':checked')) {
            $('#notary_seal_hidden').val('1');
        } else {
            $('#notary_seal_hidden').val('0');
        }

    });

    $(document).on('click', '#check_clear', function (e) {
        if ($(this).is(':checked')) {
            $('#check_clear_hidden').val('1');
        } else {
            $('#check_clear_hidden').val('0');
        }

    });
$('#editBack').click(function () {
    $('.display-ib li').removeClass('active');
    $('.display-ib li:first-child').addClass('active');
//    $('#project_tab').addClass('active');
    $('.notice-tabs .tab-pane').removeClass('active');
    $('#project').addClass('active');
})
$('#edit_secondary_doc_note').parsley();
$('#edit_secondary_doc_project').parsley();
//Start to submit note
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$('.notice-btn').click(function (e) {

    $('input[name=note]').val('');
    $('input[name=email]').val('');
    $('.note-success').empty();
    $('.note-success').css('display', 'none');
    $('.note-error').empty();
    $('.note-error').css('display', 'none');
});
$(".submitNote").click(function (e) {
    e.preventDefault();
    var note = $("input[name=note]").val();
    var email = $("#note_email").val();
    var secondary_doc_id = $('#edit_secondary_doc_id').val();
    $.ajax({
        type: 'POST',
        url: '{{url("customer/secondary-document/store_note")}}',
        data: {note: note, email: email, doc_id: secondary_doc_id},
        beforeSend: function () {
            $('#pleaseWait').modal('show');
        },
        complete: function () {
            $('#pleaseWait').modal('hide');
        },
        success: function (data) {
            if (data.success) {
                var d = new Date();
                var strDate = (d.getMonth() + 1) + "-" + d.getDate() + "-" + d.getFullYear();
                $('.note-error').empty();
                $('.note-success').empty();
                $("input[name=note]").val('');
                $("#note_email option").prop("selected", false);
                $('#note_email').selectpicker('refresh');
                $('.note-error').css('display', 'none');
                $('.note-success').show();
                $('.note-success').append('<p>' + data.success + '</p>');
                // $('.notes_div').append('<li>' + data.notes.note + '<br>' + data.notes.email + '</li>');
                $('.notes_div').prepend('<li>' + data.notes.note + '</br>Added By:'+ data.notes.name + '</br>Created at:' + strDate + '</li>');

            } else if (data.errors) {
                $('.note-error').empty();
                $.each(data.errors, function (key, value) {
                    $('.note-success').empty();
                    $('.note-success').css('display', 'none');
                    $('.note-error').show();
                    $('.note-error').append('<p>' + value + '</p>');
                });
            }
        }
    });
});
$(".submitCorrection").click(function (e) {
    e.preventDefault();
    $('.correction-error').hide();
    $('.correction-success').hide();
    $.ajax({
        type: 'POST',
        url: '{{url("customer/secondary-document/store_correction")}}',
        data: $('#edit_secondary_document_correction').serialize(),
        beforeSend: function () {
            $('#pleaseWait').modal('show');
        },
        complete: function () {
            $('#pleaseWait').modal('hide');
        },
        success: function (data) {
            if (data.success) {
                var d = new Date();
                var strDate = (d.getMonth() + 1) + "-" + d.getDate() + "-" + d.getFullYear();
                $('.correction-error').empty();
                $('.correction-success').empty();
                $("input[name=correction]").val('');
                $("#correction_email option").prop("selected", false);
                $('#correction_email').selectpicker('refresh');
                $('.correction-error').css('display', 'none');
                $('.correction-success').show();

                $('.correction-success').append('<p>' + data.success + '</p>');
//                $('.correction_div').append('<li>' + data.corrections.correction + '<br>' + data.corrections.email + '</li>');
                $('.correction_div').prepend('<li>' + data.corrections.correction + '</br>Added By:'+ data.corrections.name+ '</br>Created at:' + strDate + '</li>');

            } else if (data.errors) {
                $('.correction-error').empty();
                $.each(data.errors, function (key, value) {
                    $('.correction-success').empty();
                    $('.correction-success').css('display', 'none');
                    $('.correction-error').show();
                    $('.correction-error').append('<p>' + value + '</p>');
                });
            }
        }
    });
});

//End to submit note

//Start to fetch data of related work order

$('#edit_work_order_no').on('change', function () {
    $(".delete_parent_attachment").remove();
    $('#parent_work_order_no').val($('#edit_work_order_no').val());
//$('#edit_work_order_no').find('option').remove().end().append('<option value="">Select</option>');
    $.ajax({
        data: {'work_order_no': $('#edit_work_order_no').val()},
        type: 'post',
        url: "{{url('customer/secondary-document/work_order_details')}}",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            if (response.success) {
                var stateid = "";
                $.each(response.success, function (key, val) {
                     if (val.name == 'state') {
                        $('#soft_doc_state_value').val(val.value);
                        $("#soft_doc_state_value").selectpicker("refresh");


                    } else if (val.name == 'city') {
                        $('#soft_doc_city_value').val(val.value);
                        $("#soft_doc_city_value").selectpicker("refresh");

                    } else if (val.name == 'county') {
                        $('#county_id').val(val.value);
                        $("#parent_country").val(val.county_name);
                    } else if (val.name == 'project_type') {

                        $('#project_type').val(val.value);
                        $("#project_type").selectpicker("refresh");

                    } else if (val.name == 'your_role') {

                        $('#your_role').val(val.value);
                        $("#your_role").selectpicker("refresh");

                    } else if (val.name == 'contracted_by') {
                        $("#contracted_by_select").val(val.value).trigger('change');
                        // $('#contracted_by_select').val(val.value);
                        //$("#contracted_by_select").selectpicker("refresh");

                    } else if (val.name == 'job_start_date') {
                        $('#job_start_date').val(val.value);
                        $('#enter_into_agreement').val(val.value);


                    } else if (val.name == 'last_date_on_the_job') {
                        $('#last_date_of_labor_service_furnished').val(val.value);
                        $('#last_date_on_the_job').val(val.value);
                    } else {
                        var field_name = val.name;
                        var vars = val.value;
                        let result = vars.replace(/<br \/>/gi, "<p>");
                        $("#" + field_name).val(result);
                    }
                });

            }
            if (response.parent_attachment) {
                // console.log(response.parent_attachment);
                $.each(response.parent_attachment, function (k, v) {

                    var f1 = v.db_arr[2];
                    var f2 = f1.split('.')[0];
                    var row = '<tr class="delete_parent_attachment" id="refreshdelete' + v.db_arr[4] + '">';
                    $.each((v.ajax_arr), function (i, d) {
                        $('.norecords').closest('tr').remove();
                        var display_url = '{{asset("attachment/secondary_document/parent_document")}}/' + v.db_arr[2];
                       /* if (i == 2) {
                            row += '<td><a target="_blank" href="' + display_url + '">' + d + '</a></td>';
                        } else {*/
                            row += '<td>' + d + '</td>';
                      //  }
                    });
                    var display_url = '{{asset("attachment/secondary_document/parent_document")}}/' + v.db_arr[2];
                    var attachment_url = '{{url("customer/parent_document/removeattachment")}}';
                   /* row += "<td><a class='btn btn-secondary item red-tooltip remove-parent-attachment' data-toggle='modal' data-url='" + attachment_url + "'  data-file='" + v.db_arr[2] + "' data-target='#attachment-confirmation-modal' data-placement='bottom' title='Remove'><span class='icon-cross-remove-sign'></span></a></td>";*/
                     if(v.db_arr[2].length>0){
                        row += "<td><a target='_blank' href='" + display_url + "' class='btn btn-secondary item red-tooltip'><i class='fa fa-eye'></i></a></td>";
                            }else{
                                row += "<td></td>";
                            }
                        row += '</tr>';
                    $('#edit_document_table tbody').append(row);

                });

            }

            $('#project_owner').val("");
            var owner = '';
            if (response.owner_recipient) {

                $.each(response.owner_recipient, function (key, val) {

                    owner += val.name + ' and ';

                });
                $('#project_owner').val(owner.slice(0,-4));
                $('#property_owner').val(owner.slice(0,-4));
            }
            $('input,textarea').each(function () {
                if ($(this).val().length > 0) {
                    $(this).addClass('not-empty');
                } else {
                    $(this).removeClass('not-empty');
                }
            });
        }
    });
});
//End to fetch data of related work order

$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip({
        trigger: 'click',
        placement: 'bottom'
    });

    $("#contact").mask("(999) 999-9999");

    $('textarea').each(function () {
        if ($(this).val().length > 0) {
            $(this).addClass('not-empty');
        } else {
            $(this).removeClass('not-empty');
        }
    });
    $('textarea').keyup(function () {
        if ($(this).val().length > 0) {
            $(this).addClass('not-empty');
        } else {
            $(this).removeClass('not-empty');
        }
    });

//address varificvation tab

//save for later validation detroy
//start of aucomplete contact details
    $('#name').on('change', function () {
        var name = $('#name').val();
        var email = $('#email').val();
        var mobile = $('#mobile').val();
        var contact = $('#contact').val();
        var address = $('#address').val();
        var fax = $('#fax').val();
        var city = $('.city').val();
        var state = $('.state').val();
        $.ajax({
            data: {'name': name, 'customer_id': $('#customer_id').val()},
            type: "post",
            url: "{{ url('customer/get-contacts') }}",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (res) {
                // $('').empty();
                // $('#name').val(res.company_name);
                $('#email').val(res.email);
                $('#mobile').val(res.mobile);
                $('#contact').val(res.phone);
                $('#zip').val(res.zip);
                $('#address').val(res.mailing_address);
                $('#fax').val(res.fax);
                $('#state').val(res.state_id);
                $('#state').selectpicker('refresh');
                $('#city').val(res.city_name);
                $('#recipient_city_id').val(res.city_id);
                $('#recipient_attn').val(res.attn);
//                $('#country').val(res.country_id);
//                $('#country').selectpicker('refresh');

                $('input').each(function () {
                    if ($(this).val().length > 0) {
                        $(this).addClass('not-empty');
                    } else {
                        $(this).removeClass('not-empty');
                    }
                });
            },
        });
    });
//end of autocomplete contact details
//Start to fetch work order number of customers
//    $('#edit_work_order_no').find('option').remove().end().append('<option value="">Select</option>');
//
//    $.ajax({
//        type: "get",
//        url: "{{url('customer/secondary-document/autocomplete')}}",
//        headers: {
//            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//        },
//        success: function (response) {
//
////  console.log(response);
//            $.each(response, function (e, val) {
//
//                $("#edit_work_order_no").append("<option value='" + val.id + "'>" + val.id + "</option>");
//                $("#edit_work_order_no").selectpicker("refresh");
//            });
//            if ('{{$secondary_document->work_order_no}}') {
//                $("#edit_work_order_no").val('{{$secondary_document->work_order_no}}');
//                $("#edit_work_order_no").selectpicker("refresh");
//            }
//
//        }
//    });

$('#edit_work_order_no').select2({
    minimumInputLength: 1,
    ajax: {
        url : "{{ url('customer/secondary-document/autocomplete_V1?work_order_id='.$secondary_doc_id) }}",
        dataType: 'json',
        
        delay: 250,
        type: "get",
        processResults: function (data) {
        return {
            results:  $.map(data, function (item) {
                return {
                    text: item,
                    id: item
                }
            })
        };
        },
        cache: true
    }
});
//End to fetch work order number of customers
//save for later validation detroy
    $(".save_for_later").on('click', function () {

        $('#edit_secondary_doc_project').parsley().destroy();
    });

//Document Functionality
//Start for document section
    $('.doc_field').hide();
    $('.doc_file').hide();
    $('.datepicker').datepicker({
//  autoclose: true
    });
    $('#edit_select_document_type').change(function () {

        var selected_doc_type = $(this).val();
        $('#edit_doc_input_id').val("");
        if (selected_doc_type == 1) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('#doc_field').text('Bond Number');
        } else if (selected_doc_type == 2) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('#doc_field').text('NOC Book/Page');
        } else if (selected_doc_type == 3) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('#doc_field').text('Permit No');
        } else if (selected_doc_type == 5) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('#doc_field').text('Invoice No.');
        } else if (selected_doc_type == 4 || selected_doc_type == 6 ||
                selected_doc_type == 7 || selected_doc_type == 8
                || selected_doc_type == 9 || selected_doc_type == 13) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('#doc_field').text('Description');
        } else if (selected_doc_type == 10) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('#doc_field').text('Folio No.');
        } else if (!selected_doc_type) {
            $('.doc_field').hide();
            $('.doc_file').hide();
        }

    });

//Start to upload document
    var files;

    var result_arr = [];
    $("#submit_document").on("click", function (e) {
    if($('.update_doc').html()=='Add')
        {
        e.preventDefault();
        var extension = $('#edit_result_file').val().split('.').pop().toLowerCase();
        if ($('#edit_result_file').val() != "" && $.inArray(extension, ['csv', 'xls', 'xlsx', 'pdf', 'doc', 'docx', 'jpg', 'jpeg']) == -1) {
            $('#errormessage').html('Please Select Valid File... ');
            $('#errormessage').css('display', 'block');
        } else {
            var file_data = $('#edit_result_file').prop('files')[0];
            var selected_document_type = $('#edit_select_document_type').val();
            var doc_input = $('#edit_doc_input_id').val();
            var form_data = new FormData();

            form_data.append('file', file_data);
            form_data.append('document_type', selected_document_type);
            form_data.append('doc_value', doc_input)
            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': $('meta[name=_token]').attr('content')
                }
            });
            $.ajax({
                url: "{{url('customer/secondary-document/attachments')}}", // point to server-side PHP script
                data: form_data,
                type: 'POST',
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                processData: false,
                success: function (response) {
                    var data = jQuery.parseJSON(response);
                    var ran_no = Math.random() * (100 - 1) + 1;
                    ran_no = String(ran_no).replace('.','_');
                    data.db_arr.push('refreshdelete'+ran_no);
                    result_arr.push(data.db_arr);
                    console.log(result_arr);
                    result_arr.push('|');
                    //   console.log(result_arr);
                    $('#edit_secondary_doc_attachment').val(result_arr);
                    var f1 = data.db_arr[2];
                    var f2 = f1.split('.')[0];
                  
                    var row = '<tr id="refreshdelete' + ran_no + '">';
                    $.each((data.ajax_arr), function (i, d) {
                        $('.norecords').closest('tr').remove();
                        var display_url = '{{asset("attachment/secondary_document")}}/' + data.db_arr[2];
                        /*if (i == 1) {
                            row += '<td><a target="_blank" href="' + display_url + '">' + d + '</a></td>';
                        } else {*/
                            row += '<td>' + d + '</td>';
                        //}
                        //hide fields after add
                        $('.doc_field').hide();
                        $('.doc_file').hide();
                        // $('.doc_visibility').hide();
                        $('#edit_select_document_type').val('');
                        $('#edit_select_document_type').selectpicker('refresh');
                        $('#edit_result_file').val('');
                        $('#edit_document_name').val('');
                        $('#doc_input_id').val('');
                        $('#remove_file').val('0');
                        $('#preview').css('display','none');
                        $('#remove_icon').css('display','none');
                        $('#edit_doc_input_id').val('');
                    });
                    var display_url = '{{asset("attachment/secondary_document")}}/' + data.db_arr[2];
                    var attachment_url = '{{url("customer/removeattachment")}}';
                    row += "<td><a class='btn btn-secondary item red-tooltip remove-attachment' data-toggle='modal' data-url='" + attachment_url + "'  data-file='" + data.db_arr[2] + "' data-target='#attachment-confirmation-modal' data-placement='bottom' title='Remove'><span class='icon-cross-remove-sign'></span></a><a class='btn btn-secondary item red-tooltip edit-attachment' data-file='" + data.db_arr[2] + "' data-type='" + data.db_arr[0] + "'data-title='" + data.db_arr[1] +"'data-file-original-name='" + data.db_arr[3] + "'data-file-extension='" + data.ajax_arr[4]+"'><span class='icon-pencil-edit-button'></span></a>";
                    if(data.db_arr[2].length>0){
                                    row += "<a target='_blank' href='" + display_url + "' class='btn btn-secondary item red-tooltip'><i class='fa fa-eye'></i></a>";
                                    }
                                    row += '</td></tr>';
                    $('#edit_document_table tbody').append(row);
                }
            });
        }
    }else{
        e.preventDefault();
        var extension = $('#edit_result_file').val().split('.').pop().toLowerCase();
        if ($('#edit_result_file').val() != "" && $.inArray(extension, ['csv', 'xls', 'xlsx', 'pdf', 'doc', 'docx', 'jpg', 'jpeg']) == -1) {
            $('#errormessage').html('Please Select Valid File... ');
            $('#errormessage').css('display', 'block');
        } else {
            var file_data = $('#edit_result_file').prop('files')[0];
            var selected_document_type = $('#edit_select_document_type').val();
            var doc_input = $('#edit_doc_input_id').val();
            var doc_id = $('#edit_doc_id').val();
            var doc_visibile = $("input[name='visibility']:checked").val();
            var secondary_document_id = $('#edit_secondary_doc_id').val();
            var remove_file = $('#remove_file').val();
            var edit_document_name = $('#edit_document_name').val();
            var edit_document_original_name = $('#edit_document_name').attr('data-file-original-name');
            var edit_document_extension = $('#edit_document_name').attr('data-file-extension');
            var result;
            var row_id = $('#row_id').val();//alert(row_id);
             //console.log(result_arr);
                for( var i = 0, len = result_arr.length; i < len; i++ ) {
                    // console.log(len);console.log(result_arr[i]);
                    for( var j = 0, len1 = result_arr[i].length; j < len1; j++ ){
                        //  console.log(result_arr[j]);
                        if( result_arr[i][j] === row_id) {
                            result = i;
                            break;
                        }
                    }
                }
                result_arr.splice(result, 1);
                if(result_arr.length==1){
                    result_arr.splice(0, 1);
                }
            $('#edit_secondary_doc_attachment').val(result_arr);
            if(doc_id==''){
                    var doc_id = 0;
                }
            var form_data = new FormData();

            if(file_data)
                {
                    form_data.append('file', file_data);
                }else{
                            form_data.append('file_name',edit_document_name);
                            form_data.append('file_original_name',edit_document_original_name);
                            form_data.append('extension',edit_document_extension);
                }
            form_data.append('document_type', selected_document_type);
            form_data.append('doc_value', doc_input);
            form_data.append('visibility', doc_visibile);
            form_data.append('doc_id', doc_id);
            form_data.append('secondary_document_id',secondary_document_id);
            form_data.append('remove_file',remove_file);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': $('meta[name=_token]').attr('content')
                }
            });
            $.ajax({
                url: "{{url('customer/secondary-document/edit/attachments')}}", // point to server-side PHP script
                data: form_data,
                type: 'POST',
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                processData: false,
                success: function (response) {
                    var data = jQuery.parseJSON(response);
                   //result_arr.push(data.db_arr);
                    //console.log(result_arr);
                   // result_arr.push('|');
                    //   console.log(result_arr);
                    //$('#edit_secondary_doc_attachment').val(result_arr);
                     var del_var = $('#row_id').val();
                        //console.log(data.db_arr);
                        $("#" + del_var).remove();
                    if(data.db_arr[2] != null){
                            var f1 = data.db_arr[2];//console.log(f1);
                            var f2 = f1.split('.')[0];

                        }

                        if(data.ajax_arr[2] == null){
                            data.db_arr[2] = '';
                        }
                    var f1 = data.db_arr[2];
                    var f2 = f1.split('.')[0];
                    var row = '<tr id="refreshdelete' + data.db_arr[4] + '">';;
                  
                    $.each((data.ajax_arr), function (i, d) {
                        $('.norecords').closest('tr').remove();
                        var display_url = '{{asset("attachment/secondary_document")}}/' + data.db_arr[2];
                       /* if (i == 1) {
                            row += '<td><a target="_blank" href="' + display_url + '">' + d + '</a></td>';
                        } else {*/
                            if(d==null){
                                        row += '<td>' + " " + '</td>';
                                        }else{
                                        row += '<td>' + d + '</td>';
                                    }
                       // }
                         $('.doc_field').hide();
                        $('.doc_file').hide();
                        // $('.doc_visibility').hide();
                        $('#edit_select_document_type').val('');
                        $('#edit_select_document_type').selectpicker('refresh');
                        $('#edit_result_file').val('');
                        $('#edit_document_name').val('');
                        $('#doc_input_id').val('');

                        $('#remove_file').val('0');
                        $('#preview').css('display','none');
                        $('#remove_icon').css('display','none');
                        if($('#submit_document span').html()=="Update"){
                                        $('#submit_document span').html('Add');
                                    }
                        $('#edit_doc_input_id').val('');
                    });
                    var display_url = '{{asset("attachment/secondary_document")}}/' + data.db_arr[2];
                    var attachment_url = '{{url("customer/edit/removeattachment")}}';
                    row += "<td><a class='btn btn-secondary item red-tooltip remove-attachment' data-toggle='modal' data-url='" + attachment_url+"'data-id='" + data.db_arr[4] +"'data-file='" + data.db_arr[2] + "' data-target='#attachment-confirmation-modal' data-placement='bottom' title='Remove'><span class='icon-cross-remove-sign'></span></a><a class='btn btn-secondary item red-tooltip edit-attachment' data-file='" + data.db_arr[2] + "' data-type='" + data.db_arr[0] + "'data-title='" + data.db_arr[1] +"'data-id='" + data.db_arr[4]+ "'data-file-original-name='" + data.db_arr[3] + "'data-file-extension='" + data.db_arr[6]+"'><span class='icon-pencil-edit-button'></span></a>";
                     if(data.db_arr[2].length>0){
                        row += "<a target='_blank' href='" + display_url + "' class='btn btn-secondary item red-tooltip'><i class='fa fa-eye'></i></a>";
                        }
                        row += '</td></tr>';
                    $('#edit_document_table tbody').append(row);
                }
            });
        }
    }
    });

//Add recipient contact dynamic
    $('#add_contact').click(function () {
        var all_val = 1;


        if ($('#name').val() != "") {
            $('input[name=contact_name]').val($('#name').val());
            // $('input[name=contact_name]').addClass('not-empty');
            $('input[name=contact_no]').val($('#contact').val());
            $('input[name=contact_email]').val($('#email').val());
            $('input[name=contact_address]').val($('#address').val());
            $('input[name=contact_zip]').val($('#zip').val());
            $("#contact_state_id").val($('#state').val());
            $("#contact_state_id").selectpicker('refresh');
            $("#contact_city_id").val($('#city').val());
            $('#autocomplete_city_id').val($('#recipient_city_id').val());
            $("#contact_city_id").selectpicker('refresh');
            $('input').addClass('not-empty');
            all_val = 0;
        } else {
            $('input[name=contact_name]').val('');
        }
        if (all_val) {
            // $('input[name=contact_name]').val('');
            $('input[name=contact_no]').val('');
            $('input[name=contact_email]').val('');
            $('input[name=contact_address]').val('');
            $('input[name=contact_zip]').val('');
            $("#contact_state_id").val('');
            $("#contact_state_id").selectpicker('refresh');
            $("#contact_city_id").val('');
            $("#contact_city_id").selectpicker('refresh');
        }
        $('#name-error').html('');
        $('#address-error').html('');
        $('#city-error').html('');
        $('#state-error').html('');
        $('#zip-error').html('');
        $('#success-msg').css('display', 'none');

//        $('#contactModal').modal('show');
    });
    $(".business_phone_validation").mask("(999) 999-9999");
    $(".email_validation").attr("data-parsley-trigger", "change focusout")
            .attr("data-parsley-type", "email")
            .parsley();

    $(".zip_validation").attr("data-parsley-trigger", "change focusout")
            .attr("data-parsley-type", "digits")
            .attr("data-parsley-minlength", "5")
            .attr("data-parsley-maxlength", "5")
            .parsley();
    $('body').on('click', '#submit_contact', function () {
        var recipientForm = $('#add_recipient_contact');
//    var formData = recipientForm.serialize();

        $('#name-error').html("");
        $('#contact-error').html("");
        $('#email-error').html("");
        $('#address-error').html("");
        $('#city-error').html("");
        $('#state-error').html("");
        $('#zip-error').html("");
//  $('#country-error').html("");
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name=_token]').attr('content')
            }
        });
        $.ajax({
            url: '{{url("customer/contact-recipient/store")}}',
            type: 'POST',
            data: {
                doc_id: $('#doc_id_recipient').val(),
                contact_name: $('#contact_name').val(),
                contact_no: $('#contact_no').val(),
                contact_email: $('#contact_email').val(),
                contact_address: $('#contact_address').val(),
                contact_state_id: $('#contact_state_id').val(),
                contact_zip: $('#contact_zip').val(),
                contact_city_id: $('#autocomplete_city_id').val(),
                attn: $('#attn').val()
                        //  contact_country: $('#contact_country').val(),
            },
            success: function (data) {
                if (data.error_msg) {
                    $('#error-msg').css('display', 'block');
                    $('#error-msg').html(data.error_msg);
                    $('#success-msg').css('display', 'none');
                    $('#success-msg').html('');
                }
                if (data.errors) {
                    if (data.errors.contact_name) {
                        $('#name-error').html(data.errors.contact_name[0]);
                    }
                    if (data.errors.contact_address) {
                        $('#address-error').html(data.errors.contact_address[0]);
                    }
                    if (data.errors.contact_city_id) {
                        $('#city-error').html(data.errors.contact_city_id[0]);
                    }
                    if (data.errors.contact_state_id) {
                        $('#state-error').html(data.errors.contact_state_id[0]);
                    }
//                    if (data.errors.contact_country) {
//                        $('#country-error').html(data.errors.contact_country[0]);
//                    }
                    if (data.errors.contact_zip) {
                        $('#zip-error').html(data.errors.contact_zip[0]);
                    }
                    if (data.errors.contact_no) {
                        $('#contact-error').html(data.errors.contact_no[0]);
                    }
                    $('#error-msg').css('display', 'none');
                    $('#error-msg').html(data.errors);
                    $('#success-msg').css('display', 'none');
                    $('#success-msg').html('');

                }
                if (data.success) {

                    $('#success-msg').css('display', 'block');
                    $('#success-msg').html(data.success);
                    $('#error-msg').css('display', 'none');
                    $('#error-msg').html('');

                    // console.log($('#doc_id_recipient').val());
                    $.ajax({
                        data: {"doc_id": $('#doc_id_recipient').val()},
                        type: 'POST',

                        url: "{{url('customer/getAllContacts')}}", // point to server-side PHP script
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },

                        success: function (response) {
                            $("#name").empty("");
                            $("#name").selectpicker("refresh");
                            $.each(response.contacts, function (i, obj)
                            {
                                var div_data = "<option value='" + obj.company_name + "'>" + obj.company_name + "</option>";

                                $(div_data).appendTo('#name');
                            });
                            $("#name").trigger('change');
                        }
                    });
                    $('#name').val(data.contact_data.company_name);

                    $('#contact').val(data.contact_data.phone);
                    $('#email').val(data.contact_data.email);
                    $('#address').val(data.contact_data.mailing_address);
                    $('#city').val(data.contact_data.city_name);
                    $('#recipient_city_id').val(data.contact_data.city_id);
                    $('#state').val(data.contact_data.state_id);
                    $('#state').selectpicker('refresh');
                    $('#zip').val(data.contact_data.zip);
//                    $('#country').val(data.contact_data.country_id);
//                    $('#country').selectpicker('refresh');
                }
                $('input').each(function () {
                    if ($(this).val().length > 0) {
                        $(this).addClass('not-empty');
                    } else {
                        $(this).removeClass('not-empty');
                    }
                });
            },
        });
    });

    /**************Autocomplete County********************/
    /*county */
    src_county = "{{ url('customer/city/autocompletecountyname') }}";

    $("#parent_country").autocomplete({

        source: function (request, response) {
            $.ajax({
                url: src_county,
                dataType: "json",
                data: {
                    term: request.term
                },
                beforeSend: function () {
                    $('#countyloader').show();
                },
                complete: function () {
                    $('#countyloader').hide();
                },
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            value: item.value,
                            id: item.id     // EDIT
                        }
                    }));

                },

            });
        },
        select: function (event, ui) {

            $("#county_id").val(ui.item.id);
            $("#parent_country").val(ui.item.value);
            $('.county_select_validation').hide();
        },
        change: function (event, ui) {
            if (ui.item == null) {
                $(this).val("");
                $(this).focusout();
                $(this).focus();
                $(this).after('<ul class="parsley-errors-list filled county_select_validation"><li class="parsley-required">Select County from the list</li></ul>');

            }
        }
    });

    src = "{{ url('customer/city/autocompletecityname') }}";

    $("#city").autocomplete({

        source: function (request, response) {
            $.ajax({
                url: src,
                dataType: "json",
                data: {
                    term: request.term
                },
                beforeSend: function () {
                    $('#recipientloader').show();
                },
                complete: function () {
                    $('#recipientloader').hide();
                },
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            value: item.value,
                            id: item.id     // EDIT
                        }
                    }));

                },

            });
        },
        select: function (event, ui) {

            $("#recipient_city_id").val(ui.item.id);
            $("#city").val(ui.item.value);
        },
        change: function (event, ui) {
            if (ui.item == null) {
                $(this).val("");
                $(this).focusout();
                $(this).focus();
            }
        }
    });


    $("#contact_city_id").autocomplete({

        source: function (request, response) {
            $.ajax({
                url: src,
                dataType: "json",
                data: {
                    term: request.term
                },
                beforeSend: function () {
                    $('#recipientpopuploader').show();
                },
                complete: function () {
                    $('#recipientpopuploader').hide();
                },
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            value: item.value,
                            id: item.id     // EDIT
                        }
                    }));

                },

            });
        },
        select: function (event, ui) {

            $("#autocomplete_city_id").val(ui.item.id);
            $("#contact_city_id").val(ui.item.value);
        },
        change: function (event, ui) {
            if (ui.item == null) {
                $(this).val("");
                $(this).focusout();
                $(this).focus();
            }
        }
    });
    /*************Autocomplete County*****************/
});

/*******Generate pdf**********/
$(document).ready(function(){
    $(".generate_pdf").click(function(e){
        if ($('#edit_secondary_doc_project').parsley().validate()) {
            var secondary_doc_id =$("#edit_secondary_doc_id").val();
            var url1="{{url('customer/secondary-document/printSecondaryDocument')}}" + '/' + secondary_doc_id + '/pdf';
            window.open(url1,"_blank");
            /* $.ajax({
                method: 'GET', // Type of response and matches what we said in the route
                url: "{{url('customer/secondary-document')}}", // This is the url we gave in the route
               // data: {'id' : id}, // a JSON object to send back
                    success: function(response){ // What to do if we succeed
                        
                        window.open(url1,"_blank");
                    }
                }); */
        }
    });
});



//End to fetch data of related work order
$(document).on('click', '.remove-attachment', function () {
    var url = $(this).attr('data-url');
    var file = $(this).attr('data-file');
    var id = $(this).attr('data-id');
    var row_id = $(this).closest('tr').attr('id');
    $(".remove-attachment-model").attr("action", url);
    $("input[name='file']").remove();
    $("input[name='id']").remove();
    $("input[name='row_id']").remove();
    $('body').find('.remove-attachment-model').append('<input name="file" type="hidden" value="' + file + '">');
    $('body').find('.remove-attachment-model').append('<input name="id" type="hidden" value="' + id + '">');
    $('body').find('.remove-attachment-model').append('<input name="_method" type="hidden" value="POST">');
    $('body').find('.remove-attachment-model').append('<input name="row_id" type="hidden" value="' + row_id + '">');
});
//For account manager login parent work according to customer
$(document).on('click', '.remove-parent-attachment', function () {
    var file = $(this).attr('data-file');
    var url = $(this).attr('data-url');
    $(".remove-attachment-model").attr("action", url);
    $("input[name='file']").remove();
    $('body').find('.remove-attachment-model').append('<input name="file" type="hidden" value="' + file + '">');
    $('body').find('.remove-attachment-model').append('<input name="_method" type="hidden" value="POST">');
});
//Edit Attachement Start
    function openTab(th)
    {
        window.open(th.name,'_blank');
    }

    $('#remove_icon').click(function(){
        $('#preview').css('display','none');
        $('#remove_icon').css('display','none');
        $('#remove_file').val('1');
        $('#edit_result_file').val('');
    });

    $('#edit_result_file').change(function(e){
        var fileName = e.target.files[0].name;
        doc_icon = fileName.split('.')[1];
        if(doc_icon == 'pdf'){
            $('#preview').html('<i class="fa fa-file-pdf-o" style="font-size:36px"></i>');
        }else if(doc_icon == 'doc' || doc_icon == 'docx'){
            $('#preview').html('<i class="fa fa-file-word-o" style="font-size:36px"></i>');
        }else if(doc_icon == 'xls' || doc_icon == 'xlsx'){
            $('#preview').html('<i class="fa fa-file-excel-o" style="font-size:36px"></i>');
        }else if(doc_icon == 'jpg' || doc_icon == 'jpeg'){
            $('#preview').html('<i class="fa fa-file-image-o" style="font-size:36px"></i>');
        }else{
            $('#preview').html('<i class="fa fa-file" style="font-size:36px"></i>');
        }
        // $('#preview').html(fileName);
        $('#preview').css('display','block');
        $('#remove_icon').css('display','block');
        //alert('The file "' + fileName +  '" has been selected.');
    });

    $(document).on('click', '.edit-attachment', function () {
        var row_id = $(this).closest('tr').attr('id');
        // $('#submit_document').removeClass('old_class').addClass('new_class');
        var type = $(this).attr('data-type');
        var file = $(this).attr('data-file');
        var title = $(this).attr('data-title');
        var id = $(this).attr('data-id');
        $('#edit_document_name').val(file);
        $('#edit_document_name').attr('data-file-original-name',$(this).attr('data-file-original-name'));
        $('#edit_document_name').attr('data-file-extension',$(this).attr('data-file-extension'));
        $('#edit_doc_input_id').val(title);
        if (title != ''){$('#edit_doc_input_id').addClass('not-empty');}
        $('#edit_doc_id').val(id);
        $('#row_id').val(row_id);
        $('#preview').css('display','block');
        $('#remove_icon').css('display','block');

        //console.log(file);
        var APP_URL = {!! json_encode(url('/')) !!}
            // console.log(APP_URL);
            $("#open_doc").attr('name',APP_URL+'/attachment/work_order_document/'+file);
        doc_icon = file.split('.')[1];
        if(doc_icon == 'pdf'){
            $('#preview').html('<i class="fa fa-file-pdf-o" style="font-size:36px"></i>');
        }else if(doc_icon == 'doc' || doc_icon == 'docx'){
            $('#preview').html('<i class="fa fa-file-word-o" style="font-size:36px"></i>');
        }else if(doc_icon == 'xls' || doc_icon == 'xlsx'){
            $('#preview').html('<i class="fa fa-file-excel-o" style="font-size:36px"></i>');
        }else if(doc_icon == 'jpg' || doc_icon == 'jpeg'){
            $('#preview').html('<i class="fa fa-file-image-o" style="font-size:36px"></i>');
        }else if (doc_icon == 'csv'){
            $('#preview').html('<i class="fa fa-file" style="font-size:36px"></i>');
        }else{
            $('#preview').html('');
            $('#preview').css('display','none');
            $('#remove_icon').css('display','none');
        }

        //$('#edit_result_file').val(APP_URL+'/attachment/work_order_document/'+file);
        var id1 = file.split('.')[0];

        var id = $(this).attr('data-id');
        $('.doc_field').show();
        $('.doc_file').show();
        //$('.doc_visibility').show();
        $('.update_doc').text('Update');
        $('#edit_select_document_type').val(type);
        $('#edit_select_document_type').selectpicker('refresh');
        /* $('#errormessage').text(file);
         $('#errormessage').css('display', 'block');*/
        var selected_doc_type = type;
        //$('#edit_doc_input_id').val("");
        if (selected_doc_type == 1) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('.doc_visibility').show();
            $('#doc_field').text('Bond Number');
        } else if (selected_doc_type == 2) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('.doc_visibility').show();
            $('#doc_field').text('NOC Book/Page');
        } else if (selected_doc_type == 3) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('.doc_visibility').show();
            $('#doc_field').text('Permit No');
        } else if (selected_doc_type == 5) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('.doc_visibility').show();
            $('#doc_field').text('Invoice No.');
        } else if (selected_doc_type == 4 || selected_doc_type == 6 ||
            selected_doc_type == 7 || selected_doc_type == 8 ||
            selected_doc_type == 9) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('.doc_visibility').show();
            $('#doc_field').text('Description');
        } else if (selected_doc_type == 10) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('.doc_visibility').show();
            $('#doc_field').text('Folio No.');
        } else if (!selected_doc_type) {
            $('.doc_field').hide();
            $('.doc_file').hide();
            $('.doc_visibility').hide();
        }
        /* if($('#submit_document span').html()=="Update"){
                                        // var file = data.db_arr[2];
                                         var id = file.split('.')[0];
                                         remove_arr.push(file);
                                         $('#remove_doc').val(remove_arr);
                                         var del_var = "refreshdelete" + id;
                      console.log(del_var);
                                         $("#" + del_var).remove();
                                     }*/
        /*$(".remove-attachment-model").attr("action", url);
        $("input[name='file']").remove();
        $('body').find('.remove-attachment-model').append('<input name="file" type="hidden" value="' + file + '">');
        $('body').find('.remove-attachment-model').append('<input name="id" type="hidden" value="' + id + '">');
        $('body').find('.remove-attachment-model').append('<input name="_method" type="hidden" value="POST">');*/
        /*$.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name=_token]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url: '{{url("customer/edit/work-ordwer/removeattachment")}}',
            data: {'file':file,'id':id},
            success: function (response) {
                if (response.success) {
                    //$('#attachment-confirmation-modal').modal('hide');
                    remove_arr.push(file);
                    $('#remove_doc').val(remove_arr);
                    var del_var = "refreshdelete" + id1;

                    $("#" + del_var).remove();
                }
            }
        });*/
    });
var remove_arr = [];

$('#remove-attachment-model').submit(function (event) {
    event.preventDefault();
    var file = $('input[name=file]').val();
    var id = file.split('.')[0];
    var row_id = $('input[name=row_id]').val();

    $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name=_token]').attr('content')
        }
    });
    var submit_url = $(this).attr("action");
    $.ajax({
        type: 'POST',
        url: submit_url,
        data: $('#remove-attachment-model').serialize(),
        success: function (response) {
            console.log(response);
            if (response.success) {
                $('#attachment-confirmation-modal').modal('hide');
                remove_arr.push(row_id);
                $('#remove_doc').val(remove_arr);
                var del_var = "refreshdelete" + id;

                $("#" + row_id).remove();
            }
        }
    });
});
$('.edit_recipient').on('click', function () {
    var recipient_id = $(this).attr('data-id');
    $('.msg-success').hide();
    $('.update_recipient').prop('disabled', false);
    $('.add_recipient').prop('disabled', true);
    $('.update_contact').prop('disabled', false);
    $('.add_contact_details').prop('disabled', true);
    $.ajax({
        data: {"id": recipient_id},
        type: 'POST',
        url: "{{url('customer/secondary-document/get_recipient_data')}}", // point to server-side PHP script
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            if (response.success) {
                $("#name").empty("");
                $("#name").selectpicker("refresh");
                $.each(response.contacts, function (i, obj)
                {
                    var div_data = "<option value='" + obj.company_name + "'>" + obj.company_name + "</option>";

                    $(div_data).appendTo('#name');
                });
                $("#name").trigger('change');
                //$("#name").selectpicker("refresh");
                $("#category_id").val(response.recipient.category_id);
                $('#category_id').selectpicker('refresh');
                $('#name').val(response.recipient.name);
                $('#name').selectpicker('refresh');
                $('#contact').val(response.recipient.contact);
                $('#email').val(response.recipient.email);
                $('#address').val(response.recipient.address);
                $('#city').val(response.recipient.city_name);
                $('#recipient_city_id').val(response.recipient.city_id);
                $('#state').val(response.recipient.state_id);
                $('#state').selectpicker('refresh');
                $('#zip').val(response.recipient.zip);
                $('#recipient_id').val(recipient_id);
                $('input').each(function () {
                    if ($(this).val().length > 0) {
                        $(this).addClass('not-empty');
                    } else {
                        $(this).removeClass('not-empty');
                    }
                });
            }
        }
    });
});
$('.update_recipient').on('click', function () {
    $('.msg-success').show();
});
$('.update_contact').on('click', function () {
    $.ajax({
        data: $('#update_recipient_data').serialize(),
        type: 'POST',
        url: "{{url('customer/contact/secondary-document/update_recipient_contact')}}", // point to server-side PHP script
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            if (response.success) {
//                $('.msg-success').show();
//                $('.msg-success').html('Contact successfully updated');

                alert('Contact successfully updated');
            }
        }
    });
});
$(document).on('click', '.remove-record', function () {

    var recipient_id = $(this).attr('data-id');
    var secondary_doc_id = $(this).attr('data-secondary-doc-id');
    var partial_final_type = $(this).attr('data-type');

    var url = "{{url('customer/secondary-recipient/delete')}}" + '/' + recipient_id + '/' + secondary_doc_id + '/' + partial_final_type;
   
});
/****************** Notes- Public-private email addresses ***********/
$('input[type=radio][name=visibility]').change(function() {
    var customer_id = $('#contact_customer_id').val();
    $('#note_email').val('');
    $.ajax({
                type: "POST",
                url: '{{url("account-manager/research/email-notes")}}',
                data: {visibility: this.value, customer_id: customer_id},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                    console.log(response[0]);
                    var options = '' ;
                    options +='<option value="">Select</option>';
                    for (var i = 0; i < response[0].length; i++) {
                     options +='<option val="'+response[0][i]+'">'+response[0][i]+'</option>';
                    }
                   $('#note_email').html(options);
                   $("#note_email").selectpicker("refresh");


                }
    });
});

                        </script>
                        @stop