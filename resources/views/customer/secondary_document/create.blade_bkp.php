@extends('adminlte::page')
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<div  class="tab-pane active" id="workorder" role="tabpanel">
    <div class="tab-content">
        <section class="work-order address-book-add-readonly">
            <div class="dashboard-wrapper">
                @if (Session::get('success'))
                <div class="alert alert-success">
                     <?php echo Session::get('success'); ?>
                </div>
                @endif
                <div class="dashboard-heading">
                    @if($partial_final_type == 0)
                    <h1><span>Request for Partial Release</span></h1>
                    @else
                    <h1><span>Request for Final Release</span></h1>
                    @endif
                </div>

                <div class="dashboard-inner-body">
                    <div class="notice-tabs">
                        <div class="full-width">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs display-ib pull-left" role="tablist">
                                @if($tab=="project")
                                <li role="presentation" id="project_tab" class="active"><a href="#project" aria-controls="project" role="tab" data-toggle="tab">Project</a></li>
                                <li role="presentation"><a href="#recepients" aria-controls="recepients" role="tab" data-toggle="tab">Recipients</a></li>

                                @else 

                                <li role="presentation"><a href="#project" aria-controls="project" role="tab" data-toggle="tab">Project</a></li>
                                <li role="presentation" class="active"><a href="#recepients" aria-controls="recepients" role="tab" data-toggle="tab">Recipients</a></li>

                                @endif
                                <!--<li role="presentation" id="address_tab" class="{{ ($tab == 'address_verify')?'active':''}}"><a href="#verify_address" aria-controls="verify_address" role="tab" data-toggle="tab" >Address Verification</a></li>-->

                            </ul>
                        </div>
                        <!--                        <div class="correction-div">
                                                    <div class="display-ib pull-right">
                                                        <a href="#" class="btn btn-default corr-btn" disabled="">Corrections</a>
                                                        <a href="#" class="btn btn-default notice-btn" disabled="">Notes</a>
                        
                                                    </div>
                                                </div>-->

                        <!-- Tab panes -->
                        <div class="tab-content">
                            @if($tab=="project")
                            <div role="tabpanel" class="tab-pane active" id="project">
                                @elseif($tab=='recipients')
                                <div role="tabpanel" class="tab-pane" id="project">
                                    @endif
                                    <div class="project-wrapper">
                                        <form id="secondary_doc_project" action="{{url('customer/partial-document')}}" enctype="multipart/form-data" method="post" date-parsley-validate="">

                                            <div class="dash-subform mt-0">
                                                <!--action="{{url('customer/partial-document')}}"-->
                                                <meta name="_token" content="{{ csrf_token() }}" />
                                                {!! csrf_field() !!}
                                                <div class="row">
                                                    <!--                                                <div class="col-md-6 col-sm-6">
                                                                                                        <div class="date-box full-width">
                                                                                                            <div class="input-wrapper full-width">
                                                                                                                <div class="styled-input">
                                                                                                                    <label>Date</label>
                                                                                                                    <input class="form-control" name="date" type="text" readonly="" value="{{date('Y-m-d')}}">
                                                                                                                    <span></span>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>-->
                                                    @if(Auth::user()->hasrole('account-manager')||Auth::user()->hasrole('admin'))

                                                    <div class="col-md-12 col-sm-12">
                                                        <div class="select-opt">
                                                            <label class="col-md-12 control-label" for="textinput">Customers<span class="mandatory-field">*</span></label>
                                                            <select name="customer_id" id="customer_id" class="selectpicker" data-parsley-required="true" data-parsley-required-message="Customer  is required" data-parsley-trigger="change focusout">
                                                                <option value="">Select</option>
                                                                @foreach($customers as $customer)
                                                                <option value="{{$customer->id}}" @if (old('customer_id') == $customer->id) selected="selected" @endif > {{ucfirst(trans($customer->name))}}</option>
                                                                @endforeach
                                                            </select>
                                                            @if ($errors->has('customer_id'))
                                                            <p class="help-block">
                                                                <strong>{{ $errors->first('customer_id') }}</strong>
                                                            </p>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    @endif


                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="form-group">
                                                            <div class="readonly full-width readentry">
                                                                <label class="full-width">Date</label>
                                                                <p class="full-width">
                                                                    {{date('m-d-Y')}}
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="input-wrapper full-width">
                                                            <div class="styled-input">
                                                                <input class="form-control" name="customer" value="{{ old('customer')}}"  type="text" data-parsley-required="true" data-parsley-required-message="Your customer is required" data-parsley-trigger="change focusout" id="customer">
                                                                <label>Your Customer<span class="mandatory-field">*</span></label>
                                                                <span></span>
                                                                @if ($errors->has('customer'))
                                                                <p class="help-block">
                                                                    <strong>{{ $errors->first('customer') }}</strong>
                                                                </p>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="select-opt">
                                                            <label class="col-md-12 control-label" for="textinput">Parent Work Order Number</label>
                                                            <select class="selectpicker" name="work_order_no" data-show-subtext="true" data-live-search="true" id="work_order_no"  data-parsley-required-message="Parent work order number is required" data-parsley-trigger="change focusout" >

                                                            </select>
                                                            @if($errors->has('work_order_no'))
                                                            <p class="help-block">
                                                                <strong>{{ $errors->first('work_order_no') }}</strong>
                                                            </p>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="input-wrapper full-width">
                                                            <div class="styled-input">
                                                                <input class="form-control" name="project_name" id="project_name" value="{{old('project_name')}}"  type="text"  data-parsley-required-message="Project name is required" data-parsley-trigger="change focusout">
                                                                <label>Project Name</label>
                                                                <span></span>
                                                                @if ($errors->has('project_name'))
                                                                <p class="help-block">
                                                                    <strong>{{ $errors->first('project_name') }}</strong>
                                                                </p>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 col-sm-12 clear-b">
                                                        <div class="input-wrapper full-width">
                                                            <div class="styled-input">
                                                                <textarea  class="form-control" name="project_address" id="project_address"  data-parsley-required="true" data-parsley-required-message="Project address is required" data-parsley-trigger="change focusout"  rows="5">{{old('project_address')}}</textarea>


                                                                <label>Project Address<span class="mandatory-field">*</span></label>
                                                                <span></span>
                                                                @if ($errors->has('project_address'))
                                                                <p class="help-block">
                                                                    <strong>{{ $errors->first('project_address') }}</strong>
                                                                </p>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>



                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="input-wrapper full-width">
                                                            <div class="styled-input">
                                                                <input class="form-control"  type="text" value="{{old('project_country')?old('project_country'):''}}" name="project_country" id="parent_country"  data-parsley-required="true" data-parsley-required-message="Select County from the list" data-parsley-trigger="change focusout" />
                                                                <input type="hidden" name="county_id" value="{{old('county_id')?old('county_id'):''}}" id="county_id"/>
                                                                <label class="col-md-12 control-label" for="textinput">County<span class="mandatory-field">*</span></label>  
                                                                <span></span> 
                                                                @if ($errors->has('project_country'))
                                                                <p class="help-block">
                                                                    <strong>{{ $errors->first('project_country') }}</strong>
                                                                </p>
                                                                @endif
                                                            </div>
                                                            <div id="countyloader" style="display: none">
                                                                <img width="150px" src="{{asset('images/loader.gif')}}"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="input-wrapper full-width">
                                                            <div class="styled-input">
                                                                <input class="form-control" name="amount" value="{{old('amount')}}"  type="text" data-parsley-required="true" data-parsley-required-message="Amount/Value of Payoff required" data-parsley-trigger="change focusout" id="amount" data-parsley-type="number">
                                                                <label>($)Amount/Value of Payoff<span class="mandatory-field">*</span></label>
                                                                <span></span>
                                                                @if($errors->has('amount'))
                                                                <span class="help-block">
                                                                    <strong>{{ $errors->first('amount') }}</strong>
                                                                </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 clear-b">
                                                        <div class="input-wrapper full-width">
                                                            <div class="styled-input">
                                                                <input class="form-control" name="amount_text" value="{{old('amount_text')}}"  type="text" data-parsley-required="true" data-parsley-required-message="Amount/Value of Payoff in text required" data-parsley-trigger="change focusout" >
                                                                <label>Amount/Value of Payoff in text<span class="mandatory-field">*</span></label>
                                                                <span></span>
                                                                @if($errors->has('amount_text'))
                                                                <span class="help-block">
                                                                    <strong>{{ $errors->first('amount_text') }}</strong>
                                                                </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @if($partial_final_type == 0)
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="input-wrapper full-width">
                                                            <div class="styled-input">
                                                                <input class="form-control datepicker" name="due_date" type="text" data-parsley-required="true" data-parsley-required-message="Due date is required" data-parsley-trigger="change focusout" id="due_date">
                                                                <label>Date:Amount Paid Prior to<span class="mandatory-field">*</span></label>
                                                                <span></span>
                                                                @if($errors->has('due_date'))
                                                                <span class="help-block">
                                                                    <strong>{{ $errors->first('due_date') }}</strong>
                                                                </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endif
                                                    <!--                                                        <div class="col-md-6 col-sm-6">
                                                                                                                <div class="input-wrapper full-width">
                                                                                                                    <div class="styled-input">
                                                                                                                        <input class="form-control" name="first_name" value="{{old('first_name')}}" required="" type="text" data-parsley-required="true" data-parsley-required-message="Your first name is required" data-parsley-trigger="change focusout" id="first_name">
                                                                                                                        <label>Your First Name</label>
                                                                                                                        <span></span>
                                                                                                                        @if($errors->has('first_name'))
                                                                                                                        <span class="help-block">
                                                                                                                            <strong>{{ $errors->first('first_name') }}</strong>
                                                                                                                        </span>
                                                                                                                        @endif
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>-->
                                                    <div class="col-md-6 col-sm-6">

                                                        <div class="select-opt">
                                                            <label class="col-md-12 control-label" for="textinput">Authorise Signature<span class="mandatory-field">*</span></label>
                                                            <select name="signature" class="selectpicker" id="officers_directors" data-parsley-required="true" data-parsley-required-message="Authorise signature is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true">
                                                                <option value="">Select</option>
                                                                @if(isset($officers_directors) && count($officers_directors)>0 )
                                                                @foreach($officers_directors as $officer)
                                                                <option value="{{$officer->first_name.' '.$officer->last_name}}" @if (old('signature') == $officer->first_name.' '.$officer->last_name) selected="selected" @endif > {{ucfirst(trans($officer->first_name.' '.$officer->last_name))}}</option>
                                                                @endforeach
                                                                @endif
                                                            </select>
                                                            @if ($errors->has('signature'))
                                                            <p class="help-block">
                                                                <strong>{{ $errors->first('signature') }}</strong>
                                                            </p>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 clear-b">

                                                        <div class="select-opt">
                                                            <label class="col-md-12 control-label" for="textinput">Title<span class="mandatory-field">*</span></label>
                                                            <select name="title" class="selectpicker"  id="officers_directors_title" data-parsley-required="true" data-parsley-required-message="Title is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true">
                                                                <option value="">Select</option>
                                                                @if(isset($officers_directors) && count($officers_directors)>0)
                                                                @foreach($officers_directors as $officer)
                                                                <option value="{{$officer->title}}" @if (old('title') == $officer->title) selected="selected" @endif > {{ucfirst(trans($officer->title))}}</option>
                                                                @endforeach
                                                                @endif
                                                            </select>
                                                            @if ($errors->has('title'))
                                                            <p class="help-block">
                                                                <strong>{{ $errors->first('title') }}</strong>
                                                            </p>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <!--                                                        <div class="col-md-6 col-sm-6">
                                                                                                                <div class="input-wrapper full-width">
                                                                                                                    <div class="styled-input">
                                                                                                                        <input class="form-control" name="legal_description" value="{{old('legal_description')}}"  type="text"  data-parsley-required-message="Legal description is required" data-parsley-trigger="change focusout">
                                                                                                                        <label>Project Legal Description<span class="mandatory-field">*</span></label>
                                                                                                                        <span></span>
                                                                                                                        @if($errors->has('legal_description'))
                                                                                                                        <span class="help-block">
                                                                                                                            <strong>{{ $errors->first('legal_description') }}</strong>
                                                                                                                        </span>
                                                                                                                        @endif
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>-->
                                                    <div class="col-md-6 col-sm-6 clear-b">
                                                        <div class="input-wrapper full-width">
                                                            <div class="styled-input">
                                                                <textarea class="form-control" name="service_labour_furnished" id="service_labour_furnished"   data-parsley-required-message="Serivce labor furnished is required" data-parsley-trigger="change focusout" rows="2">{{old('service_labour_furnished')}}</textarea>
                                                                <label>Service Labor Furnished<span class="mandatory-field">*</span></label>
                                                                <span></span>
                                                                @if($errors->has('service_labour_furnished'))
                                                                <span class="help-block">
                                                                    <strong>{{ $errors->first('service_labour_furnished') }}</strong>
                                                                </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 col-sm-12">
                                                        <div class="row">
                                                            <div class="col-md-6 col-sm-6">
                                                                <div class="input-wrapper full-width">
                                                                    <div class="form-group">
                                                                        <label class="col-md-12 control-label">Comment</label>
                                                                        <textarea class="form-control" name="comment"  type="text"  data-parsley-required-message="Comment is required" data-parsley-trigger="change focusout" id="comment"></textarea>
                                                                        @if ($errors->has('comment'))
                                                                        <span class="help-block">
                                                                            <strong>{{ $errors->first('comment') }}</strong>
                                                                        </span>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 col-sm-12">
                                                        <div class="row">
                                                            <div class="col-lg-6 col-md-6 col-sm-12">
                                                                <div class="select-opt mt-top">
                                                                    <label class="col-md-12 control-label" for="textinput">Document Type</label>  
                                                                    <select class="selectpicker" name="type" id="select_document_type">
                                                                        <option value="">-Select-</option>
                                                                        <option value="1">Bond</option>
                                                                        <option value="2">Notice to Commencement</option>
                                                                        <option value="3">Permit</option>
                                                                        <option value="4">Contract</option>
                                                                        <option value="5">Invoice</option>
                                                                        <option value="6">Final Release</option>
                                                                        <option value="7">Partial Release</option>
                                                                        <option value="8">Misc Document</option>
                                                                        <option value="9">Signed Document</option>
                                                                        <option value="10">Folio</option>
                                                                    </select>
                                                                    @if ($errors->has('type'))
                                                                    <span class="help-block">
                                                                        <strong>{{ $errors->first('type') }}</strong>
                                                                    </span>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12 col-sm-12 doc_field">
                                                                <div class="row">
                                                                    <div class="col-md-12 col-sm-12">
                                                                        <div class="input-wrapper full-width">
                                                                            <div class="styled-input">
                                                                                <input class="form-control" name="doc_input"  type="text" id="doc_input_id">
                                                                                <label id="doc_field"></label>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                <div class="form-btn-div doc_file">
                                                                    <span class="btn btn-default btn-file upload-btn btn btn-primary form-btn" id="temp">
                                                                        Choose File to Upload  <input type="file" name="file_name" id="result_file">
                                                                    </span>
                                                                    <span id="errormessage" style="display: none"></span>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12 col-sm-12">
                                                                <div class="row">
                                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                                        <div class="dahboard-table doc-table full-width table-responsive">
                                                                            <table class="table table-striped" id="document_table">
                                                                                <thead class="thead-dark">
                                                                                    <tr>
                                                                                        <th scope="col">Document Type</th>
                                                                                        <th scope="col">Name</th>
                                                                                        <th scope="col">Date & Time</th>
                                                                                        <th scope="col">Type of Attachment</th>
                                                                                        <th scope="col">Options</th>
                                                                                    </tr>
                                                                                </thead>
                                                                                <tbody>
                                                                                <td class="text-center norecords" colspan="6">No record found</td>
                                                                                </tbody>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-bottom-btn">
                                                        <input type="hidden" name="secondary_doc_attachment[]" id="secondary_doc_attachment"/>
                                                        <input type="hidden" name="remove_doc[]" id="remove_doc"/>
                                                        <input type="hidden" name="partial_final_type" value="{{$partial_final_type}}"/>
                                                        <a href="{{url('customer/secondary-document')}}" class="btn btn-primary custom-btn customb-btn"><span>Back</span></a>
                                                        <a href="{{url('home')}}" class="btn btn-primary custom-btn customb-btn"><span>Cancel</span></a>
                                                        <button type="submit" class="btn btn-primary custom-btn customc-btn" name="submit" value="save_for_later" id="save_for_later"><span>Save For Later</span></button>
                                                        <button type="submit" name="submit" class="btn btn-primary custom-btn customc-btn" value="continue"><span>Continue</span></button>
                                                    </div>
                                                </div>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                                @if($tab=="project")
                                <div role="tabpanel" class="tab-pane" id="recepients">
                                    @elseif($tab=='recipients')
                                    <div role="tabpanel" class="tab-pane active" id="recepients">
                                        @endif

                                        <div class="project-wrapper"> 
                                            <form method="post" action="{{ url('customer/secondary-recipients/'.$secondary_document_id) }}" data-parsley-validate="" >
                                                {!! csrf_field() !!}
                                                <div class="dash-subform mt-0">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="sub-section-title">
                                                                        <h2>Recipients</h2>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12">
                                                                    <div class="dahboard-table table-responsive">
                                                                        <table class="table table-striped">
                                                                            <thead class="thead-dark">
                                                                                <tr>
                                                                                    <th scope="col">Category</th>
                                                                                    <th scope="col">Recipients</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                @if(isset($count) && !empty($count))
                                                                                @foreach($count as $k => $count)
                                                                                <tr>
                                                                                    <td class="checkbox-holder">
                                                                                        <input type="checkbox" id="checkbox-3-{{$k}}" class="regular-checkbox">
                                                                                        <label for="checkbox-3-{{$k}}">{{$count->name}}</label>
                                                                                    </td>
                                                                                    <td>{{$count->total}}</td>
                                                                                </tr>
                                                                                @endforeach  
                                                                                @else
                                                                            <td class="text-center" colspan="3">No records found</td>

                                                                            @endif
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="sub-section-title">
                                                                        <h2>Selected Recipients</h2>
                                                                    </div>
                                                                </div>
        <!--                                                        <span>Note : - Customer will be able to edit the details of recipient selected</span>-->
                                                                <div class="col-md-12">
                                                                    <div class="dahboard-table table-responsive">
                                                                        <table class="table table-striped">
                                                                            <thead class="thead-dark">
                                                                                <tr>
                                                                                    <th scope="col">Role</th>
                                                                                    <th scope="col">Company Name</th>
<!--                                                                                        <th scope="col">Contact Person</th>-->
<!--                                                                                        <th scope="col">Phone Number</th>-->
                                                                                    <th scope="col">Address</th>
                                                                                    <th scope="col">City</th>
                                                                                    <th scope="col">State</th>
                                                                                    <th scope="col">Zip Code</th>
                                                                                    <th scope="col">Action</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                @if(isset($recipients) && !empty($recipients))
                                                                                @foreach($recipients  as $recipient)
                                                                                <tr>
                                                                                    <td class="cl-blue">{{$recipient->name}}</td>
<!--                                                                                        <td>{{$recipient->contact}}</td>-->
<!--                                                                                        <td>{{$recipient->mobile}}</td>-->
                                                                                    <td>{{$recipient->address}}</td>
                                                                                    <td>{{$recipient->city->name}}</td>

                                                                                    <td>{{$recipient->state->name}}</td>
                                                                                    <td>{{$recipient->zip}}</td>
                                                                                    <td><a href="{{url('customer/secondary-recipient/delete/'.$recipient->id.'/'.$recipient->secondary_document_id.'/'.$partial_final_type)}}"><span class="icon-cross-remove-sign"></span></a></td>
                                                                                </tr>
                                                                                @endforeach
                                                                                @else
                                                                            <td class="text-center" colspan="5">No records found</td>

                                                                            @endif
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="dash-subform">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="sub-section-title">
                                                                <h2>Recipients</h2>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <div class="row">
                                                                <div class="col-md-6 col-sm-6">
                                                                    <input type="text" name="partial_final_type" value={{$partial_final_type}} hidden="">
                                                                    <input type="text" name="secondary_document_id" id="sec_doc_id" value={{$secondary_document_id}} hidden="">
                                                                    <div class="select-opt">

                                                                        <label class="col-md-12 control-label" for="textinput">Type of Recipient<span class="mandatory-field">*</span></label>  
                                                                        <select class="selectpicker" name="category_id" data-parsley-required="true" data-parsley-required-message="Type of recipient is required" data-parsley-trigger="change focusout">
                                                                            <option value="">select</option>
                                                                            @foreach($categories as $category)
                                                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                                                            @endforeach

                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6">

                                                            <div class="select-opt">
                                                                <label class="col-md-12 control-label" for="textinput">Name<span class="mandatory-field">*</span></label>  
                                                                <select class="selectpicker"  name="name" id="recipient_name" data-parsley-required="true" data-parsley-required-message="Name is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true">
                                                                    <option value="">Select</option>
                                                                    @if(isset($names) && !empty($names))
                                                                    @foreach($names as $each_name)
                                                                    <option value="{{$each_name->company_name}}">{{$each_name->company_name}}</option>
                                                                    @endforeach
                                                                    @endif
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-bottom-btn pd-25">
                                                                @if($tab == 'project')
                                                                <button type="button" name="add_contact_submit" class="btn btn-primary custom-btn customc-btn" value="contacts" disabled=""><span>Add Address Book</span></button>
                                                                <button type="button" name="add_contact_submit" class="btn btn-primary custom-btn customc-btn" value="contacts" disabled=""><span>Update Address Book</span></button>
                                                                @elseif($tab == 'recipients')
                                                                <button type="button" name="add_contact_submit" class="btn btn-primary custom-btn customc-btn" value="contacts" id="add_contact" data-toggle="modal" data-target="#contactModal"><span>Add Address Book</span></button>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6">
                                                            <div class="input-wrapper full-width">
                                                                <div class="styled-input">
                                                                    <input class="form-control"  type="text" name="contact" id="contact"  data-parsley-required-message="Contact is required">
                                                                    <label>Contact</label>
                                                                    <span></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--                                                            <div class="col-md-6 col-sm-6 clear-b">
                                                                                                                        <div class="input-wrapper full-width">
                                                                                                                            <div class="styled-input">
                                                                                                                                <input class="form-control" type="text" name="mobile"  id="mobile" data-parsley-required-message="Mobile number is required" data-parsley-trigger="change focusout">
                                                                                                                                <label>Mobile Number</label>
                                                                                                                                <span></span>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>-->
                                                        <div class="col-md-6 col-sm-6">
                                                            <div class="input-wrapper full-width">
                                                                <div class="styled-input">
                                                                    <input class="form-control" type="text" name="email" id="email"  data-parsley-required-message="Email is required" data-parsley-trigger="change focusout">
                                                                    <label>Email</label>
                                                                    <span></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-sm-12">
                                                            <div class="input-wrapper full-width">
                                                                <div class="styled-input">
                                                                    <input class="form-control"  type="text" name="address" id="address" data-parsley-required="true" data-parsley-required-message="Address is required" data-parsley-trigger="change focusout">
                                                                    <label>Address<span class="mandatory-field">*</span></label>
                                                                    <span></span>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <div class="col-md-6 col-sm-6 clear-b">
                                                            <div class="input-wrapper full-width">
                                                                <div class="styled-input">
                                                                    <input class="form-control"  type="text" name="city_id" id="city" data-parsley-required="true" data-parsley-required-message="Select City from the list" data-parsley-trigger="change focusout">
                                                                    <input type="hidden" name="recipient_city_id" id="recipient_city_id"/>
                                                                    <label class="col-md-12 control-label" for="textinput">City<span class="mandatory-field">*</span></label> 
                                                                    <span></span>                                                                
                                                                </div>
                                                                <div id="recipientloader" style="display: none">
                                                                    <img width="150px" src="{{asset('images/loader.gif')}}"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <input type="text" name="state_id" value="" class="state" hidden="">
                                                        <div class="col-md-6 col-sm-6">
                                                            <div class="select-opt">
                                                                <label class="col-md-12 control-label" for="textinput">State* </label>  
                                                                <select class="selectpicker" id="state" name="state_id" data-parsley-required="true" data-parsley-required-message="State is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true">
                                                                    <option value="">Select</option>
                                                                    @foreach($states as $state)
                                                                    <option value="{{$state->id}}">{{$state->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 clear-b">
                                                            <div class="input-wrapper full-width">
                                                                <div class="styled-input">
                                                                    <input class="form-control" type="text" name="zip" id="zip" data-parsley-required="true" data-parsley-required-message="Zip Code is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true">
                                                                    <label>Zip Code<span class="mandatory-field">*</span></label>
                                                                    <span></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--                                                            <div class="col-md-6 col-sm-6">
                                                                                                                        <div class="select-opt">
                                                                                                                            <label class="col-md-12 control-label" for="textinput">County</label>  
                                                                                                                            <select class="selectpicker" id="country" name="country_id"  data-parsley-required-message="Country is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true">
                                                                                                                                <option value="">Select</option>
                                                                                                                                @foreach($countries as $country)
                                                                                                                                <option value="{{$country->id}}">{{$country->name}}</option>
                                                                                                                                @endforeach
                                                                                                                            </select>
                                                                                                                        </div>
                                                                                                                    </div>-->
                                                        <div class="col-md-12">
                                                            <div class="form-bottom-btn pd-25">


                                                                @if($tab=="project")
<!--                                                                    <input type="hidden" name="verified_addr" id="verified_addr" value="false">-->
                                                                <button type="submit" name="continue" class="btn btn-primary custom-btn customc-btn" disabled=""><span>Add Recipient</span></button>
                                                                <button  type="submit" name="continue"  class="btn btn-primary custom-btn customc-btn update_recipient" value="update" disabled=""><span>Update Recipient</span></button>

                                                                <div id="ajax_loader"></div>
                                                                @elseif($tab=='recipients')
<!--                                                                    <input type="hidden" name="verified_addr" id="verified_addr" value="false">-->
                                                                <input type="hidden" name="add_in_address_book" id="add_in_address_book">
                                                                <button  type="submit" name="continue"  class="btn btn-primary custom-btn customc-btn"><span>Add Recipient</span></button>
                                                                <button  type="submit" name="continue"  class="btn btn-primary custom-btn customc-btn update_recipient" value="update" disabled=""><span>Update Recipient</span></button>
                                                                <div id="ajax_loader"></div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-bottom-btn">
                                                            <a href="#" class="btn btn-primary custom-btn customb-btn" id="createBack"><span>Back</span></a>
                                                            <a href="{{url('home')}}" class="btn btn-primary custom-btn customb-btn"><span>Cancel</span></a>
                                                            <a href="#" class="btn btn-primary custom-btn customb-btn" disabled="" title="Please Fill all mandetory project information"><span>Save for Later</span></a>
                                                            <input type="button" id="createLabel" name="create_label_submit" class="btn btn-primary custom-btn customc-btn" value="Continue" disabled="" title="Please Fill all mandetory project information"/>
                                                        </div>
                                                    </div>

                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <!-- add new contacts-->

                                    <div id="contactModal" class="modal fade register-modal addcon-modal">


                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <div id="success-msg"class="alert alert-success" style="display: none"></div>
                                                    <h4 class="modal-title">Add Contact</h4>	
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                </div>
                                                <div class="modal-body">
                                                    <form method="POST" id="add_recipient_contact">
                                                        {{csrf_field()}}
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="input-wrapper full-width">
                                                                <div class="styled-input">

                                                                    <input class="form-control"  type="text" name="contact_name" value="{{old('contact_name')}}" id="contact_name">
                                                                    <label>Name<span class="mandatory-field">*</span></label>
                                                                    <span></span>
                                                                    <p class="help-block">
                                                                        <strong id="name-error"></strong>
                                                                        </span>  
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="input-wrapper full-width">
                                                                <div class="styled-input">
                                                                    <input class="form-control"  type="text" name="contact_no" value="{{old('contact_no')}}" id="contact_no">
                                                                    <label>Contact</label>
                                                                    <span></span>
                                                                    <p class="help-block">
                                                                        <strong id="contact-error"></strong>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="input-wrapper full-width">
                                                                <div class="styled-input">
                                                                    <input class="form-control"  type="email" name="contact_email" value="{{old('contact_email')}}" id="contact_email">
                                                                    <label>Email</label>
                                                                    <span></span>
                                                                    <p class="help-block">
                                                                        <strong id="email-error"></strong>
                                                                        </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="input-wrapper full-width">
                                                                <div class="styled-input"> 
                                                                    <input class="form-control"  type="text" name="contact_address" value="{{old('contact_address')}}" id="contact_address">
                                                                    <label>Address<span class="mandatory-field">*</span></label>
                                                                    <span></span>
                                                                    <p class="help-block">
                                                                        <strong id="address-error"></strong>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="input-wrapper full-width">
                                                                <div class="styled-input">
                                                                    <input class="form-control"  type="text" name="city_id" id="city" data-parsley-required="true" data-parsley-required-message="Select City from the list" data-parsley-trigger="change focusout">
                                                                    <input type="hidden" name="recipient_city_id" id="recipient_city_id"/>
                                                                    <label class="col-md-12 control-label" for="textinput">City<span class="mandatory-field">*</span></label> 
                                                                    <span></span>                                                                
                                                                </div>
                                                                <div id="recipientloader" style="display: none">
                                                                    <img width="150px" src="{{asset('images/loader.gif')}}"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="select-opt">
                                                                <label class="col-md-12 control-label">State<span class="mandatory-field">*</span></label>
                                                                <select class="selectpicker"  name="contact_state_id" data-parsley-required="true" data-parsley-required-message="State is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true" id="contact_state_id">
                                                                    <option value="">Select</option>
                                                                    @foreach($states as $state)
                                                                    <option value="{{$state->id}}" @if(old('contact_state_id')) selected="selected" @endif>{{$state->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                                <p class="help-block">
                                                                    <strong id="state-error"></strong>
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="input-wrapper full-width">
                                                                <div class="styled-input">

                                                                    <input class="form-control"  type="text" name="contact_zip" value="{{old('contact_zip')}}" id="contact_zip">
                                                                    <label>Zip Code<span class="mandatory-field">*</span></label>
                                                                    <p class="help-block">
                                                                        <strong id="zip-error"></strong>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="select-opt">
                                                                <label class="col-md-12 control-label">Country</label>
                                                                <select class="selectpicker" name="contact_country" data-show-subtext="true" data-live-search="true" id="contact_country">
                                                                    <option value="">Select</option>
                                                                    @foreach($countries as $country)
                                                                    <option value="{{$country->id}}" @if(old('contact_country')) selected="selected" @endif>{{$country->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                                <p class="help-block">
                                                                    <strong id="country-error"></strong>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>

                                                <div class="modal-footer">
                                                    <div class="form-bottom-btn">
                                                        <input type="hidden" name="edit_secondary_doc_id" value="{{$secondary_document_id}}" id="doc_id_recipient"/>
                                                        <button type="submit" class="btn btn-primary custom-btn customc-btn" id="submit_contact"><span>Add Contact</span></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>  

                                    </div>
                                </div>
                            </div></div>
                        </section>	
                    </div>
                </div>
                <div id="address_verification_modal" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Verified Addresses</h4>
                            </div>
                            <div class="modal-body">
                                <div id="alternate_addresses"></div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" id="choose_address">Use chosen address</button>
                            </div>
                        </div>
                    </div>
                </div>
                <form action="" method="POST" class="remove-attachment-model"  id="remove-attachment-model">
                    {!! csrf_field() !!}
                    <div id="attachment-confirmation-modal" class="modal fade">
                        <div class="modal-dialog modal-confirm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div class="icon-box">
                                        <i class="fa fa-close"></i>
                                    </div>				
                                    <h4 class="modal-title">Are you sure?</h4>	
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                </div>
                                <div class="modal-body">
                                    <p>Do you really want to delete these records? This process cannot be undone.</p>
                                </div>
                                <div class="modal-footer">
                                    <div class="form-bottom-btn">
                                        <button type="button" class="btn btn-primary custom-btn customb-btn remove-attachement-from-delete-form" data-dismiss="modal"><span>Cancel</span></button>
                                        <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>Confirm</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>


                @stop
                @section('frontend_js'))
                <script type="text/javascript" src="{{ asset('js/jquery-ui.min.js') }}"></script>

                <script src="{{ asset('vendor/adminlte/plugins/datepicker/bootstrap-datepicker.js') }}" type="text/javascript"></script>

                <script type="text/javascript">

$('#secondary_doc_note').parsley();
$('#secondary_doc_project').parsley();

//Start to submit note
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$('.notice-btn').click(function (e) {

    $('input[name=note]').val('');
    $('input[name=email]').val('');
    $('.note-success').empty();
    $('.note-success').css('display', 'none');
    $('.note-error').empty();
    $('.note-error').css('display', 'none');
});
$(".submitNote").click(function (e) {
    e.preventDefault();
    var note = $("input[name=note]").val();
    var email = $("input[name=email]").val();
    var secondary_doc_id = $('#secondary_doc_id').val();
    $.ajax({
        type: 'POST',
        url: '{{url("customer/secondary-document/store_note")}}',
        data: {note: note, email: email, doc_id: secondary_doc_id},
        success: function (data) {
            if (data.success) {
                $('.note-error').empty();
                $('.note-error').css('display', 'none');
                $('.note-success').show();
                $('.note-success').append('<p>' + data.success + '</p>');
                $('.correction_div').append('<li>' + data.correction.note + '<br>' + data.correction.email + '</li>');

            } else if (data.errors) {
                $.each(data.errors, function (key, value) {
                    $('.note-error').show();
                    $('.note-error').append('<p>' + value + '</p>');
                });
            }
        }
    });
});
//End to submit note
//back to project tab
$('#createBack').click(function () {
    $('.display-ib li').removeClass('active');
    $('#project_tab').addClass('active');
    $('.notice-tabs .tab-pane').removeClass('active');
    $('#project').addClass('active');
})
//Start to fetch data of related work order

$('#work_order_no').on('change', function () {
    $.ajax({
        data: {'work_order_no': $('#work_order_no').val()},
        type: 'post',
        url: "{{url('customer/secondary-document/work_order_details')}}",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            if ($('#work_order_no').val() == "") {
                location.reload();
            }
            if (response.success) {
                var stateid = "";
                $.each(response.success, function (key, val) {

                    //  console.log(val);
                    if (val.name == 'project_name') {
                        $('#project_name').val(val.value);
                    }
                    if (val.name == 'project_address') {
                        $('#project_address').val(val.value);

                    } else if (val.name == 'customer') {
                        $('#customer').val(val.value);
                    } else if (val.name == "zip")
                    {
                        $('#project_zip').val(val.value);
                    } else if (val.name == "amount_due")
                    {
                        $('#amount').val(val.value);
                    } else if (val.name == "due_date")
                    {
                        $('#due_date').val(val.value);
                    } else if (val.name == "signature")
                    {
                        $('#signature').val(val.value);
                    } else if (val.name == "first_name")
                    {
                        $('#first_name').val(val.value);
                    }

//                    if (val.name == 'state') {
//                        $('select[name="project_state"]').val(val.value);
//                        $('select[name="project_state"]').selectpicker('refresh');
//
//
//                    }
//                    if (val.name == 'city') {
//                        $('select[name="project_city"]').val(val.value);
//                    }
//                    if (val.name == 'zip') {
//                        $('input[name=project_zip]').val(val.value);
//                    }
                    if (val.name == 'date') {
                        $('input[name="due_date"]').val(val.value);

                    }
                    if (val.name == 'county') {
                        $('#parent_country').val(val.county_name);
                        $('#county_id').val(val.value);
//                        $('#parent_country').selectpicker("refresh");
                    }
                    if (val.name == 'labor_service_furnished') {

                        $('#service_labour_furnished').val(val.value);
                    }
                });
            }
            if (response.parent_attachment) {
                // console.log(response.parent_attachment);
                $('#document_table tbody').empty();
                $.each(response.parent_attachment, function (k, v) {

                    var f1 = v.db_arr[2];
                    var f2 = f1.split('.')[0];
                    var row = '<tr id="refreshdelete' + f2 + '">';
                    $.each((v.ajax_arr), function (i, d) {
                        $('.norecords').closest('tr').remove();
                        var display_url = '{{asset("attachment/secondary_document/parent_document")}}/' + v.db_arr[2];
                        if (i == 1) {
                            row += '<td><a target="_blank" href="' + display_url + '">' + d + '</a></td>';
                        } else {
                            row += '<td>' + d + '</td>';
                        }
                    });
                    var attachment_url = '{{url("customer/parent_document/removeattachment")}}';
                    row += "<td><a class='btn btn-secondary item red-tooltip remove-parent-attachment' data-toggle='modal' data-url='" + attachment_url + "'  data-file='" + v.db_arr[2] + "' data-target='#attachment-confirmation-modal' data-placement='bottom' title='Remove'><span class='icon-cross-remove-sign'></span></a></td>";
                    row += '</tr>';
                    $('#document_table tbody').append(row);

                });

            }
            $('input').each(function () {
                if ($(this).val().length > 0) {
                    $(this).addClass('not-empty');
                } else {
                    $(this).removeClass('not-empty');
                }
            });
            $('textarea').each(function () {
                if ($(this).val().length > 0) {
                    $(this).addClass('not-empty');
                } else {
                    $(this).removeClass('not-empty');
                }
            });

        }
    });
});
//End to fetch data of related work order



$(document).ready(function () {


    $('textarea').each(function () {
        if ($(this).val().length > 0) {
            $(this).addClass('not-empty');
        } else {
            $(this).removeClass('not-empty');
        }
    });
    $('textarea').keyup(function () {
        if ($(this).val().length > 0) {
            $(this).addClass('not-empty');
        } else {
            $(this).removeClass('not-empty');
        }
    });
//address varificvation tab
    $('#createLabel').click(function () {
        $('.display-ib li').removeClass('active');
        $('#address_tab').addClass('active');
        $('.notice-tabs .tab-pane').removeClass('active');
        $('#verify_address').addClass('active');
    })
//save for later validation detroy
    $("#save_for_later").on('click', function () {
        $('#secondary_doc_project').parsley().destroy();
    });

//Start for document section
    $('.doc_field').hide();
    $('.doc_file').hide();
    $('.datepicker').datepicker({
        //  autoclose: true
    });
    $('#select_document_type').change(function () {

        var selected_doc_type = $(this).val();
        $('#doc_input_id').val("");
        if (selected_doc_type == 1) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('#doc_field').text('Bond Number');
        } else if (selected_doc_type == 2) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('#doc_field').text('NOC Book/Page');
        } else if (selected_doc_type == 3) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('#doc_field').text('Permit No');
        } else if (selected_doc_type == 5) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('#doc_field').text('Invoice No.');
        } else if (selected_doc_type == 4 || selected_doc_type == 6 ||
                selected_doc_type == 7 || selected_doc_type == 8
                || selected_doc_type == 9) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('#doc_field').text('Description');
        } else if (selected_doc_type == 10) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('#doc_field').text('Folio No.');
        } else if (!selected_doc_type) {
            $('.doc_field').hide();
            $('.doc_file').hide();
        }

    });
//End of document section
    $('#name').on('change', function () {
        var name = $('#name').val();
        var email = $('#email').val();
        var mobile = $('#mobile').val();
        var contact = $('#contact').val();
        var address = $('#address').val();
        var fax = $('#fax').val();
        var city = $('.city').val();
        var state = $('.state').val();
        $.ajax({
            data: {'name': name, },
            type: "post",
            url: "{{ url('customer/get-contacts') }}",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (res) {
                // $('').empty();
                $.each(res, function (key, value)
                {

                    $('#name').val(value.company_name);
                    $('#email').val(value.email);
                    $('#mobile').val(value.mobile);
                    $('#contact').val(value.phone);
                    $('#zip').val(value.zip);
                    $('#address').val(value.mailing_address);
                    $('#fax').val(value.fax);
                    $('#state').val(value.state_id);
                    $('#state').selectpicker('refresh');
                    $('select[name="city_id"]').val(value.city_id);
                    $('#city').selectpicker('refresh');
                    $('#country').val(value.country_id);
                    $('#country').selectpicker('refresh');
                });
                $('input').each(function () {
                    if ($(this).val().length > 0) {
                        $(this).addClass('not-empty');
                    } else {
                        $(this).removeClass('not-empty');
                    }
                });
            },
        });

    });
//Start to upload document
    var files;

    var result_arr = [];
    $("#result_file").on("change", function (e) {
        e.preventDefault();
        var extension = $('#result_file').val().split('.').pop().toLowerCase();

        if ($.inArray(extension, ['csv', 'xls', 'xlsx', 'pdf', 'doc', 'docx', 'jpg', 'jpeg']) == -1) {
            $('#errormessage').html('Please Select Valid File... ');
            $('#errormessage').css('display', 'block');
        } else {
            var file_data = $('#result_file').prop('files')[0];
            var selected_document_type = $('#select_document_type').val();
            var doc_input = $('#doc_input_id').val();
            var form_data = new FormData();

            form_data.append('file', file_data);
            form_data.append('document_type', selected_document_type);
            form_data.append('doc_value', doc_input)
            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': $('meta[name=_token]').attr('content')
                }
            });
            $.ajax({
                url: "{{url('customer/secondary-document/attachments')}}", // point to server-side PHP script
                data: form_data,
                type: 'POST',
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                processData: false,
                success: function (response) {
                    var data = jQuery.parseJSON(response);
                    result_arr.push(data.db_arr);
                    console.log(data.db_arr[2]);
                    result_arr.push('|');
                    //   console.log(result_arr);
                    $('#secondary_doc_attachment').val(result_arr);
                    var f1 = data.db_arr[2];
                    var f2 = f1.split('.')[0];
                    var row = '<tr id="refreshdelete' + f2 + '">';
                    $.each((data.ajax_arr), function (i, d) {
                        $('.norecords').closest('tr').remove();
                        var display_url = '{{asset("attachment/secondary_document")}}/' + data.db_arr[2];
                        if (i == 1) {
                            row += '<td><a target="_blank" href="' + display_url + '">' + d + '</a></td>';
                        } else {
                            row += '<td>' + d + '</td>';
                        }
                    });
                    var attachment_url = '{{url("customer/removeattachment")}}';
                    row += "<td><a class='btn btn-secondary item red-tooltip remove-attachment' data-toggle='modal' data-url='" + attachment_url + "'  data-file='" + data.db_arr[2] + "' data-target='#attachment-confirmation-modal' data-placement='bottom' title='Remove'><span class='icon-cross-remove-sign'></span></a></td>";
                    row += '</tr>';
                    $('#document_table tbody').append(row);
                }
            });
        }
    });
//End of upload document

//Start to fetch work order number of customers
    $('#work_order_no').find('option').remove().end().append('<option value="">Select</option>');

    $.ajax({
        type: "get",
        url: "{{url('customer/secondary-document/autocomplete')}}",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {

            //  console.log(response);
            $.each(response, function (e, val) {

                $("#work_order_no").append("<option value='" + val.id + "'>#" + val.id + "</option>");
                $("#work_order_no").selectpicker("refresh");
            });

        }
    });

//End to fetch work order number of customers

//Add recipient contact dynamic
    $('#add_contact').click(function () {
        var all_val = 1;
        if ($('#name').val() != "") {
            $('input[name=contact_name]').val($('#name').val());
            // $('input[name=contact_name]').addClass('not-empty');
            $('input[name=contact_no]').val($('#contact').val());
            $('input[name=contact_email]').val($('#email').val());
            $('input[name=contact_address]').val($('#address').val());
            $('input[name=contact_zip]').val($('#zip').val());
            $("#contact_state_id").val($('#state').val());
            $("#contact_state_id").selectpicker('refresh');
            $("#contact_city_id").val($('#city').val());
            $('#autocomplete_city_id').val($('#recipient_city_id').val());
            $("#contact_city_id").selectpicker('refresh');
            $('input').addClass('not-empty');
            all_val = 0;
        } else {
            $('input[name=contact_name]').val('');
        }
        if (all_val) {
            $('input[name=contact_name]').val('');
            $('input[name=contact_no]').val('');
            $('input[name=contact_email]').val('');
            $('input[name=contact_address]').val('');
            $('input[name=contact_zip]').val('');
            $("#contact_state_id").val('');
            $("#contact_state_id").selectpicker('refresh');
            $("#contact_city_id").val('');
            $("#contact_city_id").selectpicker('refresh');
            $("#contact_country").val('');
            $("#contact_country").selectpicker('refresh');
        }
        $('#success-msg').css('display', 'none');

//            $('#contactModal').modal('show');
    });

    $('body').on('click', '#submit_contact', function () {
        var recipientForm = $('#add_recipient_contact');
        //    var formData = recipientForm.serialize();

        $('#name-error').html("");
        $('#contact-error').html("");
        $('#email-error').html("");
        $('#address-error').html("");
        $('#city-error').html("");
        $('#state-error').html("");
        $('#zip-error').html("");
        $('#country-error').html("");
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name=_token]').attr('content')
            }
        });
        $.ajax({
            url: '{{url("customer/contact-recipient/store")}}',
            type: 'POST',
            data: {
                doc_id: $('#doc_id_recipient').val(),
                contact_name: $('#contact_name').val(),
                contact_no: $('#contact_no').val(),
                contact_email: $('#contact_email').val(),
                contact_address: $('#contact_address').val(),
                contact_state_id: $('#contact_state_id').val(),
                contact_zip: $('#contact_zip').val(),
                contact_city_id: $('#contact_city_id').val(),
                contact_country: $('#contact_country').val(),
            },
            success: function (data) {

                if (data.errors) {
                    if (data.errors.contact_name) {
                        $('#name-error').html(data.errors.contact_name[0]);
                    }
                    if (data.errors.contact_address) {
                        $('#address-error').html(data.errors.contact_address[0]);
                    }
                    if (data.errors.contact_city_id) {
                        $('#city-error').html(data.errors.contact_city_id[0]);
                    }
                    if (data.errors.contact_state_id) {
                        $('#state-error').html(data.errors.contact_state_id[0]);
                    }
                    if (data.errors.contact_country) {
                        $('#country-error').html(data.errors.contact_country[0]);
                    }
                    if (data.errors.contact_zip) {
                        $('#zip-error').html(data.errors.contact_zip[0]);
                    }

                }
                if (data.success) {
                    // console.log(data.contact_data)
                    $('#success-msg').css('display', 'block');
                    $('#success-msg').html(data.success);

                    // console.log($('#doc_id_recipient').val());
                    $.ajax({
                        data: {"doc_id": $('#doc_id_recipient').val()},
                        type: 'POST',
                        url: "{{url('customer/getAllContacts')}}", // point to server-side PHP script
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function (response) {
                            $("#name").empty("");
                            $("#name").selectpicker("refresh");
                            $.each(response.contacts, function (i, obj)
                            {
                                var div_data = "<option value=" + obj.company_name + ">" + obj.company_name + "</option>";

                                $(div_data).appendTo('#name');
                            });
                            $("#name").selectpicker("refresh");
                        }
                    });
                    $('#name').val(data.contact_data.company_name);
                    $('#name').selectpicker('refresh');
                    $('#contact').val(data.contact_data.phone);
                    $('#email').val(data.contact_data.email);
                    $('#address').val(data.contact_data.mailing_address);
                    $('#city').val(data.contact_data.city_id);
                    $('#city').selectpicker('refresh');
                    $('#state').val(data.contact_data.state_id);
                    $('#state').selectpicker('refresh');
                    $('#zip').val(data.contact_data.zip);
                    $('#country').val(data.contact_data.country_id);
                    $('#country').selectpicker('refresh');
                }
                $('input').each(function () {
                    if ($(this).val().length > 0) {
                        $(this).addClass('not-empty');
                    } else {
                        $(this).removeClass('not-empty');
                    }
                });
            },
        });
    });

    /**************Autocomplete County********************/
    /*county */
    src_county = "{{ url('customer/city/autocompletecountyname') }}";

    $("#parent_country").autocomplete({

        source: function (request, response) {
            $.ajax({
                url: src_county,
                dataType: "json",
                data: {
                    term: request.term
                },
                beforeSend: function () {
                    $('#countyloader').show();
                },
                complete: function () {
                    $('#countyloader').hide();
                },
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            value: item.value,
                            id: item.id     // EDIT
                        }
                    }));

                },

            });
        },
        select: function (event, ui) {

            $("#county_id").val(ui.item.id);
            $("#parent_country").val(ui.item.value);
        },
        change: function(event, ui) {
            if (ui.item == null) {
                $(this).val("");
                $(this).focusout();
                $(this).focus();
            }
        }
    });

    src = "{{ url('customer/city/autocompletecityname') }}";

    $("#city").autocomplete({

        source: function (request, response) {
            $.ajax({
                url: src,
                dataType: "json",
                data: {
                    term: request.term
                },
                beforeSend: function () {
                    $('#recipientloader').show();
                },
                complete: function () {
                    $('#recipientloader').hide();
                },
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            value: item.value,
                            id: item.id     // EDIT
                        }
                    }));

                },

            });
        },
        select: function (event, ui) {

            $("#recipient_city_id").val(ui.item.id);
            $("#city").val(ui.item.value);
        },
        change: function(event, ui) {
            if (ui.item == null) {
                $(this).val("");
                $(this).focusout();
                $(this).focus();
            }
        }
    });


    $("#contact_city_id").autocomplete({

        source: function (request, response) {
            $.ajax({
                url: src,
                dataType: "json",
                data: {
                    term: request.term
                },
                beforeSend: function () {
                    $('#recipientpopuploader').show();
                },
                complete: function () {
                    $('#recipientpopuploader').hide();
                },
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            value: item.value,
                            id: item.id     // EDIT
                        }
                    }));

                },

            });
        },
        select: function (event, ui) {

            $("#autocomplete_city_id").val(ui.item.id);
            $("#contact_city_id").val(ui.item.value);
        },
        change: function(event, ui) {
            if (ui.item == null) {
                $(this).val("");
                $(this).focusout();
                $(this).focus();
            }
        }
    });
    /*************Autocomplete County*****************/
});

$('#choose_address').click(function () {
    var selected_addr = '';
    if ($('input:radio[name="select_addr"]:checked').length > 0) {
        selected_addr = $('input:radio[name="select_addr"]:checked').val();
        selected_addr = selected_addr.split('**');
        console.log(selected_addr);
        var city = selected_addr[1];
        //var city = 'Boca Raton';
        var state = selected_addr[2];
        //var state = 'Florida';
        $('#address').val(selected_addr[0]);
        $('#zip').val(selected_addr[3]);
        $("#state option").each(function () {
            if ($(this).text() == state) {
                $(this).attr('selected', 'selected');
            }
        });
        $("#state").selectpicker("refresh");
        $("#city option").each(function () {
            if ($(this).text() == city) {
                $(this).attr('selected', 'selected');
            }
        });
        $("#city").selectpicker("refresh");
        $('#address_verification_modal').modal('hide');
    } else {
        alert("Please select one address");
    }
})

$(document).on('click', '.remove-attachment', function () {

    var url = $(this).attr('data-url');
    var file = $(this).attr('data-file');
    $(".remove-attachment-model").attr("action", url);
    $("input[name='file']").remove();
    $('body').find('.remove-attachment-model').append('<input name="file"  type="hidden" value="' + file + '">');
    $('body').find('.remove-attachment-model').append('<input name="_method" type="hidden" value="POST">');
});
//For account manager login parent work according to customer
$(document).on('click', '.remove-parent-attachment', function () {
    var file = $(this).attr('data-file');
    var url = $(this).attr('data-url');
    $(".remove-attachment-model").attr("action", url);
    $("input[name='file']").remove();
    $('body').find('.remove-attachment-model').append('<input name="file" type="hidden" value="' + file + '">');
    $('body').find('.remove-attachment-model').append('<input name="_method" type="hidden" value="POST">');
});
$('#customer_id').on('change', function () {

    $.ajax({
        type: "post",
        url: "{{url('customer/account-manager/secondary-document/autocomplete')}}",
        data: {customer_id: $('#customer_id').val()},
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            $("#work_order_no").empty();
            $("#work_order_no").selectpicker("refresh");
            //  console.log(response);
            $('#work_order_no').find('option').remove().end().append('<option value="">Select</option>');

            $.each(response, function (e, val) {

                $("#work_order_no").append("<option value='" + val.id + "'>" + val.id + "</option>");
                $("#work_order_no").selectpicker("refresh");
            });

        }
    });
});

$('#customer_id').on('change', function () {

    $.ajax({
        type: "post",
        url: "{{url('customer/account-manager/secondary-document/authoriseagent')}}",
        data: {customer_id: $('#customer_id').val()},
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            $("#officers_directors").empty();
            $("#officers_directors").selectpicker("refresh");

            $("#officers_directors_title").empty();
            $("#officers_directors_title").selectpicker("refresh");
            //  console.log(response);
            $('#officers_directors').find('option').remove().end().append('<option value="">Select</option>');
            $('#officers_directors_title').find('option').remove().end().append('<option value="">Select</option>');
            $.each(response, function (e, val) {
                $.each(val, function (e1, v1) {
                    var total_name = v1.first_name + ' ' + v1.last_name;

                    $("#officers_directors").append("<option value='" + total_name + "'>" + total_name + "</option>");
                    $("#officers_directors").selectpicker("refresh");
                    $("#officers_directors_title").append("<option value='" + v1.title + "'>" + v1.title + "</option>");
                    $("#officers_directors_title").selectpicker("refresh");
                });
            });
        }
    });
});
var remove_arr = [];
$('.remove-attachment-model').submit(function (event) {
    event.preventDefault();
    var file = $('input[name=file]').val();
    var id = file.split('.')[0];

    $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name=_token]').attr('content')
        }
    });
    var submit_url = $(this).attr("action");
    $.ajax({
        type: 'POST',
        url: submit_url,
        data: $('#remove-attachment-model').serialize(),
        success: function (response) {

            if (response.success) {
                $('#attachment-confirmation-modal').modal('hide');
                remove_arr.push(file);
                $('#remove_doc').val(remove_arr);

                var del_var = "refreshdelete" + id;

                $("#" + del_var).remove();
            }
        }
    });
});
                </script>
                @stop