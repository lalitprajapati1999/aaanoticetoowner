<style>
.select-opt .select2-container--default .select2-selection--single, .select2-selection .select2-selection--single {
    border: none !important;
    border-bottom: 1px solid #ccc !important;
    border-radius: 0;
    padding: 7px 0 !important;
    height: 40px;
    text-align: left;
    font-family: "Helvetica Normal";
    font-size:15px;
    /* position: sticky !important; */
}

.select-opt .select2-container--default .select2-selection--single {
    background: none !important;
    border: 1px solid #aaa;
    border-radius: 4px;
}

.select-opt .select2-container--default .select2-selection--single .select2-selection__arrow b {
    border-color: #888 transparent transparent transparent;
    border-style: solid;
    border-width: 7px 7px 0 7px !important;
    height: 0;
    left: 0 !important;
    margin-left: -4px;
    margin-top: -2px;
    position: absolute;
    top: 50%;
    width: 0;
}

</style>
@extends('adminlte::page')
@section('content')
<div>
    <section class="view-order secondary-doc">
        <div class="dashboard-wrapper">

           @if (Session::get('success'))
            <div class="alert alert-success">
                <?php echo Session::get('success'); ?>
            </div>
            @endif
            <div class="dashboard-heading">
                <h1><span>Secondary Document</span></h1>
            </div>
            <div class="dashboard-inner-body">
                <div class="row">
                    <div class="col-md-12">

                        <div class="search-by full-width">
                            <div class="form-group full-width">
                                <label for="" class="display-ib">Search By: </label>
                                <div class="calender-div">
                                    <p class="display-ib pull-left">Date Range</p>
                                    <div class="date-box display-ib date">
                                        <div class="form-group">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input class="form-control  " type="text" name="from_date"  id="from-date" placeholder="From Date" autocomplete="off">
                                                    <!-- <label>Start Date</label> -->
                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-default"><span class="icon-calendar"></span></button>
                                    </div>
                                    <p class="display-ib ">to</p>
                                    <div class="date-box display-ib date">
                                        <div class="form-group">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input class="form-control "  type="text" name="to_date"  id="to-date" placeholder="To Date" autocomplete="off">
                                                    <!-- <label>End Date</label> -->
                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-default"><span class="icon-calendar"></span></button>
                                    </div>
                                </div>

                                <div class="select-opt display-ib pull-left">
                                    <select class="selectpicker" name="project_address" data-show-subtext="true" data-live-search="true" id="project_address_id">

                                    </select>
                                </div>
                                <div class="select-opt display-ib pull-left" >
                                    <!-- <select class="selectpicker" name="work_order_no" data-show-subtext="true" data-live-search="true" id="work_order_no_id" >
                                    </select> -->
                                    <select class="form-control"  name="work_order_no" id="work_order_no_id" data-show-subtext="true" data-live-search="true">
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                                <div class="select-opt display-ib pull-left"  >
                                    <!-- <select class="selectpicker" name="parent_work_order_no" data-show-subtext="true" data-live-search="true" id="parent_work_order_no_id">
                                    </select> -->
                                    <select class="form-control"  name="parent_work_order_no" id="parent_work_order_no_id" data-show-subtext="true" data-live-search="true">
                                    </select>
                                </div>

                                
                                 <div class="select-opt display-ib pull-left">
                                    <select class="selectpicker" name="document_type" data-show-subtext="true" data-live-search="true" id="document_type">
                                       <option value="">Select Document Type</option>
                                        @foreach($notices as $notice)
                                        <option value="{{$notice->name}}">{{$notice->name}}</option>
                                        @endforeach

                                    </select>
                                </div>

                                <div class="form-group display-ib pull-left customer-search" style="width:40%">
                                    <input class="form-control" name="customer_name" placeholder="Contracted By" type="text" id="autocompleteCustomerName">
<!--                                        <span class="form-group-btn">
                                        <button class="btn btn-search" type="submit" data-original-title="" title=""><span class="icon-search"></span></button>
                                    </span>-->

                                </div>

                                <div class="form-btn-div form-group display-ib pull-left " style="    margin-top: 10%;">
                                        <button id="resetall" class="btn btn-primary custom-btn form-btn"><span>Reset</span></button>
                                    </div>
                            </div>
                           <!--  <div class="row">
                                <div class="col-md-12 text-right">
                                    <div class="form-btn-div full-width">
                                        <button id="resetall" class="btn btn-primary custom-btn form-btn"><span>Reset</span></button>
                                    </div>
                                </div>
                            </div> -->
                        </div>

                        <div class="row">
                            <div class="col-md-2 text-left">
                             <div class="form-btn-div" style="margin-bottom: 14%;">
                            @if(Auth::user()->hasRole('customer'))
                               
                                        <a style="" class="btn btn-primary custom-btn form-btn" href="{{url('customer/contacts')}}" style=""><span>+</span>Address Book</a>
                                    
                            @endif

                                     <a href="#TermsConditionalModal" onclick="submit_terms_condition('0')" data-toggle="modal" class="btn btn-primary custom-btn form-btn" style="margin-top: 1%;">+<span>Create New Document</span></a>
                             </div>
                            </div>
                            </div>
                            <div class="row">
                           <!--  <div class="col-md-10 ">

                                <div class="form-btn-div">

                                    <button id="partialrelease" class="btn btn-primary custom-btn form-btn"><span>Partial Conditional Lien Waiver</span></button>
                                    <button id="finalrelease" class="btn btn-primary custom-btn form-btn"><span>Partial Unconditional</span></button>
                                    <button id="partialconditional" class="btn btn-primary custom-btn form-btn"><span>Final Unconditional Waiver</span></button>
                                    <button id="finalconditional" class="btn btn-primary custom-btn form-btn"><span>Partial Unconditional Bond Waiver</span></button>

                                </div>
                            </div> -->
                        </div>

                    </div>


                    <div class="col-md-12">
                        <div class="dahboard-table table-responsive fixhead-table">
                            <table class="table table-striped" id="secondary-documents" datatable="" width="100%" cellspacing="0"   data-scroll-x="true"scroll-collapse="false">

                                <thead class="thead-dark">
                                    <tr>
                                        <th scope="col">Work Order #</th>
                                        <th scope="col">Parent Work Order</th>
                                        <th scope="col">Created Date</th>
                                        <th scope="col">Customer (Contracted By)</th>
                                        <th scope="col">Project Address</th>
                                        <th scope="col">Document Type</th>
                                        <th scope="col">Options</th>
                                    </tr>
                                </thead>

                            </table>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="pagination-div text-right">
                            <!--                            <div class="select-opt">
                                                            <select class="selectpicker">
                                                                <option value="10">10 results per page</option>
                                                                <option value="20">20 results per page</option>
                                                                <option value="30">30 results per page</option>
                                                            </select>
                                                        </div>  
                                                      
                            
                                                        <ul class="pagination">
                                                            <li class="disabled"><a rel="previous" href="#"><img src="{{url('images/left-arrow.png')}}"></a></li>
                                                            <li class="active"><span>1</span></li>
                                                            <li><a href="#">2</a></li>
                                                            <li><a href="#">3</a></li>
                                                            <li><a href="#">4</a></li>
                                                            <li><a rel="next" href="#"><img src="{{url('images/right-arrow.png')}}"></a></li>
                                                        </ul>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </section>
</div>
<!--Note Modal -->
<form method="post" data-parsley-validate="" enctype="multipart/form-data"  id="formSendMail">
    {!! csrf_field() !!}
    <div id="mailModal" class="modal fade register-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Send Email</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="secodary_doc_id" id="secodary-doc-id" value="">
                    <div class="input-wrapper full-width">
                        <div class="styled-input">
                            <input type="email" name="email" id="email" value="" class="form-control" data-parsley-required="true" data-parsley-required-message="Email is required" data-parsley-trigger="change focusout" data-parsley-type="email">
                            <label>Email</label>
                            <span></span>
                        </div>
                    </div>
<span></span>
<span></span>
                    <div class="form-btn-div doc_file" style="margin-top: 3%;">
                        <span class="btn btn-default btn-file upload-btn btn btn-primary form-btn" id="temp">
                            Choose File to Upload  <input type="file" name="file_name" id="edit_result_file">
                        </span>
                        (Allow Types:csv,xls,xlsx,pdf,doc,docx, jpg, jpeg)
                        <span id="errormessage" style="display: none"></span>
                    </div>
                    <span  id="preview" style="display: none;" ></span>
                </div>
                <div class="modal-footer">
                    <div class="form-bottom-btn">
                        <button type="submit" id="send_mail" class="btn btn-primary custom-btn customc-btn"><span>Send Email</span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>



<!--Payment Meanings  -->
<div id="paymentMeaningModal" class="modal fade register-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title meaning_title"></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">

                <div class="input-wrapper full-width">
                    <div class="styled-input meaning_desc">
                        <span></span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="form-bottom-btn">
                    <button type="button" class="btn btn-primary custom-btn customc-btn" data-dismiss="modal"><span>Cancel</span></button>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Information check -->
    <form method="get" data-parsley-validate="" id="frm_TermsConditional">

    <div id="TermsConditionalModal" class="modal fade register-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Terms and Condition</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="input-wrapper full-width">
        <div class="form-group row">      
          <div class="col-md-6">
                <div class="select-opt clear-b">
                    <label class="col-md-12 control-label" for="state_id">Select State:</label> 
                    <select class="selectpicker" id="state_id" name="state_id" data-parsley-required="true" data-parsley-required-message="Select State">
                    <option value="">Select State</option>
                            @if(isset($state_arr) && count($state_arr)>0)
                            @foreach($state_arr as $state)
                                <option value="{{ $state['id'] or '' }}"> {{ $state['name'] or '' }} </option>
                            @endforeach()
                            @endif
                    </select>
                </div>
               
          </div>
          <div class="col-md-6">
             <div class="select-opt clear-b">
                    <label class="col-md-12 control-label" for="notice_id">Select Notice:</label>
                    <select class="selectpicker" id="notice_id" name="notice_id"  data-parsley-required="true"                    
                    data-parsley-required-message="Select Notice">
                    <option value="">Select State First</option>    
                    </select>
                </div>
               
            
          </div>
        </div>
                       <!--  For<br/>
                        Create Your Own Documents<br/>
                        Notices / LIENS / RELEASES <br/> -->
                        <br/><br/>
                         Create your own Release <br/><br/>

                        <p>  I hereby consent and waive all and any claim against <strong>AAA Business Association Corporation DBA {{env('FROM_NAME')}} </strong> for any document that I or my firm have created while using {{env('FROM_NAME')}}’s database. I fully consent on behalf of myself and for my firms’ employees that the information researched and included into the making of this legal document is our sole responsibility and we do not hold {{env('FROM_NAME')}} liable for any information included on this legal document. </p>
                        <p>  __ I consent to the terms and conditions set by {{env('FROM_NAME')}} in order for our firm to process our own legal documents.</p>
                    </div>

                    <div class="input-wrapper full-width work-status">
                        <ul class="checkbox-holder">
                            <li><a href="#"><input  class="regular-checkbox not-empty"  id="terms_condition_checkbox" name="terms" data-parsley-required="true" data-parsley-required-message="Accept Seconday Document Terms and Condition" value="1" type="checkbox"><label for="terms_condition_checkbox">Accept Secondary Document Terms and Conditions</label></a></li>
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form-bottom-btn">
                        <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>Submit</span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@section('frontend_js')
<script type="text/javascript" src="{{ asset('js/jquery-ui.min.js') }}"></script>

<script src="{{ asset('vendor/adminlte/plugins/datepicker/bootstrap-datepicker.js') }}" type="text/javascript"></script>
<script type="text/javascript">


                                        $('#work_order_no_id').on('keyup keypress change', function () {
                                            // otable.search(this.value).draw();
                                            otable.fnFilter(this.value, 0, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true);
                                        });
                                        $('#work_order_no_id').on('keyup keypress change', function () {
                                            // otable.search(this.value).draw();
                                            otable.fnFilter(this.value, 0, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true);
                                        });
                                        $('#autocompleteCustomerName').on('keyup keypress change', function () {
                                            // otable.search(this.value).draw(); 
                                            otable.fnFilter(this.value, 3, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true);
                                        });
                          /*              $('#project_address_id').on('keyup keypress change', function () {
                                            // otable.search(this.value).draw();
                                           // console.log(preg_match("^[a-zA-Z0-9!@#$&()\\-`.+,/\"]*$", this.value));
                                                                                               docTable.columns(4).search("3").draw();

                                           // otable.fnFilter(this.value , 4, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true);
                                        });*/
                                        $('#parent_work_order_no_id').on('keyup keypress change', function () {
                                            // otable.search(this.value).draw();
                                            otable.fnFilter(this.value, 1, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true);
                                        });
                                        $(document).ready(function () {
                                            src = "{{ url('/customer/searchajaxcustomername') }}";
                                            $("#autocompleteCustomerName").autocomplete({
                                                source: function (request, response) {
                                                    $.ajax({
                                                        url: src,
                                                        dataType: "json",
                                                        data: {
                                                            term: request.term
                                                        },
                                                        success: function (data) {
                                                            response(data);

                                                        },
                                                        select: function (event, ui) {
                                                            $("#autocompleteCustomerName").val(ui.item.id);
                                                        }
                                                    });
                                                },
//        minLength: 3,

                                            });
                                            $(function () {
                                                $.ajaxSetup({
                                                    headers: {
                                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                                    }
                                                });
                                                var docTable = $('#secondary-documents').DataTable({
                                                    // "pageLength":10
                                                    order: [[0, 'desc' ]],
                                                    "language": {

                                                        "zeroRecords": "No Record Found",
                                                    },
                                                    processing: true,
                                                    serverSide: true,
                                                    dom: "<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-6'p>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
                                                    ajax: {
                                                        url: "{{url('customer/secondary-document/custom-filter')}}",
                                                        data: function (d) {
                                                            d.customer_name = $('input[name=customer_name]').val();
                                                            d.from_date = $('input[name=from_date]').val();
                                                            d.to_date = $('input[name=to_date]').val();
                                                            d.project_address = $('#project_address_id').val();
                                                        }
                                                    },
                                                    columns: [
                                                        {data: 'secondary_doc_id', name: 'secondary_doc_id'},
                                                        {data: 'work_order_no', render: function (data, type, row) {
                                                                if (data) {
                                                                    return  '#' + data;
                                                                } else {
                                                                    return   data;
                                                                }
                                                            }},
                                                        {data: 'created_at', render: function (data, status, row) {
                                                                if (data) {
                                                                    var date = new Date(data);
                                                                    var month = date.getMonth() + 1;
                                                                    return (month.length > 1 ? month : month) + "/" + date.getDate() + "/" + date.getFullYear();
                                                                } else
                                                                    return "";

                                                            }},
                                                      {data: 'contracted_by', class: 'dropdown pro-contby', render: function (data, status, row) {
                                                               if (data) {
                                                                   return data.length > 50 ?
                                                                           data.substr(0, 50) + '<a  data-toggle="tooltip" title="' + data + '">...</a>' :
                                                                           data;
                                                               } else
                                                                   return "NA";
                                                           }},
                                                        {data: 'project_address', render: function (data, type, row) {
                                                            if (data) {
                                                                var data_addr = $("<br />").html(data).text();
                                                                var regex = /<br\s*[\/]?>/gi;
                                                                projaddr=data.length > 10 ?
                                                                data_addr.replace(regex, ' ').substr(0, 15) + '<a  data-toggle="tooltip" title="' + data_addr.replace(regex, ' ') + '" >...</a>' :
                                                                data;
                                                                return projaddr.split('**').join('&#013;**');
                                                            } else
                                                                return "";

                                                            }
                                                            },
                                                        {data: "notice_name", class: 'col pro-temp', render: function (data, notice_id, row) {
                                                               if (data) {
                                                                   
                                                                   var data_notice = $("<br />").html(data).text();
                                                                   var regex = /<br\s*[\/]?>/gi;

                                                                   return  data.length > 50 ?
                                                                           data_notice.replace(regex, ' ').substr(0, 50) + '<a   data-toggle="tooltip" title="' + data_notice.replace(regex, ' ') + '">...</a>' :
                                                                           data;
                                                               } else
                                                                   return "NA";

                                                           }},
                                                        {data: 'action', name: 'action'}
                                                    ]

                                                });

//        $('#customer-name').on('submit', function (e) {
////            docTable.draw();
////            e.preventDefault();
//
//        });
//       
                                                $.fn.dataTable.ext.search.push(
                                                        function (settings, data, dataIndex) {
                                                            var min = $('#from-date').datepicker("getDate");
                                                            var max = $('#to-date').datepicker("getDate");
                                                            var startDate = new Date(data[4]);
                                                            if (min == null && max == null) {
                                                                return true;
                                                            }
                                                            if (min == null && startDate <= max) {
                                                                return true;
                                                            }
                                                            if (max == null && startDate >= min) {
                                                                return true;
                                                            }
                                                            if (startDate <= max && startDate >= min) {
                                                                return true;
                                                            }
                                                            return false;
                                                        }
                                                );
                                                $("#from-date").datepicker({onSelect: function () {
                                                        docTable.draw();
                                                    }, changeMonth: true, changeYear: true});
                                                $("#to-date").datepicker({onSelect: function () {
                                                        docTable.draw();
                                                    }, changeMonth: true, changeYear: true});
                                                $('#from-date, #to-date').on('change', function () {
                                                    docTable.draw();
                                                });
                                                $('#partialrelease').on('click', function () {
                                                    docTable.columns(5).search("0").draw();
                                                });
                                                $('#finalrelease').on('click', function () {
                                                    docTable.columns(5).search("1").draw();
                                                });
                                                $('#partialconditional').on('click', function () {
                                                    docTable.columns(5).search("2").draw();
                                                });
                                                $('#finalconditional').on('click', function () {
                                                    docTable.columns(5).search("3").draw();
                                                });
                                                $('#document_type').on('change', function () {
                                                    var val = $('#document_type').val();
                                                    docTable.columns(5).search(val).draw();
                                                });
                                                $('#project_address_id').on('keyup keypress change', function () {
                                                // otable.search(this.value).draw();
                                                var val=this.value;
                                                
                                                val1=val.split('<br>');
                                                docTable.columns(4).search(val1[0]).draw();
                                                // otable.fnFilter(this.value , 4, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true);
                                                });
                                                //Start to fetch project address of customers
                                                $('#project_address_id').find('option').remove().end().append('<option value="">Select Project Address</option>');

                                                $.ajax({
                                                    type: "get",
                                                    url: "{{url('customer/get-projectaddress')}}",
                                                    headers: {
                                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                                    },
                                                    success: function (response) {

                                                       $.each(response, function (e, val) {
                                                            var data_addr = $("<br />").html(val).text();
                                                                var regex = /<br\s*[\/]?>/gi;

                                                                projaddr=val.length > 10 ?
                                                                data_addr.replace(regex, '') :
                                                                val;
                                                                val1= projaddr.split('**').join('<br>**');
                                                                //console.log(val1);
                                                                    if(val1.length > 15){
                                                                        var text_val = val1.substring(0,40)+ '...';
                                                                    }
                                                                    else{
                                                                        var text_val = val1;
                                                                    }

                                                            $("#project_address_id").append("<option value='" + val1 + "' >" + text_val+ "</option>");
                                                            $("#project_address_id").selectpicker("refresh");
                                                        });

                                                    }
                                                });
                                                //End to fetch project address of customers
                                                //Start to fetch project secondary document id of customers
                                                $('#work_order_no_id').find('option').remove().end().append('<option value="">Select Work Order</option>');

                                                $('#work_order_no_id').select2({
                                                    minimumInputLength: 1,
                                                    ajax: {
                                                        url : "{{ url('customer/get-secondarydocids_V1') }}",
                                                        dataType: 'json',
                                                        delay: 250,
                                                        type: "get",
                                                        processResults: function (data) {
                                                        return {
                                                            results:  $.map(data, function (item) {
                                                                return {
                                                                    text: item,
                                                                    id: item
                                                                }
                                                            })
                                                        };
                                                        },
                                                        cache: true
                                                    }
                                                });

                                                /*$.ajax({
                                                    type: "get",
                                                    url: "{{url('customer/get-secondarydocids')}}",
                                                    headers: {
                                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                                    },
                                                    success: function (response) {

                                                        //  console.log(response);
                                                        $.each(response, function (e, val) {

                                                            $("#work_order_no_id").append("<option value='" + val.id + "'>#" + val.id + "</option>");
                                                            $("#work_order_no_id").selectpicker("refresh");
                                                        });

                                                    }
                                                });*/
                                                //End to fetch project secondary document id of customers
                                                //Start to fetch project parent work order of customers
                                                $('#parent_work_order_no_id').find('option').remove().end().append('<option value="">Select Parent Work Order</option>');

                                                $('#parent_work_order_no_id').select2({
                                                    minimumInputLength: 1,
                                                    ajax: {
                                                        url : "{{ url('customer/get-secondaryparentworkorderids_V1') }}",
                                                        dataType: 'json',
                                                        delay: 250,
                                                        type: "get",
                                                        processResults: function (data) {
                                                        return {
                                                            results:  $.map(data, function (item) {
                                                                return {
                                                                    text: item,
                                                                    id: item
                                                                }
                                                            })
                                                        };
                                                        },
                                                        cache: true
                                                    }
                                                });

                                                /*$.ajax({
                                                    type: "get",
                                                    url: "{{url('customer/get-secondaryparentworkorderids')}}",
                                                    headers: {
                                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                                    },
                                                    success: function (response) {

                                                        if (response.length != 0) {
                                                            $.each(response, function (e, val) {
                                                                $("#parent_work_order_no_id").append("<option value='" + val.work_order_no + "'>" + val.work_order_no + "</option>");
                                                                $("#parent_work_order_no_id").selectpicker("refresh");
                                                            });
                                                        } else {

                                                            $("#parent_work_order_no_id").selectpicker("refresh");
                                                        }

                                                    }
                                                });*/
                                                //End to fetch project parent work order of customers


                                            });


                                            $(function () {
                                                otable = $('#secondary-documents').dataTable();
                                            });


                                        });




                                        function setAutoComplete(data)
                                        {
                                            var selectors = ["#project_address"];
                                            //if your selector are in the data then use data.map to get your selectors.
                                            selectors.forEach(function (selector)
                                            {
                                                $(selector).autocomplete({
                                                    source: data
                                                });
                                            });
                                        }
                                        function downloadSecondaryDoc(filename) {console.log(filename);
                                            var url = '{{asset("pdf/secondary_document/")}}';
                                            var fname = filename;
                                            var filename = url + "/" + filename;

                                            //console.log(filename);return;
                                            var xhr = new XMLHttpRequest();
                                            xhr.responseType = 'blob';
                                            xhr.onload = function () {

                                                var a = document.createElement('a');
                                                a.href = window.URL.createObjectURL(xhr.response);
                                                a.download = fname;
                                                a.style.display = 'none';
                                                document.body.appendChild(a);
                                                a.click();
                                                delete a;
                                            };
                                            xhr.open('GET', filename);
                                            xhr.send();
                                        }
                                        function sendEmail(document_id) {
                                            $('#secodary-doc-id').val(document_id);
                                        }
                                        function show_meaning(meaning_id) {

                                            if (meaning_id == '0') {
                                                $('.meaning_title').html('PARTIAL CONDITIONAL LIEN WAIVER');
                                                $('.meaning_desc').html('<p>This waiver – the Conditional Waiver and Release on Progress Payment – should be used when a progress payment on the project is expected. There may be future expected payments on the project, but this waiver applies only to a specific, expected progress or partial payment.</p> <p>Because this is a “conditional” waiver, it is ok if payment has not yet been received. The waiver is “conditional” on the receipt of the payment and will be invalid if payment is not ultimately received. If payment has already been received in full, an “unconditional” waiver may be more appropriate.</p>');
                                            }
                                            if (meaning_id == '1') {
                                                $('.meaning_title').html('PARTIAL UNCONDITIOAN');
                                                $('.meaning_desc').html('<p>This waiver – the Unconditional Waiver and Release on Progress Payment – should be used when a progress payment on the project has been received. Further payments may be expected, but the specific partial payment discussed in the waiver has been received.</p><p> Because this is an “unconditional” waiver, it should only be used when payment has actually been received. If payment has not been received, if the check hasn’t yet cleared the bank, or if there is some other reason why payment might ultimately fail, this waiver should not be furnished. If this is the case, signing a “conditional” waiver is a safer option</p>');

                                            }
                                            if (meaning_id == '2') {
                                                $('.meaning_title').html('FINAL UNCONDITIOAN WAIVER');
                                                $('.meaning_desc').html('<p>This waiver – the Unconditional Waiver and Release on Final Payment – should be used when the final payment on the project has been made and received by the party that furnished labor or materials. Signing this waiver indicates that payment has been received and no further payments are expected.</p><p> Because this is an “unconditional” waiver, it should only be used when payment has actually been received. If payment has not been received, if the check hasn’t yet cleared the bank, or if there is some other reason why payment might ultimately fail, this waiver should not be furnished. If this is the case, signing a “conditional” waiver is a safer option</p>');

                                            }
                                            if (meaning_id == '3') {
                                                $('.meaning_title').html('PARTIAL UNCONDITIONAL BOND WAIVER');
                                                $('.meaning_desc').html('<p>This waiver – the Unconditional Waiver and Release on Progress Payment – should be used when a progress payment on the project has been received. Further payments may be expected, but the specific partial payment discussed in the waiver has been received.</p><p> Because this is an “unconditional” waiver, it should only be used when payment has actually been received. If payment has not been received, if the check hasn’t yet cleared the bank, or if there is some other reason why payment might ultimately fail, this waiver should not be furnished. If this is the case, signing a “conditional” waiver is a safer option.</p>');

                                            }

                                        }
                                        function submit_terms_condition(submit_type) {

                                            var form_action = "{{url('customer/partial-document')}}/" + submit_type;
                                            $('#frm_TermsConditional').attr('action', form_action);

                                        }




    $("#send_mail").on("click", function (e) {
        e.preventDefault();
        if($('#formSendMail').parsley().validate()){


        var extension = $('#edit_result_file').val().split('.').pop().toLowerCase();
        if ($('#edit_result_file').val() == "" || $.inArray(extension, ['csv', 'xls', 'xlsx', 'pdf', 'doc', 'docx', 'jpg', 'jpeg']) == -1) {
            $('#errormessage').html('Please Select Valid File... ');
            $('#errormessage').css('display', 'block');
        }
        else{
            $('#errormessage').css('display', 'none');
            var file_data = $('#edit_result_file').prop('files')[0];
            var email_val = $('#email').val();
            var secondary_doc_id = $('#secodary-doc-id').val();

            var form_data = new FormData();
            form_data.append('file', file_data);
            form_data.append('email', email_val);
            form_data.append('secondary_doc_id', secondary_doc_id);

            //console.log(JSON.stringify(form_data));
            $.ajax({
               url:"{{ url('customer/secondary-document/sendMailPdf') }}",
               method:"POST",
               data:form_data,
               contentType: false, // The content type used when sending data to the server.
               cache: false, // To unable request pages to be cached
               processData: false,
               beforeSend: function() {
                    $('#send_mail').attr("disabled", true);    
                },
                   success: function (response) {
                     $('#edit_result_file').val('');
                     $('#email').val('');
                     $('#secodary-doc-id').val(''); 
                     $('#preview').hide();
                     $('#send_mail').attr("disabled", false);
                    $('#mailModal').hide();
                   }

             });
        }
    }
    });
     $('#edit_result_file').change(function(e){
        var fileName = e.target.files[0].name;
        doc_icon = fileName.split('.')[1];
        if(doc_icon == 'pdf'){
            $('#preview').html('<i class="fa fa-file-pdf-o" style="font-size:36px;margin-left: 11%;"></i>');
        }else if(doc_icon == 'doc' || doc_icon == 'docx'){
            $('#preview').html('<i class="fa fa-file-word-o" style="font-size:36px;margin-left: 11%;"></i>');
        }else if(doc_icon == 'xls' || doc_icon == 'xlsx'){
            $('#preview').html('<i class="fa fa-file-excel-o" style="font-size:36px; margin-left: 11%;"></i>');
        }else if(doc_icon == 'jpg' || doc_icon == 'jpeg'){
            $('#preview').html('<i class="fa fa-file-image-o" style="font-size:36px;margin-left: 11%;"></i>');
        }else{
            $('#preview').html('<i class="fa fa-file" style="font-size:36px;margin-left: 11%;"></i>');
        }
        // $('#preview').html(fileName);
        $('#preview').css('display','block');
        $('#remove_icon').css('display','block');
        //alert('The file "' + fileName +  '" has been selected.');
    });


     $('#state_id').change(function(){
        $("#notice_id").empty('');
        let stateID = $(this).val();
       
        $.ajax({
            type: 'GET',
            async: false,
            url: '{{ url("/") }}/customer/notices/'+btoa(stateID),
            success: function (response) { 
                console.log(response.notices_arr.length);
                if(typeof(response) == 'object'){
                if(response.notices_arr.length>0){
                   
                    var i = 0;
                    $("#notice_id").append("<option value=''>-Select Notice-</option>");

                    $.each(response.notices_arr, function (e, val) {
                        $("#notice_id").append("<option value='" + response.notices_arr[i].id+ "'>" +response.notices_arr[i].name + "</option>");
                        i++;
                    });

                }else{
                     $("#notice_id").append("<option value=''>-Notice Not Found-</option>");
                }
                $("#notice_id").selectpicker("refresh");                             
            }
        },
            error: function (response)
            {
                console.log('error',response);
            }
        });
     });
</script>
@endsection