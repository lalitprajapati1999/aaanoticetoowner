<div class="action-icon">
    <a href="{{url('customer/secondary-document/edit/'.$secondaryDocument['secondary_doc_id'].'/'.'1')}}" class="btn btn-secondary item red-tooltip" data-toggle="tooltip" data-placement="bottom" title="Edit"><span class="icon-pencil-edit-button"></span></a>
    <?php 
     /*if(file_exists(public_path(). '/pdf/secondary_document/' . $secondaryDocument['file_name'])) {*/
     if($secondaryDocument['file_name']==NULL && empty($secondaryDocument['file_name'])) {
        ?>
        <a  href="{{url('customer/secondary-document/printSecondaryDocument/'.$secondaryDocument['secondary_doc_id'].'/1')}}" class="btn btn-secondary item red-tooltip"  target="_blank" data-toggle="tooltip" data-placement="bottom" title="Print"><span class="icon-printer">{{$secondaryDocument['file_name']}}</span></a>
        <a href="{{url('customer/secondary-document/printSecondaryDocument/'.$secondaryDocument['secondary_doc_id'].'/0')}}" type="button" class="btn btn-secondary item red-tooltip" data-toggle="tooltip" data-placement="bottom" title="Download"><span class="icon-download-button"></span></a>
        <a href="#mailModal"  onclick="sendEmail('{{$secondaryDocument['secondary_doc_id']}}')" data-toggle="modal"  class="btn btn-secondary item red-tooltip" data-toggle="tooltip" data-placement="bottom" title="Mail"><span class="icon-black-envelope"></span></a>
    <?php } else {
        $file_name = $secondaryDocument['file_name'];
        ?>
        <a  target="_blank" href="{{asset('/pdf/secondary_document/'.$secondaryDocument['file_name'])}}" class="btn btn-secondary item red-tooltip" data-toggle="tooltip" data-placement="bottom" title="Print"><span class="icon-printer"></span></a>
        <a href="#" onclick="downloadSecondaryDoc('{{$secondaryDocument['file_name']}}')" type="button" class="btn btn-secondary item red-tooltip" data-toggle="tooltip" data-placement="bottom" title="Download"><span class="icon-download-button"></span></a>
        <a href="#mailModal"  onclick="sendEmail('{{$secondaryDocument['secondary_doc_id']}}')" data-toggle="modal"  class="btn btn-secondary item red-tooltip" data-toggle="tooltip" data-placement="bottom" title="Mail"><span class="icon-black-envelope"></span></a>
        <?php } ?>
</div>