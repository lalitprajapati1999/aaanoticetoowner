@extends('adminlte::page')
@section('content')
	<div  class="tab-pane active" id="statement">
						    		<section class="statement-invoice">
										<div class="dashboard-wrapper">
											<div class="dashboard-heading">
												<h1><span>Statement Invoice</span></h1>
											</div>
											<div class="dashboard-inner-body">
												<div class="row">
													<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
														<div class="statement-top">
															<ul class="list-inline">
																<li> <span>Month:</span> January</li>
																<li> <span>Year:</span> 2018</li>
																<li> <a href="#">Reset</a> </li>
															</ul>
														</div>
													</div>
												</div>
												<div class="dahboard-table table-responsive">
													<table class="table table-striped">
														<thead class="thead-dark">
															<tr>
																<th scope="col">Work Order No.</th>
																<th scope="col">Document</th>
																<th scope="col">Contracted By</th>
																<th scope="col">Mailing</th>
																<th scope="col">Recipients</th>
																<th scope="col">Total</th>
																<th scope="col">Action</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<th scope="row">1243</th>
																<td>
																	<p class="title">Preliminary Notice</p>
																	<p>1. 3120 Mundy Street Miami FL 28732</p>
																	<p>2. 711-2880 Nulla St. Mankato Mississippi 96522</p>
																</td>
																<td>ABC Supply</td>
																<td>
																	<p>Certified</p>
																	<p>Designated</p>
																	<p>&nbsp;</p>
																</td>
																<td>
																	<p>5</p>
																	<p>4</p>
																	<p>&nbsp;</p>
																</td>
																<td>
																	<p>30.00</p>
																	<p>41.10</p>
																	<p>15.40</p>
																</td>
																<td>
																	<div class="action-icon">
																		<button type="button" class="btn btn-secondary item red-tooltip" data-toggle="tooltip" data-placement="bottom" title="Print"><span class="icon-printer"></span></button>
																		<button type="button" class="btn btn-secondary item red-tooltip" data-toggle="tooltip" data-placement="bottom" title="Download"><span class="icon-download-button"></span></button>
																	</div>
																</td>
															</tr>
															<tr>
																<th scope="row">1243</th>
																<td>
																	<p class="title">Preliminary Notice</p>
																	<p>1. 3120 Mundy Street Miami FL 28732</p>
																	<p>2. 711-2880 Nulla St. Mankato Mississippi 96522</p>
																</td>
																<td>ABC Supply</td>
																<td>
																	<p>Certified</p>
																	<p>Designated</p>
																	<p>&nbsp;</p>
																</td>
																<td>
																	<p>5</p>
																	<p>4</p>
																	<p>&nbsp;</p>
																</td>
																<td>
																	<p>30.00</p>
																	<p>41.10</p>
																	<p>15.40</p>
																</td>
																<td>
																	<div class="action-icon">
																		<button type="button" class="btn btn-secondary item red-tooltip" data-toggle="tooltip" data-placement="bottom" title="Print"><span class="icon-printer"></span></button>
																		<button type="button" class="btn btn-secondary item red-tooltip" data-toggle="tooltip" data-placement="bottom" title="Download"><span class="icon-download-button"></span></button>
																	</div>
																</td>
															</tr>
															<tr>
																<th scope="row">1243</th>
																<td>
																	<p class="title">Preliminary Notice</p>
																	<p>1. 3120 Mundy Street Miami FL 28732</p>
																	<p>2. 711-2880 Nulla St. Mankato Mississippi 96522</p>
																</td>
																<td>ABC Supply</td>
																<td>
																	<p>Certified</p>
																	<p>Designated</p>
																	<p>&nbsp;</p>
																</td>
																<td>
																	<p>5</p>
																	<p>4</p>
																	<p>&nbsp;</p>
																</td>
																<td>
																	<p>30.00</p>
																	<p>41.10</p>
																	<p>15.40</p>
																</td>
																<td>
																	<div class="action-icon">
																		<button type="button" class="btn btn-secondary item red-tooltip" data-toggle="tooltip" data-placement="bottom" title="Print"><span class="icon-printer"></span></button>
																		<button type="button" class="btn btn-secondary item red-tooltip" data-toggle="tooltip" data-placement="bottom" title="Download"><span class="icon-download-button"></span></button>
																	</div>
																</td>
															</tr>
															<tr>
																<th scope="row">1243</th>
																<td>
																	<p class="title">Preliminary Notice</p>
																	<p>1. 3120 Mundy Street Miami FL 28732</p>
																	<p>2. 711-2880 Nulla St. Mankato Mississippi 96522</p>
																</td>
																<td>ABC Supply</td>
																<td>
																	<p>Certified</p>
																	<p>Designated</p>
																	<p>&nbsp;</p>
																</td>
																<td>
																	<p>5</p>
																	<p>4</p>
																	<p>&nbsp;</p>
																</td>
																<td>
																	<p>30.00</p>
																	<p>41.10</p>
																	<p>15.40</p>
																</td>
																<td>
																	<div class="action-icon">
																		<button type="button" class="btn btn-secondary item red-tooltip" data-toggle="tooltip" data-placement="bottom" title="Print"><span class="icon-printer"></span></button>
																		<button type="button" class="btn btn-secondary item red-tooltip" data-toggle="tooltip" data-placement="bottom" title="Download"><span class="icon-download-button"></span></button>
																	</div>
																</td>
															</tr>
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</section>
						    	</div>
@stop