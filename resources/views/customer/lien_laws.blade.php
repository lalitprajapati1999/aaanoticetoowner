@extends('adminlte::page')

@section('banner')
<section class="sec-banner lien-laws-banner"></section>
@stop



@section('content')
<div class="dashboard-wrapper lien-dashboard full-width">
    <section class="lien-laws">
        <div class="row">
            <div class="col-md-3 col-md-offset-1">
                <div class="lien-blog">
                    <h1>Lien blog</h1>
                </div>
            </div>
            <div class="col-md-8">
                <div class="blog-right">
                    
                </div>
            </div>
        </div>
    </section>
    <!-----------------lean-blog ends------------------>
    <!-----------------construction-questions starts---------------->
    <section class="section-wrapper construction-questions">
        <div class="row">
            <div class="col-md-12">
                <div class="section-heading">
                    <h1 class="section-title text-center">Constructions Lien Questions </h1>
                </div>
            </div>
        </div>
        <div class="accordion-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        @if(isset($lien_blogs) && count($lien_blogs)>0)
                        @foreach($lien_blogs AS $k=>$val)
                        <div class="panel panel-default panel-faq">
                            <div class="panel-heading" role="tab" id="heading{{$k}}">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$k}}" aria-expanded="true" aria-controls="collapse{{$k}}">
                                        <i class="more-less glyphicon glyphicon-plus"></i>
                                        {{$val->question}}
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse{{$k}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading{{$k}}">
                                <div class="panel-body">
                                    {!! $val->answer !!}
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@stop
@section('frontend_js')
<script>
    new WOW().init();
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('#back-to-top').click(function () {
            $('#back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });

        $('#back-to-top').tooltip('show');
        $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('.navbar-brand').addClass('fixed-brand').css("transition", "0.3 all ease-in-out 0s");
            } else {
                $('.navbar-brand').removeClass('fixed-brand');
            }
        });
    });
</script>
@stop