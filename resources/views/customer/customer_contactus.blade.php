@extends('adminlte::page')


@section('banner')

<section class="sec-banner register-banner"></section>
@stop
@section('content')
<section class="dashboard-wrapper full-width">
    <div class="register-sub">
        <div class="section-heading dashboard-heading text-center">
            <h2 class="section-title">Contact Us</h2>
            <p class="section-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
        </div>
        @if (Session::get('success'))
        <div class="col-md-10 col-sm-10">
            <div class="alert alert-success alert-dismissible">
               <?php echo Session::get('success'); ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif
        <div class="dashboard-inner-body">
            <div class="dash-subform">
                <div class="row">
                    <form class="form-horizontal" action="{{url('customer/store/contact-us')}}" method="post" data-parsley-validate="">
            {!! csrf_field() !!}
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="input-wrapper full-width">
                        <div class="styled-input">
                            <input type="text" name="name" class="form-control" data-parsley-required="true" data-parsley-required-message="Name is required" value="{{old('name')?old('name'):$logged_in_user->name}}"/>
                            <label>Name</label>
                            <span></span>
                            @if ($errors->has('name'))
                            <p class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="input-wrapper full-width">
                        <div class="styled-input">
                            <input type="email" name="email" class="form-control"  data-parsley-required="true" data-parsley-required-message="Email is required" value="{{old('email')?old('email'):$logged_in_user->email}}"/>
                            <label>Email</label>
                            <span></span>
                            @if ($errors->has('email'))
                            <p class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="input-wrapper full-width">
                    <div class="styled-input">
                                    <input class="form-control business_phone_validation"  type="text" name="contact" id="contact" data-parsley-required="true" data-parsley-required-message="Telephone is required" data-parsley-trigger="change focusout" data-parsley-pattern="(\(\d{3})\)\s\d{3}\-\d{4}$" 
                                    >

                                    <label>Telephone<span class="mandatory-field">*</span></label>
                                    <span></span>
                                </div>
                </div>
            </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="input-wrapper full-width">
                        <div class="styled-input">
                            <textarea  name="query" class="form-control"  data-parsley-required="true" data-parsley-required-message="Query is required" ></textarea>
                            <label>Query<span class="mandatory-field">*</span></label>
                            <span></span>
                            @if ($errors->has('query'))
                            <p class="help-block">
                                <strong>{{ $errors->first('query') }}</strong>
                            </p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="form-bottom-btn">
                        <a href="{{url('/')}}" class="btn btn-primary custom-btn customb-btn"><span>Cancel</span></a>
                        <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>Send</span></button>
                    </div>
                </div>
            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@stop
@section('adminlte_js')
            <script type="text/javascript">
            $(".business_phone_validation").mask("(999) 999-9999");
            </script>

@endsection
