<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>Invoices</title>
   </head>
   <body style="margin:0; padding:0; box-sizing:border-box; -webkit-box-sizing:border-box; -moz-box-sizing:border-box; -ms-box-sizing:border-box; -o-box-sizing:border-box;">

       <div style="margin: 5px auto; width: 90%;">
       <table width="100%;" style="font-family: Arial, Helvetica, sans-serif; font-size: 10px;  border-collapse: collapse;" border="0" cellspacing="0" cellpadding="0">
          <tbody>
            <tr>
                <td valign="middle" style=" text-align:right; padding:5px 0;" >
                    <img style="display:inline;" width="100" src="{{ URL::to('/') }}/img/logo.png">
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <table style="font-family: Arial, Helvetica, sans-serif; font-size: 10px; border-collapse: collapse;" border="0" cellspacing="0" cellpadding="0" width="100%" >
                        <tbody> 
                            <tr>
                                <td style="vertical-align:top; width:35%; border-left:1px solid #ccc;  height: 28px; border-bottom:1px solid #ccc;border-top:1px solid #ccc; ">
                                    <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin:0; font-weight:normal; padding:4px;text-align: left;">AAA Notice To Owner</p>
                                    <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin:0; font-weight:normal; padding:4px;text-align: left;">PO Box 22821</p>
                                    <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin:0; font-weight:normal; padding:4px;text-align: left;">Hialeah, FL 33002 US</p>
                                    <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin:0; font-weight:normal; padding:4px;text-align: left;">info@aaanoticetoowner.com</p>
                                    <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin:0; font-weight:normal; padding:4px;text-align: left;">aaanoticetoowner.com</p>
                                </td>
                                <td style="width:20%;  height: 28px; border-right:1px solid #ccc; border-bottom:1px solid #ccc; border-top:1px solid #ccc; ">
                                    <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin:0; font-weight:normal; padding:4px;   text-align: center;" ></p>
                                </td>
                                <td style="vertical-align:top; width:20%;  height: 28px;border-right:1px solid #ccc; border-bottom:1px solid #ccc; border-top:1px solid #ccc;">
                                    <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin:0; font-weight:normal; padding:4px;text-align: right; font-weight:bold;  ">Invoice Date:</p>
                                    <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin:0; font-weight:normal; padding:4px;text-align: right; font-weight:bold;">Notice Type #:</p>
                                    <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin:0; font-weight:normal; padding:4px;text-align: right; font-weight:bold;">Currency: USD</p>
                                    <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin:0; font-weight:normal; padding:4px;text-align: right; font-weight:bold;">Client</p>
                                </td>                                       
                                <td style="vertical-align:top; width:40%;  height: 28px; border-right:1px solid #ccc; border-bottom:1px solid #ccc; border-top:1px solid #ccc;"> 
                                    <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin:0; font-weight:normal; padding:4px;text-align: left; ">{{ date('d-m-Y') }}</p>
                                    <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin:0; font-weight:normal; padding:4px;text-align: left;">{{$data['description']}}</p>
                                    <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin:0; font-weight:normal; padding:4px;text-align: left; ">USD</p>
                                    <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin:0; font-weight:normal; padding:4px;text-align: left; ">{{$data['company_name']}}</p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height:25px;">
                </td>
            </tr> 
            <tr>
                <td valign="middle" style="text-align:center; background: #d9d9d9; border-top:1px solid #ccc; border-left:1px solid #ccc; border-right:1px solid #ccc; height:30px; padding-left:10px;font-size:13px; Font-weight:bold;" >
                     Charge Summary
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <table style="font-family: Arial, Helvetica, sans-serif; font-size: 10px; border-collapse: collapse;" border="0" cellspacing="0" cellpadding="0" width="100%" >
                        <tbody>
                            <tr style="background-color:#f2f2f2;">
                                <th style=" border-bottom:1px solid #ccc;border-left: 1px solid #ccc; border-top:1px solid #ccc;padding:5px;font-size:12px;"> PRODUCT/SERVICE</th>
                                <th style="border-left:1px solid #ccc; border-bottom:1px solid #ccc; border-top:1px solid #ccc;padding:5px;font-size:12px;"> DESCRIPTION</th>
                                <th style="border-left:1px solid #ccc;font-size:12px; border-bottom:1px solid #ccc; border-top:1px solid #ccc;padding:5px;">QTY</th>
                                <th style="border-left:1px solid #ccc; border-bottom:1px solid #ccc; border-top:1px solid #ccc;padding:5px;font-size:12px;border-right:1px solid #ccc; text-align: right;"> RATE </th>
                                <th style="border-left:1px solid #ccc; border-bottom:1px solid #ccc; border-top:1px solid #ccc;padding:5px;font-size:12px;border-right:1px solid #ccc; text-align: right;"> AMOUNT </th>
                            </tr>
                            <tr style="height:50px; vertical-align:top;">
                                <td style="width:30%;  height: 28px;border-left: 1px solid #ccc; border-right:1px solid #ccc;">
                                    <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin:0; font-weight:normal; padding:4px; text-align: center;">{{$data['description']}}</p>
                                </td>
                                <td style="width:10%;  height: 28px;border-right:1px solid #ccc;">
                                    <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin:0; font-weight:normal; padding:4px; text-align: center;" >{{$data['description']}}</p>
                                </td>
                                <td style="width:20%; height: 28px; border-right:1px solid #ccc; ">
                                    <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin:0; font-weight:normal; padding:4px; text-align: center;" >1</p>
                                </td>
                                <td style="width:60%;  height: 28px;border-right:1px solid #ccc;">
                                    <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin:0; font-weight:normal; padding:4px; text-align: right; " >{{$data['charge']}}</p>
                                </td>
                                <td style="width:60%;  height: 28px;border-right:1px solid #ccc;">
                                    <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin:0; font-weight:normal; padding:4px; text-align: right; " >{{$data['charge']}}</p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:30%;  height: 28px;border-left: 1px solid #ccc; border-right:1px solid #ccc;">
                                    <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin:0; font-weight:normal; padding:4px; text-align: center;"></p>
                                </td>
                                <td style="width:10%;  height: 28px;border-right:1px solid #ccc; ">
                                    <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin:0; font-weight:normal; padding:4px; text-align: center;"></p>
                                </td>
                                <td style="width:20%; height: 28px; border-right:1px solid #ccc; ">
                                    <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin:0; font-weight:normal; padding:4px; text-align: center;"></p>
                                </td>
                                <td style="width:60%;  height: 28px;border-right:1px solid #ccc;">
                                    <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin:0; font-weight:normal; padding:4px; text-align: center;"></p>
                                </td>
                                <td style="width:60%;  height: 28px;border-right:1px solid #ccc;">
                                    <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin:0; font-weight:normal; padding:4px; text-align: center;"></p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:30%;  height: 28px;border-left: 1px solid #ccc; border-right:1px solid #ccc;">
                                    <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin:0; font-weight:normal; padding:4px; text-align: center;"></p>
                                </td>
                                <td style="width:10%;  height: 28px;border-right:1px solid #ccc;">
                                    <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin:0; font-weight:normal; padding:4px; text-align: center;"></p>
                                </td>
                                <td style="width:20%; height: 28px; border-right:1px solid #ccc; ">
                                    <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin:0; font-weight:normal; padding:4px; text-align: center;"></p>
                                </td>
                                <td style="width:60%;  height: 28px;border-right:1px solid #ccc;">
                                <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin:0; font-weight:normal; padding:4px; text-align: center;"></p>
                                </td>
                                <td style="width:20%; height: 28px; border-right:1px solid #ccc; ">
                                    <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin:0; font-weight:normal; padding:4px; text-align: center;"></p>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:30%;  height: 28px;border-left: 1px solid #ccc; border-right:1px solid #ccc; border-bottom:1px solid #ccc;">
                                    <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin:0; font-weight:normal; padding:4px; text-align: center;"></p>
                                </td>
                                <td style="width:10%;  height: 28px;border-right:1px solid #ccc; border-bottom:1px solid #ccc;">
                                    <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin:0; font-weight:normal; padding:4px; text-align: center;"></p>
                                </td>
                                <td style="width:20%; height: 28px; border-right:1px solid #ccc; border-bottom:1px solid #ccc;">
                                    <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin:0; font-weight:normal; padding:4px; text-align: center;"></p>
                                </td>
                                <td style="width:60%;  height: 28px;border-right:1px solid #ccc; border-bottom:1px solid #ccc;">
                                    <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin:0; font-weight:normal; padding:4px; text-align: center;">Total</p>
                                </td>
                                <td style="width:60%;  height: 28px;border-right:1px solid #ccc; border-bottom:1px solid #ccc;">
                                    <p style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; margin:0; font-weight:normal; padding:4px; text-align: center;">{{$data['charge']}}</p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height:25px;"></td>
            </tr>            
            <tr>
                <td valign="middle" style="text-align:center; height:30px; padding-left:10px;font-size:15px; Font-weight:bold;" >
                    THANK YOU FOR YOUR BUSINESS!
                </td>
            </tr>
                <tr>
                    <td style="height:25px;"></td>
                </tr>
        </tbody>
    </table>
</div>
</body>
</html>
