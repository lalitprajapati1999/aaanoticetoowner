<!-- <style type="text/css">
    .parsley-errors-list  {
      position: absolute !important;
  }
</style> -->
<style>
 .bootstrap-select :focus{
    outline: 0;
    border-bottom : 1px solid #032c61 !important;
 }
  
 
 .select2class :focus{
    outline: 0;
    height: 47px !important;
    border-bottom : 1px solid #032c61 !important;
}

.select2-container--default .select2-selection--single, .select2-selection .select2-selection--single {
     border-radius: 0;
     padding: 6px 12px;
     height: 34px;
     border: none !important;
     border-bottom: 1px solid #aaa !important;
 }
 
 .select2-container--default .select2-selection--single {
     background: none !important;
     border: 1px solid #aaa;
     border-radius: 4px;
 }
 </style>
@extends('adminlte::page')
@section('content')
<div role="tabpanel" class="tab-pane active" id="workorder">
    <section class="work-order address-book-add-readonly">
        <div class="dashboard-wrapper">
            @if (Session::get('success'))
            <div class="alert alert-success  msg-success">
                <?php echo Session::get('success'); ?>
            </div>
            @endif
            @if (Session::get('error'))
            <div class="alert alert-error">
                <?php echo Session::get('error'); ?>
            </div>
            @endif

            @if(!isset($is_rescind))
            @php
            $is_rescind = 0;
            @endphp
            @endif

            <div class="dashboard-heading">
                @if($is_rescind==0)
                <h1><span>CREATE YOUR OWN - {{strtoupper($notice['name'])}}</span></h1>
                @else
                 <h1><span>CREATE YOUR OWN - {{strtoupper($notice['name'])}} amendment</span></h1>
                @endif
            </div>
            <div class="dashboard-inner-body">
                <div class="notice-tabs">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs display-ib pull-left" role="tablist">
                        <li role="presentation" class="{{ ($tab == 'project')?'active':''}}" id="project_tab"><a href="#project" aria-controls="project" role="tab" data-toggle="tab">Project</a></li>
                        <li role="presentation" class="{{ ($tab == 'recipients')?'active':''}}" id="recipient_tab"><a href="#recepients" aria-controls="recepients" role="tab" data-toggle="tab" >Recipients</a></li>
                        <li role="presentation" id="address_tab" class="{{ ($tab == 'address_verify')?'active':''}}"><a href="#verify_address" aria-controls="verify_address" role="tab" data-toggle="tab" >Address Verification</a></li>

                    </ul>  


                    <div class="correction-div">
                        <div class="display-ib pull-right">
                            <a href="#" class="btn btn-default corr-btn">Corrections</a>
                            <a href="#" class="btn btn-default notice-btn">Notes</a>
                            <div class="corr-block notice-block mCustomScrollbar" data-mcs-theme="light">
                                <div class="corr-block-wrapper" >
                                    <h4>Corrections</h4>
                                    <form id="edit_work_order_correction">
                                        <div class="correction-success alert alert-success" style="display: none"></div>
                                        <div class="correction-error alert alert-danger" style="display:none"></div>
                                        <div class="row">
                                            <input type="text" name="work_order" value="{{$id}}" id="work_order" hidden="">
                                            <div class="col-md-12 col-sm-12">
                                                <div class="input-wrapper full-width">
                                                    <div class="styled-input">
                                                        <input class="form-control"  type="text" value="{{old('correction')}}" name="correction" data-parlsey-required="true" data-parsley-required-message="Correction is required">
                                                        <label>Correction<span class="mandatory-field">*</span></label>
                                                        <span></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12">
                                                <div class="select-opt">
                                                    <label class="col-md-12 control-label" for="textinput"  name="Email*"></label> 
                                                    <select class="selectpicker" name="email" id="correction_email">
                                                        <option value="">Select</option>
                                                        @foreach($note_emails as $email)
                                                        <option value="{{$email}}" @if (old('email') == $email) selected="selected" @endif > {{$email}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div id="correctionloader" style="display: none">
                                                <img width="150px" src="{{asset('images/loader.gif')}}"/>
                                            </div>
                                            <div class="form-bottom-btn">
                                                <a href="#" class="btn btn-primary custom-btn customc-btn submitCorrection"><span>Submit</span></a>
                                            </div>
                                        </div>
                                    </form>
                                    <ul class="correction_div">
                                        @if(isset($corrections) && !empty($corrections))
                                        @foreach($corrections AS $each_correction)
                                        <li>
                                            {{$each_correction->correction}}<br>
                                            Added by:{{$each_correction->name}}<br>
                                            Created at:{{date('m-d-Y',strtotime($each_correction->created_at))}}
                                        </li>
                                        @endforeach
                                        @endif
                                    </ul>
                                    <a class="closebtn">&times;</a>
                                </div>
                            </div>
                            <div class="notes-block notice-block mCustomScrollbar" data-mcs-theme="light">
                                <div class="notes-block-wrapper">
                                    <form id="edit_work_order_note">
                                        <div class="note-success alert alert-success" style="display: none"></div>
                                        <div class="note-error alert alert-danger" style="display:none"></div>
                                        <h4>Notes</h4>
                                        <p>Order Number: {{$id}}</p>
                                        <div class="row">
                                            <input type="text" name="work_order" value="{{$id}}" id="work_order" hidden="">
                                            <div class="col-md-12 col-sm-12">
                                                <div class="input-wrapper full-width">
                                                    <div class="styled-input">
                                                        <input class="form-control"  type="text" value="{{old('note')}}" name="note" data-parlsey-required="true" data-parsley-required-message="Note is required">
                                                        <label>Note<span class="mandatory-field">*</span></label>
                                                        <span></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12">
                                                <div class="select-opt">
                                                    <label class="col-md-12 control-label" for="textinput"  name="Email*"></label> 
                                                    <select class="selectpicker" name="email" id="note_email">
                                                        <option value="">Select</option>
                                                        @foreach($note_emails as $email)
                                                        <option value="{{$email}}" @if (old('email') == $email) selected="selected" @endif > {{$email}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div id="noteloader" style="display: none">
                                                <img width="150px" src="{{asset('images/loader.gif')}}"/>
                                            </div>
                                            <div class="form-bottom-btn">
                                                <a href="#" class="btn btn-primary custom-btn customc-btn submitNote"><span>Submit</span></a>
                                            </div>
                                        </div>
                                    </form>
                                    <ul class="notes_div">
                                        @if(isset($notes) && !empty($notes))
                                        @foreach($notes AS $each_note)
                                        <li>
                                            {{$each_note->note}}<br>
                                            Added by:{{$each_note->name}}<br>
                                            Created at:{{date('m-d-Y',strtotime($each_note->created_at))}}
                                        </li>
                                        @endforeach
                                        @endif
                                    </ul>
                                    <a class="closebtn1">&times;</a>
                                </div>
                            </div>
                        </div>
                        <!--                                <div id="loaderDiv">
                                                        <img id="loading-image" src="{{ asset('loader.gif')}}" style="display:none;"/>
                                                    </div>-->
                    </div>


                    <!-- Tab panes -->
                    <div class="tab-content">
                        @if($tab=="project")
                        <div role="tabpanel" class="tab-pane active" id="project">
                            @elseif($tab=='recipients')
                            <div role="tabpanel" class="tab-pane" id="project">
                                @endif
                                <div class="project-wrapper">
                                    <form method="post" id="create_work_order" action="{{ url('customer/create-your-own/edit/'.$id.'/'.$notice_id.'/'.$is_rescind) }}" data-parsley-validate="" >

                                        <div class="dash-subform">
                                            <div class="row">

                                                <div class="col-md-5">
                                                    <div class="sub-section-title">
                                                        <h2>Project Information</h2>
                                                    </div>
                                                </div>
                                                <div class="col-md-7 pull-right text-right">
                                                     <a href="{{url('customer/create-your-own/work-order-history')}}" class="btn btn-primary custom-btn customb-btn"><span>Back</span></a>
                                                    <a href="{{url('home')}}" class="btn btn-primary custom-btn customb-btn"><span>Cancel</span></a>
                                                    <input type="submit" name="continue" class="btn btn-primary custom-btn customb-btn save_for_later" value="Save For Later" id="save_for_later"/> 
                                                    <input type="submit" name="continue" class="btn btn-primary custom-btn customc-btn"  value="Continue"/>
                                                </div>

                                                <div class="col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="readonly full-width">
                                                            <label class="full-width">Work order no</label>
                                                            <p class="full-width">#{{$id}}</p>
                                                        </div>
                                                    </div>
                                                </div>

                                                @if(isset($parent_work_order) && $parent_work_order != '')

                                                <div class="col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="readonly full-width">
                                                            <label class="full-width">Parent work order</label>

                                                            <p class="full-width">{{$parent_work_order}}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endif
                                                {!! csrf_field() !!}
                                                @if(isset($duplicate) && !empty($duplicate))
                                                <input type="text" name="duplicate" value="{{$duplicate}}" hidden="">
                                                @endif
                                                <input type="text" name="notice_id" value="{{$notice['id']}}" hidden="">
                                                @if($notice_fields_section1)
                                                @foreach($notice_fields_section1 as $fields)

                                                @if($fields->type==6 && $fields->name == 'last_date_on_the_job')
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="date-box full-width">
                                                        <div class="input-wrapper full-width">

                                                            <div class="styled-input">
                                                                <?php $label = str_replace("_", " ", $fields->name); ?>

                                                                <input class="form-control datepicker"  type="text" name="{{$fields->notice_field_id.' '.$fields->name}} " value="{{$fields->value}}" id="{{$fields->name}}" data-parsley-trigger="change focusout">
                                                                <label>{{ucfirst(trans($label))}}</label>

                                                                <span></span>
                                                            </div>
                                                        </div>
                                                        <!-- <button type="button" class="btn btn-default" data-original-title="" title=""><span class="icon-calendar"></span></button> -->
                                                    </div>
                                                </div>
                                                @elseif($fields->type==6)
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="date-box full-width">
                                                        <div class="input-wrapper full-width">

                                                            <div class="styled-input">
                                                                <?php $label = str_replace("_", " ", $fields->name); ?>

                                                                <input class="form-control datepicker" type="text" name="{{$fields->notice_field_id.' '.$fields->name}} " value="{{$fields->value}}" id="{{$fields->name}}" data-parsley-required="true" data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout">
                                                                <label>{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>

                                                                <span></span>
                                                            </div>
                                                        </div>
                                                        <!-- <button type="button" class="btn btn-default" data-original-title="" title=""><span class="icon-calendar"></span></button> -->
                                                    </div>
                                                </div>
                                                @elseif($fields->type==1 && $fields->name == 'contracted_by' )
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="select-opt select2-container select2class">
                                                        <input type="hidden" value="1" name="contracted_by_exist">
                                                        <?php $label = str_replace("_", " ", $fields->name); ?>
                                                        <label class="col-md-12 control-label" for="textinput">{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>
                                                        <select class="js-example-tags form-control contracted_bys" value="{{$fields->value}}" name="{{$fields->notice_field_id.' '.$fields->name}}"  id="contracted_by_select" data-parsley-required="true" data-parsley-required-message="Contrcted by is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true">
                                                            <option value="">Select</option>
                                                            @if(isset($names) && !empty($names))
                                                            @foreach($names as $each_name)
                                                            <option value="{{$each_name->company_name}}" {{$fields->value==$each_name->company_name ? 'selected="selected"' : ''}} >{{$each_name->company_name}}</option>
                                                            @endforeach
                                                            @endif
                                                        </select>

                                                        <span></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-6" >
                                                    <div class="date-box full-width">
                                                        <div class="input-wrapper full-width" id="hidden-panel" style="display:none">
                                                            <div class="styled-input">
                                                                <label class="lableall" for="textinput">Contract Address</label>
                                                                 <input class="form-control"   type="textarea" id="addname" name="" disabled>
                                                            </div>
                                                         </div>
                                                     </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="add-new-btn inner-dash-btn full-width">
                                                        <button type="button" class="btn btn-primary custom-btn add_contact_details contracted_by_recipient" name="add_contact_submit" value="contacts" id="add_contact_contracted_by" data-toggle="modal" data-target="#contactModal"/><span>+ Add Address Book</span>
                                                    </div>
                                                </div>

                                                @elseif($fields->type==1 && $fields->name == 'date_request' )
                                                <?php $label = str_replace("_", " ", $fields->name); ?>
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="date-box  full-width">
                                                        <div class="input-wrapper full-width">
                                                            <input type="text" name="work_order" value="{{$id}}" id="work_order" hidden="">
                                                            <div class="styled-input">

                                                                <input class="form-control"  type="text" name="{{$fields->notice_field_id.' '.$fields->name}}" value="{{date('m-d-Y')}}" data-parsley-required="false" data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout" readonly="" >
                                                                <label class="full-width">Date</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                @elseif($fields->type==1 && $fields->name == 'parent_work_order' )
                                                <div class="col-md-6 col-sm-6">

                                                    <div class="select-opt">
                                                        <?php $label = str_replace("_", " ", $fields->name); ?>
                                                        <label class="col-md-12 control-label" for="textinput">{{ucfirst(trans($label))}}</label>
                                                        <input type="hidden" id="parent_work_order_id" value="{{$fields->value}}"/>
                                                        <input type="hidden" name="parent_work_order_previous" id="parent_work_order_previous" value="{{$fields->value}}"/>
                                                        <select class="form-control" name="{{$fields->notice_field_id.' '.$fields->name}}" value="{{$fields->value}}"  data-show-subtext="true" data-live-search="true" id="soft_doc_parent_work_order">
                                                            <?php if(!empty($fields->value)){ ?>
                                                                <option value="{{$fields->value}}" selected="selected">{{$fields->value}}</option>   
                                                            <?php }else{ ?>
                                                                <option value=" ">Select</option>
                                                            <?php } ?> 

                                                        </select>

                                                    </div>
                                                </div>
                                                @elseif($fields->type==1 && $fields->name == 'project_name' )
                                                <?php $label = str_replace("_", " ", $fields->name); ?>
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="input-wrapper full-width">
                                                        <div class="styled-input">
                                                            <?php $label = str_replace("_", " ", $fields->name); ?>
                                                            <input class="form-control" type="text" name="{{$fields->notice_field_id.' '.$fields->name}}" id="{{$fields->name}}" value="{{$fields->value}}">
                                                            <label>{{ucfirst(trans($label))}}</label>
                                                            <span></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                @elseif($fields->type==1 && $fields->name =='county')
                                                <div class="col-md-6 col-sm-6">
                                                    <?php $label = str_replace("_", " ", $fields->name);
                                                    ?>

                                                    <div class="input-wrapper full-width">
                                                        <div class="styled-input">
                                                            <input class="form-control" value="{{getCounty($fields->value)}}"  type="text" name="recipient_county_id" id="county" data-parsley-required="true" data-parsley-required-message="Select County from the list" data-parsley-trigger="change focusout">
                                                            <input type="hidden" name="{{$fields->notice_field_id.' '.$fields->name}}"  value="{{$fields->value}}"id="recipient_county_id"/>
                                                            <label class="col-md-12 control-label" for="textinput">{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label> 
                                                            <span></span>                                                                
                                                        </div>
                                                        <div id="recipientcountyloader" style="display: none">
                                                            <img width="150px" src="{{asset('images/loader.gif')}}"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            @elseif($fields->type==1 && $fields->name=="project_address_count")
                                                @if(!Auth::user()->hasRole('customer'))
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="input-wrapper full-width">
                                                        <div class="styled-input">
                                                            <input class="form-control"  type="text" name="{{$fields->notice_field_id.' '.$fields->name}}" value="<?php echo $fields->value ?>" id="{{$fields->name}}" data-parsley-required="true" data-parsley-required-message="Project Address Count is required" data-parsley-trigger="change focusout">
                                                            <label>Project Address Count</label>
                                                            <span></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                @else
                                                       <div class="col-md-6 col-sm-6">
                                                    <div class="input-wrapper full-width" @if(strtoupper($notice['name']) =="NOTICE TO OWNER/PRELIMINARY NOTICE")
                                                        style="position: absolute;"
                                                    @endif>
                                                        <div class="styled-input">
                                                            <input class="form-control"  type="text" name="{{$fields->notice_field_id.' '.$fields->name}}" value="<?php echo $fields->value ?>" id="{{$fields->name}}" data-parsley-required="true" data-parsley-required-message="Project Address Count is required" data-parsley-trigger="change focusout">
                                                            <label>Project Address Count</label>
                                                            <span></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endif
                                                 @elseif($fields->type==1 && $fields->name =='total_amount_satisfied')
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="input-wrapper full-width">
                                                            <div class="styled-input">
                                                                <?php $label = str_replace("_", " ", $fields->name); ?>
                                                                <input class="form-control"  type="text" name="{{$fields->notice_field_id.' '.$fields->name}}" value="{{$fields->value}}" id="{{$fields->name}}" data-parsley-required="true"  data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout"  data-parsley-type="digits"  min="1" >
                                                                <label>($){{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>
                                                                <span></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @elseif($fields->type==1)
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="input-wrapper full-width">
                                                        <div class="styled-input">
                                                            <?php $label = str_replace("_", " ", $fields->name); ?>
                                                            <input class="form-control" value="{{$fields->value}}" type="text" name="{{$fields->notice_field_id.' '.$fields->name}}" id="{{$fields->name}}" data-parsley-required="false" data-parsley-required-message="{{ucfirst($label)}} is required" data-parsley-trigger="change focusout" data-parsley-pattern="/^[a-zA-Z0-9 ]*$/">
                                                            <label>{{ucfirst(trans($label))}}    <!-- <span class="mandatory-field">*</span>-->
                                                            </label>

                                                        </div>
                                                    </div>
                                                </div>
                                                 @elseif($fields->type==1 && $fields->name=="project_address_count")
                                                @if(!Auth::user()->hasRole('customer'))
                                                {{$fields->name}}
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="input-wrapper full-width">
                                                        <div class="styled-input">
                                                            <input class="form-control"  type="text" name="{{$fields->notice_field_id.' '.$fields->name}}" value="<?php echo $fields->value ?>" id="{{$fields->name}}" data-parsley-required="true" data-parsley-required-message="Project Address Count is required" data-parsley-trigger="change focusout">
                                                            <label>Project Address Count</label>
                                                            <span></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                @else
                                                {{$fields->name}}
                                                <input class="form-control"  type="hidden" name="{{$fields->notice_field_id.' '.$fields->name}}" value="1" id="{{$fields->name}}" data-parsley-required="true" data-parsley-required-message="Project Address Count is required" data-parsley-trigger="change focusout">
                                                @endif


                                                @elseif($fields->type==2 && $fields->name=="project_address")
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="input-wrapper full-width">
                                                        <?php $label = str_replace("_", " ", $fields->name);
                                                        $projects_address = preg_replace("/<br \/>/", "<p>", $fields->value);
                                                        ?>
                                                        <label class="col-md-12 control-label">{{ucfirst(trans($label))}}<span class="mandatory-field">*</span> (Please enter multiple addresses separated by **)</label>
                                                        <div class="styled-input">
                                                            <textarea onkeyup="countAddress(this)" class="form-control projects_address" name="{{$fields->notice_field_id.' '.$fields->name}}" id="{{$fields->name}}" data-parsley-required="true" data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout"  rows="5">{{$projects_address}}</textarea>
                                                            <span></span>
                                                        </div>
                                                    </div>
                                                </div>

                                                @elseif($fields->type==2)
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="input-wrapper full-width">
                                                        <div class="styled-input">
                                                            <?php $label = str_replace("_", " ", $fields->name);
                                                                  $label = $label."/Correction";?>
                                                            <textarea id="{{$fields->name}}" class="form-control" name="{{$fields->notice_field_id.' '.$fields->name}}" data-parsley-required="true" data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout">{{strip_tags($fields->value)}}</textarea>


                                                            <label>{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>
                                                            <span></span>
                                                        </div>
                                                    </div>
                                                </div>

                                                @elseif($fields->type==3 && $fields->name == 'project_type')
                                                <?php $project_type = $fields->value; ?>
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="select-opt">
                                                        <?php $label = str_replace("_", " ", $fields->name); ?>
                                                        <label class="col-md-12 control-label" for="textinput">{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>  
                                                        <select class="selectpicker"  name="{{$fields->notice_field_id.' '.$fields->name}}" data-parsley-required="true" data-parsley-required-message="Project type is required" data-parsley-trigger="change focusout">
                                                            <option value="" >Select Type</option>
                                                            @foreach($projectTypes as $type)
                                                            <option value="{{$type->id}}" {{$fields->value==$type->id ? 'selected="selected"' : ''}}>{{$type->type}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                @elseif($fields->type==3 && $fields->name == 'your_role')
                                                <?php $your_role = $fields->value; ?>
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="select-opt">
                                                        <?php $label = str_replace("_", " ", $fields->name); ?>
                                                        <label class="col-md-12 control-label" for="textinput">{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>  
                                                        <select class="selectpicker" name="{{$fields->notice_field_id.' '.$fields->name}}" data-parsley-required="true" data-parsley-required-message="Your role is required" data-parsley-trigger="change focusout">
                                                            <option value="">Select Role</option>
                                                            @foreach($project_roles as $category)
                                                            <option value="{{$category->id}}" {{ $fields->value == $category->id ? 'selected="selected"' : '' }}>{{$category->name}}</option>
                                                            @endforeach


                                                        </select>
                                                    </div>
                                                </div>

                                                @elseif($fields->type==3 && $fields->name =='city')
                                                <?php $city_id = $fields->value; ?>
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="select-opt">
                                                        <?php $label = str_replace("_", " ", $fields->name); ?>
                                                        <label class="col-md-12 control-label" for="textinput">{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>  
                                                        <select class="selectpicker" name="{{$fields->notice_field_id.' '.$fields->name}}" id="soft_notice_city_value" data-parsley-required="true" data-parsley-required-message="City is required" data-parsley-trigger="change focusout">

                                                            @foreach($cities as $city)
                                                            <option value="{{$city->id}}" {{ $city_id == $city->id ? 'selected="selected"' : '' }}>{{$city->name}}</option>
                                                            @endforeach 
                                                        </select>
                                                    </div>
                                                </div>

                                                @elseif($fields->type==3 && $fields->name =='state')
                                                <?php $state_id = $fields->value; ?>
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="select-opt">
                                                        <?php $label = str_replace("_", " ", $fields->name); ?>
                                                        <label class="col-md-12 control-label" for="textinput" >{{ucfirst(trans($label))}}</label>  
                                                        <select class="selectpicker" id="soft_notice_state_value"  name="{{$fields->notice_field_id.' '.$fields->name}}" data-parsley-required="true" data-parsley-required-message="State is required" data-parsley-trigger="change focusout">
                                                            <option value="">Select State</option>
                                                            @foreach($states as $state)
                                                            <option value="{{$state->id}}" @if ($fields->value == $state->id) selected="selected" @endif >{{$state->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                @elseif($fields->type==3 && $fields->name =='county')
                                                <?php $country_id = $fields->value; ?>
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="select-opt">
                                                        <?php $label = str_replace("_", " ", $fields->name); ?>
                                                        <label class="col-md-12 control-label" for="textinput"  >{{ucfirst(trans($label))}}</label>  

                                                        <select class="selectpicker" name="{{$fields->notice_field_id.' '.$fields->name}}" id="country_id" value="{{$fields->value}}" data-parsley-required="true" data-parsley-required-message="Country is required" data-parsley-trigger="change focusout">

                                                            @foreach($countries as $country)
                                                            <option value="{{$country->id}}" {{ $country_id == $country->id ? 'selected="selected"' : '' }}>{{$country->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                @elseif($fields->type==3 && $fields->name =='authorise_agent')



                                                <div class="col-md-6 col-sm-6">
                                                    <div class="select-opt">
                                                        <?php $label = str_replace("_", " ", $fields->name); ?>
                                                        <label class="col-md-12 control-label" for="textinput">{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>  
                                                        <select 
                                                            <select class="selectpicker" name="{{$fields->notice_field_id.' '.$fields->name}}" id="{{$fields->name}}" class="selectpicker"  data-parsley-required="true" data-parsley-required-message="Authorise signature is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true">
                                                                <option value="">Select</option>
                                                                @if(isset($officers_directors) && count($officers_directors)>0)
                                                                @foreach($officers_directors as $officer)
                                                                <option value="{{$officer->first_name.' '.$officer->last_name}}" @if ($fields->value == $officer->first_name.' '.$officer->last_name) selected="selected" @endif > {{ucfirst(trans($officer->first_name.' '.$officer->last_name))}}</option>
                                                                @endforeach
                                                                @endif
                                                            </select>
                                                    </div>
                                                </div>
                                                @elseif($fields->type==3 && $fields->name =='title')



                                                <div class="col-md-6 col-sm-6">
                                                    <div class="select-opt">
                                                        <?php $label = str_replace("_", " ", $fields->name); ?>
                                                        <label class="col-md-12 control-label" for="textinput">{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>  
                                                        <select 
                                                            <select class="selectpicker" name="{{$fields->notice_field_id.' '.$fields->name}}" id="{{$fields->name}}" class="selectpicker"  data-parsley-required="true" data-parsley-required-message="Authorise signature is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true">
                                                                <option value="">Select</option>
                                                                @if(isset($officers_directors) && count($officers_directors)>0)
                                                                @foreach($officers_directors as $officer)
                                                                <option value="{{$officer->title}}" @if ($fields->value == $officer->title) selected="selected" @endif > {{ucfirst(trans($officer->title))}}</option>
                                                                @endforeach
                                                                @endif
                                                            </select>
                                                    </div>
                                                </div>

                                                @elseif($fields->type==4)
                                                <?php $label = str_replace("_", " ", $fields->name); ?>
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="input-wrapper full-width">
                                                        <div class="styled-input">      
                                                            <input class="checkbox" type="checkbox" name=""  value="1" id="attorneys_fees">
                                                            <label>
                                                                Attorneys Fees*
                                                            </label>  
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="col-md-6 col-sm-6" id="showattorneysfee">
                                                    <div class="input-wrapper full-width">
                                                        <div class="styled-input">


                                                            <input class="form-control" type="textbox" name="{{$fields->notice_field_id.' '.$fields->name}}"  value="" id="{{$fields->name}}" placeholder="%">
                                                            <label>{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>
                                                        </div>
                                                    </div>           
                                                </div>


                                                @elseif($fields->type==5)
                                                <?php $label = str_replace("_", " ", $fields->name); ?>
                                                <div class="col-md-4 col-sm-6">
                                                    <div class="date-box full-width">
                                                        <div class="input-wrapper full-width">
                                                            <div class="styled-input">
                                                                <input class="form-control datepicker" required=""  type="radio" name="{{$fields->notice_field_id.' '.$fields->name}}" value="{{$fields->value}}" data-parsley-required="true" data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout">
                                                                <label>{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>
                                                                <span></span>
                                                            </div>
                                                        </div>
                                                       <!--  <button type="button" class="btn btn-default" data-original-title="" title=""><span class="icon-calendar"></span></button> -->
                                                    </div>
                                                </div>
                                                @endif
                                                @endforeach
                                                @endif

                                            </div>
                                        </div>
                                        <div class="dash-subform">
                                            <div class="row">
                                                <div class=" col-md-12">
                                                    <div class="sub-section-title">
                                                        <h2>Service/Labor Furnished</h2>
                                                    </div>	
                                                </div>				
                                                @if($notice_fields_section2)
                                                @foreach($notice_fields_section2 as $fields)
                                                @if($fields->type==2 && $fields->name=="folio_s"||$fields->name == 'legal_description')

                                                <div class="col-md-6 col-sm-6">
                                                    <div class="input-wrapper full-width">
                                                        <div class="styled-input">
                                                            <?php $label = str_replace("_", " ", $fields->name); ?>
                                                            <textarea class="form-control"  type="text" name="{{$fields->notice_field_id.' '.$fields->name}}" id="{{$fields->name}}"  data-parsley-required="false" data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout" >{{strip_tags($fields->value)}}</textarea>
                                                            <label>{{ucfirst(trans($label))}} </label>
                                                            <span></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                @elseif($fields->type==2 && $fields->name == 'labor_service_furnished')
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="input-wrapper full-width">
                                                        <div class="styled-input">
                                                            <?php $label = str_replace("_", " ", $fields->name); ?>
                                                            <textarea class="form-control"  type="text" name="{{$fields->notice_field_id.' '.$fields->name}}" id="{{$fields->name}}" data-parsley-required="true" data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout">{{strip_tags($fields->value)}}</textarea>
                                                            <label>{{ucfirst(trans($label))}}<span class="mandatory-field">*</span> </label>
                                                            <span></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                @elseif($fields->type==1)
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="input-wrapper full-width">
                                                        <div class="styled-input">
                                                            <?php $label = str_replace("_", " ", $fields->name); ?>
                                                            <input class="form-control" type="text" name="{{$fields->notice_field_id.' '.$fields->name}}" value="{{$fields->value}}"  data-parsley-required="true" data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout" data-parsley-pattern="/^[a-zA-Z0-9 ]*$/">
                                                            <label>{{ucfirst(trans($label))}} </label>
                                                            <span></span>
                                                        </div>
                                                    </div>
                                                </div>



                                                @elseif($fields->type==2 && $fields->name=="project_address")
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="input-wrapper full-width">
                                                        <?php $label = str_replace("_", " ", $fields->name);
                                                        $projects_address = preg_replace("/<br \/>/", "<p>", $fields->value);
                                                        ?>
                                                        <label class="col-md-12 control-label">Project Address & Legal Description<span class="mandatory-field">*</span></label>
                                                        <div class="styled-input">
                                                            <textarea id="{{$fields->name}}" class="form-control projects_address" name="{{$fields->notice_field_id.' '.$fields->name}}"  data-parsley-required="true" data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout" rows="5">{{$projects_address}}</textarea>
                                                            <span></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                @elseif($fields->type==2)
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="input-wrapper full-width">
                                                        <div class="styled-input">
                                                            <?php $label = str_replace("_", " ", $fields->name); ?>
                                                            <textarea  class="form-control" name="{{$fields->notice_field_id.' '.$fields->name}}" id="{{$fields->name}}"  data-parsley-required="true" data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout">{{strip_tags($fields->value)}}</textarea>


                                                            <label>{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>
                                                            <span></span>
                                                        </div>
                                                    </div>
                                                </div>



                                                @elseif($fields->type==3 && $fields->name =='city')
                                                <input type="hidden" name="hidden_city_val" id="hidden_city_val"value="{{$fields->value}}">
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="select-opt">
                                                        <?php $label = str_replace("_", " ", $fields->name); ?>
                                                        <label class="col-md-12 control-label" for="textinput">{{ucfirst(trans($label))}}</label>  
                                                        <select class="selectpicker" name="{{$fields->notice_field_id.' '.$fields->name}}" data-parsley-required="true" data-parsley-required-message="City is required" data-parsley-trigger="change focusout">
                                                            <option value="">Select City</option>
                                                            @foreach($cities as $city)
                                                            <option value="{{$city->id}}">{{$city->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                @elseif($fields->type==3 && $fields->name =='state')
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="select-opt">
                                                        <?php $label = str_replace("_", " ", $fields->name); ?>
                                                        <label class="col-md-12 control-label" for="textinput"  >{{ucfirst(trans($label))}}</label>  
                                                        <select class="selectpicker" name="{{$fields->notice_field_id.' '.$fields->name}}" data-parsley-required="true" data-parsley-required-message="State is required" data-parsley-trigger="change focusout">
                                                            <option value="">Select State</option>
                                                            @foreach($states as $state)
                                                            <option value="{{$state->id}}">{{$state->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                @elseif($fields->type==3 && $fields->name =='county')
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="select-opt">
                                                        <?php $label = str_replace("_", " ", $fields->name); ?>
                                                        <label class="col-md-12 control-label" for="textinput"  >{{ucfirst(trans($label))}}</label>  
                                                        <select class="selectpicker" name="{{$fields->notice_field_id.' '.$fields->name}}" data-parsley-required="true" data-parsley-required-message="Country is required" data-parsley-trigger="change focusout">

                                                            @foreach($countries as $country)
                                                            <option value="{{$country->id}}">{{$country->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>


                                                @elseif($fields->type==4)
                                                <?php $label = str_replace("_", " ", $fields->name); ?>
                                                <div class="col-md-6 col-sm-6 clear-b">
                                                    <div class="date-box full-width">
                                                        <div class="input-wrapper full-width">
                                                            <div class="styled-input">
                                                                <input class="form-control datepicker" required="" type="checkbox" name="{{$fields->notice_field_id.' '.$fields->name}}" value="{{$fields->value}}" data-parsley-required="true" data-parsley-required-message="Country is required" data-parsley-trigger="change focusout">
                                                                <label>{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>
                                                                <span></span>
                                                            </div>
                                                        </div>
                                                       <!--  <button type="button" class="btn btn-default" data-original-title="" title=""><span class="icon-calendar"></span></button> -->
                                                    </div>
                                                </div>

                                                @elseif($fields->type==5)
                                                <?php $label = str_replace("_", " ", $fields->name); ?>
                                                <div class="col-md-4 col-sm-6">
                                                    <div class="date-box full-width">
                                                        <div class="input-wrapper full-width">
                                                            <div class="styled-input">
                                                                <input class="form-control datepicker" required=""  type="radio" name="{{$fields->notice_field_id.' '.$fields->name}}" value="{{$fields->value}}" data-parsley-required="true" data-parsley-required-message="Country is required" data-parsley-trigger="change focusout">
                                                                <label>{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>
                                                                <span></span>
                                                            </div>
                                                        </div>
                                                       <!--  <button type="button" class="btn btn-default" data-original-title="" title=""><span class="icon-calendar"></span></button> -->
                                                    </div>
                                                </div>
                                                @endif
                                                @endforeach
                                                @endif
                                                <?php
                                                $document_types = getDocumentTypes();
                                                ?>
                                                <div class="col-md-12 col-sm-12">
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                                            <div class="select-opt mt-top">
                                                                <label class="col-md-12 control-label" for="textinput">Document Type</label>  
                                                                <select class="selectpicker" name="type" id="soft_notice_work_order_doc_type">
                                                                    <option value="">-Select-</option>
                                                                    @foreach ($document_types as $key => $value) 
                                                                    <option value="{{$key}}">{{$value}}</option>
                                                                    @endforeach
                                                                </select>
                                                                @if ($errors->has('type'))
                                                                <span class="help-block">
                                                                    <strong>{{ $errors->first('type') }}</strong>
                                                                </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-sm-12 soft_notice_work_order_doc_field">
                                                            <div class="row">
                                                                <div class="col-md-12 col-sm-12">
                                                                    <div class="input-wrapper full-width">
                                                                        <div class="styled-input">
                                                                            <input class="form-control" name="doc_input"  type="text" id="soft_notice_work_order_doc_input">
                                                                            <label id="doc_field"></label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <input type="hidden" name="edit_document_name" id="edit_document_name">
                                                        <input type="hidden" name="edit_doc_id" id="edit_doc_id">
                                                        <input type="hidden" id="row_id">

                                                        <div class="form-btn-div soft_notice_work_order_doc_file">
                                                            <span class="btn btn-default btn-file upload-btn btn btn-primary form-btn" id="temp">
                                                                Choose File to Upload  <input type="file" name="file_name" id="soft_notice_work_order_result_file">
                                                            </span>
                                                            (Allow Types:csv,xls,xlsx,pdf,doc,docx, jpg, jpeg)
                                                            <span id="errormessage" style="display: none"></span>
                                                        </div>
                                                        <div class="form-btn-div doc_file col-md-12 col-sm-12 col-xs-12" >
                                                            <div style="float: left;width: 10">
                                                                <a href="javascript:;" onClick="openTab(this)" id="open_doc" name=""><span  id="preview" style="display: none;" ></span></a></div>
                                                            <div style="float: left ;width: 10;padding-left: 20px">
                                                                <a  href="javascript:;" ><span id="remove_icon" style="display: none;"  >Delete</span></a></div>

                                                            <input type="hidden" name="remove_file" id="remove_file" value="0">
                                                        </div>
                                                        <div class="col-md-12 col-sm-12 col-xs-12 soft_notice_work_order_doc_file">

                                                            <div class="add-new-btn inner-dash-btn full-width ">
                                                                <button  name="submit_doc" type="button" value="Add" id="submit_document" class="btn btn-primary custom-btn">
                                                                    <span class="update_doc">Add</span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-sm-12">
                                                            <div class="row">
                                                                <div class="col-lg-12 col-md-12 col-sm-12">
                                                                    <div class="dahboard-table doc-table full-width table-responsive">
                                                                        <table class="table table-striped" id="soft_notice_work_order_document_table">
                                                                            <thead class="thead-dark">
                                                                                <tr>
                                                                                    <th scope="col">Document Type</th>
                                                                                    <th scope="col">Document Description</th>
                                                                                    <th scope="col">Name</th>
                                                                                    <th scope="col">Date & Time</th>
                                                                                    <th scope="col">Type of Attachment</th>
                                                                                    <th scope="col">Options</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                @if(isset($attachment) && !empty($attachment) && count($attachment)>0)
                                                                                @foreach($attachment AS $each_attachment)
                                                                                <?php $file_name = explode('.', $each_attachment->file_name) ?>
                                                                                <tr id="refreshdelete{{$each_attachment->id}}">
                                                                                    <td>
                                                                                        @foreach ($document_types as $key => $value) 
                                                                                        @if ($each_attachment->type == $key) 
                                                                                        {{$value}}
                                                                                        @endif
                                                                                        @endforeach
                                                                                    </td>
                                                                                    <td>{{$each_attachment->title}}</td>
                                                                                    <td><a href="{{asset('attachment/cyo_work_order_document/'.$each_attachment->file_name)}}" target="_blank">{{$each_attachment->original_file_name}}</a></td>

                                                                                    <td>{{date('d M y h:i A',strtotime($each_attachment->created_at))}}</td>
                                                                                    <td>
                                                                                        <?php
                                                                                        $extension = explode('.', $each_attachment->original_file_name);
                                                                                        echo $ext=$each_attachment->original_file_name != "" ? $extension[1] : "";
                                                                                        ?>

                                                                                    </td>
                                                                                    <td>
                                                                                        <a class='btn btn-secondary item red-tooltip remove-attachment' data-toggle='modal'
                                                                                           data-url='{{url('customer/edit/cyo-work-ordwer/removeattachment')}}'  data-id="{{$each_attachment->id}}" data-file='{{$each_attachment->file_name}}' 
                                                                                           data-target='#attachment-confirmation-modal' data-placement='bottom' title='Remove'>
                                                                                            <span class='icon-cross-remove-sign'></span></a>
                                                                                        <a class='btn btn-secondary item red-tooltip edit-attachment'
                                                                                           data-id="{{$each_attachment->id}}" data-file='{{$each_attachment->file_name}}' data-type="{{$each_attachment->type}}" data-title="{{$each_attachment->title}}" data-file-original-name="{{$each_attachment->original_file_name}}" data-file-extension="{{$ext}}">
                                                                                            <span class='icon-pencil-edit-button'></span>
                                                                                        </a>
                                                                                        @if(!empty($each_attachment->file_name))
                                                                                        <a href="{{asset('attachment/cyo_work_order_document/'.$each_attachment->file_name)}}" target="_blank" class="btn btn-secondary item red-tooltip"><i class="fa fa-eye" style=""></i></a>
                                                                                        @endif
                                                                                    </td>
                                                                                </tr>
                                                                                @endforeach
                                                                                @else
                                                                            <td class="text-center norecords" colspan="6">No record found</td>
                                                                            @endif
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <input type="hidden" name="edit_soft_notices_attachment[]" id="edit_soft_notices_attachment"/>
                                                <input type="hidden" name="remove_doc[]" id="remove_doc"/>

                                                <div class="form-bottom-btn save_btn1">
                                                    <a href="{{url('customer/create-your-own/work-order-history')}}" class="btn btn-primary custom-btn customb-btn"><span>Back</span></a>
                                                    <a href="{{url('home')}}" class="btn btn-primary custom-btn customb-btn"><span>Cancel</span></a>
                                                    <input type="submit" name="continue" class="btn btn-primary custom-btn customb-btn save_for_later" value="Save For Later" id="save_for_later"/> 
                                                    <input type="submit" name="continue" class="btn btn-primary custom-btn customc-btn"  value="Continue"/>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>

                            @if($tab=="project")
                            <div role="tabpanel" class="tab-pane" id="recepients">
                                @elseif($tab=='recipients')
                                <div role="tabpanel" class="tab-pane active" id="recepients">
                                    @endif
                                    <div class="project-wrapper">
                                        <form id="store_recipients" action="{{url('customer/create-your-own/recipients/'.$id.'/'.$notice_id)}}" method="post" data-parsley-validate="">

                                            {!! csrf_field() !!}  
                                            <div class="dash-subform mt-0">
                                                <div class="row">
                                                     <div class="row">
                                                           
                                                            <div  class="col-md-12 pull-right text-right">
                                                                <a type="button" id="editBack" name="editBack" class="btn btn-primary custom-btn customb-btn not-empty" value="Back"><span>back</span></a>
                                                                <a href="{{url('home')}}" class="btn btn-primary custom-btn customb-btn"><span>Cancel</span></a>
                                                                <a href="{{url('customer/create-your-own/updateStatus/'.$id)}}" class="btn btn-primary custom-btn customb-btn"><span>Save for Later</span></a>

                                                               <input type="button" id="createLabel" name="submit_continue" class="btn btn-primary custom-btn customc-btn" value="Continue"/>  
                                                            </div>
                                                        </div>
                                                    <div class="col-md-4">
                                                       
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="sub-section-title">
                                                                    <h2>Recipients</h2>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="dahboard-table table-responsive">
                                                                    <table class="table table-striped">
                                                                        <thead class="thead-dark">
                                                                            <tr>
                                                                                <th scope="col">Category</th>
                                                                                <th scope="col">Recipients</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>

                                                                            @if(isset($count) && count($count)>0)
                                                                            @foreach($count as $key => $count)
                                                                            <tr>

                                                                                <td class="checkbox-holder">
                                                                                    <input type="checkbox" id="{{$key}}" class="regular-checkbox"><label for="{{$key}}">{{$count->name}}</label>
                                                                                </td>
                                                                                <td>{{$count->total}}</td>
                                                                            </tr>
                                                                            @endforeach
                                                                            @else
                                                                        <td class="text-center" colspan="4">{{'No records found'}}</td>
                                                                        @endif

                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="sub-section-title">
                                                                    <h2>Selected Recipients</h2>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="dahboard-table table-responsive">
                                                                    <table class="table table-striped">
                                                                        <thead class="thead-dark">
                                                                            <tr>
                                                                                <th scope="col">Role</th>
                                                                                <th scope="col">Company Name</th>
<!--                                                    <th scope="col">Contact Person</th>-->
<!--                                                    <th scope="col">Phone Number</th>-->
                                                                                <th scope="col">Address</th>
                                                                                <th scope="col">City</th>
                                                                                <th scope="col">State</th>
                                                                                <th scope="col">Zip Code</th>
                                                                                <th scope="col">Address Verified</th>
                                                                                <th scope="col">Options</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>


                                                                            @if(isset($recipients) && count($recipients) > 0)

                                                                            @foreach($recipients  as $recipient)
                                                                            <tr>
                                                                                <td class="cl-blue">{{$recipient->category->name}}</td>
                                                                                <td>{{$recipient->name}}</td>
    <!--                                                                            <td>{{$recipient->contact}}</td>-->
    <!--                                                                            <td>{{$recipient->mobile}}</td>-->
                                                                                <td>
                                                                                  
                                                                                    {{$recipient->address}}
                                                                                    
                                                                                </td>
                                                                                <td>{{$recipient->city->name}}</td>
                                                                                <td>{{$recipient->state->name}}</td>
                                                                                <td>{{$recipient->zip}}</td>
                                                                                <td>
                                                                                    {{ ((isset($recipient->cyo_usps_address) && $recipient->cyo_usps_address != '')?'Yes':'No') }}
                                                                                </td>
                                                                                <td>
                                                                                    <a href="#recipient" data-id="{{$recipient->id}}" class="edit_recipient"><span class="icon-pencil-edit-button"></span></a>
                                                                                    <a data-id="{{$recipient->id}}" data-notice-id="{{$notice_id}}" data-workorder-id="{{$id}}" href="#" class="remove-record" data-toggle='modal' data-toggle="tooltip" data-target='#contact-confirmation-modal' data-placement="bottom" title="Remove"><span class="icon-cross-remove-sign"></span></a>
                                                                                </td>
                                                                            </tr>
                                                                            @endforeach
                                                                            @else
                                                                        <td class="text-center" colspan="4">{{'No records found'}}</td>
                                                                        @endif


                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="dash-subform" id="recipient">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="sub-section-title">
                                                            <h2>Recipients</h2>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <div class="row">
                                                            <div class="col-md-6 col-sm-6">

                                                                <div class="select-opt">
                                                                    <input type="text" name="work_order_id" value="{{$id}}" hidden="">
                                                                    <input type="hidden" name="recipient_id" id="recipient_id">
                                                                    <label class="col-md-12 control-label" for="textinput">Type of Recipient<span class="mandatory-field">*</span></label>  
                                                                    <select class="selectpicker" name="category_id" data-parsley-required="true" data-parsley-required-message="Type of recipient is required" id="category_id">
                                                                        <option value="">Select</option>
                                                                        @foreach($categories as $category)
                                                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="select-opt select2-container select2class" >
                                                            <label class="col-md-12 control-label" for="textinput">Name<span class="mandatory-field">*</span></label>  
                                                            <select class="js-example-tags form-control recipient_name"  name="name" id="recipient_name" data-parsley-required="true" data-parsley-required-message="Name is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true">
                                                                <option value="">Select</option>
                                                                @if(isset($names) && !empty($names))
                                                                @foreach($names as $each_name)
                                                                <option  data-id="{{$each_name->id}}" value="{{$each_name->company_name}}">{{$each_name->company_name}}</option>
                                                                @endforeach
                                                                @endif
                                                            </select>
                                                            <input type="hidden" name="recipt_id"  value="" id="recipt_id">

                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-bottom-btn pd-25">

                                                            <button type="button" name="add_contact_submit" class="btn btn-primary custom-btn customc-btn add_contact_details" value="contacts" id="add_contact" data-toggle="modal" data-target="#contactModal"><span>Add/Update Address Book</span></button>
                                                            <button type="button" name="add_contact_submit" class="btn btn-primary custom-btn customc-btn update_contact" value="contacts" id="update_contact" ><span>Update Address Book</span></button>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="input-wrapper full-width">
                                                            <label class="col-md-12 control-label" for="textarea">Attn/Additional Recipient Name<span class="mandatory-field"></span></label>
                                                            <div class="styled-input">
                                                                <textarea class="form-control recipient_attns"  type="text" name="attn" id="recipient_attn" data-parsley-trigger="change focusout"></textarea>
                                                                <span></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="input-wrapper full-width">
                                                            <div class="styled-input">
                                                                <input class="form-control business_phone_validation"  type="text" name="contact" id="recipient_contact" data-parsley-trigger="change focusout" data-parsley-pattern="(\(\d{3})\)\s\d{3}\-\d{4}$">

                                                                <label>Telephone</label>
                                                                <span></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--                                                        <div class="col-md-6 col-sm-6 clear-b">
                                                                                                                <div class="input-wrapper full-width">
                                                                                                                    <div class="styled-input">
                                                    
                                                                                                                        <input class="form-control" type="text" name="mobile"  id="reipient_mobile" data-parsley-required="true" data-parsley-required-message="Mobile number is required" data-parsley-trigger="change focusout">
                                                                                                                        <label>Mobile Number</label>
                                                                                                                        <span></span>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>-->
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="input-wrapper full-width">
                                                            <div class="styled-input">

                                                                <input class="form-control email_validation" type="text" name="email" id="recipient_email" > 
                                                                <label>Email</label>
                                                                <span></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 col-sm-12">
                                                        <div class="input-wrapper full-width">
                                                            <div class="styled-input">

                                                                <textarea class="form-control"  type="text" name="address" id="recipient_address"  data-parsley-required="true" data-parsley-required-message="Address is required" data-parsley-trigger="change focusout"></textarea>

                                                                <label>Address<span class="mandatory-field">*</span></label>
                                                                <span></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                   <div class="col-md-6 col-sm-6 clear-b">
                                                           <div class="input-wrapper full-width">
                                                               <div class="styled-input">
                                                                   <input class="form-control" type="hidden" name="zip" id="recipient_zip" data-parsley-required="true" data-parsley-required-message="Zip code is required" data-parsley-trigger="change focusout">
                                                                   <input class="form-control new_reipient_zip"  type="text" name="new_reipient_zip" value="{{old('new_reipient_zip')}}" id="new_addresscontact_zip">
                                                                   <label>Zip Code<span class="mandatory-field">*</span></label>
                                                                   <span></span>
                                                               </div>
                                                           </div>
                                                       </div>

                                                   <!--  <div class="col-md-6 col-sm-6">

                                                        <div class="input-wrapper full-width">
                                                            <div class="styled-input">
                                                                <input class="form-control"  type="text" name="city_id" id="recipient_city" data-parsley-required="true" data-parsley-required-message="Select City from the list" data-parsley-trigger="change focusout
                                                                ">
                                                                <input type="hidden" name="recipient_city_id" id="recipient_city_id"/>
                                                                <label class="col-md-12 control-label" for="textinput">City<span class="mandatory-field">*</span></label> 
                                                                <span></span>                                                                
                                                            </div>
                                                            <div id="recipientloader" style="display: none">
                                                                <img width="150px" src="{{asset('images/loader.gif')}}"/>
                                                            </div>
                                                        </div>
                                                    </div>

 -->
                                                     <div class="col-md-6 col-sm-6">
                                                        <div class="select-opt select2-container select2class">
                                                        <label class="col-md-12 control-label" for="textinput">City<span class="mandatory-field">*</span></label> 
                                                            <select class="js-example-tags form-control load_ajax_cities"  name="city_id" id="recipient_city"  data-parsley-required-message="City is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true">
                                                                <option value="">Select</option>    
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 clear-b">
                                                        <div class="select-opt">
                                                            <label class="col-md-12 control-label" for="textinput">State* </label>  
                                                            <select class="selectpicker" id="recipient_state" name="state_id" data-parsley-required="true" data-parsley-required-message="State is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true">
                                                                <option value="">Select</option>
                                                                @foreach($states as $state)
                                                                <option value="{{$state->id}}">{{$state->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                  
                                                    <input type="hidden" name="verified_cyo_usps_address" id="verified_cyo_usps_address">
                                                    <!--                                                        <div class="col-md-6 col-sm-6 ">
                                                                                                                <div class="select-opt">
                                                                                                                    <label class="col-md-12 control-label" for="textinput">County</label>  
                                                    
                                                    
                                                                                                                    <select class="selectpicker"name="country_id" id="recipient_county" data-show-subtext="true" data-live-search="true">
                                                                                                                        <option value="">Select</option>
                                                                                                                        @foreach($countries as $county)
                                                                                                                        <option value="{{$county->id}}">{{$county->name}}</option>
                                                                                                                        @endforeach
                                                                                                                    </select>
                                                                                                                </div>
                                                    
                                                                                                            </div>-->




                                                    <div class="col-md-12">
                                                        <div class="form-bottom-btn pd-25">
                                                            <input type="hidden" name="verified_addr" id="verified_addr" value="false">
                                                            <input type="hidden" name="continue_val" id="continue_val"/>

                                                            <button id="add_recipients" type="submit" name="continue" class="btn btn-primary custom-btn customc-btn add_recipient check_button_val" value="add"><span>Add Recipient</span></button>
                                                            <button id="update_recipients" type="submit" name="continue"  class="btn btn-primary custom-btn customc-btn update_recipient check_button_val" value="update" disabled=""><span>Update Recipient</span></button>
                                                            <div id="ajax_loader"></div>
                                                        </div>
                                                    </div>
                                                    </form>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-bottom-btn">
                                                            <!--<input type="button" id="editBack" name="editBack" class="btn btn-primary custom-btn customb-btn" value="Back"/>-->
                                                            <a type="button" id="editBack" name="editBack" class="btn btn-primary custom-btn customb-btn not-empty" value="Back"><span>back</span></a>
                                                            <a href="{{url('home')}}" class="btn btn-primary custom-btn customb-btn"><span>Cancel</span></a>
                                                            <a href="{{url('customer/create-your-own/updateStatus/'.$id)}}" class="btn btn-primary custom-btn customb-btn"><span>Save for Later</span></a>

                                                            <input type="button" id="createLabel" name="submit_continue" class="btn btn-primary custom-btn customc-btn" value="Continue"/>  

     <!-- <a  href="{{url('customer/create-your-own/printpdf/'.$id)}}" name="submit" class="btn btn-primary custom-btn customc-btn" value="submit" target="_blank"><span>Submit</span></a>  --> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>



                                </div>
                                @include('customer.create_your_own.verify_address')
                            </div>
                        </div>
                    </div>
                    </section>
                    <input type="text" name="wo_id" id="wo_id" value="{{$id}}" hidden="">
                    <div id="address_verification_modal" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Verified Addresses</h4>
                                </div>
                                <div class="modal-body">
                                    <div id="alternate_addresses"></div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" id="choose_address">Use chosen address</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- add new contacts-->

                <div id="contactModal" class="modal fade register-modal addcon-modal">
   
   
                       <div class="modal-dialog">
                           <div class="modal-content">
                               <div class="modal-header">
                                   <div id="success-msg"class="alert alert-success" style="display: none"></div>
                                   <div id="error-msg"class="alert alert-error" style="display: none"></div>
                                   <h4 class="modal-title">Add/Update Address Book</h4>	
                                   <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                               </div>
                               <div class="modal-body">
                                   <form method="POST" id="add_recipient_contact">
                                       {{csrf_field()}}
                                       <div class="col-md-6 col-sm-6 col-xs-12">
                                           <div class="input-wrapper full-width">
                                               <div class="styled-input">
   
                                                   <input class="form-control"  type="text" name="contact_name" value="{{old('contact_name')}}" id="contact_name">
                                                   <input type="hidden" name="contact_id" value="" id="contact_id">
                                                   <label>Name<span class="mandatory-field">*</span></label>
                                                   <span></span>
                                                   <p class="help-block">
                                                       <strong id="name-error"></strong>
                                                   </p>  
                                               </div>
                                           </div>
                                       </div> 
                                       <div class="col-md-6 col-sm-6 col-xs-12 ">
                                           <div class="input-wrapper full-width">
                                               <label class="col-md-12 control-label" for="textarea">Attn/Additional Recipient Name<span class="mandatory-field"></span></label>
                                               <div class="styled-input">
                                                   <textarea class="form-control attn recipient_attns addressattn"   name="attn" value="{{old('attn')}}" id="attn" ></textarea>
                                                   <p class="help-block">
                                                       <strong id="attn-error"></strong>
                                                   </p>
                                               </div>
                                           </div>
                                       </div>
                                       <div class="col-md-6 col-sm-6 col-xs-12">
                                           <div class="input-wrapper full-width">
                                               <div class="styled-input">
                                                   <label class="lableall">Telephone</label>
                                                   <input class="form-control business_phone_validation addresscontact_no"  type="text" name="contact_no" value="{{old('contact_no')}}" id="contact_no">
                                                   <span></span>
                                                   <p class="help-block">
                                                       <strong id="contact-error"></strong>
                                                   </p>
                                               </div>
                                           </div>
                                       </div>
                                       <div class="col-md-6 col-sm-6 col-xs-12">
                                           <div class="input-wrapper full-width">
                                               <div class="styled-input">
                                                   <label class="lableall">Email</label>
                                                   <input class="form-control email_validation addresscontact_email"  type="email" name="contact_email" value="{{old('contact_email')}}" id="contact_email">
                                                   <span></span>
                                                   <p class="help-block">
                                                       <strong id="email-error"></strong>
                                                       </span>
                                               </div>
                                           </div>
                                       </div>
                                       <div class="col-md-12 col-sm-2 col-xs-12">
                                           <div class="input-wrapper full-width">
                                               <div class="styled-input"> 
                                                   <label class="lableall">Address<span class="mandatory-field">*</span></label>
                                                   <textarea class="form-control addresscontact_address" name="contact_address"  id="contact_address">{{old('contact_address')}}</textarea>
                                                   <span></span>
                                                   <p class="help-block">
                                                       <strong id="address-error"></strong>
                                                   </p>
                                               </div>
                                           </div>
                                       </div>
                                       &nbsp;
                                       <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <label class="lableall">Zip Code<span class="mandatory-field">*</span></label>
                                                    <input class="form-control"  type="hidden" name="selected_address_book" value="" id="selected_address_book">
                                                    <input class="form-control new_addresscontact_zip"  type="text" name="new_addresscontact_zip" value="{{old('new_addresscontact_zip')}}" id="new_addresscontact_zip">
                                                    <input class="form-control addresscontact_zip"  type="hidden" name="contact_zip" value="{{old('contact_zip')}}" id="contact_zip">
                                                    <!-- <input type="text" id='employeeid' readonly> -->
                                                    <input class="contact_zipurl"  type="hidden" name="contact_zipurl" id="contact_zipurl" value="{{ url('admin/ZipCodeLookups') }}">
                                                    <input class="contact_stateurl"  type="hidden" name="contact_stateurl" id="contact_stateurl" value="{{ url('admin/getStateName') }}">
                                                    <span></span>
                                                    <p class="help-block" >
                                                        <strong id="zip-error"></strong>
                                                    </p>
                                                </div>
                                                <div id="zippopuploader" style="display: none">
                                                    <img width="150px" src="{{asset('images/loader.gif')}}"/>
                                                </div>
                                            </div>
                                        </div>
                                       <div class="col-md-6 col-sm-6 col-xs-12">
                                       <div class="select-opt select2-container select2class col-xs-12">
                                               <label class="col-md-12 control-label lableall" for="textinput">City<span class="mandatory-field">*</span></label> 
                                               <select class="select2-hidden-accessible load_ajax_cities addresscontact_city_id"  name="contact_city_id" id="contact_city_id"  style="width: 200px;">
                                                       <option value="">-Select-</option>    
   
                                                   </select> <p class="help-block" style="margin:0">
                                                                   <strong id="city-error"></strong>
                                                               </p>                            </div>
                                           </div>
                                       <div class="col-md-6 col-sm-6 col-xs-12">
                                           <div class="select-opt">
                                               <label class="col-md-12 control-label lableall">State<span class="mandatory-field">*</span></label>
                                               <select class="selectpicker addresscontact_state_id"  name="contact_state_id" data-parsley-required="true" data-parsley-required-message="State is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true" id="contact_state_id">
                                                   <option value="">Select</option>
                                                   @foreach($states as $state)
                                                   <option value="{{$state->id}}" @if(old('contact_state_id')) selected="selected" @endif>{{$state->name}}</option>
                                                   @endforeach
                                               </select>
                                               <p class="help-block">
                                                   <strong id="state-error"></strong>
                                               </p>
                                           </div>
                                       </div>
                                     
                                       <!--                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                           <div class="select-opt">
                                                                                               <label class="col-md-12 control-label">Country</label>
                                                                                               <select class="selectpicker" name="contact_country" data-show-subtext="true" data-live-search="true" id="contact_country">
                                                                                                   <option value="">Select</option>
                                                                                                   @foreach($countries as $country)
                                                                                                   <option value="{{$country->id}}" @if(old('contact_country')) selected="selected" @endif>{{$country->name}}</option>
                                                                                                   @endforeach
                                                                                               </select>
                                                                                               <p class="help-block">
                                                                                                   <strong id="country-error"></strong>
                                                                                               </p>
                                                                                           </div>
                                                                                       </div>-->
                                   </form>
                               </div>
   
                               <div class="modal-footer">
                                   <div class="form-bottom-btn">
                                       <input type="hidden" name="edit_secondary_doc_id" value="{{$id}}" id="doc_id_recipient"/>
                                       <button type="submit" class="btn btn-primary custom-btn customc-btn" id="submit_contact"><span>Add/Update Address Book</span></button>
                                   </div>
                               </div>
                           </div>
                       </div>  
   
                   </div>
 
                <form action="" method="POST" class="remove-attachment-model" id="remove-attachment-model">
                    {!! csrf_field() !!}
                    <div id="attachment-confirmation-modal" class="modal fade">
                        <div class="modal-dialog modal-confirm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div class="icon-box">
                                        <i class="fa fa-close"></i>
                                    </div>				
                                    <h4 class="modal-title">Are you sure?</h4>	
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                </div>
                                <div class="modal-body">
                                    <p>Do you really want to delete these records? This process cannot be undone.</p>
                                </div>
                                <div class="modal-footer">
                                    <div class="form-bottom-btn">
                                        <button type="button" class="btn btn-primary custom-btn customb-btn remove-attachement-from-delete-form" data-dismiss="modal"><span>Cancel</span></button>
                                        <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>Confirm</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <form action="" method="GET" class="remove-record-model">
                    {!! csrf_field() !!}
                    <div id="contact-confirmation-modal" class="modal fade">
                        <div class="modal-dialog modal-confirm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div class="icon-box">
                                        <i class="fa fa-close"></i>
                                    </div>				
                                    <h4 class="modal-title">Are you sure?</h4>	
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                </div>
                                <div class="modal-body">
                                    <input type="text" name="contact_id" id="contact_id" hidden="">              
                                    <p>Do you really want to delete these records? This process cannot be undone.</p>
                                </div>
                                <div class="modal-footer">
                                    <div class="form-bottom-btn">
                                        <button type="button" class="btn btn-primary custom-btn customb-btn remove-data-from-delete-form" data-dismiss="modal"><span>Cancel</span></button>
                                        <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>Confirm</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                @stop

                @section('adminlte_js')
                <script type="text/javascript" src="{{ asset('js/jquery-ui.min.js') }}"></script>

                <script src="{{ asset('vendor/adminlte/plugins/datepicker/bootstrap-datepicker.js') }}" type="text/javascript"></script> 
                <script type="text/javascript">
                    $(function () {
    $("#date").datepicker({ minDate: 0 });
});

var cityUrl = "{{ url('customer/city-list') }}";
     $(document).ready(function () { 
     $(".load_ajax_cities").select2({
                tags:true,
              ajax: { 
               url: cityUrl,
               type: "get",
               dataType: 'json',
               //delay: 250,
               data: function (params) {
                return {
                  name: params.term // search term
                };
               },
               processResults: function (response) {
                 return {
                    results: response
                 };
               },
               cache: true
              }
             });   
        $('input,textarea,select').on('focus', function(e) {
            e.preventDefault();
            $(this).attr("autocomplete", "nope");  
        });
        $('#rush_hour').click(function(){
            if ($(this).is(':checked'))
                $('#rush_hour_charges').show();
            else
                $('#rush_hour_charges').hide();
        });
        
    });
//Start to submit note
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
//back to project tab
$('#editBack').click(function () {
    $('.display-ib li').removeClass('active');
    $('.display-ib li:first-child').addClass('active');
    //    $('#project_tab').addClass('active');
    $('.notice-tabs .tab-pane').removeClass('active');
    $('#project').addClass('active');
})
//save for later
$(".save_for_later").on('click', function () {
    $('#create_work_order').parsley().destroy();
});

//Start to fetch data of related work order

$('#soft_doc_parent_work_order').on('change', function () {
    $(".delete_parent_attachment").remove();
    $.ajax({
        data: {'work_order_no': $('#soft_doc_parent_work_order').val()},
        type: 'post',
        url: "{{url('customer/create-your-own/work_order_details')}}",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            if ($('#soft_doc_parent_work_order').val() == "") {
                location.reload();
            }
            if (response.success) {
                var stateid = "";
                $.each(response.success, function (key, val) {
                    if (val.name == 'state') {
                        $('#soft_doc_state_value').val(val.value);
                        $("#soft_doc_state_value").selectpicker("refresh");


                    } else if (val.name == 'city') {
                        $('#soft_doc_city_value').val(val.value);
                        $("#soft_doc_city_value").selectpicker("refresh");

                    } else if (val.name == 'county') {

                        $('#recipient_county_id').val(val.value);
                        $("#county").val(val.county_name);
                    } else if (val.name == 'project_type') {

                        $('#project_type').val(val.value);
                        $("#project_type").selectpicker("refresh");

                    } else if (val.name == 'your_role') {

                        $('#your_role').val(val.value);
                        $("#your_role").selectpicker("refresh");

                    } else if (val.name == 'contracted_by') {
                        $("#contracted_by_select").val(val.value).trigger('change');

                    } else if (val.name == 'job_start_date') {
                        $('#job_start_date').val(val.value);
                        $('#enter_into_agreement').val(val.value);
                    } else if (val.name == 'last_date_on_the_job') {
                        $('#last_date_of_labor_service_furnished').val(val.value);
                        $('#last_date_on_the_job').val(val.value);

                    } else {
                        var field_name = val.name;
                        var vars = val.value;
                        let result = vars.replace(/<br \/>/gi, "<p>");
                        $("#" + field_name).val(result);
                    }




                });
            }
            if (response.parent_attachment) {

               // $('#soft_notice_work_order_document_table tbody').empty();
                // console.log(response.parent_attachment);
                $.each(response.parent_attachment, function (k, v) {

                    var f1 = v.db_arr[2];
                    var f2 = f1.split('.')[0];
                    var row = '<tr class="delete_parent_attachment" id="refreshdelete' + v.db_arr[4] + '">';
                    $.each((v.ajax_arr), function (i, d) {
                        $('.norecords').closest('tr').remove();
                        var display_url = '{{asset("attachment/cyo_work_order_document/parent_document")}}/' + v.db_arr[2];
                       /* if (i == 1) {
                            row += '<td><a target="_blank" href="' + display_url + '">' + d + '</a></td>';
                        } else {*/
                            row += '<td>' + d + '</td>';
                       // }
                    });
                    var display_url = '{{asset("attachment/cyo_work_order_document/parent_document")}}/' + v.db_arr[2];
                    var attachment_url = '{{url("customer/cyo-work-order-document/parent_document/removeattachment")}}';
                    /*row += "<td><a class='btn btn-secondary item red-tooltip remove-parent-attachment' data-toggle='modal' data-url='" + attachment_url + "'  data-file='" + v.db_arr[2] + "' data-target='#attachment-confirmation-modal' data-placement='bottom' title='Remove'><span class='icon-cross-remove-sign'></span></a></td>";*/
                     if(v.db_arr[2].length>0){
                        row += "<td><a target='_blank' href='" + display_url + "' class='btn btn-secondary item red-tooltip'><i class='fa fa-eye'></i></a></td>";
                            }else{
                                row += "<td></td>";
                            }
                        row += '</tr>';
                    $('#soft_notice_work_order_document_table tbody').append(row);

                });

            }
            $('input').each(function () {
                if ($(this).val().length > 0) {
                    $(this).addClass('not-empty');
                } else {
                    $(this).removeClass('not-empty');
                }
            });
            $('textarea').each(function () {
                if ($(this).val().length > 0) {
                    $(this).addClass('not-empty');
                } else {
                    $(this).removeClass('not-empty');
                }
            });

        }
    });
});

 //contact address dispaly
     $('.contracted_bys').on('change', function () {
            document.getElementById('hidden-panel').style.display = 'block';
            var name = $('.contracted_bys').val();
            var recipt_id = $(this).find(':selected').attr('data-id');
            $.ajax({
                data: {'name': name, 'recipt_id': recipt_id},
                type: "post",
                url: "{{ url('customer/get-contacts') }}",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (res) {   
                   // console.log(res); 
                    $('#addname').val(res.company_address);
                },
    
            });
        });
//End to fetch data of related work order
////Start of fetch cities according to state for on load recipient
$('#state_id').on('change', function () {
    $('#city_id').find('option').remove().end().append('<option value="">Select</option>');

    $.ajax({
        data: {'state': $(this).val()},
        type: "post",
        url: "{{url('cities') }}",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {

            $.each(response, function (e, val) {
                $("#city_id").append("<option value='" + val.id + "'>" + val.name + "</option>");
                $("#city_id").selectpicker("refresh");
            });

        }
    });
});
//End of fetch cities according to state
//End to submit note
$(document).ready(function () {
    var tomorrow = new Date(Date.now() + 24 * 60 * 60 * 1000);
    //$('#job_start_date').attr('readonly', 'true');
    //$('#last_date_on_the_job').attr('readonly', 'true');

    $("#job_start_date").datepicker({
        todayBtn: 1,
        autoclose: true,
    }).on('changeDate', function (selected) {
        var minDate = new Date(selected.date.valueOf());
        $('#last_date_on_the_job').datepicker('setStartDate', minDate);
    });

    $("#last_date_on_the_job").datepicker()
            .on('changeDate', function (selected) {
                var maxDate = new Date(selected.date.valueOf());
                $('#job_start_date').datepicker('setEndDate', maxDate);
            });
    //Check if all labels are generated
    var chk_all_label_generated = '<?php
                                                                                        if ($label_generated_count == count($recipients)) {
                                                                                            echo "yes";
                                                                                        } else {
                                                                                            echo "no";
                                                                                        }
                                                                                        ?>';
    if (chk_all_label_generated == 'yes')
    {
        $('#save_labels_for_later').hide();
    }
    //Check if work order status is mailing then don't show proceed to mailing button
    var chk_work_order_status = '<?php
                                                                                        if ($work_order_status == 5) {
                                                                                            echo 'yes';
                                                                                        } else {
                                                                                            echo 'no';
                                                                                        }
                                                                                        ?>';
    if (chk_work_order_status == 'yes')
    {
        $('#createLabel1').hide();
    }

    var tomorrow = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
    $('.shipping_date').datepicker({dateFormat: 'yy-mm-dd',
        minDate: 0});
    $('#myModal').modal('hide');
    $('[data-toggle="tooltip"]').tooltip({
        trigger: 'click',
        placement: 'bottom'
    });
    $('textarea, input').each(function () {
        if ($(this).val().length > 0) {
            $(this).addClass('not-empty');
        } else {
            $(this).removeClass('not-empty');
        }
    });
    $('textarea, input').keyup(function () {
        if ($(this).val().length > 0) {
            $(this).addClass('not-empty');
        } else {
            $(this).removeClass('not-empty');
        }
    });

    $('#showattorneysfee').hide();
    //collection_for_attorneys_fees
    $('#attorneys_fees').click(function () {

        $('#showattorneysfee').toggle();
    })


    //Start to fetch parent work order number of customers
    //$('#soft_doc_parent_work_order').find('option').remove().end().append('<option value="">Select</option>');
    var work_ordid=$('#doc_id_recipient').val();

    $('#soft_doc_parent_work_order').select2({
        minimumInputLength: 1,
        ajax: {
            url : "{{ url('customer/create-your-own/autocomplete_V1?work_order_id='.$id) }}",
            dataType: 'json',
            
            delay: 250,
            type: "get",
            processResults: function (data) {
            return {
                results:  $.map(data, function (item) {
                    return {
                        text: item,
                        id: item
                    }
                })
            };
            },
            cache: true
        }
    });
    
    //End to fetch parent work order number of customerss
    $('.datepicker').datepicker({
        autoclose: true
    });

    //Start Document Functionalty

    $('.soft_notice_work_order_doc_field').hide();
    $('.soft_notice_work_order_doc_file').hide();
    $('#soft_notice_work_order_doc_type').change(function () {

        var selected_doc_type = $(this).val();
        $('#soft_notice_work_order_doc_input').val("");
        if (selected_doc_type == 1) {
            $('.soft_notice_work_order_doc_field').show();
            $('.soft_notice_work_order_doc_file').show();
            $('#doc_field').text('Bond Number');
        } else if (selected_doc_type == 2) {
            $('.soft_notice_work_order_doc_field').show();
            $('.soft_notice_work_order_doc_file').show();
            $('#doc_field').text('NOC Book/Page');
        } else if (selected_doc_type == 3) {
            $('.soft_notice_work_order_doc_field').show();
            $('.soft_notice_work_order_doc_file').show();
            $('#doc_field').text('Permit No');
        } else if (selected_doc_type == 5) {
            $('.soft_notice_work_order_doc_field').show();
            $('.soft_notice_work_order_doc_file').show();
            $('#doc_field').text('Invoice No.');
        } else if (selected_doc_type == 4 || selected_doc_type == 6 ||
                selected_doc_type == 7 || selected_doc_type == 8
                || selected_doc_type == 9 || selected_doc_type == 13) {
            $('.soft_notice_work_order_doc_field').show();
            $('.soft_notice_work_order_doc_file').show();
            $('#doc_field').text('Description');
        } else if (selected_doc_type == 10) {
            $('.soft_notice_work_order_doc_field').show();
            $('.soft_notice_work_order_doc_file').show();
            $('#doc_field').text('Folio No.');
        }else if (selected_doc_type == 11) {
            $('.soft_notice_work_order_doc_field').show();
            $('.soft_notice_work_order_doc_file').show();
            $('#doc_field').text('COL Signed.');
        }else if (selected_doc_type == 12) {
            $('.soft_notice_work_order_doc_field').show();
            $('.soft_notice_work_order_doc_file').show();
            $('#doc_field').text('Recorded COL/SCOL.');
        }else if (!selected_doc_type) {
            $('.soft_notice_work_order_doc_field').hide();
            $('.soft_notice_work_order_doc_file').hide();
        }

    });
    var result_arr = [];
    $("#submit_document").on("click", function (e) {
        if($('.update_doc').html()=='Add') {
            e.preventDefault();
            var extension = $('#soft_notice_work_order_result_file').val().split('.').pop().toLowerCase();

            if ($('#soft_notice_work_order_result_file').val() != "" && $.inArray(extension, ['csv', 'xls', 'xlsx', 'pdf', 'doc', 'docx', 'jpg', 'jpeg']) == -1) {
                $('#errormessage').html('Please Select Valid File... ');
                $('#errormessage').css('display', 'block');
            } else {
                var file_data = $('#soft_notice_work_order_result_file').prop('files')[0];
                var selected_document_type = $('#soft_notice_work_order_doc_type').val();
                var doc_input = $('#soft_notice_work_order_doc_input').val();
                var form_data = new FormData();

                form_data.append('file', file_data);
                form_data.append('document_type', selected_document_type);
                form_data.append('doc_value', doc_input)
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-Token': $('meta[name=_token]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{url('customer/create-your-own/attachments')}}", // point to server-side PHP script
                    data: form_data,
                    type: 'POST',
                    contentType: false, // The content type used when sending data to the server.
                    cache: false, // To unable request pages to be cached
                    processData: false,
                    success: function (response) {
                        var data = jQuery.parseJSON(response);
                        var ran_no = Math.random() * (100 - 1) + 1;
                        ran_no = String(ran_no).replace('.','_');
                        data.db_arr.push('refreshdelete'+ran_no);
                        result_arr.push(data.db_arr);
                        result_arr.push('|');
                        $('#edit_soft_notices_attachment').val(result_arr);
                        var f1 = data.db_arr[2];
                        var f2 = f1.split('.')[0];
                        var row = '<tr id="refreshdelete' + ran_no + '">';
                        $.each((data.ajax_arr), function (i, d) {
                            $('.norecords').closest('tr').remove();
                            var display_url = '{{asset("attachment/cyo_work_order_document")}}/' + data.db_arr[2];
                           /* if (i == 1) {
                                row += '<td><a target="_blank" href="' + display_url + '">' + d + '</a></td>';
                            } else {*/
                                row += '<td>' + d + '</td>';
                           // }
                        });
                        //hide fields after add
                        $('.soft_notice_work_order_doc_field').hide();
                        $('.soft_notice_work_order_doc_file').hide();
                        // $('.doc_visibility').hide();
                        $('#soft_notice_work_order_doc_type').val('');
                        $('#soft_notice_work_order_doc_type').selectpicker('refresh');
                        $('#soft_notice_work_order_result_file').val('');
                        $('#edit_document_name').val('');
                        $('#soft_notice_work_order_doc_input').val('');
                        $('#remove_file').val('0');
                        $('#preview').css('display','none');
                        $('#remove_icon').css('display','none');
                         var display_url = '{{asset("attachment/cyo_work_order_document")}}/' + data.db_arr[2];
                        var attachment_url = '{{url("customer/create-your-own/removeattachment")}}';
                        // row += "<td><a class='btn btn-secondary item red-tooltip remove-attachment' data-toggle='modal' data-url='" + attachment_url + "'  data-file='" + data.db_arr[2] + "' data-target='#attachment-confirmation-modal' data-placement='bottom' title='Remove'><span class='icon-cross-remove-sign'></span></a></td>";
                        // row += '</tr>';
                        row += "<td><a class='btn btn-secondary item red-tooltip remove-attachment' data-toggle='modal' data-url='" + attachment_url + "'  data-file='" + data.db_arr[2] + "' data-target='#attachment-confirmation-modal' data-placement='bottom' title='Remove'><span class='icon-cross-remove-sign'></span></a><a class='btn btn-secondary item red-tooltip edit-attachment' data-file='" + data.db_arr[2] + "' data-type='" + data.db_arr[0] + "'data-title='" + data.db_arr[1] +"'data-file-original-name='" + data.db_arr[3] + "'data-file-extension='" + data.ajax_arr[4]+"'><span class='icon-pencil-edit-button'></span></a>";
                        if(data.db_arr[2].length>0){
                                    row += "<a target='_blank' href='" + display_url + "' class='btn btn-secondary item red-tooltip'><i class='fa fa-eye'></i></a>";
                                    }
                                    row += '</td></tr>';

                        $('#soft_notice_work_order_document_table tbody').append(row);
                    }
                });
            }
        }
        else{
            e.preventDefault();
            var extension = $('#soft_notice_work_order_result_file').val().split('.').pop().toLowerCase();

            if ($('#soft_notice_work_order_result_file').val() != "" && $.inArray(extension, ['csv', 'xls', 'xlsx', 'pdf', 'doc', 'docx', 'jpg', 'jpeg']) == -1) {
                $('#errormessage').html('Please Select Valid File... ');
                $('#errormessage').css('display', 'block');
            } else
            {
                $('#errormessage').html('');
                var file_data = $('#soft_notice_work_order_result_file').prop('files')[0];
                var selected_document_type = $('#soft_notice_work_order_doc_type').val();
                var doc_input = $('#soft_notice_work_order_doc_input').val();
                //var file_data = $('#soft_notice_work_order_result_file').prop('files')[0];
               // var selected_document_type = $('#edit_document_type').val();
               // var doc_input = $('#soft_notice_work_order_doc_input').val();
                var doc_id = $('#edit_doc_id').val();
                var doc_visibile = $("input[name='visibility']:checked").val();
                var work_order_id = $('#work_order').val();
                var remove_file = $('#remove_file').val();
                var edit_document_name = $('#edit_document_name').val();
                var edit_document_original_name = $('#edit_document_name').attr('data-file-original-name');
                var edit_document_extension = $('#edit_document_name').attr('data-file-extension');
                var row_id = $('#row_id').val();
                //remove array value
                var result;
                //console.log(result_arr);
                for( var i = 0, len = result_arr.length; i < len; i++ ) {
                    // console.log(len);console.log(result_arr[i]);
                    for( var j = 0, len1 = result_arr[i].length; j < len1; j++ ){
                        //  console.log(result_arr[j]);
                        if( result_arr[i][j] === row_id ) {
                            result = i;
                            break;
                        }
                    }
                }
                result_arr.splice(result, 1);
                if(result_arr.length==1){
                    result_arr.splice(0, 1);
                }
                $('#edit_soft_notices_attachment').val(result_arr);
                //console.log(result_arr,'hi');
                console.log(result_arr);
                if(doc_id==''){
                    var doc_id = 0;
                }



                var form_data = new FormData();

                if(file_data)
                {
                    form_data.append('file', file_data);
                }else{
                            form_data.append('file_name',edit_document_name);
                            form_data.append('file_original_name',edit_document_original_name);
                            form_data.append('extension',edit_document_extension);
                }
                form_data.append('document_type', selected_document_type);
                form_data.append('doc_value', doc_input);
                form_data.append('visibility', doc_visibile);
                form_data.append('doc_id', doc_id);
                form_data.append('work_order_id',work_order_id);
                form_data.append('remove_file',remove_file);


                $.ajaxSetup({
                    headers: {
                        'X-CSRF-Token': $('meta[name=_token]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{url('customer/create-your-own/edit/attachments')}}", // point to server-side PHP script
                    data: form_data,
                    type: 'POST',
                    contentType: false, // The content type used when sending data to the server.
                    cache: false, // To unable request pages to be cached
                    processData: false,
                    success: function (response) {
                        var data = jQuery.parseJSON(response);
                        var del_var = $('#row_id').val();
                        $("#" + del_var).remove();
                        //add id to new row
                       // console.log(data.ajax_arr,'hi');
                        if(data.db_arr[2] != null){
                            var f1 = data.db_arr[2];//console.log(f1);
                            var f2 = f1.split('.')[0];
                        }
                        if(data.ajax_arr[2] == null){
                            data.db_arr[2] = '';
                        }
                        var row = '<tr id="refreshdelete' + data.db_arr[4] + '">';
                        $.each((data.ajax_arr), function (i, d) {
                            $('.norecords').closest('tr').remove();
                            var display_url = '{{asset("attachment/work_order_document")}}/' + data.db_arr[2];
                           /* if (i == 1) {
                                row += '<td><a target="_blank" href="' + display_url + '">' + d + '</a></td>';
                            } else {*/
                                row += '<td>' + d + '</td>';
                           // }
                            //
                            $('.soft_notice_work_order_doc_field').hide();
                            $('.soft_notice_work_order_doc_file').hide();
                            $('.doc_visibility').hide();
                            $('#soft_notice_work_order_doc_type').val('');
                            $('#soft_notice_work_order_doc_type').selectpicker('refresh');
                            if($('#submit_document span').html()=="Update"){
                                $('#submit_document span').html('Add');
                            }
                            $('#soft_notice_work_order_result_file').val('');
                            $('#soft_notice_work_order_doc_input').val('');
                            $('#remove_file').val('0');
                            $('#preview').css('display','none');
                            $('#remove_icon').css('display','none');
                            $('#edit_document_name').val('');


                        });
                        var display_url = '{{asset("attachment/work_order_document")}}/' + data.db_arr[2];
                        var attachment_url = '{{url("customer/edit/cyo-work-ordwer/removeattachment")}}';
                        row += "<td><a class='btn btn-secondary item red-tooltip remove-attachment' data-toggle='modal' data-url='" + attachment_url + "'data-id='" + data.db_arr[4] + "'  data-file='" + data.db_arr[2] + "' data-target='#attachment-confirmation-modal' data-placement='bottom' title='Remove'><span class='icon-cross-remove-sign'></span></a><a class='btn btn-secondary item red-tooltip edit-attachment' data-file='" + data.db_arr[2] + "' data-type='" + data.db_arr[5] + "'data-title='" + data.db_arr[1] + "'data-id='" + data.db_arr[4]+ "'data-file-original-name='" + data.db_arr[3] + "'data-file-extension='" + data.db_arr[6]+"'><span class='icon-pencil-edit-button'></span></a>";
                         if(data.db_arr[2].length>0){
                            row += "<a target='_blank' href='" + display_url + "' class='btn btn-secondary item red-tooltip'><i class='fa fa-eye'></i></a>";
                            }
                            row += '</td></tr>';
                        $('#soft_notice_work_order_document_table tbody').append(row);

                    }
                });
            }
        }

    });
    //End Document Fucntionality

    //Edit Attachement Start
    function openTab(th)
    {
        window.open(th.name,'_blank');
    }
    $('#remove_icon').click(function(){
        $('#preview').css('display','none');
        $('#remove_icon').css('display','none');
        $('#remove_file').val('1');
        $('#soft_notice_work_order_result_file').val('');
    })
    $('#soft_notice_work_order_result_file').change(function(e){
        var fileName = e.target.files[0].name;
        doc_icon = fileName.split('.')[1];
        if(doc_icon == 'pdf'){
            $('#preview').html('<i class="fa fa-file-pdf-o" style="font-size:36px"></i>');
        }else if(doc_icon == 'doc' || doc_icon == 'docx'){
            $('#preview').html('<i class="fa fa-file-word-o" style="font-size:36px"></i>');
        }else if(doc_icon == 'xls' || doc_icon == 'xlsx'){
            $('#preview').html('<i class="fa fa-file-excel-o" style="font-size:36px"></i>');
        }else if(doc_icon == 'jpg' || doc_icon == 'jpeg'){
            $('#preview').html('<i class="fa fa-file-image-o" style="font-size:36px"></i>');
        }else{
            $('#preview').html('<i class="fa fa-file" style="font-size:36px"></i>');
        }
        // $('#preview').html(fileName);
        $('#preview').css('display','block');
        $('#remove_icon').css('display','block');
        //alert('The file "' + fileName +  '" has been selected.');
    });

    $(document).on('click', '.edit-attachment', function () {
        var row_id = $(this).closest('tr').attr('id');
        // $('#submit_document').removeClass('old_class').addClass('new_class');
        var type = $(this).attr('data-type');
        var file = $(this).attr('data-file');
        var title = $(this).attr('data-title');
        var id = $(this).attr('data-id');

        $('#edit_document_name').val(file);
        $('#edit_document_name').attr('data-file-original-name',$(this).attr('data-file-original-name'));
        $('#edit_document_name').attr('data-file-extension',$(this).attr('data-file-extension'));
        $('#soft_notice_work_order_doc_input').val(title);
        if (title != ''){$('#soft_notice_work_order_doc_input').addClass('not-empty');}
        $('#edit_doc_id').val(id);
        $('#row_id').val(row_id);
        $('#preview').css('display','block');
        $('#remove_icon').css('display','block');

        //console.log(file);
        var APP_URL = {!! json_encode(url('/')) !!}
            // console.log(APP_URL);
            $("#open_doc").attr('name',APP_URL+'/attachment/work_order_document/'+file);
        doc_icon = file.split('.')[1];
        if(doc_icon == 'pdf'){
            $('#preview').html('<i class="fa fa-file-pdf-o" style="font-size:36px"></i>');
        }else if(doc_icon == 'doc' || doc_icon == 'docx'){
            $('#preview').html('<i class="fa fa-file-word-o" style="font-size:36px"></i>');
        }else if(doc_icon == 'xls' || doc_icon == 'xlsx'){
            $('#preview').html('<i class="fa fa-file-excel-o" style="font-size:36px"></i>');
        }else if(doc_icon == 'jpg' || doc_icon == 'jpeg'){
            $('#preview').html('<i class="fa fa-file-image-o" style="font-size:36px"></i>');
        }else if (doc_icon == 'csv'){
            $('#preview').html('<i class="fa fa-file" style="font-size:36px"></i>');
        }else{
            $('#preview').html('');
            $('#preview').css('display','none');
            $('#remove_icon').css('display','none');
        }

        //$('#soft_notice_work_order_result_file').val(APP_URL+'/attachment/work_order_document/'+file);
        var id1 = file.split('.')[0];

        var id = $(this).attr('data-id');
        $('.soft_notice_work_order_doc_field').show();
        $('.soft_notice_work_order_doc_file').show();
        $('.doc_visibility').show();
        $('.update_doc').text('Update');
        $('#soft_notice_work_order_doc_type').val(type);
        $('#soft_notice_work_order_doc_type').selectpicker('refresh');
        /* $('#errormessage').text(file);
         $('#errormessage').css('display', 'block');*/
        var selected_doc_type = type;
        //$('#soft_notice_work_order_doc_input').val("");
        if (selected_doc_type == 1) {
            $('.soft_notice_work_order_doc_field').show();
            $('.soft_notice_work_order_doc_file').show();
            $('.doc_visibility').show();
            $('#doc_field').text('Bond Number');
        } else if (selected_doc_type == 2) {
            $('.soft_notice_work_order_doc_field').show();
            $('.soft_notice_work_order_doc_file').show();
            $('.doc_visibility').show();
            $('#doc_field').text('NOC Book/Page');
        } else if (selected_doc_type == 3) {
            $('.soft_notice_work_order_doc_field').show();
            $('.soft_notice_work_order_doc_file').show();
            $('.doc_visibility').show();
            $('#doc_field').text('Permit No');
        } else if (selected_doc_type == 5) {
            $('.soft_notice_work_order_doc_field').show();
            $('.soft_notice_work_order_doc_file').show();
            $('.doc_visibility').show();
            $('#doc_field').text('Invoice No.');
        } else if (selected_doc_type == 4 || selected_doc_type == 6 ||
            selected_doc_type == 7 || selected_doc_type == 8 ||
            selected_doc_type == 9) {
            $('.soft_notice_work_order_doc_field').show();
            $('.soft_notice_work_order_doc_file').show();
            $('.doc_visibility').show();
            $('#doc_field').text('Description');
        } else if (selected_doc_type == 10) {
            $('.soft_notice_work_order_doc_field').show();
            $('.soft_notice_work_order_doc_file').show();
            $('.doc_visibility').show();
            $('#doc_field').text('Folio No.');
        }  else if (selected_doc_type == 11) {
            $('.soft_notice_work_order_doc_field').show();
            $('.soft_notice_work_order_doc_file').show();
            $('.doc_visibility').show();
            $('#doc_field').text('COL Signed.');
        }  else if (selected_doc_type == 12) {
            $('.soft_notice_work_order_doc_field').show();
            $('.soft_notice_work_order_doc_file').show();
            $('.doc_visibility').show();
            $('#doc_field').text('Recorded COL/SCOL.');
        } else if (!selected_doc_type) {
            $('.soft_notice_work_order_doc_field').hide();
            $('.soft_notice_work_order_doc_file').hide();
            $('.doc_visibility').hide();
        }

    });
    //Edit Attachement End


    //End to fetch parent work order number of customerss

});
$(document).on('click', '.remove-attachment', function () {
    var url = $(this).attr('data-url');
    var file = $(this).attr('data-file');
    var id = $(this).attr('data-id');
    var row_id = $(this).closest('tr').attr('id');
    $(".remove-attachment-model").attr("action", url);
    $("input[name='file']").remove();
    $("input[name='row_id']").remove();
    $("input[name='id']").remove();
    $('body').find('.remove-attachment-model').append('<input name="file" type="hidden" value="' + file + '">');
    $('body').find('.remove-attachment-model').append('<input name="id" type="hidden" value="' + id + '">');
    $('body').find('.remove-attachment-model').append('<input name="_method" type="hidden" value="POST">');
     $('body').find('.remove-attachment-model').append('<input name="row_id" type="hidden" value="' + row_id + '">');
});
var remove_arr = [];

$('#remove-attachment-model').submit(function (event) {
    event.preventDefault();
    var file = $('input[name=file]').val();
    var id = file.split('.')[0];
    var row_id = $('input[name=row_id]').val();

    $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name=_token]').attr('content')
        }
    });
    var submit_url = $(this).attr("action");
    $.ajax({
        type: 'POST',
        url: submit_url,
        data: $('#remove-attachment-model').serialize(),
        success: function (response) {
            if (response.success) {
                $('#attachment-confirmation-modal').modal('hide');
                remove_arr.push(row_id);
                $('#remove_doc').val(remove_arr);
                var del_var = "refreshdelete" + id;

                $("#" + row_id).remove();
            }
        }
    });
});
 function checkForInputs(sel)
    {
        var val = sel.getAttribute('value');
        var name = sel.getAttribute('name');
            var data_value = sel.getAttribute('data-value');
        var cnt = name.split('_');
        cnt = cnt[3];
        switch (val)
        {
            case 'firm mail':
            case 'next day delivery':
                $('input[name="manual_inputs' + cnt + '"]').attr('placeholder', 'Enter mailing label here');
                // $('input[name="manual_inputs' + cnt + '"]').attr('required', true);
                $('input[name="shipping_date' + cnt + '"]').removeAttr('required');
                $('#service_type_' + cnt).removeAttr('required');
                $('#service_type_div_' + cnt).hide();
                $('input[name="manual_inputs' + cnt + '"]').show();
                $('input[name="shipping_date' + cnt + '"]').hide();
                break;
            default:
                $('input[name="manual_inputs' + cnt + '"]').hide();
                $('input[name="manual_inputs' + cnt + '"]').removeAttr('required');
                $('input[name="shipping_date' + cnt + '"]').attr('placeholder', 'Enter shipping date here');
                // $('input[name="shipping_date' + cnt + '"]').attr('required', true);
                $('input[name="shipping_date' + cnt + '"]').show();
                $('#service_type_' + cnt).attr('required', true);
                $('#service_type_div_' + cnt).show();
        }
        $(".selectpicker").selectpicker('refresh');
    }
$(document).on('click', '.remove-record', function () {

    var recipient_id = $(this).attr('data-id');
    var notice_id = $(this).attr('data-notice-id');
    var work_order_id = $(this).attr('data-workorder-id');

    var url = "{{url('customer/create-your-own/recipient/delete/')}}" + '/' + recipient_id + '/' + notice_id + '/' + work_order_id;
     if (url.indexOf("#") ==-1){
            url += '#recipient'; 
    }
            $(".remove-record-model").attr("action", url); 
});
var note_url = '{{url("customer/create-your-own/store_note")}}';
var correction_url = '{{url("customer/create-your-own/store_correction")}}';
var src = "{{ url('customer/city/autocompletecityname') }}";
var get_contact = "{{ url('customer/get-contacts') }}";
var cyo_contact_add = '{{url("customer/contact-cyoworkorderrecipient/store")}}';
var get_cyo_contacts = "{{url('customer/getAllCyoWorkOrderContacts')}}";
var get_recipient_data = "{{url('customer/create-your-own/get_recipient_data')}}";
var update_address_book = "{{url('customer/contact/cyowork-order/update_recipient_contact')}}";
var getVerifiedAddress = '{{url("account-manager/research/getVerifiedAddress")}}';
var getPackageType = '{{url("get-package-types")}}';
var getAddOn = '{{url("get-add-ons")}}';
var getProhibitedAddOns = '{{url("get-prohibited")}}';
var verified_address_count = '{{(isset($verified_address_count)  && count($recipients) > 0 && count($recipients) == $verified_address_count)?"yes":"no"}}';
var checkFieldsUrl = "{{ url('customer/create-your-own/checkFields') }}";
/*county */
var src_county = "{{ url('customer/city/autocompletecountyname/'.$notice_id) }}";
var createlable_url = '{{url("customer/create-your-own/createLabel")}}';
var autocompletecityzip_src = "{{ url('customer/city/autocompletecityzip') }}";
var base_url = "{{url('/customer/create-your-own/work-order-history')}}";
 var customer_ids = '';
                </script>
               <!--  <script type="text/javascript" src="{{ asset('js/autocomplete_zip.js') }}"></script> -->
                <script type="text/javascript" src="{{ asset('js/dashboard.js') }}"></script>
                @endsection