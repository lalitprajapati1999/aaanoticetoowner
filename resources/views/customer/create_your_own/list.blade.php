@extends('adminlte::page')
@section('content')
<div role="tabpanel" >
    <section class="create-own">
        <div class="dashboard-wrapper">
            <div class="dashboard-heading">
                <h1><span>Create Your Own</span></h1>
            </div>
            <div class="dashboard-inner-body">
                <div class="createown-wrapper">

                    <div class="grid">
                        @foreach($notices as $notice)
                        <figure class="effect-sadie">
                            <figcaption>
                                <a href="{{url('customer/create-your-own/create/'.$notice->type.'/'.$notice->id.'/'.$notice->state_id)}}"><h2>{{$notice->name}}</h2></a><br/>
                                <a href="{{url('customer/create-your-own/create/'.$notice->type.'/'.$notice->id.'/'.$notice->state_id)}}"><p>{{$notice->name}}</p></a>
                            </figcaption>			
                        </figure>
                        @endforeach
                    </div>


                </div>
            </div>
        </div>
    </section>




    @stop
    @section('frontend_js')
    <script type="text/javascript">

    </script>
    @stop