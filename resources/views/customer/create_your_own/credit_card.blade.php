@extends('adminlte::page')

<!--  -->
@section('content')

<div  id="workorder">
    <section class="work-order address-book-add-readonly">
        <div class="dashboard-wrapper">
            <div class="dashboard-heading">
                <h1><span>Credit Card Details</span></h1>
            </div>
            @if ($message = Session::get('errorMessage'))
                <div class="flash-message">
                    <div class="alert alert-danger" style="margin: unset">
                        {{ $message }}
                    </div>
                </div>
            @endif

            <div class="dashboard-inner-body">                
                <form method="post" data-parsley-validate="" id="credit-card-form" action="{{ url('customer/check-credit-card') }}" autocomplete="off">
                    <meta name="_token" content="{{ csrf_token() }}" />
                    {!! csrf_field() !!}                    
                    <div class="dash-subform">
                            <div class="row">
                               
                            </div>
                            <h2>Your Payment Amount Is - {{ $amount }}</h2>
                            <div class="row">
                                <input type="hidden" name="id" value="{{$id}}">
                                <input type="hidden" name="work_order_id" value="{{$work_order_id}}">
                                <div class="col-md-6 col-sm-6">
                                    <div class="input-wrapper full-width">
                                        @if(!empty($user_cards))
                                            <label>Saved Cards</label>
                                            </br>
                                            @foreach($user_cards as $card)
                                              <input type="radio" id="card_id"  name="card_id" value="{{ $card['id'] }}">&nbsp; {{ $card['number']}}<br/>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        <div class="row">      
                                  
                                <div class="col-md-6 col-sm-6">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                            <input id="credit_card_name" type="text" name="credit_card_name" class="form-control" data-parsley-required="true" value="" data-parsley-required-message="Person Name is required" data-parsley-trigger="change focusout" data-parsley-pattern="/^[a-zA-Z ]*$/" maxlength="100">    
                                            <label>Person's name (As it appears on card)<span class="mandatory-field">*</span></label>
                                            <span></span>
                                            @if ($errors->has('credit_card_name'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('credit_card_name') }}</strong>
                                            </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="input-wrapper full-width">                                       
                                        <div class="select-opt">
                                            <label class="control-label" for="textinput">Credit Card Type<span class="mandatory-field">*</span></label>
                                            <select name="card_type" class="selectpicker" data-show-subtext="true" data-live-search="true" data-parsley-required="true" data-parsley-required-message="Credit Card Type is required" data-parsley-trigger="change focusout" id='card_type'>
                                                <option value="">-Select-</option>
                                                @php ($card_types = ['Mastercard'=>'Mastercard','Visa'=>'Visa','American Express'=>'American Express'])
                                                @foreach($card_types as $key=>$val)
                                                <option value="{{$key}}"> {{$val}}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('card_type'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('card_type') }}</strong>
                                            </p>
                                            @endif
                                        </div>
                                    </div>  
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                            <input type="text" name="credit_card_date" class="form-control datepicker" data-parsley-required="true" value="" data-parsley-required-message="Expiration Date is required" data-parsley-trigger="change focusout"> <label>Expiration Date (Format - MM/YYYY)<span class="mandatory-field">*</span></label>
                                            <span></span>
                                            @if ($errors->has('credit_card_date'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('credit_card_date') }}</strong>
                                            </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                                <input id="card_number" pattern="[0-9\s]*" type="text" name="card_number" class="form-control" data-parsley-required="true" value="" data-parsley-required-message="Card Number is required" data-parsley-trigger="change focusout">       
                                                <label>Card Number<span class="mandatory-field">*</span></label>
                                                <span></span>
                                                @if ($errors->has('card_number'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('card_number') }}</strong>
                                                </p>
                                                @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                            <input type="text" name="security_code" class="form-control stylish_security_code" data-parsley-required="true" value="" data-parsley-required-message="Security Code is required" data-parsley-minlength="3" data-parsley-maxlength="3" data-parsley-minlength-message="Security code must be 3 digits" data-parsley-maxlength-message="Security code must be 3 digits" data-parsley-trigger="change focusout">    
                                            <input type="hidden" name="security_code" class="hidden_security_code" value="{{ old('security_code') }}">
                                            <label>Security Code<span class="mandatory-field">*</span></label>
                                            <span></span>
                                            @if ($errors->has('security_code'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('security_code') }}</strong>
                                            </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                            <input type="text" name="credit_zip_code" class="form-control" data-parsley-required="true" value="" data-parsley-required-message="Zip Code of Credit Card is required" data-parsley-trigger="change focusout" data-parsley-type="digits">    
                                            <label>Zip Code of Credit Card<span class="mandatory-field">*</span></label>
                                            <span></span>
                                            @if ($errors->has('credit_zip_code'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('credit_zip_code') }}</strong>
                                            </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12">
                                    <div class="input-wrapper full-width">
                                        <input type="checkbox" id="terms_checkbox" class="regular-checkbox" data-parsley-required-message="Accept terms and conditions" required data-parsley-trigger="change focusout" name="terms">

                                        <label for="terms_checkbox_label" class="agree-label">I agree to the AAA Business Associates authorization to charge on my credit card.</label>
                                        @if ($errors->has('terms'))
                                        <p class="help-block">
                                            <strong>{{ $errors->first('terms') }}</strong>
                                        </p>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12">
                                    <div class="panel-group">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4>
                                                    <a data-toggle="collapse" href="#collapse1">Terms and Conditions</a>
                                                </h4>
                                            </div>
                                            <div id="collapse1" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p><span id="card_holder_name_undertaking">@if(isset($customer_name))
                                                            {{ $customer_name }}
                                                            @else (Card Holder Name)
                                                            @endif</span> hereby represent that once this authorization form is signed, I agree to pay the full amount charged and there will be no cancellation or refund.</p>
                                                    <p>If and only if, AAA Business Assoc. DBA AAA Notice To Owner do not receive full statement payment, we AAA Notice to Owner have the right to charge your Credit or Debit Card.</p>
                                                    <p>I understand and consent to the use of my Credit or Debit Card without my signature on the charge that a photocopy of this agreement will serve as an original, and this Credit or Debit Card authorization cannot be revoked</p>
                                                    <p>Once the transaction has been completed, you will receive an email or text message with a copy of your receipt.</p>
                                                    <p>**** NOTE: This transaction does not include mailing fees for this document, All mailing fees will be charged seperately on your statement. ****</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                           
                        </div>
                       
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="form-bottom-btn">                               
                                <button type="submit" class="btn btn-primary custom-btn customc-btn" id="submit_card_button"><span>Submit</span></button>
                            </div>
                        </div>  
                    </div>
                </form>
            </div>
    </section>    
</div>
@endsection

@section('adminlte_js')
<script type="text/javascript" src="{{ asset('js/jquery-ui.min.js') }}"></script>

<script src="{{ asset('vendor/adminlte/plugins/datepicker/bootstrap-datepicker.js') }}" type="text/javascript"></script>

<script type="text/javascript">
$('input').on('focus', function(e) {
    //$('.datepicker').attr("autocomplete", "off"); 
});
$(document).ready(function () {
    <?php if(session('success')){ ?>    
        generatePdfAndRecordAfterPayment();
    <?php } ?>
    <?php if(session('errorMessage')){ ?>    
        $('#pleaseWait').modal('hide');
    <?php } ?>

    $('input,textarea,select').on('focus', function(e) {
        e.preventDefault();
        $('#card_number').attr("autocomplete", "off"); 
        //$(this).attr("autocomplete", "nope");
        //$(input).attr("autocomplete", "off"); 
    });   
    $('#credit_card_name').keyup(function () {
        var card_holder_name = $(this).val();
        $('#card_holder_name_undertaking').html(card_holder_name);
    });
    $('.datepicker').datepicker({ format:'mm/yyyy',autoclose: true });
    $('#card_number').mask("9999 9999 9999 9999 999");

    $('#card_type').change(function () {
        var sel = $(this).val();
        console.log(sel);
        switch (sel)
        {
            case 'American Express':
                $('#card_number').attr('minlength', '15');
                $('#card_number').attr('maxlength', '15');
                $('#card_number').mask("9999 9999 9999 9999 999");
                break;
            case 'Mastercard':
                $('#card_number').attr('minlength', '12');
                $('#card_number').attr('maxlength', '16');
                $('#card_number').mask("9999 9999 9999 9999");
                break;
            case 'Visa':
                $('#card_number').attr('minlength', '12');
                $('#card_number').attr('maxlength', '16');
                $('#card_number').mask("9999 9999 9999 9999");
                break;
            default:
                $('#card_number').attr('minlength', '12');
                $('#card_number').attr('maxlength', '16');
        }
    })
    $('#submit_card_button').click(function (e) {
        e.preventDefault();              
        var card_id =  ($('input[name="card_id"]:checked').val()) ? $('input[name="card_id"]:checked').val() : '';
       
        if(card_id.length!=0){  
          $('#credit-card-form').submit();
          $('#pleaseWait').modal('show'); 
        }else{ 
            var valid_form = $('#credit-card-form').parsley().validate();            
            if (valid_form == true)
            {   
               $('#pleaseWait').modal('show');  
               $('#credit-card-form').submit();                           
            }
        }    
    })
})

 function generatePdfAndRecordAfterPayment(){
    var work_id = "<?php echo $work_order_id ?>";
    var base_url = "{{url('/customer/create-your-own/work-order-history')}}";
    var data_url = "{{url('/account-manager/printpdf')}}"+'/'+ work_id +"{{('/cyo/'.'Notice')}}";
    $('#pleaseWait').modal('hide');
    window.open(data_url, '_blank');
    window.location = base_url;
}

$.fn.betta_pw_fld = function(pwfld_sel, hiddenfld_sel) {

    // this is the form the plugin is called on
    $(this).each(function() {

        // the plugin accepts the css selector of the pw field (pwfld_sel)
        var pwfld = $(this).find(pwfld_sel);

        // on keyup, do the masking visually while filling a field for actual use
        pwfld.off('keyup.betta_pw_fld');


        pwfld.on('keyup.betta_pw_fld', function() {
            var pchars = $(this).val().replace(/[^\d\*]/g, '');
            if (pchars == '') return;

            // we'll need the hidden characters too (for form submission)
            var hiddenfld = $(this).parents('form').find(hiddenfld_sel);
            var hchars = hiddenfld.val();

            var newval = '';
            var newhpw = '';

            for (i=0; i<pchars.length; i++) {
                if (pchars[i] == '*') {
                    newhpw += hchars[i];
                } else {
                    newhpw += pchars[i];
                }
                newval += '*';
            }

            // set the updated (masked), visual pw field
            $(this).val(newval);

            // keep the pw hidden and ready for form submission in a hidden input
            hiddenfld.val(newhpw);
        });
    });
}

$('#credit-card-form').betta_pw_fld('.stylish_security_code', '.hidden_security_code');
$('#card_id').click(function (){    
    console.log($('#card_id').val());
    $('#credit-card-form').parsley().destroy();
});
</script>
@endsection