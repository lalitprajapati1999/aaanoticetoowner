@extends('adminlte::page')
@section('content')
<div role="tabpanel">
	<section class="create-own">
		<div class="dashboard-wrapper">
			<div class="dashboard-heading">
				<h1><span>Create Your Own</span></h1>
			</div>
			<div class="dashboard-inner-body">
				<div class="createown-wrapper">
					<div class="grid">
						<figure class="effect-sadie">
							<figcaption>
								<a href="{{url('customer/create-your-own/work-order-history')}}">
									<h2>View Work Order</h2>
								</a>
								<a href="{{url('customer/create-your-own/work-order-history')}}">
									<p>View Work Order</p>
								</a>
							</figcaption>
						</figure>
					</div>
					<div class="grid">
						<figure class="effect-sadie">
							<figcaption>

								<a href="" data-target="#favoritesModal" data-toggle="modal">
									<h2>Create Your Own Notice</h2>
								</a>
								<a href="" data-target="#favoritesModal" data-toggle="modal">
									<p>Create Your Own Notice</p>
								</a></p>
							</figcaption>
						</figure>
					</div>

				</div>
			</div>
		</div>
	</section>
</div>
<form method="get" action="{{url('customer/create-your-own/notice-list')}}" data-parsley-validate="">
	<meta name="_token" content="{{ csrf_token() }}" />
	{!! csrf_field() !!}
	<div id="favoritesModal" class="modal fade register-modal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Select State</h4>
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">

					<div class="col-md-12 col-sm-12">
						<div class="select-opt">
							<label class="col-md-12 control-label" for="textinput"></label>
							<select class="selectpicker" id="state_id" name="state_id" data-parsley-required="true" data-parsley-required-message="State is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true">
								<option value="">Select</option>
								@foreach($states as $state)
								<option value={{$state->id}}>{{$state->name}}</option>
								@endforeach
							</select>

						</div>
					</div>

					<div class="modal-footer">
						<div class="form-bottom-btn">
							<button type="submit" class="btn btn-primary custom-btn customc-btn"><span>Submit</span></button>
						</div>
					</div>
				</div>
			</div>
		</div>
</form>

@stop
@section('frontend_js')
@section('frontend_js')
<script type="text/javascript">
	/*function cyo_info(notice_type,notice_id){
		$('#notice_type').val(notice_type);
		$('#notice_id').val(notice_id);

	}*/
</script>
@stop
@stop