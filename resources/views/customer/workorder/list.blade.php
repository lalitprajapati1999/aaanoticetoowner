@extends('adminlte::page')

<!--  -->
@section('content')

<div  id="workorder">
    <section class="work-order address-book-add-readonly">
        <div class="dashboard-wrapper">
            <div class="dashboard-heading">
                <h1><span>New Work Order</span></h1>
            </div>
            <div class="dashboard-inner-body">
                <div class="order-by">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="order-details full-width list-inline">
                                <li><p>Company Name:</p> <span>{{$customer_details->company_name}}</span></li>
                                <li><p>Contact Name:</p> <span>{{$customer_details->contact_person}}</span></li>
                                <li><p>Phone Number:</p> <span> {{$customer_details->office_number}}</span></li>
                            </ul>
                            <ul class="order-details full-width list-inline">
                                <li><p>Mailing Address:</p> <span>{{$customer_details->mailing_address}}</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <form method="get" data-parsley-validate="" id="workorder_step1" action="{{ url('customer/work-order/create/') }}" >
                    <meta name="_token" content="{{ csrf_token() }}" />
                    {!! csrf_field() !!}
                    <div class="dash-subform">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="select-opt">
                                    <label class="col-md-12 control-label" for="textinput">Select Type</label> 
                                </div>
                                <ul class="nav nav-tabs radio-tabs button-holder full-width" role="tablist">
                                    <li role="presentation" class="col-md-6 active"><a href="#soft-doc" aria-controls="profile" role="tab" data-toggle="tab"><input type="radio" id="radio-1-2" name="radio-1-set" class="regular-radio"  value="soft" checked /><label for="radio-1-2">Soft Document</label></a></li>
                                    <li role="presentation" class=" col-md-6"><a href="#hard-doc" aria-controls="home" role="tab" data-toggle="tab"><input type="radio" id="radio-1-1" name="radio-1-set" class="regular-radio" value="hard"  /><label for="radio-1-1">Hard Document</label></a></li>

                                </ul>
                            </div>
                            <div class="col-md-6">
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane" id="hard-doc">
                                        <div class="row">
                                            <div class="col-lg-8 col-md-8 col-sm-12">
                                                <div class="select-opt">
                                                    <label class="col-md-12 control-label" for="textinput">State<span class="mandatory-field">*</span></label>  
                                                    <select id="project_type1" class="selectpicker"  name="project_type1"  data-parsley-required-message="State is required" data-parsley-trigger="change focusout">
                                                        <option value="">Select State</option>
                                                        @foreach($states as $state)
                                                        <option value="{{$state->id}}">{{$state->name}}</option>@endforeach
                                                    </select>
                                                    @if ($errors->has('project_type1'))
                                                    <p class="help-block">
                                                        <strong>{{ $errors->first('project_type1') }}</strong>
                                                    </p>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-12">
                                                <div class="select-opt">
                                                    <label class="col-md-12 control-label" for="textinput">Notice Type<span class="mandatory-field">*</span></label>  
                                                    <select id="notice_id1" class="selectpicker" name="notice_id1"  data-parsley-required-message="Notice type is required" data-parsley-trigger="change focusout">
                                                        <option value="">-Select Hard Document-</option>

                                                    </select>
                                                    @if ($errors->has('notice_id1'))
                                                    <p class="help-block">
                                                        <strong>{{ $errors->first('notice_id1') }}</strong>
                                                    </p>
                                                    @endif

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane active" id="soft-doc">
                                        <div class="row">
                                            <div class="col-lg-8 col-md-8 col-sm-12">
                                                <div class="select-opt">
                                                    <label class="col-md-12 control-label" for="textinput">State<span class="mandatory-field">*</span></label>  
                                                    <select id="project_type2" class="selectpicker" name="project_type2" data-parsley-required="true" data-parsley-required-message="State is required" data-parsley-trigger="change focusout">
                                                        <option value=" ">Select State</option>
                                                        @foreach($states as $state)
                                                        <option value="{{$state->id}}">{{$state->name}}
                                                        </option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('project_type2'))
                                                    <p class="help-block">
                                                        <strong>{{ $errors->first('project_type2') }}</strong>
                                                    </p>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-12">
                                                <div class="select-opt">
                                                    <label class="col-md-12 control-label" for="textinput">Notice Type<span class="mandatory-field">*</span></label>  
                                                    <select class="selectpicker" name="notice_id2"  id="notice_id2" data-parsley-required="true" data-parsley-required-message="Notice type is required" data-parsley-trigger="change focusout">
                                                        <option value="">-Select Soft Document-</option>

                                                    </select>
                                                    @if ($errors->has('notice_id2'))
                                                    <p class="help-block">
                                                        <strong>{{ $errors->first('notice_id2') }}</strong>
                                                    </p>
                                                    @endif
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="form-bottom-btn">
                                <a href="{{url('home')}}" class="btn btn-primary custom-btn customb-btn"><span>Cancel</span></a>
                                <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>Continue</span></button>

                            </div>
                        </div>  
                    </div></form>
            </div>
    </section>
</div>

@endsection
@section('frontend_js')
<script src="{{ asset('vendor/adminlte/plugins/datepicker/bootstrap-datepicker.js') }}" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function () {
    $('ul.nav.nav-tabs.radio-tabs li').click(function () {
        if ($('input[name=radio-1-set]:checked').val() == 'soft')
        {
            $('#notice_id1').attr('data-parsley-required', true);
            $('#notice_id2').removeAttr('data-parsley-required');
            $('#project_type1').attr('data-parsley-required', true);
            $('#project_type2').removeAttr('data-parsley-required');
            $('#project_type1').val('');
            $('#project_type1').selectpicker('refresh');
            $('#notice_id1').find('option:not(:first)').remove();
            $('#notice_id1').selectpicker('refresh');
        } else {
            $('#notice_id1').removeAttr('data-parsley-required');
            $('#notice_id2').attr('data-parsley-required', true);
            $('#project_type1').removeAttr('data-parsley-required');
            $('#project_type2').attr('data-parsley-required', true);

            $('#project_type2').val('');
            $('#project_type2').selectpicker('refresh');
            $('#notice_id2').find('option:not(:first)').remove();
            $('#notice_id2').selectpicker('refresh');
        }

        $('input[type="radio"]').attr('checked', false);
        $(this).find('input[type="radio"]').attr('checked', true);
    });
    $('.datepicker').datepicker({
        autoclose: true
    })
    //select notices with state_id

    $('#project_type1').on('change', function () {
        $state_id = this.value;

        $("#notice_id1").empty('');
        $.ajax({
            type: "post",
            url: "{{url('customer/new-work-order/get-hard-notices')}}",
            data: {'state_id': $state_id},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                $("#notice_id1").append("<option value=''>-Select Hard Document-</option>");
                //  console.log(response);
                $.each(response, function (e, val) {

                    $("#notice_id1").append("<option value='" + val.id + "'>" + val.name + "</option>");

                });
                $("#notice_id1").selectpicker("refresh");

            }
        });
    });

    //soft notices

    $('#project_type2').on('change', function () {
        $state_id = this.value;
        $('#notice_id2').empty('');
        $.ajax({
            type: "post",
            url: "{{url('customer/new-work-order/get-soft-notices')}}",
            data: {'state_id': $state_id},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                $("#notice_id2").append("<option value=''>-Select Soft Document-</option>");
                //  console.log(response);
                $.each(response, function (e, val) {
                    $("#notice_id2").append("<option value='" + val.id + "'>" + val.name + "</option>");
                });
                $("#notice_id2").selectpicker("refresh");
            }
        });
    });


});



</script>

@endsection