<style>
.zip-custom-errror .parsley-errors-list  {
      position: unset !important;
  }
  .zip-custom-errror{
      margin-top: 11px;
  }
  .bootstrap-select :focus{
    outline: 0;
    border-bottom : 1px solid #032c61 !important;
 }
  
 
 .select2class :focus{
    outline: 0;
    height: 47px !important;
    border-bottom : 1px solid #032c61 !important;
}

.select2-container--default .select2-selection--single, .select2-selection .select2-selection--single {
    border-radius: 0;
    padding: 6px 12px;
    height: 34px;
    border: none !important;
    border-bottom: 1px solid #aaa !important;
}

.select2-container--default .select2-selection--single {
    background: none !important;
    border: 1px solid #aaa;
    border-radius: 4px;
}
</style>
@extends('adminlte::page')
@section('content')
<div role="tabpanel" class="tab-pane active" id="workorder">
    <section class="work-order address-book-add-readonly">
        <div class="dashboard-wrapper">
            @if (Session::get('success'))
            <div class="alert alert-success msg-success no-margin">
                <?php echo Session::get('success'); ?>
            </div>
            @endif
            <div class="dashboard-heading">
                <h1><span>{{strtoupper($notice['name'])}}</span></h1>
            </div>
            <div class="dashboard-inner-body">
                <div class="order-by">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="order-details full-width list-inline">
                                <li><p>Company Name:</p> <span>{{$customer_details->company_name}}</span></li>
                                <li><p>Contact Name:</p> <span>{{$customer_details->contact_person}}</span></li>
                                <li><p>Phone Number:</p> <span> {{$customer_details->office_number}}</span></li>
                            </ul>
                            <ul class="order-details full-width list-inline">
                                <li><p>Mailing Address:</p> <span>{{$customer_details->mailing_address}}</span></li>
                                @if($id) 
                                <li><p>Work Order #:</p> <span>{{$id}}</span></li>
                                @endif
                                @if(isset($parent_work_order)  && !empty($parent_work_order))
                                <li><p>Parent Work Order #: </p> <span>{{$parent_work_order}}</span></li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="notice-tabs">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs display-ib pull-left" role="tablist">
                        @if($tab=="project")
                        <li class="active" id="project_tab" role="presentation" ><a href="#project" aria-controls="project" role="tab" data-toggle="tab">Project</a></li>
                        <li role="presentation"  id="recipient_tab"><a href="#recepients" aria-controls="recepients" role="tab" data-toggle="tab">Recipients</a></li>

                        @else 
                        <li role="presentation"><a href="#project" aria-controls="project" role="tab" data-toggle="tab">Project</a></li>
                        <li role="presentation" class="active"  id="recipient_tab"><a href="#recepients" aria-controls="recepients" role="tab" data-toggle="tab">Recipients</a></li>
                        @endif

                    </ul>  

                    <div class="correction-div">
                        <div class="display-ib pull-right">
                            <a href="#" class="btn btn-default corr-btn">Corrections</a>
                            <a href="#" class="btn btn-default notice-btn">Notes</a>
                            <div class="corr-block notice-block mCustomScrollbar" data-mcs-theme="light">
                                <div class="corr-block-wrapper" >
                                    <h4>Corrections</h4>
                                    <form id="edit_work_order_correction">
                                        <div class="correction-success alert alert-success no-margin" style="display: none"></div>
                                        <div class="correction-error alert alert-danger no-margin" style="display:none"></div>
                                        <div class="row">
                                            <input type="text" name="work_order" value="{{$id}}" id="work_order" hidden="">
                                            <div class="col-md-12 col-sm-12">
                                                <div class="input-wrapper full-width">
                                                    <div class="styled-input">
                                                        <input class="form-control"  type="text" value="{{old('correction')}}" name="correction" data-parlsey-required="true" data-parsley-required-message="Correction is required">
                                                        <label>Correction<span class="mandatory-field">*</span></label>
                                                        <span></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12">
                                                <div class="select-opt">
                                                    <label class="col-md-12 control-label" for="textinput"  name="Email*"></label> 
                                                    <select class="selectpicker" name="email" id="correction_email">
                                                        <option value="">Select</option>
                                                        @foreach($note_emails as $email)
                                                        <option value="{{$email}}" @if (old('email') == $email) selected="selected" @endif > {{$email}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div id="correctionloader" style="display: none">
                                                <img width="150px" src="{{asset('images/loader.gif')}}"/>
                                            </div>
                                            <div class="form-bottom-btn">
                                                <a href="#" class="btn btn-primary custom-btn customc-btn submitCorrection"><span>Submit</span></a>
                                            </div>
                                        </div>
                                    </form>
                                    <ul class="correction_div">
                                        @if(isset($corrections) && !empty($corrections))
                                        @foreach($corrections AS $each_correction)
                                        <li>
                                            {{$each_correction->correction}}<br>
                                            Added by:{{$each_correction->name}}<br>
                                            Created at:{{date('m-d-Y',strtotime($each_correction->created_at))}}

                                        </li>
                                        @endforeach
                                        @endif
                                    </ul>
                                    <a class="closebtn">&times;</a>
                                </div>
                            </div>
                            <div class="notes-block notice-block mCustomScrollbar" data-mcs-theme="light">
                                <div class="notes-block-wrapper">
                                    <form id="edit_work_order_note">
                                        <div class="note-success no-margin alert alert-success" style="display: none"></div>
                                        <div class="note-error no-margin alert alert-danger" style="display:none"></div>
                                        <h4>Notes</h4>
                                        <p>Order Number: {{$id}}</p>
                                        <div class="row">
                                            <input type="text" name="work_order" value="{{$id}}" id="work_order" hidden="">
                                            <div class="col-md-12 col-sm-12">
                                                <div class="input-wrapper full-width">
                                                    <div class="styled-input">
                                                        <input class="form-control"  type="text" value="{{old('note')}}" name="note" data-parlsey-required="true" data-parsley-required-message="Note is required">
                                                        <label>Note<span class="mandatory-field">*</span></label>
                                                        <span></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12">
                                                <div class="select-opt">
                                                    <label class="col-md-12 control-label" for="textinput"  name="Email*"></label> 
                                                    <select class="selectpicker" name="email" id="note_email">
                                                        <option value="">Select</option>
                                                        @foreach($note_emails as $email)
                                                        <option value="{{$email}}" @if (old('email') == $email) selected="selected" @endif > {{$email}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div id="noteloader" style="display: none">
                                                <img width="150px" src="{{asset('images/loader.gif')}}"/>
                                            </div>
                                            <div class="form-bottom-btn">
                                                <a href="#" class="btn btn-primary custom-btn customc-btn submitNote"><span>Submit</span></a>
                                            </div>
                                        </div>
                                    </form>
                                    <ul class="notes_div">
                                        @if(isset($notes) && !empty($notes))
                                        @foreach($notes AS $each_note)
                                        <li>
                                            {{$each_note->note}}<br>
                                            Added by:{{$each_note->name}}<br>
                                            Created at:{{date('m-d-Y',strtotime($each_note->created_at))}}
                                        </li>
                                        @endforeach
                                        @endif
                                    </ul>
                                    <a class="closebtn1">&times;</a>
                                </div>
                            </div>
                        </div>
                        <!--                                <div id="loaderDiv">
                                                        <img id="loading-image" src="{{ asset('loader.gif')}}" style="display:none;"/>
                                                    </div>-->
                    </div>


                    <!-- Tab panes -->
                    <div class="tab-content">
                        @if($tab=="project")
                        <div role="tabpanel" class="tab-pane active" id="project">
                            @elseif($tab=='recipients')
                            <div role="tabpanel" class="tab-pane" id="project">
                                @endif

                                <div class="project-wrapper">
                                    <form method="post" id="create_work_order" action="{{ url('customer/work-order/create/'.$id.'/'.$notice_id) }}" data-parsley-validate="">

                                        <div class="dash-subform">
                                            <div class="row">

                                                <div class="col-md-5">
                                                    <div class="sub-section-title">
                                                        <h2>Project Information</h2>
                                                    </div>
                                                </div>
                                                <div class="col-md-7 pull-right text-right">
                                                    
                                                    <a href="{{url('customer/view-work-order')}}" class="btn btn-primary custom-btn customb-btn"><span>Back</span></a>
                                                    <a href="{{url('home')}}" class="btn btn-primary custom-btn customb-btn"><span>Cancel</span></a>
                                                    <input type="submit" name="continue" class="btn btn-primary custom-btn customb-btn save_for_later" value="Save For Later" id=""save_for_later/>
                                                    <input type="submit" name="continue" class="btn btn-primary custom-btn customc-btn"  value="Continue"/>
                                                
                                                </div>
                                                {!! csrf_field() !!}
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="readonly full-width">
                                                            <label class="full-width">Work order no</label>
                                                            <p class="full-width">#{{$id}}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                @if(isset($parent_work_order) && $parent_work_order != '')
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="readonly full-width">
                                                            <label class="full-width">Parent work order</label>
                                                            <p class="full-width">{{$parent_work_order}}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endif
                                                @if(isset($duplicate) && !empty($duplicate))
                                                <input type="text" name="duplicate" value="{{$duplicate}}" hidden="">
                                                @endif
                                                <input type="text" name="notice_id" value="{{$notice['id']}}" hidden="">
                                                @if($notice_fields_section1)
                                                @foreach($notice_fields_section1 as $fields)
                                                @if($fields->type==6 && $fields->name == 'last_date_on_the_job')
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="date-box full-width">
                                                        <div class="input-wrapper full-width">

                                                            <div class="styled-input">
                                                                <?php $label = str_replace("_", " ", $fields->name); ?>

                                                                <input class="form-control datepicker"  type="text" name="{{$fields->notice_field_id.' '.$fields->name}} " value="{{$fields->value}}" id="{{$fields->name}}"  data-parsley-trigger="change focusout">
                                                                <label>{{ucfirst(trans($label))}}</label>

                                                                <span></span>
                                                            </div>
                                                        </div>
                                                        <!-- <button type="button" class="btn btn-default" data-original-title="" title=""><span class="icon-calendar"></span></button> -->
                                                    </div>
                                                </div>
                                                @elseif($fields->type==6)
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="date-box full-width">
                                                        <div class="input-wrapper full-width">

                                                            <div class="styled-input">
                                                                <?php $label = str_replace("_", " ", $fields->name); ?>

                                                                <input class="form-control datepicker" type="text" name="{{$fields->notice_field_id.' '.$fields->name}} " value="{{$fields->value}}" id="{{$fields->name}}" data-parsley-required="true" data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout">
                                                                <label>{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>

                                                                <span></span>
                                                            </div>
                                                        </div>
                                                        <!-- <button type="button" class="btn btn-default" data-original-title="" title=""><span class="icon-calendar"></span></button> -->
                                                    </div>
                                                </div>

                                                @elseif($fields->type==1 && $fields->name == 'date_request' )
                                                <?php $label = str_replace("_", " ", $fields->name); ?>
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="date-box  full-width">
                                                        <div class="input-wrapper full-width">
                                                            <input type="text" name="work_order" value="{{$id}}" id="work_order" hidden="">
                                                            <div class="styled-input">

                                                                <input class="form-control"  type="text" name="{{$fields->notice_field_id.' '.$fields->name}}" value="{{date('m-d-Y')}}" data-parsley-required="false" data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout" readonly="" >
                                                                <label class="full-width">Date</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @elseif($fields->type==1 && $fields->name == 'contracted_by' )
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="select-opt select2-container select2class">
                                                        <input type="hidden" value="1" name="contracted_by_exist">
                                                        <?php $label = str_replace("_", " ", $fields->name); ?>
                                                        <label class="col-md-12 control-label" for="textinput">{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>
                                                        <select class="js-example-tags form-control contracted_bys"  name="{{$fields->notice_field_id.'_'.$fields->name}}"  id="contracted_by_select" data-parsley-required="true" data-parsley-required-message="Contrcted by is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true">
                                                            <option value="">Select</option>
                                                            @if(isset($names) && !empty($names))
                                                            @foreach($names as $each_name)
                                                            <option value="{{$each_name->company_name}}" {{$fields->value==$each_name->company_name ? 'selected="selected"' : ''}}>{{$each_name->company_name}}</option>
                                                            @endforeach
                                                            @endif
                                                        </select>

                                                        <span></span>
                                                    </div>
                                                </div>
                                                                    
                                                <div class="col-md-6 col-sm-6 clear-b" >
                                                        <div class="date-box full-width">
                                                            <div class="input-wrapper full-width" id="hidden-panel" style="display:none">
                                                                <div class="styled-input">
                                                                    <label class="lableall" for="textinput">Contract Address</label>
                                                                    <textarea id="addname" class="form-control" name="addname" data-parsley-trigger="change focusout"  rows="1" disabled></textarea>
                                                                </div>
                                                             </div>
                                                        </div>
                                                </div>
                                                <div class="col-md-6 col-sm-6 clear-b">
                                                    <div class="add-new-btn inner-dash-btn full-width">
                                                        <button type="button" class="btn btn-primary custom-btn add_contact_details contracted_by_recipient" name="add_contact_submit" value="contacts" id="add_contact_contracted_by" data-toggle="modal" data-target="#contactModal"><span>+ Add/Update Address Book</span></a>
                                                    </div>
                                                </div>

                                               <!--  <button type="button" name="add_contact_submit" class="btn btn-primary custom-btn customc-btn add_contact_details contracted_by_recipient" value="contacts" id="add_contact" data-toggle="modal" data-target="#contactModal"><span>Add Address Book</span></button> -->

                                                @elseif($fields->type==1 && $fields->name == 'parent_work_order' )
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="select-opt">
                                                        <?php $label = str_replace("_", " ", $fields->name); ?>
                                                        <label class="col-md-12 control-label" for="textinput">{{ucfirst(trans($label))}}</label>
                                                        <input type="hidden" id="parent_work_order_id" value="{{$fields->value}}"/>
                                                        <input type="hidden" name="parent_work_order_previous" id="parent_work_order_previous" value="{{$fields->value}}"/>
                                                        <!-- <select class="selectpicker" name="{{$fields->notice_field_id.' '.$fields->name}}"   data-show-subtext="true" data-live-search="true" id="soft_doc_parent_work_order" data-parsley-required="false" data-parsley-required-message="Parent Work Order number is required" data-parsley-trigger="change focusout">

                                                        </select> -->
                                                        <select class="form-control"  name="{{$fields->id.' '.$fields->name}}" id="soft_doc_parent_work_order" data-show-subtext="true" data-live-search="true">
                                                            <?php if(!empty($fields->value)){ ?>
                                                                <option value="{{$fields->value}}" selected="selected">{{$fields->value}}</option>   
                                                            <?php }else{ ?>
                                                                <option value=" ">Select</option>
                                                            <?php } ?>                                
                                                        </select>

                                                    </div>
                                                </div>
                                                @elseif($fields->type==1 && $fields->name == 'project_name' )
                                                <?php $label = str_replace("_", " ", $fields->name); ?>
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="input-wrapper full-width">
                                                        <div class="styled-input">
                                                            <?php $label = str_replace("_", " ", $fields->name); ?>
                                                            <input class="form-control" type="text" value="{{$fields->value}}" name="{{$fields->notice_field_id.' '.$fields->name}}" id="{{$fields->name}}">
                                                            <label>{{ucfirst(trans($label))}}</label>
                                                            <span></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                @elseif($fields->type==1  && $fields->name == 'your_job_reference_no' )
                                                <?php $label = str_replace("_", " ", $fields->name); ?>
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="input-wrapper full-width">
                                                        <div class="styled-input">
                                                            <?php $label = str_replace("_", " ", $fields->name); ?>
                                                            <input class="form-control" value="{{$fields->value}}" type="text" name="{{$fields->notice_field_id.' '.$fields->name}}" id="{{$fields->name}}" data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout">
                                                            <label>{{ucfirst(trans($label))}}</label>
                                                            <span></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                @elseif($fields->type==1 && $fields->name =='county')
                                                <div class="col-md-6 col-sm-6">
                                                    <?php $label = str_replace("_", " ", $fields->name);
                                                    ?>

                                                    <div class="input-wrapper full-width">
                                                        <div class="styled-input">
                                                            <input class="form-control" value="{{getCounty($fields->value)}}"  type="text" name="recipient_county_id" id="county" data-parsley-required="true" data-parsley-required-message="Select County from the list" data-parsley-trigger="change focusout">
                                                            <input type="hidden" name="{{$fields->notice_field_id.' '.$fields->name}}"  value="{{$fields->value}}"id="recipient_county_id"/>
                                                            <label class="col-md-12 control-label" for="textinput">{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label> 
                                                            <span></span>                                                                
                                                        </div>
                                                        <div id="recipientcountyloader" style="display: none">
                                                            <img width="150px" src="{{asset('images/loader.gif')}}"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                @elseif($fields->type==1 && $fields->name=="project_address_count")
                                                @if(!Auth::user()->hasRole('customer'))
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="input-wrapper full-width">
                                                        <div class="styled-input">
                                                            <input class="form-control"  type="text" name="{{$fields->notice_field_id.' '.$fields->name}}" value="<?php echo $fields->value ?>" id="{{$fields->name}}" data-parsley-required="true" data-parsley-required-message="Project Address Count is required" data-parsley-trigger="change focusout">
                                                            <label>Project Address Count</label>
                                                            <span></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                @else
                                                <input class="form-control"  type="hidden" name="{{$fields->notice_field_id.' '.$fields->name}}" value="1" id="{{$fields->name}}" data-parsley-required="true" data-parsley-required-message="Project Address Count is required" data-parsley-trigger="change focusout">
                                                @endif
                                                 @elseif($fields->type==1 && $fields->name =='total_amount_satisfied')
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="input-wrapper full-width">
                                                            <div class="styled-input">
                                                                <?php $label = str_replace("_", " ", $fields->name); ?>
                                                                <input class="form-control"  type="text" name="{{$fields->notice_field_id.' '.$fields->name}}" value="{{$fields->value}}" id="{{$fields->name}}" data-parsley-required="true"  data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout"  data-parsley-type="digits"  min="1" >
                                                                <label>($){{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>
                                                                <span></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @elseif($fields->type==1)
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="input-wrapper full-width">
                                                        <div class="styled-input">
                                                            <?php $label = str_replace("_", " ", $fields->name); ?>
                                                            <input class="form-control" value="{{$fields->value}}" type="text" name="{{$fields->notice_field_id.' '.$fields->name}}" id="{{$fields->name}}" data-parsley-required="true" data-parsley-required-message="{{ucfirst($fields->name)}} is required" data-parsley-trigger="change focusout" data-parsley-pattern="/^[a-zA-Z0-9 ]*$/">
                                                            <label>{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>

                                                        </div>
                                                    </div>
                                                </div>


                                                @elseif($fields->type==2 && $fields->name=="project_address")
                                                <div class="col-md-6 col-sm-6 clear-b">
                                                    <div class="input-wrapper full-width">
                                                        <label class="col-md-12 control-label">Project Address<span class="mandatory-field">*</span> (Please enter multiple addresses separated by **)</label>
                                                        <div class="styled-input">
                                                            <?php $label = str_replace("_", " ", $fields->name);
                                                              $project_address = preg_replace("/<br \/>/", "<p>", $fields->value);  
                                                            ?>
                                                            <textarea id="{{$fields->name}}" class="form-control projects_address"  name="{{$fields->notice_field_id.' '.$fields->name}}" data-parsley-required="true" data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout"  rows="5">{{$project_address}}</textarea>
                                                            <span></span>
                                                        </div>
                                                    </div>
                                                </div>

                                                @elseif($fields->type==2)
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="input-wrapper full-width">
                                                        <div class="styled-input">
                                                            <?php $label = str_replace("_", " ", $fields->name); ?>
                                                            <textarea id="{{$fields->name}}" class="form-control" name="{{$fields->notice_field_id.' '.$fields->name}}" value="{{$fields->value}}" data-parsley-required="true" data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout">{{strip_tags($fields->value)}}</textarea>


                                                            <label>{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>
                                                            <span></span>
                                                        </div>
                                                    </div>
                                                </div>



                                                @elseif($fields->type==3 && $fields->name == 'project_type')
                                                <?php $project_type = $fields->value; ?>
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="select-opt">
                                                        <?php $label = str_replace("_", " ", $fields->name); ?>
                                                        <label class="col-md-12 control-label" for="textinput">{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>  
                                                        <select class="selectpicker"  name="{{$fields->notice_field_id.' '.$fields->name}}" data-parsley-required="true" data-parsley-required-message="Project type is required" data-parsley-trigger="change focusout">
                                                            <option value="" >Select Type</option>
                                                            @foreach($projectTypes as $type)
                                                            <option value="{{$type->id}}"  {{$fields->value==$type->id ? 'selected="selected"' : ''}}>{{$type->type}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                @elseif($fields->type==3 && $fields->name == 'your_role')
                                                <?php $your_role = $fields->value; ?>
                                                <div class="col-md-6 col-sm-6 clear-b">
                                                    <div class="select-opt">
                                                        <?php $label = str_replace("_", " ", $fields->name); ?>
                                                        <label class="col-md-12 control-label" for="textinput">{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>  
                                                        <select class="selectpicker" name="{{$fields->notice_field_id.' '.$fields->name}}" data-parsley-required="true" data-parsley-required-message="Your role is required" data-parsley-trigger="change focusout">
                                                            <option value="">Select Role</option>
                                                            @foreach($project_roles as $category)
                                                            <option value="{{$category->id}}" {{ $fields->value == $category->id ? 'selected="selected"' : '' }}>{{$category->name}}</option>
                                                            @endforeach


                                                        </select>
                                                    </div>
                                                </div>

                                                @elseif($fields->type==3 && $fields->name =='city')
                                                <?php $city_id = $fields->value; ?>
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="select-opt">
                                                        <?php $label = str_replace("_", " ", $fields->name); ?>
                                                        <label class="col-md-12 control-label" for="textinput">{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>  
                                                        <select class="selectpicker" name="{{$fields->notice_field_id.' '.$fields->name}}" id="soft_notice_city_value" data-parsley-required="true" data-parsley-required-message="City is required" data-parsley-trigger="change focusout">

                                                            @foreach($cities as $city)
                                                            <option value="{{$city->id}}" {{ $city_id == $city->id ? 'selected="selected"' : '' }}>{{$city->name}}</option>
                                                            @endforeach 
                                                        </select>
                                                    </div>
                                                </div>

                                                @elseif($fields->type==3 && $fields->name =='state')
                                                <?php $state_id = $fields->value; ?>
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="select-opt">
                                                        <?php $label = str_replace("_", " ", $fields->name); ?>
                                                        <label class="col-md-12 control-label" for="textinput" >{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>  
                                                        <select class="selectpicker" id="soft_notice_state_value"  name="{{$fields->notice_field_id.' '.$fields->name}}" id="soft_notice_state_value" data-parsley-required="true" data-parsley-required-message="State is required" data-parsley-trigger="change focusout">
                                                            <option value="">Select State</option>
                                                            @foreach($states as $state)
                                                            <option value="{{$state->id}}" @if ($fields->value == $state->id) selected="selected" @endif >{{$state->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                @elseif($fields->type==3 && $fields->name =='county')
                                                <?php $country_id = $fields->value; ?>
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="select-opt">
                                                        <?php $label = str_replace("_", " ", $fields->name); ?>
                                                        <label class="col-md-12 control-label" for="textinput"  >{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>  

                                                        <select class="selectpicker" name="{{$fields->notice_field_id.' '.$fields->name}}" id="country_id" value="{{$fields->value}}" data-parsley-required="true" data-parsley-required-message="Country is required" data-parsley-trigger="change focusout">

                                                            @foreach($countries as $country)
                                                            <option value="{{$country->id}}" {{ $country_id == $country->id ? 'selected="selected"' : '' }}>{{$country->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                @elseif($fields->type==3 && $fields->name =='authorise_agent')



                                                <div class="col-md-6 col-sm-6">
                                                    <div class="select-opt">
                                                        <?php $label = str_replace("_", " ", $fields->name); ?>
                                                        <label class="col-md-12 control-label" for="textinput">{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>  
                                                        <select 
                                                            <select class="selectpicker" name="{{$fields->notice_field_id.' '.$fields->name}}" id="{{$fields->name}}" class="selectpicker"  data-parsley-required="true" data-parsley-required-message="Authorise signature is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true">
                                                                <option value="">Select</option>
                                                                @if(isset($officers_directors) && count($officers_directors)>0)
                                                                @foreach($officers_directors as $officer)
                                                                <option value="{{$officer->first_name.' '.$officer->last_name}}" @if ($fields->value == $officer->first_name.' '.$officer->last_name) selected="selected" @endif > {{ucfirst(trans($officer->first_name.' '.$officer->last_name))}}</option>
                                                                @endforeach
                                                                @endif
                                                            </select>
                                                    </div>
                                                </div>
                                                @elseif($fields->type==3 && $fields->name =='title')



                                                <div class="col-md-6 col-sm-6">
                                                    <div class="select-opt">
                                                        <?php $label = str_replace("_", " ", $fields->name); ?>
                                                        <label class="col-md-12 control-label" for="textinput">{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>  
                                                        <select 
                                                            <select class="selectpicker" name="{{$fields->notice_field_id.' '.$fields->name}}" id="{{$fields->name}}" class="selectpicker"  data-parsley-required="true" data-parsley-required-message="Authorise signature is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true">
                                                                <option value="">Select</option>
                                                                @if(isset($officers_directors) && count($officers_directors)>0)
                                                                @foreach($officers_directors as $officer)
                                                                <option value="{{$officer->title}}" @if ($fields->value == $officer->title) selected="selected" @endif > {{ucfirst(trans($officer->title))}}</option>
                                                                @endforeach
                                                                @endif
                                                            </select>
                                                    </div>
                                                </div>
                                                @elseif($fields->type==4)
                                                <?php $label = str_replace("_", " ", $fields->name); ?>
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="input-wrapper full-width">
                                                        <div class="styled-input">      
                                                            <input class="checkbox" type="checkbox" name=""  value="1" id="attorneys_fees">
                                                            <label>
                                                                Attorneys Fees
                                                            </label>  
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="col-md-6 col-sm-6" id="showattorneysfee">
                                                    <div class="input-wrapper full-width">
                                                        <div class="styled-input">


                                                            <input class="form-control" type="textbox" name="{{$fields->notice_field_id.' '.$fields->name}}"  value="" id="{{$fields->name}}" placeholder="%">
                                                            <label>{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>
                                                        </div>
                                                    </div>           
                                                </div>


                                                @elseif($fields->type==5)
                                                <?php $label = str_replace("_", " ", $fields->name); ?>
                                                <div class="col-md-4 col-sm-6">
                                                    <div class="date-box full-width">
                                                        <div class="input-wrapper full-width">
                                                            <div class="styled-input">
                                                                <input class="form-control datepicker" required=""  type="radio" name="{{$fields->notice_field_id.' '.$fields->name}}" value="{{$fields->value}}" data-parsley-required="true" data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout">
                                                                <label>{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>
                                                                <span></span>
                                                            </div>
                                                        </div>
                                                       <!--  <button type="button" class="btn btn-default" data-original-title="" title=""><span class="icon-calendar"></span></button> -->
                                                    </div>
                                                </div>
                                                @endif
                                                @endforeach
                                                @endif

                                            </div>
                                        </div>
                                        <div class="dash-subform">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="sub-section-title">
                                                        <h2>Service/Labor Furnished</h2>
                                                    </div>  
                                                </div>
                                                @if($notice_fields_section2)
                                                @foreach($notice_fields_section2 as $fields)

                                                @if($fields->type==2 && $fields->name=="folio_s" || $fields->name == 'legal_description')
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="input-wrapper full-width">
                                                        <div class="styled-input">
                                                            <?php $label = str_replace("_", " ", $fields->name); ?>
                                                            <textarea class="form-control"  type="text" name="{{$fields->notice_field_id.' '.$fields->name}}" id="{{$fields->name}}"  data-parsley-required="false" data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout">{{strip_tags($fields->value)}}</textarea>
                                                            <label>{{ucfirst(trans($label))}} </label>
                                                            <span></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                @elseif($fields->type==2 && $fields->name == 'labor_service_furnished')
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="input-wrapper full-width">
                                                        <div class="styled-input">
                                                            <?php $label = str_replace("_", " ", $fields->name); ?>
                                                            <textarea class="form-control"  type="text" name="{{$fields->notice_field_id.' '.$fields->name}}" id="{{$fields->name}}" data-parsley-required="true" data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout">{{strip_tags($fields->value)}}</textarea>
                                                            <label>{{ucfirst(trans($label))}}<span class="mandatory-field">*</span> </label>
                                                            <span></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                @elseif($fields->type==1)
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="input-wrapper full-width">
                                                        <div class="styled-input">
                                                            <?php $label = str_replace("_", " ", $fields->name); ?>
                                                            <input class="form-control"  type="text" name="{{$fields->notice_field_id.' '.$fields->name}}"  data-parsley-required="true" data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout" data-parsley-pattern="/^[a-zA-Z0-9 ]*$/">
                                                            <label>{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>
                                                            <span></span>
                                                        </div>
                                                    </div>
                                                </div>


                                                @elseif($fields->type==2 && $fields->name=="project_address")
                                                <div class="col-md-6 col-sm-6 clear-b">
                                                    <div class="input-wrapper full-width">
                                                        <label class="col-md-12 control-label">Project Address<span class="mandatory-field">*</span></label>
                                                        <div class="styled-input">
                                                            <?php $label = str_replace("_", " ", $fields->name);
                                                            $project_address = preg_replace("/<br \/>/", "<p>", $fields->value);  
                                                            ?>
                                                            <textarea id="{{$fields->name}}" class="form-control projects_address" name="{{$fields->notice_field_id.' '.$fields->name}}" data-parsley-required="true" data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout"  rows="5">{{$project_address}}</textarea>
                                                            <span></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                @elseif($fields->type==2)
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="input-wrapper full-width">
                                                        <div class="styled-input">
                                                            <?php $label = str_replace("_", " ", $fields->name); ?>
                                                            <textarea id="{{$fields->name}}" class="form-control" name="{{$fields->notice_field_id.' '.$fields->name}}" data-parsley-required="true" data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout">{{strip_tags($fields->value)}}</textarea>


                                                            <label>{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>
                                                            <span></span>
                                                        </div>
                                                    </div>
                                                </div>


                                                @elseif($fields->type==3 && $fields->name =='city')
                                                <input type="hidden" name="hidden_city_val" id="hidden_city_val"value="{{$fields->value}}">
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="select-opt">
                                                        <?php $label = str_replace("_", " ", $fields->name); ?>
                                                        <label class="col-md-12 control-label" for="textinput">{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>  
                                                        <select class="selectpicker" name="{{$fields->notice_field_id.' '.$fields->name}}" data-parsley-required="true" data-parsley-required-message="City is required" data-parsley-trigger="change focusout">
                                                            <option value="">Select City</option>
                                                            @foreach($cities as $city)
                                                            <option value="{{$city->id}}">{{$city->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                @elseif($fields->type==3 && $fields->name =='state')
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="select-opt">
                                                        <?php $label = str_replace("_", " ", $fields->name); ?>
                                                        <label class="col-md-12 control-label" for="textinput"  >{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>  
                                                        <select class="selectpicker" name="{{$fields->notice_field_id.' '.$fields->name}}" data-parsley-required="true" data-parsley-required-message="State is required" data-parsley-trigger="change focusout">
                                                            <option value="">Select State</option>
                                                            @foreach($states as $state)
                                                            <option value="{{$state->id}}">{{$state->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>


                                                @elseif($fields->type==3 && $fields->name =='county')
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="select-opt">
                                                        <?php $label = str_replace("_", " ", $fields->name); ?>
                                                        <label class="col-md-12 control-label" for="textinput"  >{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>  
                                                        <select class="selectpicker" name="{{$fields->notice_field_id.' '.$fields->name}}" data-parsley-required="true" data-parsley-required-message="Country is required" data-parsley-trigger="change focusout">

                                                            @foreach($countries as $country)
                                                            <option value="{{$country->id}}">{{$country->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>


                                                @elseif($fields->type==4)
                                                <?php $label = str_replace("_", " ", $fields->name); ?>
                                                <div class="col-md-6 col-sm-6 clear-b">
                                                    <div class="date-box full-width">
                                                        <div class="input-wrapper full-width">
                                                            <div class="styled-input">
                                                                <input class="form-control datepicker" required="" type="checkbox" name="{{$fields->notice_field_id.' '.$fields->name}}" value="{{$fields->value}}" data-parsley-required="true" data-parsley-required-message="Country is required" data-parsley-trigger="change focusout">
                                                                <label>{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>
                                                                <span></span>
                                                            </div>
                                                        </div>
                                                        <!-- <button type="button" class="btn btn-default" data-original-title="" title=""><span class="icon-calendar"></span></button> -->
                                                    </div>
                                                </div>

                                                @elseif($fields->type==5)
                                                <?php $label = str_replace("_", " ", $fields->name); ?>
                                                <div class="col-md-4 col-sm-6">
                                                    <div class="date-box full-width">
                                                        <div class="input-wrapper full-width">
                                                            <div class="styled-input">
                                                                <input class="form-control datepicker" required=""  type="radio" name="{{$fields->notice_field_id.' '.$fields->name}}" value="{{$fields->value}}" data-parsley-required="true" data-parsley-required-message="Country is required" data-parsley-trigger="change focusout">
                                                                <label>{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>
                                                                <span></span>
                                                            </div>
                                                        </div>
                                                       <!--  <button type="button" class="btn btn-default" data-original-title="" title=""><span class="icon-calendar"></span></button> -->
                                                    </div>
                                                </div>
                                                @endif
                                                @endforeach
                                                @endif
                                                <?php
                                                $document_types = getDocumentTypes();
                                                ?>
                                                <div class="col-md-12 col-sm-12">
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                                            <div class="select-opt mt-top">
                                                                <label class="col-md-12 control-label" for="textinput">Document Type</label>  
                                                                <select class="selectpicker" name="type" id="soft_notice_work_order_doc_type">
                                                                    <option value="">-Select-</option>
                                                                    @foreach ($document_types as $key => $value) 
                                                                    <option value="{{$key}}">{{$value}}</option>
                                                                    @endforeach
                                                                </select>
                                                                @if ($errors->has('type'))
                                                                <span class="help-block">
                                                                    <strong>{{ $errors->first('type') }}</strong>
                                                                </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-sm-12 soft_notice_work_order_doc_field">
                                                            <div class="row">
                                                                <div class="col-md-12 col-sm-12">
                                                                    <div class="input-wrapper full-width">
                                                                        <div class="styled-input">
                                                                            <input class="form-control" name="doc_input"  type="text" id="soft_notice_work_order_doc_input">
                                                                            <label id="doc_field"></label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <input type="hidden" name="edit_document_name" id="edit_document_name">
                                                        <input type="hidden" name="edit_doc_id" id="edit_doc_id">
                                                        <input type="hidden" id="row_id">
                                                        <div class="form-btn-div soft_notice_work_order_doc_file">
                                                            <span class="btn btn-default btn-file upload-btn btn btn-primary form-btn" id="temp">
                                                                Choose File to Upload  <input type="file" name="file_name" id="soft_notice_work_order_result_file">

                                                            </span>
                                                            (Allow Types:csv,xls,xlsx,pdf,doc,docx, jpg, jpeg)
                                                            <span id="errormessage" style="display: none"></span>
                                                        </div>
                                                        <div class="form-btn-div doc_file col-md-12 col-sm-12 col-xs-12" >
                                                            <div style="float: left;width: 10">
                                                                <a href="javascript:;" onClick="openTab(this)" id="open_doc" name=""><span  id="preview" style="display: none;" ></span></a></div>
                                                            <div style="float: left ;width: 10;padding-left: 20px">
                                                                <a  href="javascript:;" ><span id="remove_icon" style="display: none;"  >Delete</span></a></div>

                                                            <input type="hidden" name="remove_file" id="remove_file" value="0">
                                                        </div>
                                                        <div class="col-md-12 col-sm-12 col-xs-12 soft_notice_work_order_doc_file">

                                                            <div class="add-new-btn inner-dash-btn full-width ">
                                                                <button  name="submit_doc" type="button" value="Add" id="submit_document" class="btn btn-primary custom-btn">
                                                                    <span class="update_doc">Add</span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-sm-12">
                                                            <div class="row">
                                                                <div class="col-lg-12 col-md-12 col-sm-12">
                                                                    <div class="dahboard-table doc-table full-width table-responsive">
                                                                        <table class="table table-striped" id="edit_document_table">
                                                                            <thead class="thead-dark">
                                                                                <tr>
                                                                                    <th scope="col">Document Type</th>
                                                                                    <th scope="col">Document Description</th>
                                                                                    <th scope="col">Name</th>
                                                                                    <th scope="col">Date & Time</th>
                                                                                    <th scope="col">Type of Attachment</th>
                                                                                    <th scope="col">Options</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                @if(isset($attachment) && !empty($attachment) && count($attachment)>0)
                                                                                @foreach($attachment AS $each_attachment)
                                                                                <?php
                                                                                $file_name = explode('.', $each_attachment->file_name);
                                                                                if ($each_attachment->visibility != 1) {
                                                                                    ?>
                                                                                    <tr id="refreshdelete{{$each_attachment->id}}">
                                                                                        <td>
                                                                                            @foreach ($document_types as $key => $value) 
                                                                                            @if ($each_attachment->type == $key) 
                                                                                            {{$value}}
                                                                                            @endif
                                                                                            @endforeach
                                                                                        </td>
                                                                                        <td>{{$each_attachment->title}}</td>
                                                                                        <td> <?php
                                                                                            $extension = explode('.', $each_attachment->original_file_name);
                                                                                            if ($each_attachment->original_file_name != "" && isset($extension[1])) {
                                                                                                ?><a href="{{asset('attachment/work_order_document/'.$each_attachment->file_name)}}" target="_blank">{{$each_attachment->original_file_name}}</a> <?php
                                                                                            } else {
                                                                                                echo "";
                                                                                            }
                                                                                            ?></td>
                                                                                        <td>{{date('d M y h:i A',strtotime($each_attachment->created_at))}}</td>
                                                                                        <td>
                                                                                            <?php
                                                                                            $extension = explode('.', $each_attachment->original_file_name);
                                                                                            if ($each_attachment->original_file_name != "" && isset($extension[1])) {
                                                                                                echo $ext=$extension[1];
                                                                                            } else {
                                                                                                echo $ext="";
                                                                                            }
                                                                                            ?>

                                                                                        </td>
                                                                                        <td><a class='btn btn-secondary item red-tooltip remove-attachment' data-toggle='modal' 
                                                                                               data-url='{{url('customer/edit/work-ordwer/removeattachment')}}'  data-id="{{$each_attachment->id}}" data-file='{{$each_attachment->file_name}}' 
                                                                                               data-target='#attachment-confirmation-modal' data-placement='bottom' title='Remove'>
                                                                                                <span class='icon-cross-remove-sign'></span></a>
                                                                                            <a class='btn btn-secondary item red-tooltip edit-attachment'
                                                                                                   data-id="{{$each_attachment->id}}" data-file='{{$each_attachment->file_name}}' data-type="{{$each_attachment->type}}" data-title="{{$each_attachment->title}}"  data-file-original-name="{{$each_attachment->original_file_name}}" data-file-extension={{$ext}}>
                                                                                                <span class='icon-pencil-edit-button'></span>
                                                                                            </a>
                                                                                            @if(!empty($each_attachment->file_name))
                                                                                            <a href="{{asset('attachment/work_order_document/'.$each_attachment->file_name)}}" target="_blank" class="btn btn-secondary item red-tooltip"><i class="fa fa-eye" style=""></i></a>
                                                                                            @endif
                                                                                        </td>
                                                                                    </tr>
                                                                                <?php } ?>
                                                                                @endforeach
                                                                                @else
                                                                            <td class="text-center norecords" colspan="6">No record found</td>
                                                                            @endif
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <input type="hidden" name="edit_soft_notices_attachment[]" id="edit_soft_notices_attachment"/>
                                                <input type="hidden" name="remove_doc[]" id="remove_doc"/>
                                                <div class="form-bottom-btn save_btn1">
                                                    <a href="{{url('customer/view-work-order')}}" class="btn btn-primary custom-btn customb-btn"><span>Back</span></a>
                                                    <a href="{{url('home')}}" class="btn btn-primary custom-btn customb-btn"><span>Cancel</span></a>
                                                    <input type="submit" name="continue" class="btn btn-primary custom-btn customb-btn save_for_later" value="Save For Later" id=""save_for_later/>
                                                    <input type="submit" name="continue" class="btn btn-primary custom-btn customc-btn"  value="Continue"/>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>

                            @if($tab=="project")
                            <div role="tabpanel" class="tab-pane" id="recepients">
                                @elseif($tab=='recipients')
                                <div role="tabpanel" class="tab-pane active" id="recepients">
                                    @endif
                                    <div class="project-wrapper">
                                        <form action="{{url('customer/work-order/recipient/create/'.$id.'/'.$notice_id)}}" method="post" data-parsley-validate="" id="update_recipient_data">
                                            {!! csrf_field() !!}  
                                            <div class="dash-subform mt-0">
                                            <div class="row">
                                               
                                                <div  class="col-md-12 pull-right text-right">
                                                      <a type="button" id="editBack" name="editBack" class="btn btn-primary custom-btn customb-btn not-empty" value="Back"><span>back</span></a>

                                                            <a href="{{url('home')}}" class="btn btn-primary custom-btn customb-btn"><span>Cancel</span></a>

                                                            <a href="{{url('customer/work-order/updateStatus/'.$id.'/'.'0')}}" class="btn btn-primary custom-btn customb-btn" name="submit" value="save_for_later"><span>Save for Later</span></a>

                                                              

 @if($status=='0')
                                                            <a  href="{{url('customer/work-order/updateStatus/'.$id.'/'.'1')}}"  class="btn btn-primary custom-btn customc-btn" name="submit" value="submit" title="Please fill all mandetory project information" disabled="" style="pointer-events: none"><span>Submit</span></a>

                                                            @elseif($status=='1')
                                                            <a  href="{{url('customer/work-order/updateStatus/'.$id.'/'.'1')}}"  class="btn btn-primary custom-btn customc-btn" name="submit" value="submit" ><span>Submit</span></a>
                                                            @endif   
                                                </div>
                                            </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="sub-section-title">
                                                                    <h2>Recipients</h2>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="dahboard-table table-responsive">
                                                                    <table class="table table-striped">
                                                                        <thead class="thead-dark">
                                                                            <tr>
                                                                                <th scope="col">Category</th>
                                                                                <th scope="col">Recipients</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>

                                                                            @if(isset($count) && count($count)>0)
                                                                            @foreach($count as $key => $count)
                                                                            <tr>

                                                                                <td class="checkbox-holder">
                                                                                    <input type="checkbox" id="{{$key}}" class="regular-checkbox"><label for="{{$key}}">{{$count->name}}</label>
                                                                                </td>
                                                                                <td>{{$count->total}}</td>
                                                                            </tr>
                                                                            @endforeach
                                                                            @else
                                                                        <td class="text-center" colspan="4">{{'No records found'}}</td>
                                                                        @endif

                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="sub-section-title">
                                                                    <h2>Selected Recipients</h2>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="dahboard-table table-responsive">
                                                                    <table class="table table-striped">
                                                                        <thead class="thead-dark">
                                                                            <tr>
                                                                                <th scope="col">Role</th>
                                                                                <th scope="col">Company Name</th>
<!--                                                    <th scope="col">Contact Person</th>-->
<!--                                                    <th scope="col">Phone Number</th>-->
                                                                                <th scope="col">Address</th>
                                                                                <th scope="col">City</th>
                                                                                <th scope="col">State</th>
                                                                                <th scope="col">Zip Code</th>
                                                                                <th scope="col">Options</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>


                                                                            @if(isset($recipients) && count($recipients) > 0)

                                                                            @foreach($recipients  as $recipient)
                                                                            <tr>
                                                                                <td class="cl-blue">{{$recipient->category->name}}</td>
                                                                                <td>{{$recipient->name}}</td>
    <!--                                                                            <td>{{$recipient->contact}}</td>-->
    <!--                                                                            <td>{{$recipient->mobile}}</td>-->
                                                                                <td>
                                                                                   
                                                                                    {{$recipient->address}}
                                                                                    
                                                                                </td>
                                                                                <td>{{$recipient->city->name}}</td>
                                                                                <td>{{$recipient->state->name}}</td>
                                                                                <td>{{$recipient->zip}}</td>
                                                                                <td>
                                                                                    <a href="#recipient" data-id="{{$recipient->id}}" class="edit_recipient"><span class="icon-pencil-edit-button"></span></a>
                                                                                  <!--<a href="{{url('customer/recipient/deleteCreated/'.$recipient->id.'/'.$notice_id.'/'.$id)}}"><span class="icon-cross-remove-sign"></span></a>-->
                                                                                    <a data-id="{{$recipient->id}}" data-notice-id="{{$notice_id}}" data-workorder-id="{{$id}}" href="#" class="remove-record" data-toggle='modal' data-toggle="tooltip" data-target='#contact-confirmation-modal' data-placement="bottom" title="Remove"><span class="icon-cross-remove-sign"></span></a>
                                                                                </td>
                                                                            </tr>
                                                                            @endforeach
                                                                            @else
                                                                        <td class="text-center" colspan="4">{{'No records found'}}</td>
                                                                        @endif


                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="dash-subform" id="recipient">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="sub-section-title">
                                                            <h2>Recipients</h2>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <div class="row">
                                                            <div class="col-md-6 col-sm-6">

                                                                <div class="select-opt">
                                                                    <label class="col-md-12 control-label" for="textinput">Type of Recipient<span class="mandatory-field">*</span></label>  
                                                                    <input type="text" name="work_order_id" value="{{$id}}" hidden="">
                                                                    <input type="hidden" name="recipient_id" id="recipient_id">
                                                                    <select class="selectpicker" name="category_id" id="category_id" data-parsley-required="true" data-parsley-required-message="Type of recipient is required">
                                                                        <option value="">Select</option>
                                                                        @foreach($categories as $category)
                                                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="select-opt select2-container select2class">
                                                            <label class="col-md-12 control-label" for="textinput">Name<span class="mandatory-field">*</span></label>  
                                                            <select class="js-example-tags form-control recipient_name"  name="name" id="recipient_name" data-parsley-required="true" data-parsley-required-message="Name is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true">
                                                                <option value="">Select</option>
                                                                @if(isset($names) && !empty($names))
                                                                @foreach($names as $each_name)
                                                                <option data-id="{{$each_name->id}}"  value="{{$each_name->company_name}}">{{$each_name->company_name}}</option>
                                                                @endforeach
                                                                @endif
                                                            </select>
                                                            <input type="hidden" name="recipt_id"  value="" id="recipt_id">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-bottom-btn pd-25">
                                                            <button type="button" name="add_contact_submit" class="btn btn-primary custom-btn customc-btn add_contact_details" value="contacts" id="add_contact" data-toggle="modal" data-target="#contactModal"><span>Add/Update Address Book</span></button>
                                                            <button type="button" name="add_contact_submit" class="btn btn-primary custom-btn customc-btn update_contact" value="contacts" id="update_contact" ><span>Update Address Book</span></button>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="input-wrapper full-width">
                                                            <label class="col-md-12 control-label" for="textarea">Attn/Additional Recipient Name<span class="mandatory-field"></span></label>
                                                            <div class="styled-input">
                                                                <textarea class="form-control recipient_attns"  type="text" name="attn" id="recipient_attn" data-parsley-trigger="change focusout" ></textarea>
                                                                <span></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="input-wrapper full-width">
                                                            <div class="styled-input">
                                                                <input class="form-control business_phone_validation"  type="text" name="contact" id="reipient_contact" data-parsley-trigger="change focusout" data-parsley-pattern="(\(\d{3})\)\s\d{3}\-\d{4}$">

                                                                <label>Telephone</label>
                                                                <span></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--                                                        <div class="col-md-6 col-sm-6 clear-b">
                                                                                                                <div class="input-wrapper full-width">
                                                                                                                    <div class="styled-input">
                                                    
                                                                                                                        <input class="form-control" type="text" name="mobile"  id="reipient_mobile" data-parsley-required="true" data-parsley-required-message="Mobile number is required" data-parsley-trigger="change focusout">
                                                                                                                        <label>Mobile Number</label>
                                                                                                                        <span></span>
                                                                                                                    </div>
                                                                                                                </div>
                                                                                                            </div>-->
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="input-wrapper full-width">
                                                            <div class="styled-input">

                                                                <input class="form-control email_validation" type="text" name="email" id="reipient_email"> 
                                                                <label>Email</label>
                                                                <span></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 col-sm-12">
                                                        <div class="input-wrapper full-width">
                                                            <div class="styled-input">

                                                                <textarea class="form-control"  type="text" name="address" id="reipient_address"  data-parsley-required="true" data-parsley-required-message="Address is required" data-parsley-trigger="change focusout"></textarea>

                                                                <label>Address<span class="mandatory-field">*</span></label>
                                                                <span></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 clear-b">
                                                            <div class="input-wrapper full-width">
                                                                <div class="styled-input">
                                                                    <label class="lableall">Zip Code<span class="mandatory-field">*</span></label> 
                                                                    <input class="form-control zip_validation reipient_zip" type="hidden" name="zip" id="reipient_zip" data-parsley-required="true" data-parsley-required-message="Zip code is required" data-parsley-trigger="change focusout">
                                                                    <input class="form-control new_reipient_zip"  type="text" name="new_reipient_zip" value="{{old('new_reipient_zip')}}" id="new_addresscontact_zip">
                                                                    {{-- <label>Zip Code<span class="mandatory-field">*</span></label> --}}
                                                                    <span></span>
                                                                </div>
                                                            </div>
                                                            <div id="zipreciptloader" style="display: none">
                                                                <img width="150px" src="{{asset('images/loader.gif')}}"/>
                                                            </div>
                                                        </div>
                                                     <div class="col-md-6 col-sm-6">
                                                        <div class="select-opt select2-container select2class">
                                                        <label class="col-md-12 control-label" for="textinput">City<span class="mandatory-field">*</span></label> 
                                                            <select class="select2-hidden-accessible load_ajax_cities addresscontact_city_id"  name="city_id" id="recipient_city"  style="width: 200px;" data-parsley-required="true" data-parsley-required-message="City is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true">
                                                                <option value="">-Select-</option>    

                                                            </select>
                                                        </div>
                                                    </div>

                                                   <!--  <div class="col-md-6 col-sm-6">

                                                        <div class="input-wrapper full-width">
                                                            <div class="styled-input">
                                                                <input class="form-control load_ajax_cities"  type="text" name="city_id" id="recipient_city" data-parsley-required="true" data-parsley-required-message="Select City from the list" data-parsley-trigger="change focusout">
                                                                <input type="hidden" name="recipient_city_id" id="recipient_city_id"/>
                                                                <label class="col-md-12 control-label" for="textinput">City<span class="mandatory-field">*</span></label> 
                                                                <span></span>                                                                
                                                            </div>
                                                            <div id="recipientloader" style="display: none">
                                                                <img width="150px" src="{{asset('images/loader.gif')}}"/>
                                                            </div>
                                                            <p class="help-block">
                                         

                                                                <strong id="city-error"></strong>
                                                            </p>
                                                        </div>
                                                    </div>
 -->
                                                    <div class="col-md-6 col-sm-6 clear-b">
                                                        <div class="select-opt">
                                                            <label class="col-md-12 control-label" for="textinput">State* </label>  
                                                            <select class="selectpicker" id="recipient_state" name="state_id" data-parsley-required="true" data-parsley-required-message="State is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true">
                                                                <option value="">Select</option>
                                                                @foreach($states as $state)
                                                                <option value="{{$state->id}}">{{$state->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                        

                                                    <!--                                                    <div class="col-md-6 col-sm-6 ">
                                                                                                            <div class="select-opt">
                                                                                                                <label class="col-md-12 control-label" for="textinput">County</label>  
                                                    
                                                    
                                                                                                                <select class="selectpicker"name="country_id" id="recipient_county" data-show-subtext="true" data-live-search="true">
                                                                                                                    <option value="">Select</option>
                                                                                                                    @foreach($countries as $county)
                                                                                                                    <option value="{{$county->id}}">{{$county->name}}</option>
                                                                                                                    @endforeach
                                                                                                                </select>
                                                                                                            </div>
                                                    
                                                                                                        </div>-->




                                                    <div class="col-md-12">
                                                        <div class="form-bottom-btn pd-25">
                                                            <button  type="submit" name="continue"   class="btn btn-primary custom-btn customc-btn add_recipient" value="add"><span>Add Recipient</span></button>
                                                            <button  type="submit" name="continue"  class="btn btn-primary custom-btn customc-btn update_recipient" value="update" disabled=""><span>Update Recipient</span></button>

                                                        </div>
                                                    </div>
                                                    </form>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-bottom-btn">
                                                            <!--<input type="button" id="editBack" name="editBack" class="btn btn-primary custom-btn customb-btn" value="Back"/>-->
                                                            <a type="button" id="editBack" name="editBack" class="btn btn-primary custom-btn customb-btn not-empty" value="Back"><span>back</span></a>

                                                            <a href="{{url('home')}}" class="btn btn-primary custom-btn customb-btn"><span>Cancel</span></a>

                                                            <a href="{{url('customer/work-order/updateStatus/'.$id.'/'.'0')}}" class="btn btn-primary custom-btn customb-btn" name="submit" value="save_for_later"><span>Save for Later</span></a>

                                                            @if($status=='0')
                                                            <a  href="{{url('customer/work-order/updateStatus/'.$id.'/'.'1')}}"  class="btn btn-primary custom-btn customc-btn" name="submit" value="submit" title="Please fill all mandetory project information" disabled="" style="pointer-events: none"><span>Submit</span></a>

                                                            @elseif($status=='1')
                                                            <a  href="{{url('customer/work-order/updateStatus/'.$id.'/'.'1')}}"  class="btn btn-primary custom-btn customc-btn" name="submit" value="submit" ><span>Submit</span></a>
                                                            @endif                                                       
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>



                                </div>
                                <!-- add new contacts-->

                               <div id="contactModal" class="modal fade register-modal addcon-modal">
    
    
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <div id="success-msg"class="alert alert-success no-margin" style="display: none"></div>
                                                    <div id="error-msg"class="alert alert-error no-margin" style="display: none"></div>
                                                    <h4 class="modal-title">Add/Update Address Book</h4>   
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                </div>
                                                <div class="modal-body">
                                                    <form method="POST" id="add_recipient_contact">
                                                        {{csrf_field()}}
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="input-wrapper full-width">
                                                                <div class="styled-input">
    
                                                                    <input class="form-control"  type="text" name="contact_name" value="{{old('contact_name')}}" id="contact_name">
                                                                    <input type="hidden" name="contact_id" value="" id="contact_id">
                                                                    <label>Name<span class="mandatory-field">*</span></label>
                                                                    <span></span>
                                                                    <p class="help-block">
                                                                        <strong id="name-error"></strong>
                                                                        </span>  
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-xs-12 ">
                                                            <div class="input-wrapper full-width">
                                                                <label class="col-md-12 control-label" for="textarea">Attn/Additional Recipient Name<span class="mandatory-field"></span></label>
                                                                <div class="styled-input">
                                                                    <textarea class="form-control attn recipient_attns addressattn"   name="attn" value="{{old('attn')}}" id="attn" ></textarea>
                                                                    <p class="help-block">
                                                                        <strong id="attn-error"></strong>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="input-wrapper full-width">
                                                                <div class="styled-input">
                                                                    <label class="lableall">Telephone</label>
                                                                    <input class="form-control business_phone_validation addresscontact_no"  type="text" name="contact_no" value="{{old('contact_no')}}" id="contact_no">
                                                                    <span></span>
                                                                    <p class="help-block">
                                                                        <strong id="contact-error"></strong>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <div class="input-wrapper full-width">
                                                                <div class="styled-input">
                                                                    <label class="lableall">Email</label>
                                                                    <input class="form-control email_validation addresscontact_email"  type="email" name="contact_email" value="{{old('contact_email')}}" id="contact_email">
                                                                    <span></span>
                                                                    <p class="help-block">
                                                                        <strong id="email-error"></strong>
                                                                        </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                            <div class="input-wrapper full-width">
                                                                <div class="styled-input"> 
                                                                    <label class="lableall">Address<span class="mandatory-field">*</span></label>
                                                                    <textarea class="form-control addresscontact_address"  name="contact_address"  id="contact_address">{{old('contact_address')}}</textarea>
                                                                    <span></span>
                                                                    <p class="help-block">
                                                                        <strong id="address-error"></strong>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="input-wrapper full-width">
                                                                <div class="styled-input">
                                                                    <label class="lableall">Zip Code<span class="mandatory-field">*</span></label>
                                                                    <input class="form-control"  type="hidden" name="selected_address_book" value="" id="selected_address_book">
                                                                    <input class="form-control new_addresscontact_zip"  type="text" name="new_addresscontact_zip" value="{{old('new_addresscontact_zip')}}" id="new_addresscontact_zip">
                                                                    <input class="form-control addresscontact_zip"  type="hidden" name="contact_zip" value="{{old('contact_zip')}}" id="contact_zip">
                                                                    <!-- <input type="text" id='employeeid' readonly> -->
                                                                    <input class="contact_zipurl"  type="hidden" name="contact_zipurl" id="contact_zipurl" value="{{ url('admin/ZipCodeLookups') }}">
                                                                    <input class="contact_stateurl"  type="hidden" name="contact_stateurl" id="contact_stateurl" value="{{ url('admin/getStateName') }}">
                                                                    <span></span>
                                                                    <p class="help-block" >
                                                                        <strong id="zip-error"></strong>
                                                                    </p>
                                                                </div>
                                                                <div id="zippopuploader" style="display: none">
                                                                    <img width="150px" src="{{asset('images/loader.gif')}}"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="select-opt select2-container select2class">
                                                            <label class="col-md-12 control-label lableall" for="textinput">City<span class="mandatory-field">*</span></label> 
                                                                <select class="select2-hidden-accessible load_ajax_cities addresscontact_city_id"  name="contact_city_id" id="contact_city_id"  style="width: 200px;">
                                                                    <option value="">-Select-</option>    
    
                                                                </select>
                                                                    <p class="help-block"
                                                                style="margin:0">
                                                <strong id="city-error"></strong>
                                            </p>
                                                            </div>
                                                        </div>
                                                       <!--  <div class="col-md-6 col-sm-6 col-xs-12">
                                                            <div class="input-wrapper full-width">
                                                                <div class="styled-input">
                                                                    <input class="form-control"  type="text" name="contact_city_id" id="contact_city_id" data-parsley-required="true" data-parsley-required-message="City is required" data-parsley-trigger="change focusout">
                                                                    <input type="hidden" name="autocomplete_city_id" id="autocomplete_city_id"/>
                                                                    <label class="col-md-12 control-label" for="textinput">City<span class="mandatory-field">*</span></label> 
                                                                    <span></span>
    
                                                                    <p class="help-block">
                                                                        <strong id="city-error"></strong>
                                                                    </p>
                                                                </div>
                                                                <div id="recipientpopuploader" style="display: none">
                                                                    <img width="150px" src="{{asset('images/loader.gif')}}"/>
                                                                </div>
                                                                </select>
    
                                                            </div>
                                                        </div> -->
                                                        <div class="col-md-6 col-sm-6 col-xs-12 clear-b">
                                                            <div class="select-opt">
                                                                <label class="col-md-12 control-label lableall">State<span class="mandatory-field">*</span></label>
                                                                <select class="selectpicker form-control addresscontact_state_id"  name="contact_state_id" data-parsley-required="true" data-parsley-required-message="State is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true" id="contact_state_id">
                                                                    <option value="">Select</option>
                                                                    @foreach($states as $state)
                                                                    <option value="{{$state->id}}" @if(old('contact_state_id')) selected="selected" @endif>{{$state->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                                <p class="help-block">
                                                                    <strong id="state-error"></strong>
                                                                </p>
                                                            </div>
                                                        </div>
                                                   
                                                        <!--                                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                                                                                                <div class="select-opt">
                                                                                                                    <label class="col-md-12 control-label">Country</label>
                                                                                                                    <select class="selectpicker" name="contact_country" data-show-subtext="true" data-live-search="true" id="contact_country">
                                                                                                                        <option value="">Select</option>
                                                                                                                        @foreach($countries as $country)
                                                                                                                        <option value="{{$country->id}}" @if(old('contact_country')) selected="selected" @endif>{{$country->name}}</option>
                                                                                                                        @endforeach
                                                                                                                    </select>
                                                                                                                    <p class="help-block">
                                                                                                                        <strong id="country-error"></strong>
                                                                                                                    </p>
                                                                                                                </div>
                                                                                                            </div>-->
                                                    </form>
                                                </div>
    
                                                <div class="modal-footer full-width">
                                                    <div class="form-bottom-btn">
                                                        <input type="hidden" name="edit_secondary_doc_id" value="{{$id}}" id="doc_id_recipient"/>
                                                        <button type="submit" class="btn btn-primary custom-btn customc-btn" id="submit_contact"><span>Add/Update Address Book</span></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>  
    
                                    </div>
                                </div>
                            </div>
                        </div>
    
                        </section>
                    </div>
                <form action="" method="POST" class="remove-attachment-model" id="remove-attachment-model">
                    {!! csrf_field() !!}
                    <div id="attachment-confirmation-modal" class="modal fade">
                        <div class="modal-dialog modal-confirm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div class="icon-box">
                                        <i class="fa fa-close"></i>
                                    </div>              
                                    <h4 class="modal-title">Are you sure?</h4>  
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                </div>
                                <div class="modal-body">
                                    <p>Do you really want to delete these records? This process cannot be undone.</p>
                                </div>
                                <div class="modal-footer">
                                    <div class="form-bottom-btn">
                                        <button type="button" class="btn btn-primary custom-btn customb-btn remove-attachement-from-delete-form" data-dismiss="modal"><span>Cancel</span></button>
                                        <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>Confirm</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <form action="" method="GET" class="remove-record-model">
                    {!! csrf_field() !!}
                    <div id="contact-confirmation-modal" class="modal fade">
                        <div class="modal-dialog modal-confirm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div class="icon-box">
                                        <i class="fa fa-close"></i>
                                    </div>              
                                    <h4 class="modal-title">Are you sure?</h4>  
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                </div>
                                <div class="modal-body">
                                    <input type="text" name="contact_id" id="contact_id" hidden="">              
                                    <p>Do you really want to delete these records? This process cannot be undone.</p>
                                </div>
                                <div class="modal-footer">
                                    <div class="form-bottom-btn">
                                        <button type="button" class="btn btn-primary custom-btn customb-btn remove-data-from-delete-form" data-dismiss="modal"><span>Cancel</span></button>
                                        <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>Confirm</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                @stop

                @section('adminlte_js')
                <script type="text/javascript" src="{{ asset('js/jquery-ui.min.js') }}"></script>
                <script src="{{ asset('vendor/adminlte/plugins/datepicker/bootstrap-datepicker.js') }}" type="text/javascript"></script>

<script type="text/javascript">
    //autocomplete cit
var cityUrl = "{{ url('customer/city-list') }}";
//$('input,select,textarea').attr('autocomplete','nope');

     $(document).ready(function () {  
        $('input,textarea,select').on('focus', function(e) {
            e.preventDefault();
            $(this).attr("autocomplete", "nope");  
        });
        //     });


        /********* receipient_city  & contact_city_id*******/
             $(".load_ajax_cities").select2({
                tags:true,
              ajax: { 
               url: cityUrl,
               type: "get",
               dataType: 'json',
               //delay: 250,
               data: function (params) {
                return {
                  name: params.term // search term
                };
               },
               processResults: function (response) {
                 return {
                    results: response
                 };
               },
               cache: true
              }
             });


        });
      
    //Start to submit note
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
//back to project tab
$('#editBack').click(function () {
    $('.display-ib li').removeClass('active');
    $('.display-ib li:first-child').addClass('active');
//    $('#project_tab').addClass('active');
    $('.notice-tabs .tab-pane').removeClass('active');
    $('#project').addClass('active');
});
//Start to submit note and correction
$('.notice-btn').click(function (e) {

    $('input[name=note]').val('');
    $("#note_email option").prop("selected", false);
    $('#note_email').selectpicker('refresh');
    $('.note-success').empty();
    $('.note-success').css('display', 'none');
    $('.note-error').empty();
    $('.note-error').css('display', 'none');
});

$('.corr-btn').click(function (e) {

    $('input[name=correction]').val('');
    $("#correction_email option").prop("selected", false);
    $('#correction_email').selectpicker('refresh');
    $('.correction-success').empty();
    $('.correction-success').css('display', 'none');
    $('.correction-error').empty();
    $('.correction-error').css('display', 'none');
});
$(".submitNote").click(function (e) {
    e.preventDefault();
    $('.note-success').hide();
    $('.note-error').hide();
    var note = $("input[name=note]").val();
    var email = $("#note_email").val();
    var work_order_id = $('#work_order').val();
    $.ajax({
        type: 'POST',
        url: '{{url("customer/work-order/store_note")}}',
        data: $('#edit_work_order_note').serialize(),
        beforeSend: function () {
            $('#pleaseWait').modal('show');
        },
        complete: function () {
            $('#pleaseWait').modal('hide');
        },
        success: function (data) {
            console.log(data);
            if (data.success) {
                var d = new Date();
                var strDate = (d.getMonth() + 1) + "-" + d.getDate() + "-" + d.getFullYear();

                $('.note-error').empty();
                $('.note-success').empty();
                $("input[name=note]").val('');
                $("#note_email option").prop("selected", false);
                $('#note_email').selectpicker('refresh');
                $('.note-error').css('display', 'none');
                $('.note-success').show();
                $('.note-success').append('<p>' + data.success + '</p>');
                // $('.notes_div').append('<li>' + data.notes.note + '<br>' + data.notes.email + '</li>');

                $('.notes_div').prepend('<li>' + data.notes.note + '</br>Added by:' + data.notes.name + '</br>Created at:' + strDate + '</li>');



            } else if (data.errors) {
                $('.note-error').empty();
                $.each(data.errors, function (key, value) {
                    $('.note-success').empty();
                    $('.note-success').css('display', 'none');
                    $('.note-error').show();
                    $('.note-error').append('<p>' + value + '</p>');
                });
            }
        }
    });
});
$(".submitCorrection").click(function (e) {
    e.preventDefault();
    $('.correction-success').hide();
    $('.correction-error').hide();
    $.ajax({
        type: 'POST',
        url: '{{url("customer/work-order/store_correction")}}',
        data: $('#edit_work_order_correction').serialize(),
        beforeSend: function () {
            $('#pleaseWait').modal('show');
        },
        complete: function () {
            $('#pleaseWait').modal('hide');
        },
        success: function (data) {
            if (data.success) {
                var d = new Date();
                var strDate = (d.getMonth() + 1) + "-" + d.getDate() + "-" + d.getFullYear();
                $('.correction-error').empty();
                $('.correction-success').empty();
                $("input[name=correction]").val('');
                $("#correction_email option").prop("selected", false);
                $('#correction_email').selectpicker('refresh');
                $('.correction-error').css('display', 'none');
                $('.correction-success').show();

                $('.correction-success').append('<p>' + data.success + '</p>');
//                $('.correction_div').append('<li>' + data.corrections.correction + '<br>' + data.corrections.email + '</li>');

                $('.correction_div').prepend('<li>' + data.corrections.correction + '</br>Added by:' + data.corrections.name + '</br>Created at:' + strDate + '</li>');



            } else if (data.errors) {
                $('.correction-error').empty();
                $.each(data.errors, function (key, value) {
                    $('.correction-success').empty();
                    $('.correction-success').css('display', 'none');
                    $('.correction-error').show();
                    $('.correction-error').append('<p>' + value + '</p>');
                });
            }
        }
    });
});

//End to submit note and correction
//Start to fetch data of related work order

$('#soft_doc_parent_work_order').on('change', function () {
     $(".delete_parent_attachment").remove();
    $.ajax({
        data: {'work_order_no': $('#soft_doc_parent_work_order').val()},
        type: 'post',
        url: "{{url('customer/work-order-document/work_order_details')}}",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            //console.log(response);
            if (response.success) {
                var stateid = "";
                $.each(response.success, function (key, val) {
                    if (val.name == 'state') {
                        $('#soft_doc_state_value').val(val.value);
                        $("#soft_doc_state_value").selectpicker("refresh");


                    } else if (val.name == 'city') {
                        $('#soft_doc_city_value').val(val.value);
                        $("#soft_doc_city_value").selectpicker("refresh");

                    } else if (val.name == 'county') {

                        $('#recipient_county_id').val(val.value);
                        $("#county").val(val.county_name);
                    } else if (val.name == 'project_type') {

                        $('#project_type').val(val.value);
                        $("#project_type").selectpicker("refresh");

                    } else if (val.name == 'your_role') {

                        $('#your_role').val(val.value);
                        $("#your_role").selectpicker("refresh");

                    } else if (val.name == 'contracted_by') {
                        $("#contracted_by_select").val(val.value).trigger('change');
                        //    $('#contracted_by_select').val(val.value);
                        //    $("#contracted_by_select").selectpicker("refresh");

                    } else if (val.name == 'job_start_date') {
                        $('#job_start_date').val(val.value);
                        $('#enter_into_agreement').val(val.value);
                    } else if (val.name == 'last_date_on_the_job') {
                        $('#last_date_of_labor_service_furnished').val(val.value);
                        $('#last_date_on_the_job').val(val.value);

                    } else {
                        var field_name = val.name;
                        var vars = val.value;
                        let result = vars.replace(/<br \/>/gi, "<p>");
                        $("#" + field_name).val(result);
                    }



                });
            }
            if (response.parent_attachment) {
                //  $('#edit_document_table tbody').empty();
                // console.log(response.parent_attachment);
                $.each(response.parent_attachment, function (k, v) {

                    var f1 = v.db_arr[2];
                    var f2 = f1.split('.')[0];
                    /*var row = '<tr id="refreshdelete' + f2 + '">';*/
                    var row = '<tr class="delete_parent_attachment" id="refreshdelete' + v.db_arr[4] + '">';
                    $.each((v.ajax_arr), function (i, d) {
                        $('.norecords').closest('tr').remove();
                        var display_url = '{{asset("attachment/work_order_document/parent_document")}}/' + v.db_arr[2];
                        /*if (i == 2) {
                            row += '<td><a target="_blank" href="' + display_url + '">' + d + '</a></td>';
                        } else {*/
                            row += '<td>' + d + '</td>';
                        //}
                    });
                    var display_url = '{{asset("attachment/work_order_document/parent_document")}}/' + v.db_arr[2];
                    var attachment_url = '{{url("customer/work-order-document/parent_document/removeattachment")}}';
                    /*row += "<td><a class='btn btn-secondary item red-tooltip remove-parent-attachment' data-toggle='modal' data-url='" + attachment_url + "'  data-file='" + v.db_arr[2] + "' data-target='#attachment-confirmation-modal' data-placement='bottom' title='Remove'><span class='icon-cross-remove-sign'></span></a></td>";*/
                    if(v.db_arr[2].length>0){
                        row += "<td><a target='_blank' href='" + display_url + "' class='btn btn-secondary item red-tooltip'><i class='fa fa-eye'></i></a></td>";
                            }else{
                                row += "<td></td>";
                            }
                        row += '</tr>';
                    $('#edit_document_table tbody').append(row);

                });

            }
            $('input').each(function () {
                if ($(this).val().length > 0) {
                    $(this).addClass('not-empty');
                } else {
                    $(this).removeClass('not-empty');
                }
            });
            $('textarea').each(function () {
                if ($(this).val().length > 0) {
                    $(this).addClass('not-empty');
                } else {
                    $(this).removeClass('not-empty');
                }
            });

        }
    });
});
//End to fetch data of related work order
////Start of fetch cities according to state for on load recipient
$('#state_id').on('change', function () {
    $('#city_id').find('option').remove().end().append('<option value="">Select</option>');

    $.ajax({
        data: {'state': $(this).val()},
        type: "post",
        url: "{{url('cities') }}",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {

            $.each(response, function (e, val) {
                $("#city_id").append("<option value='" + val.id + "'>" + val.name + "</option>");
                $("#city_id").selectpicker("refresh");
            });

        }
    });
});
//End of fetch cities according to state
//End to submit note
$(document).ready(function () {
    var tomorrow = new Date(Date.now() + 24 * 60 * 60 * 1000);
   // $('#job_start_date').attr('readonly', 'true');
    //$('#last_date_on_the_job').attr('readonly', 'true');


    $("#job_start_date").datepicker({
        todayBtn: 1,
        autoclose: true,
    }).on('changeDate', function (selected) {
        var minDate = new Date(selected.date.valueOf());
        $('#last_date_on_the_job').datepicker('setStartDate', minDate);
    });

    $("#last_date_on_the_job").datepicker()
            .on('changeDate', function (selected) {
                var maxDate = new Date(selected.date.valueOf());
                $('#job_start_date').datepicker('setEndDate', maxDate);
            });
    /*************notes and correction*******************/
    $(".corr-btn").click(function () {
        $(".corr-block").css("width", "415px");
    });
    $(".closebtn").click(function () {
        $(".corr-block").css("width", "0");
    });
    $(".notice-btn").click(function () {
        $(".notes-block").css("width", "415px");
    });
    $(".closebtn1").click(function () {
        $(".notes-block").css("width", "0");
    });
    /*************notes and correction*******************/
    $('[data-toggle="tooltip"]').tooltip({
        trigger: 'click',
        placement: 'bottom'
    });
    //save for later
    $(".save_for_later").on('click', function () {
        $('#create_work_order').parsley().destroy();
    });
    $('textarea').each(function () {
        if ($(this).val().length > 0) {
            $(this).addClass('not-empty');
        } else {
            $(this).removeClass('not-empty');
        }
    });
    $('textarea').keyup(function () {
        if ($(this).val().length > 0) {
            $(this).addClass('not-empty');
        } else {
            $(this).removeClass('not-empty');
        }
    });

    $('#collection_for_attorneys_fees').hide();
    //collection_for_attorneys_fees
    $('#attorneys_fees').click(function () {

        $('#collection_for_attorneys_fees').toggle();
    })
//Start to fetch parent work order number of customers
    $('#soft_doc_parent_work_order').select2({
            minimumInputLength: 1,
            ajax: {
                url : "{{ url('customer/secondary-document/autocomplete_V1?work_order_id='.$id) }}",
                dataType: 'json',
                delay: 250,
                type: "get",
                processResults: function (data) {
                return {
                    results:  $.map(data, function (item) {
                        return {
                            text: item,
                            id: item
                        }
                    })
                };
                },
                cache: true
            }
        });

    /*$('#soft_doc_parent_work_order').find('option').remove().end().append('<option value="">Select</option>');

    $.ajax({
        type: "get",
        url: "{{url('customer/secondary-document/autocomplete')}}",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {

            //  console.log(response);

            $.each(response, function (e, val) {

                $("#soft_doc_parent_work_order").append("<option value='" + val.id + "'>#" + val.id + "</option>");
                $("#soft_doc_parent_work_order").selectpicker("refresh");
            });
            if ($('#parent_work_order_id').val()) {
                $("#soft_doc_parent_work_order").val($('#parent_work_order_id').val());
                $("#soft_doc_parent_work_order").selectpicker("refresh");
            }

        }
    });*/
//End to fetch parent work order number of customerss
    $('.datepicker').datepicker({
        autoclose: true
    });

    $('#recipient_name').on('change', function () {
        $('input[name=contact_name]').val('');
        $('input[name=contact_no]').val('');
        $('input[name=contact_email]').val('');
        $('#contact_address').val('');
        $('#attn').val('');
        $('input[ name=contact_zip]').val('');
        $("#contact_state_id").val('');
        $("#contact_state_id").selectpicker('refresh');
        $("#contact_city_id").val('').trigger('change');
        $('#autocomplete_city_id').val('');
        $('.new_reipient_zip').val('');
        var name = $('#recipient_name').val();
        var recipt_id = $(this).find(':selected').attr('data-id');
        $('#recipt_id').val(recipt_id);
        $.ajax({
            data: {'name': name, 'recipt_id': recipt_id},
            type: "post",
            url: "{{ url('customer/get-contacts') }}",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (res) {

                //   $('#reipient_name').val(res.company_name);
                $('#reipient_email').val(res.email);
                $('#reipient_mobile').val(res.mobile);
                $('#reipient_contact').val(res.phone);
                $('#reipient_zip').val(res.zip);
                $('#reipient_address').val(res.mailing_address);
                $('#reipient_fax').val(res.fax);
                $("#recipient_state").val(res.state_id);
                $("#recipient_state").selectpicker("refresh");
                var urlzipcode = $('#contact_zipurl').val();
                        $.ajax({
                            url: urlzipcode,
                            type: 'post',
                            dataType: "json",
                            data: {
                                zipcode: res.zip,
                                cityName:res.city_id
                            },
                            success: function(data) {
                                $(".new_reipient_zip").val(data[0].label);
                            }
                        });
                 if(res.city_id==""){
                    $("#recipient_city").val(res.city_id);
                    $("#recipient_city").trigger('change');
                   }else{
                    var option = new Option(res.city_name, res.city_id, true, true);
                    $("#recipient_city").append(option).trigger('change');
                   }
                /*$('#recipient_city').val(res.city_name);
                $('#recipient_city_id').val(res.city_id);*/
                $('#recipient_attn').val(res.attn);
//                      $('#recipient_city_id').attr("placeholder", res.city_id);
                //$("#recipient_city").selectpicker("refresh");
//                    $('#recipient_county').val(value.country_id);
//                    $("#recipient_county").selectpicker("refresh");


                $('input,textarea').each(function () {
                    if ($(this).val().length > 0) {
                        $(this).addClass('not-empty');
                    } else {
                        $(this).removeClass('not-empty');
                    }
                });

            },
        });
    });



//Start Document Functionalty
    $
    $('.soft_notice_work_order_doc_field').hide();
    $('.soft_notice_work_order_doc_file').hide();
    $('#soft_notice_work_order_doc_type').change(function () {
        var selected_doc_type = $(this).val();
        $('#soft_notice_work_order_doc_input').val("");
        if (selected_doc_type == 1) {
            $('.soft_notice_work_order_doc_field').show();
            $('.soft_notice_work_order_doc_file').show();
            $('#soft_notice_work_order_doc_field').text('Bond Number');
        } else if (selected_doc_type == 2) {
            $('.soft_notice_work_order_doc_field').show();
            $('.soft_notice_work_order_doc_file').show();
            $('#soft_notice_work_order_doc_field').text('NOC Book/Page');
        } else if (selected_doc_type == 3) {
            $('.soft_notice_work_order_doc_field').show();
            $('.soft_notice_work_order_doc_file').show();
            $('#soft_notice_work_order_doc_field').text('Permit No');
        } else if (selected_doc_type == 5) {
            $('.soft_notice_work_order_doc_field').show();
            $('.soft_notice_work_order_doc_file').show();
            $('#soft_notice_work_order_doc_field').text('Invoice No.');
        } else if (selected_doc_type == 4 || selected_doc_type == 6 ||
                selected_doc_type == 7 || selected_doc_type == 8
                || selected_doc_type == 9 || selected_doc_type == 13) {
            $('.soft_notice_work_order_doc_field').show();
            $('.soft_notice_work_order_doc_file').show();
            $('#soft_notice_work_order_doc_field').text('Description');
        } else if (selected_doc_type == 10) {
            $('.soft_notice_work_order_doc_field').show();
            $('.soft_notice_work_order_doc_file').show();
            $('#soft_notice_work_order_doc_field').text('Folio No.');
        } else if (selected_doc_type == 11) {
            $('.soft_notice_work_order_doc_field').show();
            $('.soft_notice_work_order_doc_file').show();
            $('#soft_notice_work_order_doc_field').text('COL Signed.');
        } else if (selected_doc_type == 12) {
            $('.soft_notice_work_order_doc_field').show();
            $('.soft_notice_work_order_doc_file').show();
            $('#soft_notice_work_order_doc_field').text('Recorded COL/SCOL.');
        } else if (!selected_doc_type) {
            $('.soft_notice_work_order_doc_field').hide();
            $('.soft_notice_work_order_doc_file').hide();
        }

    });
    var result_arr = [];
    $("#submit_document").on("click", function (e) {
        if($('.update_doc').html()=='Add'){
        e.preventDefault();
        var extension = $('#soft_notice_work_order_result_file').val().split('.').pop().toLowerCase();

        if ($('#soft_notice_work_order_result_file').val() != "" && $.inArray(extension, ['csv', 'xls', 'xlsx', 'pdf', 'doc', 'docx', 'jpg', 'jpeg']) == -1) {
            $('#errormessage').html('Please Select Valid File... ');
            $('#errormessage').css('display', 'block');
        } else {
            $('#errormessage').html('');
            var file_data = $('#soft_notice_work_order_result_file').prop('files')[0];
            var selected_document_type = $('#soft_notice_work_order_doc_type').val();
            var doc_input = $('#soft_notice_work_order_doc_input').val();
            var form_data = new FormData();

            form_data.append('file', file_data);
            form_data.append('document_type', selected_document_type);
            form_data.append('doc_value', doc_input)
            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': $('meta[name=_token]').attr('content')
                }
            });
            $.ajax({
                url: "{{url('customer/work-order/attachments')}}", // point to server-side PHP script
                data: form_data,
                type: 'POST',
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                processData: false,
                success: function (response) {
                    var data = jQuery.parseJSON(response);
                    var ran_no = Math.random() * (100 - 1) + 1;
                    ran_no = String(ran_no).replace('.','_');
                    data.db_arr.push('refreshdelete'+ran_no);
                    result_arr.push(data.db_arr);
                    result_arr.push('|');

                    $('#edit_soft_notices_attachment').val(result_arr);


                    var f1 = data.db_arr[2];
                    var f2 = f1.split('.')[0];
                   
                    var row = '<tr id="refreshdelete' + ran_no + '">';
                    $.each((data.ajax_arr), function (i, d) {
                        $('.norecords').closest('tr').remove();
                        var display_url = '{{asset("attachment/work_order_document")}}/' + data.db_arr[2];
                       /* if (i == 1) {
                            row += '<td><a target="_blank" href="' + display_url + '">' + d + '</a></td>';
                        } else {*/
                            row += '<td>' + d + '</td>';
                        //}
                         //hide fields after add
                        $('.soft_notice_work_order_doc_field').hide();
                        $('.soft_notice_work_order_doc_file').hide();
                        // $('.doc_visibility').hide();
                        $('#soft_notice_work_order_doc_type').val('');
                        $('#soft_notice_work_order_doc_type').selectpicker('refresh');
                        $('#soft_notice_work_order_result_file').val('');
                        $('#edit_document_name').val('');
                        $('#edit_document_name').attr('data-file-original-name','');
                        $('#edit_document_name').attr('data-file-extension','');
                        $('#soft_notice_work_order_doc_input').val('');
                        $('#remove_file').val('0');
                        $('#preview').css('display','none');
                        $('#remove_icon').css('display','none');
                    });
                    var display_url = '{{asset("attachment/work_order_document")}}/' + data.db_arr[2];
                    var attachment_url = '{{url("customer/work-order/removeattachment")}}';
                    row += "<td><a class='btn btn-secondary item red-tooltip remove-attachment' data-toggle='modal' data-url='" + attachment_url + "'  data-file='" + data.db_arr[2] + "' data-target='#attachment-confirmation-modal' data-placement='bottom' title='Remove'><span class='icon-cross-remove-sign'></span></a><a class='btn btn-secondary item red-tooltip edit-attachment' data-file='" + data.db_arr[2] + "' data-type='" + data.db_arr[0] + "'data-title='" + data.db_arr[1] +"'data-file-original-name='" + data.db_arr[3] + "'data-file-extension='" + data.ajax_arr[4]+"'><span class='icon-pencil-edit-button'></span></a>";
                    if(data.db_arr[2].length>0){
                                    row += "<a target='_blank' href='" + display_url + "' class='btn btn-secondary item red-tooltip'><i class='fa fa-eye'></i></a>";
                                    }
                                    row += '</td></tr>';
                    $('#edit_document_table tbody').append(row);
                }
            });
        }
    }else{
        e.preventDefault();
        var extension = $('#soft_notice_work_order_result_file').val().split('.').pop().toLowerCase();

        if ($('#soft_notice_work_order_result_file').val() != "" && $.inArray(extension, ['csv', 'xls', 'xlsx', 'pdf', 'doc', 'docx', 'jpg', 'jpeg']) == -1) {
            $('#errormessage').html('Please Select Valid File... ');
            $('#errormessage').css('display', 'block');
        } else {
            $('#errormessage').html('');
            var file_data = $('#soft_notice_work_order_result_file').prop('files')[0];
            var selected_document_type = $('#soft_notice_work_order_doc_type').val();
            var doc_input = $('#soft_notice_work_order_doc_input').val();
            var doc_id = $('#edit_doc_id').val();
            var doc_visibile = $("input[name='visibility']:checked").val();
            var work_order_id = $('#work_order').val();
            var remove_file = $('#remove_file').val();
            var edit_document_name = $('#edit_document_name').val();
            var edit_document_original_name = $('#edit_document_name').attr('data-file-original-name');
            var edit_document_extension = $('#edit_document_name').attr('data-file-extension');
            var row_id = $('#row_id').val();
            var result;
                //console.log(result_arr);
                for( var i = 0, len = result_arr.length; i < len; i++ ) {
                    // console.log(len);console.log(result_arr[i]);
                    for( var j = 0, len1 = result_arr[i].length; j < len1; j++ ){
                        //  console.log(result_arr[j]);
                        if( result_arr[i][j] === row_id ) {
                            result = i;
                            break;
                        }
                    }
                }
                result_arr.splice(result, 1);
                if(result_arr.length==1){
                    result_arr.splice(0, 1);
                }
                $('#edit_soft_notices_attachment').val(result_arr);
                //console.log(result_arr,'hi');
                console.log(result_arr);
                if(doc_id==''){
                    var doc_id = 0;
                }

            var form_data = new FormData();

             if(file_data)
            {
                form_data.append('file', file_data);
            }else{
                            form_data.append('file_name',edit_document_name);
                            form_data.append('file_original_name',edit_document_original_name);
                            form_data.append('extension',edit_document_extension);
            }
            form_data.append('document_type', selected_document_type);
            form_data.append('doc_value', doc_input);
            form_data.append('visibility', doc_visibile);
            form_data.append('doc_id', doc_id);
            form_data.append('work_order_id',work_order_id);
            form_data.append('remove_file',remove_file);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': $('meta[name=_token]').attr('content')
                }
            });
            $.ajax({
                url: "{{url('customer/work-order/edit/attachments')}}", // point to server-side PHP script
                data: form_data,
                type: 'POST',
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                processData: false,
                success: function (response) {
                    var data = jQuery.parseJSON(response);
                    //result_arr.push(data.db_arr);
                   // result_arr.push('|');
                    var del_var = $('#row_id').val();
                        //console.log(data.db_arr);
                    $("#" + del_var).remove();

                    //$('#edit_soft_notices_attachment').val(result_arr);
                    if(data.db_arr[2] != null){
                            var f1 = data.db_arr[2];//console.log(f1);
                            var f2 = f1.split('.')[0];

                        }

                        if(data.ajax_arr[2] == null){
                            data.db_arr[2] = '';
                        }


                    var f1 = data.db_arr[2];
                    var f2 = f1.split('.')[0];
                    var row = '<tr id="refreshdelete' + data.db_arr[4] + '">';
                    $.each((data.ajax_arr), function (i, d) {
                        $('.norecords').closest('tr').remove();
                        var display_url = '{{asset("attachment/work_order_document")}}/' + data.db_arr[2];
                       /* if (i == 1) {
                            row += '<td><a target="_blank" href="' + display_url + '">' + d + '</a></td>';
                        } else {*/
                             if(d==null){
                                        row += '<td>' + " " + '</td>';
                                        }else{
                                        row += '<td>' + d + '</td>';
                                    }
                        //}
                        $('.soft_notice_work_order_doc_field').hide();
                        $('.soft_notice_work_order_doc_file').hide();
                        $('#soft_notice_work_order_doc_type').val('');
                        $('#soft_notice_work_order_doc_type').selectpicker('refresh');
                            if($('#submit_document span').html()=="Update"){
                                $('#submit_document span').html('Add');
                            }
                            $('#soft_notice_work_order_result_file').val('');
                            $('#soft_notice_work_order_doc_input').val('');
                            $('#remove_file').val('0');
                            $('#preview').css('display','none');
                            $('#remove_icon').css('display','none');
                            $('#edit_document_name').val('');
                            $('#edit_document_name').attr('data-file-original-name','');
                            $('#edit_document_name').attr('data-file-extension','');
                    });
                    var display_url = '{{asset("attachment/work_order_document")}}/' + data.db_arr[2];
                    var attachment_url = '{{url("customer/edit/work-ordwer/removeattachment")}}';

                    row += "<td><a class='btn btn-secondary item red-tooltip remove-attachment' data-toggle='modal' data-url='" + attachment_url + "'  data-file='" + data.db_arr[2] + "'data-id='" + data.db_arr[4]+ "' data-target='#attachment-confirmation-modal' data-placement='bottom' title='Remove'><span class='icon-cross-remove-sign'></span></a><a class='btn btn-secondary item red-tooltip edit-attachment' data-file='" + data.db_arr[2] + "' data-type='" + data.db_arr[5] + "'data-title='" + data.db_arr[1] + "'data-id='" + data.db_arr[4]+"'data-file-original-name='" + data.db_arr[3] + "'data-file-extension='" + data.db_arr[6]+"'><span class='icon-pencil-edit-button'></span></a>";
                    if(data.db_arr[2].length>0){
                        row += "<a target='_blank' href='" + display_url + "' class='btn btn-secondary item red-tooltip'><i class='fa fa-eye'></i></a>";
                    }
                                row += '</td></tr>';

                    $('#edit_document_table tbody').append(row);
                }
            });
        }
    }



    });
//End Document Fucntionality


//End to fetch parent work order number of customerss
    //contact address dispaly
    $('.contracted_bys').on('change', function () {
        document.getElementById('hidden-panel').style.display = 'block';
            var name = $('.contracted_bys').val();
            var recipt_id = $(this).find(':selected').attr('data-id');
            $.ajax({
                data: {'name': name, 'recipt_id': recipt_id},
                type: "post",
                url: "{{ url('customer/get-contacts') }}",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (res) {   
                    //console.log(res); 
                    $('#addname').val(res.company_address);
                },
    
            });
      });
//Add recipient contact dynamic
    $('#add_contact,#add_contact_contracted_by').click(function () {
        var all_val = 1;
        $('#selected_address_book').val('');
        if ($(this).hasClass('contracted_by_recipient')) {
            var recipt_id ='';
			var name =$('#contracted_by_select').val();
			$.ajax({
					data: {'name': name, 'recipt_id': recipt_id},
					type: "post",
					url: "{{ url('customer/get-contacts') }}",
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					success: function (res) {   
					// console.log(res); 
						$('.addressattn').val(res.attn);
						$('.addresscontact_no').val(res.phone);
						$('.addresscontact_email').val(res.email);
						$('.addresscontact_address').val(res.company_address);
						$('.addresscontact_city_id')
							.empty() 
							.append($("<option/>") 
								.val(res.city_id) 
								.text(res.city_name))
							.val(res.city_id) 
							.trigger("change"); 
						$(".addresscontact_state_id").val(res.state_id).trigger('change');
						$('.addresscontact_zip').val(res.zip);
						$('#contact_id').val(res.id);
						var urlzipcode = $('#contact_zipurl').val();
                    $.ajax({
                        url: urlzipcode,
                        type: 'post',
                        dataType: "json",
                        data: {
                            zipcode: res.zip,
                            cityName:res.city_id
                        },
                        success: function(data) {
                            $(".new_addresscontact_zip").val(data[0].label);
                        }
                    });
                    $('#contact_id').val(res.id);
                }
    
            });
            $('input[name=contact_name]').val($('#contracted_by_select').val());
            $('input[name=contact_name]').addClass('not-empty');
            $('#selected_address_book').val('project');
        } else if ($('#recipient_name').val() != "") {
            var recipt_id="";
			var name =$('#recipient_name').val();
			var get_contact= "{{ url('customer/get-contacts') }}";
			$.ajax({
				data: {'name': name, 'recipt_id': recipt_id},
				type: "post",
				url:get_contact,
				headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				success: function(res) {
                var zipcode = $('#reipient_zip').val();
                var urlzipcode = $('#contact_zipurl').val();
                $.ajax({
                    url: urlzipcode,
                    type: 'post',
                    dataType: "json",
                    data: {
                        zipcode: zipcode,
                        cityName:res.city_id
                    },
                    success: function(data) {
                        $(".new_addresscontact_zip").val(data[0].label);
                    }
                });
                $('#contact_id').val(res.id);
            }
            });
			
            $('input[name=contact_name]').val($('#recipient_name').val());
            $('input[name=contact_no]').val($('#reipient_contact').val());
            $('input[name=contact_email]').val($('#reipient_email').val());
            $('#contact_address').val($('#reipient_address').val());
            $('input[name=contact_zip]').val($('#reipient_zip').val());
            $("#contact_state_id").val($('#recipient_state').val());
            $("#contact_state_id").selectpicker('refresh');
            $("#attn").val($('#recipient_attn').val());
            var city_val = $("#recipient_city").select2('data');
            //console.log(city_val);
             var option = new Option(city_val[0]['text'], city_val[0]['id'], true, true);
            $("#contact_city_id").append(option).trigger('change');
            //$("#contact_city_id").val($('#recipient_city').val());
            //$('#autocomplete_city_id').val($('#recipient_city_id').val());
            //$("#contact_city_id").selectpicker('refresh');

            $('input,textarea').addClass('not-empty');
            all_val = 0;
        } else {
            $('input[name=contact_name]').val('');
        }
        if (all_val) {
            $('input[name=contact_no]').val('');
            $('input[name=contact_email]').val('');
            $('#contact_address').val('');
            $('#attn').val('');
            $('input[ name=contact_zip]').val('');
            $("#contact_state_id").val('');
            $("#contact_state_id").selectpicker('refresh');
            //$("#contact_city_id").val('');
            $("#contact_city_id").val('').trigger('change')
            $('#autocomplete_city_id').val('');
        }
//        $("#contact_country").val('');
//        $("#contact_country").selectpicker('refresh');
        $('#name-error').html('');
        $('#address-error').html('');
        $('#city-error').html('');
        $('#state-error').html('');
        $('#zip-error').html('');
        $('#success-msg').css('display', 'none');

//        $('#contactModal').modal('show');
    });
    $(".business_phone_validation").mask("(999) 999-9999");
    $(".email_validation").attr("data-parsley-trigger", "change focusout")
            .attr("data-parsley-type", "email")
            .parsley();

    $(".zip_validation").attr("data-parsley-trigger", "change focusout")
            .attr("data-parsley-type", "digits")
            .attr("data-parsley-minlength", "5")
            .attr("data-parsley-maxlength", "5")
            .parsley();

    $('body').on('click', '#submit_contact', function () {
        var recipientForm = $('#add_recipient_contact');
//    var formData = recipientForm.serialize();

        $('#name-error').html("");
        $('#contact-error').html("");
        $('#email-error').html("");
        $('#address-error').html("");
        $('#city-error').html("");
        $('#state-error').html("");
        $('#zip-error').html("");
        //$('#country-error').html("");
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name=_token]').attr('content')
            }
        });
        $.ajax({
            url: '{{url("customer/contact_workorderrecipient/store")}}',
            type: 'POST',
            data: {
                doc_id: $('#doc_id_recipient').val(),
                contact_name: $('#contact_name').val(),
                contact_id: $('#contact_id').val(),
                attn: $('#attn').val(),
                contact_no: $('#contact_no').val(),
                contact_email: $('#contact_email').val(),
                contact_address: $('#contact_address').val(),
                contact_state_id: $('#contact_state_id').val(),
                contact_zip: $('#contact_zip').val(),
                contact_city_id: $('#contact_city_id').val(),
                // contact_country: $('#contact_country').val(),
            },
            success: function (data) {
                if (data.error_msg) {
                    $('#error-msg').css('display', 'block');
                    $('#error-msg').html(data.error_msg);
                    $('#success-msg').css('display', 'none');
                    $('#success-msg').html('');
                }
                if (data.errors) {
                    if (data.errors.contact_name) {
                        $('#name-error').html(data.errors.contact_name[0]);
                    }
                    if (data.errors.contact_address) {
                        $('#address-error').html(data.errors.contact_address[0]);
                    }
                    if (data.errors.contact_city_id) {
                        $('#city-error').html(data.errors.contact_city_id[0]);
                    }
                    if (data.errors.contact_state_id) {
                        $('#state-error').html(data.errors.contact_state_id[0]);
                    }
//                    if (data.errors.contact_country) {
//                        $('#country-error').html(data.errors.contact_country[0]);
//                    }
                    if (data.errors.contact_zip) {
                        $('#zip-error').html(data.errors.contact_zip[0]);
                    }
                    if (data.errors.contact_no) {
                        $('#contact-error').html(data.errors.contact_no[0]);
                    }
                    $('#error-msg').css('display', 'none');
                    $('#error-msg').html(data.errors);
                    $('#success-msg').css('display', 'none');
                    $('#success-msg').html('');
                }
                if (data.success) {
                    // console.log(data.contact_data)
                    $('#success-msg').css('display', 'block');
                    $('#success-msg').html(data.success);
                    $('#error-msg').css('display', 'none');
                    $('#error-msg').html('');
                    // console.log($('#doc_id_recipient').val());
                    $.ajax({
                        data: {"doc_id": $('#doc_id_recipient').val()},
                        type: 'POST',

                        url: "{{url('customer/getAllWorkOrderContacts')}}", // point to server-side PHP script
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },

                        success: function (response) {
                            var selected_address_book = $("#selected_address_book").val();
                            var recipient_name_selected = $('#recipient_name').val();
                            var contracted_by_select = $('#contracted_by_select').val();


                            $("#contracted_by_select").empty("");

                            $("<option value=''>Select</option>").appendTo('#contracted_by_select');
                            $.each(response.contacts, function (i, obj)
                            {

                                if (i == 0 && selected_address_book == "project") {
                                    var d_data = "<option value='" + obj.company_name + "' selected='selected'>" + obj.company_name + "</option>";
                                } else {
                                    if (obj.company_name == data.contact_data.company_name) {
                                        var d_data = "<option value='" + obj.company_name + "' selected='selected'>" + obj.company_name + "</option>";
                                    } else {
                                        var d_data = "<option value='" + obj.company_name + "'>" + obj.company_name + "</option>";
                                    }
                                }


                                $(d_data).appendTo('#contracted_by_select');
                            });
                            $("#contracted_by_select").trigger('change');



                            $("#recipient_name").empty("");
                            $("<option value=''>Select</option>").appendTo('#recipient_name');
                            $.each(response.contacts, function (i, obj)
                            {
                                if (i == 0 && selected_address_book != "project") {
                                    var div_data = "<option data-id='" + obj.id + "' value='" + obj.company_name + "' selected='selected'>" + obj.company_name + "</option>";
                                } else {
                                    if (obj.company_name == data.contact_data.company_name) {
                                        var div_data = "<option data-id='" + obj.id + "' value='" + obj.company_name + "' selected='selected'>" + obj.company_name + "</option>";
                                    } else {
                                        var div_data = "<option data-id='" + obj.id + "' value='" + obj.company_name + "'>" + obj.company_name + "</option>";
                                    }
                                }
                                $(div_data).appendTo('#recipient_name');
                            });
                            if (selected_address_book == "project") {
                                $("#recipient_name").trigger('change.select2');
                            } else {
                                $("#recipient_name").trigger('change');
                            }
                        }
                    });
                    $('#recipient_name').val(data.contact_data.company_name).trigger('change');
                    $('#recipient_name').selectpicker('refresh');
                    $('#reipient_contact').val(data.contact_data.phone);
                    $('#reipient_email').val(data.contact_data.email);
                    $('#reipient_address').val(data.contact_data.mailing_address);
                    $('#recipient_city').val(data.contact_data.city_name);
                    $('#recipient_city_id').val(data.contact_data.city_id);
                    $('#recipient_state').val(data.contact_data.state_id);
                    $('#recipient_state').selectpicker('refresh');
                    $('#reipient_zip').val(data.contact_data.zip);
                    // $('#recipient_county').val(data.contact_data.country_id);
                    //  $('#recipient_county').selectpicker('refresh');
                    $('#contactModal').modal('hide');
                }
                $('input,textarea').each(function () {
                    if ($(this).val().length > 0) {
                        $(this).addClass('not-empty');
                    } else {
                        $(this).removeClass('not-empty');
                    }
                });
            },
        });
    });

    //Recipient city
    src = "{{ url('customer/city/autocompletecityname') }}";

    $("#recipient_city").autocomplete({

        source: function (request, response) {
            $.ajax({
                url: src,
                dataType: "json",
                data: {
                    term: request.term
                },
                beforeSend: function () {
                    $('#recipientloader').show();
                },
                complete: function () {
                    $('#recipientloader').hide();
                },
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            value: item.value,
                            id: item.id     // EDIT
                        }
                    }));

                },

            });
        },
        select: function (event, ui) {

            $("#recipient_city_id").val(ui.item.id);
            $("#recipient_city").val(ui.item.value);
        },
        change: function (event, ui) {
            if (ui.item == null) {
                $(this).val("");
                $(this).focusout();
                $(this).focus();
            }
        }
    });


    $("#contact_city_id").autocomplete({

        source: function (request, response) {
            $.ajax({
                url: src,
                dataType: "json",
                data: {
                    term: request.term
                },
                beforeSend: function () {
                    $('#recipientpopuploader').show();
                },
                complete: function () {
                    $('#recipientpopuploader').hide();
                },
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            value: item.value,
                            id: item.id     // EDIT
                        }
                    }));

                },

            });
        },
        select: function (event, ui) {

            $("#autocomplete_city_id").val(ui.item.id);
            $("#contact_city_id").val(ui.item.value);
        },
        change: function (event, ui) {
            if (ui.item == null) {
                $(this).val("");
                $(this).focusout();
                $(this).focus();
            }
        }
    });
    /*county */
    src_county = "{{ url('customer/city/autocompletecountyname/'.$notice_id) }}";

    $("#county").autocomplete({

        source: function (request, response) {
            $.ajax({
                url: src_county,
                dataType: "json",
                data: {
                    term: request.term
                },
                beforeSend: function () {
                    $('#recipientcountyloader').show();
                },
                complete: function () {
                    $('#recipientcountyloader').hide();
                },
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            value: item.value,
                            id: item.id     // EDIT
                        }
                    }));

                },

            });
        },
        select: function (event, ui) {

            $("#recipient_county_id").val(ui.item.id);
            $("#county").val(ui.item.value);
        },
        change: function (event, ui) {
            if (ui.item == null) {
                $(this).val("");
                $(this).focusout();
                $(this).focus();
            }
        }
    });



});
//Edit Attachement Start
    function openTab(th)
    {
        window.open(th.name,'_blank');
    }

    $('#remove_icon').click(function(){
        $('#preview').css('display','none');
        $('#remove_icon').css('display','none');
        $('#remove_file').val('1');
        $('#soft_notice_work_order_result_file').val('');
    });

    $('#soft_notice_work_order_result_file').change(function(e){
        var fileName = e.target.files[0].name;
        doc_icon = fileName.split('.')[1];
        if(doc_icon == 'pdf'){
            $('#preview').html('<i class="fa fa-file-pdf-o" style="font-size:36px"></i>');
        }else if(doc_icon == 'doc' || doc_icon == 'docx'){
            $('#preview').html('<i class="fa fa-file-word-o" style="font-size:36px"></i>');
        }else if(doc_icon == 'xls' || doc_icon == 'xlsx'){
            $('#preview').html('<i class="fa fa-file-excel-o" style="font-size:36px"></i>');
        }else if(doc_icon == 'jpg' || doc_icon == 'jpeg'){
            $('#preview').html('<i class="fa fa-file-image-o" style="font-size:36px"></i>');
        }else{
            $('#preview').html('<i class="fa fa-file" style="font-size:36px"></i>');
        }
        // $('#preview').html(fileName);
        $('#preview').css('display','block');
        $('#remove_icon').css('display','block');
        //alert('The file "' + fileName +  '" has been selected.');
    });

    $(document).on('click', '.edit-attachment', function () {
        var row_id = $(this).closest('tr').attr('id');
        // $('#submit_document').removeClass('old_class').addClass('new_class');
        var type = $(this).attr('data-type');
        var file = $(this).attr('data-file');
        var title = $(this).attr('data-title');
        var id = $(this).attr('data-id');
        $('#edit_document_name').val(file);
        $('#edit_document_name').attr('data-file-original-name',$(this).attr('data-file-original-name'));
        $('#edit_document_name').attr('data-file-extension',$(this).attr('data-file-extension'));
        $('#soft_notice_work_order_doc_input').val(title);
                //alert($(this).attr('data-file-original-name'));

        if (title != ''){$('#soft_notice_work_order_doc_input').addClass('not-empty');}
        $('#edit_doc_id').val(id);
        $('#row_id').val(row_id);
        $('#preview').css('display','block');
        $('#remove_icon').css('display','block');

        //console.log(file);
        var APP_URL = {!! json_encode(url('/')) !!}
            // console.log(APP_URL);
            $("#open_doc").attr('name',APP_URL+'/attachment/work_order_document/'+file);
        doc_icon = file.split('.')[1];
        if(doc_icon == 'pdf'){
            $('#preview').html('<i class="fa fa-file-pdf-o" style="font-size:36px"></i>');
        }else if(doc_icon == 'doc' || doc_icon == 'docx'){
            $('#preview').html('<i class="fa fa-file-word-o" style="font-size:36px"></i>');
        }else if(doc_icon == 'xls' || doc_icon == 'xlsx'){
            $('#preview').html('<i class="fa fa-file-excel-o" style="font-size:36px"></i>');
        }else if(doc_icon == 'jpg' || doc_icon == 'jpeg'){
            $('#preview').html('<i class="fa fa-file-image-o" style="font-size:36px"></i>');
        }else if (doc_icon == 'csv'){
            $('#preview').html('<i class="fa fa-file" style="font-size:36px"></i>');
        }else{
            $('#preview').html('');
            $('#preview').css('display','none');
            $('#remove_icon').css('display','none');
        }

        //$('#edit_result_file').val(APP_URL+'/attachment/work_order_document/'+file);
        var id1 = file.split('.')[0];

        var id = $(this).attr('data-id');
        $('.soft_notice_work_order_doc_field').show();
        $('.soft_notice_work_order_doc_file').show();
        $('.doc_visibility').show();
        $('.update_doc').text('Update');
        $('#soft_notice_work_order_doc_type').val(type);
        $('#soft_notice_work_order_doc_type').selectpicker('refresh');
        /* $('#errormessage').text(file);
         $('#errormessage').css('display', 'block');*/
        var selected_doc_type = type;
        //$('#edit_doc_input_id').val("");
        if (selected_doc_type == 1) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('.doc_visibility').show();
            $('#doc_field').text('Bond Number');
        } else if (selected_doc_type == 2) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('.doc_visibility').show();
            $('#doc_field').text('NOC Book/Page');
        } else if (selected_doc_type == 3) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('.doc_visibility').show();
            $('#doc_field').text('Permit No');
        } else if (selected_doc_type == 5) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('.doc_visibility').show();
            $('#doc_field').text('Invoice No.');
        } else if (selected_doc_type == 4 || selected_doc_type == 6 ||
            selected_doc_type == 7 || selected_doc_type == 8 ||
            selected_doc_type == 9) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('.doc_visibility').show();
            $('#doc_field').text('Description');
        } else if (selected_doc_type == 10) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('.doc_visibility').show();
            $('#doc_field').text('Folio No.');
        } else if (selected_doc_type == 11) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('.doc_visibility').show();
            $('#doc_field').text('COL Signed.');
        } else if (selected_doc_type == 12) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('.doc_visibility').show();
            $('#doc_field').text('Recorded COL/SCOL.');
        } else if (!selected_doc_type) {
            $('.doc_field').hide();
            $('.doc_file').hide();
            $('.doc_visibility').hide();
        }
        /* if($('#submit_document span').html()=="Update"){
                                        // var file = data.db_arr[2];
                                         var id = file.split('.')[0];
                                         remove_arr.push(file);
                                         $('#remove_doc').val(remove_arr);
                                         var del_var = "refreshdelete" + id;
                      console.log(del_var);
                                         $("#" + del_var).remove();
                                     }*/
        /*$(".remove-attachment-model").attr("action", url);
        $("input[name='file']").remove();
        $('body').find('.remove-attachment-model').append('<input name="file" type="hidden" value="' + file + '">');
        $('body').find('.remove-attachment-model').append('<input name="id" type="hidden" value="' + id + '">');
        $('body').find('.remove-attachment-model').append('<input name="_method" type="hidden" value="POST">');*/
        /*$.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name=_token]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url: '{{url("customer/edit/work-ordwer/removeattachment")}}',
            data: {'file':file,'id':id},
            success: function (response) {
                if (response.success) {
                    //$('#attachment-confirmation-modal').modal('hide');
                    remove_arr.push(file);
                    $('#remove_doc').val(remove_arr);
                    var del_var = "refreshdelete" + id1;

                    $("#" + del_var).remove();
                }
            }
        });*/
    });
//Edit Attachement End

$(document).on('click', '.remove-attachment', function () {
    var url = $(this).attr('data-url');
    var file = $(this).attr('data-file');
    var id = $(this).attr('data-id');
    var row_id = $(this).closest('tr').attr('id');
    $(".remove-attachment-model").attr("action", url);
     $("input[name='file']").remove();
        $("input[name='id']").remove();
        $("input[name='row_id']").remove();
    $('body').find('.remove-attachment-model').append('<input name="file" type="hidden" value="' + file + '">');
    $('body').find('.remove-attachment-model').append('<input name="id" type="hidden" value="' + id + '">');
    $('body').find('.remove-attachment-model').append('<input name="_method" type="hidden" value="POST">');
      $('body').find('.remove-attachment-model').append('<input name="row_id" type="hidden" value="' + row_id + '">');
});
$(document).on('click', '.remove-parent-attachment', function () {
    var file = $(this).attr('data-file');
    var url = $(this).attr('data-url');
    $(".remove-attachment-model").attr("action", url);
    $("input[name='file']").remove();
    $('body').find('.remove-attachment-model').append('<input name="file" type="hidden" value="' + file + '">');
    $('body').find('.remove-attachment-model').append('<input name="_method" type="hidden" value="POST">');
});
var remove_arr = [];
$('#remove-attachment-model').submit(function (event) {
    event.preventDefault();
    var file = $('input[name=file]').val();
    var id = file.split('.')[0];
    var row_id = $('input[name=row_id]').val();

    $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name=_token]').attr('content')
        }
    });
    var submit_url = $(this).attr("action");
    $.ajax({
        type: 'POST',
        url: submit_url,
        data: $('#remove-attachment-model').serialize(),
        success: function (response) {
            if (response.success) {
                $('#attachment-confirmation-modal').modal('hide');
                remove_arr.push(row_id);
                $('#remove_doc').val(remove_arr);
                var del_var = "refreshdelete" + id;

                $("#" + row_id).remove();
            }
        }
    });
});
$(document).on('click', '.remove-record', function () {

    var recipient_id = $(this).attr('data-id');
    var notice_id = $(this).attr('data-notice-id');
    var work_order_id = $(this).attr('data-workorder-id');

    var url = "{{url('customer/recipient/deleteCreated/')}}" + '/' + recipient_id + '/' + notice_id + '/' + work_order_id;
     if (url.indexOf("#") ==-1){
            url += '#recipient'; 
        }
            $(".remove-record-model").attr("action", url);
});
$('.edit_recipient').on('click', function () {
    var recipient_id = $(this).attr('data-id');
    $('.msg-success').hide();
    $('.update_recipient').prop('disabled', false);
    $('.add_recipient').prop('disabled', true);
    $('.update_contact').prop('disabled', false);
    // $('.add_contact_details').prop('disabled', true);
    $.ajax({
        data: {"id": recipient_id},
        type: 'POST',
        url: "{{url('customer/work-order/get_recipient_data')}}", // point to server-side PHP script
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            if (response.success) {
                //console.log(response);
                $("#category_id").val(response.recipient.category_id);
                $('#category_id').selectpicker('refresh');
//                $('#recipient_name').val(response.recipient.name).trigger('change');
                $('#recipient_name').val(response.recipient.name).trigger('change.select2');
//                $('#recipient_name').selectpicker('refresh');
                $('#reipient_contact').val(response.recipient.contact);
                $('#reipient_email').val(response.recipient.email);
                $('#reipient_address').val(response.recipient.address);
                var option = new Option(response.recipient.city_name, response.recipient.city_id, true, true);
                $("#recipient_city").append(option).trigger('change');
                /*$('#recipient_city').val(response.recipient.city_name);
                $('#recipient_city_id').val(response.recipient.city_id);*/
                $('#recipient_state').val(response.recipient.state_id);
                $('#recipient_state').selectpicker('refresh');
                $('#reipient_zip').val(response.recipient.zip);
                $('#recipient_attn').val(response.recipient.attn);
                $('#recipient_id').val(recipient_id);
                $('#recipt_id').val(response.recipient.contact_id);
                var urlzipcode = $('#contact_zipurl').val();
                    $.ajax({
                        url: urlzipcode,
                        type: 'post',
                        dataType: "json",
                        data: {
                            zipcode: response.recipient.zip,
                            cityName:response.recipient.city_id
                        },
                        success: function(data) {
                            $(".new_reipient_zip").val(data[0].label);
                        }
                    });
                $('input,textarea').each(function () {
                    if ($(this).val().length > 0) {
                        $(this).addClass('not-empty');
                    } else {
                        $(this).removeClass('not-empty');
                    }
                });
            }
        }
    });
});
$('.update_recipient').on('click', function () {
    $('.msg-success').show();
});
$('.update_contact').on('click', function () {
    var recipt_id = $('#recipient_name').find(':selected').attr('data-id');

    if (recipt_id != null) {
        $.ajax({
            data: $('#update_recipient_data').serialize(),
            type: 'POST',
            url: "{{url('customer/wo_update_recipient_contact')}}", // point to server-side PHP script
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                if (response.success) {
//                $('.msg-success').show();
//                $('.msg-success').html('Contact successfully updated');
                    alert('Contact successfully updated');
                }
            }
        });
    } else {
        alert("Name is not exist in the address book.Please add it to address book")
    }
});
var autocompletecityzip_src = "{{ url('customer/city/autocompletecityzip') }}";

                </script>
               <!--  <script type="text/javascript" src="{{ asset('js/autocomplete_zip.js') }}"></script> -->
                @endsection