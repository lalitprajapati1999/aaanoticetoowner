<!--<table border="1" style="width: 700px;font-size: 12px;padding:7px;table-layout: fixed;page-break-after:always;">
    <tbody>
        <tr>
            <td style="text-align: left;padding: 5px;font-size: 12px;">
                <table>
                    <tr>
                        <td style="text-align: center;">
                            <img  src="{{url('images/logo.png')}}" style="height: 120px;width: 120px;">
                        </td>
                    </tr>
                    <tr style="font-size: 12px;">
                        <td>AAA BUSINESS ASSOC.<br />
                            P.O. Box 22821<br />
                            Hialeah Florida 33002</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <th scope="col" class="action">Work Order No.</th>
            <th scope="col" class="action">Document</th>
            <th scope="col" class="action">Contracted By</th>
            <th scope="col" class="action">Mailing</th>
            <th scope="col" class="action">Recipients</th>
            <th scope="col" class="action">Total</th>
        </tr>
<?php foreach ($result as $result_val) { ?>
                                        <tr>
                                            <td>
                                                {{$result_val['workorder_id']}}
                                            </td>
                                            <td>
                                                {{$result_val['notice_name']}}
                                                <br/>
                                                @if(isset($result_val['recipients']) && count($result_val['recipients']) > 0)
                                                @foreach($result_val['recipients'] AS $k=>$val)
                                                {{$k+1}}.{{$val->address}} {{$val->state}} {{$val->city}} {{$val->zip}}<br/>

                                                @endforeach
                                                @endif
                                            </td>
                                            <td>
                                                {{$result_val['contracted_by']}}
                                            </td>
                                            <td>
    <?php echo $result_val['workorder_id'] ?>
                                            </td>
                                            <td>
    <?php echo $result_val['label_count'] ?>
                                            </td>
                                            <td>
    <?php echo $result_val['total_amount'] ?>
                                            </td>
                                        </tr>
<?php } ?>
    </tbody>
</table>-->
<!DOCTYPE html>
<html>
    <head>
        <title>Statement</title>
    </head>
    <body>
        <table style="width: 800px;font-size: 14px;margin: 0 auto">
            <tr>
                <td scope="col" width="200px">
                    <table width="100%">
                        <tr>
                            <td><img src="{{url('images/logo.png')}}" style="height: 150px;width: 150px;"></td>
                        </tr>
                        <tr>
                            <td style="padding: 10px;text-transform: uppercase;">
                                {{$customer_company_name->company_name}} <br> 	
                                {{$customer_company_name->mailing_address}}<br> 	
                                {{$company_city_name->name}} {{$company_state_name->name}}  {{$customer_company_name->mailing_zip}}
                            </td>
                        </tr>
                    </table>
                </td>
                <td width="500px" scope="col">
                    <table>
                        <tr>
                            <td scope="col" style="font-weight: bold;padding: 10px;text-align: center;font-size: 17px;">{{env('FROM_NAME')}},{{env('FROM_ADDRESS')}}<br/>{{env('FROM_CITY')}}, {{env('FROM_STATE' )}} - {{env('FROM_ZIPCODE')}}<br/> Tel:{{env('FROM_PHONE_NUMBER')}} <br/> {{env('FROM_EMAIL')}}</td>
                        </tr>
                        <tr>
                            <td style="padding: 10px;text-transform: uppercase;" align="right">Statement <br> 	
                                {{$statement_month}} {{$statement_year}}
                            </td>
                        </tr>
                       <!-- <tr>
                            <td style="padding: 10px;font-size:11px;font-weight:bold;text-transform: uppercase;" align="right">	
                                term:15 days
                            </td>
                        </tr>-->
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <table>
                        <tr>
                            <td style="border-bottom: 3px solid #000;text-align: left;">
                                <table>
                                    <tr>
                                        <th scope="col" width="75px" style="padding: 2px">
                                            W/O #
                                        </th>
                                         <th scope="col" width="100px" style="padding: 2px">
                                          Your Project
                                        </th>
                                        <th scope="col" width="235px" style="padding: 2px">
                                            Document
                                        </th>
                                       
                                        <th scope="col" width="170px" style="padding: 2px">
                                            Contracted By 
                                        </th>
                                        <th scope="col" width="200px" style="padding: 2px">
                                            Mailing
                                        </th>
                                        <th scope="col" width="75px" style="padding: 2px">
                                            Recipient
                                        </th>
                                        <th scope="col" width="75px" style="padding: 2px">
                                            Total
                                        </th>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <?php foreach ($result as $result_val) { ?>
                            <tr>
                                <td style="border-bottom: 1px solid #000;">
                                    <table>

                                        <tr>
                                            <td scope="col" width="75px" style="word-break: break-all;padding: 2px;">
                                                # {{$result_val['workorder_id']}}
                                            </td>
                                             <td scope="col" width="100px" style="text-transform: uppercase;word-break: break-all;padding: 2px;">
                                                <?php echo isset($result_val['your_job_reference_no']) ? $result_val['your_job_reference_no'] : "-" ?>
                                            </td>
                                            <td scope="col" width="235px" style="text-transform: uppercase;word-break: break-all;padding: 2px;">
                                                {{$result_val['notice_name']}}

                                                <?php /*  @if(isset($result_val['recipients']) && count($result_val['recipients']) > 0)
                                                  @foreach($result_val['recipients'] AS $k=>$val)
                                                  {{$k+1}}.{{$val->address}} {{$val->state}} {{$val->city}} {{$val->zip}}<br/>

                                                  @endforeach
                                                  @endif */ ?>
                                                <br/>
                                                {{$result_val['project_address']}}
                                            </td>
                                           
                                            <td scope="col" width="170px" style="text-transform: uppercase;word-break: break-all;padding: 2px;">
                                                {{$result_val['contracted_by']}}
                                            </td>
                                            <td scope="col" width="200px" style="text-transform: uppercase;word-break: break-all;padding: 2px;">
                                                <?php echo  '&nbsp;<br/>'.$result_val['type_of_label'] ?>
                                            </td>
                                            <td scope="col" width="75px" style="text-transform: uppercase;word-break: break-all;padding: 2px;">
                                                <?php echo  '&nbsp;<br/>'.$result_val['label_count'] ?>
                                            </td>
                                            <td scope="col" width="75px" style="text-transform: uppercase;word-break: break-all;padding: 2px;">
                                                <?php echo $result_val['total_amount'] ?>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        <?php } ?>


                        <tr>
                            <td style="border-bottom: 1px solid #000;text-transform: uppercase;">
                                <table>
                                    <tr>
                                        <th scope="col" style="text-align: left;font-size: 12px;">
                                            please pay promtly to avoid collection fees</br>late fee of $15.00 will be applied if statement is not paid within the 15 day term
                                        </th>

                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom: 1px solid #000;text-transform: uppercase;">
                                <table width="100%">
                                    <tr>
                                        <td scope="col" style="text-align: left;font-weight: bold;">
                                            total
                                        </td>
                                        <td scope="col" style="font-weight: bold;" align="right">
                                            {{$final_total}}
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>


        </table>

        <script type="text/javascript">
            window.print();
        </script>
    </body>
</html>

