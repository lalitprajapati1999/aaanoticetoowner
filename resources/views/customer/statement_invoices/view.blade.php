<style type="text/css">
	.right-sec{float: right;width: 29%;}.logo-sec{float: right;}
	.label-heads{opacity: 0.5;}
	.dotted-ln{border: 0.5px dashed black; opacity: 0.5}
	.head-sec{ color:#051c42; float: left; }
	.tbl-heads{color:#051c42;}
	.backgrd{background-color: #c9ced4}
</style>
<div class="container-fluid bg-3 text-center">  
	<!-- <div class="col-sm-12"> -->
		<div class="logo-sec">
			<img src="{{ url('/img/logo.png') }}">
		</div>	
	<!-- </div> -->
	<h3 class="head-sec">Invoice: #{{ $invoice_arr['doc_number'] }}</h3>
</div>		
		<div class="row">
			<div class="col-sm-6">
		      <span class="label-heads">Bill To</span>
				<p>
		        	{{ $billing_address['Line1'] or '' }} <br>
		        	{{ $billing_address['Line2'] or '' }}
		        	{{ $billing_address['Line3'] or '' }}
		        	{{ $billing_address['Line4'] or '' }}
		        	{{ $billing_address['Line5'] or '' }}
		        	{{ $billing_address['City'] or '' }}
		        	{{ $billing_address['Country'] or '' }}
		        	{{ $billing_address['PostalCode'] or '' }}
		        	{{ $billing_address['Note'] or '' }}
		        </p>
		    </div>
		    <div class="col-sm-6">
		      <span class="label-heads">Invoice Date </span>				
				<p>	{{ $invoice_arr['tranx_date'] or '' }}</p>		
		    </div>								
		</div>		
		<div class="row">
			<div class="col-sm-4"> 
		      <div class="label-heads"> Job Ref No</div>
		      <p>{{ $invoice_arr['job_ref_no'] or '' }}</p> 
		    </div>
		    <div class="col-sm-4">
		      <div class="label-heads"> Notice Type</div>	
				<p>{{ $invoice_arr['notice_type'] or '' }}</p>
		    </div>
		    <div class="col-sm-4">
			    <div class="label-heads"> Job Location</div>	
				<p>{{ $invoice_arr['job_location'] or '' }}</p> 		
		    </div>		
		</div>		
  		<div class="row">
  		<div class="col-sm-12">
		    <table class="table table-responsive" border="0">
		  		<tr class="tbl-heads backgrd">
			    	<th>PRODUCT/SERVICE</th>
			    	<th>DESCRIPTION</th>
			    	<th>QTY</th>
			    	<th>RATE</th>
			    	<th>AMOUNT</th>
			    </tr>
			  	@if(isset($other_details_arr) && count($other_details_arr)>0)
			  	@foreach($other_details_arr as $details)
			  	@php 
			  		if($details['DetailType'] == 'SubTotalLineDetail' )
			  		{
						continue;	  			
			  		}	    		  		
			  	@endphp
			    	<tr>
			    		<td>	    			
			    			@php
				    			switch ($details['SalesItemLineDetail']['ItemRef']) 
				    			{
				    				case config('constants.QBO.NoticeBaseAmount'):
							        	$product = 'NTO';
							        break;
							        case config('constants.QBO.CertifiedMail'):
							        	$product = 'Certified Mail';
							        break;
							        case config('constants.QBO.NextDayDelivery'):
							        	$product = 'Next Day Delivery';
							        break;
							        case config('constants.QBO.PriorityMail'):
							        	$product = 'Priority Mail';
							        break;
							        case config('constants.QBO.RushHourCharges'):
							        	$product = 'Rush Fee';
							        break;
							        case config('constants.QBO.FirmMail'):
							        	$product = 'Firm Mail';
							        break;
							        case config('constants.QBO.AdditionalAddress'):
							        	$product = 'Additional Address';
							        break;
							        case config('constants.QBO.ElectronicReturnReceipt'):
							        	$product = 'Electronic Return Receipt';
							        break;
							        case config('constants.QBO.ReturnReceiptRequested'):
							        	$product = 'Return Receipt Requested';
							        break;
							        case config('constants.QBO.ManualCharges'):
							        	$product = 'Manual';
							        break;
									default:
										$product = '-';
									break;
			    				}
			    			@endphp
			    			{{ $product or '' }} 
			    		</td>
				    	<td>{{ $details['Description'] or '' }}</td>
				    	<td>{{ $details['SalesItemLineDetail']['Qty'] or '' }}</td>
				    	<td>${{ $details['SalesItemLineDetail']['UnitPrice'] or '' }}</td>
				    	<td>${{ $details['Amount'] or '' }}</td>
				    </tr>	      
			    @endforeach	    
			    @endif
		    </table>
	    <hr class="dotted-ln"/>
		</div>
    	<div class="row">
    		<div class="col-sm-12">
	    		<div style="float: right; padding-right: 50px">
	    			<span class="label-heads">TOTAL : </span> 
	    			<span style="font-weight: bold;">${{ $invoice_arr['balance'] or '' }}</span>
	    		</div>
    		<div>
    	</div>
  
</div>