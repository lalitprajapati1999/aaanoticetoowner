@extends('adminlte::page')
@section('content')
<div>
   <section class="address-book">
      <div class="dashboard-wrapper">
         @if (Session::get('success'))
         <div class="alert alert-success">
            <?php echo Session::get('success'); ?>
         </div>
         @endif
         <div class="dashboard-heading">
            <h1><span>{{ $page_title or '' }}</span></h1>
         </div>
         <div class="dashboard-inner-body">
            <div class="row"></div>
            <div class="dahboard-table table-responsive fixhead-table">
               <table class="table table-striped" id="invoice-data" datatable="" width="100%" cellspacing="0" data-scroll-x="true" scroll-collapse="false">
                  <thead class="thead-dark">
                     <tr>
                        <th scope="col">Invoice ID</th>
                        <th scope="col">Invoice Date</th>
                        <th scope="col">Notice Type</th>
                        <th scope="col">Doc Number</th>
                        <th scope="col">Invoice Status</th>
                        <th scope="col">Total Amount</th>
                        <th scope="col" width="20%">Action</th>
                     </tr>
                  </thead>
               </table>
            </div>
         </div>
      </div>
   </section>
</div>
<!-- Modal -->
<div class="modal fade" id="invoiceDetails" role="dialog">
   <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Invoice Details</h4>
         </div>
         <div class="modal-body" id="invoice-details">
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div>
      </div>
   </div>
</div>

@endsection
@section('frontend_js')
<script type="text/javascript" src="{{ asset('js/jquery-ui.min.js') }}"></script>
<script type="text/javascript">
   $(function() {
      $.ajaxSetup({
         headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
      });
      var contactsTable = $('#invoice-data').DataTable({
         //fixedHeader: true,
         "search": {
            "caseInsensitive": true,
            'use_wildcards': false
         },
         "language": {

            "zeroRecords": "No Record Found",
         },
         processing: true,
         serverSide: true,
         dom: "<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-6'p>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
         ordering: true,
         order: [
            [1, 'desc']
         ],
         pagingType: "simple_numbers",
         ajax: {
            url: "{{url('customer/invoice/invoice-filter')}}",
            data: function(d) {}
         },
         columns: [{
               data: 'invoice_id',
               name: 'invoice_id'
            },
            {
               data: 'tranx_date',
               name: 'tranx_date'
            },
            {
               data: 'notice_type',
               name: 'notice_type'
            },
            {
               data: 'doc_number',
               name: 'doc_number'
            },
            {
               render: function(data, type, row, meta) {

                  let invoice_status;
                  if (row.invoice_status == null) {
                     invoice_status = 'Pending';
                  } else {
                     invoice_status = row.invoice_status;
                  }
                  return invoice_status;
               }
            },
            {
               render: function(data, type, row, meta) {
                  return '$' + row.total_amt;
               }
            },
            {
               render: function(data, type, row, meta) {
                  return ` <a href="javascript:void(0)" data-invoice-id="` + row.id + `" title="view Details" class="btn btn-secondary item red-tooltip toggleModel"><span class="glyphicon">&#xe140;</span></a>`;
               }
            }
         ]
      });
   });

   $(document).on("click", ".toggleModel", function() {

      let invoiceId = $(this).attr('data-invoice-id');

      let view_url = '{{ url("/") }}/customer/invoice/invoice-details/' + btoa(invoiceId);
      $('#invoiceDetails').modal('toggle');

      $.ajax({
         type: 'GET',
         async: false,
         url: view_url,
         success: function(response) {
            if (typeof(response) == 'object') {
               $('#invoice-details').empty();
               $('#invoice-details').append(response['html']);
            }
         },
         error: function(response) {
            console.log('error', response);
         }
      });
   });
</script>
@endsection