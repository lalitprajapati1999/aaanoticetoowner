@extends('adminlte::page')
@section('content')
<div>
    <section class="address-book reports">
        <div class="dashboard-wrapper">

            @if(Session::get('success'))
            <div class="alert alert-success">
                <?php echo Session::get('success'); ?>
            </div>
            @endif
            @if(Session::get('errors'))
            <div class="alert alert-error">
               <?php echo Session::get('error'); ?>
            </div>
            @endif
            <div class="employee-status"></div>
            <div class="dashboard-heading">
                <h1><span>Statement</span></h1>
            </div>
            <div class="dashboard-inner-body">
                <div class="row">
                    <form role="form" method="post" id="search-data">
                        <div class="col-md-7">
                            <div class="search-by">
                                <div class="form-group full-width">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-xs-6 report_type_html">
                                            <div class="select-opt full-width pull-left">
                                                <label for="" class="display-ib">Month: </label>
                                                <select class="selectpicker" name="month" class="month" id="month">
                                                    <!--                                                <option value="">Select</option>-->
                                                    <option value="">Select</option>
                                                    <option value="01" @if(date('m') == '01') selected='selected' @endif>Jan</option>
                                                    <option value="02"@if(date('m') == '02') selected='selected' @endif>Feb</option>
                                                    <option value="03" @if(date('m') == '03') selected='selected' @endif>Mar</option>
                                                    <option value="04" @if(date('m') == '04') selected='selected' @endif>Apr</option>
                                                    <option value="05" @if(date('m') == '05') selected='selected' @endif>May</option>
                                                    <option value="06" @if(date('m') == '06') selected='selected' @endif>June</option>
                                                    <option value="07" @if(date('m') == '07') selected='selected' @endif>Jul</option>
                                                    <option value="08" @if(date('m') == '08') selected='selected' @endif>Aug</option>
                                                    <option value="09" @if(date('m') == '09') selected='selected' @endif>Sep</option>
                                                    <option value="10" @if(date('m') == '10') selected='selected' @endif>Oct</option>
                                                    <option value="11" @if(date('m') == '11') selected='selected' @endif>Nov</option>
                                                    <option value="12" @if(date('m') == '12') selected='selected' @endif>Dec</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <div class="select-opt full-width pull-left">
                                                <label for="" class="display-ib">Year: </label>
                                                <select class="selectpicker name" name="year" id="year">
                                                    <option value="">Select</option>
                                                    <?php
                                                    $minOffset = 0;
                                                    $maxOffset = 2; // Change to whatever you want
                                                    $thisYear = date('Y');
                                                    for ($i = $minOffset; $i <= $maxOffset; $i++) {
                                                        $year = $thisYear - $i;
                                                        ?>
                                                        <option value="{{$year}}" @if(date('Y') == $year) selected='selected' @endif>{{$year}}</option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <span class="form-group-btn">
                            <button class="btn btn-search" id="search_info" type="submit"><span class="icon-search"></span></button>
                        </span>
                    </form>

                    <form id="reportstable1" action ="{{url('customer/statement-invoice/print/')}}" method="POST" class="full-width">
                        {!! csrf_field() !!}
                        <div class="col-md-5 col-md-offset-2 text-right pull-right">
                            <div class="add-new-btn display-ib">
<!--                                <button  type="submit" class="btn btn-primary custom-btn"><span>Email</span></button>-->
                                <!--<a href="#mailModal" id="sendReportEmail"  data-toggle="modal" class="btn btn-primary custom-btn" data-toggle="tooltip" data-placement="bottom" title="Email"><span>Email</span></a>-->
                            </div>
                            <div id="searchdata"></div>
                            <div class="add-new-btn display-ib">
                                <button type="submit" name="reporttype" formtarget="_blank" value="printdata" class="btn btn-primary custom-btn"><span>Print</span></button>
                            </div>

                        </div>
                    </form>
                </div>

                <div class="dahboard-table table-responsive fixhead-table">
                    <table class="table table-striped" id="statementinvoices-data" datatable="" width="100%" cellspacing="0"   data-scroll-x="true"scroll-collapse="false">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col" class="action">Work Order No.</th>
                                <th scope="col" class="action">Your Project</th>
                                <th scope="col" class="action">Document</th>

                                <th scope="col" class="action">Contracted By</th>
                                <th scope="col" class="action">Mailing</th>
                                <th scope="col" class="action">Recipients</th>
                                <th scope="col" class="action">Total</th>
                             <!--   <th scope="col" class="action">Options</th>-->

                            </tr>
                        </thead>

                    </table>

                </div>
            </div>

        </div>
    </section>
</div>
@stop
@section('frontend_js')
<script type="text/javascript" src="{{asset('js/dataTables.buttons.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/buttons.html5.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/buttons.print.min.js')}}"></script>
<script type='text/javascript'>
$(document).ready(function () {
    var statementInvoicesTable = $('#statementinvoices-data').DataTable({
        // "pageLength":10
        processing: true,
        serverSide: true,
        bpaginate: true,
        dom: "<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-6'p>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
        ajax: {
            url: "{{url('customer/statement-invoices/custom-filter')}}",
            data: function (d) {
                d.month = $('#month').val();
                d.year = $('#year').val();
            }
        },
        columns: [
            {data: 'workorder_id', render: function (data, type, row) {
                    return "#" + data;
                }},
            {data: 'your_job_reference_no', name: 'your_job_reference_no'},
            {data: 'document', name: 'document'},

            {data: 'contracted_by', name: 'contracted_by'},
            {data: 'type_of_label', render: function (data, type, row) {
                     data = '&nbsp;<br/>'+data; 
                     return data.split(",").join("<br/>");
                }},
            {data: 'label_count', render: function (data, type, row) {
                    data = '&nbsp;<br/>'+data; 
                    return data.split(",").join("<br/>");
                }},
            {data: 'total_amount', render: function (data, type, row) {
                    return data.split(",").join("<br/>");
                }},

                    //  {data: 'options', name: 'options'}
        ],
        order: [[0, 'desc' ]],

        /*  dom: 'Bfrtip',
         buttons: [
         
         {
         extend: 'print',
         text: 'Print',
         exportOptions: {
         columns: [0, 1, 2]
         }
         }
         
         ]*/
    });
    $('#search-data').on('submit', function (e) {
        statementInvoicesTable.draw();
        e.preventDefault();
    });
    $('body').tooltip({selector: '[data-toggle="tooltip"]',
         trigger: 'hover',
                                                            placement: 'top',
    });
});
$('#search_info').on('click', function () {
    $("#searchdata").html("");

    $.each($('#search-data').serializeArray(), function (i, field) {
        console.log(field);
        $("#searchdata").append('<input type="hidden" name="' + field.name + '" value="' + field.value + '">');
    });
});
</script>
@stop