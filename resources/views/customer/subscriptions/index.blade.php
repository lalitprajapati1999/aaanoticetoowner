

@extends('layouts.page')
@section('content')
<style>
    .ui-datepicker-calendar {
        display: none;
    }
    .parsley-errors-list{
     top: 60px; 
    }
</style>
​

<section class="section-wrapper pricing price-subscription">
    <div class="main-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="section-heading">
                    <h1 class="section-title text-center">We get it done, you get paid</h1>
                </div>
            </div>
        </div>
        <div class="subscription-body">
            <div class="row">
                <div class="col-md-5">
                    <div class="left-subsci full-width">
                        @if(count($pricings)>0)
                        <div class="sub-section-title">
                            <h2 class="">Package Details</h2>
                        </div>
                        <div class="dahboard-table table-responsive fixhead-table" style="height: 60%;">
                            <table class="table table-striped" id="contacts-data" datatable="" width="100%" cellspacing="0"   data-scroll-x="true"scroll-collapse="false">
                                <thead class="thead-dark">
                                    <tr><th scope="col"> Name</th>
                                            <th scope="col">Charge</th>
                                        </tr>

                                </thead>
                                <tbody>
                                    @foreach($pricings as $pricing)
                                        <tr><th scope="col">{{ $pricing->name}}</th>
                                            <th scope="col">${{ $pricing->charge}}</th>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        @endif
                        <div class="price-body">
                            <div class="row">
                                @if(!empty($package))
                                <div class="col-lg-12 col-sm-12 col-xs-12 col-md-12">
                                    <div class="price-box">
                                        <div class="price-top">
                                            <h1>${{$package->charge}}</h1>
                                            <h4>{{$package->name}}</h4>
                                        </div>
                                        <div class="price-middle">
                                            <div class="listing-group text-center">
                                                <h3 class="pricem-title">{{$package->description}}</h3>
                                                <ul class="list-group full-width">
                                                    <li>Unlimited Recipients</li>
                                                    <li>Unlimited Projects</li>
                                                    <li>Unlimited Database</li>
                                                    <li>Free Partial Release</li>
                                                    <li>Free Final Release</li>
                                                    <li>Deadline Reminder</li>
                                                    <li>Chat Support</li>
                                                    <li>You Create it We Mail it</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div> 
                <div class="col-md-7">
                    <div class="right-subsci full-width">
                        <div class="sub-section-title">
                            <h2 class="">Payment Method</h2>
                        </div>
                        <!-- <form class="form-horizotal"> -->
                        <div class="row form-horizontal">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <ul class="nav nav-tabs radio-tabs button-holder full-width" role="tablist">
                                    <li role="presentation" class="active col-md-6"><a href="#hard-doc" aria-controls="home" role="tab" data-toggle="tab"><input type="radio" id="radio-1-1" name="radio-1-set" class="regular-radio" value="hard" checked /><label for="radio-1-1">Credit Card</label></a></li>
                                    <li role="presentation" class="col-md-6"><a href="#soft-doc" aria-controls="profile" role="tab" data-toggle="tab"><input type="radio" id="radio-1-2" name="radio-1-set" class="regular-radio"  value="soft" /><label for="radio-1-2">Payment through the Check</label></a></li>
                                </ul>
                            </div>
                            <div class="col-md-12">
                                @if (Session::get('success'))
                                <div class="flash-message">
                                    <div class="alert alert-success" style="    margin: unset">
                                        <?php echo Session::get('success'); ?>
                                    </div>
                                </div>

                                @elseif(session('error'))
                                <div class="flash-message">
                                    <div class="alert alert-danger" style="    margin: unset">
                                      <?php echo Session::get('error'); ?>
                                    </div>
                                </div>
                                @endif

                                <div class="tab-content full-width">
                                    <div role="tabpanel" class="tab-pane active" id="hard-doc">
                                        <form id="credit-card-form" data-parsley-validate action="{{ url('customer/subscription/get-paid')}}" method="POST">
                                            {{ csrf_field() }}
                                            @if(!empty($user_cards))
                                            <h2>Saved Cards</h2>
                                            @foreach($user_cards as $card)
                                            <input type="radio" id="card_id"  name="card_id" value="{{ $card['id']}}">{{ $card['number']}}<br/>
                                            @endforeach
                                            @endif
                                            <div class="sub-section-title">
                                                <h2 class="sec-title">Credit Card Details</h2>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                    <div class="input-wrapper full-width">
                                                        <div class="styled-input">
                                                            <input type="hidden" name="package_id" value="{{ $package['id'] }}">
                                                            <input type="hidden" name="amount" value="{{ $package['charge'] }}">
                                                            <input id="credit_card_name" type="text" name="credit_card_name" class="form-control" data-parsley-required="true" value="{{ old('credit_card_name') }}" data-parsley-required-message="Person Name is required" data-parsley-trigger="change focusout" data-parsley-pattern="/^[a-zA-Z ]*$/" maxlength="100">    
                                                            <label>Person's name (As it appears on card)<span class="mandatory-field">*</span></label>
                                                            <span></span>
                                                            @if ($errors->has('credit_card_name'))
                                                            <p class="help-block">
                                                                <strong>{{ $errors->first('credit_card_name') }}</strong>
                                                            </p>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                    <div class="select-opt">
                                                        <label class="col-md-12 control-label" for="textinput">Card Type<span class="mandatory-field">*</span></label>
                                                        <select name="card_type" class="selectpicker" data-show-subtext="true" data-live-search="true" data-parsley-required="true" data-parsley-required-message="Card Type is required" data-parsley-trigger="change focusout" id='card_type'>
                                                            <option value="">-Select-</option>
                                                            @php ($card_types = ['Mastercard'=>'Mastercard','Visa'=>'Visa','American Express'=>'American Express','Discover Card'=>'Discover Card','Maestro'=>'Maestro'])
                                                            @foreach($card_types as $key=>$val)
                                                            <option value="{{$key}}" @if (old('card_type') == $key) selected="selected" @endif > {{$val}}</option>
                                                            @endforeach
                                                        </select>
                                                        @if ($errors->has('card_type'))
                                                        <p class="help-block">
                                                            <strong>{{ $errors->first('card_type') }}</strong>
                                                        </p>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                    <div class="input-wrapper full-width">
                                                        <div class="styled-input">
                                                            <input id="card_number" pattern="[0-9\s]*" type="text" name="card_number" class="form-control" data-parsley-required="true" value="{{ old('card_number') }}" data-parsley-required-message="Card Number is required" data-parsley-trigger="change focusout">    
                                                            <label>Card Number<span class="mandatory-field">*</span></label>
                                                            <span></span>
                                                            @if ($errors->has('card_number'))
                                                            <p class="help-block">
                                                                <strong>{{ $errors->first('card_number') }}</strong>
                                                            </p>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                    <div class="input-wrapper full-width">
                                                        <div class="styled-input">
                                                            <input type="text" id="datepicker" name="credit_card_date" class="form-control not-empty" data-parsley-required="true" value="{{ old('credit_card_date') }}" data-parsley-required-message="Expiry Date is required" data-parsley-trigger="change focusout" onfocusout="checkForInput()">    
                                                            <label>Expiry Date<span class="mandatory-field">*</span></label>
                                                            <span></span>
                                                            @if ($errors->has('credit_card_date'))
                                                            <p class="help-block">
                                                                <strong>{{ $errors->first('credit_card_date') }}</strong>
                                                            </p>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                    <div class="input-wrapper full-width">
                                                        <div class="styled-input">
                                                            <!-- <input type="text" name="security_code" class="form-control" data-parsley-required="true" value="{{ old('security_code') }}" data-parsley-required-message="Security Code is required" data-parsley-trigger="change focusout" >     -->
                                                            <!-- data-parsley-type="digits" -->
                                                            <input type="text" class="form-control stylish_security_code" data-parsley-required="true" value="{{ old('security_code') }}" data-parsley-required-message="Security Code is required" data-parsley-minlength="3" data-parsley-maxlength="3" data-parsley-minlength-message="Security code must be 3 digits" data-parsley-maxlength-message="Security code must be 3 digits" data-parsley-trigger="change focusout"><br>
                                                            <input type="hidden" name="security_code" class="hidden_security_code" value="{{ old('security_code') }}">
                                                            <label>Security Code<span class="mandatory-field">*</span></label>
                                                            <span></span>
                                                            @if ($errors->has('security_code'))
                                                            <p class="help-block">
                                                                <strong>{{ $errors->first('security_code') }}</strong>
                                                            </p>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                    <div class="input-wrapper full-width">
                                                        <div class="styled-input">
                                                            <input type="text" name="credit_zip_code" class="form-control" data-parsley-required="true" value="{{ old('credit_zip_code') }}" data-parsley-required-message="Zip Code of Credit Card is required" data-parsley-trigger="change focusout" data-parsley-type="digits">    
                                                            <label>Zip Code of Credit Card<span class="mandatory-field">*</span></label>
                                                            <span></span>
                                                            @if ($errors->has('credit_zip_code'))
                                                            <p class="help-block">
                                                                <strong>{{ $errors->first('credit_zip_code') }}</strong>
                                                            </p>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                         <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-bottom-btn text-right">
                                                <button type="submit" name="continue" class="btn btn-primary custom-btn customc-btn" value="submit"><span>Pay</span></button>
                                                 <a class="btn btn-primary custom-btn customb-btn not-empty" href="{{url('/home')}}"><span>Back to Dashboard</span></a>
                                            </div>
                                        </div>
                                    </form>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="soft-doc">
                                        <form data-parsley-validate action="{{ url('customer/subscription/save_cheque_details') }}" method="POST" name="cheque_form" id="cheque_form" >
                                            {{ csrf_field() }}
                                            <input type="hidden" name="id" value="{{ $package['id'] }}">
                                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                <div class="input-wrapper full-width">
                                                    <div class="styled-input">
                                                       <!--  <input class="form-control"  type="text" name="cheque_number" data-parlsey-required="true" data-parsley-required-message="Check number is required"
                                                        data-parsley-trigger="change focusout" data-parsley-type="digits"> -->
                                                         <input id="cheque_number" pattern="[0-9\s]*" type="text" name="cheque_number" class="form-control" data-parsley-required="true"" data-parsley-required-message="Check Number is required" data-parsley-trigger="change focusout" data-parsley-type="digits">
                                                        @if ($errors->has('cheque_number'))
                                                        <p class="help-block"> <strong>{{ $errors->first('cheque_number') }}</strong> </p>
                                                        @endif
                                                        <label>Check Number </label>
                                                        <span></span>	
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                                <div class="input-wrapper full-width">
                                                    <div class="styled-input">
                                                       <!--  <textarea class="payment-bank" id="bankdetails" name="bankdetails"
                                                        data-parlsey-required="true" data-parsley-required-message="Bank  Details is required"
                                                        data-parsley-trigger="change focusout" data-parsley-pattern="/^[a-zA-Z ]*$/" maxlength="100"></textarea> -->
                                                        <textarea id="bankdetails" pattern="^[a-zA-Z ]*$" type="text" name="bankdetails" class="form-control" data-parsley-required="true"" data-parsley-required-message="Bank  Details is required" data-parsley-trigger="change focusout"></textarea>
                                                        @if ($errors->has('bankdetails'))
                                                        <p class="help-block"> <strong>{{ $errors->first('bankdetails') }}</strong> </p>
                                                        @endif
                                                        <label>Bank Details </label>
                                                        <span></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-bottom-btn text-right">
                                                <button id="submit_card_button" type="submit" name="get_paid" class="btn btn-primary custom-btn customc-btn" value="submit"><span>Pay</span></button>
                                                <a class="btn btn-primary custom-btn customb-btn not-empty" href="{{url('/home')}}"><span>Back to Dashboard</span></a>

                                            </div>
                                            
                                        </div>
                                        </div>
                                       
                                    </form>
                                </div>
                               <!--  <div role="tabpanel" class="tab-pane" id="soft-doc">
                                    <form action="{{ url('customer/subscription/save_cheque_details') }}" method="POST" name="cheque_form" parsley-validate>
                                        {{ csrf_field() }}
                                        <input type="hidden" name="id" value="{{ $package['id'] }}">
                                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input class="form-control"  type="text" name="cheque_number" data-parlsey-required="true" data-parsley-required-message="Check number is required">
                                                    @if ($errors->has('cheque_number'))
                                                    <p class="help-block"> <strong>{{ $errors->first('cheque_number') }}</strong> </p>
                                                    @endif
                                                    <label>Check Number </label>
                                                    <span></span>	
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <textarea class="payment-bank" id="bankdetails" name="bankdetails"></textarea>
                                                    @if ($errors->has('bankdetails'))
                                                    <p class="help-block"> <strong>{{ $errors->first('bankdetails') }}</strong> </p>
                                                    @endif
                                                    <label>Bank Details </label>
                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-bottom-btn text-right">
                                                <button type="submit" name="continue" class="btn btn-primary custom-btn customc-btn" value="submit"><span>Pay</span></button>
                                                 <a class="btn btn-primary custom-btn customb-btn not-empty" href="{{url('/home')}}"><span>Back to Dashboard</span></a>
                                            </div>
                                        </div>
                                    </form>

                                </div> -->
                            </div>
                        </div>
                        <!-- </form> -->
                    </div>
                </div>
                <!-- <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="form-bottom-btn text-right">
                        <a class="btn btn-primary custom-btn customb-btn"><span>Back</span></a>
                        <button type="submit" name="continue" class="btn btn-primary custom-btn customc-btn" VALUE="Continue"><span>Pay</span></button>
                    </div>
                    </div> -->
            </div>
        </div>
        @stop

        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        @section('frontend_js')
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script type="text/javascript">
$(document).ready(function () {
    $('input,textarea,select').on('focus', function(e) {
            e.preventDefault();
            $(this).attr("autocomplete", "nope"); 
            $('#credit_card_name').attr("autocomplete", "off"); 
            $('input[name=cheque_number]').attr("autocomplete", "off"); 

 
        });
    $("#datepicker").datepicker({changeMonth: true,
        changeYear: true, dateFormat: 'mm/yy', minDate: 0, showButtonPanel: true, onClose: function (dateText, inst) {
            $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
            $('#datepicker').addClass('not-empty');
        }});
    $('#card_number').mask("9999 9999 9999 9999 999");

    $('#card_type').change(function () {
        var sel = $(this).val();
        console.log(sel);
        switch (sel)
        {
            case 'American Express':
                $('#card_number').attr('minlength', '15');
                $('#card_number').attr('maxlength', '15');
                $('#card_number').mask("9999 9999 9999 9999 999");
                break;
            case 'Mastercard':
                $('#card_number').attr('minlength', '12');
                $('#card_number').attr('maxlength', '16');
                $('#card_number').mask("9999 9999 9999 9999");
                break;
            case 'Visa':
                $('#card_number').attr('minlength', '12');
                $('#card_number').attr('maxlength', '16');
                $('#card_number').mask("9999 9999 9999 9999");
                break;
            case 'Maestro':
                $('#card_number').attr('minlength', '12');
                $('#card_number').attr('maxlength', '19');
                $('#card_number').mask("9999 9999 9999 9999 999");
                break;
            case 'Discover Card':
                $('#card_number').attr('minlength', '16');
                $('#card_number').attr('maxlength', '19');
                $('#card_number').mask("9999 9999 9999 9999 9999 999");
                break;
            default:
                $('#card_number').attr('minlength', '12');
                $('#card_number').attr('maxlength', '16');
        }
    })
    $('#submit_card_button').click(function () {
        var valid_form = $('form').parsley().validate();
        if (valid_form == true)
        {
            $('#credit-card-form').submit();
        }
    })
    $('ul.nav.nav-tabs.radio-tabs li').click(function () {
        $('input[type="radio"]').attr('checked', false);
        $(this).find('input[type="radio"]').attr('checked', true);
    });

    $.fn.betta_pw_fld = function(pwfld_sel, hiddenfld_sel) {

        // this is the form the plugin is called on
        $(this).each(function() {

        // the plugin accepts the css selector of the pw field (pwfld_sel)
        var pwfld = $(this).find(pwfld_sel);
        
        // on keyup, do the masking visually while filling a field for actual use
        pwfld.off('keyup.betta_pw_fld');


        pwfld.on('keyup.betta_pw_fld', function() {
            var pchars = $(this).val().replace(/[^\d\*]/g, '');
            if (pchars == '') return;

            // we'll need the hidden characters too (for form submission)
            var hiddenfld = $(this).parents('form').find(hiddenfld_sel);
            var hchars = hiddenfld.val();

            var newval = '';
            var newhpw = '';
    
            for (i=0; i<pchars.length; i++) {
                if (pchars[i] == '*') {
                    newhpw += hchars[i];
                } else {
                    newhpw += pchars[i];
                }
                newval += '*';
            }
    
            // set the updated (masked), visual pw field
            $(this).val(newval);

            // keep the pw hidden and ready for form submission in a hidden input
            hiddenfld.val(newhpw);
        });
    });
}
$('#credit-card-form').betta_pw_fld('.stylish_security_code', '.hidden_security_code');

});

$('#card_id').click(function (){
    console.log($('#card_id').val());
    $('#credit-card-form').parsley().destroy();

});

        </script>
        @stop
