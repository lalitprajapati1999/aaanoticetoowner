@extends('adminlte::page')

@section('banner')
<section class="sec-banner deadline-banner"></section>
@stop

@section('content')

<section class="dashboard-wrapper deadline-calculator deadline-dashboard">
    <div class="row">
        <div class="col-md-12">
            <div class="section-heading dashboard-heading text-center">
                <h2 class="section-title">Deadline Calculator</h2>
                  <p class="section-text">Don't wait get paid<br/>
                      Protect your payment by sending notice to your jobs</p>
            </div>
        </div>
    </div>
    <div class="dashboard-inner-body">
        <div class="dash-subform">
            <div class="row">
                <div class="deadline-body">
                    <form id="deadline_calculator" data-parsley-validate="" class="calculator" action="{{url('customer/calculate-date')}}" method="post">
                            <meta name="csrf-token" content="{{ csrf_token() }}">
                            {!! csrf_field() !!}
                            <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="inner-wrap">
                                    <h2>STATE</h2>
                                    <div class="select-opt">
                                        <label for="textinput" class="control-label">Specify State<span class="mandatory-field">*</span></label>
                                        <select name="state" class="selectpicker" required data-parsley-required-message="State is required" data-parsley-trigger="change focusout" id="state">
                                            <option value="">Select</option>
                                            @foreach($states As $state)
                                            <option value="{{$state->id}}" @if (old('state') == $state->id) selected="selected" @endif > {{ucfirst(trans($state->name))}}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('state'))
                                        <p class="help-block">
                                            <strong>{{ $errors->first('state') }}</strong>
                                        </p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="inner-wrap">
                                    <h2>Document Type</h2>
                                    <div class="select-opt">
                                        <label for="textinput" class="control-label">Select Type<span class="mandatory-field">*</span></label>
                                        <select name="document_type" class="selectpicker" required data-parsley-required-message="Document type is required" data-parsley-trigger="change focusout" id="notices">
                                            <option value="">Select</option>
                                        </select>
                                        @if ($errors->has('document_type'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('document_type') }}</strong>
                                        </span>
                                        @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="Job-day">
                            <div class="row">
                                <div class="col-md-12">
                                    <h2 class="firstjob"></h2>
                                    <div class="row">
                                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6">
                                            <div class="inner-wrap-small">
                                                <div class="select-opt">
                                                    <label for="textinput" class="control-label">Month<span class="mandatory-field">*</span></label>
                                                    <select name="month" class="selectpicker" required data-parsley-required-message="Month is required" data-parsley-trigger="change focusout">
                                                        <option value="">Select</option>
                                                        <option value="01">Jan</option>
                                                        <option value="02">Feb</option>
                                                        <option value="03">Mar</option>
                                                        <option value="04">Apr</option>
                                                        <option value="05">May</option>
                                                        <option value="06">June</option>
                                                        <option value="07">Jul</option>
                                                        <option value="08">Aug</option>
                                                        <option value="09">Sep</option>
                                                        <option value="10">Oct</option>
                                                        <option value="11">Nov</option>
                                                        <option value="12">Dec</option>
                                                    </select>
                                                    @if ($errors->has('month'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('month') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6">
                                            <div class="inner-wrap-small">
                                                <div class="select-opt">
                                                    <label for="textinput" class="control-label">Day<span class="mandatory-field">*</span></label>
                                                    <select name="day" class="selectpicker" required data-parsley-required-message="Day is required" data-parsley-trigger="change focusout">
                                                        <option value="">Select</option>
                                                        <?php for ($i = 1; $i <= 31; $i++) { ?>
                                                            <option value="{{$i}}" @if (old('day') == $i) selected="selected" @endif>{{$i}}</option>
                                                        <?php } ?>
                                                    </select>
                                                    @if ($errors->has('day'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('day') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-6">
                                            <div class="inner-wrap-small">
                                                <div class="select-opt">
                                                    <label for="textinput" class="control-label">Year<span class="mandatory-field">*</span></label>
                                                    <select name="year" class="selectpicker" required data-parsley-required-message="Year is required" data-parsley-trigger="change focusout">
                                                        <option value="">Select</option>
                                                        <option value="2018">2018</option>
                                                        <option value="2019">2019</option>
                                                        <option value="2020">2020</option>
                                                        <option value="2021">2021</option>
                                                        <option value="2022">2022</option>
                                                        <option value="2023">2023</option>
                                                        <option value="2024">2024</option>
                                                    </select>
                                                    @if ($errors->has('year'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('year') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-bottom-btn">
                                                <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>Calculate Date</span></button>
                                                <p class="note">Note : This calculation includes weekends and federal holidays.<a href="#">View Holidays</a></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-----------------page body ends------------------>

@stop
@section('frontend_js')
<script>
    new WOW().init();
    $('#state').on('change',function(){
         $('#notices').find('option').remove().end().append('<option value="">Select</option>');
         
        $("#notices").selectpicker("refresh");
            $.ajax({
            data: {'state': $(this).val()},
            type: "post",
            url: "{{url('customer/deadline-calculator/notices') }}",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
//             var result = $.parseJSON(response);
           //  console.log(result);
                
               $.each(response,function(e,val) {
                    $("#notices").append("<option value='"+val.id+"'>"+val.name+"</option>");
                    $("#notices").selectpicker("refresh");
                });
               
            }
        });
    });
</script>
<script type="text/javascript">
    
    $(document).ready(function () {
        $('#notices').on('change',function(){
           
            if($('#notices').val()== '9'){
                $('.firstjob').html('First Day On The Job*');
            }else if($('#notices').val()== '3'){
                $('.firstjob').html('Last Day On The Job*');
            }else if($('#notices').val()== ''){
                $('.firstjob').html('');
            }
        })
        $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('#back-to-top').click(function () {
            $('#back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });

        $('#back-to-top').tooltip('show');
        $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('.navbar-brand').addClass('fixed-brand').css('transition', '0.3 all ease-in-out 0s');
            } else {
                $('.navbar-brand').removeClass('fixed-brand');
            }
        });
        
        
    });
</script>
@stop