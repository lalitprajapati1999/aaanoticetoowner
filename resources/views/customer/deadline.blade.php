@extends('adminlte::page')

@section('banner')
<section class="sec-banner deadline-banner"></section>
@stop

@section('content')
{!! csrf_field() !!}

<section class="section-wrapper deadline-calculator">
    <div class="main-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="section-heading">
                    <h1 class="section-title">Deadline Calculator</h1>
                    <h2 class="section-subpara">Deadline</h2>
                </div>
            </div>
        </div>
        <div class="deadline-body">
            <div class="dahboard-table">
            <table id="deadlineTable" class="table table-striped table-hover display ">
                <thead class="thead-dark">
                    <tr class="bg-primary">
                        <th data-orderable="false">State</th>
                        <th data-orderable="false">Document</th>  
                        <th data-orderable="false">Std working Span</th> 
                        @if($document_id == 3)
                        <th data-orderable="false">Last day on the Job</th>
                        @elseif($document_id == 9)
                         <th data-orderable="false">First day on the Job</th>
                         @else
                          <th data-orderable="false">Job Start Date</th>
                          @endif
<!--                        <th data-orderable="false">Last Day on Job</th>-->
                        <th data-orderable="false"> Due Date</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-primary">{{$state->name}}</td>
                        <td class="text-primary">{{$document_type->name}}</td>
                        <td class="text-primary">{{$days.' Days'}}</td>
                        <td class="text-primary">{{$job_start_date}}</td>
<!--                        <td class="text-primary">{{$last_day_on_job}}</td>-->
                        <td class="text-primary">{{$due_date}}</td>   
                    </tr>
                </tbody>
            </table>
            </div>
        </div>
    </div>
</section>
<!-----------------page body ends------------------>

@stop
@section('frontend_js')
<script>
    new WOW().init();
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('#back-to-top').click(function () {
            $('#back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });

        $('#back-to-top').tooltip('show');
        $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('.navbar-brand').addClass('fixed-brand').css('transition', '0.3 all ease-in-out 0s');
            } else {
                $('.navbar-brand').removeClass('fixed-brand');
            }
        });
    });
</script>
@stop