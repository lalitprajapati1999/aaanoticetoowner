@if(\Request::ajax())
    <form action="{{url('customer/save-clerk-court-recorded-date/0')}}" method="POST">
        {!! csrf_field() !!}

        <input type="hidden" class="add_node_work_order_id" name="wo_id" id="ccrd_wo_id" value="{{$workOrder->id}}">    
        <input type="hidden" class="add_node_work_order_id" name="wo_field_id" id="ccrd_wo_field_id" value="{{$workOrderFieldId}}">    
        <input type="hidden" class="add_node_work_order_id" name="notice_field_id" id="ccrd_notice_field_id" value="{{$noticeFieldId}}">    
        <input type="hidden" class="add_node_work_order_id" name="notice_id" id="ccrd_notice_id" value="{{$notice_id}}">    
        <div class="input-wrapper full-width">
            <div class="col-md-12 col-sm-12">
            <h4>For {{$noticeType}}, #{{$workOrder->id}}</h4>
                <div class="input-wrapper full-width">
                    <div class="styled-input">

                        <input class="form-control datepicker not-empty" id="ccrd" type="text" name="ccrd" value="{{$clerCourtRecordedDate}}">

                        <label>Select Clerk of Court Recorded Date<span class="mandatory-field">*</span></label>
                        <p></p>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="form-bottom-btn">
                    <button type="submit" id="add-ccrd-submit" class="btn btn-primary custom-btn customc-btn modal-submit"><span>Save</span></button>
                </div>
            </div>
        </div>
    </form>
@else
    <div id="modal_ccrd" class="modal fade register-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"> Add Last Date On The Job</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div class="alert alert-success" id="modal-success" style="display: none">
                    </div>
                </div>
                <div class="modal-body">
                Loading
                </div>
            </div>
        </div>
    </div>
@endif