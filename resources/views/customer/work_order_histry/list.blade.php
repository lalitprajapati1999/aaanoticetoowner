<style type="text/css">
  .fixhead-table .dataTables_wrapper .dataTables_scroll div.dataTables_scrollBody {
   margin-top: 00px !important;;
    }
    
</style>
@extends('adminlte::page')
@section('content')
<div  id="vieworder">
    <section class="view-order">
        <div class="dashboard-wrapper">
            @if (Session::get('success'))
            <div class="alert alert-success">
                <?php echo Session::get('success'); ?>
            </div>
            @endif
            <div class="dashboard-heading">
                <h1><span>WORK ORDER HISTORY</span></h1>

            </div>
            <div class="dashboard-inner-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="search-by">
                            <div class="form-group full-width">
                                <label for="">Search By: </label>
                                <ul class="nav nav-tabs" role="tablist" id="tabs">
                                    <li role="presentation" class="">
                                        <a href="#1" aria-controls="home" role="tab" data-toggle="tab">
                                            <div class="form-btn-div">
                                                <span class="btn btn-primary custom-btn form-btn ">Date Range</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li role="presentation">
                                        <a href="#2" aria-controls="profile" role="tab" data-toggle="tab">
                                            <div class="form-btn-div">
                                                <span class="btn btn-primary custom-btn form-btn">Work Status</span>
                                            </div>
                                        </a>
                                    </li>

                                    <li role="presentation">
                                        <a href="#3" aria-controls="messages" role="tab" data-toggle="tab">
                                            <div class="form-btn-div">
                                                <span class="btn btn-primary custom-btn form-btn">Customer</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li role="presentation">
                                        <a href="#4" aria-controls="settings" role="tab" data-toggle="tab">
                                            <div class="form-btn-div">
                                                <span class="btn btn-primary custom-btn form-btn">Project</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li role="presentation">
                                        <a href="#5" aria-controls="settings" role="tab" data-toggle="tab">
                                            <div class="form-btn-div">
                                                <span class="btn btn-primary custom-btn form-btn">Hide or Show Options</span>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="tab-content">
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <div class="form-btn-div full-width">
                                            <button id="" class="btn btn-primary custom-btn form-btn" onclick="reset()"><span>Reset</span></button>
                                        </div>
                                    </div>
                                </div>
                                <!----------------date range starts---------------->
                                <div role="tabpanel" class="tab-pane active" id="1">
                                    <div class="col-md-12">
                                        <div class="calender-div">
                                            <form class="date-box display-ib date "  >
                                                <div class="form-group">
                                                    <div class="input-wrapper full-width">
                                                        <div class="styled-input ">
                                                            <input class="form-control " required="" type="text" id="from-date" name="from_date" @if(isset(Session::get('view-work-order')['from_date'])) value="{{Session::get('view-work-order')['from_date']}}" @endif autocomplete="off"> 
                                                            <!-- <label>Start Date</label> -->
                                                            <span></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <button type="button" class="btn btn-default"><span class="icon-calendar"></span></button>
                                            </form>
                                            <p class="display-ib">To</p>
                                            <form class="date-box display-ib date " >
                                                <div class="form-group">
                                                    <div class="input-wrapper full-width">
                                                        <div class="styled-input">
                                                            <input class="form-control  " required="" type="text" name="to_date" @if(isset(Session::get('view-work-order')['to_date'])) value="{{Session::get('view-work-order')['to_date']}}" @endif id="to-date" autocomplete="off">
                                                            <!-- <label>End Date</label> -->
                                                            <span></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <button type="button" class="btn btn-default"><span class="icon-calendar"></span></button>
                                            </form>

<!--<button class="btn btn-search" type="submit" data-original-title="" title=""><span class="icon-search"></span></button>-->
                                        </div>
                                    </div>
                                </div>
                                <!----------------date range ends---------------->
                                <!----------------work status starts---------------->
                                <div role="tabpanel" class="tab-pane" id="2">
                                    <div class="col-md-10">
                                        <div class="work-status full-width">
                                            <ul class="checkbox-holder">
                                                <li><a href="#"><input type="checkbox" id="checkbox-1-1" class="regular-checkbox" value="1" name="status" onchange="filterStatus()" @if(isset(Session::get('view-work-order')['types']) && in_array(1, Session::get('view-work-order')['types']))) checked @endif /><label for="checkbox-1-1">Request</label></a></li>
                                                <li><a href="#"><input type="checkbox" id="checkbox-1-2" class="regular-checkbox" value="6" name="status" onchange="filterStatus()" @if(isset(Session::get('view-work-order')['types']) && in_array(6, Session::get('view-work-order')['types']))) checked @endif /><label for="checkbox-1-2">Completed</label></a></li>
                                                <li><a href="#"><input type="checkbox" id="checkbox-1-3" class="regular-checkbox" value="2" name="status" onchange="filterStatus()" @if(isset(Session::get('view-work-order')['types']) && in_array(2, Session::get('view-work-order')['types']))) checked @endif /><label for="checkbox-1-3">Processing</label></a></li>
                                                <li><a href="#"><input type="checkbox" id="checkbox-1-5" class="regular-checkbox" value="5" name="status" onchange="filterStatus()" @if(isset(Session::get('view-work-order')['types']) && in_array(5, Session::get('view-work-order')['types']))) checked @endif /><label for="checkbox-1-5">Mailing</label></a></li>
                                                <li><a href="#"><input type="checkbox" id="checkbox-1-6" class="regular-checkbox" value="7" name="status" onchange="filterStatus()" @if(isset(Session::get('view-work-order')['types']) && in_array(7, Session::get('view-work-order')['types']))) checked @endif /><label for="checkbox-1-6">Cancelled</label></a></li>
                                                <li><a href="#"><input type="checkbox" id="checkbox-1-7" class="regular-checkbox" value="4" name="status" onchange="filterStatus()" @if(isset(Session::get('view-work-order')['types']) && in_array(4, Session::get('view-work-order')['types']))) checked @endif /><label for="checkbox-1-7">Pending Signature</label></a></li>
                                                <li><a href="#"><input type="checkbox" id="checkbox-1-8" class="regular-checkbox" value="0"  name="status" onchange="filterStatus()" @if(isset(Session::get('view-work-order')['types']) && in_array(0, Session::get('view-work-order')['types']))) checked @endif /><label for="checkbox-1-8">Draft</label></a></li>
                                                <li><a href="#"><input type="checkbox" id="checkbox-1-9" class="regular-checkbox" value="1"  name="type" onchange="doc_type()" @if(isset(Session::get('view-work-order')['doc_type'])) && Session::get('view-work-order')['doc_type']==1)  checked @endif /><label for="checkbox-1-9">Hard Document</label></a></li>

      <!--<li><a href="#"><input type="checkbox" id="checkbox-1-9" class="regular-checkbox" value="3" name="status" onchange="filterStatus()"/><label for="checkbox-1-9">Recording</label></a></li>
      <li><a href="#"><input type="checkbox" id="checkbox-1-9" class="regular-checkbox" value="8" name="status" onchange="filterStatus()"/><label for="checkbox-1-9">Restricted</label></a></li>-->
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!----------------work status ends---------------->
                                <!----------------customer starts---------------->
                                <div role="tabpanel" class="tab-pane" id="3">
                                    <div class="col-md-8">
                                        <div class="customer full-width">
                                            <!--                                 <div class="select-opt">
                                                                                <label class="col-md-12 control-label" for="textinput">Customer</label> 
                                                                                <select class="selectpicker customer_name" name="customer_name" id="customer_name" >
                                                                                   <option>Select</option>
                                                                                   @foreach($customers as $customer)
                                                                                   <option value="{{$customer->name}}">{{$customer->name}}</option>
                                                                                   @endforeach
                                                                                </select>
                                                                             </div>-->
                                            <!-- {{-- <div class="input-wrapper">
                                                <div class="styled-input">
                                                    <input class="form-control contractedby" required="" type="text" id="customer_cst" @if (Session::get('view-work-order')['customer_cst']) value="{{Session::get('view-work-order')['customer_cst']}}" @endif>
                                                    <label>Customer's Customer</label>
                                                    <span></span>
                                                </div>
                                            </div> --}} -->
                                            
                                            <div class="select-opt clear-b">
                                                <label class="col-md-12 control-label" for="textinput">Customer's Customer</label> 
                                                <select class="selectpicker" id="customer_cst" name="customer_cst">
                                                    <option value="">Select Customer's Customer</option>
                                                    @foreach($contracted_by as $manager)
                                                    <option value="{{str_replace('/','\\/',$manager->company_name)}}" @if(isset(Session::get('view-work-order')['customer_cst']) && Session::get('view-work-order')['customer_cst']==str_replace('/','\\/',$manager->company_name))  selected @endif>{{$manager->company_name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="input-wrapper">
                                                <div class="styled-input">
                                                    <input class="form-control" required="" type="text" id="general_contractor" @if (isset(Session::get('view-work-order')['general_contractor'])) value="{{Session::get('view-work-order')['general_contractor']}}" @endif>
                                                    <label>Genral Contractor</label>
                                                    <span></span>
                                                </div>
                                            </div>
                                            <div class="select-opt clear-b">
                                                <label class="col-md-12 control-label" for="textinput">Account Manager</label> 
                                                <select class="selectpicker" id="account_manager" name="account_manager">
                                                    <option value="">Select Account Manager</option>
                                                    @foreach($account_managers as $manager)
                                                    <option value="{{$manager->name}}" @if(isset(Session::get('view-work-order')['account_manager']) && Session::get('view-work-order')['account_manager']==$manager->name)  selected @endif>{{$manager->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="4">
                                    <div class="col-md-8">
                                        <div class="customer project full-width">
                                            <form action="#" method="post" id="reg-form">
                                                <div class="col-md-6">
                                                    <div class="input-wrapper full-width">
                                                        <div class="styled-input">
                                                            <input class="form-control projectInfo" id="work_order_id" required="" type="text" name="workorder_id" @if (isset(Session::get('view-work-order')['work_order_id'])) value="{{Session::get('view-work-order')['work_order_id']}}" @endif/>
                                                            <label>Work Order No</label>
                                                            <span></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="input-wrapper full-width">
                                                        <div class="styled-input">
                                                            <input class="form-control projectInfo" name="parent_work_order" id="parent_work_order" required="" type="text" @if (isset(Session::get('view-work-order')['parent_work_order'])) value="{{Session::get('view-work-order')['parent_work_order']}}" @endif>
                                                            <label>Parent work order</label>
                                                            <span></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--                                    <div class="col-md-6">
                                                                                       <div class="select-opt">
                                                                                          <label class="col-md-12 control-label" for="textinput">Customer</label> 
                                                                                          <select class="selectpicker customer_name" name="customer_name" id="customer_name" >
                                                                                               <option>Select</option>
                                                                                             @foreach($customers as $customer)
                                                                                             <option value="{{$customer->name}}">
                                                                                                {{$customer->name}}
                                                                                             </option>
                                                                                             @endforeach
                                                                                          </select>
                                                                                       </div>
                                                                                    </div>-->
                                             <!--    <div class="col-md-6">
                                                    <div class="input-wrapper">
                                                        <div class="styled-input">
                                                            <input class="form-control contractedby" required="" type="text">
                                                            <label>Customer's Customer</label>
                                                            <span></span>
                                                        </div>
                                                    </div>
                                                </div> -->
                                                <div class="col-md-6">
                                                    <div class="select-opt">
                                                        <label class="col-md-12 control-label" for="textinput">State</label> 
                                                        <select class="selectpicker notice_state" name="notice_state" id="notice_state" class="notice_state"  @if (isset(Session::get('view-work-order')['notice_state'])) value="{{Session::get('view-work-order')['notice_state']}}" @endif />
                                                            <option>Select</option>
                                                            @foreach($states as $state)
                                                            <option value="{{$state->name}}" @if(isset(Session::get('view-work-order')['notice_state']) && Session::get('view-work-order')['notice_state']==$state->name)  selected @endif>{{$state->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </form>
                                            <div class="col-md-6">
                                                <div class="input-wrapper full-width">
                                                    <div class="styled-input">
                                                        <input class="form-control projectInfo" name="project_address" id="project_address" required="" type="text" @if (isset(Session::get('view-work-order')['project_address'])) value="{{Session::get('view-work-order')['project_address']}}" @endif />
                                                        <label>Project Address</label>
                                                        <span></span>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>
                                <!----------------customer ends---------------->
                                <!----------------project starts---------------->
                                <!----------------project ends---------------->
                                <!----------------hide or show options starts---------------->
                                <?php   $tableCol = array('wo_id'=> 0,'company_name'=> 1, 'proj_address'=> 2,'cont_by'=> 3, 'submitted_date'=> 4, 'job_start_date'=> 5,'clerk_date'=> 6,'due_date'=> 7,'account_manager'=> 8,'cust'=> 9,'state'=> 10,'status'=> 11, 'notice_type'=> 12, 'parent_wo'=> 13,'amnt_due'=> 14, 'owner'=> 15,'general_cont'=> 16, 'option'=> 17, 'doc_type'=> 18,'user_name'=>19);
                                ?>
                                <div role="tabpanel" class="tab-pane" id="5">
                                    <div class="col-md-12">
                                        <div class="hide-opt full-width">
                                            <div class="col-md-3 col-sm-6">
                                                <div class="toggle-btn full-width">
                                                    <div class="onoffswitch display-ib">
                                                        <a class="toggle-vis" data-column="{{$tableCol['proj_address']}}">
                                                            <label class="switch" id="project-add">
                                                                <input type="checkbox" id="hid-show-{{$tableCol['proj_address']}}" name="hide-show" value="{{$tableCol['proj_address']}}" @if(isset(Session::get('view-work-order')['hide_show']) && in_array($tableCol['proj_address'], Session::get('view-work-order')['hide_show'])) checked @endif >
                                                                <span class="slider round"></span>
                                                            </label>
                                                        </a>
                                                    </div>
                                                    <p class="display-ib toggle-text">Project Address</p>

                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6">
                                                <div class="toggle-btn full-width">
                                                    <div class="onoffswitch display-ib">
                                                        <a class="toggle-vis" data-column="{{$tableCol['submitted_date']}}">
                                                            <label class="switch" id="submitted-date">
                                                                <input type="checkbox" id="hid-show-{{$tableCol['submitted_date']}}" name="hide-show" value="{{$tableCol['submitted_date']}}" @if(isset(Session::get('view-work-order')['hide_show']) && in_array($tableCol['submitted_date'], Session::get('view-work-order')['hide_show'])) checked @endif >
                                                                <span class="slider round"></span>
                                                            </label>
                                                        </a>
                                                    </div>
                                                    <p class="display-ib toggle-text">Submitted Date</p>
                                                </div>
                                            </div> 
                                            <div class="col-md-3 col-sm-6">
                                                <div class="toggle-btn full-width">
                                                    <div class="onoffswitch display-ib">
                                                        <a class="toggle-vis" data-column="{{$tableCol['parent_wo']}}">
                                                            <label class="switch" id="project-parent">
                                                                <input type="checkbox" id="hid-show-{{$tableCol['parent_wo']}}" name="hide-show" value="{{$tableCol['parent_wo']}}" @if(isset(Session::get('view-work-order')['hide_show']) && in_array($tableCol['parent_wo'], Session::get('view-work-order')['hide_show'])) checked @endif>
                                                                <span class="slider round"></span>
                                                            </label>
                                                        </a>
                                                    </div>
                                                    <p class="display-ib toggle-text">Parent Work Order</p>
                                                </div>
                                            </div>

                                            <div class="col-md-3 col-sm-6">
                                                <div class="toggle-btn full-width">
                                                    <div class="onoffswitch display-ib">
                                                        <a class="toggle-vis" data-column="{{$tableCol['notice_type']}}">
                                                            <label class="switch" id="project-template">
                                                                <input type="checkbox" id="hid-show-{{$tableCol['notice_type']}}" name="hide-show" value="{{$tableCol['notice_type']}}" @if(isset(Session::get('view-work-order')['hide_show']) && in_array($tableCol['notice_type'], Session::get('view-work-order')['hide_show'])) checked @endif >
                                                                <span class="slider round"></span>
                                                            </label>
                                                        </a>
                                                    </div>
                                                    <p class="display-ib toggle-text">Notice Type</p>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 clear-b">
                                                <div class="toggle-btn full-width">
                                                    <div class="onoffswitch display-ib">
                                                        <a class="toggle-vis" data-column="{{$tableCol['job_start_date']}}">
                                                            <label class="switch" id="project-start">
                                                                <input type="checkbox" id="hid-show-{{$tableCol['job_start_date']}}" name="hide-show" value="{{$tableCol['job_start_date']}}" @if(isset(Session::get('view-work-order')['hide_show']) && in_array($tableCol['job_start_date'], Session::get('view-work-order')['hide_show'])) checked @endif >
                                                                <span class="slider round"></span>

                                                            </label>
                                                        </a>
                                                    </div>
                                                    <p class="display-ib toggle-text">Job Start Date</p>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6">
                                                <div class="toggle-btn full-width">
                                                    <div class="onoffswitch display-ib">
                                                        <a class="toggle-vis" data-column="{{$tableCol['amnt_due']}}">
                                                            <label class="switch" id="project-amount">
                                                                <input type="checkbox" id="hid-show-{{$tableCol['amnt_due']}}" name="hide-show" value="{{$tableCol['amnt_due']}}" @if(isset(Session::get('view-work-order')['hide_show']) && in_array($tableCol['amnt_due'], Session::get('view-work-order')['hide_show'])) checked @endif>
                                                                <span class="slider round"></span>

                                                            </label>
                                                        </a>
                                                    </div>
                                                    <p class="display-ib toggle-text">Amount Due To Customer</p>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6">
                                                <div class="toggle-btn full-width">
                                                    <div class="onoffswitch display-ib">
                                                        <a class="toggle-vis" data-column="{{$tableCol['due_date']}}">
                                                            <label class="switch" id="project-duedate">
                                                                <input type="checkbox" id="hid-show-{{$tableCol['due_date']}}" name="hide-show" value="{{$tableCol['due_date']}}" @if(isset(Session::get('view-work-order')['hide_show']) && in_array($tableCol['due_date'], Session::get('view-work-order')['hide_show'])) checked @endif>
                                                                <span class="slider round"></span>
                                                            </label>
                                                        </a>
                                                    </div>
                                                    <p class="display-ib toggle-text">Due Date</p>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6">
                                                <div class="toggle-btn full-width">
                                                    <div class="onoffswitch display-ib">
                                                        <a class="toggle-vis" data-column="{{$tableCol['owner']}}">
                                                            <label class="switch" id="project-owner">
                                                                <input type="checkbox" id="hid-show-{{$tableCol['owner']}}" name="hide-show" value="{{$tableCol['owner']}}" @if(isset(Session::get('view-work-order')['hide_show']) && in_array($tableCol['owner'], Session::get('view-work-order')['hide_show'])) checked @endif>
                                                                <span class="slider round"></span>
                                                            </label>
                                                        </a>
                                                    </div>
                                                    <p class="display-ib toggle-text">Owner</p>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6 clear-b">
                                                <div class="toggle-btn full-width">
                                                    <div class="onoffswitch display-ib">
                                                        <a class="toggle-vis" data-column="{{$tableCol['general_cont']}}">
                                                            <label class="switch" id="project-gen">
                                                                <input type="checkbox" id="hid-show-{{$tableCol['general_cont']}}" name="hide-show" value="{{$tableCol['general_cont']}}" @if(isset(Session::get('view-work-order')['hide_show']) && in_array($tableCol['general_cont'], Session::get('view-work-order')['hide_show'])) checked @endif >
                                                                <span class="slider round"></span>
                                                            </label>
                                                        </a>
                                                    </div>
                                                    <p class="display-ib toggle-text">General Contractor</p>
                                                </div>
                                            </div>

                                            <!--  <div class="col-md-3 col-sm-6">
                                                <div class="toggle-btn full-width">
                                                   <div class="onoffswitch display-ib">
                                                      <label class="switch" id="project-exclude">
                                                      <input type="checkbox" checked>
                                                      <span class="slider round"></span>
                                                      </label>
                                                   </div>
                                                   <p class="display-ib toggle-text">Exclude Texas</p>
                                                </div>
                                             </div> -->
                                            <!--  <div class="col-md-3 col-sm-6">
                                                <div class="toggle-btn full-width">
                                                   <div class="onoffswitch display-ib">
                                                      <label class="switch" id="project-state">
                                                      <input type="checkbox" checked>
                                                      <span class="slider round"></span>
                                                      </label>
                                                   </div>
                                                   <p class="display-ib toggle-text">State</p>
                                                </div>
                                             </div> -->
                                            <!-- <div class="col-md-3 col-sm-6 clear-b">
                                               <div class="toggle-btn full-width">
                                                  <div class="onoffswitch display-ib">
                                                     <label class="switch" id="project-tempname">
                                                     <input type="checkbox" checked>
                                                     <span class="slider round"></span>
                                                     </label>
                                                  </div>
                                                  <p class="display-ib toggle-text">Template Name</p>
                                               </div>
                                            </div> -->
                                            <div class="col-md-3 col-sm-6"">
                                                <div class="toggle-btn full-width">
                                                    <div class="onoffswitch display-ib">
                                                        <a class="toggle-vis" data-column="{{$tableCol['cont_by']}}">
                                                            <label class="switch" id="project-contby">
                                                                <input type="checkbox" id="hid-show-{{$tableCol['cont_by']}}" name="hide-show" value="{{$tableCol['cont_by']}}" @if(isset(Session::get('view-work-order')['hide_show']) && in_array($tableCol['cont_by'], Session::get('view-work-order')['hide_show'])) checked @endif>
                                                                <span class="slider round"></span>
                                                            </label>
                                                        </a>
                                                    </div>
                                                    <p class="display-ib toggle-text">Contracted By</p>
                                                </div>
                                            </div>
                                            <!-- <div class="col-md-3 col-sm-6"">
                                               <div class="toggle-btn full-width">
                                                  <div class="onoffswitch display-ib">
                                                     <label class="switch" id="project-acc">
                                                     <input type="checkbox" checked>
                                                     <span class="slider round"></span>
                                                     </label>
                                                  </div>
                                                  <p class="display-ib toggle-text">Account</p>
                                               </div>
                                            </div> -->
                                            <div class="col-md-3 col-sm-6">
                                                <div class="toggle-btn full-width">
                                                    <div class="onoffswitch display-ib">
                                                        <a class="toggle-vis" data-column="{{$tableCol['account_manager']}}">
                                                            <label class="switch" id="project-accman">
                                                                <input type="checkbox" id="hid-show-{{$tableCol['account_manager']}}" name="hide-show" value="{{$tableCol['account_manager']}}" @if(isset(Session::get('view-work-order')['hide_show']) && in_array($tableCol['account_manager'], Session::get('view-work-order')['hide_show'])) checked @endif>
                                                                <span class="slider round"></span>
                                                            </label>
                                                        </a>
                                                    </div>
                                                    <p class="display-ib toggle-text">Account Manager</p>
                                                </div>
                                            </div>
                                            <div class="col-md-3 col-sm-6">
                                                <div class="toggle-btn full-width">
                                                    <div class="onoffswitch display-ib">
                                                        <a class="toggle-vis" data-column="{{$tableCol['state']}}">
                                                            <label class="switch" id="project-state">
                                                                <input type="checkbox" id="hid-show-{{$tableCol['state']}}" name="hide-show" value="{{$tableCol['state']}}" @if(isset(Session::get('view-work-order')['hide_show']) && in_array($tableCol['state'], Session::get('view-work-order')['hide_show'])) checked @endif>
                                                                <span class="slider round"></span>
                                                            </label>
                                                        </a>
                                                    </div>
                                                    <p class="display-ib toggle-text">State</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!----------------hide or show options ends---------------->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="dahboard-table table-responsive fixhead-table workorder-table">
                            {{-- {!! $html->table() !!} --}}
                            <table class="table table-striped " id="datatable"  width="100%" cellspacing="0" datatable="" width="100%" cellspacing="0"   data-scroll-x="true"scroll-collapse="false">
                                <thead class="thead-dark">
                                    <tr>
                                        <th scope="col">Work Order</th>
                                        <th scope="col" class="pro-customer">Company Name</th>
                                        <th scope="col">Project Address</th>
                                        <th scope="col" class="pro-contby">Contracted By</th>
                                         <th scope="col" class="submit-date">Submitted Date</th>
                                        <th scope="col">Job Start Date</th>
                                        <th scope="col">Recorded Date</th>
                                        <th scope="col" class="pro-duedate">Due Date</th>
                                        
                                        <th scope="col" class="pro-accman">Account Manager</th>
                                        <th scope="col" class="pro-accman">Customer</th>
                                        <th scope="col" class="pro-state">State</th>
                                      
                                        <th scope="col" class="pro-status">Status</th>
                                        <th scope="col" class="pro-temp">Notice Type</th>
                                        <!-- <th scope="col" class="pro-order">Work Order No</th> -->
                                        <th scope="col" class="pro-parent">Parent Work Order</th>
                                        <th scope="col" class="pro-due">Amount Due To Customer</th>
                <!--                                        <th scope="col" class="pro-duedate">Due Date Immidate</th>-->
                                        <th scope="col" class="pro-owner">Owner</th>
                                        <th scope="col" class="pro-gen">General Contractor</th>
                    
                                        <th scope="col" class="col pro-options">Options</th>
                                        <th scope="col" class="document-type">Document Type </th>
                                       

                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="pagination-div text-right">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@include('customer.work_order_histry.modal_ldonj')
@include('customer.work_order_histry.modal_ccrd')
<form method="post" data-parsley-validate="" id="add-note-form">
    {!! csrf_field() !!}
    <div id="favoritesModal" class="modal fade register-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title"> Add Note</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div class="alert alert-success" id="modal-success-note" style="display: none">
                    </div>

                </div>
                <div class="modal-body">
                    <input type="hidden" class="add_node_work_order_id" name="work_order_id" id="work_order_number">    
                    <div class="input-wrapper full-width">
                        <div class="col-md-12 col-sm-12">
                            <div class="input-wrapper full-width">
                                <div class="styled-input">
                                    <input class="form-control" id="note" type="text" name="note" required="" data-parlsey-required="true" data-parsley-required-message="Note is required">
                                    <label>Note<span class="mandatory-field">*</span></label>
                                    <p></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <div class="input-wrapper full-width">
                                <label>Email<span class="mandatory-field">*</span></label>
                                <div class="styled-input">
                                    <select class="form-control" name="email" data-parlsey-required="true" data-parsley-required-message="Email is required" data-parsley-trigger="change focusout" data-parsley-type="email" id="note-mail" required>
                                        <option value="">Select</option>
                                        @foreach($note_emails as $email)
                                            <option value="{{$email}}" @if (old('email') == $email) selected="selected" @endif > {{$email}}</option>
                                        @endforeach
                                    </select>
                                    <p></p>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="form-bottom-btn">
                                <button type="button" id="add-note-submit" class="btn btn-primary custom-btn customc-btn modal-submit"><span>Submit</span></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </form>
            <?php
                if(isset(Session::get('view-work-order')['ref_this'])){
                            $tab=Session::get('view-work-order')['ref_this'];
                }else{
                            $tab="#1";
                }
                    
                $ids=array();
                if(isset(Session::get('view-work-order')['hide_show'])){
                            $values=Session::get('view-work-order')['hide_show'];
                }else{
                            $values=[$tableCol['proj_address'],$tableCol['parent_wo'],$tableCol['job_start_date'],$tableCol['due_date'],$tableCol['notice_type'],$tableCol['status'],$tableCol['submitted_date']];
                }
                    
                $page=0;
                if(isset(Session::get('view-work-order')['page'])){
                    $page=Session::get('view-work-order')['page'];

                }
                if($page!=0){
                    $paging="$('#datatable').DataTable().page($page).draw( 'page' )";                                                           
                }
            ?>
            @stop
            @section('frontend_js')
            <!-- DataTables -->
            <!-- <link rel="stylesheet" href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
            <link rel="stylesheet" href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.css">
            
            <script src="https://code.jquery.com/jquery-3.3.1.js" type="text/javascript"></script>
            <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" type="text/javascript"></script>
            <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js" type="text/javascript"></script> -->

            <!-- <script src="{{ asset('vendor/adminlte/plugins/datepicker/bootstrap-datepicker.js') }}" type="text/javascript"></script> -->
            <script type="text/javascript">
                var dt_page=parseInt("{{$page}}");
                $(document).ready(function(){
                    var tab="{{$tab}}";
                    //alert(tab);
                    $('a[href="'+tab+'"]').trigger('click');
                    var values=<?php echo json_encode($values); ?>;
                       for(var i = 0; i < values.length; i++){
                            $("#hid-show-"+values[i]).attr("checked", "checked");
                       }
                   
                });

            function tabSelectionWithAjax()
            {
               var array=[];
               var types = $('input:checkbox[name="status"]:checked').each(function (i,val) {
                   return  array.push( val.value );
               });
               var array1=[];
               var hid_show = $('input:checkbox[name="hide-show"]:checked').each(function (i,val) {
                   return  array1.push( val.value );
               });
               var from_date =$("input[name='from_date']").val();
               var to_date =$("input[name='to_date']").val();
               var type =$("input:checkbox[name='type']:checked").val();
               //var customer_name=$('#customer_name1').val();
               var customer_cst=$('#customer_cst').val();
               var general_contractor=$('#general_contractor').val();
               var account_manager=$('#account_manager').val();
               var work_order_id=$('#work_order_id').val();
               var parent_work_order=$('#parent_work_order').val();
               //var customer_name2=$('#customer_name2').val();
               var notice_state=$('#notice_state').val();
               var project_address=$('#project_address').val();
               var ref_this = $("#tabs li.active a").attr("href");
               var page=$('#datatable').DataTable().page.info().page;
               var page_type=(window.location.href).split('/').pop();
               //var page_type='research';
               
               var data1= {work_order_id:work_order_id,parent_work_order:parent_work_order,notice_state:notice_state,project_address:project_address,
               account_manager:account_manager,general_contractor:general_contractor,customer_cst:customer_cst,
               to_date:to_date,from_date:from_date,types:array,doc_type:type,ref_this:ref_this,hide_show:array1,page:page,page_type:page_type};

               var tabselection = "<?php echo url(''); ?>" + "/account-manager/research/tabselection";

               $.ajax({
                   type: 'GET',
                   url: tabselection,
                   data: data1,
                   success: function (response) {

                   }
               });
           }

          function reset(){
              var page_type=(window.location.href).split('/').pop();

                var url="<?php echo url(''); ?>"+"/account-manager/research/resetselection";
                 $.ajax({
                    type: 'GET',
                    url: url,
                    data: {page_type:page_type},
                    success: function (response) {
                        window.location.href="<?php echo url(''); ?>"+"/customer/view-work-order";
                    }
                });
                }
          function filterStatus() {

              //build a regex filter string with an or(|) condition
              var types = $('input:checkbox[name="status"]:checked').map(function () {
                  return  this.value;
              }).get().join('|');

              tabSelectionWithAjax();                                                       //filter in column 0, with an regex, no smart filtering, no inputbox,not case sensitive
              $('#datatable').dataTable().fnFilter(types, 11, true, true, true, true, true, true, true, true, true);


          }
          function doc_type() {

              //build a regex filter string with an or(|) condition
              var types = $('input:checkbox[name="type"]:checked').map(function () {
                  return  this.value;
              }).get().join('|');

                    tabSelectionWithAjax();
              //filter in column 0, with an regex, no smart filtering, no inputbox,not case sensitive
              $('#datatable').dataTable().fnFilter(types, 18, true, true, true, true, true, true, true, true, true);

          }
          function notes(work_order_id) {
              $('.add_node_work_order_id').val(work_order_id);
          }

              // function for fire event onchnage options from dropdwon list
              function select_action(value, workorder_id, notice_id) {
                  //console.log(value + "--" + workorder_id);

                  var continue_work_url = "<?php echo url(''); ?>" + "/customer/work-order/edit/" + workorder_id + "/" + notice_id + "/" + "0";
                  var cancel_work_url = "<?php echo url(''); ?>" + "/customer/work-order/cancel/" + workorder_id;
                  var duplicate_work_url = "<?php echo url(''); ?>" + "/customer/work-order/duplicate/" + workorder_id + "/" + notice_id + "/" + "0";
                 /* var view_work_url = "<?php echo url(''); ?>" + "/customer/printpdf/" + workorder_id;*/
                  var view_work_url = "<?php echo url(''); ?>" + "/account-manager/printpdf/" + workorder_id + '/work_order/Notice';
                  var view_requested_work_order = "<?php echo url(''); ?>" + "/customer/work-order/view/" + workorder_id;
                  var generate_work_order_pdf = "<?php echo url(''); ?>" + "/account-manager/printpdf/" + workorder_id + '/work_order/Notice';
                  var rescind_work_order = "<?php echo url(''); ?>" + "/customer/work-order/rescind/" + workorder_id + "/" + notice_id + "/" + "1";
                  //console.log("Url ==="+continue_work_url);

                  if (value == "cancel_work_order") {
                      window.location.replace(cancel_work_url);
                  } else if (value == "add_note") {
                        $("#modal-success-note").hide();
                        $("#modal-success-note").html('');

                      $('.add_node_work_order_id').val(workorder_id);
                      //notes(workorder_id);
                      $("#favoritesModal").modal('show');
                  } else if (value == "continue_working") {
                      window.location.replace(continue_work_url);

                  } else if (value == "duplicate_work_order") {
                    var result = confirm("Do you want to duplicate work order #"+workorder_id+"?");
                    if (result) {
                        window.location.replace(duplicate_work_url);
                    }else{
                        $('.options_list').val('');
                    }
		          } else if (value == "view_work_order") {
		              window.location.replace(view_work_url,'_blank');
		          } else if (value == "view_requested_work_order") {
		              window.location.replace(view_requested_work_order,'_blank');
		          } else if (value == "generate_work_order_pdf") {
		              window.open(generate_work_order_pdf, '_blank');
		          } else if (value == "rescind_work_order") {
		               window.location.replace(rescind_work_order);
		          } else if (value == "add_ldonj") {
                        loadModalLastDateOnJob(workorder_id);
		          }
                  else if (value == "add_ccrd") {
                    loadModalClerkCourtRecordedDate(workorder_id);
                }
		      }

     
   $(document).ready(function () {
    /****************** autoclose datepicker ***********/
    $('.datepicker').datepicker().on('changeDate', function(ev){                 
        $('.datepicker').datepicker('hide');
    });
      // var view_workorder_url = "<?php echo url(''); ?>" + "/customer/work-order/view/";
       var view_workorder_url = "<?php echo url(''); ?>" + "/customer/work-order/view/printworkorder/";


       const tableHead = {'wo_id': 0,'company_name': 1, 'proj_address': 2,'cont_by': 3, 'submitted_date': 4, 'job_start_date': 5,'clerk_date': 6,'due_date': 7,'account_manager':8,'cust': 9,'state': 10,'status': 11, 'notice_type': 12, 'parent_wo':13,'amnt_due': 14, 'owner': 15,'general_cont': 16, 'option': 17, 'doc_type': 18};

       var docTable = $('#datatable').DataTable({
         // "initComplete": function(settings, json) {
         //                tabSelectionWithAjax();
         //            if(dt_page>0){
         //                $('#datatable').DataTable().page(dt_page).draw( 'page' );
         //        }
                                                                                                                      
         //                    },
           //stateSave: true,

           pageResize: true, // enable page resize
           processing: true,
           'fixedHeader': {
               'header': true,
               'footer': true
           },

           "language": {

               zeroRecords: "No Record Found",
               paginate: {
                   sNext: "<img src='/images/right-arrow.png'>",
                   sPrevious: "<img src='/images/left-arrow.png'>"
               }
           },
           dom: "<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-6'p>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
           fixedColumns: {
               leftColumns: 1,
           },
           /*************************/
           scrollY: "500px",
           scrollX: true,
           scrollCollapse: true,
           // lengthMenu: [10],
           pagingType: "simple_numbers", /************************/
           processing: true,
           serverSide: true,
           fixedHeader: true,
           autoWidth: false,

           columnDefs: [
           
                {"width": "180px", "targets": tableHead.wo_id},
                {"width": "200px", "targets": tableHead.company_name},
                {"width": "200px", "targets": tableHead.proj_address, "sortable": false},
                {"width": "150px", "targets": tableHead.cont_by, "sortable": false, "visible": false},
                {"width": "200px", "targets": tableHead.submitted_date, "sortable": false},
                {"width": "200px", "targets": tableHead.job_start_date, "sortable": false},
                {"width": "200px", "targets": tableHead.clerk_date, "sortable": false},
                {"width": "180px", "targets": tableHead.due_date, "sortable": false},
                {"width": "200px", "targets": tableHead.account_manager, "sortable": false, "visible": false},
                {"width": "250px", "targets": tableHead.state, "sortable": false, "visible": false},
                {"width": "200px", "targets": tableHead.cust, "sortable": false, "visible": false},
                {"width": "150px", "targets": tableHead.status, "sortable": false},
                {"width": "180px", "targets": tableHead.notice_type, "sortable": false},
               // {"width": "150px", "targets": 13},
                {"width": "200px", "targets": tableHead.parent_wo, "sortable": false},
                {"width": "200px", "targets": tableHead.amnt_due, "sortable": false, "visible": false},
                {"width": "200px", "targets": tableHead.owner, "sortable": false, "visible": false},
                {"width": "200px", "targets": tableHead.general_cont, "sortable": false,"visible": false},
                {"width": "200px", "targets": tableHead.option, "sortable": false},
                {"width": "200px", "targets": tableHead.doc_type, "sortable": false, "visible": false},

           ],
           order: [[0, 'desc' ]],

          // fixedColumns: true,
           // bInfo: false,
           ajax: {
               url: "{{url('customer/get-work-order')}}",
               data: function (d) {

                   d.from_date = $('input[name=from_date]').val();
                   d.to_date = $('input[name=to_date]').val();

               }
           },
           "fnRowCallback": function (nRow, data, iDisplayIndex, iDisplayIndexFull) {
               var d = new Date();
               var strDate = (d.getMonth() + 1) + "/" + d.getDate() + "/" + d.getFullYear();
               // var strDate = '12/24/2018';
               if (data.type == 2) {
                   if (data.job_start_date && data.job_start_date != '') {

                       if (data.status != "6" && Date.parse(strDate) >= Date.parse(data.highligted_date))
                       {
                           $('td', nRow).addClass('cl-notification1');
                       }
                   }
               } else {
                   if (data.clerk_of_court_recorded_date && data.clerk_of_court_recorded_date != '') {

                       if (data.status != "6" && Date.parse(strDate) >= Date.parse(data.highligted_date))
                       {
                           $('td', nRow).addClass('cl-notification2');
                       }
                   }
                if (data.last_date_on_the_job && data.last_date_on_the_job != '') {

                if (data.status != "6" && Date.parse(strDate) >= Date.parse(data.highligted_date))
                {
                    $('td', nRow).addClass('cl-notification2');
                }
                }
               }
           },
            columns: [

            {data: 'workorder_id', render: function (data, type, row) {
                    if (data) {
                        return '<a href="' + view_workorder_url + '' + data + '" data-toggle="tooltip" target=_blank>#' + data + '</a>';

                    } else {
                        return   data;
                    }
                }},
            {data: 'company_name', render: function (data, id, row) {
                    if (data) {
                        data = data.length > 20 ?
                                data.substr(0, 20) + '<a data-toggle="tooltip" title="' + data + '" data-html="true">...</a>' :
                                data;

                        return data;
                    } else
                        return "NA";
            }},
            {data: 'project_address', class: 'col project-value', render: function (data, type, row) {
                    if (data) {
                        var data_addr = $("<br />").html(data).text();
                        var regex = /<br\s*[\/]?>/gi;

                        projaddr=data.length > 10 ?
                                data_addr.replace(regex, ' ').substr(0, 14) + '<a  data-toggle="tooltip" title="' + data_addr.replace(regex, ' ') + '" data-html="true">...</a>' :
                                data;
                            return projaddr.split('**').join('&#013;**');
                    } else
                        return "NA";

                }},
            {data: 'contracted_by', class: 'dropdown pro-contby', render: function (data, status, row) {
                    if (data) {
                        return data.length > 15 ?
                                data.substr(0, 15) + '<a  data-toggle="tooltip" title="' + data + '">...</a>' :
                                data;
                    } else
                        return "NA";
                }},
            {data: 'created_at', render: function (data, status, row) {
                    if (data && row['status'] != "0") {
                        var date = new Date(data);
                        var month = date.getMonth() + 1;
                        return (month.length > 1 ? month : month) + "/" + date.getDate() + "/" + date.getFullYear();
                    } else
                        return "NA";

            }},
            {data: 'job_start_date', class: 'col pro-start', render: function (data, status, row) {
                    if (data) {
                        var date = new Date(data);
                        var month = date.getMonth() + 1;
                        return (month.length > 1 ? month : month) + "/" + date.getDate() + "/" + date.getFullYear();
                    } else
                        return "NA";
                }},
                {data: 'clerk_of_court_recorded_date', render: function (data, status, row) {
                if (data!="NA") {
                    var date = new Date(data);
                    var month = date.getMonth() + 1;
                    return (month.length > 1 ? month : month) + "/" + date.getDate() + "/" + date.getFullYear();
                } else
                    return "NA";
            }},
            {data: 'due_date', render: function (data, status, row) {
                    if (data) {
                        var date = new Date(data);
                        var month = date.getMonth() + 1;
                        return (month.length > 1 ? month : month) + "/" + date.getDate() + "/" + date.getFullYear();
                    } else
                        return "NA";
                }},
            {data: "account_manager_name", name: 'account_manager_name', render: function (data, status, row) {
                    if (data) {
                        return data;
                    } else
                        return "NA";
                }},
             {data: 'customer_name', render: function (data, id, row) {
                    if (data) {
                        return data;
                    } else
                        return "NA";
                }},
             
            {data: "project_state", class: ' pro-state', render: function (data, status, row) {
                    if (data) {
                        return data;
                    } else
                        return "NA";
                }},

           

            {data: 'status', class: 'col pro-status', render: function (data, status, row) {
                    if (data == 0)
                        return 'Draft';
                    else if (data == 1)
                        return 'Request';
                    else if (data == 5)
                        return 'Mailing';
                    else if (data == 2)
                        return 'Processing';
                    else if (data == 3)
                        return 'Recording';
                    else if (data == 4)
                        return 'Pending Signature';
                    else if (data == 7)
                        return 'Cancelled';
                    else if (data == 8)
                        return 'Restricted';
                    else if (data == 6)
                        return 'Completed ';
                }

            },
            {data: "notice_name", class: 'col pro-temp', render: function (data, notice_id, row) {
                    if (data) {
                        var data_notice = $("<br />").html(data).text();
                        var regex = /<br\s*[\/]?>/gi;
                        return  data.length > 20 ?
                                data_notice.replace(regex, ' ').substr(0, 20) + '<a   data-toggle="tooltip" title="' + data_notice.replace(regex, ' ') + '">...</a>' :
                                data;
                    } else
                        return "NA";

                }},

            {data: "parent_work_order", class: 'col pro-parent', render: function (data, parent_id, row) {
                    if (data)
                        return data;
                    else
                        return "NA";
                }},

            {data: 'amount_due', class: 'col pro-due', render: function (data, status, row) {
                    if (data) {
                        return data;
                    } else
                        return "NA";
                }},

//                                                                {data: 'due_date', class: 'col pro-duedate', render: function (data, status, row) {
//                                                                        if (data) {
//                                                                            var date = new Date(data);
//                                                                            var month = date.getMonth() + 1;
//                                                                            return (month.length > 1 ? month : "0" + month) + "/" + date.getDate() + "/" + date.getFullYear();
//                                                                        } else
//                                                                            return "NA";
//                                                                    }},
            {data: 'project_owner', class: 'col pro-owner', render: function (data, status, row) {
                    if (data) {
                        return data.length > 15 ?
                                data.substr(0, 15) + '<a href="#" data-toggle="tooltip" title="' + data + '">...</a>' :
                                data;
                    } else
                        return "NA";
                }},
            {data: 'general_contracted', class: 'col pro-cont', render: function (data, status, row) {
                    if (data) {
                        return data.length > 15 ?
                                data.substr(0, 15) + '<a href="#" data-toggle="tooltip" title="' + data + '">...</a>' :
                                data;
                    } else
                        return "NA";
                }},
           

            {data: "actions", name: "actions", class: 'col pro-options', 'orderable': false, 'searchable': false},
            {data: 'type', class: 'document-type', render: function (data, status, row) {
                    return data;
                }},
                // {data: 'account_manager_name', class: 'user-name', render: function (data, status, row) {
                //     return data;
                //     }},
        ]
       });


        // $(window).bind('resize', function () {
        //    docTable.fnAdjustColumnSizing();
        // });
      
     $('a.toggle-vis').on('click', function (e) {
         e.preventDefault();
         /*******toggle button checked n unchecked********/
         if ($(this).find('.switch input').attr('checked')) {
             $(this).find('.switch input').attr('checked', false);
         } else {
             $(this).find('.switch input').attr('checked', true);
         }
        tabSelectionWithAjax();

         // Get the column API object
         var column = docTable.column($(this).attr('data-column'));
         //console.log(column);

         // Toggle the visibility
         column.visible(!column.visible());
     });

     $.fn.dataTable.ext.search.push(function (settings, data, dataIndex) {
         var min = $('#from-date').datepicker("getDate");
         var max = $('#to-date').datepicker("getDate");
         var startDate = new Date(data[4]);
         if (min == null && max == null) {
             return true;
         }
         if (min == null && startDate <= max) {
             return true;
         }
         if (max == null && startDate >= min) {
             return true;
         }
         if (startDate <= max && startDate >= min) {
             return true;
         }
         return false;
     });

     $('#from-date, #to-date').on('change', function () {
        tabSelectionWithAjax();
        docTable.draw();

     });
     $('#from-date').on('submit', function (e) {
        tabSelectionWithAjax();
        docTable.draw();
        e.preventDefault();
     });
     $('#to-date').on('submit', function (e) {
        tabSelectionWithAjax();
        docTable.draw();
        e.preventDefault();
     });
     $(function () {
        var otable = $('#datatable').DataTable();
     });
    $(document).ready(function(){
      $('[data-toggle="tooltip"]').tooltip();   
    });

     //var customer_name=$('#customer_name').val();
    var customer_cst=$('#customer_cst').val();
    if(customer_cst=="Select Customer's Customer")  customer_cst="";
    var general_contractor=$('#general_contractor').val();
    var account_manager=$('#account_manager').val();
    //if(customer_name=="Select")  customer_name="";
    if(account_manager=="Select Account Manager")  account_manager="";
    var work_order_id=$('#work_order_id').val();

    var parent_work_order=$('#parent_work_order').val();
    var customer_name2=$('#customer_name2').val();
    var notice_state=$('#notice_state').val();
    var project_address=$('#project_address').val();
   // if(customer_name2=="Select")  customer_name2="";
    if(notice_state=="Select")  notice_state="";


    filterStatus();
    doc_type();
    if(work_order_id != ''){
        $('#datatable').dataTable().fnFilter(work_order_id, tableHead.wo_id, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true);
    }
       // $('#datatable').dataTable().fnFilter(customer_name2, 17, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true);
        //$('#datatable').dataTable().fnFilter(customer_name, 1, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true);
    if(customer_cst != ''){
        $('#datatable').dataTable().fnFilter(customer_cst, tableHead.cont_by, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true);
    }
    if(general_contractor != ''){
        $('#datatable').dataTable().fnFilter(general_contractor, tableHead.general_cont, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true);
    }
    if(account_manager != ''){
        $('#datatable').dataTable().fnFilter(account_manager, tableHead.account_manager, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true);
    }
    if(parent_work_order != ''){
        $('#datatable').dataTable().fnFilter(parent_work_order, tableHead.parent_wo, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true);
    }
    if(notice_state != ''){
        $('#datatable').dataTable().fnFilter(notice_state, tableHead.state, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true);
    }
    if(project_address != ''){
        $('#datatable').dataTable().fnFilter(project_address, tableHead.proj_address, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true);
    }
        var hid_show = $('input:checkbox[name="hide-show"]:checked').each(function (i,val) {
            // Get the column API object
            var column = docTable.column(val.value);
           // console.log(column.visible());

            // Toggle the visibility
            column.visible(column.visible());
        });

    // $('#project-amount input').click(function () {
    //     tabSelectionWithAjax();
    //     $('#datatable').dataTable().columns().visible();
    // });
    // //      $('#project-duedate input').click(function () {
    // //           valueChanged();
    // //      });
    // $('#project-owner input').click(function () {
    //     tabSelectionWithAjax();
    //     $('#datatable').dataTable().columns().visible();
    // });
    // $('#project-gen input').click(function () {
    //     tabSelectionWithAjax();
    //     $('#datatable').dataTable().columns().visible();
    // });
    // $('#project-contby input').click(function () {
    //     tabSelectionWithAjax();
    //     $('#datatable').dataTable().columns().visible();

    // });

    // $('#project-accman input').click(function () {
    //     tabSelectionWithAjax();
    //     $('#datatable').dataTable().columns().visible();
    // });
    // $('#project-state input').click(function () {
    //     tabSelectionWithAjax();
    //     $('#datatable').dataTable().columns().visible();
    // });


     //customer filter with project address
     $('#project_address').on('keyup keypress change', function () {
        tabSelectionWithAjax();
         // otable.search(this.value).draw();
         $('#datatable').dataTable().fnFilter(this.value, tableHead.proj_address, true, true, true, true, true, true, true, true, true);
     });
     //customer filter with parent work order
     $('#parent_work_order').on('keyup keypress change', function () {
        tabSelectionWithAjax();
         // otable.search(this.value).draw();
         $('#datatable').dataTable().fnFilter(this.value, tableHead.parent_wo, true, true, true, true, true, true, true, true, true, true);
     });
     //customer filter with parent work order
     $('#general_contractor').on('keyup keypress change', function () {
        tabSelectionWithAjax();
         // otable.search(this.value).draw();
         $('#datatable').dataTable().fnFilter(this.value, tableHead.general_cont, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true);
     });
     $('#work_order_id').on('keyup keypress change', function () {
        tabSelectionWithAjax();
         // otable.search(this.value).draw();
        $('#datatable').dataTable().fnFilter(this.value, tableHead.wo_id, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true);
     });

     $('.contractedby').on('keyup keypress change', function () {
        tabSelectionWithAjax();
       // otable.search(this.value).draw();
       $('#datatable').dataTable().fnFilter(this.value, tableHead.cont_by, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true);
     });
     /*$('.customer_name').on('change', function () {
                    tabSelectionWithAjax();
         // otable.search(this.value).draw();
         $('#datatable').dataTable().fnFilter(this.value, 8, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true);
     });*/
     $('.notice_state').on('change', function () {
        tabSelectionWithAjax();
         // otable.search(this.value).draw();
         $('#datatable').dataTable().fnFilter(this.value, tableHead.state, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true);
     });

     $('#account_manager').on('change', function () {
        tabSelectionWithAjax();
         // otable.search(this.value).draw();
         $('#datatable').dataTable().fnFilter(this.value, tableHead.account_manager, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true);
     });

     $('#customer_cst').on('change', function () {

        tabSelectionWithAjax();
        // otable.search(this.value).draw();
        $('#datatable').dataTable().fnFilter(this.value, tableHead.cont_by, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true);
     });
   /*  $('body').tooltip({selector: '[data-toggle="tooltip"]',
         trigger: 'hover',
         placement: 'top',
     });*/

 });
$('#datatable').on( 'page.dt', function () {
    var info = $('#datatable').DataTable().page.info();
    tabSelectionWithAjax();
} );
 // CSRF for all ajax call
    $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content') } })
$('#add-note-submit').on('click', function (e) {
    //alert("{{ url('customer/work-order/add-note') }}");
    $("#modal-success-note").hide();
    $("#modal-success-note").html('');
    $('#add-note-form').parsley().validate();
    $.ajax({
          url: "{{ url('customer/work-order/add-note') }}",
          type: "POST",
          data: {note: $("#note").val(),
                email: $("#note-mail").val(),
                work_order_id: $("#work_order_number").val()
                },
          //async: true,
         dataType: "json",
        beforeSend: function() {
                $('#add-note-submit').attr("disabled", true);    
        },
         success:function(data) {
            if(data.result=="success"){
                $("#modal-success-note").show();
                $("#modal-success-note").html(data.message);
                $('#add-note-submit').attr("disabled", false); 
                $("#note").val('');
                $("#note-mail").val('');
            }
            
          }
    });

});

function loadModalLastDateOnJob(workorder_id) {
    var cyo = 0;
    $.ajax({
        type: 'GET',
        url: "<?php echo url(''); ?>" + "/customer/modal-add-last-date-on-job/"+workorder_id  +"/"+cyo,
        beforeSend: function () {
            $('#modal_ldonj .modal-body').html('loading...');
        },
        success: function (response) {
            //$('#modal_ldonj #work_order_id').val(workorder_id);
            $('#modal_ldonj .modal-body').html(response);
            $('.datepicker').datepicker();
            $("#modal_ldonj").modal('show');
            $('#modal_ldonj input').focus();
            $('.options_list').val('');
        }
    });
}

function loadModalClerkCourtRecordedDate(workorder_id) {
    
    var cyo = 0;
    $.ajax({
        type: 'GET',
        url: "<?php echo url(''); ?>" + "/customer/modal-add-clerk-court-recorded-date/"+workorder_id  +"/"+cyo,
        beforeSend: function () {
            $('#modal_ccrd .modal-body').html('loading...');
        },
        success: function (response) {
            $('#modal_ccrd .modal-body').html(response);
            $('.datepicker').datepicker();
            $("#modal_ccrd").modal('show');
            $('#modal_ccrd input').focus();
            $('.options_list').val('');
        }
    });
}
</script>
<style type="text/css">
.options_list{
    /*      width: 260px;*/
    width:100%;
}
#datatable{
    border-collapse: collapse;width:100%
}
</style>
@endsection