@extends('adminlte::page')
@section('content')
<div>
    <section class="create-own">
        <div class="dashboard-wrapper">
            <div class="dashboard-heading">
                <h1><span>Create Pin</span></h1>
            </div>
            <div class="dashboard-inner-body">
                <div class="createown-wrapper">
                    <div class="grid">
                        <figure class="effect-sadie">
                            <figcaption>
                                <h2>Preliminary Notice</h2>
                                <p>Create Your Own</p>
                            </figcaption>			
                        </figure>
                    </div>
                    <div class="grid">
                        <figure class="effect-sadie">
                            <figcaption>
                                <h2>Intent to lien</h2>
                                <p>Create Your Own</p>
                            </figcaption>			
                        </figure>
                    </div>
                    <div class="grid">
                        <figure class="effect-sadie">
                            <figcaption>
                                <h2>Non Payment Notice</h2>
                                <p>Create Your Own</p>
                            </figcaption>			
                        </figure>
                    </div>
                    <div class="grid">
                        <figure class="effect-sadie">
                            <figcaption>
                                <h2>Claim of Lien</h2>
                                <p>Create Your Own</p>
                            </figcaption>			
                        </figure>
                    </div>
                    <div class="grid">
                        <figure class="effect-sadie">
                            <figcaption>
                                <h2>Waiver up on progress of payment</h2>
                                <p>Create Your Own</p>
                            </figcaption>			
                        </figure>
                    </div>
                    <div class="grid">
                        <figure class="effect-sadie">
                            <figcaption>
                                <h2>Satisfaction of lien</h2>
                                <p>Create Your Own</p>
                            </figcaption>			
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@stop