@extends('adminlte::page')
@section('content')
<style type="text/css">
    .errorsection {
        display:none;
    }
    .select2class :focus{
    outline: 0;
    height: 47px !important;
    border-bottom : 1px solid #032c61 !important;
}
</style>

<div class="main-content">
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="address">

            <!----------------------address book add-2 starts---------------------------->
            <!--          -->
            <form method="post" id="create_address_book_form" data-parsley-validate=""  class="form-horizontal" action="{{ url('customer/contact/create') }}">
                <section class="address-book-add">
                    <div class="dashboard-wrapper">
                        <div class="contact-status"></div>
                        <div class="dashboard-heading">
                            <h1><span>Add New Contact</span></h1>
                        </div>
                        {!! csrf_field() !!}
                        <div class="dashboard-inner-body">
                            <div class="dash-subform">
                                <div class="row">
                                    @if(Auth::user()->hasrole('account-manager'))

                                    <div class="col-md-6 col-sm-6">
                                        <div class="select-opt">
                                            <label class="col-md-12 control-label" for="textinput">Customers<span class="mandatory-field">*</span></label>
                                            <select name="customer_id" class="selectpicker" data-parsley-required="true" data-parsley-required-message="Customer  is required" data-parsley-trigger="change focusout">
                                                <option value="">Select</option>
                                                @foreach($customers as $customer)
                                                <option value="{{$customer->id}}" @if (old('customer_id') == $customer->id) selected="selected" @endif > {{ucfirst(trans($customer->name))}}</option>
                                                @endforeach
                                            </select>

                                        </div>
                                    </div>
                                    @endif
                                    <div class="col-md-6 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input {{ $errors->has('company_name') ? 'has-error' : '' }}">
                                                <input type="text" name="company_name" value="{{ old('company_name')}}" class="form-control" data-parsley-required="true" data-parsley-required-message="Company Name is required" data-parsley-trigger="change focusout">
                                                <label>Company or Contact Person Name<span class="mandatory-field">*</span></label>
                                                <span></span>
                                                @if ($errors->has('company_name'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('company_name') }}</strong>
                                                </p>
                                                @endif
<!--                                                <p class="help-block errorsection" data-name="company_name" data-index=""></p>-->
                                            </div>
                                        </div>
                                    </div>    
                                    <div class="col-md-6 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <label class="control-label" for="textarea">Attn/Additional Recipient Name<span class="mandatory-field">*</span></label>
                                            <div class="styled-input">
                                                <textarea class="form-control recipient_attns"  type="text" name="attn" id="attn" data-parsley-trigger="change focusout">{{old('attn')}}</textarea>
                                                <span></span>
                                                @if ($errors->has('attn'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('attn') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="mailing_address" value="{{old('mailing_address')}}" class="form-control" data-parsley-required="true" data-parsley-required-message="Mailing Address is required" data-parsley-trigger="change focusout" data-parsley-maxlength="100">
                                                <label>Mailing Address<span class="mandatory-field">*</span></label>
                                                <span></span>
                                                @if ($errors->has('mailing_address'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('mailing_address') }}</strong>
                                                </p>
                                                @endif
                                            <!--<p class="help-block errorsection" data-name="mailing_address" data-index=""></p>-->
                                            </div>
                                        </div>
                                    </div>

                                 <div class="col-md-6 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input {{ $errors->has('zip') ? 'has-error' : '' }}">
                                            <label class="lableall">Zip Code<span class="mandatory-field">*</span></label>
                                                <input class="form-control"  type="hidden" name="selected_address_book" value="" id="selected_address_book">
                                                <input class="form-control new_addresscontact_zip"  type="text" name="new_addresscontact_zip" value="{{old('new_addresscontact_zip')}}" id="new_addresscontact_zip" data-parsley-required="true" data-parsley-required-message="Zip code is required" data-parsley-trigger="change focusout">
                                                <input class="form-control addresscontact_zip"  type="hidden" name="zip"  value="{{old('zip')}}" data-parsley-required="true" data-parsley-required-message="Zip code is required" data-parsley-trigger="change focusout" >

                                                {{-- <input class="form-control addresscontact_zip"  type="hidden" name="contact_zip" value="{{old('contact_zip')}}" id="contact_zip"> --}}
                                                <!-- <input type="text" id='employeeid' readonly> -->
                                                <input class="contact_zipurl"  type="hidden" name="contact_zipurl" id="contact_zipurl" value="{{ url('admin/ZipCodeLookups') }}">
                                                <input class="contact_stateurl"  type="hidden" name="contact_stateurl" id="contact_stateurl" value="{{ url('admin/getStateName') }}">
                                                <span></span>
                                                <p class="help-block" >
                                                    <strong id="zip-error"></strong>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="select-opt select2-container col-md-12 select2class">
                                            <label class="col-md-12 control-label" for="textinput">City<span class="mandatory-field">*</span></label> 
                                            
                                                <select class="js-example-tags form-control load_ajax_cities"  name="city" id="city_value" data-parsley-required="true" data-parsley-required-message="City is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true">
                                                    <option value="">Select</option>    
                                                </select>
                                                <span></span>
                                                @if ($errors->has('city'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('city') }}</strong>
                                                </p>
                                                @endif
                                       </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="select-opt">
                                            <label class="col-md-12 control-label" for="textinput">State<span class="mandatory-field">*</span></label>
                                            <select name="state" class="selectpicker addresscontact_state_id" data-parsley-required="true" data-parsley-required-message="State is required" data-parsley-trigger="change focusout" id='state_value' data-show-subtext="true" data-live-search="true">
                                                <option value="">Select</option>
                                                @foreach($states as $state)
                                                <option value="{{$state->id}}" @if (old('state') == $state->id) selected="selected" @endif > {{ucfirst(trans($state->name))}}</option>
                                                @endforeach
                                            </select>
                                            <!--<p class="help-block errorsection" data-name="state" data-index=""></p>-->
                                            @if ($errors->has('state'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('state') }}</strong>
                                            </p>
                                            @endif
                                        </div>
                                    </div>
                                    <!--                                    <div class="col-md-6 col-sm-6">
                                    
                                                                            <div class="select-opt">
                                                                                <label class="col-md-12 control-label" for="textinput">County<span class="mandatory-field">*</span></label>  
                                                                                <select name="country"  class="selectpicker" data-show-subtext="true" data-live-search="true" data-parsley-required="true" data-parsley-required-message="County is required" data-parsley-trigger="change focusout">
                                                                                    <option value="">Select</option>
                                                                                    @foreach($countries as $country)
                                                                                    <option value="{{$country->id}}" @if (old('country') == $country->id) selected="selected" @endif> {{ucfirst(trans($country->name))}}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                                <p class="help-block errorsection" data-name="country" data-index=""></p>
                                                                                 @if ($errors->has('country'))
                                                                                        <p class="help-block">
                                                                                            <strong>{{ $errors->first('country') }}</strong>
                                                                                        </p>
                                                                                        @endif
                                                                            </div>
                                                                        </div>-->
                               
                                    <div class="col-md-6 col-sm-6 clear-b">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="email" name="business_email" value="{{old('business_email')}}" class="form-control" data-parsley-trigger="change focusout" data-parsley-type="email">
                                                <label>Business Email</label>
                                                <span></span>
                                                @if ($errors->has('business_email'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('business_email') }}</strong>
                                                </p>
                                                @endif
                                            <!--<p class="help-block errorsection" data-name="business_email" data-index=""></p>-->

                                            </div>
                                        </div>
                                    </div>
                                    <div  class="col-md-6 col-sm-6 business_phone_wrapper clear-b">
                                        <div id="business_phone"  class="input-wrapper full-width">
                                            <div class="styled-input  {{ $errors->has('business_phone') ? 'has-error' : '' }}">
                                                <input type="text" name="business_phone[]" value="{{old('business_phone.0')}}" class="form-control business_phone_validation"  id="" data-parsley-trigger="change focuso" data-parsley-pattern='(\(\d{3})\)\s\d{3}\-\d{4}$'>
                                                <label>Business Phone</label>
                                                <span></span>
                                                @if ($errors->has('business_phone.0'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('business_phone.0') }}</strong>
                                                </p>
                                                @endif
                                            <!--<p class="help-block errorsection" data-name="business_phone" data-index="0"></p>-->
                                            </div>
                                        </div>

                                        <div class="plus-btn">
                                            <a type ="button" name="add_business_phone" id="add_business_phone" class="btn btn-primary custom-btn input-btn" value="+ More"><span>+ More</span></a>
                                        </div>
                                    </div>


                                </div>
                            </div>

                            <div class="dash-subform addcontactperson">
                                <div class="row">
                                    <div id="contact_person">
                                        <h2>Contact Person 1</h2>
                                        <div class="main" id="cp1">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="col-md-12 col-sm-12">
                                                        <div class="input-wrapper full-width">
                                                            <div class="styled-input">
                                                                <input type="text" name="contact_name[]" value="{{old('contact_name.0')}}" class="form-control" data-parsley-required-message="Name is required" data-parsley-trigger="change focusout" data-parsley-maxlength="50">
                                                                <label>Name</label>
                                                                <span></span>
                                                                <!--<p class="help-block errorsection" data-name="contact_name" data-index="0"></p>-->
                                                                @if ($errors->has('contact_name.0'))
                                                                <p class="help-block">
                                                                    <strong>{{ $errors->first('contact_name.0') }}</strong>
                                                                </p>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 col-sm-12">
                                                        <div id="contact_email" class="input-wrapper full-width">
                                                            <div class="styled-input">
                                                                <input type="text" name="contact_email1[]" value="{{old('contact_email1.0')}}" id="c_e_value_1"  class="form-control contact_email_value email_validation" data-parsley-required-message="Email is required" data-parsley-trigger="change focusout" data-parsley-type="email">
                                                                <label>Email</label>
                                                                <span></span>
                                                                <!--<p class="help-block errorsection" data-name="contact_email1" data-index="0"></p>-->
                                                                @if ($errors->has('contact_email1.0'))
                                                                <p class="help-block">
                                                                    <strong>{{ $errors->first('contact_email1.0') }}</strong>
                                                                </p>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <a type="button" class="btn btn-primary custom-btn input-btn add_contact_email" id="add_contact_email_1"><span>+ More</span></a>
                                                    </div>
                                                    <div class="col-md-12 col-sm-12">
                                                        <div class="input-wrapper full-width">
                                                            <div class="styled-input">
                                                                <input type="text" name="contact_fax[]" value="{{old('contact_fax.0')}}" class="form-control fax_number_validation" data-parsley-required-message="Fax is required" data-parsley-trigger="change focusout">
                                                                <label>Fax Number</label>
                                                                <span></span>
                                                                @if ($errors->has('contact_fax.0'))
                                                                <p class="help-block">
                                                                    <strong>{{ $errors->first('contact_fax.0') }}</strong>
                                                                </p>
                                                                @endif
                                                                            <!--<p class="help-block errorsection" data-name="contact_fax" data-index="0"></p>-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="col-md-12 col-sm-12">
                                                        <div class="input-wrapper full-width">
                                                            <div class="styled-input">
                                                                <input type="text" name="designation[]" value="{{old('designation.0')}}" class="form-control"  data-parsley-required-message="Designation is required" data-parsley-trigger="change focusout" data-parsley-maxlength="50">
                                                                <label>Position</label>
                                                                <span></span>
                                                                <!--<p class="help-block errorsection" data-name="designation" data-index="0"></p>-->
                                                                @if ($errors->has('designation.0'))
                                                                <p class="help-block">
                                                                    <strong>{{ $errors->first('designation.0') }}</strong>
                                                                </p>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div  class="col-md-12 col-sm-12">
                                                        <div id="contact_mobile" class="input-wrapper full-width">
                                                            <div class="styled-input">
                                                                <input type="text"  name="contact_mobile1[]" value="{{old('contact_mobile1.0')}}" class="form-control mobile_validation" id="c_m_v_1"  data-parsley-required-message="Cell number is required" data-parsley-trigger="change focusout">
                                                                <label>Mobile Number</label>
                                                                <span></span>
                                                                @if ($errors->has('contact_mobile1.0'))
                                                                <p class="help-block">
                                                                    <strong>{{ $errors->first('contact_mobile1.0') }}</strong>
                                                                </p>
                                                                @endif
                                                                            <!--<p class="help-block errorsection" data-name="contact_mobile1" data-index="0"></p>-->
                                                            </div>
                                                        </div>
                                                        <a  type="button" class="btn btn-primary custom-btn input-btn add_contact_mobile" id="add_contact_mobile_1"><span>+ More</span></a>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="add-new-btn">
                                            <a type="button" id="add_contact_person" class="btn btn-primary custom-btn addcontact"><span>+ Add Another Contact Person</span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="text" name="contact_person_array[]" id="contact_person_array" hidden="">
                            <input type="text" name="contact_email_array[]" id="contact_email_array" hidden="">
                            <input type="text" name="contact_count" value="" id="count_person" hidden="">

                            <div class="col-md-12 col-sm-12">
                                <div class="row">
                                    <div class="form-bottom-btn">
                                        <a href="{{URL::previous()}}" class="btn btn-primary custom-btn customb-btn"><span>Cancel</span></a>
                                        <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>Save Contact</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </section>
            </form>
            <!----------------------address book add2 ends---------------------------->
        </div>
    </div>
</div>

@stop
@section('frontend_js')
<script type="text/javascript" src="{{ asset('js/jquery-ui.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function() {
    $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
    });
    
    $(".new_addresscontact_zip").autocomplete({
        source: function(request, response) {
            // Fetch data
            var urlzipcode = $('#contact_zipurl').val();
           $('.addresscontact_zip').val(request.term); 
            $.ajax({
                url: urlzipcode,
                type: 'post',
                dataType: "json",
                data: {
                    zipcode: request.term
                },
                success: function(data) {
                    response(data);
                }
            });
        },
        select: function(event, ui) {
            // Set selection
            $('.new_addresscontact_zip').val(ui.item.label); // display the selected text
            
            return false;
        }
    });
      $('.new_addresscontact_zip').on('autocompleteselect', function (e, ui) {
        var bookid = ui.item.label;
        var res = bookid.split(",");
        //$(".citiesname").val(res[1]);
        //$(".cities").html(res[1]);
        $('.addresscontact_zip').val(res[0]);
        
        $('#city_value')
            .empty()
            .append($("<option/>")
                .text(res[1]))
            .val(res[1])
            .trigger("change");
        //$('.cities').attr('disabled', 'disabled');
        var statename = res[2];
        var contact_stateurl = $('#contact_stateurl').val();
        $.ajax({
            url: contact_stateurl,
            type: 'post',
            dataType: "json",
            data: {
                statename: statename
            },
            success: function(data) {
                $(".addresscontact_state_id").val(data.id).trigger('change');
               // $('.addresscontact_state_id').attr('disabled', 'disabled');

            }
        });
    });
    var zipcode = $('.addresscontact_zip').val();
    var cityName = $('.load_ajax_cities').val();
    var urlzipcode = $('#contact_zipurl').val();
        $.ajax({
        url: urlzipcode,
        type: 'post',
        dataType: "json",
        data: {
            zipcode:zipcode,
            cityName:cityName
        },
        success: function(data) {
            $(".new_addresscontact_zip").val(data[0].label);
            var bookid = data[0].label;
            var res = bookid.split(",");
            $('#city_value')
            .empty()
            .append($("<option/>")
            .text(res[1]))
            .val(res[1])
            .trigger("change");
            var contact_stateurl = $('#contact_stateurl').val();
            $.ajax({
                url: contact_stateurl,
                type: 'post',
                dataType: "json",
                    data: {
                        statename: res[2]
                    },
                    success: function(data) {
                        $(".addresscontact_state_id").val(data.id).trigger('change');
                    }
            });
        }
                          
    });
  

  $('input,textarea,select').on('focus', function(e) {
        e.preventDefault();
        $(this).attr("autocomplete", "nope");  
    });

  var cityUrl = "{{ url('customer/city-list') }}";
       
             $(".load_ajax_cities").select2({
                tags:true,
              ajax: { 
               url: cityUrl,
               type: "get",
               dataType: 'json',
               //delay: 250,
               data: function (params) {
                return {
                  name: params.term // search term
                };
               },
               processResults: function (response) {
                 return {
                    results: response
                 };
               },
               cache: true
              }
             });
        

    //Start Autocomplete City
    src_county = "{{ url('customer/city/autocompletecityname') }}";

    // $("#city_value").autocomplete({

    //     source: function (request, response) {
    //         $.ajax({
    //             url: src_county,
    //             dataType: "json",
    //             data: {
    //                 term: request.term
    //             },
    //             beforeSend: function () {
    //                 $('#cityloader').show();
    //             },
    //             complete: function () {
    //                 $('#cityloader').hide();
    //             },
    //             success: function (data) {

    //                 response($.map(data, function (item) {
    //                     return {
    //                         value: item.value,
    //                         id: item.id     // EDIT
    //                     }
    //                 }));

    //             },

    //         });
    //     },
    //     select: function (event, ui) {

    //         $("#city_value_id").val(ui.item.id);
    //         $("#city_value").val(ui.item.value);
    //     },
    //     change: function (event, ui) {
    //         if (ui.item == null) {
    //             $(this).val("");
    //             $(this).focusout();
    //             $(this).focus();
    //         }
    //     }
    // });

    //End Autocomplete city
    $(".mobile_validation").mask("(999) 999-9999");
    var contact_person_array = [];
    var sales_person_array = [];
    var contact_mobile_count = [];
    contact_person_array.push('0');
    sales_person_array.push('1');
    $('#contact_person_array').val(contact_person_array);
    $('#sales_person_array').val(sales_person_array);


//contact persons
    //  var business_phone = $('#business_phone').html();
    var business_phone = '<div class="input-wrapper full-width">' +
            '<div class="styled-input">' +
            '<input type="text" name="business_phone[]" class="form-control business_phone_validation">' +
            '<label>Business Phone</label>' +
            '<span></span>' +
            '<p class="help-block errorsection" data-name="business_phone" data-index=""></p>' +
            '</div>' +
            '<i class="fa fa-remove removeBusinessPhone"></i>' +
            '</div>';
    var contact_email = $('#contact_email').html();
    var contact_mobile = $('#contact_mobile').html();
    var sales_email = $('#sales_email').html();
    var sales_mobile = $('#sales_mobile').html();
    var contact_person = $('#contact_person').html();
    var sales_person = $('#sales_person').html();
    var contact_count = 1;
    var contact_person_mobile_count = 1;
    var contact_person_email_count = 1;

    var contact_e = [];
    var sales_count = 1;
    contact_e.push(1);

    $('#count_person').val(contact_count);
    $('#count_sales').val(sales_count);
    //$('#contact_email_array').val(contact_e);
    var business_phone_count = 1;
    var business_phone_limit = 3;
   
    $('#add_business_phone').click(function ()
    {
        if (business_phone_count < business_phone_limit) {
            business_phone_count++;
//                var eleLength = $('#business_phone')[0].childElementCount;
//                var element = $('#business_phone')[0].children;
//                $(element).each(function (index) {
//                    if (index === eleLength - 1) {
            //     var increment_index = +index + +1;

//                    business_phone.find('.errorsection').attr('data-index',index);
            var business_phone = '<div class="input-wrapper full-width">' +
                    '<div class="styled-input">' +
                    '<input type="text" name="business_phone[]" class="form-control business_phone_validation" id="business_phone_count_' + business_phone_count + '">' +
                    '<label>Business Phone</label>' +
                    '<span></span>' +
                    '<p class="help-block errorsection" data-name="business_phone" data-index=""></p>' +
                    '</div>' +
                    '<i class="fa fa-remove removeBusinessPhone"></i>' +
                    '</div>';
            $('.business_phone_wrapper').first().append(business_phone);

            $('input').keyup(function () {
                if ($(this).val().length > 0) {
                    $(this).addClass('not-empty');

                } else {
                    $(this).removeClass('not-empty');

                }
            });
            $(".business_phone_validation").mask("(999) 999-9999");
            // $("#business_phone_count_"+business_phone_count).attr('data-parsley-trigger','change focusout')
            //                             .attr('data-parsley-pattern','(\(\d{3})\)\s\d{3}\-\d{4}$').parsley();

        } else {
            alert('You can add business phone upto 2 only');

        }

    });
    $('.business_phone_wrapper').on('click', '.removeBusinessPhone', function (e) {
        e.preventDefault();
        $(this).parent().remove(); //Remove field html
        business_phone_count--; //Decrement field counter
    });

    $('body').on('click', '.removeContactPerson', function (e) {
        e.preventDefault();
        $(this).parent().remove(); //Remove field html
        contact_count--; //Decrement field counter
        $('#count_person').val(contact_count);
        contact_person_array.pop('0');
        $('#contact_person_array').val(contact_person_array);
    });
    /* $(document).on('click', '.removeAdditionalEmail', function () {
     
     $('#' + $(this).attr('id')).prev("div").remove();
     $('#' + $(this).attr('id')).remove();
     company_limit++;
     });*/
    var contact_person_mobile_limit = 3;
    var x = 1;
    $('body').on('click', '.add_contact_mobile', function ()
    {
        var main_id = this.id;
        main_id = main_id.split('_');
        var mobilecheck = "c_m_v_" + main_id[3];

        //alert($('.main'))
        //alert($('#cp1 .contact_email_value').length);

        mobilecheck = mobilecheck + ($(":input[id^=" + mobilecheck + "]").length);
//            alert(emailcheck);

        contact_person_mobile_count = mobilecheck.split('_')[3].substring(1);
//            alert(contact_person_email_count);
        if (contact_person_mobile_count < contact_person_mobile_limit) {

            var contact_mobile_html = $(this).prev().first().children().html();
//                var contact_mobile_html = '<input name="contact_mobile1[]" value="" class="form-control mobile_validation" id="c_m_v_1" data-parsley-required-message="Cell number is required" data-parsley-trigger="change focusout" data-parsley-type="digits" data-parsley-minlength="10" data-parsley-maxlength="10" data-parsley-id="34" type="text">'+
//                                          '<label>Mobile Number</label>'+
//                                          '<span></span>';

            $(this).prev().append("<div class='styled-input'>" + contact_mobile_html + "</div>" + '<i id="additionalPhone' + x + '" class="fa fa-remove removeAdditionalCell" />');
            $('input').keyup(function () {
                if ($(this).val().length > 0) {
                    $(this).addClass('not-empty');
//                           $('.mobile_validation').attr('data-parsley-type','digits')
//                                        .attr('data-parsley-trigger','change focusout')
//                                        .attr('data-parsley-minlength','10')
//                                        .attr('data-parsley-maxlength','10')
//                                        .parsley();

                } else {
                    $(this).removeClass('not-empty');
//                                   $('.mobile_validation').attr('data-parsley-type','digits')
//                                        .attr('data-parsley-trigger','change focusout')
//                                        .attr('data-parsley-minlength','10')
//                                        .attr('data-parsley-maxlength','10')
//                                        .parsley().destroy();
                }
            });
            $(".mobile_validation").mask("(999) 999-9999");
            contact_person_mobile_count++;
            x++;
            //$(this).parent().parent().append('<div class="col-md-10">'+contact_mobile_html+'<div');
        } else {
            alert('You can add contact person mobile upto 2 only');

        }
    });
    $(document).on('click', '.removeAdditionalCell', function () {

        $('#' + $(this).attr('id')).prev("div").remove();
        $('#' + $(this).attr('id')).remove();
        contact_person_mobile_count--;

    });
    var contact_person_email_limit = 3;
    var x = 1;
    $('body').on('click', '.add_contact_email', function ()
    {
        var main_id = this.id;
        main_id = main_id.split('_');
        var emailcheck = "c_e_value_" + main_id[3];

        //alert($('.main'))
        //alert($('#cp1 .contact_email_value').length);

        emailcheck = emailcheck + ($(":input[id^=" + emailcheck + "]").length);
//            alert(emailcheck);

        contact_person_email_count = emailcheck.split('_')[3].substring(1);
//            alert(contact_person_email_count);

        if (contact_person_email_count < contact_person_email_limit) {

            var l1 = contact_e.length;
            var l2 = l1 - 1;

            contact_e[l2] = contact_e[l2] + 1;
            $('#contact_email_array').val(contact_e);
            var contact_email_html = $(this).prev().first().children().html();

//                var contact_email_html =   '<input name="contact_email1[]" value="" id="c_e_value_1" class="form-control contact_email_value email_validation" data-parsley-required-message="Email is required" data-parsley-trigger="change focusout" data-parsley-type="email" data-parsley-id="28" type="text">'+
//                                           '<label>Enter Email</label>'+
//                                           '<span></span>';
            // console.log(contact_email_html);
            $(this).prev().append("<div class='styled-input'>" + contact_email_html + "</div>" + '<i id="additionalEmail' + x + '" class="fa fa-remove removeAdditionalEmail" />');
            $('input').keyup(function () {
                if ($(this).val().length > 0) {
                    $(this).addClass('not-empty');
                    $('.email_validation').attr('data-parsley-type', 'email')
                            .attr('data-parsley-trigger', 'change focusout')
                            .parsley();


                } else {
                    $(this).removeClass('not-empty');
                    $('.email_validation').removeAttr('data-parsley-type', 'email')
                            .removeAttr('data-parsley-trigger', 'change focusout')
                            .parsley().destroy();


                }
            });
            contact_person_email_count++;
            x++;
        } else {
            alert('You can add contact person email upto 2 only');

        }
    });
    $(document).on('click', '.removeAdditionalEmail', function () {

        $('#' + $(this).attr('id')).prev("div").remove();
        $('#' + $(this).attr('id')).remove();
        contact_person_email_count--;
    });

//        $('body').on('click', '.add_sales_mobile', function ()
//        {
//            var sales_email_html = $(this).parent().parent().children().html();
//            $(this).parent().parent().append('<div class="col-md-10">' + sales_email_html + '</div');
//
//        });
//        $('body').on('click', '.add_sales_email', function ()
//        {
//            var sales_mobile_html = $(this).parent().parent().children().html();
//            $(this).parent().parent().append('<div class="col-md-10">' + sales_mobile_html + '</div>');
//
//        });
    var contact_limit = 3;
    $('#add_contact_person').click(function ()
    {
        if (contact_count < contact_limit) {

            contact_count++;
            $('#count_person').val(contact_count);
            var l3 = contact_e.length;
            var l4 = l3 - 1;
            console.log($('#contact_email'));
            contact_e.push(1);
            contact_person_array.push('0');
            $('#contact_person_array').val(contact_person_array);
            var eleLength = $('#contact_person')[0].childElementCount;
            var element = $('#contact_person')[0].children;
            $(element).each(function (index) {
                if (index === eleLength - 1) {

//                        $(this).append('<h2>' + "Contact Person " + contact_count + '</h2>');

                    $(this).append('<div class="main" id="cp' + contact_count + '">' +
                            '<h2>' + "Contact Person " + contact_count + '</h2>' +
                            '<div class="row">' +
                            '<div class="col-md-6">' +
                            '<div class="col-md-12 col-sm-12">' +
                            '<div class="input-wrapper full-width">' +
                            '<div class="styled-input">' +
                            '<input type="text" name="contact_name[]" class="form-control" data-parsley-trigger="change focusout">' +
                            '<label>Name</label>' +
                            '<span></span>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '<div class="col-md-12 col-sm-12">' +
                            '<div id="contact_email" class="input-wrapper full-width">' +
                            '<div class="styled-input">' +
                            '<input type="text" id="c_e_value_' + contact_count + '"  class="form-control email_validation contact_email_value"  name=' + "contact_email" + contact_count + '[]' + ' data-parsley-trigger="change focusout" data-parsley-type="email">' +
                            '<label>Email</label>' +
                            '<span></span>' +
                            '</div>' +
                            '</div>' +
                            '<a type="button" class="btn btn-primary custom-btn input-btn add_contact_email" id="add_contact_email_' + contact_count + '"><span>+ More</span></a>' +
                            '</div>' +
                            '<div class="col-md-12 col-sm-12">' +
                            '<div class="input-wrapper full-width">' +
                            '<div class="styled-input">' +
                            '<input type="text" name="contact_fax[]" class="form-control fax_number_validation" data-parsley-trigger="change focusout">' +
                            '<label>Fax Number</label>' +
                            '<span></span>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '<div class="col-md-6">' +
                            '<div class="col-md-12 col-sm-12">' +
                            '<div class="input-wrapper full-width">' +
                            '<div class="styled-input">' +
                            '<input type="text" name="designation[]" class="form-control" data-parsley-trigger="change focusout">' +
                            '<label>Position</label>' +
                            '<span></span>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '<div  class="col-md-12 col-sm-12">' +
                            '<div id="contact_mobile" class="input-wrapper full-width">' +
                            '<div class="styled-input">' +
                            '<input type="text"  class="form-control mobile_validation" name=' + "contact_mobile" + contact_count + '[]' + ' data-parsley-trigger="change focusout" id="c_m_v_' + contact_count + '" >' +
                            '<label>Mobile Number</label>' +
                            '<span></span>' +
                            '</div>' +
                            '</div>' +
                            '<a  type="button" class="btn btn-primary custom-btn input-btn add_contact_mobile" id="add_contact_mobile_' + contact_count + '"><span>+ More</span></a>' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '<i class="fa fa-remove removeContactPerson"></i>' +
                            '</div>');

                }
                $('input').keyup(function () {
                    if ($(this).val().length > 0) {
                        $(this).addClass('not-empty');
                    } else {
                        $(this).removeClass('not-empty');
                    }
                });
                $(".fax_number_validation").mask("(999) 999-9999");
                $(".mobile_validation").mask("(999) 999-9999");


            });
        } else {
            alert('You can add contact person upto 2 only');

        }
    });
//        $('#add_sales_person').click(function ()
//        {
//            sales_count++;
//            $('#count_sales').val(sales_count);
//            sales_person_array.push('1');
//            $('#sales_person_array').val(sales_person_array);
//            var eleLength = $('#sales_person')[0].childElementCount;
//            var element = $('#sales_person')[0].children;
//            $(element).each(function (index) {
//                if (index === eleLength - 1) {
//                    $(this).append('<label>' + "Sales Person " + sales_count + '</label>');
//                    $(this).attr('id', 'sales_' + sales_count);
//                    $(this).append(
//                            '<div class="main">' +
//                            '<div class="form-group col-md-10">' +
//                            '<input type="text" name="sales_person_name[]" class="form-control" placeholder="Enter Name">'
//                            + '</div>'
//                            + '<div id="sales_email">' +
//                            '<div class="form-group col-md-10">' +
//                            '<input type="text" class="form-control" placeholder="Enter Email" name=' + "sales_person_email" + sales_count + '[]' + '>' +
//                            '</div>' +
//                            '<div class="form-group col-md-2">' +
//                            '<input type="button" class="add_sales_email" value="+More">' +
//                            '</div>' +
//                            '</div>' +
//                            '<div id="sales_mobile">' +
//                            '<div class="form-group col-md-10">' +
//                            '<input type="text" class="form-control" placeholder="Cell Number" name=' + "sales_person_mobile" + sales_count + '[]' + ' >' +
//                            '</div>' +
//                            '<div class="form-group col-md-2">' +
//                            '<input type="button" class="add_sales_mobile" value="+More">' +
//                            '</div>' +
//                            '</div>' +
//                            '<div class="form-group col-md-10">' +
//                            '<input type="text" name="sales_person_fax[]" class="form-control" placeholder="Fax Number">' +
//                            '</div>' +
//                            '</div>');
//                }
//            });
//        });
    $(".business_phone_validation").mask("(999) 999-9999");


    $(".business_phone_validation").on("blur", function () {
        var last = $(this).val().substr($(this).val().indexOf("-") + 1);

        if (last.length == 5) {
            var move = $(this).val().substr($(this).val().indexOf("-") + 1, 1);

            var lastfour = last.substr(1, 4);

            var first = $(this).val().substr(0, 9);

            $(this).val(first + move + '-' + lastfour);
        }
    });

    $(".fax_number_validation").mask("(999) 999-9999");


    $(".fax_number_validation").on("blur", function () {
        var last = $(this).val().substr($(this).val().indexOf("-") + 1);

        if (last.length == 5) {
            var move = $(this).val().substr($(this).val().indexOf("-") + 1, 1);

            var lastfour = last.substr(1, 4);

            var first = $(this).val().substr(0, 9);

            $(this).val(first + move + '-' + lastfour);
        }
    });
});


//    $('#create_address_book_form').on('submit', function (event) {
//
//        event.preventDefault();
//        var submit_url = $(this).attr("action");
//        $(".errorsection").each(function () {
//            $(this).html("");
//        });
//        $.ajax({
//            type: 'POST',
//            url: submit_url,
//            dataType: "json",
//            async: false,
//            cache: false,
//            data: $("#create_address_book_form").serialize(),
//            success: function (response) {
//                if (response.status == true) {
//                    var div = $('<div/>', {
//                        class: 'alert alert-success',
//                        title: 'now this div has a title!'
//                    });
//                    div.html(response.message);
//                    $(".contact-status").html(div);
//                    $('html, body').animate({
//                        'scrollTop': $("body").position().top
//                    });
//                    setTimeout(function () {
//                        location.reload();
//                    }, 2000);
//                } else {
//                    var div = $('<div/>', {
//                        class: 'alert alert-error',
//                        title: 'now this div has a title!'
//                    });
//                    div.html(response.message);
//                    // $(".contact-status").html(div);
//                    $('html, body').animate({
//                        'scrollTop': $(".contact-status").position().top
//                    });
//
//                }
//            }, error: function (response) {
//                console.log(response);
//                if (response.status === 404 && response.responseJSON.status == false) {
//                    $.each(response.responseJSON.error, function (key, value) {
//                        $(".errorsection").each(function () {
//                            var name = $(this).data('name');
//                            var index = $(this).data('index');
//                            if (index != "" && (parseInt(index) >= +0)) {
//                                index = "." + index;
//                            }
//                            if (name + index == key) {
//                                //alert(name+index);
//                                $(this).html(value).show();
//                            }
//                        });
//                    });
//                }
//            }
//        });
//    });
</script>
@stop
