<div class="action-icon">

    <a href="{{url('customer/contact/'.$contact->id.'/edit')}}" class="btn btn-secondary item red-tooltip" data-toggle="tooltip" data-placement="bottom" title="Edit"><span class="icon-pencil-edit-button"></span></a>

    <a data-id="{{$contact->id}}" href="#" class="btn btn-secondary item red-tooltip remove-record" data-toggle='modal' data-toggle="tooltip" data-target='#contact-confirmation-modal' data-placement="bottom" title="Remove"><span class="icon-cross-remove-sign"></span></a>

</div>