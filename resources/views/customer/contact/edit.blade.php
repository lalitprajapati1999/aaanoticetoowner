<style type="text/css">
    .errorsection {
        display:none;
    }
    .select2class :focus{
    outline: 0;
    height: 47px !important;
    border-bottom : 1px solid #032c61 !important;
}
</style>
@extends('adminlte::page')
@section('content')
<div class="main-content">
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="address">

            <!-- Default box -->
            <form method="post" class="form-horizontal" id="edit_address_book_form" data-parsley-validate="" action="{{ url('customer/contact/edit/'.$id) }}">
                {!! csrf_field() !!}
                <section class="address-book-add">
                    <div class="dashboard-wrapper">
                        <div class="contact-status"></div>
                        <div class="dashboard-heading">
                            <h1><span> Edit Contact</span></h1>
                        </div>
                        {!! csrf_field() !!}
                        <div class="dashboard-inner-body">
                            <div class="dash-subform">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6">
                                                <div class="input-wrapper full-width">
                                                    <div class="styled-input">
                                                        <input type="text" name="company_name" value="{{ old('company_name')  ? old('company_name'): $contact->company_name}}" class="form-control" data-parsley-required="true" data-parsley-required-message="Company Name is required" data-parsley-trigger="change focusout">
                                                        <label>Company or Contact Person Name<span class="mandatory-field">*</span></label>
                                                        <span></span>
                                                        @if ($errors->has('company_name'))
                                                        <p class="help-block">
                                                            <strong>{{ $errors->first('company_name') }}</strong>
                                                        </p>
                                                        @endif
    <!--                                                        <p class="help-block errorsection" data-name="company_name" data-index=""></p>-->

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <div class="input-wrapper full-width">
                                                 <label class="control-label" for="textarea">Attn/Additional Recipient Name<span class="mandatory-field"></span></label>
                                                    <div class="styled-input">
                                                        <textarea class="form-control recipient_attns"  type="text" name="attn" id="attn" data-parsley-trigger="change focusout" >{{old('attn')  ? old('attn'): $contact->attn}}</textarea>
                                                        <span></span>
                                                        @if ($errors->has('attn'))
                                                        <p class="help-block">
                                                            <strong>{{ $errors->first('attn') }}</strong>
                                                        </p>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="mailing_address" value="{{ old('mailing_address')  ? old('mailing_address'): $contact->mailing_address}}" class="form-control" data-parsley-required="true" data-parsley-required-message="Mailing Address is required" data-parsley-trigger="change focusout" data-parsley-maxlength="100">
                                                <label>Mailing Address<span class="mandatory-field">*</span></label>
                                                <span></span>
                                                @if ($errors->has('mailing_address'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('mailing_address') }}</strong>
                                                </p>
                                                @endif
                                            <!--<p class="help-block errorsection" data-name="mailing_address" data-index=""></p>-->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input {{ $errors->has('zip') ? 'has-error' : '' }}">
                                            <label class="lableall">Zip Code<span class="mandatory-field">*</span></label>
                                                <input class="form-control"  type="hidden" name="selected_address_book" value="" id="selected_address_book">
                                                <input class="form-control new_addresscontact_zip"  type="text" name="new_addresscontact_zip" value="{{old('zip')?old('zip'):$contact->zip}}" id="new_addresscontact_zip">
                                                <input  class="form-control addresscontact_zip" type="hidden" name="zip"  value="{{old('zip')?old('zip'):$contact->zip}}"  class="form-control" data-parsley-required="true" data-parsley-required-message="Zip code is required" data-parsley-trigger="change focusout" data-parsley-type="digits" data-parsley-minlength="5" data-parsley-maxlength="5">
                                                {{-- <input class="form-control addresscontact_zip"  type="hidden" name="contact_zip" value="{{old('contact_zip')}}" id="contact_zip"> --}}
                                                <!-- <input type="text" id='employeeid' readonly> -->
                                                <input class="contact_zipurl"  type="hidden" name="contact_zipurl" id="contact_zipurl" value="{{ url('admin/ZipCodeLookups') }}">
                                                <input class="contact_stateurl"  type="hidden" name="contact_stateurl" id="contact_stateurl" value="{{ url('admin/getStateName') }}">
                                                <span></span>
                                                <p class="help-block" >
                                                    <strong id="zip-error"></strong>
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                    <?php $city_id = $contact->city_id ?>
<!--                                    <input type="hidden" name="hidden_city" value="{{$city_id}}" id="hidden_city"/>-->

                                     <div class="col-md-6 col-sm-6">
                                        <div class="select-opt select2-container col-md-12 select2class">
                                            <label class="col-md-12 control-label" for="textinput">City<span class="mandatory-field">*</span></label> 
                                                <select class="js-example-tags form-control load_ajax_cities"  name="city" id="city_value" data-parsley-required="true" data-parsley-required-message="City is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true">
                                                    <option value="">Select</option> 
                                                </select>
                                                 @if ($errors->has('city'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('city') }}</strong>
                                                </p>
                                                @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="select-opt">
                                            <label class="col-md-12 control-label" for="textinput">State<span class="mandatory-field">*</span></label>
                                            <select name="state" class="selectpicker addresscontact_state_id" data-parsley-required="true" data-parsley-required-message="State is required" data-parsley-trigger="change focusout" id="edit_state" data-show-subtext="true" data-live-search="true">
                                                <option value="">Select</option>
                                                @foreach($states as $state)
                                                <option value="{{$state->id}}"   @if($state->id== $contact->state_id)  selected="selected"  @endif > {{ucfirst(trans($state->name))}}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('state'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('state') }}</strong>
                                            </p>
                                            @endif
                                    <!--<p class="help-block errorsection" data-name="state" data-index=""></p>-->
                                        </div>
                                    </div>
                                    <!--                                    <div class="col-md-6 col-sm-6">
                                    
                                                                            <div class="select-opt">
                                                                                <label class="col-md-12 control-label" for="textinput">County<span class="mandatory-field">*</span></label>  
                                                                                <select name="country"  class="selectpicker" data-show-subtext="true" data-live-search="true" data-parsley-required="true" data-parsley-required="true" data-parsley-required-message="County is required" data-parsley-trigger="change focusout">
                                                                                    <option value="">Select</option>
                                                                                    @foreach($countries as $country)
                                                                                    <option value="{{$country->id}}" {{old('country')?old('country'): ($country->id== $contact->country_id ? 'selected="selected"' : '' )}} > {{ucfirst(trans($country->name))}}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                                <p class="help-block errorsection" data-name="country" data-index=""></p>
                                                                                @if ($errors->has('country'))
                                                                                <p class="help-block">
                                                                                    <strong>{{ $errors->first('country') }}</strong>
                                                                                </p>
                                                                                @endif
                                                                            </div>
                                                                        </div>-->
                                
                                    <div class="col-md-6 col-sm-6 clear-b">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="email" name="business_email" value="{{old('business_email')?old('business_email'):$contact->email}}" class="form-control"  data-parsley-type="email">
                                                <label>Business Email</label>
                                                <span></span>
                                                <!--<p class="help-block errorsection" data-name="business_email" data-index=""></p>-->
                                                @if ($errors->has('business_email'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('business_email') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div  class="col-md-6 col-sm-6 business_phone_wrapper clear-b">

                                        <?php
                                        $business_phone = $contact->phone;
                                        $business_phone_arr = explode(',', $business_phone);
                                        ?>
                                        @if(isset($business_phone) && $business_phone != '')
                                        @foreach($business_phone_arr As $ph=>$each_business_phn)
                                        <div  class="input-wrapper full-width">
                                            <div class="styled-input">

                                                <input type="text" name="phone[]" value="{{$each_business_phn}}" class="form-control business_phone_validation">
                                                <label>Business Phone</label>
                                                <span></span>
                                                <!--<p class="help-block errorsection" data-name="business_phone" data-index="0"></p>-->
                                                @if ($errors->has('phone.0'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('phone.0') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                            @if($ph != 0)
                                            <i class="fa fa-remove removeBusinessPhone"></i>
                                            @endif
                                        </div>
                                        @endforeach
                                        @else
                                        <div  class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="phone[]" value="" class="form-control business_phone_validation">
                                                <label>Business Phone</label>
                                                <span></span>
                                                <!--<p class="help-block errorsection" data-name="business_phone" data-index="0"></p>-->
                                                @if ($errors->has('phone.0'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('phone.0') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                        @endif


                                        @if(isset($business_phone) && $business_phone != '')
                                        <?php
                                        $business_phone = $contact->phone;
                                        $business_phone_arr = explode(',', $business_phone);
                                        $count = count($business_phone_arr);
                                        ?>

                                        @endif
                                        @if(isset($count) && $count<3)

                                        <div class="plus-btn">
                                            <a type ="button" name="add_business_phone" id="add_business_phone" class="btn btn-primary custom-btn input-btn" value="+ More"><span>+ More</span></a>
                                        </div>
                                        @elseif(isset($count) && $count>=3)
                                        @else
                                        <div class="plus-btn">
                                            <a type ="button" name="add_business_phone" id="add_business_phone" class="btn btn-primary custom-btn input-btn" value="+ More"><span>+ More</span></a>
                                        </div>
                                        @endif
                                    </div>

                                </div>
                            </div>

                            <div class="dash-subform addcontactperson">
                                <div class="row">
                                    @if(count($contact->contactPersons()->get())>0 )

                                    @foreach($contact->contactPersons()->get() AS $k=>$each_contact_person)
                                    <div id="contact_person{{$k+1}}" class='total_div'>
                                        <h2>Contact Person {{$k+1}}</h2>
                                        <div class="main" id="cp{{$k+1}}">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="col-md-12 col-sm-12">
                                                        <div class="input-wrapper full-width">
                                                            <div class="styled-input">
                                                                <input type="text" name="contact_name[]" value="{{ $each_contact_person->name}}" class="form-control"  data-parsley-required-message="Name is required" data-parsley-trigger="change focusout" data-parsley-maxlength="50">
                                                                <label>Name</label>
                                                                <span></span>
                                                                @if ($errors->has('contact_name.0'))
                                                                <p class="help-block">
                                                                    <strong>{{ $errors->first('contact_name.0') }}</strong>
                                                                </p>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 col-sm-12">
                                                        <div id="contact_email{{$k+1}}" class="input-wrapper full-width">

                                                            <?php
                                                            $email = $each_contact_person->email;
                                                            $email_arr = explode(',', $email);
                                                            ?>
                                                            @if(count($email_arr)>0)
                                                            @foreach($email_arr As $e=>$each_email)
                                                            <div class="styled-input">
                                                                <input type="text" name="contact_email{{$k+1}}[]" value="{{$each_email}}" id="c_e_value_{{$k+1}}"  class="form-control contact_email_value email_validation"  class="form-control contact_email_value" data-parsley-required-message="Email is required" data-parsley-trigger="change focusout" data-parsley-type="email">
                                                                <label>Email</label>
                                                                <span></span>
                                                                @if ($errors->has('contact_email.0'))
                                                                <p class="help-block">
                                                                    <strong>{{ $errors->first('contact_email1.0') }}</strong>
                                                                </p>
                                                                @endif
                                                            </div>

                                                            @endforeach
                                                            @endif
                                                        </div>
                                                        <a type="button" class="btn btn-primary custom-btn input-btn add_contact_email"id="add_contact_email_{{$k+1}}"><span>+ More</span></a>
                                                    </div>
                                                    <div class="col-md-12 col-sm-12">
                                                        <div class="input-wrapper full-width">
                                                            <div class="styled-input">
                                                                <input type="text" name="contact_fax[]" value="{{ $each_contact_person->fax}}" class="form-control  fax_number_validation"  data-parsley-required-message="Fax is required" data-parsley-trigger="change focusout">
                                                                <label>Fax Number</label>
                                                                <span></span>
                                                                @if ($errors->has('contact_fax.0'))
                                                                <p class="help-block">
                                                                    <strong>{{ $errors->first('contact_fax.0') }}</strong>
                                                                </p>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="col-md-12 col-sm-12">
                                                        <div class="input-wrapper full-width">
                                                            <div class="styled-input">
                                                                <input type="text" name="designation[]" value="{{ $each_contact_person->designation}}" class="form-control"  data-parsley-required-message="Designation is required" data-parsley-trigger="change focusout" data-parsley-maxlength="50">
                                                                <label>Position</label>
                                                                <span></span>
                                                                @if ($errors->has('designation.0'))
                                                                <p class="help-block">
                                                                    <strong>{{ $errors->first('designation.0') }}</strong>
                                                                </p>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div  class="col-md-12 col-sm-12">
                                                        <div id="contact_mobile{{$k+1}}" class="input-wrapper full-width">

                                                            <?php
                                                            $mobile = $each_contact_person->mobile;
                                                            $mobile_arr = explode(',', $mobile);
                                                            ?>
                                                            @if(count($mobile_arr)>0)
                                                            @foreach($mobile_arr As $m=>$each_mobile)
                                                            <div class="styled-input">
                                                                <input type="text"  name="contact_mobile{{$k+1}}[]" id="c_m_v_{{$k+1}}" value="{{$each_mobile}}"  class="form-control mobile_validation"  data-parsley-required-message="Cell number is required" data-parsley-trigger="change focusout">
                                                                <label>Mobile Number</label>
                                                                <span></span>
                                                                @if ($errors->has('contact_mobile.0'))
                                                                <p class="help-block">
                                                                    <strong>{{ $errors->first('contact_mobile.0') }}</strong>
                                                                </p>
                                                                @endif
                                                            </div>
                                                            @endforeach
                                                            @endif

                                                        </div>
                                                        <a  type="button" class="btn btn-primary custom-btn input-btn add_contact_mobile" id="add_contact_mobile_{{$k+1}}"><span>+ More</span></a>
                                                    </div>
                                                </div>
                                            </div>
                                            @if($k != 0)
                                         <!--<i class="fa fa-remove removeContactPerson"></i>-->
                                            @endif
                                        </div>
                                    </div>
                                    @endforeach
                                    @else
                                    <div id="contact_person1" class='total_div'>
                                        <h2>Contact Person</h2>
                                        <div class="main" id="cp1">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="col-md-12 col-sm-12">
                                                        <div class="input-wrapper full-width">
                                                            <div class="styled-input">
                                                                <input type="text" name="contact_name[]" class="form-control"  data-parsley-required-message="Name is required" data-parsley-trigger="change focusout" data-parsley-maxlength="50">
                                                                <label>Name</label>
                                                                <span></span>
                                                                @if ($errors->has('contact_name.0'))
                                                                <p class="help-block">
                                                                    <strong>{{ $errors->first('contact_name.0') }}</strong>
                                                                </p>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 col-sm-12">
                                                        <div id="contact_email1" class="input-wrapper full-width">


                                                            <div class="styled-input">
                                                                <input type="email" name="contact_email1[]" id="c_e_value_1"  class="form-control contact_email_value email_validation" data-parsley-required-message="Email is required" data-parsley-trigger="change focusout" data-parsley-type="email">
                                                                <label>Email</label>
                                                                <span></span>
                                                                @if ($errors->has('contact_email.0'))
                                                                <p class="help-block">
                                                                    <strong>{{ $errors->first('contact_email1.0') }}</strong>
                                                                </p>
                                                                @endif
                                                            </div>


                                                        </div>
                                                        <a type="button" class="btn btn-primary custom-btn input-btn add_contact_email" id="add_contact_email_1"><span>+ More</span></a>
                                                    </div>
                                                    <div class="col-md-12 col-sm-12">
                                                        <div class="input-wrapper full-width">
                                                            <div class="styled-input">
                                                                <input type="text" name="contact_fax[]" class="form-control fax_number_validation"  data-parsley-required-message="Fax is required" data-parsley-trigger="change focusout">
                                                                <label>Fax Number</label>
                                                                <span></span>
                                                                @if ($errors->has('contact_fax.0'))
                                                                <p class="help-block">
                                                                    <strong>{{ $errors->first('contact_fax.0') }}</strong>
                                                                </p>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="col-md-12 col-sm-12">
                                                        <div class="input-wrapper full-width">
                                                            <div class="styled-input">
                                                                <input type="text" name="designation[]" class="form-control"  data-parsley-required-message="Designation is required" data-parsley-trigger="change focusout" data-parsley-maxlength="50">
                                                                <label>Position</label>
                                                                <span></span>
                                                                @if ($errors->has('designation.0'))
                                                                <p class="help-block">
                                                                    <strong>{{ $errors->first('designation.0') }}</strong>
                                                                </p>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div  class="col-md-12 col-sm-12">
                                                        <div id="contact_mobile1" class="input-wrapper full-width">


                                                            <div class="styled-input">
                                                                <input type="text"  name="contact_mobile1[]" id="c_m_v_1"   class="form-control mobile_validation"  data-parsley-required-message="Cell number is required" data-parsley-trigger="change focusout" >
                                                                <label>Mobile Number</label>
                                                                <span></span>
                                                                @if ($errors->has('contact_mobile.0'))
                                                                <p class="help-block">
                                                                    <strong>{{ $errors->first('contact_mobile.0') }}</strong>
                                                                </p>
                                                                @endif
                                                            </div>


                                                        </div>
                                                        <a  type="button" class="btn btn-primary custom-btn input-btn add_contact_mobile" id="add_contact_mobile_1"><span>+ More</span></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                    <div class="col-md-12 addBefore">
                                        <div class="add-new-btn">
                                            <a type="button" id="add_contact_person" class="btn btn-primary custom-btn addcontact"><span>+ Add Another Contact Person</span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="text" name="contact_person_array[]" id="contact_person_array" hidden="">
                            <input type="text" name="contact_email_array[]" id="contact_email_array" hidden="">
                            <input type="text" name="contact_count" value="" id="count_person" hidden="">
                            <div class="col-md-12 col-sm-12">
                                <div class="row">
                                    <div class="form-bottom-btn">
                                        <a href="{{URL::previous()}}" class="btn btn-primary custom-btn customb-btn"><span>Cancel</span></a>
                                        <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>Save Contact</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </form>
        </div>
    </div>
</div>
<input type="hidden" value="{{$contact->city_name}},{{$contact->city_id}}" id="oldcity">
@endsection

@section('frontend_js')
<script type="text/javascript" src="{{ asset('js/jquery-ui.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function () {
  $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
    });
    $(".new_addresscontact_zip").autocomplete({
        source: function(request, response) {
            // Fetch data
            var urlzipcode = $('#contact_zipurl').val();
            $('.addresscontact_zip').val(request.term); // save selected id to input
            $.ajax({
                url: urlzipcode,
                type: 'post',
                dataType: "json",
                data: {
                    zipcode: request.term
                },
                success: function(data) {
                    response(data);
                }
            });
        },
        select: function(event, ui) {
            // Set selection
            $('.new_addresscontact_zip').val(ui.item.label); // display the selected text
           
            return false;
        }
    });
    $('.new_addresscontact_zip').on('autocompleteselect', function (e, ui) {
        var bookid = ui.item.label;
        var res = bookid.split(",");
         $('.addresscontact_zip').val(res[0]); // save selected id to input
        $('#city_value')
            .empty()
            .append($("<option/>")
                .text(res[1]))
            .val(res[1])
            .trigger("change");
        $('.city_value').attr('disabled', 'disabled');
        var contact_stateurl = $('#contact_stateurl').val();
        $.ajax({
            url: contact_stateurl,
            type: 'post',
            dataType: "json",
            data: {
                statename: res[2]
            },
            success: function(data) {
                $(".addresscontact_state_id").val(data.id).trigger('change');
               // $('.addresscontact_state_id').attr('disabled', 'disabled');
            }
        });
    });
    var zipcode = $('.addresscontact_zip').val();
    var cityName =$('#oldcity').val();
    var city_name_arr=cityName.split(',');
    var urlzipcode = $('#contact_zipurl').val();
        $.ajax({
        url: urlzipcode,
        type: 'post',
        dataType: "json",
        data: {
            zipcode:zipcode,
            cityName:city_name_arr[1]
        },
        success: function(data) {
            $(".new_addresscontact_zip").val(data[0].label);
            var bookid = data[0].label;
            var res = bookid.split(",");
            $('#city_value')
            .empty()
            .append($("<option/>")
            .text(res[1]))
            .val(res[1])
            .trigger("change");
            $('.city_value').attr('disabled', 'disabled');
            var contact_stateurl = $('#contact_stateurl').val();
            $.ajax({
                url: contact_stateurl,
                type: 'post',
                dataType: "json",
                    data: {
                        statename: res[2]
                    },
                    success: function(data) {
                        $(".addresscontact_state_id").val(data.id).trigger('change');
                       // $('.addresscontact_state_id').attr('disabled', 'disabled');
                    }
            });
        }
                          
    });


     $('input,textarea,select').on('focus', function(e) {
            e.preventDefault();
            $(this).attr("autocomplete", "nope");  
        });
  var city_nm =$('#oldcity').val();
  var city_arr=city_nm.split(',');

        var option = new Option(city_arr[0], city_arr[1], true, true);
       $("#city_value").append(option).trigger('change');

     var cityUrl = "{{ url('customer/city-list') }}";
       
             $(".load_ajax_cities").select2({
                tags:true,
              ajax: { 
               url: cityUrl,
               type: "get",
               dataType: 'json',
               //delay: 250,
               data: function (params) {
                return {
                  name: params.term // search term
                };
               },
               processResults: function (response) {
                 return {
                    results: response
                 };
               },
               cache: true
              }
             });

    //Start Autocomplete City
    src_county = "{{ url('customer/city/autocompletecityname') }}";

    $("#city_value").autocomplete({

        source: function (request, response) {
            $.ajax({
                url: src_county,
                dataType: "json",
                data: {
                    term: request.term
                },
                beforeSend: function () {
                    $('#cityloader').show();
                },
                complete: function () {
                    $('#cityloader').hide();
                },
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            value: item.value,
                            id: item.id     // EDIT
                        }
                    }));

                },

            });
        },
        select: function (event, ui) {

            $("#city_value_id").val(ui.item.id);
            $("#city_value").val(ui.item.value);
        },
        change: function (event, ui) {
            if (ui.item == null) {
                $(this).val("");
                $(this).focusout();
                $(this).focus();
            }
        }
    });
    $(".mobile_validation").mask("(999) 999-9999");

    var contact_person_array = [];
    var sales_person_array = [];
    var contact_mobile_count = [];
    contact_person_array.push('0');
    sales_person_array.push('1');
    $('#contact_person_array').val(contact_person_array);
    $('#sales_person_array').val(sales_person_array);


    //contact persons
//        var business_phone = $('#business_phone').html();
    //  var contact_email = $('#contact_email').html();
    //  var contact_mobile = $('#contact_mobile').html();
    var business_phone = '<div class="input-wrapper full-width">' +
            '<div class="styled-input">' +
            '<input type="text" name="phone[]" class="form-control business_phone_validation">' +
            '<label>Business Phone</label>' +
            '<span></span>' +
            '<p class="help-block errorsection" data-name="business_phone" data-index=""></p>' +
            '</div>' +
            '<i class="fa fa-remove removeBusinessPhone"></i>' +
            '</div>';
    var sales_email = $('#sales_email').html();
    var sales_mobile = $('#sales_mobile').html();
    var contact_person = $('#contact_person').html();
    var sales_person = $('#sales_person').html();
    var contact_count = $('.addcontactperson .total_div').length;
    var contact_e = [];
    var sales_count = 1;
    contact_e.push(1);

    $('#count_person').val(contact_count);
    $('#count_sales').val(sales_count);
    var p_length = $('input[name*="phone"]').length;
    var business_phone_count = 1;
    var business_phone_limit = 3-p_length;
    var contact_person_mobile_count = 1;
    var contact_person_email_count = 1;
    
    $('#add_business_phone').click(function ()
    {
        //                    console.log(business_phone);
        //                    var eleLength = $('#business_phone')[0].childElementCount;
        //                    var element = $('#business_phone')[0].children;
        //                    $(element).each(function (index) {
        //                        if (index === eleLength - 1) {
        //                            $(this).append(business_phone);
        //                        }
        //                    });
        if (business_phone_count <= business_phone_limit) {

//                $(this).parent().prev().append('<div class="input-wrapper full-width">'+
//                            '<div class="styled-input">' +
//                        '<input name="phone[]" class="form-control business_phone_validation" required="" type="text">' +
//                        '<label>Business Phone</label>' +
//                        '<span></span>' +
//                        '</div>'+
//                '<i class="fa fa-remove removeBusinessPhone"></i>'+
//                '</div>');
            business_phone_count++;

            $('.business_phone_wrapper').first().append(business_phone);
            $('input').keyup(function () {
                if ($(this).val().length > 0) {
                    $(this).addClass('not-empty');

                } else {
                    $(this).removeClass('not-empty');

                }
            });
            $(".business_phone_validation").mask("(999) 999-9999");
        } else {
            alert('You can add business phone upto 2 only');

        }


    });
    $('.business_phone_wrapper').on('click', '.removeBusinessPhone', function (e) {
        e.preventDefault();
        $(this).parent().remove(); //Remove field html
        business_phone_count--; //Decrement field counter
    });
    $('body').on('click', '.removeContactPerson', function (e) {
        e.preventDefault();
        $(this).parent().parent().remove(); //Remove field html
        contact_count--; //Decrement field counter
    });
    var contact_person_mobile_limit = 3;
    var x = 1;
    $('body').on('click', '.add_contact_mobile', function ()
    {

        var main_id = this.id;

        main_id = main_id.split('_');
        var mobilecheck = "c_m_v_" + main_id[3];
        mobilecheck = mobilecheck + ($(":input[id^=" + mobilecheck + "]").length);

        contact_person_mobile_count = mobilecheck.split('_')[3].substring(1);
        if (contact_person_mobile_count < contact_person_mobile_limit) {
            var contact_mobile_html = $(this).prev().first().children().html();
//                  var contact_mobile_html = '<input name="contact_mobile1[]" value="" class="form-control mobile_validation" id="c_m_v_1" data-parsley-required-message="Cell number is required" data-parsley-trigger="change focusout" data-parsley-type="digits" data-parsley-minlength="10" data-parsley-maxlength="10" data-parsley-id="34" type="text">'+
//                                          '<label>Mobile Number</label>'+
//                                          '<span></span>';

            $(this).prev().append("<div class='styled-input'>" + contact_mobile_html + "</div>" + '<i id="additionalPhone' + x + '" class="fa fa-remove removeAdditionalCell" />');
            $('input').keyup(function () {
                if ($(this).val().length > 0) {
                    $(this).addClass('not-empty');
//                        $('.mobile_validation').attr('data-parsley-type', 'digits')
//                                .attr('data-parsley-trigger', 'change focusout')
//                                .attr('data-parsley-minlength', '10')
//                                .attr('data-parsley-maxlength', '10')
//                                .parsley();
                } else {
                    $(this).removeClass('not-empty');
//                        $('.mobile_validation').attr('data-parsley-type', 'digits')
//                                .attr('data-parsley-trigger', 'change focusout')
//                                .attr('data-parsley-minlength', '10')
//                                .attr('data-parsley-maxlength', '10')
//                                .parsley().destroy();
                }
            });
            contact_person_mobile_count++;
            x++;
            $(".mobile_validation").mask("(999) 999-9999");

        } else {
            alert('You can add contact person mobile upto 2 only');

        }
    });
    $(document).on('click', '.removeAdditionalCell', function () {

        $('#' + $(this).attr('id')).prev("div").remove();
        $('#' + $(this).attr('id')).remove();
        // company_limit++;

    });
    var contact_person_email_limit = 3;
    var x = 1;
    $('body').on('click', '.add_contact_email', function ()
    {
        var main_id = this.id;
        main_id = main_id.split('_');
        var emailcheck = "c_e_value_" + main_id[3];

        //alert($('.main'))
        //alert($('#cp1 .contact_email_value').length);

        emailcheck = emailcheck + ($(":input[id^=" + emailcheck + "]").length);
//            alert(emailcheck);

        contact_person_email_count = emailcheck.split('_')[3].substring(1);
//            alert(contact_person_email_count);

        if (contact_person_email_count < contact_person_email_limit) {

            var l1 = contact_e.length;
            var l2 = l1 - 1;

            contact_e[l2] = contact_e[l2] + 1;
            $('#contact_email_array').val(contact_e);
            var contact_email_html = $(this).prev().first().children().html();
           
           // console.log(contact_email_html);
//                var contact_email_html =   '<input name="contact_email1[]" value="" id="c_e_value_1" class="form-control contact_email_value email_validation" data-parsley-required-message="Email is required" data-parsley-trigger="change focusout" data-parsley-type="email" data-parsley-id="28" type="text">'+
//                                           '<label>Enter Email</label>'+
//                                           '<span></span>';
            // console.log(contact_email_html);
            $(this).prev().append("<div class='styled-input'>" + contact_email_html + "</div>" + '<i id="additionalEmail' + x + '" class="fa fa-remove removeAdditionalEmail" />');
            $('input').keyup(function () {
                if ($(this).val().length > 0) {
                    $(this).addClass('not-empty');
                    $('.email_validation').attr('data-parsley-type', 'email')
                            .attr('data-parsley-trigger', 'change focusout')
                            .parsley();


                } else {
                    $(this).removeClass('not-empty');
                    $('.email_validation').removeAttr('data-parsley-type', 'email')
                            .removeAttr('data-parsley-trigger', 'change focusout')
                            .parsley().destroy();


                }
            });
            contact_person_email_count++;
            x++;
        } else {
            alert('You can add contact person email upto 2 only');

        }
    });
    $(document).on('click', '.removeAdditionalEmail', function () {

        $('#' + $(this).attr('id')).prev("div").remove();
        $('#' + $(this).attr('id')).remove();
        contact_person_mobile_count--;
    });

//                $('body').on('click', '.add_sales_mobile', function ()
//                {
//                    var sales_email_html = $(this).parent().parent().children().html();
//                    $(this).parent().parent().append(sales_email_html);
//
//                });
//                $('body').on('click', '.add_sales_email', function ()
//                {
//                    var sales_mobile_html = $(this).parent().parent().children().html();
//                    $(this).parent().parent().append(sales_mobile_html);
//
//                });
    var contact_limit = 3;

    $('#add_contact_person').click(function ()
    {
        if (contact_count < contact_limit) {

            contact_count++;
            $('#count_person').val(contact_count);
            var l3 = contact_e.length;
            var l4 = l3 - 1;
            contact_e.push(1);
            //  contact_person_array.push('0');
            //  $('#contact_person_array').val(contact_person_array);
//                    var eleLength = $('#contact_person')[0].childElementCount;
//                    var element = $('#contact_person')[0].children;
//                    $(element).each(function (index) {
//                        if (index === eleLength - 1) {


            $('<div id="contact_person' + contact_count + '" class="total_div">' +
                    '<h2>Contact Person ' + contact_count + '</h2>' +
                    '<div class="main" id="cp' + contact_count + '">' +
                    '<div class="row">' +
                    '<div class="col-md-6">' +
                    '<div class="col-md-12 col-sm-12">' +
                    '<div class="input-wrapper full-width">' +
                    '<div class="styled-input">' +
                    '<input type="text" name="contact_name[]" class="form-control" data-parsley-trigger="change focusout">' +
                    '<label>Name</label>' +
                    '<span></span>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-md-12 col-sm-12">' +
                    '<div id="contact_email' + contact_count + '" class="input-wrapper full-width">' +
                    '<div class="styled-input">' +
                    '<input type="text" id="c_e_value_' + contact_count + '"  class="form-control contact_email_value email_validation"  name=' + "contact_email" + contact_count + '[]' + '>' +
                    '<label>Email</label>' +
                    '<span></span>' +
                    '</div>' +
                    '</div>' +
                    '<a type="button" class="btn btn-primary custom-btn input-btn add_contact_email" id="add_contact_email_' + contact_count + '"><span>+ More</span></a>' +
                    '</div>' +
                    '<div class="col-md-12 col-sm-12">' +
                    '<div class="input-wrapper full-width">' +
                    '<div class="styled-input">' +
                    '<input type="text" name="contact_fax[]" class="form-control fax_number_validation">' +
                    '<label>Fax Number</label>' +
                    '<span></span>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-md-6">' +
                    '<div class="col-md-12 col-sm-12">' +
                    '<div class="input-wrapper full-width">' +
                    '<div class="styled-input">' +
                    '<input type="text" name="designation[]" class="form-control">' +
                    '<label>Position</label>' +
                    '<span></span>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<div  class="col-md-12 col-sm-12">' +
                    '<div id="contact_mobile' + contact_count + '" class="input-wrapper full-width">' +
                    '<div class="styled-input">' +
                    '<input type="text"   class="form-control mobile_validation"  id="c_m_v_' + contact_count + '"  name=' + "contact_mobile" + contact_count + '[]' + '>' +
                    '<label>Mobile Number</label>' +
                    '<span></span>' +
                    '</div>' +
                    '</div>' +
                    '<a  type="button" class="btn btn-primary custom-btn input-btn add_contact_mobile" id="add_contact_mobile_' + contact_count + '"><span>+ More</span></a>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<i class="fa fa-remove removeContactPerson"></i>' +
                    '</div></div>').insertBefore('.addBefore');

            //  }
            $('input').keyup(function () {
                if ($(this).val().length > 0) {
                    $(this).addClass('not-empty');


                } else {
                    $(this).removeClass('not-empty');

                }
            });
            $(".fax_number_validation").mask("(999) 999-9999");
            $(".mobile_validation").mask("(999) 999-9999");
            // });
        } else {
            alert('You can add contact person upto 2 only');

        }
    });


//                $('#add_sales_person').click(function ()
//                {
//                    sales_count++;
//                    $('#count_sales').val(sales_count);
//                    sales_person_array.push('1');
//                    $('#sales_person_array').val(sales_person_array);
//                    var eleLength = $('#sales_person')[0].childElementCount;
//                    var element = $('#sales_person')[0].children;
//                    $(element).each(function (index) {
//                        if (index === eleLength - 1) {
//                            $(this).append('<label>' + "Sales Person " + sales_count + '</label>');
//                            $(this).attr('id', 'sales_' + sales_count);
//                            $(this).append(
//                                    '<div class="main">' +
//                                    '<div class="form-group col-md-10">' +
//                                    '<input type="text" name="sales_person_name[]" class="form-control" placeholder="Enter Name">'
//                                    + '</div>'
//                                    + '<div id="sales_email">' +
//                                    '<div class="form-group col-md-10">' +
//                                    '<input type="text" class="form-control" placeholder="Enter Email" name=' + "sales_person_email" + sales_count + '[]' + '>' +
//                                    '</div>' +
//                                    '<div class="form-group col-md-2">' +
//                                    '<input type="button" class="add_sales_email" value="+More">' +
//                                    '</div>' +
//                                    '</div>' +
//                                    '<div id="sales_mobile">' +
//                                    '<div class="form-group col-md-10">' +
//                                    '<input type="text" class="form-control" placeholder="Cell Number" name=' + "sales_person_mobile" + sales_count + '[]' + ' >' +
//                                    '</div>' +
//                                    '<div class="form-group col-md-2">' +
//                                    '<input type="button" class="add_sales_mobile" value="+More">' +
//                                    '</div>' +
//                                    '</div>' +
//                                    '<div class="form-group col-md-10">' +
//                                    '<input type="text" name="sales_person_fax[]" class="form-control" placeholder="Fax Number">' +
//                                    '</div>' +
//                                    '</div>');
//                        }
//                    });
//                });

    $(".business_phone_validation").mask("(999) 999-9999");


    $(".business_phone_validation").on("blur", function () {
        var last = $(this).val().substr($(this).val().indexOf("-") + 1);

        if (last.length == 5) {
            var move = $(this).val().substr($(this).val().indexOf("-") + 1, 1);

            var lastfour = last.substr(1, 4);

            var first = $(this).val().substr(0, 9);

            $(this).val(first + move + '-' + lastfour);
        }
    });


    $(".fax_number_validation").mask("(999) 999-9999");


    $(".fax_number_validation").on("blur", function () {
        var last = $(this).val().substr($(this).val().indexOf("-") + 1);

        if (last.length == 5) {
            var move = $(this).val().substr($(this).val().indexOf("-") + 1, 1);

            var lastfour = last.substr(1, 4);

            var first = $(this).val().substr(0, 9);

            $(this).val(first + move + '-' + lastfour);
        }
    });


});

/*
 $('#contactLable').click(function(){ 
 $('#contactValue').val(''); 
 var contactLable=$('#contactLable').val();   
 if(jQuery.inArray(contactLable, contactlable1) == -1)
 {  
 contactlable1.push(contactLable);
 type1.push(0);
 name1.push(null); 
 contactvalue1.push(null);
 }
 else{ 
 $.each( contactlable1, function( key, value ) {
 if(value==contactLable){
 $('#contactValue').val(contactvalue1[key]);    
 }
 });         
 }
 $('#lable_array').val(contactlable1);
 $('#type_array').val(type1);
 $('#name_array').val(name1); 
 $('#value_array').val(contactvalue1);    
 });  
 $('#contactValue').blur(function(){
 var contact=$('#contactValue').val(); 
 var contactlab=$('#contactLable :selected').text();  
 $.each( contactlable1, function( key, value ) {
 if(value==contactlab){
 contactvalue1[key]=contact; 
 }
 });        
 $('#value_array').val(contactvalue1); 
 });
 
 //sales person  
 $('#salesLable').click(function(){
 $('#salesValue').val(''); 
 var salesLable=$('#salesLable').val();
 if(jQuery.inArray(salesLable, contactlable1) == -1)
 {
 contactlable1.push(salesLable);
 type1.push(1);
 name1.push(null);
 contactvalue1.push(null);
 }
 else{
 $.each( contactlable1, function( key, value ) {
 if(value==salesLable){
 $('#salesValue').val(contactvalue1[key]);    
 }
 }); 
 }
 $('#lable_array').val(contactlable1);
 $('#type_array').val(type1);
 $('#name_array').val(name1);
 $('#value_array').val(contactvalue1); 
 });
 $('#salesValue').blur(function(){
 var saleslab=$('#salesLable :selected').text();
 var sales=$('#salesValue').val();      
 $.each( contactlable1, function( key, value ) {
 if(value==saleslab){
 contactvalue1[key]=sales; 
 }
 }); 
 $('#value_array').val(contactvalue1);   
 });
 //cell lable 
 $('#cellLable').click(function(){
 $('#cellValue').val('');   
 var cellLable=$('#cellLable').val();
 if(jQuery.inArray(cellLable, contactlable1) == -1)
 {
 contactlable1.push(cellLable);
 type1.push(3);
 name1.push(null);
 contactvalue1.push(null);
 }
 else{
 $.each( contactlable1, function( key, value ) {
 if(value==cellLable){
 $('#cellValue').val(contactvalue1[key]);    
 }
 }); 
 }
 $('#lable_array').val(contactlable1);
 $('#type_array').val(type1);
 $('#name_array').val(name1);
 $('#value_array').val(contactvalue1);   
 });
 $('#cellValue').blur(function(){
 var celllab=$('#cellLable :selected').text();
 var cell=$('#cellValue').val();     
 $.each( contactlable1, function( key, value ) {
 if(value==celllab){
 contactvalue1[key]=cell; 
 }
 }); 
 $('#value_array').val(contactvalue1);   
 });
 //Email lable
 $('#emailLable').click(function(){
 $('#emailValue').val('');     
 var emailLable=$('#emailLable').val();
 if(jQuery.inArray(emailLable, contactlable1) == -1)
 {
 contactlable1.push(emailLable);
 type1.push(4);
 name1.push(null);
 contactvalue1.push(null);
 }
 else{
 $.each( contactlable1, function( key, value ) {
 if(value==emailLable){
 $('#emailValue').val(contactvalue1[key]);    
 }
 }); 
 }
 $('#lable_array').val(contactlable1);
 $('#type_array').val(type1);
 $('#name_array').val(name1);
 });
 $('#emailValue').blur(function(){
 var emaillab=$('#emailLable :selected').text();
 var email=$('#emailValue').val();     
 $.each( contactlable1, function( key, value ) {
 if(value==emaillab){
 contactvalue1[key]=email; 
 }
 }); 
 $('#value_array').val(contactvalue1); 
 });
 //Business phone
 $('#BusinessPhoneLable').click(function(){
 $('#BusinessPhoneValue').val('');
 var phoneLable=$('#BusinessPhoneLable').val();
 if(jQuery.inArray(phoneLable, contactlable1) == -1)
 {
 contactlable1.push(phoneLable);
 type1.push(2);
 contactvalue1.push(null);
 }
 else{
 $.each( contactlable1, function( key, value ) {
 if(value==phoneLable){
 $('#BusinessPhoneValue').val(contactvalue1[key]);    
 }
 }); 
 }
 $('#value_array').val(contactvalue1);
 $('#lable_array').val(contactlable1);
 $('#type_array').val(type1);
 });
 
 $('#BusinessPhoneValue').blur(function(){
 var businesslab=$('#BusinessPhoneLable :selected').text();
 var business=$('#BusinessPhoneValue').val();     
 $.each( contactlable1, function( key, value ) {
 if(value==businesslab){
 contactvalue1[key]=business; 
 }
 }); 
 $('#value_array').val(contactvalue1);   
 });
 $('#name').blur(function(){
 var name= $('#name').val(); 
 var phoneValue=$('#BusinessPhoneLable').val();
 name1.push(name);
 $('#name_array').val(name1);
 });*/


//    $('#edit_address_book_form').on('submit', function (event) {
//
//        event.preventDefault();
//        var submit_url = $(this).attr("action");
//        $(".errorsection").each(function () {
//            $(this).html("");
//        });
//        $.ajax({
//            type: 'POST',
//            url: submit_url,
//            dataType: "json",
//            async: false,
//            cache: false,
//            data: $("#edit_address_book_form").serialize(),
//            success: function (response) {
//                if (response.status == true) {
//                    var div = $('<div/>', {
//                        class: 'alert alert-success',
//                        title: 'now this div has a title!'
//                    });
//                    div.html(response.message);
//                    $(".contact-status").html(div);
//                    $('html, body').animate({
//                        'scrollTop': $("body").position().top
//                    });
//                    setTimeout(function () {
//                        location.reload();
//                    }, 2000);
//                } else {
//                    var div = $('<div/>', {
//                        class: 'alert alert-error',
//                        title: 'now this div has a title!'
//                    });
//                    div.html(response.message);
//                    // $(".contact-status").html(div);
//                    $('html, body').animate({
//                        'scrollTop': $(".contact-status").position().top
//                    });
//
//                }
//            }, error: function (response) {
//                console.log(response);
//                if (response.status === 404 && response.responseJSON.status == false) {
//                    $.each(response.responseJSON.error, function (key, value) {
//                        $(".errorsection").each(function () {
//                            var name = $(this).data('name');
//                            var index = $(this).data('index');
//                            if (index != "" && (parseInt(index) >= +0)) {
//                                index = "." + index;
//                            }
//                            if (name + index == key) {
//                                //alert(name+index);
//                                $(this).html(value).show();
//                            }
//                        });
//                    });
//                }
//            }
//        });
//    });
</script>
@stop