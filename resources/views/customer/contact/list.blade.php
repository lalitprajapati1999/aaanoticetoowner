@extends('adminlte::page')
@section('content')

<div>
    <section class="address-book">
        <div class="dashboard-wrapper">
             
                     @if (Session::get('success'))
                    <div class="alert alert-success">
                        <?php echo Session::get('success'); ?>
                    </div>
                @endif

            <div class="dashboard-heading">
                <h1><span>Address Book</span></h1>
            </div>

            <div class="dashboard-inner-body">
            <input type="hidden" value="{{$id}}" id="id" />
                    <div class="row">
                        @if(empty($id))
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <div class="add-new-btn">
                                <a  href="{{url('customer/contact/create')}}" type="button" class="btn btn-primary custom-btn"><span>+ Add New Contact</span></a>
                            </div>
                        </div>
                        @endif
                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-2 col-xs-12">
                                    <div class="form-group">
                                        <label for="">Filter By: </label>
                                    </div>
                                </div>
                                <div class="col-lg-5 col-md-6 col-sm-8 col-xs-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Company or Contact Name" name="company_name"  id="autocompleteCompanyName" autocomplete="off">
<!--                                        <span class="form-group-btn">
                                            <button class="btn btn-search" type="submit"><span class="icon-search"></span></button>
                                        </span>-->
                                    </div>
                                </div>
<!--                                <form role="form" method="post" id="contact-name">
                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Contact Name" name="contact_name">
                                        <span class="form-group-btn">
                                            <button class="btn btn-search" type="submit"><span class="icon-search"></span></button>
                                        </span>
                                    </div>
                                </div>
                                </form>-->
                            </div>
                        </div>
                    </div>
                <div class="dahboard-table table-responsive fixhead-table">
                    <table class="table table-striped" id="contacts-data" datatable="" width="100%" cellspacing="0"   data-scroll-x="true"scroll-collapse="false">
                        <thead class="thead-dark">
                            <tr>
<!--                                <th scope="col">Full Name</th>-->
                                <th scope="col">Company</th>
                                <th scope="col">Address</th>
                                <th scope="col">Attn</th>
                                <th scope="col">Business Phone</th>

                                <th scope="col">Email</th>
                               <!--  <th scope="col">Created At</th> -->
                                @if(empty($id)) 
                                    <th scope="col">Options</th>
                                @endif
                            </tr>
                        </thead>
               
                    </table>

                </div>
            </div>
        </div>
    </section>
</div>
<!--        <meta name="csrf-token" content="{{ csrf_token() }}">-->
@if(empty($id))
<form action="" method="POST" class="remove-record-model">
    {!! csrf_field() !!}
    <div id="contact-confirmation-modal" class="modal fade">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="icon-box">
                        <i class="fa fa-close"></i>
                    </div>				
                    <h4 class="modal-title">Are you sure?</h4>	
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <input type="text" name="contact_id" id="contact_id" hidden="">              
                    <p>Do you really want to delete these records? This process cannot be undone.</p>
                </div>
                <div class="modal-footer">
                    <div class="form-bottom-btn">
                        <button type="button" class="btn btn-primary custom-btn customb-btn remove-data-from-delete-form" data-dismiss="modal"><span>Cancel</span></button>
                        <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>Confirm</span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endif
@endsection

@section('frontend_js')
<script src="{{ asset('vendor/adminlte/plugins/datepicker/bootstrap-datepicker.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ asset('js/jquery-ui.min.js') }}"></script>

<script type="text/javascript">
 $(function(){
     var url = "{{ (Auth::user()->hasRole('customer')) ? url('customer/contacts/custom-filter') : url('contacts/custom-filter')}}";
     
     var columnsData = [
//           {data:'name',name:'name'},
           {data:'company_name',name:'company_name'},
           {data:'mailing_address',
             "render": function(data, type, row){
                return data.split(", ").join("<br/>");
            }},
          {data: 'attn', render: function (data, type, row) {
                    if (data) {
                        var data_addr = $("<br />").html(data).text();
                        var regex = /<br\s*[\/]?>/gi;

                        projaddr=data.length > 25 ?
                                data_addr.replace(regex, ' ').substr(0, 30) + '' :
                                data;
                            return projaddr.split('**').join('&#013;**');
                    } else {
                        return "NA";
                    } 
           }},
           {data:'phone',name:'phone'},
//           
           {data:'email',name:'email'},
          // {data:'created_at',name:'created_at'},
          // {data:'action',name:'action'}
       ];
       if($('#id').val() == '') {
           columnsData.push( {data:'action',name:'action'});
       }
      $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var contactsTable = $('#contacts-data').DataTable({
         //fixedHeader: true,
         "search": {
            "caseInsensitive": true,
            'use_wildcards' : false
          },  
         "language": {
           
            "zeroRecords": "No Record Found",
        },
       processing:true,
       serverSide:true,
        dom: "<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-6'p>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
       ordering:true,
      order: [ [3, 'desc'] ],
      pagingType: "simple_numbers",
       ajax:{
           url: url+"/"+$('#id').val(),
           data :function(d){
               d.company_name = $('input[name=company_name]').val();
               d.contact_name = $('input[name=contact_name]').val();

           }
       },
       columns:columnsData 
       
    });
    
    $('#company-name').on('submit',function(e){
        contactsTable.draw();
        e.preventDefault();
    });
    $('#contact-name').on('submit',function(e){
        contactsTable.draw();
        e.preventDefault();
    });
    
 });

$(document).ready(function () {
      $('.datepicker').datepicker({
          dateFormat : 'dd-mm-yy'
      });
      
      
       src = "{{ url('customer/contact/searchajaxcompanyname') }}";

     $("#autocompleteCompanyName").autocomplete({
        source: function(request, response) {
            $.ajax({
                url: src,
                dataType: "json",
                data: {
                    term : request.term
                },
                success: function(data) {
                  //console.log(data);
                  response(data);
                   
                },
                 select: function( event, ui) {

                    $("#autocompleteCompanyName").val(ui.item.id);
                }
            });
        },
//        minLength: 3,
       
    });
  });
    $(document).on('click', '.remove-record', function () {
       
        var contactid = $(this).attr('data-id');
        var url = "{{url('customer/contact/delete') }}";
        $(".remove-record-model").attr("action", url);
        $('body').find('.remove-record-model').append('<input name="id" type="hidden" value="' + contactid + '">');
        $('body').find('.remove-record-model').append('<input name="_method" type="hidden" value="POST">');
    });
     $(function () {
           otable = $('#contacts-data').dataTable();
       }); 
    $('#autocompleteCompanyName').on('keyup keypress change', function () {
      otable.fnFilter(this.value);
      otable.fnFilter(this.value, 0, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true);
    }); 
//     $('#autocompleteCompanyName').on('keyup keypress change', function () {
//       var val = this.value;
//         if (!val.match(/[^a-zA-Z0-9]/)) {
//             otable.fnFilter(this.value, 0, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true);
//           }
//   });

    
</script>
@endsection
