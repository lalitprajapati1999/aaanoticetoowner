<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        {{--
        <title>@yield('title_prefix', config('adminlte.title_prefix', '            '))
            @yield('title', config('adminlte.title',            'AdminLTE 2'))
            @yield('title_postfix', config('adminlte.title_postfix', ''))</title>
    --}}
        <title>
            {{ isset($title) ? $title.' :: '.config('adminlte.title', 'AdminLTE 2') : config('adminlte.title', 'AdminLTE 2') }} {{ $extraTitle ?? ''}}
        </title>
        @yield('before_styles')

        <link rel="stylesheet" type="text/css" href="{{asset('css/print.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/animate.css')}}">
        <link rel="stylesheet" href="{{asset('css/hover-min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/font-awesome.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/owl.carousel.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-select.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/select2.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/owl.theme.default.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/parts-selector.css')}}">

        <link rel="stylesheet" type="text/css" href="{{asset('css/parsley.css')}}">
        <!-- Tell the browser to be responsive to screen width -->
        <!--<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">-->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, maximum-scale=1, user-scalable=no">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/bootstrap/dist/css/bootstrap.min.css') }}">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/font-awesome/css/font-awesome.min.css') }}">
        <!-- Ionicons -->
        <link rel="stylesheet" href="{{ asset('vendor/adminlte/vendor/Ionicons/css/ionicons.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/jquery.dataTables.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/fixedHeader.dataTables.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/jquery.ui.autocomplete.css')}}">
        
        @if(config('adminlte.plugins.select2'))
        <!-- Select2 -->
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.css">
        @endif

        <!-- Theme style -->
        <link rel="stylesheet" href="{{ asset('vendor/adminlte/dist/css/AdminLTE.min.css') }}">

        @if(config('adminlte.plugins.datatables'))
        <!-- DataTables -->
        <link rel="stylesheet" href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
        @endif

        <link rel="stylesheet" type="text/css" href="{{asset('css/jquery.mCustomScrollbar.css')}}">    



        @yield('adminlte_css')

        <!-- Custom CSS -->
        <!-- <link rel="stylesheet" href="{{ asset('css/custom.css') }}"> -->

        <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
       <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}">


        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->





        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

        @yield('after_styles')

    </head>
    <body class="dashboard-body adjust-body">

        @yield('body')


        @yield('before_scripts')

        <script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>


        <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
        <!-- <script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script> -->
        <script type="text/javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
        <script  type="text/javascript" src="{{ asset('js/select2.js') }}"></script>
        <script  type="text/javascript" src="{{ asset('js/bootstrap-select.js') }}"></script>
        <script  type="text/javascript" src="{{ asset('js/wow.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/owl.carousel.min.js') }}"></script>
        <script type="text/javascript" src="{{asset('js/parts-selector.js')}}"></script>
        <script type="text/javascript" src="{{ asset('js/moment.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/dataTables.responsive.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/bootstrap-datepicker.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/parsley.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/jquery.mCustomScrollbar.js')}}"></script>
        <script src="{{ asset('vendor/backpack/tinymce/tinymce.min.js') }}"></script>
        <script src="{{ asset('vendor/backpack/ckeditor/ckeditor.js') }}"></script>
        <script src="{{ asset('vendor/backpack/ckeditor/adapters/jquery.js') }}"></script>
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script type="text/javascript" src="{{ asset('js/custom.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/jquery.mask.js') }}"></script>

        <script src="{{asset('js/dataTables.fixedHeader.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('js/dataTables.fixedColumns.min.js')}}" type="text/javascript"></script>

        @yield('adminlte_js')
        <div id="pleaseWait" class="modal fade" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- <div class="modal-header">
                            <h2 class="modal-title">
                                    <span style="color:#2695e7;text-align:center;"> We are processing your request<span>	
                            </h2>
                    </div> -->
                    <div class="modal-body">                 
                        <div style="text-align:center;">
                            <h3>We are processing your request.<br/>Please wait... <br/><img style='width:30%' src='{{asset("images/loader.gif")}}'></h3>
                        </div>                 
                    </div>
                </div>
            </div>
            <script>
/****************** autoclose datepicker ***********/
// $('.datepicker').datepicker().on('changeDate', function(ev){                 
//     $('.datepicker').datepicker('hide');
// });
$("#from-date").click(function () {
    $("#from-date").datepicker('show').on('changeDate',function(ev){                 
      $('.datepicker').hide();
  });
});
$("#to-date").click(function () {
    $("#to-date").datepicker('show').on('changeDate',function(ev){                 
      $('.datepicker').hide();
  });
});
/** tab activation ***/
$(window).on('load', function() {
    var self_url = window.location.href;
    if (self_url.indexOf("#")!=-1 ){
        var tab_name = self_url.split("#").pop();
        $("#" +tab_name + "_tab a ").trigger( "click" );
    }

});

//                $(document).ready(function () {

        //     $('input,textarea,select').on('focus', function(e) {
        //     e.preventDefault();
        //     $(this).attr("autocomplete", "nope");  
        // });
        //     });
//$('input,select,textarea').attr('autocomplete','nope');
//get focus to on click dropdown 
/*
$("button.dropdown-toggle").focus(function(){console.log($(this));
    $(this).parents('.bootstrap-select').addClass('open');
});
$(document).on('focus', '.select2', function (e) {
    if (e.originalEvent) {
        var s2element = $(this).siblings('select');
        s2element.select2('open');

        // Set focus back to select2 element on closing.
        s2element.on('select2:closing', function (e) {
            s2element.select2('focus');
        });
    }
});*/

//$(document).on('focus', '.select2', function() {
  

  //$('#contracted_by_select').select2('open');
 // $("#contracted_by_select").select2().trigger("select2:close");
//});
   //$("#contracted_by_select").select2().trigger("select2:close");
     //$('#contracted_by_select').select2('closed');

$('#submitMailingForm').on('submit', function (e) {

    e.preventDefault();



    var submit_url = $(this).attr("action");
    $.ajax({
        type: 'POST',
        url: submit_url,
        dataType: "json",
        async: false,
        cache: false,
        data: $("#submitMailingForm").serialize(),
        success: function (response) {
            if (response.status == 'success') {
                window.location = '<?php echo url('account-manager/mailing/1') ?>';
            } else {
                $('#errormessage').css('display', 'block');
                $('#errormessage').html(response.message);
            }
        }
    });
});
$('#submitCyoMailingForm').on('submit', function (e) {

    e.preventDefault();



    var submit_url = $(this).attr("action");
    $.ajax({
        type: 'POST',
        url: submit_url,
        dataType: "json",
        async: false,
        cache: false,
        data: $("#submitCyoMailingForm").serialize(),
        success: function (response) {

            if (response.status == 'success') {
                window.location = '<?php echo url('account-manager/cyo-mailing/1') ?>';
            } else {
                $('#cyoerrormessage').css('display', 'block');
                $('#cyoerrormessage').html(response.message);

            }
        }
    });
});
$('#sidebarCollapse').on('click', function () {
            $('#sidebar, #content').toggleClass('active');
              if($("#sidebar").hasClass("active")){
                   /* $(".dashboard-body.adjust-body .dashboard-main #content.adjust-content").css({"width":"calc(100% - 55px)"});*/
                    $("#sidebarCollapse").css({"left": "16%"});
                    $("#mobile-view-icon").removeClass('fa fa-ellipsis-v');
                    $("#mobile-view-icon").addClass('fa fa-times');

              }else{
                  
                 /*   $(".dashboard-body.adjust-body .dashboard-main #content.adjust-content").css({"width":"100%"});*/
                    $("#sidebarCollapse").css({"left": ""});
                    $("#mobile-view-icon").addClass('fa fa-ellipsis-v');


              }

        });
            </script>
    </body>
</html>
