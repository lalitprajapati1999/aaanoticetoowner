@if (is_string($item))
    <li role="presentation" class="header">{{ $item }}</li>
@else
    <li role="presentation">
      

@if($item['text']=='Mailing')
            @if(isset(\Auth::user()->pin) && \Auth::user()->pin !=NULL)
              <a title="{{ $item['text'] }}" href="#" id="mailing_module" data-toggle="modal" data-target="#mailingModal"
           @if (isset($item['target'])) target="{{ $item['target'] }}" @endif
        >
                            <div class="{{ $item['icon'] or '' }}"></div>
            {{-- <i class="fa fa-fw fa-{{ $item['icon'] or 'circle-o' }} {{ isset($item['icon_color']) ? 'text-' . $item['icon_color'] : '' }}"></i> --}}
             <span>{{ $item['text'] }}</span>
            
            @endif
            @elseif($item['text']=='Cyo Mailing')
            @if(isset(\Auth::user()->pin) && \Auth::user()->pin !=NULL)
              <a title="{{ $item['text'] }}" href="#" id="cyo_mailing_module" data-toggle="modal" data-target="#cyomailingModal"
           @if (isset($item['target'])) target="{{ $item['target'] }}" @endif
        >
                            <div class="{{ $item['icon'] or '' }}"></div>
            {{-- <i class="fa fa-fw fa-{{ $item['icon'] or 'circle-o' }} {{ isset($item['icon_color']) ? 'text-' . $item['icon_color'] : '' }}"></i> --}}
             <span>{{ $item['text'] }}</span>
            
            @endif
            @else
              <a title="{{ $item['text'] }}" href="{{ $item['href'] }}"
           @if (isset($item['target'])) target="{{ $item['target'] }}" @endif
        >
                        <div class="{{ $item['icon'] or '' }}"></div>
            {{-- <i class="fa fa-fw fa-{{ $item['icon'] or 'circle-o' }} {{ isset($item['icon_color']) ? 'text-' . $item['icon_color'] : '' }}"></i> --}}
            <span>{{ $item['text'] }}</span>
            @endif
            @if (isset($item['label']))
                <span class="pull-right-container">
                    <span class="label label-{{ $item['label_color'] or 'primary' }} pull-right">{{ $item['label'] }}</span>
                </span>
            @elseif (isset($item['submenu']))
                <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
                </span>
            @endif
        </a>
        @if (isset($item['submenu']))
            <ul class="{{ $item['submenu_class'] }}">
                @each('adminlte::partials.menu-item', $item['submenu'], 'item')
            </ul>
        @endif
    </li>
@endif