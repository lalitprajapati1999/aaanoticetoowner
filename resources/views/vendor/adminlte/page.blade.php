@extends('adminlte::master')
@section('before_styles')
{{-- Encrypted CSRF token for Laravel, in order for Ajax requests to work --}}
<meta name="csrf-token" content="{{ csrf_token() }}" />
@stop
@section('body')
<section class="dashboard-main">
    <!-- Main Header -->
    <div class="container-fluid">
        <div class="row">
            <button type="button" id="sidebarCollapse" class="btn btn-info">
                <i id="mobile-view-icon" class="fa fa-ellipsis-v" aria-hidden="true"></i><span class="mobile-show-sidebar">Sidebar</span>
            </button>
            <!--SideBar Start-->
            <div class="adjust-sidebar " id="sidebar">
                <div class="left-sidebar">
                    <div class="sidebar-logo">
                        @if(Auth::user()->hasRole('customer'))
                        <a href="{{url('/')}}"><img src="{{url('images/logo.png')}}" class="img-responsive center-block"></a>
                        @else
                        <a href="{{url('/')}}"><img src="{{url('images/logo.png')}}" class="img-responsive center-block"></a>
                        @endif
                    </div>
                    <!-- sidebar: style can be found in sidebar.less -->
                    <div class="mCustomScrollbar full-width sidenav-wrapper"  data-mcs-theme="light">
                        <ul class="nav nav-tabs" role="tablist">
                            <!-- Sidebar Menu -->

                            @each('adminlte::partials.menu-item', $adminlte->menu(), 'item')
                            <!-- /.sidebar-menu -->
                        </ul>
                    </div>
                    <!-- /.sidebar -->
                </div>
            </div>
            <!--SideBar End-->
            <div class="adjust-content" id="content">
                <div class="dashboard-content">
                    <div class="dashboard-header full-width">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <div class="small-sidebar"><i class="fa fa-bars" aria-hidden="true"></i></div>
                            <!--                  <form class="search-bar navbar-left" method="GET" role="search">
                                                 <div class="form-group search-box">
                                                    <input type="text" class="form-control" placeholder="Search">
                                                    <span class="form-group-btn">
                                                    <button class="btn btn-search" type="button" data-original-title="" title=""><span class="icon-search"></span></button>
                                                    </span>
                                                 </div>
                                              </form>-->
                            <ul class="nav navbar-nav navbar-right">
                                @if(Auth::user()->hasRole('customer')||Auth::user()->hasRole('account-manager'))
                                @if(Auth::user()->hasRole('customer'))
                                <li><a href="{{url('home')}}"><span>Dashboard</span></a></li>
                                @else
                                <li><a href="{{url('admin/dashboard')}}"><span>Dashboard</span></a></li>
                                @endif
                                <li><a href="{{url('customer/deadline-calculator')}}"><span>Deadline Calculator</span></a></li>
                                <li><a href="{{url('customer/lien_blog')}}"><span>Lien Blog</span></a></li>
                                <li><a href="{{url('customer/contact-us')}}"><span>Contact Us</span></a></li>
                                <li><a href="#" data-toggle="modal" data-target="#contactInfoModal"><span>Contact Info</span></a>

                                </li>
                                @endif
                                <li><a href="tel:1866 222-5479"><img src="{{url('images/phone-icon-w.png')}}"><span>1866 222-5479</span></a></li>
                                 @if(Auth::user()->hasRole('admin'))
                                <li class="applicant-noti">
                                    <a href="#">New Applicant : <span><?php echo getNewApplicantsCount(); ?></span></a>
                                </li>
                                @endif
                                <li class="dropdown acc-user">
                                    <a href="#" class="dropdown-toggle full-width" data-toggle="dropdown" role="button" aria-expanded="false">
                                        <img src="{{url('images/user-dummy.png')}}"><span>Welcome, {{Auth::User()->name}}</span>
                                        <span class="caret"></span></a>
                                    <ul class="dropdown-menu" role="menu">

                                        <li>
                                            <a class="btn btn-default btn-flat" href="javascript::void(0);" 
                                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                                               >
                                                <i class="fa fa-fw fa-power-off"></i> {{ trans('adminlte::adminlte.log_out') }}
                                            </a>
                                            <form id="logout-form" action="{{ url(config('adminlte.logout_url', 'auth/logout')) }}" method="POST" style="display: none;">
                                                @if(config('adminlte.logout_method'))
                                                {{ method_field(config('adminlte.logout_method')) }}
                                                @endif
                                                {{ csrf_field() }}
                                            </form>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <!-- /.navbar-collapse -->
                    </div>
                    <!-- Content Wrapper. Contains page content -->
                    <div class="main-content">
                        <div class="tab-content">
                            @yield('content_header')
                            @yield('content')
                        </div> 
                    </div>
                </div>
            </div>
        </div>
</section>
<form method="post" id="submitMailingForm" action="{{ url('account-manager/mailing/checkpin') }}" data-parsley-validate="">
    {{ csrf_field() }}
    <div id="mailingModal" class="modal fade">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <div class="modal-header">

                    <h4 class="modal-title">Mailing Privacy</h4>  
                    <button type="button" class="close mailing_close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <span id="errormessage" class="alert alert-danger alert-dismissable" style="display: none"></span>

                <div class="modal-body">
                    <p>Please enter your pin !</p>
                    <div class="input-wrapper full-width">
                        <div class="styled-input">
                            <input type="text" name="mailing_pin" id="mailing_pin" class="form-control"  data-parsley-required="true" data-parsley-required-message="Pin is required" data-parsley-trigger="change focusout"></input>
                            <label>Pin<span class="mandatory-field">*</span></label>
                            <span></span>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form-bottom-btn">
                        <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>Submit</span></button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</form>
<form method="post" id="submitCyoMailingForm" action="{{ url('account-manager/mailing/checkpin') }}" data-parsley-validate="">
    {{ csrf_field() }}
    <div id="cyomailingModal" class="modal fade">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <div class="modal-header">

                    <h4 class="modal-title">Mailing Privacy</h4>  
                    <button type="button" class="close cyo_mailing_close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <span id="cyoerrormessage" class="alert alert-danger alert-dismissable" style="display: none"></span>

                <div class="modal-body">
                    <p>Please enter your pin !</p>
                    <div class="input-wrapper full-width">
                        <div class="styled-input">
                            <input type="text" name="mailing_pin" id="cyo_mailing_pin" class="form-control"  data-parsley-required="true" data-parsley-required-message="Pin is required" data-parsley-trigger="change focusout"></input>
                            <label>Pin<span class="mandatory-field">*</span></label>
                            <span></span>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form-bottom-btn">
                        <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>Submit</span></button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</form>
<div id="contactInfoModal" class="modal fade">
    <div class="modal-dialog modal-confirm">
        <div class="modal-content">
            <div class="modal-header">

                <h4 class="modal-title">Contact Info</h4>  
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <p>AAA NOTICE TO OWNER - AAA BUSINESS ASSOC. CORP.<br/>
                    <?php echo env('FROM_ADDRESS') . ' ' . env('FROM_CITY') . ' ' . env('FROM_STATE') . ' ' . env('FROM_ZIPCODE'); ?><br/></p>
                <p>TEL: (786) 337-9602 | FAX: (786) 337-9604</p>
                <p>info@aaanoticetoowner.com</p>
            </div>

        </div>
    </div>
</div>
<!-- ./wrapper -->


@stop
@section('adminlte_js')
<!-- <script src="{{ asset('vendor/adminlte/dist/js/adminlte.min.js') }}"></script> -->
<script>
    new WOW().init();
</script>

<script type="text/javascript">
    //$('input,select,textarea').attr('autocomplete','off');
/*$("button").focus(function(){
  $('.bootstrap-select ').addClass('open')
});*/
    $(document).ready(function () {
        
        $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        if ($('#back-to-top').length) {
            // scroll body to 0px on click
            $('#back-to-top').click(function () {
                $('#back-to-top').tooltip('hide');
                $('body,html').animate({
                    scrollTop: 0
                }, 800);
                return false;
            });
            $('#back-to-top').tooltip('show');
        }
        // $('.datepicker').datepicker({
        //     uiLibrary: 'bootstrap'
        // });
        
        $(function () {
            $('.red-tooltip').tooltip()
            $(".red-tooltip").tooltip();
        });
        $(".btn.btn-default.corr-btn").click(function () {
            $(".corr-block.notice-block").toggleClass("notice-show");
        });
        $(".btn.btn-default.notice-btn").click(function () {
            $(".notes-block.notice-block").toggleClass("notice-show");
        });
        $(function () {
            if ($('.red-tooltip').length) {
                $('.red-tooltip').tooltip()
                $(".red-tooltip").tooltip();
            }
        });
        window.ParsleyConfig = {
            //   errorsWrapper: '<div></div>',
            errorTemplate: '<span class="alert alert-danger parsley"></span>',
            classHandler: function (el) {
                return el.$element.closest('input');
            },
            successClass: 'has-success',
            errorClass: 'has-error'
        };

    });

    $('.mailing_close').on('click', function () {
        $('#errormessage').css('display', 'none');
        $('#errormessage').empty();
        $('#mailing_pin').val('');
    });

    $('.cyo_mailing_close').on('click', function () {
        $('#cyoerrormessage').css('display', 'none');
        $('#cyoerrormessage').empty();
        $('#cyo_mailing_pin').val('');
    });
    /************************************************************************************/
    $(function () {
        var content = "<input type='text' class='bss-input' onKeyDown='event.stopPropagation();' onKeyPress='addSelectInpKeyPress(this,event)' onClick='event.stopPropagation()' placeholder='Add item'> <span class='glyphicon glyphicon-plus addnewicon' onClick='addSelectItem(this,event,1);'></span>";

        var divider = $('<option/>')
                .addClass('divider')
                .data('divider', true);


        var addoption = $('<option/>', {class: 'addItem'})
                .data('content', content);
        var addBtn = "<li><a role='option' class='addItem' aria-disabled='false' tabindex='0' aria-selected='false'><span class='glyphicon glyphicon-ok check-mark'></span><span class='text'><input type='text' class='bss-input' onkeydown='event.stopPropagation();' onkeypress='addSelectInpKeyPress(this,event)'' onclick='event.stopPropagation()'' placeholder='Add item'> <span class='glyphicon glyphicon-plus addnewicon' onclick='addSelectItem(this,event,1);'></span></span></a></li>";

        $('.add_item')
                .append(divider)
                .append(addoption)
                .selectpicker();
        $('.bootstrap-select.add_item .dropdown-menu.inner').append(addBtn);
    });


    function addSelectItem(t, ev)
    {
        ev.stopPropagation();

        var bs = $(t).closest('.bootstrap-select')
        var txt = bs.find('.bss-input').val().replace(/[|]/g, "");
        var txt = $(t).prev().val().replace(/[|]/g, "");
        if ($.trim(txt) == '')
            return;

        // Changed from previous version to cater to new
        // layout used by bootstrap-select.
        var p = bs.find('select.add_item');
        var o = $('option', p).eq(-2);
        o.before($("<option>", {"selected": true, "text": txt}));
        p.selectpicker('refresh');
    }

    function addSelectInpKeyPress(t, ev)
    {
        ev.stopPropagation();

        // do not allow pipe character
        if (ev.which == 124)
            ev.preventDefault();

        // enter character adds the option
        if (ev.which == 13)
        {
            ev.preventDefault();
            addSelectItem($(t).next(), ev);
        }
    }
    $('.add_item').selectpicker('refresh');

    /**********************************************************************************/


    $('#mailingModal').on('shown.bs.modal', function() {
        $('#mailing_pin').focus()
    });
    $('#cyomailingModal').on('shown.bs.modal', function() {
        $('#cyo_mailing_pin').focus()
    });
</script>
<!-- @stack('js') -->
@yield('frontend_js')
@yield('js')
<script type="text/javascript" src="{{ asset('js/frontend.js') }}"></script>  
@stop