@extends('adminlte::page')

@section('content_header')
<!--    <h1>
      {{ trans('backpack::base.dashboard') }}<small>
    </h1>-->
<!--    <ol class="breadcrumb">
      <li><a href="{{ backpack_url() }}">{{ config('adminlte.title', 'AdminLTE 2') }}</a></li>
      <li class="active">{{ trans('backpack::base.dashboard') }}</li>
    </ol>-->
@endsection


@section('content')
  @if(Auth::user()->hasRole('super-admin'))
  <div>
                                    <section class="create-own">
                                        <div class="dashboard-wrapper">
                                            <div class="dashboard-heading">
                                                <h1><span>Dashboard</span></h1>
                                            </div>
                                            <div class="dashboard-inner-body">
                                                <div class="createown-wrapper">
                                                  
                                                    <div class="grid">
                                                        <figure class="effect-sadie">
                                                            <figcaption>
                                                                <a href="{{url('admin/notices')}}"><h2>Manage Notices</h2></a>
                                                                <a href="{{url('admin/notices')}}"><p>Manage Notices</p></a>
                                                            </figcaption>           
                                                        </figure>
                                                    </div>
                                                    <div class="grid">
                                                        <figure class="effect-sadie">
                                                            <figcaption>
                                                                <a href="{{url('admin/states')}}"><h2>States</h2></a>
                                                                <a href="{{url('admin/states')}}"><p>States</p></a>
                                                            </figcaption>           
                                                        </figure>
                                                    </div>
                                                    <div class="grid">
                                                        <figure class="effect-sadie">
                                                            <figcaption>
                                                                <a href="{{url('admin/cities')}}"><h2>City</h2></a>
                                                                <a href="{{url('admin/cities')}}"><p>City</p></a>
                                                            </figcaption>           
                                                        </figure>
                                                    </div>
                                                    <div class="grid">
                                                        <figure class="effect-sadie">
                                                            <figcaption>
                                                                <a href="{{url('admin/notice_templates')}}"><h2>Notice Template</h2></a>
                                                                <a href="{{url('admin/notice_templates')}}"><p>Notice Template</p></a>
                                                            </figcaption>           
                                                        </figure>
                                                    </div>
                                                      <div class="grid">
                                                        <figure class="effect-sadie">
                                                            <figcaption>
                                                                <a href="{{url('admin/edit-account-info')}}"><h2>Account Setting</h2></a>
                                                                <a href="{{url('admin/edit-account-info')}}"><p>Account Setting</p></a>
                                                            </figcaption>           
                                                        </figure>
                                                    </div>
                                                   
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
@elseif(Auth::user()->hasRole('admin'))
 <div>
                                    <section class="create-own">
                                        <div class="dashboard-wrapper">
                                 <div class="dashboard-heading">
                                                <h1><span>Dashboard</span></h1>
                                            </div>
                                            <div class="dashboard-inner-body">
                                                <div class="createown-wrapper">
                                                    <div class="grid">
                                                        <figure class="effect-sadie">
                                                            <figcaption>
                                                                <a href="{{url('admin/new-applicants')}}"><h2>New Applicants</h2></a>
                                                                <a href="{{url('admin/new-applicants')}}"><p>New Applicants</p></a>
                                                            </figcaption>           
                                                        </figure>
                                                    </div>
                                                   
                                                    <div class="grid">
                                                        <figure class="effect-sadie">
                                                            <figcaption>
                                                                <a href="{{url('admin/customer')}}"><h2>Customer</h2></a>
                                                                <a href="{{url('admin/customer')}}"><p>Customer</p></a>
                                                            </figcaption>           
                                                        </figure>
                                                    </div>
                                                     <div class="grid">
                                                        <figure class="effect-sadie">
                                                            <figcaption>
                                                                <a href="{{url('admin/employees')}}"><h2>Employees</h2></a>
                                                                <a href="{{url('admin/employees')}}"><p>Employees</p></a>
                                                            </figcaption>           
                                                        </figure>
                                                    </div>
                                                    <div class="grid">
                                                        <figure class="effect-sadie">
                                                            <figcaption>
                                                                <a href="{{url('customer/secondary-document')}}"><h2>Secondary Document</h2></a>
                                                                <a href="{{url('customer/secondary-document')}}"><p>Secondary Document</p></a>
                                                            </figcaption>           
                                                        </figure>
                                                    </div>
                                                    <div class="grid">
                                                        <figure class="effect-sadie">
                                                            <figcaption>
                                                                <a href="{{url('admin/network')}}"><h2>Network</h2></a>
                                                                <a href="{{url('admin/network')}}"><p>Network</p></a>
                                                            </figcaption>           
                                                        </figure>
                                                    </div>
                                                     <div class="grid">
                                                        <figure class="effect-sadie">
                                                            <figcaption>
                                                                <a href="{{url('admin/edit-account-info')}}"><h2>Account Settings</h2></a>
                                                                <a href="{{url('admin/edit-account-info')}}"><p>Account Settings</p></a>
                                                            </figcaption>           
                                                        </figure>
                                                    </div>
                                                    <div class="grid">
                                                        <figure class="effect-sadie">
                                                            <figcaption>
                                                                <a href="{{url('account-manager/research')}}"><h2>Research</h2></a>
                                                                <a href="{{url('account-manager/research')}}"><p>Research</p></a>
                                                            </figcaption>           
                                                        </figure>
                                                    </div>
                                                      <div class="grid">
                                                        <figure class="effect-sadie">
                                                            <figcaption>
                                                                <a href="#" id="mailing_module" data-toggle="modal" data-target="#mailingModal"><h2>Mailing</h2></a>
                                                                <a href="#" id="mailing_module" data-toggle="modal" data-target="#mailingModal"><p>Mailing</p></a>
                                                            </figcaption>           
                                                        </figure>
                                                    </div>
                                                      <div class="grid">
                                                        <figure class="effect-sadie">
                                                            <figcaption>
                                                                <a href="#" id="cyo_mailing_module" data-toggle="modal" data-target="#cyomailingModal"><h2>Cyo Mailing</h2></a>
                                                                <a href="#" id="cyo_mailing_module" data-toggle="modal" data-target="#cyomailingModal"><p>Cyo Mailing</p></a>
                                                            </figcaption>           
                                                        </figure>
                                                    </div>
                                                    {{-- <div class="grid">
                                                        <figure class="effect-sadie">
                                                            <figcaption>
                                                                <a href="{{url('admin/notice_templates')}}"><h2>Notice Template</h2></a>
                                                                <a href="{{url('admin/notice_templates')}}"><p>Notice Template</p></a>
                                                            </figcaption>           
                                                        </figure>
                                                    </div> --}}
                                                      <div class="grid">
                                                        <figure class="effect-sadie">
                                                            <figcaption>
                                                                <a href="{{url('admin/testimonial')}}"><h2>Testimonial</h2></a>
                                                                <a href="{{url('admin/testimonial')}}"><p>Testimonial</p></a>
                                                            </figcaption>           
                                                        </figure>
                                                    </div>
                                                      <div class="grid">
                                                        <figure class="effect-sadie">
                                                            <figcaption>
                                                                <a href="{{url('admin/lien_blog')}}"><h2>Lien blog</h2></a>
                                                                <a href="{{url('admin/lien_blog')}}"><p>Lien blog</p></a>
                                                            </figcaption>           
                                                        </figure>
                                                    </div>
                                                    <div class="grid">
                                                        <figure class="effect-sadie">
                                                            <figcaption>
                                                                <a href="{{url('admin/category')}}"><h2>Customer Role</h2></a>
                                                                <a href="{{url('admin/category')}}"><p>Customer Role</p></a>
                                                            </figcaption>           
                                                        </figure>
                                                    </div>
                                                    <div class="grid">
                                                        <figure class="effect-sadie">
                                                            <figcaption>
                                                                <a href="{{url('admin/project-type')}}"><h2>Project Type</h2></a>
                                                                <a href="{{url('admin/project-type')}}"><p>Project Type</p></a>
                                                            </figcaption>           
                                                        </figure>
                                                    </div>
                                                    <div class="grid">
                                                        <figure class="effect-sadie">
                                                            <figcaption>
                                                                <a href="{{url('admin/manage-subscriptions')}}"><h2>Manage Subscriptions</h2></a>
                                                                <a href="{{url('admin/manage-subscriptions')}}"><p>Manage Subscriptions</p></a>
                                                            </figcaption>           
                                                        </figure>
                                                    </div>
                                                      <div class="grid">
                                                        <figure class="effect-sadie">
                                                            <figcaption>
                                                                <a href="{{url('admin/reports')}}"><h2>Reports</h2></a>
                                                                <a href="{{url('admin/reports')}}"><p>Reports</p></a>
                                                            </figcaption>           
                                                        </figure>
                                                    </div>
                                                    <div class="grid">
                                                        <figure class="effect-sadie">
                                                            <figcaption>
                                                                <a href="{{url('admin/project-roles')}}"><h2>Project Roles</h2></a>
                                                                <a href="{{url('admin/project-roles')}}"><p>Project Roles</p></a>
                                                            </figcaption>           
                                                        </figure>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
@elseif(Auth::user()->hasRole('account-manager'))
 <div>
                                    <section class="create-own">
                                        <div class="dashboard-wrapper">
                                
                                            <div class="dashboard-inner-body">
                                                <div class="createown-wrapper">
                                                     <div class="grid">
                                                        <figure class="effect-sadie">
                                                            <figcaption>
                                                                <a href="{{url('admin/customer')}}"><h2>Customer</h2></a>
                                                                <a href="{{url('admin/customer')}}"><p>Customer</p></a>
                                                            </figcaption>           
                                                        </figure>
                                                    </div>
                                                    <div class="grid">
                                                        <figure class="effect-sadie">
                                                            <figcaption>
                                                                <a href="{{url('customer/secondary-document')}}"><h2>Secondary Document</h2></a>
                                                                <a href="{{url('customer/secondary-document')}}"><p>Secondary Document</p></a>
                                                            </figcaption>           
                                                        </figure>
                                                    </div>
                                                    <div class="grid">
                                                        <figure class="effect-sadie">
                                                            <figcaption>
                                                                <a href="{{url('account-manager/research')}}"><h2>Research</h2></a>
                                                                <a href="{{url('account-manager/research')}}"><p>Research</p></a>
                                                            </figcaption>           
                                                        </figure>
                                                    </div>
                                                   
<!--                                                    <div class="grid">
                                                        <figure class="effect-sadie">
                                                            <figcaption>
                                                                <a href=""><h2>Administration</h2></a>
                                                                <a href=""><p>Administration</p></a>
                                                            </figcaption>           
                                                        </figure>
                                                    </div>
                                                    <div class="grid">
                                                        <figure class="effect-sadie">
                                                            <figcaption>
                                                                <a href="#"><h2>Hard Document</h2></a>
                                                                <a href="#"><p>Hard Document</p></a>
                                                            </figcaption>           
                                                        </figure>
                                                    </div>-->
                                                      <div class="grid">
                                                        <figure class="effect-sadie">
                                                            <figcaption>
                                                                <a href="#" id="mailing_module" data-toggle="modal" data-target="#mailingModal"><h2>Mailing</h2></a>
                                                                <a href="#" id="mailing_module" data-toggle="modal" data-target="#mailingModal"><p>Mailing</p></a>
                                                            </figcaption>           
                                                        </figure>
                                                    </div>
                                                     <!--  <div class="grid">
                                                        <figure class="effect-sadie">
                                                            <figcaption>
                                                                <a href="#" id="cyo_mailing_module" data-toggle="modal" data-target="#cyomailingModal"><h2>Cyo Mailing</h2></a>
                                                                <a href="#" id="cyo_mailing_module" data-toggle="modal" data-target="#cyomailingModal"><p>Cyo Mailing</p></a>
                                                            </figcaption>           
                                                        </figure>
                                                    </div> -->
<!--                                                    <div class="grid">
                                                        <figure class="effect-sadie">
                                                            <figcaption>
                                                                <a href="{{url('admin/category')}}"><h2>Category</h2></a>
                                                                <a href="{{url('admin/category')}}"><p>Category</p></a>
                                                            </figcaption>           
                                                        </figure>
                                                    </div>-->
                                                    
<!--                                                    <div class="grid">
                                                        <figure class="effect-sadie">
                                                            <figcaption>
                                                                <a href="{{url('admin/manage-subscriptions')}}"><h2>Manage Subscriptions</h2></a>
                                                                <a href="{{url('admin/manage-subscriptions')}}"><p>Manage Subscriptions</p></a>
                                                            </figcaption>           
                                                        </figure>
                                                    </div>-->
                                                    <div class="grid">
                                                        <figure class="effect-sadie">
                                                            <figcaption>
                                                                <a href="{{url('admin/edit-account-info')}}"><h2>Account Settings</h2></a>
                                                                <a href="{{url('admin/edit-account-info')}}"><p>Account Settings</p></a>
                                                            </figcaption>           
                                                        </figure>
                                                    </div>
                                                    <div class="grid">
                                                        <figure class="effect-sadie">
                                                            <figcaption>
                                                                <a href="{{url('account-manager/create-your-own')}}"><h2>Create Your Own</h2></a>
                                                                <a href="{{url('account-manager/create-your-own')}}"><p>Create Your Own</p></a>
                                                            </figcaption>           
                                                        </figure>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
@endif
@endsection
