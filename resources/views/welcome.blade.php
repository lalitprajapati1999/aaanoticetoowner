@extends('layouts.page')

@section('banner')
<section class="banner-sec">
    <div class="main-wrapper banner-wrapper">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 text-center">
                <div class="banner-text">
                    <h1 class="text-uppercase wow fadeIn col-md-12">power your business</h1>
                    <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12 wow fadeIn">
                        <p class="">Send your Notice to Owner </br> and Start protecting your lien rights today.</p>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 wow bounceIn">
                        <a href="{{ route(config('adminlte.register_url', 'register')) }}" type="button" class="btn btn-primary custom-btn"><span>Become a Member! It's Free</span></a>
                    </div>
                </div>
                <img src="images/banner-star.png" class="banner-star wow flash">
            </div>
        </div>
    </div>
</section>
@stop

@section('content')
<!-------------our services starts-------------------->
<section class="section-wrapper">
    <div class="main-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="section-heading text-center">
                    <h3 class="section-subtitle wow fadeInUp">Let the experts figure it out for you</h3>
                    <h2 class="section-title wow fadeInUp">Our Services</h4>
                </div>

                <!-----------------------services for large devices-------------------------->

                <ul class="services full-width visible-lg visible-md visible-sm hidden-xs">
                    <li>
                        <div class="polygon-shape">
                            <div class="flip-container">
                                <div class="flipper">
                                    <div class="front">
                                        <span class="services-img"><img src="images/service-1.png"></span>
                                        <h2 class="service-title">Request your Preliminary Notice Today!<br> Easy as 1-2-3.</h2>
                                    </div>
                                    <div class="back">
                                        <span class="services-img"><img src="images/service-1.png"></span>
                                        <h2 class="service-title">Request your Preliminary Notice Today! Easy as 1-2-3.</h2>
                                        <ul class="service-data">
                                            <li>Sign up today!<br>Register from any device or call us<br>Customer Service is our priority</li>
                                            <li>Choose your deal!<br>One price, all recipients<br>Free Waivers</li>
                                            <li>It’s easy - Save time<br>Already have your customer information<br>Project address and job start date?<br>That’s all you need  we do the rest!</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="polygon-shape">
                            <div class="flip-container">
                                <div class="flipper">
                                    <div class="front">
                                        <span class="services-img"><img src="images/service-2.png"></span>
                                        <h2 class="service-title">MIND AT EASE</h2>
                                    </div>
                                    <div class="back">
                                        <span class="services-img"><img src="images/service-2.png"></span>
                                        <h2 class="service-title">MIND AT EASE</h2>
                                       <!--  <ul class="service-data">
                                            <li>Track and manage your projects all in one place, 
                                                Notice to Owner, waivers, Claim of Liens, and more.
                                                With our Data Base it’s easy, reliable and saves you time.</li>
                                            <li>Anytime ,– Anywhere you can submit and track all your project activities.</li>
                                            <li>Can’t get to it, Call Us!<br>Our reliable Customer Service is waiting for
                                                you to start your Lien protection process.</li>
                                        </ul> -->
                                        <ul class="service-data">
                                            <li>Track and Manage your projects in one place!
                                            </li>
                                            <li>
                                            Notice to Owner, Claim of Lien, Waivers and more.
                                            </li>
                                            <li>
                                               Our data base ensures a smooth and reliable process
                                                and will save you time.
                                            </li>
                                            <li>You can submit your projects - Anytime, Anywhere.
                                            </li>
                                            <li>
                                                Can't get to it? Call us!
                                            </li>
                                            <li>
                                            Our customer service representatives are waiting and available for you to start your Lien Protection Process.
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="polygon-shape">
                            <div class="flip-container">
                                <div class="flipper">
                                    <div class="front">
                                        <span class="services-img"><img src="images/service-3.png"></span>
                                        <h2 class="service-title">E- FILE</h2>
                                    </div>
                                    <div class="back">
                                        <span class="services-img"><img src="images/service-3.png"></span>
                                        <h2 class="service-title">E- FILE</h2>                                    
                                        <ul class="service-data">
                                            <li>Getting paid first, it’s easier than ever!<br>E-sign your documents and email them from any device.</li>
                                            <li>Saving time is essential.<br>One click and your waiver is first in line for payment.</li>
                                            <li>Need a Claim of Lien or Satisfaction of Lien?<br>Electronic filing is the most efficient and reliable way to get it done fast!</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="polygon-shape">
                            <div class="flip-container">
                                <div class="flipper">
                                    <div class="front">
                                        <span class="services-img"><img src="images/service-4.png"></span>
                                        <h2 class="service-title">PROCESS YOUR WAIVERS</h2>
                                    </div>
                                    <div class="back">
                                        <span class="services-img"><img src="images/service-4.png"></span>
                                        <h2 class="service-title">PROCESS YOUR WAIVERS</h2>
                                        <ul class="service-data">
                                            <li>No paper work!<br>Manage your waivers from one place, create your free 
                                                waivers in just one click.</li>
                                            <li>Ease & simplicity of creating your Releases is available across your devices .</li>
                                            <li>Be First to Get Paid!<br>E-sign and email your waiver electronically 
                                                from any device.</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>

                <!-----------------------services for mobile devices-------------------------->
                <ul class="services-xs visible-xs hidden-sm hidden-md hidden-lg full-width">
                    <li class="wow fadeInUp full-width">
                        <div class="service-data">
                            <span class="services-img"><img src="images/service-1.png"></span>
                            <h2 class="service-title">Request your prelim notice today in 3 easy steps</h2>
                            <ul class="service-data">
                                <li>Requesting a notice as easy as 1-2-3</li>
                                <li>Notice on the go!</li>
                                <li>E-sign your documents and save time</li>
                                <li>E-record claim of liens </li>
                            </ul>
                        </div>
                    </li>
                    <li class="wow fadeInUp full-width">
                        <div class="service-data">
                            <span class="services-img"><img src="images/service-2.png"></span>
                            <h2 class="service-title">Mind at Ease</h2>
                            <ul class="service-data">
                                <li>Requesting a notice as easy as 1-2-3</li>
                                <li>Notice on the go!</li>
                                <li>E-sign your documents and save time</li>
                                <li>E-record claim of liens </li>
                            </ul>
                        </div>
                    </li>
                    <li class="wow fadeInUp full-width">
                        <div class="service-data">
                            <span class="services-img"><img src="images/service-3.png"></span>
                            <h2 class="service-title">E-File your Documents</h2>                                    
                            <ul class="service-data">
                                <li>Requesting a notice as easy as 1-2-3</li>
                                <li>Notice on the go!</li>
                                <li>E-sign your documents and save time</li>
                                <li>E-record claim of liens </li>
                            </ul>
                        </div>
                    </li>
                    <li class="wow fadeInUp full-width">
                        <div class="service-data">
                            <span class="services-img"><img src="images/service-4.png"></span>
                            <h2 class="service-title">Receiving Payment Process your Free Waivers</h2>
                            <ul class="service-data">
                                <li>Requesting a notice as easy as 1-2-3</li>
                                <li>Notice on the go!</li>
                                <li>E-sign your documents and save time</li>
                                <li>E-record claim of liens </li>
                            </ul>
                        </div>
                    </li>
                </ul>
                <!-----------------------services for mobile devices-------------------------->
            </div>
        </div>      
    </div>
</section>
<!-------------our services ends-------------------->



<!-------------sign today starts-------------------->
<section class="section-wrapper sign-today">
    <div class="main-wrapper">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="sign-text text-center">
                    <h2 class="wow fadeInUp">Sign up Today!</h2>
                    <p class="full-width wow fadeInUp">Just a click away</p>
                    <a href="{{ route(config('adminlte.register_url', 'register')) }}" type="button" class="btn btn-primary customw-btn wow bounceIn"><span>Register Now</span></a>
                </div>
            </div>
            <div class="col-md-4">
                <ul class="specifications">
                    <li class="wow fadeInRight"> Manage your Preliminary Notices in one place!</li>
                    <li class="wow fadeInRight"> Track All Waivers!</li>
                    <li class="wow fadeInRight">Need a Claim of Lien? Easy as 1-2-3!</li>
                    <li class="wow fadeInRight">Request – E-Sign – Submit!</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-------------sign today ends-------------------->



<!-------------how works starts-------------------->
<section class="section-wrapper how-works">
    <div class="main-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="section-heading text-center">
                    <h2 class="section-title wow fadeInUp">Ready to create your own! </h2>
                    <h3 class="section-subtitle wow fadeInUp">How it works</h3>
                </div>
            </div>
            <div class="col-md-8 col-md-offset-2">
                <ul class="works full-width">
                    <li class="wow fadeInRight">
                        <div class="work-sec">
                            <div class="work-image">
                                <img src="images/works-1.png">
                            </div>
                            <div class="work-text">
                                <p>Create your own documents, using our database</p>
                            </div>
                        </div>
                    </li>
                    <li class="wow fadeInLeft">
                        <div class="work-sec">
                            <div class="work-image">
                                <img src="images/works-2.png">
                            </div>
                            <div class="work-text">
                                <p>Fast and easy, saves you money</p>
                            </div>
                        </div>
                    </li>
                    <li class="wow fadeInRight">
                        <div class="work-sec">
                            <div class="work-image">
                                <img src="images/works-3.png">
                            </div>
                            <div class="work-text">
                                <p>You create it, We mail it</p>
                            </div>
                        </div>
                    </li>
                    <li class="wow fadeInLeft">
                        <div class="work-sec">
                            <div class="work-image">
                                <img src="images/works-4.png">
                            </div>
                            <div class="work-text">
                                <p>Track your prelim notices, waiver,claim of lien and much more all in one place</p>
                            </div>
                        </div>
                    </li>
                    <li class="wow fadeInRight">
                        <div class="work-sec">
                            <div class="work-image">
                                <img src="images/works-5.png">
                            </div>
                            <div class="work-text">
                                <p>E-file your documents</p>
                            </div>
                        </div>
                    </li>
                    <li class="wow fadeInLeft">
                        <div class="work-sec">
                            <div class="work-image">
                                <img src="images/works-6.png">
                            </div>
                            <div class="work-text">
                                <p>Notice to Owner on the Go!</p>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col-md-12 text-center wow bounceIn">
                <a href="{{ route(config('adminlte.register_url', 'register')) }}" type="button" class="btn btn-primary custom-btn hvr-rectangle-out">Create your own Today!</a>
            </div>
        </div>      
    </div>
</section>
<!-------------how works ends-------------------->



<!-------------customer stories starts-------------------->
<section class="section-wrapper reviews">
    <div class="main-wrapper">
        <div class="row">
            <div class="col-md-3 col-sm-12 col-xs-12">
                <div class="stories-left wow fadeInLeft">
                    <h2>Our customer stories</h2>
                </div>
            </div>

            <div class="col-md-9 col-sm-12 col-xs-12">
                <div class="owl-carousel owl-theme stories-slider wow fadeInRight">
                    <?php $testimonials = TestimonialHelper::getTestiominals(); ?>
                    @if(isset($testimonials) && count($testimonials) >0)
                    @foreach($testimonials AS $each_testimonial)
                    <div class="item">
                        <div class="stories-wrapper">
                            @if(isset($each_testimonial->image) && $each_testimonial->image != '')
                       
                                  <img src= "{{asset('images/'.$each_testimonial->image)}}" class="img-circle"> 
                              
                            @else
                            <img  class="img-circle"> 
                            <!--<img src="{{asset('images/customer.png')}}" class="img-circle">--> 
                            @endif
                            <div class="review">
                                <!-- <p class="full-width">"I'm a testimonial. Click to edit me and add text that says something nice about you and your services. Let your customers review you and tell their friends how great  you are."</p> -->
                                {!! $each_testimonial->content!!}
                                <h5>{{$each_testimonial->client_name}}</h5>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @endif

                </div>
            </div>
        </div>
    </div>
</section>
<!-------------customer stories ends-------------------->



<!-------------our promises starts-------------------->
<section class="section-wrapper promises">
    <div class="main-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="section-heading text-center">
                    <h2 class="section-title wow fadeInUp">Our PROMISE</h2>
                </div>
                <div class="full-width promises-text text-center wow fadeInUp">
                    <h6>We will work alongside you and your Company, assuring your lien rights are secured.</h6>
                    <p>Our promise is that you will not be alone when making important decisions regarding your Preliminary Notices, Liens, Bond Claims and any releases you may provide. Our experienced and dedicated Account Managers are here to guide you and ease your concerns. One of our main priorities is expediting the process of your work orders, with all the proper research they will be completed within 5 business days or your Notice is on us.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-------------our promises ends-------------------->



<!-------------notices starts-------------------->
<section class="section-wrapper notices-sec">
    <div class="main-wrapper">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                <div class="time full-width text-center wow fadeIn">
                    <h2>YOUR TIME MATTERS </h2>
                    <p class="full-width">Request your Preliminary Notice today! It's EASIER than EVER. </p>
                    <p class="full-width">Login - e-File - Send </p>
                </div>
                <div class="time full-width text-center wow fadeIn">
                    <span>
                        <img src="images/notice-icon.png">
                    </span>
                    <h2>Protect your lien rights by sending your Notice on time</h2>
                </div>
            </div>
            <div class="col-md-8 col-md-offset-2 col-sm-12 col-xs-12 notice">
                <div class="row">
                    <div class="col-md-4 col-sm-4 col-xs-12 wow fadeIn notice-list">
                        <h4>Don't waste time</h4>
                        <ul>
                            <li>Notice to owner</li>
                            <li>Preliminary Notice</li>
                            <li>Partial Release</li>
                            <li>Final Release</li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 wow fadeIn notice-list">
                        <h4>Start today</h4>
                        <ul>
                            <li>Waivers</li>
                            <li>Non Payment Notice</li>
                            <li>Bond Claim</li>
                            <li>Intent To Lien</li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-12 wow fadeIn notice-list">
                        <h4>Need help - call us</h4>
                        <ul>
                            <li>Claim Of lien</li>
                            <li>Satisfaction Of  Lien</li>     
                            <li>Notice of Commencement</li>     
                            <li>Create Your Own Documents</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-------------notices ends-------------------->


<!-------------contact starts-------------------->
<section class="section-wrapper contact-us">
    <div class="main-wrapper">
        <div class="row">
            <div class="contact col-md-12 text-center">
                <h2>you're building our future, let us take care of yours</h2>
                <a href="{{url('/contact-us')}}" type="button" class="btn btn-primary customw-btn hvr-sweep-to-top"><span>Contact us</span></a>
                <a href="{{ route(config('adminlte.register_url', 'register')) }}" type="button" class="btn btn-primary custom-btn"><span>Join Us Today</span></a>
                <div class="row">
                    <div class="col-md-12 text-center">
                        <ul class="question list-inline">
                            <li>
                                <p>Have a question, call us:</p>
                            </li>
                            <li>
                                <a href="tel:1866 222-5479"><span><i class="fa fa-phone" aria-hidden="true"></i></span>1866 222-5479, (786)337 9602 </a>
                            </li>
                            <li>
                                <a href="mailto:info@aaanoticetoowner.com"><span><i class="fa fa-envelope" aria-hidden="true"></i></span>info@aaanoticetoowner.com</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-------------contact ends-------------------->
@stop