@extends('layouts.page')
@section('content')

<!--------------------login starts-------------------------->
<section class="login-sec sec-banner">
    <div class="main-wrapper">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
                <ul class="login-panel full-width">
                    <li class="">
                        @if (Session::get('success'))
                        <div class="flash-message">
                            <div class="alert alert-success">
                                <?php echo Session::get('success'); ?>
                            </div>
                        </div>

                        @elseif(session('error'))
                        <div class="flash-message">
                            <div class="alert alert-danger">
                                {{Session::get('error')}}
                            </div>
                        </div>
                        @endif

                        <div class="polygon-shape">
                            <div class="login-container">
                                <div class="login-div">
                                    <h2 class="full-width">Login</h2>
                                    
                                    <form class="form-horizontal full-width"  data-parsley-validate="" action="{{ url(config('adminlte.login_url', 'login')) }}" method="post">
                                        {!! csrf_field() !!}
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="input-wrapper full-width">
                                                    <div class="styled-input">
                                                      
                                                        <input class="form-control" type="email" name="email" value="{{old('email')}}" data-parsley-required="true" data-parsley-required-message="Email is required" data-parsley-trigger="change focusout" data-parsley-type="email">

                                                        <label>Email ID<span class="mandatory-field">*</span></label>
                                                        <span></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="input-wrapper full-width">
                                                    <div class="styled-input">
                                                        <input class="form-control" name="password" type="password" value="{{old('password')}}" data-parsley-required="true" data-parsley-required-message="Password is required" data-parsley-trigger="change focusout" data-parsley-minlength="6" data-parsley-maxlength="20">
                                                        <label>Password<span class="mandatory-field">*</span></label>
                                                        @if ($errors->has('password'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('password') }}</strong>
                                                        </span>
                                                        @endif
                                                        <span></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 text-right">
                                            <div class="form-group">
                                                <a href="{{ url(config('adminlte.password_reset_url', 'password/reset')) }}" class="password-link">Forgot your password?</a>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-primary custom-btn2"><span>Login</span></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="polygon-shape">
                            <div class="login-container">
                                <div class="register-div">
                                    <h2 class="full-width">Register</h2>
                                    <h6>Be the first to get paid</h6>
                                    <p>Have Your Mind at Ease, request your Prelim Notice today. </p>
                                    <a href="{{url('register')}}" type="button" class="btn btn-primary custom-btn2"><span>Register Now</span></a>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!--------------------login starts-------------------------->
@stop

@section('frontend_js')
<script src="{{ asset('vendor/adminlte/plugins/iCheck/icheck.min.js') }}"></script>
<script>

$(function () {
$('input').iCheck({
    checkboxClass: 'icheckbox_square-blue',
    radioClass: 'iradio_square-blue',
    increaseArea: '20%' // optional
});
});
</script>
<script>
    new WOW().init();
</script>
<script type="text/javascript">
   // $('input,select,textarea').attr('autocomplete','off');
    $(document).ready(function () {
        $('input,textarea,select').on('focus', function(e) {
        e.preventDefault();
        $(this).attr("autocomplete", "nope"); 
        });
        $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('#back-to-top').click(function () {
            $('#back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });

        $('#back-to-top').tooltip('show');
        $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('.navbar-brand').addClass('fixed-brand').css("transition", "0.3 all ease-in-out 0s");
            } else {
                $('.navbar-brand').removeClass('fixed-brand');
            }
        });
    });
</script>
@yield('js')
@stop
