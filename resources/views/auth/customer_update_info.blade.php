@extends('adminlte::page')
<style>
    .ui-datepicker-calendar {
        display: none;
    }
</style>
@section('content')
<section class="work-order address-book-add-readonly">
    <div class="dashboard-wrapper">
        <div class="dashboard-heading">
            <h1><span>Account Setting</span></h1>
        </div>

        <div class="dashboard-inner-body">
            <div class="notice-tabs">

                @if(Auth::user()->hasRole('super-admin') || Auth::user()->hasRole('admin') || Auth::user()->hasRole('account-manager'))
                <div class="full-width">
                    <ul class="nav nav-tabs display-ib pull-left" role="tablist">
                        <li role="presentation"  class="active"><a href="#admin_change_password" aria-controls="change_password" role="tab" data-toggle="tab">Change Password</a></li>
                    </ul>
                </div>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="admin_change_password">
                        <form  class="form" action="{{url('admin/change-password')}}" method="post" data-parsley-validate="">
                            {!! csrf_field() !!}
                            <div class="project-wrapper">
                                <div class="row">
                                    <div class="dash-subform">
                                        @if (Session::get('success'))
                                        <div class="flash-message">
                                            <div class="alert alert-success">
                                                <?php echo Session::get('success'); ?>
                                            </div>
                                        </div>

                                        @elseif(session('error'))
                                        <div class="flash-message">
                                            <div class="no-margin alert alert-danger">
                                                {{Session::get('error')}}
                                            </div>
                                        </div>
                                        @endif

                                        @if($errors->count())
                                        <div class="no-margin alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $e)
                                                <li>{{ $e }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        @endif
                                        <div class="col-md-8 col-sm-12">
                                            <div class="input-wrapper full-width">
                                                @php
                                                $label = trans('backpack::base.old_password');
                                                $field = 'old_password';
                                                @endphp
                                                <div class="styled-input">
                                                    <input autocomplete="new-password" class="form-control" type="password" name="{{ $field }}" id="{{ $field }}" value="" data-parsley-required="true" data-parsley-required-message="Old password is required"  data-parsley-trigger="change focusout">
                                                    <label>{{ $label }}<span class="mandatory-field">*</span></label>
                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8 col-sm-12">
                                            <div class="input-wrapper full-width">
                                                @php
                                                $label = trans('backpack::base.new_password');
                                                $field = 'new_password';
                                                @endphp
                                                <div class="styled-input">
                                                    <input autocomplete="new-password" class="form-control" required type="password" name="{{ $field }}" id="{{ $field }}" value=""  data-parsley-required="true" data-parsley-required-message="New password is required" data-parsley-trigger="change focusout">
                                                    <label>{{ $label }}<span class="mandatory-field">*</span></label>
                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8 col-sm-12">
                                            <div class="input-wrapper full-width">
                                                @php
                                                $label = trans('backpack::base.confirm_password');
                                                $field = 'confirm_password';
                                                @endphp
                                                <div class="styled-input">
                                                    <input autocomplete="new-password" class="form-control" required type="password" name="{{ $field }}" id="{{ $field }}" value=""  data-parsley-required="true" data-parsley-required-message="Confirm password is required" data-parsley-trigger="change focusout">
                                                    <label>{{ $label }}<span class="mandatory-field">*</span></label>
                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12">
                                <div class="row">
                                    <div class="form-bottom-btn">
                                        <a href="{{ backpack_url() }}" class="btn btn-primary custom-btn customb-btn"><span>{{ trans('backpack::base.cancel') }}</span></a>
                                        <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>{{ trans('backpack::base.change_password') }}</span></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                @endif
                <!-- Nav tabs -->
                @if(Auth::user()->hasRole('customer'))
                <div class="full-width">
                    <ul class="nav nav-tabs display-ib pull-left" role="tablist">
                        <li role="presentation" id="update_info" class="active"><a href="#update_account_info" aria-controls="update_account_info" role="tab" data-toggle="tab">Update Account Info</a></li>
                        <li role="presentation"><a href="#change_password" aria-controls="change_password" role="tab" data-toggle="tab">Change Password</a></li>
                        <li role="presentation"><a href="#package_pricing" aria-controls="package_pricing" role="tab" data-toggle="tab">Pricing</a></li>
                        <li role="presentation"><a href="#credit_cards" aria-controls="credit_cards" role="tab" data-toggle="tab">Credit Cards</a></li>
                    </ul>
                </div>

                <!-- Tab panes -->
                <div class="tab-content">

                <div role="tabpanel" class="tab-pane active" id="update_account_info">   
                    <span id="accounterrormessage" class="alert alert-danger alert-dismissable" style="display: none"></span>
                    <span id="accountsuccessmessage" class="alert alert-success alert-dismissable" style="display: none"></span>

                    <form data-parsley-validate="" id="submitAccountSettingForm" class="form" action="{{url('admin/change_account_setting')}}" method="post">
                        <div class="project-wrapper">
                            {!! csrf_field() !!}
                            <div class="success-msg">
                            </div>
                            @if(session()->has('message'))
                            <div class="no-margin alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                            @endif
                            @if(session()->has('error'))
                            @if ($errors->count())
                            <div class="no-margin alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $e)
                                    <li>{{ $e }}</li>
                                    @endforeach
                                </ul>
                            </div>
                            @endif
                            @endif
                            <div class="dash-subform mt-0 addcontactperson">

                                <!-- @if(Auth::user()->hasRole('admin'))-->

                                <!--                                    <div class="col-md-6 col-sm-6">
                                                        <div class="input-wrapper full-width">
                                                            <div class="styled-input">
                                                                <input type="text" name="name" class="form-control" value="{{ old('name') ? old('name'):$user['name']}}" required data-parsley-required-message="Full Name is required" data-parsley-trigger="change focusout" data-parsley-pattern="/^[a-zA-Z ]*$/">
                                                                <label>Name</label>
                                                                <span></span>
                                                            </div>
                                                            @if ($errors->has('name'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('name') }}</strong>
                                                            </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="input-wrapper full-width">
                                                            <div class="styled-input">
                                                                <input type="email" name="email" class="form-control" value="{{ old('email') ? old('email'):$user['email']}}" required data-parsley-required-message="Email is required" data-parsley-trigger="change focusout" data-parsley-type="email" readonly="">
                                                                <label>Email Address</label>
                                                                <span></span>
                                                            </div>

                                                        </div>
                                                    </div>-->
                                <!--                                    @endif-->
                                <div class="row">
                                    <h2>Login Details</h2>

                                    <div class="col-md-6 col-sm-6">
                                        <div class="input-wrapper full-width">

                                            <div class="styled-input">
                                                <input type="text" name="name" class="form-control" value="{{ old('name') ? old('name'):$user['name']}}" required data-parsley-required-message="Full Name is required" data-parsley-trigger="change focusout" data-parsley-pattern="/^[a-zA-Z ]*$/">
                                                <label>Name<span class="mandatory-field">*</span></label>
                                                <span></span>

                                                @if ($errors->has('name'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-sm-6">

                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="email" name="email" class="form-control"  value="{{ old('email')?old('email'):$user['email'] }}" data-parsley-required="true" data-parsley-required-message="Email is required" data-parsley-trigger="change focusout" data-parsley-type="email">
                                                <label>{{ trans('adminlte::adminlte.email') }}<span class="mandatory-field">*</span></label>
                                                <span></span>

                                                @if ($errors->has('email'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="dash-subform">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="company_name" class="form-control" value="{{ old('company_name') ? old('company_name'):$customer['company_name']}}" required data-parsley-required-message="Company Name is required" data-parsley-trigger="change focusout">
                                                <label>{{ trans('adminlte::adminlte.company_name') }}<span class="mandatory-field">*</span></label>
                                                <span></span>
                                                @if ($errors->has('company_name'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('company_name') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="contact_person" class="form-control" value="{{ old('contact_person') ? old('contact_person'):$customer['contact_person']}}" required data-parsley-required-message="Contact Person is required" data-parsley-trigger="change focusout">
                                                <label>{{ trans('adminlte::adminlte.contact_person') }}<span class="mandatory-field">*</span></label>
                                                <span></span>
                                                @if ($errors->has('contact_person'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('contact_person') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="dash-subform">
                                <div class="row">
                                    <h2>Mailing Address</h2>

                                    <div class="col-md-12 col-sm-12">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="mailing_address" class="form-control" value="{{ old('mailing_address') ? old('mailing_address'):$customer['mailing_address']}}" required data-parsley-required-message="Mailing Address is required" data-parsley-trigger="change focusout">
                                                <label>{{ trans('adminlte::adminlte.mailing_address') }}<span class="mandatory-field">*</span></label>
                                                <span></span>
                                                @if ($errors->has('mailing_address'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('mailing_address') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                <?php $mailing_city1 = $customer['mailing_city_id']; ?>
                                    <div class="col-md-4 col-sm-4">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input class="form-control"  type="text" name="mailing_city" value="{{old('mailing_city')?old('mailing_city'):$customer['mailing_city_name']}}" id="mailing_city_value" data-parsley-required="true" data-parsley-required-message="Select City from the list" data-parsley-trigger="change focusout">
                                                <label>City<span class="mandatory-field">*</span></label> 
                                                <span></span>
                                                @if ($errors->has('mailing_city'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('mailing_city') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                            <div id="cityloader" style="display: none">
                                                <img width="150px" src="{{asset('images/loader.gif')}}"/>
                                            </div>
                                            <input type="hidden" name="mailing_city_value_id" id="mailing_city_value_id" value="{{old('mailing_city_value_id')?old('mailing_city_value_id'):$mailing_city1}}"/>
                                        </div>
                                    </div>

                                  
                                    <div class="col-md-4 col-sm-4">
                                        <?php $mailing_state1 = $customer['mailing_state_id'] ?>

                                        <div class="select-opt">
                                            <label class="col-md-12 control-label" for="textinput">State<span class="mandatory-field">*</span></label>  
                                            <select name="mailing_state" class="selectpicker" required data-parsley-required-message="State is required" data-parsley-trigger="change focusout">
                                                @foreach($states as $mailing_state)
                                                <option value="{{$mailing_state->id}}" {{ $mailing_state1== $mailing_state->id ? 'selected="selected"' : '' }} > {{ucfirst(trans($mailing_state->name))}}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('mailing_state'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('mailing_state') }}</strong>
                                            </p>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-sm-4">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="mailing_zip" class="form-control" value="{{ old('mailing_zip') ? old('mailing_zip'):$customer['mailing_zip']}}"   placeholder="" required data-parsley-required-message="Zip code is required" data-parsley-trigger="change focusout" data-parsley-type="number" data-parsley-minlength="5" data-parsley-maxlength="5">
                                                <label>{{ trans('adminlte::adminlte.zip') }}<span class="mandatory-field">*</span></label>
                                                <span></span>
                                                @if ($errors->has('mailing_zip'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('mailing_zip') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="dash-subform">
                                <div class="row">
                                    <h2>Physical Address</h2>
                                    <div class="col-md-12 col-sm-12">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="physical_address" class="form-control" value="{{ old('company_physical_address') ? old('physical_address'):$customer['physical_address']}}" required data-parsley-required-message="Physical address is required" data-parsley-trigger="change focusout">
                                                <label>{{ trans('adminlte::adminlte.company_physical_address') }}<span class="mandatory-field">*</span></label>
                                                <span></span>
                                                @if ($errors->has('physical_address'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('physical_address') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <?php $physical_city1 = $customer['physical_city_id']; ?>
                                    <div class="col-md-4 col-sm-4">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input class="form-control"  type="text" name="physical_city" value="{{old('physical_city')?old('physical_city'):$customer['physical_city_name']}}" id="physical_city_value" data-parsley-required="true" data-parsley-required-message="Select City from the list" data-parsley-trigger="change focusout">
                                                <label>City<span class="mandatory-field">*</span></label> 
                                                <span></span>
                                                @if ($errors->has('physical_city'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('physical_city') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                            <div id="cityloader" style="display: none">
                                                <img width="150px" src="{{asset('images/loader.gif')}}"/>
                                            </div>
                                            <input type="hidden" name="physical_city_value_id" id="physical_city_value_id" value="{{old('physical_city_value_id')?old('physical_city_value_id'):$physical_city1}}"/>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4">
                                        <?php $physical_state1 = $customer['physical_state_id'] ?>
                                        <div class="select-opt">
                                            <label class="col-md-12 control-label" for="textinput">State<span class="mandatory-field">*</span></label>  
                                            <select name="physical_state" class="selectpicker" required data-parsley-required-message="State is required" data-parsley-trigger="change focusout">
                                                @foreach($states as $physical_state)
                                                <option value="{{$physical_state->id}}" {{ $physical_state1 == $physical_state->id ? 'selected="selected"' : '' }}> {{ucfirst(trans($physical_state->name))}}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('physical_state_id'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('physical_state') }}</strong>
                                            </p>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-sm-4">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="physical_zip" class="form-control" value="{{ old('physical_zip') ? old('physical_zip'):$customer['physical_zip']}}"  placeholder="{{ trans('adminlte::adminlte.zip') }}" required data-parsley-required-message="Zip Code is required" data-parsley-trigger="change focusout" data-parsley-type="number" data-parsley-minlength="5" data-parsley-maxlength="5">
                                                <label>{{ trans('adminlte::adminlte.zip') }}<span class="mandatory-field">*</span></label>
                                                <span></span>
                                                @if ($errors->has('physical_zip'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('physical_zip') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="hear_about" class="form-control" value="{{ old('hear_about') ? old('hear_about'):$customer['hear_about']}}" 
                                                        placeholder="">
                                                <label>{{ trans('adminlte::adminlte.hear_about') }}</label>
                                                <span></span>
                                                @if ($errors->has('hear_about'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('hear_about') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="office_number" id="office_number" class="form-control" value="{{ old('office_number') ? old('office_number'):$customer['office_number']}}"   placeholder="" required data-parsley-required-message="Office number is required" data-parsley-trigger="change focusout" data-parsley-pattern='(\(\d{3})\)\s\d{3}\-\d{4}$'>
                                                <label>{{ trans('adminlte::adminlte.office_number') }}<span class="mandatory-field">*</span></label>
                                                <span></span>
                                                @if ($errors->has('office_number'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('office_number') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="fax" id="fax_number" class="form-control" value="{{ old('number') ? old('fax'):$customer['fax_number']}}" placeholder="" data-parsley-trigger="change focusout" data-parsley-pattern='(\(\d{3})\)\s\d{3}\-\d{4}$'>
                                                <label>{{ trans('adminlte::adminlte.fax') }}</label>
                                                <span></span>
                                                @if ($errors->has('fax'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('fax') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <?php
                                    if (!empty($customer['company_email'])) {
                                        $company_arr = explode(',', $customer['company_email']);
                                    }
                                    ?>

                                    @if(!empty($company_arr))
                                    <div class="col-md-8 col-sm-8">
                                        <div id="html-div" class="input-wrapper full-width additional_email">
                                            @foreach($company_arr AS $e=>$each_company_email)

                                            <div class="styled-input">
                                                <input type="email" name="company_email[]" id="company_email"  class="form-control"  value="{{$each_company_email}}" required data-parsley-required-message="Email is required" data-parsley-trigger="change focusout" data-parsley-type="email"/>
                                                <label>Email<span class="mandatory-field">*</span></label>
                                                <span></span>
                                            </div>
                                            @if ($errors->has('company_email.'.$e))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('company_email.'.$e) }}</strong>
                                            </p>
                                            @endif
                                            @endforeach

                                        </div>
                                    </div>
                                    @endif

                                    <div class="col-md-12 col-sm-12">
                                        <div class="form-btn-div">
                                            <a class="btn btn-primary custom-btn form-btn screens-duplicate-button" ><span>+</span> Additional Email Address</a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="dash-subform">
                                <div class="row" id="reg_agents">
                                    <h2>Officer/Director</h2>
                                    @if(!empty($customer_agents))
                                    @foreach($customer_agents As $k=>$each_agent)
                                    <div>
                                        <div class="col-md-4 col-sm-4">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text" name="agent_first_name[]" class="form-control" value="{{old('agent_first_name.'.$k)?old('agent_first_name.'.$k):$each_agent->first_name}}" 
                                                            placeholder="" required data-parsley-required-message="First Name is required" data-parsley-trigger="change focusout" data-parsley-pattern="/^[a-zA-Z ]*$/">
                                                    <label>{{ trans('adminlte::adminlte.agent_first_name') }}<span class="mandatory-field">*</span></label>
                                                    <span></span>

                                                    @if ($errors->has('agent_first_name.'.$k))
                                                    <p class="help-block">
                                                        <strong>{{ $errors->first('agent_first_name.'.$k) }}</strong>
                                                    </p>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text" name="agent_last_name[]" class="form-control" value="{{old('agent_last_name.'.$k)?old('agent_last_name.'.$k):$each_agent->last_name}}" 
                                                            placeholder="" required data-parsley-required-message="Last Name is required" data-parsley-trigger="change focusout" data-parsley-pattern="/^[a-zA-Z ]*$/">
                                                    <label>{{ trans('adminlte::adminlte.agent_last_name') }}<span class="mandatory-field">*</span></label>
                                                    <span></span>

                                                    @if ($errors->has('agent_last_name.'.$k))
                                                    <p class="help-block">
                                                        <strong>{{ $errors->first('agent_last_name.'.$k) }}</strong>
                                                    </p>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text" name="agent_title[]" class="form-control" value="{{old('agent_title.'.$k)?old('agent_title.'.$k):$each_agent->title}}" 
                                                            placeholder="" required data-parsley-required-message="Title is required" data-parsley-trigger="change focusout" data-parsley-pattern="/^[a-zA-Z ]*$/">
                                                    <label>{{ trans('adminlte::adminlte.agent_title') }}<span class="mandatory-field">*</span></label>
                                                    <span></span>

                                                    @if ($errors->has('agent_title.'.$k))
                                                    <p class="help-block">
                                                        <strong>{{ $errors->first('agent_title.'.$k) }}</strong>
                                                    </p>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="rghtagent"><i data-id="{{$each_agent->id}}" class="fa fa-remove removeAgent removeExistingAgent"></i></div>
                                    @endforeach
                                    @endif
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="form-btn-div">
                                            <a class="btn btn-primary custom-btn form-btn agent-duplicate-button"><span>+</span> Add More Agents</a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="dash-subform">

                                @if(!empty($customer_repres) && count($customer_repres) > 0) 
                                <div class="row" id="representative">
                                    <h2>Representative</h2>
                                    @foreach($customer_repres As $k=>$each_repres)
                                    <div class="representativeDivs full-width">
                                        <div class="col-md-6 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text" name="repres_contact_person[]" class="form-control" value="{{old('repres_contact_person.'.$k)?old('repres_contact_person.'.$k):$each_repres->contact_person}}"  data-parsley-required-message="Contact person is required" data-parsley-trigger="change focusout" data-parsley-pattern="/^[a-zA-Z ]*$/">
                                                    <label>{{ trans('adminlte::adminlte.contact_person') }}</label>
                                                    <span></span>
                                                </div>
                                                @if ($errors->has('repres_contact_person.'.$k))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('repres_contact_person.'.$k) }}</strong>
                                                </span>
                                                @endif

                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text" name="repres_company_branch_name[]" class="form-control" value="{{old('repres_company_branch_name.'.$k)?old('repres_company_branch_name.'.$k):$each_repres->branch_name}}"  data-parsley-required-message="Company branch name is required" data-parsley-trigger="change focusout">
                                                    <label>{{ trans('adminlte::adminlte.repres_company_branch_name') }}</label>
                                                    <span></span>
                                                </div>
                                                @if ($errors->has('repres_company_branch_name.'.$k))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('repres_company_branch_name.'.$k) }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="email" name="repres_email[]" class="form-control" value="{{old('repres_email.'.$k)?old('repres_email.'.$k):$each_repres->email}}"  data-parsley-required-message="Mobile number is required" data-parsley-trigger="change focusout" >
                                                    <label>{{ trans('adminlte::adminlte.email') }}</label>
                                                    <span></span>
                                                </div>
                                                @if ($errors->has('repres_email.'.$k))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('repres_email.'.$k) }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text" name="repres_mobile_number[]" class="form-control mobile_no_masking" value="{{old('repres_mobile_number.'.$k)?old('repres_mobile_number.'.$k):$each_repres->mobile_number}}"  data-parsley-required-message="Mobile number is required" data-parsley-trigger="change focusout" data-parsley-pattern='(\(\d{3})\)\s\d{3}\-\d{4}$'>
                                                    <label>{{ trans('adminlte::adminlte.mobile_number') }}</label>
                                                    <span></span>
                                                </div>
                                                @if ($errors->has('repres_mobile_number.'.$k))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('repres_mobile_number.'.$k) }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="rght"><i data-id="{{$each_repres->id}}" class="fa fa-remove removeRepresentative removeExistingRepresentative "></i></div>
                                    @endforeach
                                </div>
                                @else
                                <div class="row" id="representative">
                                    <h2>Representative</h2>
                                    <div class="representativeDivs">
                                        <div class="col-md-6 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text" name="repres_contact_person[]" class="form-control" value="{{old('repres_contact_person.0')}}"  data-parsley-required-message="Contact person is required" data-parsley-trigger="change focusout" data-parsley-pattern="/^[a-zA-Z ]*$/">
                                                    <label>{{ trans('adminlte::adminlte.contact_person') }}</label>
                                                    <span></span>
                                                </div>
                                                @if ($errors->has('repres_contact_person.'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('repres_contact_person.') }}</strong>
                                                </span>
                                                @endif

                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text" name="repres_company_branch_name[]" class="form-control" value="{{old('repres_company_branch_name.0')}}"  data-parsley-required-message="Company branch name is required" data-parsley-trigger="change focusout">
                                                    <label>{{ trans('adminlte::adminlte.repres_company_branch_name') }}</label>
                                                    <span></span>
                                                </div>
                                                @if ($errors->has('repres_company_branch_name.'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('repres_company_branch_name.') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="email" name="repres_email[]" class="form-control" value="{{old('repres_email.0')}}"  data-parsley-required-message="Mobile number is required" data-parsley-trigger="change focusout" >
                                                    <label>{{ trans('adminlte::adminlte.email') }}</label>
                                                    <span></span>
                                                </div>
                                                @if ($errors->has('repres_email.'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('repres_email.') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text" name="repres_mobile_number[]" class="form-control mobile_no_masking" value="{{ old('repres_mobile_number.0')}}"  data-parsley-required-message="Mobile number is required" data-parsley-trigger="change focusout" data-parsley-pattern='(\(\d{3})\)\s\d{3}\-\d{4}$'>
                                                    <label>{{ trans('adminlte::adminlte.mobile_number') }}</label>
                                                    <span></span>
                                                </div>
                                                @if ($errors->has('repres_mobile_number.'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('repres_mobile_number.') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="form-btn-div">
                                            <a class="btn btn-primary custom-btn form-btn representative-duplicate-button"><span>+</span> Add Another Representative</a>
                                        </div>
                                    </div>
                                    {{-- <div class="col-md-6 col-sm-12">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input {{ $errors->has('no_of_offices') ? 'has-error' : '' }}">
                                                <input type="text" name="no_of_offices" class="form-control" value="{{ old('no_of_offices') ? old('no_of_offices'):$customer['no_of_offices']}}"
                                                id="number_offices"  data-parsley-required-message="No of offices is required" data-parsley-trigger="change focusout" data-parsley-type="digits">    
                                                <label>How many branches throughout the united states?</label>
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>  --}}
                                </div>

                    </div>
                    <div id="showBranchDiv">

                        @if(!empty($branches))
                        @foreach($branches AS $k=>$each_branch)


                        <div class="subdiv">
                            <div class="dash-subform">
                                <div class="row">
                                    <div class="col-md-4 col-sm-4">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="branch_name[]" value="{{old('branch_name.'.$k)?old('branch_name.'.$k):$each_branch->name}}" 
                                                        id="branch_name" class="form-control" />
                                                <label>{{ trans('adminlte::adminlte.branch_name') }}</label>
                                                <span></span>
                                                @if ($errors->has('branch_name.'.$k))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('branch_name.'.$k) }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="branch_phone[]"  value="{{old('branch_phone.'.$k)?old('branch_phone.'.$k):$each_branch->phone}}"  class="form-control branch_phone" data-parsley-trigger="change focusout" data-parsley-pattern='(\(\d{3})\)\s\d{3}\-\d{4}$'/>
                                                <label>{{ trans('adminlte::adminlte.branch_phone') }}</label>
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="branch_contact_person[]" value="{{old('branch_contact_person.'.$k)?old('branch_contact_person.'.$k):$each_branch->contact_person}}" class="form-control" />
                                                <label>{{ trans('adminlte::adminlte.contact_person') }}</label>
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8 col-sm-8">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="branch_address[]"  value="{{old('branch_address.'.$k)?old('branch_address.'.$k):$each_branch->address}}"  class="form-control" />
                                                <label>{{ trans('adminlte::adminlte.branch_address') }}</label>
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-sm-4">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input class="form-control"  type="text" name="branch_city[]" value="{{old('branch_city.'.$k)?old('branch_city.'.$k):$each_branch->branch_city_name}}" id="city_val{{$k}}"  data-parsley-trigger="change focusout">
                                                <label>City</label> 
                                                <span></span>
                                                @if ($errors->has('branch_city'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('branch_city') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                            <div class="bcityloader" style="display: none">
                                                <img width="150px" src="{{asset('images/loader.gif')}}"/>
                                            </div>
                                            <input type="hidden" name="branch_city_ids[]" class="branch_city_ids" id="branch_city_ids" value="{{$each_branch->city_id}}"/>

                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col-md-4 col-sm-4">
                                        <div class="select-opt">
                                            <?php $stateObject = []; ?>
                                            <label class="col-md-12 control-label" for="textinput">State</label>  
                                            <select name="branch_state[]"  class="selectpicker">

                                                @foreach($states as $branch_state)

                                                <?php $stateObject[$branch_state->id] = ucfirst(trans($branch_state->name)); ?>

                                                <option value="{{$branch_state->id}}" {{$each_branch->state_id == $branch_state->id?'selected="selected"':''}}> {{ucfirst(trans($branch_state->name))}}</option>
                                                @endforeach
                                            </select>

                                            @if ($errors->has('branch_state'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('branch_state') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" class="form-control branch_zip" name="branch_zip[]" value="{{old('branch_zip.'.$k)?old('branch_zip.'.$k):$each_branch->zip}}" placeholder="" data-parsley-trigger="change focusout" data-parsley-type="digits" data-parsley-minlength="5" data-parsley-maxlength="5"/>
                                                <label>{{ trans('adminlte::adminlte.zip') }}</label>
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="branch_country[]" value="{{old('branch_country.'.$k)?old('branch_country.'.$k):$each_branch->branch_county_name}}" class="form-control" id="county_val{{$k}}"/>
                                                <label>{{ trans('adminlte::adminlte.branch_country') }}</label>
                                                <span></span>
                                            </div>
                                            <div class="bcountyloader" style="display: none">
                                                <img width="150px" src="{{asset('images/loader.gif')}}"/>
                                            </div>

                                            <input type="hidden" name="branch_county_ids[]" class="branch_county_ids" id="branch_county_ids" value="{{$each_branch->country}}"/>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="sub-section-title">
                                            <h4>Registered Agents</h4>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="branch_first_name[]" value="{{old('branch_first_name.'.$k)?old('branch_first_name.'.$k):$each_branch->first_name}}" 
                                                        id="branch_first_name" class="form-control" />
                                                <label>{{ trans('adminlte::adminlte.agent_first_name') }}</label>
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="branch_last_name[]"  value="{{old('branch_last_name.'.$k)?old('branch_last_name.'.$k):$each_branch->last_name}}"  class="form-control"  />
                                                <label>{{ trans('adminlte::adminlte.agent_last_name') }}</label>
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="branch_title[]" value="{{old('branch_title.'.$k)?old('branch_title.'.$k):$each_branch->title}}" class="form-control" />
                                                <label>{{ trans('adminlte::adminlte.agent_title') }}</label>
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        @endforeach
                        @endif
                    </div> 

                    </div>
                    <div class="col-md-12 col-sm-12">
                        <div class="row">
                            <div class="form-bottom-btn">
                                @if(!Auth::user()->hasRole('customer'))
                                <a href="{{ backpack_url() }}" class="btn btn-primary custom-btn customb-btn"><span>{{ trans('backpack::base.cancel') }}</span></a>
                                @else
                                <a href="{{url('home') }}" class="btn btn-primary custom-btn customb-btn"><span>{{ trans('backpack::base.cancel') }}</span></a>
                                @endif
                                <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>{{ trans('backpack::base.save') }}</span></button>
                            </div>
                        </div>
                    </div>

                    </form>
                </div>

                <div role="tabpanel" class="tab-pane" id="change_password">
                    <span id="errormessage" class="alert alert-danger alert-dismissable" style="display: none"></span>
                    <span id="successmessage" class="alert alert-success alert-dismissable" style="display: none"></span>

                    <form id="submitPasswordForm" class="form" action="{{ url('admin/customer/change-password') }}" method="post">
                        <div class="project-wrapper">
                            {!! csrf_field() !!}
                            <div class="row">
                                <div class="dash-subform">
                                    @if (session('success'))
                                    <div class="no-margin alert alert-success">
                                        {{ session('success') }}
                                    </div>
                                    @endif
                                    @if ($errors->count())
                                    <div class="no-margin alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $e)
                                            <li>{{ $e }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif
                                    <div class="col-md-8 col-sm-12">
                                        <div class="input-wrapper full-width">
                                            @php
                                            $label = trans('backpack::base.old_password');
                                            $field = 'old_password';
                                            @endphp
                                            <div class="styled-input">
                                                <input autocomplete="new-password" class="form-control" required type="password" name="{{ $field }}" id="{{ $field }}" value="">
                                                <label>{{ $label }}</label>
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-sm-12">
                                        <div class="input-wrapper full-width">
                                            @php
                                            $label = trans('backpack::base.new_password');
                                            $field = 'new_password';
                                            @endphp
                                            <div class="styled-input">
                                                <input autocomplete="new-password" class="form-control" required type="password" name="{{ $field }}" id="{{ $field }}" value="">
                                                <label>{{ $label }}</label>
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-sm-12">
                                        <div class="input-wrapper full-width">
                                            @php
                                            $label = trans('backpack::base.confirm_password');
                                            $field = 'confirm_password';
                                            @endphp
                                            <div class="styled-input">
                                                <input autocomplete="new-password" class="form-control" required type="password" name="{{ $field }}" id="{{ $field }}" value="">
                                                <label>{{ $label }}</label>
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <div class="row">
                                <div class="form-bottom-btn">

                                    <input type="button" id="cancel" name="cancel" class="btn btn-primary custom-btn customb-btn" value="{{ trans('backpack::base.cancel') }}"/>
                                    <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>{{ trans('backpack::base.change_password') }}</span></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                <div role="tabpanel" class="tab-pane" id="package_pricing">
                    <?php
                    if (isset($pricing) && (!empty($pricing->toArray()))) {
                    } elseif (isset($main_pricing) && (!empty($main_pricing->toArray()))) {
                        $pricing = $main_pricing;
                    }
                    if (isset($pricing) && (!empty($pricing))) {
                        $range_package=config('constants.PACKAGE_RANGE');
                        $basePackageNoticeToOwner = array();
                        $basePackageBCOL = array();
                        $basePackageCOL = array();
                        foreach ($pricing as $p) {

                            if (($p->master_notice_id == config('constants.MASTER_NOTICE')['NTO']['ID']) && ($p->package_id == 1)) {
                                if(!$p->additional_address){
                                if(in_array($p->upper_limit,$range_package)){
                                  $p->upper_limit =array_search($p->upper_limit,$range_package);  
                                }
                                
                                $basePackageNoticeToOwner[] = array(
                                    'notice' => $p->notice->name,
                                    'limit' => $p->lower_limit . " - " . $p->upper_limit,
                                    'charge' => "$" . $p->charge
                                );
                            }
                            } else if (($p->master_notice_id == config('constants.MASTER_NOTICE')['BCOL']['ID']) && ($p->package_id == 1)) {
                                if(!$p->additional_address){
                                    if(in_array($p->upper_limit,$range_package)){
                                        $p->upper_limit =array_search($p->upper_limit,$range_package);  
                                    }

                                    $basePackageBCOL[] = array(
                                        'notice' => $p->notice->name,
                                        'limit' => $p->lower_limit . " - " . $p->upper_limit,
                                        'charge' => "$" . $p->charge
                                    );
                                }
                            } else if (($p->master_notice_id == config('constants.MASTER_NOTICE')['COL']['ID']) && ($p->package_id == 1)) {
                                if(!$p->additional_address){
                                    if(in_array($p->upper_limit,$range_package)){
                                        $p->upper_limit =array_search($p->upper_limit,$range_package);  
                                    }

                                    $basePackageCOL[] = array(
                                        'notice' => $p->notice->name,
                                        'limit' => $p->lower_limit . " - " . $p->upper_limit,
                                        'charge' => "$" . $p->charge
                                    );
                                }
                            }elseif ($p->package_id == 1) {
                                $otherBasePackagePricing[] = array(
                                    'notice' => $p->notice->name,
                                    'limit' => "",
                                    'charge' => "$" . $p->charge
                                );
                            }
                        }
                    }
                    ?>
                    <div class="row">
                        <div class="col-md-6">
                               <div class="panel panel-success">
                                    <div class="panel-heading">Base Package Charges</div>
                                    <div class="panel-body">
                                        <div class="table-responsive">    
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>Notice</th>
                                                        <th>Limit</th>
                                                        <th>Charges</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    if (isset($basePackageNoticeToOwner) && (!empty($basePackageNoticeToOwner))) {
                                                    $x = 0;
                                                    foreach ($basePackageNoticeToOwner as $row) {
                                                        $x++;
                                                        ?>
                                                        <tr>
                                                            <?php if ($x == 1) { ?>
                                                                <td rowspan="<?php echo(count($basePackageNoticeToOwner)) ?>"><?php echo($row['notice']); ?></td>
                                                            <?php } ?>
                                                            <td><?php echo($row['limit']); ?></td>
                                                            <td dir="rtl"><?php echo($row['charge']); ?></td>
                                                        </tr>
                                                    <?php } }?>
                                                    <tr height = 20px></tr>
                                                    <?php
                                                    if (isset($basePackageBCOL) && (!empty($basePackageBCOL))) {
                                                    $x = 0;
                                                    foreach ($basePackageBCOL as $row) {
                                                        $x++;
                                                        ?>
                                                        <tr>
                                                            <?php if ($x == 1) { ?>
                                                                <td rowspan="<?php echo(count($basePackageBCOL)) ?>"><?php echo($row['notice']); ?></td>
                                                            <?php } ?>
                                                            <td><?php echo($row['limit']); ?></td>
                                                            <td dir="rtl"><?php echo($row['charge']); ?></td>
                                                        </tr>
                                                    <?php } }?>
                                                    <tr height = 20px></tr>
                                                    <?php
                                                    if (isset($basePackageCOL) && (!empty($basePackageBCOL))) {
                                                    $x = 0;
                                                    foreach ($basePackageCOL as $row) {
                                                        $x++;
                                                        ?>
                                                        <tr>
                                                            <?php if ($x == 1) { ?>
                                                                <td rowspan="<?php echo(count($basePackageCOL)) ?>"><?php echo($row['notice']); ?></td>
                                                            <?php } ?>
                                                            <td><?php echo($row['limit']); ?></td>
                                                            <td dir="rtl"><?php echo($row['charge']); ?></td>
                                                        </tr>
                                                    <?php } }?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                         <div class="col-md-6">
                            <?php if (isset($otherBasePackagePricing) && (!empty($otherBasePackagePricing))) { ?>
                                <div class="panel panel-success">
                                    <div class="panel-heading">Base Package Per Notice Charges Post Subscription</div>
                                    <div class="panel-body">
                                        <div class="table-responsive">    
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th>Notice</th>
                                                        <th>Charges</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    foreach ($otherBasePackagePricing as $row) {
                                                        $x++;
                                                        ?>
                                                        <tr>
                                                            <td><?php echo($row['notice']); ?></td>
                                                            <td dir="rtl"><?php echo($row['charge']); ?></td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="row">
                                            
                        @if(!empty($customer_subscription))
                        <div class="col-md-12">
                            <div class="panel panel-success">
                                <div class="panel-heading">Subscription Details</div>
                                <div class="panel-body">
                                    @php($today = date('Y-m-d'))
                                    @if($customer_subscription['end_date'] >= $today)
                                    <div class="table-responsive">          
                                        <table class="table table-bordered">
                                            <tr>
                                                <td>Package selected</td>
                                                <td>{{$customer_subscription->package->name}}</td>
                                            </tr>
                                            <tr>
                                                <td>Amount</td>
                                                <td>${{$customer_subscription->charges}}</td>
                                            </tr>
                                            <tr>
                                                <td>Subscription Start Date</td>
                                                <td>{{date('d M Y',strtotime($customer_subscription->start_date))}}</td>
                                            </tr>
                                            <tr>
                                                <td>Subscription End Date</td>
                                                <td>{{date('d M Y',strtotime($customer_subscription->end_date))}}</td>
                                            </tr>
                                        </table>
                                    </div>
                                    @else
                                    <p class="error">Your subscription has ended.Please renew your subscription</p>
                                    @endif
                                </div>
                            </div>
                        </div>
                        @else
                        @if(Session::get('warning'))
                        <div class="col-md-12">                                    
                            <div class="alert alert-warning">
                                <h2>{{Session::get('warning')}} <a href="{{url('pricing')}}">here</a></h2>
                            </div>
                        </div>
                        @endif
                        @endif
                    </div>
                </div>

                <div role="tabpanel" class="tab-pane" id="credit_cards">   
                    <span id="accounterrormessage" class="alert alert-danger alert-dismissable" style="display: none"></span>
                    <span id="accountsuccessmessage" class="alert alert-success alert-dismissable" style="display: none"></span>

                    <form id="credit-card-form" class="form-horizontal" data-parsley-validate="" action="{{ url('customer/check-credit-card') }}" method="post">
                        <div class="project-wrapper">
                            {!! csrf_field() !!}
                            <input type="hidden" name="card_save_module" value="account_setting">
                            <div class="success-msg">
                            </div>
                            @if(session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                            @endif
                            @if(session()->has('success'))
                            <div class="alert alert-success">
                                {{ session()->get('success') }}
                            </div>
                            @endif
                            @if ($errors->count())
                                <div class="no-margin alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $e)
                                        <li>{{ $e }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            @if(!empty(session()->get('error')))
                            <div class="no-margin alert alert-danger">
                                {{ session()->get('error') }}
                            </div>
                            @endif
                            <div class="dash-subform mt-0">
                            <div class="row">
                                @if(!empty($user_cards))
                                
                                <h2>Saved Cards</h2>
                                
                                <div class="col-md-12 col-sm-12">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                            <?php $i=0; ?>
                                            @foreach($user_cards as $card)
                                           <?php
                                                $ico = 'credit-card';
                                                if($ico == 'American Express')
                                                    $ico = 'amex';
                                                else
                                                    $ico = 'cc-'.$card['cardType'];
                                            ?>
                                                <div class="col-md-4 div-{{$card['id']}}" >
                                                    <div class="thumbnail">
                                                        <div class="caption">
                                                            <div class='col-lg-12'><h4>{{ucwords($card['name'])}}</h4>
                                                            </div>
                                                            <div class='col-lg-12 well well-add-card'>
                                                                <h4>{{$card['number']}}
                                                                    <span class="fa fa-{{$ico}} pull-right"></span>
                                                                    <span class="pull-right">{{$card['expMonth']}}-{{$card['expYear']}}</span>
                                                                </h4>
                                                            </div>
                                                            <button type="button" class="btn btn-danger remove-card" id="{{$card['id']}}">Remove</button>
                                                            @if(date('Y')< $card['expYear'] && date('m') < $card['expMonth'])
                                                                <span class='text-danger pull-right'>Expired</span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                @endif
                            </div>
                            <div class="row">
                                    <h2>Credit Card Details</h2>
                                    <div class="col-md-4 col-sm-4">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                            
                                                <input id="credit_card_name" type="text" name="credit_card_name" class="form-control" data-parsley-required="true" value="{{ old('credit_card_name') }}" data-parsley-required-message="Person Name is required" data-parsley-trigger="change focusout" data-parsley-pattern="/^[a-zA-Z ]*$/" maxlength="100">    
                                                <label>Person's name (As it appears on card)<span class="mandatory-field">*</span></label>
                                                <span></span>
                                                @if ($errors->has('credit_card_name'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('credit_card_name') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-sm-4">
                                        <div class="select-opt">
                                            <label class="col-md-12 control-label" for="textinput">Credit Card Type<span class="mandatory-field">*</span></label>
                                            <select name="card_type" class="selectpicker" data-show-subtext="true" data-live-search="true" data-parsley-required="true" data-parsley-required-message="Credit Card Type is required" data-parsley-trigger="change focusout" id='card_type'>
                                                <option value="">-Select-</option>
                                                @php ($card_types = ['Mastercard'=>'Mastercard','Visa'=>'Visa','American Express'=>'American Express','Cyrus'=>'Cyrus','Maestro'=>'Maestro'])
                                                @foreach($card_types as $key=>$val)
                                                <option value="{{$key}}" @if (old('card_type') == $key) selected="selected" @endif > {{$val}}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('card_type'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('card_type') }}</strong>
                                            </p>
                                            @endif
                                        </div>  
                                    </div>

                                    <div class="col-md-4 col-sm-4">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" id="datepicker" name="credit_card_date" class="form-control" data-parsley-required="true" value="{{ old('credit_card_date') }}" data-parsley-required-message="Expiration Date is required" data-parsley-trigger="change input ">   <label>Expiration Date<span class="mandatory-field">*</span></label>
                                                <span></span>
                                                @if ($errors->has('credit_card_date'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('credit_card_date') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-sm-4">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input id="card_number" pattern="[0-9\s]*" type="text" name="card_number" class="form-control" data-parsley-required="true" value="{{ old('card_number') }}" data-parsley-required-message="Card Number is required" data-parsley-trigger="change focusout">       
                                                <label>Card Number<span class="mandatory-field">*</span></label>
                                                <span></span>
                                                @if ($errors->has('card_number'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('card_number') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="security_code" class="form-control stylish_security_code" data-parsley-required="true" value="{{ old('security_code') }}" data-parsley-required-message="Security Code is required" data-parsley-minlength="3" data-parsley-maxlength="3" data-parsley-minlength-message="Security code must be 3 digits" data-parsley-maxlength-message="Security code must be 3 digits" data-parsley-trigger="change focusout">    
                                                <input type="hidden" name="security_code" class="hidden_security_code" value="{{ old('security_code') }}">
                                                <label>Security Code<span class="mandatory-field">*</span></label>
                                                <span></span>
                                                @if ($errors->has('security_code'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('security_code') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="credit_zip_code" class="form-control" data-parsley-required="true" value="{{ old('credit_zip_code') }}" data-parsley-required-message="Zip Code of Credit Card is required" data-parsley-trigger="change focusout" data-parsley-type="digits" data-parsley-minlength="5" data-parsley-maxlength="5">    
                                                <label>Zip Code of Credit Card<span class="mandatory-field">*</span></label>
                                                <span></span>
                                                @if ($errors->has('credit_zip_code'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('credit_zip_code') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <div class="input-wrapper full-width">
                                            <input type="checkbox" id="terms_checkbox" class="regular-checkbox" data-parsley-required-message="Accept terms and conditions" required data-parsley-trigger="change focusout" name="terms" data-parsley-required="true">

                                            <label for="terms_checkbox_label" class="agree-label">I agree to the AAA Business Associates authorization to charge on my credit card.</label>
                                            @if ($errors->has('terms'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('terms') }}</strong>
                                            </p>
                                            @endif
                                        </div>
                                    </div><br><br>
                                
                                    <div class="col-md-12 col-sm-12" style="top: 30px;">
                                        <div class="panel-group">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4>
                                                        <a data-toggle="collapse" href="#collapse1">Terms and Conditions</a>
                                                    </h4>
                                                </div>
                                                <div id="collapse1" class="panel-collapse collapse">
                                                    <div class="panel-body">
                                                        <p><span id="card_holder_name_undertaking">@if(isset($customer_name))
                                                                {{ $customer_name }}
                                                                @else (Card Holder Name)
                                                                @endif</span> hereby represent that once this authorization form is signed, I agree to pay the full amount charged and there will be no cancellation or refund.</p>
                                                        <p>If and only if, AAA Business Assoc. DBA AAA Notice To Owner do not receive full statement payment, we AAA Notice to Owner have the right to charge your Credit or Debit Card.</p>
                                                        <p>I understand and consent to the use of myCredit or Debit Card without my signature on the charge that a photocopy of this agreement will serve as an original, and this Credit or Debit Card authorization cannot be revoked</p>
                                                        <p>Once the transaction has been completed, you will receive an email or text message with a copy of your receipt.</p>
                                                        <p>**** NOTE: AAA will be using Quickbooks to complete payments. How will you fulfill remaining balances for partial payments made for statements? ****</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <div class="form-bottom-btn">
                                            <button type="button" class="btn btn-primary custom-btn customc-btn" id="save_card"><span>Save Card</span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>  
            </div>
            @endif
        </div>
    </div>
</div>

</section>
<div id="successModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Account Setting Message</h4>
            </div>
            <div class="modal-body">
                <div id="account_setting_message"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
 <form action="" method="GET" class="remove-record-model">
                    <input type="hidden" name="_token" value="ABjR1JugZhktskqmtZI1bYa7dA4FjvtrR8jq3275">
                    <div id="contact-confirmation-modal" class="modal fade">
                        <div class="modal-dialog modal-confirm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <div class="icon-box">
                                        <i class="fa fa-close"></i>
                                    </div>              
                                    <h4 class="modal-title">Are you sure?</h4>  
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                </div>
                                <div class="modal-body">
                                    <input type="text" name="contact_id" id="contact_id" hidden="">              
                                    <p>Do you really want to delete these records? This process cannot be undone.</p>
                                </div>
                                <div class="modal-footer">
                                    <div class="form-bottom-btn">
                                        <button type="button" class="btn btn-primary custom-btn customb-btn remove-data-from-delete-form" data-dismiss="modal"><span>Cancel</span></button>
                                        <button type="button" class="btn btn-primary custom-btn customc-btn" id="confirm-delete"><span>Confirm</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

@stop
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@section('frontend_js')
<script type="text/javascript" src="{{ asset('js/jquery-ui.min.js') }}"></script>

<script type="text/javascript">
    


$(document).ready(function () {
    $('input,textarea,select').on('focus', function(e) {
        e.preventDefault();
        $(this).attr("autocomplete", "nope");
        $('#credit_card_name').attr("autocomplete", "off"); 
    });
    $('#cancel').click(function () {
    $('.display-ib li').removeClass('active');
    $('#update_info').addClass('active');
    $('.notice-tabs .tab-pane').removeClass('active');
    $('#update_account_info').addClass('active');
});
    //Autocomplete for already exist branch

     var exist_county_arr = [];
    var exist_city_arr = [];
    $('input[id^="city_val"]').on("focus.autocomplete", null, function () {
        $(this).autocomplete({

            source: function (request, response) {
                $.ajax({
                    url: src_city,
                    dataType: "json",
                    data: {
                        term: request.term
                    },
                    beforeSend: function () {
                        $('.bcityloader').show();
                    },
                    complete: function () {
                        $('.bcityloader').hide();
                    },
                    success: function (data) {

                        response($.map(data, function (item) {
                            return {
                                value: item.value,
                                id: item.id     // EDIT
                            }
                        }));

                    },

                });
            },
            select: function (event, ui) {
                var cityid = ui.item.id;

                exist_city_arr.push(cityid);
                $("#branch_city_ids").val(exist_city_arr);

                $(this).val(ui.item.value);
            }
        });
    });
    src_county_name = "{{ url('customer/city/autocompletecountyname') }}";
    $('input[id^="county_val"]').on("focus.autocomplete", null, function () {

        $(this).autocomplete({

            source: function (request, response) {
                $.ajax({
                    url: src_county_name,
                    dataType: "json",
                    data: {
                        term: request.term
                    },
                    beforeSend: function () {
                        $('.bcountyloader').show();
                    },
                    complete: function () {
                        $('.bcountyloader').hide();
                    },
                    success: function (data) {

                        response($.map(data, function (item) {
                            return {
                                value: item.value,
                                id: item.id     // EDIT
                            }
                        }));

                    },

                });
            },
            select: function (event, ui) {
                var countyid = ui.item.id;

                exist_county_arr.push(countyid);

                $("#branch_county_ids").val(exist_county_arr);
                $(this).val(ui.item.value);
            }
        });
    }); 
});
$("#office_number").mask("(999) 999-9999");
$('.mobile_no_masking').mask("(999) 999-9999");
$("#fax_number").mask("(999) 999-9999");
$(".branch_phone").mask("(999) 999-9999");
$(function () {
    var formData = new FormData($('#edit-account-setting')[0]);


    $(document).on('submit', '#edit-account-setting', function (e) {
        e.preventDefault();

        $('input+span>strong').text('');
        $('input').parent().parent().removeClass('has-error');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "POST",
            url: "{{ route('backpack.account.info') }}",
            dataType: 'json',
            processData: false,
            contentType: false,
            cache: false,
            data: formData
        })
                .done(function (data) {
                    $("div .success-msg").html('Account settings updated successfully');
                })
                .fail(function (data) {
                    $.each(data.responseJSON, function (key, value) {
                        var input = '#edit-account-setting input[name=' + key + ']';
                        $(input + '+span>strong').text(value);
                        $(input).parent().parent().addClass('has-error');
                    });
                });
    });
});
// $(document).ready(function () {


var counter = 1;
var x = 1;
var company_limit = 3;
var agent_limit = 3;
var repres_limit = 3;
// var questions = $('#html-div').html();


$('.screens-duplicate-button').click(function ()
{
    var questions = '<div class="styled-input">' +
            '<input type="email" name="company_email[]" id="company_email"  class="form-control email_validation"  value="" data-parsley-trigger="change focusout" data-parsley-type="email"/>'
    '<label></label> <span></span>' +
            '</div>';

    if (x < company_limit)
    {
        counter++;
        //$('#html-div').append(questions);
        var eleLength = $('#html-div')[0].childElementCount;
        var element = $('#html-div')[0].children;
        var i = 0;
        $(element).each(function (index) {

            if (index === eleLength - 1) {
                // $(this).attr('id','quetions_'+ counter);
                $(this).after(questions + '<i id="additionalEmail' + x + '" class="fa fa-remove removeAdditionalEmail" />');

            }
            i++;
        });
        x++;
    } else {
        alert('You can add company email upto 3 only');
    }
    $('input').keyup(function () {
        if ($(this).val().length > 0) {
            $(this).addClass('not-empty');
            $('.email_validation').attr('data-parsley-type', 'email')
                    .attr('data-parsley-trigger', 'change focusout')
                    .parsley();
        } else {
            $(this).removeClass('not-empty');
            $('.email_validation').removeAttr('data-parsley-type', 'email')
                    .removeAttr('data-parsley-trigger', 'change focusout')
                    .parsley().destroy();
        }
    });
});

//Registered Agents
var agentCount = 1;

$('.agent-duplicate-button').click(function () {
    var agentDiv = '<div>' +
            '<div class="col-md-4 col-sm-4">' +
            '<div class="input-wrapper full-width">' +
            '<div class="styled-input">' +
            '<input type="text" name="agent_first_name[]" class="form-control" value=""placeholder=""  data-parsley-trigger="change focusout" data-parsley-pattern="/^[a-zA-Z ]*$/">' +
            '<label>First Name</label>' +
            '<span></span>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-4 col-sm-4">' +
            '<div class="input-wrapper full-width">' +
            '<div class="styled-input">' +
            '<input type="text" name="agent_last_name[]" class="form-control" value="" placeholder="" data-parsley-trigger="change focusout" data-parsley-pattern="/^[a-zA-Z ]*$/">' +
            '<label>Last Name</label>' +
            '<span></span>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-4 col-sm-4">' +
            '<div class="input-wrapper full-width">' +
            '<div class="styled-input">' +
            '<input type="text" name="agent_title[]" class="form-control" value="" placeholder=""  data-parsley-trigger="change focusout" data-parsley-pattern="/^[a-zA-Z ]*$/">' +
            '<label>Title</label>' +
            '<span></span>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>';

    if (agentCount < agent_limit) {
        var eleLength = $('#reg_agents')[0].childElementCount;
        var element = $('#reg_agents')[0].children;

        $(element).each(function (index) {

            if (index === eleLength - 1) {
                // $(this).attr('id','quetions_'+ counter);
                $(this).after(agentDiv + '<div class="rghtagent"><i id="agent' + agentCount + '" class="fa fa-remove removeAgent" /></div>');

            }
            $('input').keyup(function () {
                if ($(this).val().length > 0) {
                    $(this).addClass('not-empty');

                } else {
                    $(this).removeClass('not-empty');

                }
            });
        });
        agentCount++;
    } else {
        alert('You can add officers upto 3 only');
    }
    $('input').keyup(function () {
        if ($(this).val().length > 0) {
            $(this).addClass('not-empty');

        } else {
            $(this).removeClass('not-empty');

        }
    });

});



//Representative
var represCount = 1;
// var representativeDiv = $('#representative').html();

$('.representative-duplicate-button').click(function () {
    var representativeDiv = '<div class="representativeDivs full-width">' +
            '<div class="col-md-6 col-sm-6">' +
            '<div class="input-wrapper full-width">' +
            '<div class="styled-input">' +
            '<input type="text" name="repres_contact_person[]" class="form-control" value="" data-parsley-trigger="change focusout" data-parsley-pattern="/^[a-zA-Z ]*$/">' +
            '<label>Contact Person</label>' +
            '<span></span>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-6 col-sm-6">' +
            '<div class="input-wrapper full-width">' +
            '<div class="styled-input">' +
            '<input type="text" name="repres_company_branch_name[]" class="form-control" value="" data-parsley-trigger="change focusout" data-parsley-pattern="/^[a-zA-Z ]*$/">' +
            '<label>Position</label>' +
            '<span></span>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-6 col-sm-6">' +
            '<div class="input-wrapper full-width">' +
            '<div class="styled-input">' +
            '<input type="email" name="repres_email[]" class="form-control email_validation" value="" data-parsley-trigger="change focusout" data-parsley-type="email">' +
            '<label>Email Address</label>' +
            '<span></span>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-6 col-sm-6">' +
            '<div class="input-wrapper full-width">' +
            '<div class="styled-input">' +
            '<input type="text" name="repres_mobile_number[]" class="form-control mobile_validation mobile_no_masking" value="" data-parsley-trigger="change focusout">' +
            '<label>Mobile Number</label>' +
            '<span></span>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>';


    if (represCount < repres_limit) {
        var eleLength = $('#representative')[0].childElementCount;
        var element = $('#representative')[0].children;
        $(element).each(function (index) {

            if (index === eleLength - 1) {
                // $(this).attr('id','quetions_'+ counter);
                $(this).after(representativeDiv + '<div class="rght"><i id="repres' + represCount + '" class="fa fa-remove removeRepresentative" /></div>');
                // $(this).append();
            }
            $('input').keyup(function () {
                if ($(this).val().length > 0) {
                    $(this).addClass('not-empty');
                    $('.email_validation').attr('data-parsley-type', 'email')
                            .attr('data-parsley-trigger', 'change focusout')
                            .parsley();

//                    $('.mobile_validation').attr('data-parsley-type','digits')
//                                        .attr('data-parsley-trigger','change focusout')
//                                        .parsley();
                } else {
                    $(this).removeClass('not-empty');
                    $('.email_validation').removeAttr('data-parsley-type', 'email')
                            .removeAttr('data-parsley-trigger', 'change focusout')
                            .parsley().destroy();

//                     $('.mobile_validation').removeAttr('data-parsley-type','digits')
//                                        .removeAttr('data-parsley-trigger','change focusout')
//                                        .parsley().destroy();
                }
            });
            $('.mobile_no_masking').mask("(999) 999-9999");
        });
        represCount++;
    }
});
$(document).on('click', '.removeAdditionalEmail', function () {

    $('#' + $(this).attr('id')).parent("div").remove();
    //$('#' + $(this).attr('id')).remove();
    company_limit++;
});
$(document).on('click', '[id^="agent"].removeAgent', function () {

    $('#' + $(this).attr('id')).parent().prev("div").remove();
    $('#' + $(this).attr('id')).remove();
    agent_limit++;
});

//remove existing agents from db
$(document).on('click', '.removeExistingAgent', function () {
    var agentId = $(this).data('id');
    var parentDiv = $(this).parent();

    if (confirm("You are removing Officer/Director record. Are you sure?") == true) {

        $.ajax({
            url: remove_agent+'/'+agentId,
            success: function (data) {
                if(data.result == true) {
                    parentDiv.prev("div").remove();
                    parentDiv.remove();
                } else {
                    alert('Error while removing...');
                }
            },
        });
    } 
});

$(document).on('click', '[id^="repres"].removeRepresentative', function () {
    $('#' + $(this).attr('id')).parent().prev("div").remove();
    $('#' + $(this).attr('id')).parent().remove();
    repres_limit++;
});
//remvoe existing representative from db
$(document).on('click', '.removeExistingRepresentative', function () {
    var agentId = $(this).data('id');
    var parentDiv = $(this).parent();

    if (confirm("You are removing representative record. Are you sure?") == true) {
        $.ajax({
            url: remove_representative+'/'+agentId,
            success: function (data) {
                if(data.result == true) {
                    parentDiv.prev("div").remove();
                    parentDiv.remove();
                } else {
                    alert('Error while removing...');
                }
            },
        });
    } 
});

if ($("#showBranchDiv > div").length < 0) {
    $('#showBranchDiv').hide();
}



$('#number_offices').keyup(function () {
    var branch_div_html = '<div class="dash-subform">' +
            '<div class="row">' +
            '<div class="col-md-4 col-sm-4">' +
            '<div class="input-wrapper full-width">' +
            '<div class="styled-input">' +
            '<input type="text" name="branch_name[]" value="" id="branch_name" class="form-control" />' +
            '<label>Branch Name</label>' +
            '<span></span>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-4 col-sm-4">' +
            '<div class="input-wrapper full-width">' +
            '<div class="styled-input">' +
            '<input type="text" name="branch_phone[]"  value=""  class="form-control branch_phone" data-parsley-trigger="change focusout" data-parsley-pattern="(\(\d{3})\)\s\d{3}\-\d{4}$"/>' +
            '<label>Phone Number</label>' +
            '<span></span>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-4 col-sm-4">' +
            '<div class="input-wrapper full-width">' +
            '<div class="styled-input">' +
            '<input type="text" name="branch_contact_person[]" value="" class="form-control" />' +
            '<label>Contact Person</label>' +
            '<span></span>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="row">' +
            '<div class="col-md-8 col-sm-8">' +
            '<div class="input-wrapper full-width">' +
            '<div class="styled-input">' +
            '<input type="text" name="branch_address[]"  value=""  class="form-control" />' +
            '<label>Address</label>' +
            '<span></span>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-4 col-sm-4">' +
            '<div class="input-wrapper full-width">' +
            '<div class="styled-input">' +
            '<input class="form-control branch_city_val"  type="text" name="branch_city[]" >' +
            '<label>City</label>' +
            '<span></span>' +
            '</div>' +
            '<div class="branchcityloader" style="display: none">' +
            '<img width="150px" src=""/>' +
            '</div>' +
            '<input type="hidden" name="branch_city_ids[]" id="dynamic_branch_city_ids"/>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="row">' +
            '<div class="col-md-4 col-sm-4">' +
            '<div class="select-opt">' +
            '<label class="col-md-12 control-label" for="textinput">State</label>' +
            '<select name="branch_state[]"   class="selectpicker bstate">' +
            '<option value="">Select</option>' +

            '</select>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-4 col-sm-4">' +
            '<div class="input-wrapper full-width">' +
            '<div class="styled-input">' +
            '<input type="text" class="form-control branch_zip" name="branch_zip[]" value="" placeholder="" data-parsley-trigger="change focusout" data-parsley-type="digits" data-parsley-minlength="5" data-parsley-maxlength="5"/>' +
            '<label>Zip Code</label>' +
            '<span></span>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-4 col-sm-4">' +
            '<div class="input-wrapper full-width">' +
            '<div class="styled-input">' +
            '<input class="form-control branch_country"  type="text" name="branch_country[]">' +
            '<label>County</label>' +
            '<span></span>' +
            '</div>' +
            '<div class="branchcountyloader" style="display: none">' +
            '<img width="150px" src=""/>' +
            '</div>' +
            '<input type="hidden" name="branch_county_ids[]" id="branch_county_ids" class="branch_county_ids"/>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="row">' +
            '<div class="col-md-12 col-sm-12">' +
            '<div class="sub-section-title">' +
            '<h4>Registered Agents</h4>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-4 col-sm-4">' +
            '<div class="input-wrapper full-width">' +
            '<div class="styled-input">' +
            '<input type="text" name="branch_first_name[]" value"" id="branch_first_name" class="form-control" />' +
            '<label>First Name</label>' +
            '<span></span>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-4 col-sm-4">' +
            '<div class="input-wrapper full-width">' +
            '<div class="styled-input">' +
            '<input type="text" name="branch_last_name[]"  value=""  class="form-control"  />' +
            '<label>Last Name</label>' +
            '<span></span>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-4 col-sm-4">' +
            '<div class="input-wrapper full-width">' +
            '<div class="styled-input">' +
            '<input type="text" name="branch_title[]" value="" class="form-control" />' +
            '<label>Title</label>' +
            '<span></span>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>';
    var branchDiv = document.getElementById('showBranchDiv');
    var number_of_offices = $('#number_offices').val();
    var prev_show_div = $("#showBranchDiv > div").length;

    if ($(this).val() == "") {
        $('#showBranchDiv').hide();
    } else {
        var i;
        if (number_of_offices <= 3) {
            var branch_div_html_1 = '';
            var prev_branch_div = prev_show_div;
            for (i = prev_branch_div; i < number_of_offices; i++) {
                branch_div_html_1 += branch_div_html;
               
                              

                 $('.bstate').selectpicker('refresh');
                $('#showBranchDiv').append(branch_div_html_1);
                $('#showBranchDiv').show();

                $('input').keyup(function () {
                    if ($(this).val().length > 0) {
                        $(this).addClass('not-empty');
                        $('.branch_zip').attr('data-parsley-type', 'digits')
                                .attr('data-parsley-minlength', '5')
                                .attr('data-parsley-maxlength', '5')
                                .attr('data-parsley-trigger', 'change focusout')
                                .parsley();
                        $(".branch_phone").mask("(999) 999-9999");


                        $(".branch_phone").on("blur", function () {
                            var last = $(this).val().substr($(this).val().indexOf("-") + 1);

                            if (last.length == 5) {
                                var move = $(this).val().substr($(this).val().indexOf("-") + 1, 1);

                                var lastfour = last.substr(1, 4);

                                var first = $(this).val().substr(0, 9);

                                $(this).val(first + move + '-' + lastfour);
                            }
                        });

                    } else {
                        $(this).removeClass('not-empty');
                        $('.branch_zip').removeAttr('data-parsley-type')
                                .removeAttr('data-parsley-trigger')
                                .removeAttr('data-parsley-minlength')
                                .removeAttr('data-parsley-maxlength')
                                .parsley();

                    }
                });
            }
        } else {
            alert('Please enter number of brances upto 3');
        }
    }
//Autocomplete for branch city

    var county_arr = [];
    var city_arr = [];
    var prev_arr =$("input[name=branch_city_ids]").val();
   console.log(prev_arr);
    var prev_county_arr = $(".branch_county_ids").val();
    $(".branch_city_val").on("focus.autocomplete", null, function () {
        $(this).autocomplete({

            source: function (request, response) {
                $.ajax({
                    url: src_city,
                    dataType: "json",
                    data: {
                        term: request.term
                    },
                    beforeSend: function () {
                        $('.branchcityloader').show();
                    },
                    complete: function () {
                        $('.branchcityloader').hide();
                    },
                    success: function (data) {

                        response($.map(data, function (item) {
                            return {
                                value: item.value,
                                id: item.id     // EDIT
                            }
                        }));

                    },

                });
            },
            select: function (event, ui) {
                var cityid = ui.item.id;

//                city_arr.push(cityid);
               console.log(cityid);
               // prev_arr.push(city_arr);
                $("input[name=branch_city_ids]").push(cityid);
//                 $("#branch_city_ids").val(prev_arr+','+cityid);
//                 console.log($("#branch_city_ids").val());
                $(this).val(ui.item.value);
            }
        });
    });
    src_county_name = "{{ url('customer/city/autocompletecountyname') }}";
    $(".branch_country").on("focus.autocomplete", null, function () {

        $(this).autocomplete({

            source: function (request, response) {
                $.ajax({
                    url: src_county_name,
                    dataType: "json",
                    data: {
                        term: request.term
                    },
                    beforeSend: function () {
                        $('.branchcountyloader').show();
                    },
                    complete: function () {
                        $('.branchcountyloader').hide();
                    },
                    success: function (data) {

                        response($.map(data, function (item) {
                            return {
                                value: item.value,
                                id: item.id     // EDIT
                            }
                        }));

                    },

                });
            },
            select: function (event, ui) {
                var countyid = ui.item.id;
                console.log(countyid);
               // county_arr.push(countyid);
              //  prev_county_arr.push(city_arr);
               $("input[name=branch_county_ids]").push(countyid);
              //   $("#branch_county_ids").val(prev_county_arr);
                $(this).val(ui.item.value);
            }
        });
    });

});


//  });
$('#submitPasswordForm').on('submit', function (e) {

    e.preventDefault();



    var submit_url = $(this).attr("action");
    $.ajax({
        type: 'POST',
        url: submit_url,
        dataType: "json",
        async: false,
        cache: false,
        data: $("#submitPasswordForm").serialize(),
        success: function (response) {

            if (response.success) {
                $('#successmessage').css('display', 'block');
                $('#successmessage').html(response.success);
            } else if (response.error) {
                $.each(response.error, function (key, value) {
                    $('#errormessage').css('display', 'block');
                    $('#errormessage').html(value);
                });

            }
        }
    });
});

$('#submitAccountSettingForm').on('submit', function (e) {

    e.preventDefault();
      
    var submit_url = $(this).attr("action");
    $.ajax({
        type: 'POST',
        url: submit_url,
        dataType: "json",
        async: false,
        cache: false,
        data: $("#submitAccountSettingForm").serialize(),
        success: function (response) {

            if (response.success) {
                $('#successModal').modal('show');
                $('#account_setting_message').html(response.success+'. Page will refresh automatically...');
                setTimeout(() => {
                    location.reload();
                }, 100);
            } else if (response.error) {
                $.each(response.error, function (key, value) {
                     $('#account_setting_message').html(value);
                });
            }  
        },error:function(result){
            $('#successModal').modal('hide');
        }
    });
});
$(document).ready(function () {
    src_city = "{{ url('customer/city/autocompletecityname') }}";
    remove_agent = "{{ url('admin/remove_agent') }}";
    remove_representative = "{{ url('admin/remove_representative') }}";
    //Mailing city value autocomplete
    $("#mailing_city_value").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: src_city,
                dataType: "json",
                data: {
                    term: request.term
                },
                beforeSend: function () {
                    $('#mailingcityloader').show();
                },
                complete: function () {
                    $('#mailingcityloader').hide();
                },
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            value: item.value,
                            id: item.id     // EDIT
                        }
                    }));

                },
            });
        },
        select: function (event, ui) {

            $("#mailing_city_value_id").val(ui.item.id);
            $("#mailing_city_value").val(ui.item.value);
        },
        change: function(event, ui) {
            if (ui.item == null) {
                $(this).val("");
                $(this).focusout();
                $(this).focus();
            }
        }
    });
    //Physical city value autocomplete
    $("#physical_city_value").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: src_city,
                dataType: "json",
                data: {
                    term: request.term
                },
                beforeSend: function () {
                    $('#cityloader').show();
                },
                complete: function () {
                    $('#cityloader').hide();
                },
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            value: item.value,
                            id: item.id     // EDIT
                        }
                    }));

                },
            });
        },
        select: function (event, ui) {

            $("#physical_city_value_id").val(ui.item.id);
            $("#physical_city_value").val(ui.item.value);
        },
        change: function(event, ui) {
            if (ui.item == null) {
                $(this).val("");
                $(this).focusout();
                $(this).focus();
            }
        }
    });
   
});
$(document).ready(function () {
    $('#skip_card_btn').click(function () {
        window.location = "<?php echo url('home') ?>";
    })
    $('#credit_card_name').keyup(function () {
        var card_holder_name = $(this).val();
        $('#card_holder_name_undertaking').html(card_holder_name);
    })
    $("#datepicker").datepicker({changeMonth: true,
        changeYear: true, dateFormat: 'mm/yy', minDate: 0, showButtonPanel: true, onClose: function (dateText, inst) {

            $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
            $('#datepicker').addClass('not-empty');
             $('#credit-card-form').parsley().validate();
        }});
    $('#card_number').mask("9999 9999 9999 9999 999");

    $('#card_type').change(function () {
        var sel = $(this).val();
        console.log(sel);
        switch (sel)
        {
            case 'American Express':
                $('#card_number').attr('length', '15');
                $('#card_number').mask("9999 9999 9999 9999 999");
                break;
            case 'Mastercard':
                $('#card_number').attr('minlength', '12');
                $('#card_number').attr('maxlength', '16');
                $('#card_number').mask("9999 9999 9999 9999");
                break;
            case 'Visa':
                $('#card_number').attr('minlength', '12');
                $('#card_number').attr('maxlength', '16');
                $('#card_number').mask("9999 9999 9999 9999");
                break;
            case 'Maestro':
                $('#card_number').attr('minlength', '12');
                $('#card_number').attr('maxlength', '19');
                $('#card_number').mask("9999 9999 9999 9999 999");
                break;
            case 'Discover Card':
                $('#card_number').attr('minlength', '16');
                $('#card_number').attr('maxlength', '19');
                $('#card_number').mask("9999 9999 9999 9999 9999 999");
                break;
            default:
                $('#card_number').attr('minlength', '12');
                $('#card_number').attr('maxlength', '16');
        }
    })
    $('#save_card').click(function () {
        var valid_form = $('#credit-card-form').parsley().validate();
        if (valid_form == true)
        {
            $('#credit-card-form').submit();
        }
    })

})


$(document).on('click', '.remove-card', function (e) {
    var id = this.id;
    $('#contact-confirmation-modal').modal('show');
    $('#confirm-delete').click(function() {
        $('#contact-confirmation-modal').modal('hide');

        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        $.ajax({
                    type: 'POST',
                    url: "{{url('customer/subscription/delete-card')}}",
                    dataType: "json",
                    data: {card_id : id},
                    success: function(data){
                        if(data.result=="success"){
                            $('.alert-success').text(data.message);
                            $(".div-"+id).remove();
                        }

                    }

            });
    });
    
});

$.fn.betta_pw_fld = function(pwfld_sel, hiddenfld_sel) {

    // this is the form the plugin is called on
    $(this).each(function() {

        // the plugin accepts the css selector of the pw field (pwfld_sel)
        var pwfld = $(this).find(pwfld_sel);

        // on keyup, do the masking visually while filling a field for actual use
        pwfld.off('keyup.betta_pw_fld');


        pwfld.on('keyup.betta_pw_fld', function() {
            var pchars = $(this).val().replace(/[^\d\*]/g, '');
            if (pchars == '') return;

            // we'll need the hidden characters too (for form submission)
            var hiddenfld = $(this).parents('form').find(hiddenfld_sel);
            var hchars = hiddenfld.val();

            var newval = '';
            var newhpw = '';

            for (i=0; i<pchars.length; i++) {
                if (pchars[i] == '*') {
                    newhpw += hchars[i];
                } else {
                    newhpw += pchars[i];
                }
                newval += '*';
            }

            // set the updated (masked), visual pw field
            $(this).val(newval);

            // keep the pw hidden and ready for form submission in a hidden input
            hiddenfld.val(newhpw);
        });
    });
}

$('#credit-card-form').betta_pw_fld('.stylish_security_code', '.hidden_security_code');
</script>
@stop

