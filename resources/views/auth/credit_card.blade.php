

@extends('layouts.page')
@section('banner')
<section class="sec-banner register-banner"></section>
@stop
@section('content')
<style>
    .ui-datepicker-calendar {
        display: none;
    }
</style>
<meta name="csrf-token" content="{{ csrf_token() }}">
<section class="register-sec">
    <div class="main-wrapper">

        @if(session()->has('error'))
        @if ($errors->count())
        <div class="no-margin alert alert-danger">
            <ul>
                @foreach ($errors->all() as $e)
                <li>{{ $e }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="register-form credit-form">
                    @if (Session::get('success'))
                    <div class="flash-message">
                        <div class="no-margin alert alert-success">
                          <?php echo Session::get('success'); ?>
                        </div>
                    </div>

                    @elseif(session('error'))
                    <div class="flash-message">
                        <div class="no-margin alert alert-danger">
                            {{Session::get('error')}}
                        </div>
                    </div>
                    @endif
                    <form id="credit-card-form" class="form-horizontal" data-parsley-validate="" action="{{ url('check-credit-card') }}" method="post">
                        {!! csrf_field() !!}
                        <div class="register-sub">
                            <div class="section-heading text-center">
                                <h2 class="section-title">Credit Card Details</h2>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-sm-4">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                            <input id="credit_card_name" type="text" name="credit_card_name" class="form-control" data-parsley-required="true" value="{{ old('credit_card_name') }}" data-parsley-required-message="Person Name is required" data-parsley-trigger="change focusout" data-parsley-pattern="/^[a-zA-Z ]*$/" maxlength="100">    
                                            <label>Person's name (As it appears on card)<span class="mandatory-field">*</span></label>
                                            <span></span>
                                            @if ($errors->has('credit_card_name'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('credit_card_name') }}</strong>
                                            </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-4">
                                    <div class="select-opt">
                                        <label class="col-md-12 control-label" for="textinput">Credit Card Type<span class="mandatory-field">*</span></label>
                                        <select name="card_type" class="selectpicker" data-show-subtext="true" data-live-search="true" data-parsley-required="true" data-parsley-required-message="Credit Card Type is required" data-parsley-trigger="change focusout" id='card_type'>
                                            <option value="">-Select-</option>
                                            @php ($card_types = ['Mastercard'=>'Mastercard','Visa'=>'Visa','American Express'=>'American Express','Cyrus'=>'Cyrus','Maestro'=>'Maestro'])
                                            @foreach($card_types as $key=>$val)
                                            <option value="{{$key}}" @if (old('card_type') == $key) selected="selected" @endif > {{$val}}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('card_type'))
                                        <p class="help-block">
                                            <strong>{{ $errors->first('card_type') }}</strong>
                                        </p>
                                        @endif
                                    </div>  
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                            <input type="text" id="datepicker" name="credit_card_date" class="form-control" data-parsley-required="true" value="{{ old('credit_card_date') }}" data-parsley-required-message="Expiration Date is required" data-parsley-trigger="change focusout">   <label>Expiration Date<span class="mandatory-field">*</span></label>
                                            <span></span>
                                            @if ($errors->has('credit_card_date'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('credit_card_date') }}</strong>
                                            </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-sm-4">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                            <input id="card_number" pattern="[0-9\s]*" type="text" name="card_number" class="form-control" data-parsley-required="true" value="{{ old('card_number') }}" data-parsley-required-message="Card Number is required" data-parsley-trigger="change focusout">       
                                            <label>Card Number<span class="mandatory-field">*</span></label>
                                            <span></span>
                                            @if ($errors->has('card_number'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('card_number') }}</strong>
                                            </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                            <input type="text" name="security_code" class="form-control stylish_security_code" data-parsley-required="true" value="{{ old('security_code') }}" data-parsley-required-message="Security Code is required" data-parsley-minlength="3" data-parsley-maxlength="3" data-parsley-minlength-message="Security code must be 3 digits" data-parsley-maxlength-message="Security code must be 3 digits" data-parsley-trigger="change focusout">    
                                            <input type="hidden" name="security_code" class="hidden_security_code" value="{{ old('security_code') }}">
                                            <label>Security Code<span class="mandatory-field">*</span></label>
                                            <span></span>
                                            @if ($errors->has('security_code'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('security_code') }}</strong>
                                            </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                            <input type="text" name="credit_zip_code" class="form-control" data-parsley-required="true" value="{{ old('credit_zip_code') }}" data-parsley-required-message="Zip Code of Credit Card is required" data-parsley-trigger="change focusout" data-parsley-type="digits">    
                                            <label>Zip Code of Credit Card<span class="mandatory-field">*</span></label>
                                            <span></span>
                                            @if ($errors->has('credit_zip_code'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('credit_zip_code') }}</strong>
                                            </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="input-wrapper full-width">
                                        <input type="checkbox" id="terms_checkbox" class="regular-checkbox" data-parsley-required-message="Accept terms and conditions" required data-parsley-trigger="change focusout" name="terms">

                                        <label for="terms_checkbox_label" class="agree-label">I agree to the AAA Business Associates authorization to charge on my credit card.</label>
                                        @if ($errors->has('terms'))
                                        <p class="help-block">
                                            <strong>{{ $errors->first('terms') }}</strong>
                                        </p>
                                        @endif
                                    </div>
                                </div>
                            
                                <div class="col-md-12 col-sm-12">
                                    <div class="panel-group">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4>
                                                    <a data-toggle="collapse" href="#collapse1">Terms and Conditions</a>
                                                </h4>
                                            </div>
                                            <div id="collapse1" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <p><span id="card_holder_name_undertaking">@if(isset($customer_name))
                                                            {{ $customer_name }}
                                                            @else (Card Holder Name)
                                                            @endif</span> hereby represent that once this authorization form is signed, I agree to pay the full amount charged and there will be no cancellation or refund.</p>
                                                    <p>If and only if, AAA Business Assoc. DBA AAA Notice To Owner do not receive full statement payment, we AAA Notice to Owner have the right to charge your Credit or Debit Card.</p>
                                                    <p>I understand and consent to the use of myCredit or Debit Card without my signature on the charge that a photocopy of this agreement will serve as an original, and this Credit or Debit Card authorization cannot be revoked</p>
                                                    <p>Once the transaction has been completed, you will receive an email or text message with a copy of your receipt.</p>
                                                    <p>**** NOTE: AAA will be using Quickbooks to complete payments. How will you fulfill remaining balances for partial payments made for statements? ****</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="col-md-12 col-sm-12">
                                <button type="button" class="btn btn-default custom-btn customc-btn" id="skip_card_btn"><span>Skip</span></button>
                                <button type="button" class="btn btn-primary custom-btn customc-btn" id="submit_register_button"><span>Submit & Register</span></button>
                                </div> -->
                                <div class="col-md-12 col-sm-12">
                                    <div class="form-bottom-btn">
                                        <button type="button" class="btn btn-default custom-btn customc-btn" id="skip_card_btn"><span>Skip</span></button>
                                        <button type="button" class="btn btn-primary custom-btn customc-btn" id="submit_register_button"><span>Submit & Register</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Modal -->
<div id="terms_alert" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Accept Terms</h4>
            </div>
            <div class="modal-body">
                <p>Please accept the terms and conditions to proceed further.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
@stop
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@section('frontend_js')
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script type="text/javascript">
$('#datepicker').datepicker().on('changeDate', function(ev){                 
     $('#datepicker').datepicker('hide');
});
$(document).ready(function () {
    $('input,textarea,select').on('focus', function(e) {
        e.preventDefault();
        $(this).attr("autocomplete", "nope");
        $('#credit_card_name').attr("autocomplete", "off"); 
    });
    $('#skip_card_btn').click(function () {
        window.location = "<?php echo url('home') ?>";
    })
    $('#credit_card_name').keyup(function () {
        var card_holder_name = $(this).val();
        $('#card_holder_name_undertaking').html(card_holder_name);
    })
    $("#datepicker").datepicker({changeMonth: true,
        changeYear: true, dateFormat: 'mm/yy', minDate: 0, showButtonPanel: true, onClose: function (dateText, inst) {
            
            $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
            $('#datepicker').addClass('not-empty');

        }});
    $('#card_number').mask("9999 9999 9999 9999 999");

    $('#card_type').change(function () {
        var sel = $(this).val();
        console.log(sel);
        switch (sel)
        {
            case 'American Express':
                $('#card_number').attr('minlength', '15');
                $('#card_number').attr('maxlength', '15');
                $('#card_number').mask("9999 9999 9999 9999 999");
                break;
            case 'Mastercard':
                $('#card_number').attr('minlength', '12');
                $('#card_number').attr('maxlength', '16');
                $('#card_number').mask("9999 9999 9999 9999");
                break;
            case 'Visa':
                $('#card_number').attr('minlength', '12');
                $('#card_number').attr('maxlength', '16');
                $('#card_number').mask("9999 9999 9999 9999");
                break;
            case 'Maestro':
                $('#card_number').attr('minlength', '12');
                $('#card_number').attr('maxlength', '19');
                $('#card_number').mask("9999 9999 9999 9999 999");
                break;
            case 'Discover Card':
                $('#card_number').attr('minlength', '16');
                $('#card_number').attr('maxlength', '19');
                $('#card_number').mask("9999 9999 9999 9999 9999 999");
                break;
            default:
                $('#card_number').attr('minlength', '12');
                $('#card_number').attr('maxlength', '16');
        }
    })
    $('#submit_register_button').click(function () {
        var valid_form = $('form').parsley().validate();
        if (valid_form == true)
        {
            $('#credit-card-form').submit();
        }
    })

})

$.fn.betta_pw_fld = function(pwfld_sel, hiddenfld_sel) {

    // this is the form the plugin is called on
    $(this).each(function() {

        // the plugin accepts the css selector of the pw field (pwfld_sel)
        var pwfld = $(this).find(pwfld_sel);

        // on keyup, do the masking visually while filling a field for actual use
        pwfld.off('keyup.betta_pw_fld');


        pwfld.on('keyup.betta_pw_fld', function() {
            var pchars = $(this).val().replace(/[^\d\*]/g, '');
            if (pchars == '') return;

            // we'll need the hidden characters too (for form submission)
            var hiddenfld = $(this).parents('form').find(hiddenfld_sel);
            var hchars = hiddenfld.val();

            var newval = '';
            var newhpw = '';

            for (i=0; i<pchars.length; i++) {
                if (pchars[i] == '*') {
                    newhpw += hchars[i];
                } else {
                    newhpw += pchars[i];
                }
                newval += '*';
            }

            // set the updated (masked), visual pw field
            $(this).val(newval);

            // keep the pw hidden and ready for form submission in a hidden input
            hiddenfld.val(newhpw);
        });
    });
}

$('#credit-card-form').betta_pw_fld('.stylish_security_code', '.hidden_security_code');
</script>
@stop

