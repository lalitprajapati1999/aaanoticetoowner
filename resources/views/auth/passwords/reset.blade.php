@extends('layouts.page')

@section('content')

<section class="login-sec sec-banner password-sec">
        <div class="main-wrapper">
            <div class="row">
                <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
                    <div class="password-panel full-width">
                        <div class="password-container">
                            <div class="password-header full-width">
                                <h2 class="full-width">Reset Password</h2>
                            </div>
                            
                            <form class="form-horizontal full-width" action="{{ url(config('adminlte.password_reset_url', 'password/reset')) }}" method="post">
                                {!! csrf_field() !!}
                                <div class="full-width">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="input-wrapper full-width">
                                                   <div class="styled-input">
                                                        <input type="email" name="email" class="form-control" value="{{ $email or old('email') }}"
                                                        >
                                                        <label>Email</label>
                                                  <span></span>
                                                 <i class="glyphicon glyphicon-envelope form-control-feedback"></i>
                                                 @if ($errors->has('email'))
                                                     <i class="help-block">
                                                         <strong>{{ $errors->first('email') }}</strong>
                                                     </i>
                                                 @endif
                                               </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="full-width">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <div class="input-wrapper full-width">
                                                       <div class="styled-input">
                                                            <input type="password" name="password" class="form-control">  <label>Email</label>
                                                      <span></span>
                    <i class="glyphicon glyphicon-lock form-control-feedback"></i>
                    @if ($errors->has('password'))
                        <i class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </i>
                    @endif
                                                          
                                                 
                                                   </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <div class="full-width">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="input-wrapper full-width">
                                                   <div class="styled-input">
                                                        <input type="password" name="password_confirmation" class="form-control"
                                                        >
                                                  <label>Reenter Password</label>
                                                  <span></span>
                                                 <i class="glyphicon glyphicon-log-in form-control-feedback"></i>
                                                 @if ($errors->has('password_confirmation'))
                                                     <i class="help-block">
                                                         <strong>{{ $errors->first('password_confirmation') }}</strong>
                                                     </i>
                                                 @endif
                                                  
                                               </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="full-width">
                                    <div class="col-md-6">
                                        <div class="form-bottom-btn full-width">
                                            <a href="#" type="button" class="btn btn-primary custom-btn customb-btn"><span>Back</span></a>
                                            <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>Reset Password</span></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop