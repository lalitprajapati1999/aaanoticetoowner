@extends('layouts.page')
@section('content')
<!--------------------login starts-------------------------->

<section class="login-sec sec-banner password-sec">
	<div class="main-wrapper">
		<div class="row">
			<div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
                    @if (Session::get('succcess'))
                    <div class="flash-message">
                        <div class="alert alert-success">
                            <?php echo Session::get('success'); ?>
                        </div>
                    </div>

                    @elseif(session('error'))
                    <div class="flash-message">
                        <div class="alert alert-danger">
                            {{Session::get('error')}}
                        </div>
                    </div>
                    @endif

                    @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    @endif

				<div class="password-panel full-width">
					<div class="password-container">
						<div class="password-header full-width">
							<h2 class="full-width">Reset Password</h2>
							<div class="full-width">
								<div class="form-group">
									<a href="#" class="password-link forgot-link">Enter Your email to reset password</a>
								</div>
							</div>
						</div>
                        <form class="form-horizontal full-width" action="{{ url(config('adminlte.password_email_url', 'password/email')) }}" method="post">
                            {!! csrf_field() !!}
							<div class="full-width">
								<div class="col-md-6">
									<div class="form-group">
										<div class="input-wrapper full-width">
									  	 	<div class="styled-input">
                                                    <input type="email" name="email" class="form-control" value="{{ $email or old('email') }}" >
                                                    <label>Email ID<span class="mandatory-field">*</span></label>
										        <span></span>
                                                <i class="glyphicon glyphicon-envelope form-control-feedback"></i>
                                                @if ($errors->has('email'))
                                                    <p class="help-block">
                                                        <strong>{{ $errors->first('email') }}</strong>
													</p>
                                                @endif
										      
										   </div>
										</div>
									</div>
								</div>
							</div>
							<div class="full-width">
								<div class="col-md-6">
									<div class="form-bottom-btn full-width">
										<a href="{{url('/login')}}" type="button" class="btn btn-primary custom-btn customb-btn"><span>Back</span></a>
										<button type="submit" class="btn btn-primary custom-btn customc-btn"><span>Submit</span></button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop