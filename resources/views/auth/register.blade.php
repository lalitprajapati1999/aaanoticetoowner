<style>
  .bootstrap-select :focus{
    outline: 0;
    border-bottom : 1px solid #032c61 !important;
 }
  
 
 .select2class :focus{
    outline: 0;
    height: 47px !important;
    border-bottom : 1px solid #032c61 !important;
}
</style>
@extends('layouts.page')


@section('banner')

<section class="sec-banner register-banner"></section>
@stop
@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<section class="register-sec">
    <div class="main-wrapper">

        @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
        @endif
        @if(session()->has('error'))
        @if ($errors->count())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $e)
                <li>{{ $e }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="register-form">
                    <!--data-parsley-validate=""--> 
                    <form id="join-now-form" class="form-horizontal" data-parsley-validate="" action="{{ url(config('adminlte.register_url', 'register')) }}" method="post">
                        {!! csrf_field() !!}
                        <div class="register-sub">
                            <div class="section-heading text-center">
                                <h2 class="section-title">AAA Notice to Owner offers </h2>
                                <p class="section-text">Service that gets your construction invoices paid !</p>
                                <p class="section-text"></p>
                            </div>
    <!--                        <p class="login-box-msg">{{ trans('adminlte::adminlte.register_message') }}</p>  -->
                            <!--                        <form class="form-horizontal" action="{{ url(config('adminlte.register_url', 'register')) }}" method="post">
                                                        {!! csrf_field() !!}-->
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                            <input type="text" name="company_name" class="form-control" data-parsley-required="true" value="{{ old('company_name') }}" data-parsley-required-message="Company Name is required" data-parsley-trigger="change focusout" data-parsley-maxlength="200">    
                                            <label>{{ trans('adminlte::adminlte.company_name') }}<span class="mandatory-field">*</span></label>
                                            <span></span>
                                            @if ($errors->has('company_name'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('company_name') }}</strong>
                                            </p>
                                            @endif
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                            <input type="text" name="contact_person" class="form-control" data-parsley-required="true" value="{{ old('contact_person')}}"  data-parsley-required-message="Contact Person is required" data-parsley-trigger="change focusout" data-parsley-maxlength="200">
                                            <label>{{ trans('adminlte::adminlte.contact_person') }}<span class="mandatory-field">*</span></label>
                                            <span></span>

                                            @if ($errors->has('contact_person'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('contact_person') }}</strong>
                                            </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="register-sub">
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="sub-section-title">
                                        <h2>Mailing Address</h2>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                            <input type="text" class="form-control"  data-parsley-required="true" onchange="$('#same_as_mailing').change()" name="mailing_address" id="mailing_address_id" value="{{ old('mailing_address')}}"  data-parsley-required-message="Mailing Address is required" data-parsley-trigger="change focusout" data-parsley-maxlength="500"/>
                                            <label>{{ trans('adminlte::adminlte.mailing_address') }}<span class="mandatory-field">*</span></label>
                                            <span></span>
                                            @if ($errors->has('mailing_address'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('mailing_address') }}</strong>
                                            </p>
                                            @endif
                                        </div>

                                    </div>
                                </div>

                               <!--  <div class="col-md-4 col-sm-4">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                            <input class="form-control"  type="text" name="city" onchange="$('#same_as_mailing').change()"  value="{{old('city')}}" id="city_value" data-parsley-required="true" data-parsley-required-message="Select City from the list" data-parsley-trigger="change focusout">
                                            <label>City<span class="mandatory-field">*</span></label> 
                                            <span></span>

                                        </div>
                                        <div id="mailingcityloader" style="display: none">
                                            <img width="150px" src="{{asset('images/loader.gif')}}"/>
                                        </div>
                                        <input type="hidden" name="mailing_city_value_id" id="mailing_city_value_id" />

                                    </div>
                                </div> -->
                                <div class="col-md-4 col-sm-4">
                                        <div class="select-opt select2-container select2class col-md-12">
                                            <label class="col-md-12 control-label" for="textinput">City<span class="mandatory-field">*</span></label>
                                            <select class="js-example-tags form-control load_ajax_cities"  name="mailing_city_value_id" onchange="$('#same_as_mailing').change()"  value="{{old('mailing_city_value_id')}}" id="mailing_city_value_id" data-parsley-required="true" data-parsley-required-message="City is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true">
                                                <option value="">Select</option>  
                                            </select>
                                        </div> 
                                       
                                </div>

                                <div class="col-md-4 col-sm-4">
                                    <div class="select-opt ">
                                        <label class="col-md-12 control-label" for="textinput">State<span class="mandatory-field">*</span></label>
                                        <select name="state" class="selectpicker"  onchange="$('#same_as_mailing').change()"  data-show-subtext="true" data-live-search="true" data-parsley-required="true" data-parsley-required-message="State is required" data-parsley-trigger="change focusout" id='state_value'>
                                            <option value="">Select</option>
                                            @foreach($states as $state)
                                            <option value="{{$state->id}}" @if (old('state') == $state->id) selected="selected" @endif > {{ucfirst(trans($state->name))}}</option>
                                            @endforeach 
                                        </select>
                                        @if ($errors->has('state'))
                                        <p class="help-block">
                                            <strong>{{ $errors->first('state') }}</strong>
                                        </p>
                                        @endif
                                    </div>
                                </div>

                                <!--   <div class="col-md-4 col-sm-4">
                                      <div class="select-opt">
                                          <label class="col-md-12 control-label" for="textinput">City</label>  
                                          <select required name="mailing_city" class="selectpicker" data-parsley-required-message="City is required" data-parsley-trigger="change focusout">
                                              @foreach($cities as $mailing_city)
                                              <option value="{{$mailing_city->id}}" @if (old('mailing_city') == $mailing_city->id) selected="selected" @endif > {{ucfirst(trans($mailing_city->name))}}</option>
                                              @endforeach
                                          </select>
                                         
                                      </div>
                                  </div>
                                  
                                  <div class="col-md-4 col-sm-4">
                                      <div class="select-opt">
                                          <label class="col-md-12 control-label" for="textinput">State</label>  
                                          <select required name="mailing_state"  class="selectpicker" data-parsley-required-message="State is required" data-parsley-trigger="change focusout">
                                              @foreach($states as $mailing_state)
                                              <option value="{{$mailing_state->id}}" @if (old('mailing_state') == $mailing_state->id) selected="selected" @endif > {{ucfirst(trans($mailing_state->name))}}</option>
                                              @endforeach
                                          </select>
                                          
                                      </div>
                                  </div> -->
                                <div class="col-md-4 col-sm-4">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                            <input type="text" class="form-control" name="mailing_zip" onchange="$('#same_as_mailing').change()"  id="mailing_zip_id" value="{{ old('mailing_zip')}}" placeholder="" data-parsley-required="true" data-parsley-required-message="Zip code is required" data-parsley-trigger="change focusout" data-parsley-type="digits" data-parsley-minlength="5"  data-parsley-maxlength="5"/>
                                            <label>{{ trans('adminlte::adminlte.zip') }}<span class="mandatory-field">*</span></label>
                                            <span></span>

                                            @if ($errors->has('mailing_zip'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('mailing_zip') }}</strong>
                                            </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="register-sub">
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="sub-section-title">
                                        <h2>Physical Address</h2>
                                    </div>
                                </div>
<!--                                <label><input type="checkbox" id="same_as_mailing" name="sameasaboveadress"/>Same as above</label>-->
                                <div class="checkbox-holder same-above">
                                    <input type="checkbox" id="same_as_mailing" class="regular-checkbox"><label for="same_as_mailing">Same As Above</label>
                                </div>
                                <div class="col-md-12 col-sm-12">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                            <input type="text" name="company_physical_address" id="company_physical_address_id" class="form-control" data-parsley-required="true" value="{{ old('company_physical_address')}}" data-parsley-required-message="Physical address is required" data-parsley-trigger="change focusout" data-parsley-maxlength="500"/>
                                            <label>{{ trans('adminlte::adminlte.company_physical_address') }}<span class="mandatory-field">*</span></label>
                                            <span></span>
                                            @if ($errors->has('company_physical_address'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('company_physical_address') }}</strong>
                                            </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>


                                <!-- <div class="col-md-4 col-sm-4">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                            <input class="form-control"  type="text" name="physical_city" value="{{old('physical_city')}}" id="physical_city_value" data-parsley-required="true" data-parsley-required-message="Select City from the list" data-parsley-trigger="change focusout">
                                            <label>City<span class="mandatory-field">*</span></label> 
                                            <span></span>

                                        </div>
                                        <div id="cityloader" style="display: none">
                                            <img width="150px" src="{{asset('images/loader.gif')}}"/>
                                        </div>
                                        <input type="hidden" name="physical_city_value_id" id="physical_city_value_id" />
                                    </div>
                                </div> -->
                                <div class="col-md-4 col-sm-4">
                                        <div class="select-opt select2-container select2class col-md-12">
                                            <label class="col-md-12 control-label" for="textinput">City<span class="mandatory-field">*</span></label>
                                            <select class="js-example-tags form-control load_ajax_cities"  name="physical_city_value_id"  value="{{old('physical_city_value_id')}}" id="physical_city_value_id" data-parsley-required="true" data-parsley-required-message="City is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true">
                                                <option value=" ">Select</option>  
                                            </select>
                                        </div> 
                                       
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="select-opt">
                                        <label class="col-md-12 control-label" for="textinput">State<span class="mandatory-field">*</span></label>  
                                        <select  name="physical_state"  class="selectpicker" data-parsley-required="true" data-parsley-required-message="State is required" data-parsley-trigger="change focusout" id='physical_state_value' data-show-subtext="true" data-live-search="true">
                                            <option value="">Select</option>
                                            @foreach($states as $physical_state)
                                            <option value="{{$physical_state->id}}" @if (old('physical_state') == $physical_state->id) selected="selected" @endif > {{ucfirst(trans($physical_state->name))}}</option>
                                            @endforeach 
                                        </select>

                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-4">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                            <input type="text" name="physical_zip" id="physical_zip_id" value="{{ old('physical_zip')}}" class="form-control" data-parsley-required="true" data-parsley-required-message="Zip Code is required" data-parsley-trigger="change focusout" data-parsley-type="digits"  data-parsley-minlength="5"  data-parsley-maxlength="5"/>
                                            <label>{{ trans('adminlte::adminlte.zip') }}<span class="mandatory-field">*</span></label>
                                            <span></span>

                                            @if ($errors->has('physical_zip'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('physical_zip') }}</strong>
                                            </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                            <input type="text" name="hear_about" value="{{ old('hear_about')}}" class="form-control" />
                                            <label>{{ trans('adminlte::adminlte.hear_about') }}</label>
                                            <span></span>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                            <!---->
                                            <input type="text"  name="office_number" class="form-control" value="{{ old('office_number')}}"  id="office_number" data-parsley-required="true"  data-parsley-required-message="Office number is required" data-parsley-trigger="change focusout" data-parsley-pattern="(\(\d{3})\)\s\d{3}\-\d{4}$"/>
                                            <label>{{ trans('adminlte::adminlte.office_number') }}<span class="mandatory-field">*</span></label>
                                            <span></span>


                                            @if ($errors->has('office_number'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('office_number') }}</strong>
                                            </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                            <input type="text" name="fax" class="form-control"  value="{{ old('fax')}}" data-parsley-trigger="change focusout"  id="fax_number" data-parsley-pattern="(\(\d{3})\)\s\d{3}\-\d{4}$"/>
                                            <label>{{ trans('adminlte::adminlte.fax') }}</label>
                                            <span></span>

                                            @if ($errors->has('fax'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('fax') }}</strong>
                                            </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-6 col-sm-6">
                                    <div id="html-div" class="input-wrapper full-width additional_email">
                                        <div class="styled-input">
                                            <input type="email" id="email" name="company_email[]" class="form-control"  value="{{old('company_email.0')}}" data-parsley-required="true" data-parsley-required-message="Email is required" data-parsley-trigger="change focusout" data-parsley-type="email" />
                                            <label>{{ trans('adminlte::adminlte.email') }}<span class="mandatory-field">*</span></label>
                                            <span></span>

                                            @if ($errors->has('company_email.0'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('company_email.0') }}</strong>
                                            </p>
                                            @endif
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12">
                                    <div class="form-btn-div">
                                        <a class="btn btn-primary custom-btn form-btn screens-duplicate-button" ><span>+</span> Additional Email Address</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="register-sub">
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="sub-section-title">
                                        <h2>Login Details</h2>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                            <input type="text" name="name" class="form-control" value="{{ old('name') }}" data-parsley-required="true" data-parsley-required-message="Full Name is required" data-parsley-trigger="change focusout" data-parsley-maxlength="100" >
                                            <label>{{ trans('adminlte::adminlte.full_name') }}<span class="mandatory-field">*</span></label>
                                            <span></span>

                                            @if ($errors->has('name'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                            <input type="email" name="email" class="form-control"  value="{{ old('email') }}" data-parsley-required="true" data-parsley-required-message="Email is required" data-parsley-trigger="change focusout" data-parsley-type="email" >
                                            <label>{{ trans('adminlte::adminlte.email') }}<span class="mandatory-field">*</span></label>
                                            <span></span>

                                            @if ($errors->has('email'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                            <input type="password" name="password" class="form-control" id="inputPassword" data-parsley-required="true" data-parsley-required-message="Password is required" data-parsley-trigger="change focusout" data-parsley-minlength="6" data-parsley-maxlength="20">
                                            <label>{{ trans('adminlte::adminlte.password') }}<span class="mandatory-field">*</span></label>
                                            <span></span>

                                            @if ($errors->has('password'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 col-sm-6">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                            <input type="password" name="password_confirmation" class="form-control" data-parsley-required="true" data-parsley-required-message="Password confirmation is required" data-parsley-trigger="change focusout" data-parsley-equalto="#inputPassword" data-parsley-equalto-message='Retype password must be same as password'>
                                            <label>{{ trans('adminlte::adminlte.retype_password') }}<span class="mandatory-field">*</span></label>
                                            <span></span>

                                            @if ($errors->has('password_confirmation'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                                            </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="register-sub">
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="sub-section-title">
                                        <h2>Officer/Director</h2>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="reg_agents">
                                <div>
                                    <div class="col-md-4 col-sm-4">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="agent_first_name[]"   value="{{old('agent_first_name.0')}}" class="form-control" data-parsley-required="true" data-parsley-required-message="First Name is required" data-parsley-trigger="change focusout"  data-parsley-maxlength="100"/>
                                                <label>{{ trans('adminlte::adminlte.agent_first_name') }}<span class="mandatory-field">*</span></label>
                                                <span></span>

                                                @if ($errors->has('agent_first_name'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('agent_first_name') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="agent_last_name[]"  value="{{old('agent_last_name.0')}}"  class="form-control" data-parsley-required="true" data-parsley-required-message="Last Name is required" data-parsley-trigger="change focusout"  data-parsley-maxlength="100" />
                                                <label>{{ trans('adminlte::adminlte.agent_last_name') }}<span class="mandatory-field">*</span></label>
                                                <span></span>

                                                @if ($errors->has('agent_last_name'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('agent_last_name') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="agent_title[]" value="{{old('agent_title.0')}}" class="form-control" data-parsley-required="true" data-parsley-required-message="Title is required" data-parsley-trigger="change focusout"  data-parsley-maxlength="100"/>
                                                <label>{{ trans('adminlte::adminlte.agent_title') }}<span class="mandatory-field">*</span></label>
                                                <span></span>

                                                @if ($errors->has('agent_title'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('agent_title') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="form-btn-div">
                                        <a class="btn btn-primary custom-btn form-btn agent-duplicate-button"><span>+</span> Add More Agents</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="register-sub">
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="sub-section-title">
                                        <h2>Representative</h2>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="representative">
                                <div class="full-width">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="repres_contact_person[]"  class="form-control" value="{{old('repres_contact_person.0')}}" data-parsley-required-message="Contact person is required" data-parsley-trigger="change focusout"  data-parsley-maxlength="100"/>
                                                <label>{{ trans('adminlte::adminlte.contact_person') }}</label>
                                                <span></span>

                                                @if ($errors->has('repres_contact_person'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('repres_contact_person') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="repres_company_branch_name[]" class="form-control"  value="{{old('repres_company_branch_name.0')}}" data-parsley-required-message="Position is required" data-parsley-trigger="change focusout"  data-parsley-maxlength="200"/>
                                                <label>{{ trans('adminlte::adminlte.repres_company_branch_name') }}</label>
                                                <span></span>

                                                @if ($errors->has('repres_company_branch_name'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('repres_company_branch_name') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="email" name="repres_email[]" class="form-control"  value="{{old('repres_email.0')}}" data-parsley-required-message="Email is required" data-parsley-trigger="change focusout" data-parsley-type="email"/>
                                                <label>{{ trans('adminlte::adminlte.repres_email') }}</label>
                                                <span></span>

                                                @if ($errors->has('repres_email'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('repres_email') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="repres_mobile_number[]" class="form-control mobile_no_masking"  value="{{ old('repres_mobile_number.0')}}" data-parsley-trigger="change focusout" data-parsley-required-message="Mobile No is required"  data-parsley-pattern="(\(\d{3})\)\s\d{3}\-\d{4}$">
                                                <label>{{ trans('adminlte::adminlte.mobile_number') }}</label>
                                                <span></span>

                                                @if ($errors->has('repres_mobile_number'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('repres_mobile_number') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="form-btn-div">
                                        <a class="btn btn-primary custom-btn form-btn representative-duplicate-button"><span>+</span> Add Another Representative</a>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input {{ $errors->has('no_of_offices') ? 'has-error' : '' }}">
                                            <input type="text" name="no_of_offices" class="form-control" value="{{ old('no_of_offices') }}"
                                                   id="number_offices" data-parsley-required-message="No of offices is required" data-parsley-trigger="change focusout" data-parsley-type="digits">    
                                            <label>How many branches throughout the united states?</label>
                                            <span></span>

                                            @if ($errors->has('no_of_offices'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('no_of_offices') }}</strong>
                                            </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="showBranchDiv">
                                <div class="subdiv">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input {{ $errors->has('branch_name') ? 'has-error' : '' }}">
                                                    <input type="text" name="branch_name[]" value="{{old('branch_name.0')}}" class="form-control branch_name"/>
                                                    <label>{{ trans('adminlte::adminlte.branch_name') }}</label>
                                                    <span></span>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text" name="branch_phone[]"  value="{{old('branch_phone.0')}}"  class="form-control branch_phone" data-parsley-trigger="change focusout" data-parsley-trigger="change focusout"/>
                                                    <label>{{ trans('adminlte::adminlte.branch_phone') }}</label>
                                                    <span></span>
                                                </div>
                                                @if ($errors->has('branch_phone'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('branch_phone') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text" name="branch_contact_person[]" value="{{old('branch_contact_person.0')}}" class="form-control branch_contact_person" />
                                                    <label>{{ trans('adminlte::adminlte.contact_person') }}</label>
                                                    <span></span>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8 col-sm-8">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text" name="branch_address[]"  value="{{old('branch_address.0')}}"  class="form-control branch_address" />
                                                    <label>{{ trans('adminlte::adminlte.branch_address') }}</label>
                                                    <span></span>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="col-md-4 col-sm-4">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input class="form-control branch_city_val"  type="text" name="branch_city[]" >
                                                    <label>City</label> 
                                                    <span></span>
                                                </div>
                                                <div class="branchcityloader" style="display: none">
                                                    <img width="150px" src="{{asset('images/loader.gif')}}"/>
                                                </div>
                                                <input type="hidden" name="branch_city_ids[]"  />
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">

                                        <div class="col-md-4 col-sm-4">
                                            <div class="select-opt">
                                                <label class="col-md-12 control-label" for="textinput">State</label>  
                                                <select name="branch_state[]"  data-show-subtext="true" data-live-search="true" class='branch_state_value'>
                                                    <option value="">Select</option>
                                                    @foreach($states as $branch_state)
                                                    <option value="{{$branch_state->id}}"> {{ucfirst(trans($branch_state->name))}}</option>
                                                    @endforeach 
                                                </select>

                                            </div>
                                        </div>

                                        <div class="col-md-4 col-sm-4">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text" class="form-control branch_zip" name="branch_zip[]" value="{{old('branch_zip.0')}}" placeholder="" data-parsley-trigger="change focusout" data-parsley-type="digits"  data-parsley-minlength="5"  data-parsley-maxlength="5"/>
                                                    <label>{{ trans('adminlte::adminlte.zip') }}</label>
                                                    <span></span>
                                                </div>
                                                @if ($errors->has('branch_zip'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('branch_zip') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-4 col-sm-4">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input class="form-control branch_country"  type="text" name="branch_country[]">
                                                    <label>County</label> 
                                                    <span></span>

                                                </div>
                                                <div class="branchcountyloader" style="display: none">
                                                    <img width="150px" src="{{asset('images/loader.gif')}}"/>
                                                </div>
                                                <input type="hidden" name="branch_county_ids[]"  class='branch_county_ids'/>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="sub-section-title">
                                                <h4>Registered Agents</h4>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text" name="branch_first_name[]" value="{{old('branch_first_name.0')}}" class="form-control branch_first_name" />
                                                    <label>{{ trans('adminlte::adminlte.agent_first_name') }}</label>
                                                    <span></span>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text" name="branch_last_name[]"  value="{{old('branch_last_name.0')}}"  class="form-control branch_last_name"  />
                                                    <label>{{ trans('adminlte::adminlte.agent_last_name') }}</label>
                                                    <span></span>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text" name="branch_title[]" value="{{old('branch_title.0')}}" class="form-control branch_title" />
                                                    <label>{{ trans('adminlte::adminlte.agent_title') }}</label>
                                                    <span></span>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="form-bottom-btn">
                                        <a href="{{ URL::previous() }}" class="btn btn-primary custom-btn customb-btn"><span>Back</span></a>
    <!--                                    <a href="#" class="btn btn-primary custom-btn customc-btn"><span>Continue</span></a>-->
                                        <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>Continue</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <p class="note">Note - All the details that entered during registration can be edited clicking on <a href="#">"Edit Profile"</a> on <a href="#">"Account Setting"</a> screen.</p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</section>

@stop
@section('frontend_js')
<script type="text/javascript" src="{{ asset('js/jquery-ui.min.js') }}"></script>

<script type="text/javascript">
    
     var cityUrl = "{{ url('customer/city-list') }}";
                                              $(document).ready(function () {
                                                     $(".load_ajax_cities").select2({
                                                            tags:true,
                                                            ajax: { 
                                                            url: cityUrl,
                                                            type: "get",
                                                            dataType: 'json',
                                                           //delay: 250,
                                                           data: function (params) {
                                                            return {
                                                              name: params.term // search term
                                                            };
                                                           },
                                                           processResults: function (response) {
                                                             return {
                                                                results: response
                                                             };
                                                           },
                                                           cache: true
                                                          }
                                                         }); 
                                                      
                                                    $('input,textarea,select').on('focus', function(e) {
                                                                e.preventDefault();
                                                                $(this).attr("autocomplete", "nope");  
                                                        });
                                                    src_city = "{{ url('customer/city/autocompletecityname') }}";

                                                    //Mailing city value autocomplete
                                                /*    $("#city_value").autocomplete({
                                                        source: function (request, response) {
                                                            $.ajax({
                                                                url: src_city,
                                                                dataType: "json",
                                                                data: {
                                                                    term: request.term
                                                                },
                                                                beforeSend: function () {
                                                                    $('#mailingcityloader').show();
                                                                },
                                                                complete: function () {
                                                                    $('#mailingcityloader').hide();
                                                                },
                                                                success: function (data) {

                                                                    response($.map(data, function (item) {
                                                                        return {
                                                                            value: item.value,
                                                                            id: item.id     // EDIT
                                                                        }
                                                                    }));

                                                                },

                                                            });
                                                        },
                                                        select: function (event, ui) {

                                                            $("#mailing_city_value_id").val(ui.item.id);
                                                            $("#city_value").val(ui.item.value);
                                                        },
                                                        change: function (event, ui) {
                                                            if (ui.item == null) {
                                                                $(this).val("");
                                                                $(this).focusout();
                                                                $(this).focus();
                                                            }
                                                        }
                                                    });
*/
                                                    //Physical city value autocomplete
                                                   /* $("#physical_city_value").autocomplete({

                                                        source: function (request, response) {
                                                            $.ajax({
                                                                url: src_city,
                                                                dataType: "json",
                                                                data: {
                                                                    term: request.term
                                                                },
                                                                beforeSend: function () {
                                                                    $('#cityloader').show();
                                                                },
                                                                complete: function () {
                                                                    $('#cityloader').hide();
                                                                },
                                                                success: function (data) {

                                                                    response($.map(data, function (item) {
                                                                        return {
                                                                            value: item.value,
                                                                            id: item.id     // EDIT
                                                                        }
                                                                    }));

                                                                },

                                                            });
                                                        },
                                                        select: function (event, ui) {

                                                            $("#physical_city_value_id").val(ui.item.id);
                                                            $("#physical_city_value").val(ui.item.value);
                                                        },
                                                        change: function (event, ui) {
                                                            if (ui.item == null) {
                                                                $(this).val("");
                                                                $(this).focusout();
                                                                $(this).focus();
                                                            }
                                                        }
                                                    });
*/

                                                    //Autocomplete for branch country

                                                    //End
                                                    var counter = 1;
                                                    var x = 1;
                                                    var company_limit = 3;
                                                    var agent_limit = 3;
                                                    var repres_limit = 3;
                                                    // var questions = $('#html-div').html();
                                                    var questions = '<div class="styled-input">' +
                                                            '<input type="email" name="company_email[]" class="form-control email_validation"/>' +
                                                            '<label>Email</label>' +
                                                            '<span></span>' +
                                                            '</div>';



                                                    $('.screens-duplicate-button').click(function ()
                                                    {
                                                        if (x < company_limit)
                                                        {
                                                            counter++;
                                                            //$('#html-div').append(questions);
                                                            var eleLength = $('#html-div')[0].childElementCount;
                                                            var element = $('#html-div')[0].children;
                                                            var i = 0;
                                                            $(element).each(function (index) {

                                                                if (index === eleLength - 1) {

                                                                    // $(this).attr('id','quetions_'+ counter);
                                                                    $(this).after(questions + '<i id="additionalEmail' + x + '" class="fa fa-remove removeAdditionalEmail" />');


                                                                }

                                                                i++;
                                                            });
                                                            x++;


                                                        } else {
                                                            alert('You can add company email upto 3 only');
                                                        }
                                                        $('input,textarea').keyup(function () {
                                                            if ($(this).val().length > 0) {
                                                                $(this).addClass('not-empty');
                                                                $('.email_validation').attr('data-parsley-type', 'email')
                                                                        .attr('data-parsley-trigger', 'change focusout')
                                                                        .parsley();
                                                            } else {
                                                                $(this).removeClass('not-empty');
                                                                $('.email_validation').removeAttr('data-parsley-type', 'email')
                                                                        .removeAttr('data-parsley-trigger', 'change focusout')
                                                                        .parsley().destroy();
                                                            }

                                                        });
                                                          $('input,textarea').each(function () {
                                                                if ($(this).val().length > 0) {
                                                                    $(this).addClass('not-empty');
                                                                } else {
                                                                    $(this).removeClass('not-empty');
                                                                }
                                                            });


                                                    });

                                                    //Registered Agents
                                                    var agentCount = 1;
                                                    //var agentDiv = $('#reg_agents').html();
                                                    var agentDiv = '<div>' +
                                                            '<div class="col-md-4 col-sm-4">' +
                                                            '<div class="input-wrapper full-width">' +
                                                            '<div class="styled-input">' +
                                                            '<input type="text" name="agent_first_name[]" value="" class="form-control"  data-parsley-trigger="change focusout" data-parsley-maxlength="200" />' +
                                                            '<label>First Name</label>' +
                                                            '<span></span>' +
                                                            '</div>' +
                                                            '</div>' +
                                                            '</div>' +
                                                            '<div class="col-md-4 col-sm-4">' +
                                                            '<div class="input-wrapper full-width">' +
                                                            '<div class="styled-input">' +
                                                            '<input type="text" name="agent_last_name[]"  value=""  class="form-control" data-parsley-trigger="change focusout" data-parsley-maxlength="200"  />' +
                                                            '<label>Last Name</label>' +
                                                            '<span></span>' +
                                                            '</div>' +
                                                            '</div>' +
                                                            '</div>' +
                                                            '<div class="col-md-4 col-sm-4">' +
                                                            '<div class="input-wrapper full-width">' +
                                                            '<div class="styled-input">' +
                                                            '<input type="text" name="agent_title[]" value="" class="form-control" data-parsley-trigger="change focusout" data-parsley-maxlength="200"  />' +
                                                            '<label>Title</label>' +
                                                            '<span></span>' +
                                                            '</div>' +
                                                            '</div>' +
                                                            '</div>' +
                                                            '</div>';

                                                    $('.agent-duplicate-button').click(function () {
                                                        if (agentCount < agent_limit) {
                                                            var eleLength = $('#reg_agents')[0].childElementCount;
                                                            var element = $('#reg_agents')[0].children;
                                                            $(element).each(function (index) {

                                                                if (index === eleLength - 1) {
                                                                    // $(this).attr('id','quetions_'+ counter);
                                                                    $(this).after(agentDiv + '<div class="rghtagent"><i id="agent' + agentCount + '" class="fa fa-remove removeAgent" /></div>');

                                                                }
                                                            });
                                                            agentCount++;
                                                        } else {
                                                            alert('You can add officers upto 3 only');
                                                        }
                                                        $('input').keyup(function () {
                                                            if ($(this).val().length > 0) {
                                                                $(this).addClass('not-empty');

                                                            } else {
                                                                $(this).removeClass('not-empty');

                                                            }
                                                        });
                                                    });



                                                    //Representative
                                                    var represCount = 1;
                                                    //var representativeDiv = $('#representative').html();
                                                    var representativeDiv =
                                                            //'<div class="row" id="representative">' +
                                                            '<div class="full-width">' +
                                                            '<div class="col-md-6 col-sm-6">' +
                                                            '<div class="input-wrapper full-width">' +
                                                            '<div class="styled-input">' +
                                                            '<input type="text" name="repres_contact_person[]"  class="form-control repres_contact_person" value=""   />' +
                                                            '<label>Contact Person</label>' +
                                                            '<span></span>' +
                                                            '</div>' +
                                                            '</div>' +
                                                            '</div>' +
                                                            '<div class="col-md-6 col-sm-6">' +
                                                            '<div class="input-wrapper full-width">' +
                                                            '<div class="styled-input">' +
                                                            '<input type="text" name="repres_company_branch_name[]" class="form-control repres_company_branch_name"  value="" />' +
                                                            '<label>Position</label>' +
                                                            '<span></span>' +
                                                            '</div>' +
                                                            '</div>' +
                                                            '</div>' +
                                                            '<div class="col-md-6 col-sm-6">' +
                                                            '<div class="input-wrapper full-width">' +
                                                            '<div class="styled-input">' +
                                                            '<input type="email" name="repres_email[]" class="form-control email_validation"  value="" data-parsley-trigger="change focusout" data-parsley-type="email"/>' +
                                                            '<label>Email Address</label>' +
                                                            '<span></span>' +
                                                            '</div>' +
                                                            '</div>' +
                                                            '</div>' +
                                                            '<div class="col-md-6 col-sm-6">' +
                                                            '<div class="input-wrapper full-width">' +
                                                            '<div class="styled-input">' +
                                                            '<input type="text" name="repres_mobile_number[]" class="form-control mobile_validation mobile_no_masking"   value="" data-parsley-trigger="change focusout" data-parsley-type="(\(\d{3})\)\s\d{3}\-\d{4}$"/>' +
                                                            '<label>{{ trans("adminlte::adminlte.mobile_number") }}</label>' +
                                                            '<span></span>' +
                                                            '</div>' +
                                                            '</div>' +
                                                            '</div>' +
                                                            '</div>';
                                                    //    '</div>';

                                                    $('.representative-duplicate-button').click(function () {
                                                        if (represCount < repres_limit) {
                                                            var eleLength = $('#representative')[0].childElementCount;
                                                            var element = $('#representative')[0].children;
                                                            $(element).each(function (index) {

                                                                if (index === eleLength - 1) {
                                                                    // $(this).attr('id','quetions_'+ counter);

                                                                    $('.repres_contact_person').attr('data-parsley-trigger', 'change focusout')
                                                                            .attr('data-parsley-maxlength', '100')
                                                                            .parsley();
                                                                    $('.repres_company_branch_name').attr('data-parsley-trigger', 'change focusout')
                                                                            .attr('data-parsley-maxlength', '200')
                                                                            .parsley();
                                                                    $(this).after(representativeDiv + '<div class="rght"><i id="repres' + represCount + '" class="fa fa-remove removeRepresentative" /></div></div');
                                                                    // $(this).append();
                                                                }

                                                            });
                                                            represCount++;
                                                        } else {
                                                            alert('You can add representative upto 3 only');
                                                        }

                                                        $('input,textarea').keyup(function () {
                                                            if ($(this).val().length > 0) {
                                                                $(this).addClass('not-empty');
                                                                $('.email_validation').attr('data-parsley-type', 'email')
                                                                        .attr('data-parsley-trigger', 'change focusout')
                                                                        .parsley();

//                    $('.mobile_validation').attr('data-parsley-type', 'digits')
//                            .attr('data-parsley-minlength','10')
//                             .attr('data-parsley-maxlength','10')
//                            .attr('data-parsley-trigger', 'change focusout')
//                            .parsley();
                                                            } else {
                                                                $(this).removeClass('not-empty');
                                                                $('.email_validation').removeAttr('data-parsley-type', 'email')
                                                                        .removeAttr('data-parsley-trigger', 'change focusout')
                                                                        .parsley().destroy();

//                    $('.mobile_validation').removeAttr('data-parsley-type', 'digits')
//                            .removeAttr('data-parsley-trigger', 'change focusout')
//                                .removeAttr('data-parsley-minlength','10')
//                             .removeAttr('data-parsley-maxlength','10')
//                              .parsley().destroy();
                                                            }
                                                        });

                                                        $('input,textarea').each(function () {
                                                            if ($(this).val().length > 0) {
                                                                $(this).addClass('not-empty');
                                                            } else {
                                                                $(this).removeClass('not-empty');
                                                            }
                                                        });
                                                        $('.mobile_no_masking').mask("(999) 999-9999");

                                                    });
                                                    $(document).on('click', '.removeAdditionalEmail', function () {

                                                        $('#' + $(this).attr('id')).prev("div").remove();
                                                        $('#' + $(this).attr('id')).remove();
                                                        company_limit++;
                                                    });
                                                    $(document).on('click', '.removeAgent', function () {

                                                        $('#' + $(this).attr('id')).parent().prev("div").remove();
                                                        $('#' + $(this).attr('id')).remove();
                                                        agent_limit++;
                                                    });

                                                    $(document).on('click', '.removeRepresentative', function () {

//            $('#' + $(this).attr('id')).parent().prev("div").remove();
                                                        $('#' + $(this).attr('id')).parent().prev("div").remove();
                                                        $('#' + $(this).attr('id')).remove();
                                                        repres_limit++;
                                                    });

                                                    if ($('#number_offices').val() > 0) {
                                                        for (i = 1; i <= $('#number_offices').val(); i++) {

                                                            $('#subdiv').show();
                                                        }
                                                    } else {
                                                        $('#showBranchDiv').hide();
                                                    }

                                                    var branch_div_html = $('#showBranchDiv').html();
                                                    $('#number_offices').keyup(function () {
                                                        var branchDiv = document.getElementById('showBranchDiv');

                                                        var number_of_offices = $('#number_offices').val();


                                                        if ($(this).val() == "") {
                                                            $('#showBranchDiv').hide();
                                                        } else {
                                                            var i;
                                                            if (number_of_offices <= 3) {
                                                                var branch_div_html_1 = '';
                                                                for (i = 1; i <= number_of_offices; i++) {

                                                                    branch_div_html_1 += branch_div_html;
                                                                    $('#showBranchDiv').html(branch_div_html_1);
                                                                    $('#showBranchDiv').show();
                                                                    $('input').keyup(function () {
                                                                        if ($(this).val().length > 0) {
                                                                            $(this).addClass('not-empty');
                                                                            $('.branch_zip').attr('data-parsley-type', 'digits')
                                                                                    .attr('data-parsley-minlength', '5')
                                                                                    .attr('data-parsley-maxlength', '5')
                                                                                    .attr('data-parsley-trigger', 'change focusout')
                                                                                    .parsley();
                                                                            $('.branch_name').attr('data-parsley-trigger', 'change focusout')
                                                                                    .attr('data-parsley-maxlength', '100')
                                                                                    .parsley();


                                                                            $('.branch_address').attr('data-parsley-trigger', 'change focusout')
                                                                                    .attr('data-parsley-maxlength', '500')
                                                                                    .parsley();

                                                                            $('.branch_first_name').attr('data-parsley-trigger', 'change focusout')
                                                                                    .attr('data-parsley-maxlength', '100')
                                                                                    .parsley();
                                                                            $('.branch_last_name').attr('data-parsley-trigger', 'change focusout')
                                                                                    .attr('data-parsley-maxlength', '100')
                                                                                    .parsley();
                                                                            $('.branch_title').attr('data-parsley-trigger', 'change focusout')
                                                                                    .attr('data-parsley-maxlength', '100')
                                                                                    .parsley();
                                                                            $(".branch_phone").mask("(999) 999-9999");
                                                                            $('.branch_contact_person').attr('data-parsley-trigger', 'change focusout')
                                                                                    .attr('data-parsley-pattern', '/^[a-zA-Z ]*$/')
                                                                                    .attr('data-parsley-maxlength', '100')
                                                                                    .parsley();


                                                                            $(".branch_phone").on("blur", function () {
                                                                                var last = $(this).val().substr($(this).val().indexOf("-") + 1);

                                                                                if (last.length == 5) {
                                                                                    var move = $(this).val().substr($(this).val().indexOf("-") + 1, 1);

                                                                                    var lastfour = last.substr(1, 4);

                                                                                    var first = $(this).val().substr(0, 9);

                                                                                    $(this).val(first + move + '-' + lastfour);
                                                                                }
                                                                            });

                                                                        } else {
                                                                            $(this).removeClass('not-empty');
                                                                            $('.branch_zip').removeAttr('data-parsley-type')
                                                                                    .removeAttr('data-parsley-trigger')
                                                                                    .removeAttr('data-parsley-minlength')
                                                                                    .removeAttr('data-parsley-maxlength')
                                                                                    .parsley();

                                                                        }
                                                                    });

                                                                }
                                                            } else {
                                                                alert('Please enter number of brances upto 3');
                                                            }
                                                        }

                                                        $('.branch_state_value').selectpicker('refresh');
                                                        //Autocomplete for branch city
                                                        var county_arr = [];
                                                        var city_arr = [];
                                                        $(".branch_city_val").on("focus.autocomplete", null, function () {
                                                            $(this).autocomplete({

                                                                source: function (request, response) {
                                                                    $.ajax({
                                                                        url: src_city,
                                                                        dataType: "json",
                                                                        data: {
                                                                            term: request.term
                                                                        },
                                                                        beforeSend: function () {
                                                                            $('.branchcityloader').show();
                                                                        },
                                                                        complete: function () {
                                                                            $('.branchcityloader').hide();
                                                                        },
                                                                        success: function (data) {

                                                                            response($.map(data, function (item) {
                                                                                return {
                                                                                    value: item.value,
                                                                                    id: item.id     // EDIT
                                                                                }
                                                                            }));

                                                                        },

                                                                    });
                                                                },
                                                                select: function (event, ui) {
                                                                    var cityid = ui.item.id;

                                                                    city_arr.push(cityid);
                                                                    $("#branch_city_ids").val(city_arr);

                                                                    $(this).val(ui.item.value);
                                                                    $('.city_select_validation').hide();
                                                                },
                                                                change: function (event, ui) {
                                                                    if (ui.item == null) {
                                                                        $(this).val("");
                                                                        $(this).focusout();
                                                                        $(this).focus();
                                                                        $(this).after('<ul class="parsley-errors-list filled city_select_validation"><li class="parsley-required">Select City from the list</li></ul>');

                                                                    }
                                                                }
                                                            });
                                                        });
                                                        src_county_name = "{{ url('customer/city/autocompletecountyname') }}";
                                                        $(".branch_country").on("focus.autocomplete", null, function () {

                                                            $(this).autocomplete({

                                                                source: function (request, response) {
                                                                    $.ajax({
                                                                        url: src_county_name,
                                                                        dataType: "json",
                                                                        data: {
                                                                            term: request.term
                                                                        },
                                                                        beforeSend: function () {
                                                                            $('.branchcountyloader').show();
                                                                        },
                                                                        complete: function () {
                                                                            $('.branchcountyloader').hide();
                                                                        },
                                                                        success: function (data) {

                                                                            response($.map(data, function (item) {
                                                                                return {
                                                                                    value: item.value,
                                                                                    id: item.id     // EDIT
                                                                                }
                                                                            }));

                                                                        },

                                                                    });
                                                                },
                                                                select: function (event, ui) {
                                                                    var countyid = ui.item.id;

                                                                    county_arr.push(countyid);

                                                                    $(".branch_county_ids").val(county_arr);
                                                                    $(this).val(ui.item.value);
                                                                    $('.county_select_validation').hide();

                                                                },
                                                                change: function (event, ui) {
                                                                    if (ui.item == null) {
                                                                        $(this).val("");
                                                                        $(this).focusout();
                                                                        $(this).focus();
                                                                        $(this).after('<ul class="parsley-errors-list filled county_select_validation"><li class="parsley-required">Select County from the list</li></ul>');

                                                                    }
                                                                }
                                                            });
                                                        });
                                                    });



                                                    $("#office_number").mask("(999) 999-9999");
                                                    $('.mobile_no_masking').mask("(999) 999-9999");

                                                    $("#office_number").on("blur", function () {
                                                        var last = $(this).val().substr($(this).val().indexOf("-") + 1);

                                                        if (last.length == 5) {
                                                            var move = $(this).val().substr($(this).val().indexOf("-") + 1, 1);

                                                            var lastfour = last.substr(1, 4);

                                                            var first = $(this).val().substr(0, 9);

                                                            $(this).val(first + move + '-' + lastfour);
                                                        }
                                                    });

                                                    $("#fax_number").mask("(999) 999-9999");


                                                    $("#fax_number").on("blur", function () {
                                                        var last = $(this).val().substr($(this).val().indexOf("-") + 1);

                                                        if (last.length == 5) {
                                                            var move = $(this).val().substr($(this).val().indexOf("-") + 1, 1);

                                                            var lastfour = last.substr(1, 4);

                                                            var first = $(this).val().substr(0, 9);

                                                            $(this).val(first + move + '-' + lastfour);
                                                        }
                                                    });


                                                });
//Same as above for physical address
                                                $('#same_as_mailing').on('change', function () {
                                                    if (this.checked) {
                                                        $('#company_physical_address_id').val($('#mailing_address_id').val());
                                                        $('#physical_state_value').val($('#state_value').val());
                                                        $("#physical_state_value").selectpicker("refresh");
                                                        /*$('#physical_city_value').val($('#mailing_city_value_id').val());*/
                                                        /* $("#physical_city_value").val($('#mailing_city_value_id').val());*/
                                                        

                                                        $('#physical_city_value_id').empty().trigger('change');
                                                        var option = new Option($('#mailing_city_value_id').select2("data")[0]['text'],$('#mailing_city_value_id').val(),true, true);
                                                        
                                                        $("#physical_city_value_id").html(option);
                                                        $('#physical_zip_id').val($('#mailing_zip_id').val());
                                                        if ($('#company_physical_address_id').val()) {

                                                            $('input[name="company_physical_address"]').removeAttr('data-parsley-required')
                                                                    .removeAttr('data-parsley-required-message')
                                                                    .removeAttr('data-parsley-trigger')
                                                                    .parsley()
                                                                    .destroy();

                                                        }
                                                        if ($('#physical_state_value').val()) {
                                                            $('#physical_state_value').removeAttr('data-parsley-required')
                                                                    .removeAttr('data-parsley-required-message')
                                                                    .removeAttr('data-parsley-trigger')
                                                                    .parsley()
                                                                    .destroy();
                                                        }
                                                        if ($('#physical_city_value_id').val()) {
                                                            $('#physical_city_value_id').removeAttr('data-parsley-required')
                                                                    .removeAttr('data-parsley-required-message')
                                                                    .removeAttr('data-parsley-trigger')
                                                                    .parsley()
                                                                    .destroy();
                                                        }
                                                        if ($('#physical_zip_id').val()) {
                                                            $('#physical_zip_id').removeAttr('data-parsley-required')
                                                                    .removeAttr('data-parsley-type')
                                                                    .removeAttr('data-parsley-trigger')
                                                                    .removeAttr('data-parsley-required-message')
                                                                    .parsley()
                                                                    .destroy();
                                                        }
                                                        $('#company_physical_address_id').attr('readonly', true);
                                                        // $('#physical_state_value').attr('disabled', true);
                                                        $('#physical_city_value').attr('readonly', true);
                                                        $('#physical_zip_id').attr('readonly', true);
                                                    } else {
                                                        $('#company_physical_address_id').val('');
                                                        $('#physical_state_value').val('');
                                                        $('#physical_state_value').selectpicker("refresh");
                                                        /*$('#physical_city_value').val('');
                                                        $('#physical_city_value').selectpicker("refresh");*/
                                                        $("#physical_city_value_id").select2("destroy");

                                                        $("#physical_city_value_id").select2();
                                                        $('#physical_zip_id').val('');

                                                        if (!$('#company_physical_address_id').val()) {

                                                            $('#company_physical_address_id').attr('data-parsley-required', 'true')
                                                                    .attr('data-parsley-required-message', 'The physical address is required')
                                                                    .attr('data-parsley-trigger', 'change focusout')
                                                                    .parsley();

                                                        }
                                                        if (!$('#physical_state_value').val()) {

                                                            $('#physical_state_value').attr('data-parsley-required', 'true')
                                                                    .attr('data-parsley-required-message', 'State is required')
                                                                    .attr('data-parsley-trigger', 'change focusout')
                                                                    .parsley();

                                                        }
                                                        if (!$('#physical_city_value_id').val()) {

                                                            $('#physical_city_value_id').attr('data-parsley-required', 'true')
                                                                    .attr('data-parsley-required-message', 'City is required')
                                                                    .attr('data-parsley-trigger', 'change focusout')
                                                                    .parsley();

                                                        }
                                                        if (!$('#physical_zip_id').val()) {

                                                            $('#physical_zip_id').attr('data-parsley-required', 'true')
                                                                    .attr('data-parsley-required-message', 'Zip Code is required')
                                                                    .attr('data-parsley-trigger', 'change focusout')
                                                                    .parsley();

                                                        }

                                                        $('#company_physical_address_id').attr('readonly', false);
                                                        //$('#physical_state_value').attr('disabled', false);
                                                        $('#physical_city_value').attr('readonly', false);
                                                        $('#physical_zip_id').attr('readonly', false);
                                                    }
                                                    $('input,textarea').each(function () {
                                                        if ($(this).val().length > 0) {
                                                            $(this).addClass('not-empty');
                                                        } else {
                                                            $(this).removeClass('not-empty');
                                                        }
                                                    });


                                                });


// function validate(field,value){
//    //Validate Company Name
//    if(field == 'company_name'){
//        if($.trim(value)==""){
//            $('#company_name_error').html('<strong>Please Enter Company Name.</strong>');
//        }
//        if(!/^[a-zA-Z ]+$/i.test(value)){
//            $('#company_name_error').html('<strong>Invalid Company Name.</strong>');
//        }   
//        
//    }
//    //Validate Contact Person
//    if(field == 'contact_person'){
//        if($.trim(value)==""){
//            $('#contact_person_error').html('<strong>Please Enter Contact Person.</strong>');
//        }
//        if(!/^[a-zA-Z ]+$/i.test(value)){
//            $('#contact_person_error').html('<strong>Invalid Contact Person.</strong>');
//        }   
//        
//    }
//    //Validate Mailing Address
//    if(field == 'mailing_address'){
//        if($.trim(value)==""){
//            $('#mailing_address_error').html('<strong>Please Enter Mailing Address.</strong>');
//        }
//    }
//    
//    //Validate
//    if(field == 'mailing_zip'){
//        if($.trim(value)==""){
//         $('#mailing_zip_error').html('<strong>Please Enter Mailing Zip.</strong>');
//        }
//        
//        if(!NaN(value)){
//            $('#mailing_zip_error').html('<strong>Invalid Mailing Zip.</strong>');
//        }
//    }
//    
//    if(field == 'company_physical_address'){
//        if($.trim(value)==""){
//            $('#company_physical_address_error').html('<strong>Please Enter Physical Address.</strong>');
//        }
//    }
//    
//    if(field == 'physical_zip'){
//        if($.trim(value)==""){
//         $('#physical_zip_error').html('<strong>Please Enter Physical Zip.</strong>');
//        }
//        
//        if(!NaN(value)){
//            $('#physical_zip_error').html('<strong>Invalid Physical Zip.</strong>');
//        }
//    }
//    if(field == 'office_number'){
//         if(!NaN(value)){
//            $('#office_number_error').html('<strong>Invalid Office Number.</strong>');
//        }
//    }
// }
</script>
@stop
