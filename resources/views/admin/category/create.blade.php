@extends('adminlte::page')

@section('content')

    <section class="">
        <div class="dashboard-wrapper">
            <div class="dashboard-heading">
                <h1><span>Customer Role</span></h1>
            </div>
            <div class="dashboard-inner-body">
                @if(Session::get('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
                @endif
                <form method="post" class="form" id="notice-form" data-parsley-validate="" action="{{url('admin/category/create')}}">
                {!! csrf_field() !!}
                    <div class="project-wrapper">
                    <div class="dash-subform">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
<!--                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="select-opt {{ $errors->has('notices') ? 'has-error' : '' }}">
                                            <label class="col-md-12 control-label" for="textinput">Customer Role</label>
                                            <select name="parent_category" class="selectpicker" >
                                                <option value="">Select Parent Customer Role*</option>
                                                @foreach($categories as $category)
                                                <option value="{{$category->id}}"> {{ucfirst(trans($category->name))}}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('parent_category'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('parent_category') }}</strong>
                                            </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>-->
                                
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input {{ $errors->has('name') ? 'has-error' : '' }}">
                                                    <input type="text" name="name" value="{{ old('name')}}" class="form-control" required data-parsley-required-message='Name is required' data-parsley-trigger="change focusout">
                                                    <label>Name<span class="mandatory-field">*</span></label>
                                                    @if ($errors->has('name'))
                                                    <p class="help-block">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </p>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-12 col-sm-12">
                                <div class="row">
                                    <div class="form-bottom-btn">
                                        <a href="{{url('admin/category')}}" class="btn btn-primary custom-btn customb-btn"><span>Cancel</span></a>
                                        <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>Save</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                                </div>
            </form>
        </div>
    </div>
</section>

@stop

@section('frontend_js')

@stop
