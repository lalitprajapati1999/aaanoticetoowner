@extends('adminlte::page')

@section('content')
  
    <section class="notice_template">
        <div class="dashboard-wrapper">
            <div class="dashboard-heading">
                <h1><span>Update Customer Role</span></h1>
            </div>
            <div class="dashboard-inner-body">
                @if(Session::get('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
                @endif
                  <form method="post" action="{{url('admin/category/update')}}">
                {!! csrf_field() !!}   
                  <input type="hidden" name="category_id" id="notice-form-edit" data-parsley-validate="" value="{{$CategoryData->id}}"/>
                <div class="project-wrapper">
                <div class="dash-subform">
                    <div class="row">
                         <div class="col-md-12 col-sm-12">
<!--                             <div class="row">
                                <div class="col-md-6 col-sm-6">
                                 <div class="select-opt {{ $errors->has('notices') ? 'has-error' : '' }}">
                                        <label class="col-md-12 control-label" for="textinput">Customer Role</label>
                                        <select name="parent_id" class="selectpicker" data-parsley-required="true"  data-parsley-required-message="Notice name is required" data-parsley-trigger="change focusout">
                                            @foreach($categories as $category)
                                            <option value="{{$category->id}}" @if ($category->id == $CategoryData->parent_id) selected="selected" @endif > {{ucfirst(trans($category->name))}}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('parent_id'))
                                        <p class="help-block">
                                            <strong>{{ $errors->first('parent_id') }}</strong>
                                        </p>
                                        @endif
                                    </div>
                                </div>
                            </div>-->
                           
                             <div class="row">
                                  <div class="col-md-12 col-sm-12">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input {{ $errors->has('name') ? 'has-error' : '' }}">
                                                    <input type="text" name="name" value="{{ $CategoryData->name}}" class="form-control" data-parsley-required="true" data-parsley-required-message='Name is required' data-parsley-trigger="change focusout">
                                                    <label>Name<span class="mandatory-field">*</span></label>
                                                    <span></span>
                                                    @if ($errors->has('name'))
                                                    <p class="help-block">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </p>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                             </div>
                            
                              <div class="col-md-12 col-sm-12">
                            <div class="row">
                                <div class="form-bottom-btn">
                                    <a href="{{url('admin/category')}}" class="btn btn-primary custom-btn customb-btn"><span>Cancel</span></a>
                                    <input type="submit" class="btn btn-primary custom-btn customc-btn" value="Update Category"/>
                                </div>
                            </div>
                        </div>
                         </div>
                    </div>
                </div>
                </div>
                </form>
            </div>
        </div>
    </section>

@stop

@section('frontend_js')

@stop
