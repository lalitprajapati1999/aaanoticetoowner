@extends('adminlte::page')

@section('content')

    <section class="">
        <div class="dashboard-wrapper">
            <div class="dashboard-heading">
                <h1><span>Project Role</span></h1>
            </div>
            <div class="dashboard-inner-body">
                @if(Session::get('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
                @endif
                <form method="post" class="form" id="notice-form" data-parsley-validate="" action="{{url('admin/project-role/create')}}">
                {!! csrf_field() !!}
                    <div class="project-wrapper">
                    <div class="dash-subform">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input {{ $errors->has('type') ? 'has-error' : '' }}">
                                                    <input type="text" name="name" value="{{ old('name')}}" class="form-control" required data-parsley-required-message='Project Role is required' data-parsley-trigger="change focusout">
                                                    <label>Project Role<span class="mandatory-field">*</span></label>
                                                    @if ($errors->has('name'))
                                                    <p class="help-block">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </p>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-md-12 col-sm-12">
                                <div class="row">
                                    <div class="form-bottom-btn">
                                        <a href="{{url('admin/project-type')}}" class="btn btn-primary custom-btn customb-btn"><span>Cancel</span></a>
                                        <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>Save</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                                </div>
            </form>
        </div>
    </div>
</section>

@stop

@section('frontend_js')

@stop
