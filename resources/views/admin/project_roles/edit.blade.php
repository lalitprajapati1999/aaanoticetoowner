@extends('adminlte::page')

@section('content')
  
    <section class="notice_template">
        <div class="dashboard-wrapper">
            <div class="dashboard-heading">
                <h1><span>Project Role</span></h1>
            </div>
            <div class="dashboard-inner-body">
                @if(Session::get('success'))
                <div class="alert alert-success">
                    {{ Session::get('success') }}
                </div>
                @endif
                  <form method="post" action="{{url('admin/project-role/update')}}">
                {!! csrf_field() !!}   
                  <input type="hidden" name="type_id" id="notice-form-edit" data-parsley-validate="" value="{{$TypesData->id}}"/>
                <div class="project-wrapper">
                <div class="dash-subform">
                    <div class="row">
                         <div class="col-md-12 col-sm-12">
                             
                           
                             <div class="row">
                                  <div class="col-md-12 col-sm-12">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input {{ $errors->has('type') ? 'has-error' : '' }}">
                                                    <input type="text" name="name" value="{{ $TypesData->name}}" class="form-control" data-parsley-required="true" data-parsley-required-message='Project Type is required' data-parsley-trigger="change focusout">
                                                    <label>Project Role<span class="mandatory-field">*</span></label>
                                                    <span></span>
                                                    @if ($errors->has('name'))
                                                    <p class="help-block">
                                                        <strong>{{ $errors->first('name') }}</strong>
                                                    </p>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                             </div>
                            
                              <div class="col-md-12 col-sm-12">
                            <div class="row">
                                <div class="form-bottom-btn">
                                    <a href="{{url('admin/project-role')}}" class="btn btn-primary custom-btn customb-btn"><span>Cancel</span></a>
                                    <input type="submit" class="btn btn-primary custom-btn customc-btn" value="Update Project Role"/>
                                </div>
                            </div>
                        </div>
                         </div>
                    </div>
                </div>
                </div>
                </form>
            </div>
        </div>
    </section>

@stop

@section('frontend_js')

@stop
