@extends('adminlte::page')
@section('content')

<div>
   
    <section class="address-book">
        <div class="dashboard-wrapper">
             @if(Session::get('success'))
        <div class="no-margin alert alert-success">{{ Session::get('success') }}</div>
        @endif
        @if(Session::has('error'))
        <div class="no-margin alert alert-danger">{{ Session::get('error') }}</div>
        @endif
            <div class="dashboard-heading">
                <h1><span>Project Types</span></h1>
            </div>
            <div class="dashboard-inner-body">
                <form class="" action="" method="post">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <div class="add-new-btn">
                                <a  href="{{url('admin/project-type/create')}}" type="button" class="btn btn-primary custom-btn">
                                    <span>+ Add Project Type</span></a>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="dahboard-table table-responsive fixhead-table">
                    <table class="table table-striped" datatable="" width="100%" cellspacing="0"   data-scroll-x="true"scroll-collapse="false">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">Project Type</th>
                                <th scope="col">Options</th>
                            </tr>
                        </thead>
                        <tbody>
                             @if(count($projectTypes)>0)
                            @foreach($projectTypes as $type)
                            <tr>
                                <td>{{$type->type}}</td>
                              
                                <td>
                                     <div class="action-icon">

                                        <a href="{{url('admin/project-type/edit/'.$type->id)}}" class="btn btn-secondary item red-tooltip" data-toggle="tooltip" data-placement="bottom" title="Edit"><span class="icon-pencil-edit-button"></span></a>

                                        <a href="#" data-target="#attachment-confirmation-modal" onclick="removeProjectType(<?php echo $type->id; ?>)" data-toggle="modal" class="btn btn-secondary item red-tooltip" data-placement="bottom" title="Remove"><span class="icon-cross-remove-sign"></span></a>
                                     </div>
                                </td>
                            </tr>
                            @endforeach
                            @else
                        <td class="text-center" colspan="4">{{'No records found'}}</td>
                            @endif
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </section>
</div>
<form action="{{url('admin/project-type/destroy')}}" method="get" class="remove-attachment-model">
       {!! csrf_field() !!}
<div id="attachment-confirmation-modal" class="modal fade">
    <div class="modal-dialog modal-confirm">
        <div class="modal-content">
            <div class="modal-header">
                <div class="icon-box">
                    <i class="fa fa-close"></i>
                </div>
                <input type="text" name="id" id="project_type_id" hidden>              
                <h4 class="modal-title">Are you sure?</h4>  
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <p>Do you really want to delete these records? This process cannot be undone.</p>
            </div>
            <div class="modal-footer">
                <div class="form-bottom-btn">
                      <button type="button" class="btn btn-primary custom-btn customb-btn remove-attachement-from-delete-form" data-dismiss="modal"><span>Cancel</span></button>
                    <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>Confirm</span></button>
                </div>
            </div>
        </div>
    </div>
</div>
</form>
@stop

@section('frontend_js')
<!-- DATA TABLES SCRIPT -->
<script src="{{ asset('vendor/adminlte/plugins/datatables/jquery.dataTables.js') }}" type="text/javascript"></script>

<script src="{{ asset('vendor/backpack/crud/js/crud.js') }}"></script>
<script src="{{ asset('vendor/backpack/crud/js/form.js') }}"></script>
<script src="{{ asset('vendor/backpack/crud/js/list.js') }}"></script>


<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.bootstrap.min.js" type="text/javascript"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js" type="text/javascript"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js" type="text/javascript"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js" type="text/javascript"></script>
<script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js" type="text/javascript"></script>
<script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js" type="text/javascript"></script>
<script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.colVis.min.js" type="text/javascript"></script>


<script src="{{ asset('vendor/adminlte/plugins/datatables/dataTables.bootstrap.js') }}" type="text/javascript"></script>

<script type="text/javascript">
function removeProjectType(id){
    $('#project_type_id').val(id);
}
jQuery(document).ready(function ($) {



});
</script>

<!-- CRUD LIST CONTENT - crud_list_scripts stack -->

@endsection
