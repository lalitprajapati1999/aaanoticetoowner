@extends('adminlte::page')

@section('content')
<section class="work-order">
    <div class="dashboard-wrapper">
        <div class="dashboard-heading">
            <h1><span>Register New Employee</span></h1>
        </div>

        <div class="dashboard-inner-body">
            <!--data-parsley-validate=""-->
            <form id="" class="form"  data-parsley-validate="" action="{{ url('admin/employee/store') }}" method="post">
                {!! csrf_field() !!}
                @if (Session::get('success'))
                <div class="alert alert-success no-margin">
                   <?php echo Session::get('success'); ?>
                </div>
                @endif
                @if(session()->has('error'))
                @if ($errors->count())
                <div class="alert alert-danger no-margin">
                    <ul>
                        @foreach ($errors->all() as $e)
                        <li>{{ $e }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @endif
                <div class="project-wrapper">
                    <div class="dash-subform">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="row">
                                    <div class="col-md-8 col-sm-8">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="name" value="{{ old('name')}}" class="form-control" required data-parsley-required-message="Name is required" data-parsley-trigger="change focusout" data-parsley-pattern="/^[a-zA-Z ]*$/">
                                                <label>Employee Full Name<span class="mandatory-field">*</span></label>
                                                <span></span>
                                                @if ($errors->has('name'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="email" name="email" value="{{ old('email')}}" class="form-control" required data-parsley-required-message="Email is required" data-parsley-trigger="change focusout" data-parsley-type="email">
                                                <label>Email<span class="mandatory-field">*</span></label>
                                                <span></span>
                                                @if ($errors->has('email'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="password" name="password" value="{{old('password')}}" class="form-control" id="inputPassword" required data-parsley-required-message="Password is required" data-parsley-trigger="change focusout" data-parsley-minlength="6" data-parsley-maxlength="20">
                                                <label>Password<span class="mandatory-field">*</span></label>
                                                <span></span>

                                                @if ($errors->has('password'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="password" name="retype_password" value="{{old('retype_password')}}"  class="form-control" required data-parsley-required-message="Retype password is required" data-parsley-trigger="change focusout" data-parsley-equalto="#inputPassword" data-parsley-equalto-message='Not same as Password'>
                                                <label>Retype Password<span class="mandatory-field">*</span></label>
                                                <span></span>

                                                @if ($errors->has('retype_password'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('retype_password') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <!--                                   <div class="col-md-8 col-sm-8">
                                                                            <div class="input-wrapper full-width">
                                                                                <div class="styled-input">
                                                                                    <input type="radio" name="role" value="2" @if(old('role') == 2) checked="checked" @endif>Admin<br>
                                                                                    <input type="radio" name="role" value="3" @if(old('role') == 3) checked="checked" @endif>Account Manager<br>
                                                                                    <label>Role</label>
                                                                                </div>
                                                                                @if ($errors->has('role'))
                                                                                <span class="help-block">
                                                                                    <strong>{{ $errors->first('role') }}</strong>
                                                                                </span>
                                                                                @endif
                                                                            </div>
                                                                        </div>-->

                                    <div class="col-md-6 col-sm-6">
                                        <div class="radio-group">
                                            <div class="readonly full-width">
                                                <label class="full-width control-label">Role<span class="mandatory-field">*</span></label>
                                                <ul class="button-holder full-width list-inline">
                                                    <li><input type="radio" id="radio-1-4" name="role" data-parsley-required="true" data-parsley-required-message="The role is required" class="regular-radio" value="2" @if(old('role') == 2) checked="checked" @endif><label for="radio-1-4">Admin</label></li>
                                                    <li><input type="radio" id="radio-1-5" name="role" data-parsley-required="true" data-parsley-required-message="The role is required" class="regular-radio" value="3" @if(old('role') == 3) checked="checked" @endif><label for="radio-1-5">Account Manager</label></li>
                                                </ul>
                                                @if ($errors->has('role'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('role') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-sm-6 clear-b">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="contact_number" value="{{old('contact_number')}}" class="form-control"  data-parsley-trigger="change focusout" id="contact_no" required data-parsley-required-message="Contact number is required"/>
                                                <label>Contact Number<span class="mandatory-field">*</span></label>
                                                <span></span>

                                                @if ($errors->has('contact_number'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('contact_number') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="emergency_contact_number" value="{{ old('emergency_contact_number')}}" class="form-control"  data-parsley-trigger="change focusout" id="emergency_contact_no" required data-parsley-required-message="Emergency contact number is required"/>
                                                <label>Emergency Contact Number<span class="mandatory-field">*</span></label>
                                                <span></span>

                                                @if ($errors->has('emergency_contact_number'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('emergency_contact_number') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <!--                                     <div class="col-md-6 col-sm-6">
                                                                            <div class="input-wrapper full-width">
                                                                                <div class="styled-input">
                                                                                    <input type="radio" name="mailing_access" value="yes" @if(old('mailing_access') == 'yes') checked="checked" @endif> Yes<br>
                                                                                    <input type="radio" name="mailing_access" value="no" @if(old('mailing_access') == 'no') checked="checked" @endif> No<br>
                                                                                    <label>Allow Access on Mailing</label>
                                                                                </div>
                                                                            </div>
                                                                        </div> -->
                                    
                                    <div class="col-md-6 col-sm-6 show_mailing_access" style="display: none">
                                        <div class="radio-group">
                                            <div class="readonly full-width">
                                                <label class="full-width control-label">Allow Access on Mailing</label>
                                                <ul class="button-holder full-width list-inline">
                                                    <li><input type="radio" id="radio-1-7" name="mailing_access" value="yes"  class="regular-radio" @if(old('mailing_access') == 'yes') checked="checked" @endif><label for="radio-1-7">Yes</label></li>
                                                    <li><input type="radio" id="radio-1-8" name="mailing_access" value="no"  class="regular-radio" @if(old('mailing_access') == 'no') checked="checked" @endif><label for="radio-1-8">No</label></li>
                                                </ul>
                                                @if ($errors->has('mailing_access'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('mailing_access') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6 col-sm-6"  id="create_pin" style="display: none">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="create_pin" id="create_pin_value" value="{{ old('create_pin')}}" class="form-control" />
                                                <label>Create Mailing Pin<span class="mandatory-field">*</span></</label>
                                                <span></span>

                                                @if ($errors->has('create_pin'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('create_pin') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                 
                                             <div class="col-md-6 col-sm-6"  id="admin_create_pin" style="display: none">
                                          
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="admin_create_pin" id="admin_create_pin_value" value="{{ old('admin_create_pin')}}" class="form-control" />
                                                <label>Create Mailing Pin<span class="mandatory-field">*</span></</label>
                                                <span></span>

                                                @if ($errors->has('admin_create_pin'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('admin_create_pin') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12">
                    <div class="row">
                        <div class="form-bottom-btn">
                            <a href="{{ url('admin/employees') }}" class="btn btn-primary custom-btn customb-btn"><span>{{ trans('backpack::base.cancel') }}</span></a>
                            <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>Create New Employee</span></button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection
@section('frontend_js')
<script type="text/javascript">
    $(document).ready(function () {
         $('input,textarea,select').on('focus', function(e) {
            e.preventDefault();
            $(this).attr("autocomplete", "nope");  
        });
        $('input[name="role"]').on('click', function () {
            if ($(this).val() == '3') {
                  $("#admin_create_pin").hide();
                $('.show_mailing_access').show();
                  $('input[name="mailing_access"]') .attr('data-parsley-required', 'true')
                                .attr('data-parsley-required-message', 'The mailing access is required')
                                .parsley();
                        
                $('input[name="mailing_access"]').on('click', function () {
                   
                    if ($(this).val() == 'yes') {
                        $("#create_pin").css("display", "block")
                         $('#create_pin_value').attr('data-parsley-required', 'true')
                                .attr('data-parsley-required-message', 'The mailing pin is required')
                                .attr('data-parsley-trigger','keyup')
                                .attr('data-parsley-minlength','4')
                                .attr('data-parsley-minlength-message','Pin should be of exactly four digits')
                                .attr('data-parsley-maxlength-message','Pin should have exactly four characters')
                                .attr('data-parsley-maxlength','4')
                                .parsley();

                    } else {
                        $("#create_pin").css("display", "none")
                         $('#create_pin_value').removeAttr('data-parsley-required')
                                .removeAttr('data-parsley-required-message')
                                .removeAttr('data-parsley-trigger')
                                .removeAttr('data-parsley-minlength')
                                .removeAttr('data-parsley-minlength-message')
                                .removeAttr('data-parsley-maxlength')
                                .removeAttr('data-parsley-maxlength-message')
                                .parsley()
                                .destroy();
                        $('#create_pin_value').val('');
                    }
                });
            } else {
                $('.show_mailing_access').hide();
                  $("#create_pin").hide();
//                  $('input[name="mailing_access"]').removeAttr('data-parsley-required')
//                                .removeAttr('data-parsley-required-message')
//                                .parsley()
//                                .destroy();
                   $("#admin_create_pin").css("display", "block")
                         $('#admin_create_pin_value').attr('data-parsley-trigger','keyup')
                                .attr('data-parsley-minlength','4')
                                 .attr('data-parsley-required', 'true')
                                .attr('data-parsley-required-message', 'The mailing pin is required')
                                .attr('data-parsley-minlength-message','Pin should be of exactly four digits')
                                .attr('data-parsley-maxlength-message','Pin should have exactly four characters')
                                .attr('data-parsley-maxlength','4')
                                .parsley();

            }
        });
        if ("{{old('mailing_access')}}" == 'yes') {
            $("#create_pin").css("display", "block");
        }
        if("{{old('role')}}" == '3'){
             $('.show_mailing_access').show();
        }
          if("{{old('role')}}" == '2'){
             $('#admin_create_pin').show();
        }

        $("#contact_no").mask("(999) 999-9999");


        $("#contact_no").on("blur", function () {
            var last = $(this).val().substr($(this).val().indexOf("-") + 1);

            if (last.length == 5) {
                var move = $(this).val().substr($(this).val().indexOf("-") + 1, 1);

                var lastfour = last.substr(1, 4);

                var first = $(this).val().substr(0, 9);

                $(this).val(first + move + '-' + lastfour);
            }
        });
//    emergency_contact_no
        $("#emergency_contact_no").mask("(999) 999-9999");


        $("#emergency_contact_no").on("blur", function () {
            var last = $(this).val().substr($(this).val().indexOf("-") + 1);

            if (last.length == 5) {
                var move = $(this).val().substr($(this).val().indexOf("-") + 1, 1);

                var lastfour = last.substr(1, 4);

                var first = $(this).val().substr(0, 9);

                $(this).val(first + move + '-' + lastfour);
            }
        });
    });

</script>
@endsection