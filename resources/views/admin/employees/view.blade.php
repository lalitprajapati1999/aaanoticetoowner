@extends('adminlte::page')
@section('content')
<section class="address-book-add-readonly">
    <div class="dashboard-wrapper">
        <div class="dashboard-heading">
            <h1><span>View Employee Details</span></h1>
        </div>
        <div class="dashboard-inner-body">
            <div class="dash-subform">
                <div class="row">
                    
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">Employee Full Name</label>
                                <p class="full-width">{{$employee->name}}</p>
                            </div>
                        </div>
                    </div>
                        <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">Role</label>
                                <p class="full-width">{{ucfirst($employee_role->name)}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">Email</label>
                                <p class="full-width">{{$employee->email}}</p>
                            </div>
                        </div>
                    </div>
                     <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">Password</label>
                                <p class="full-width">{{$employee->password_text}}</p>
                            </div>
                        </div>
                    </div>
                    

                    <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">Contact Number</label>
                                <p class="full-width">{{$employee->contact_number?$employee->contact_number:'N/A'}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">Emergency Contact Number</label>
                                <p class="full-width">{{$employee->emergency_contact?$employee->emergency_contact:'N/A'}}</p>
                            </div>
                        </div>
                    </div>
                    @if($employee_role->role_id == 3)
                     <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">Mailing Access Pin</label>
                                <p class="full-width">{{$employee->pin?$employee->pin:'N/A'}}</p>
                            </div>
                        </div>
                    </div>
                    @else
                      <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">Administration Pin</label>
                                <p class="full-width">{{$employee->pin?$employee->pin:'N/A'}}</p>
                            </div>
                        </div>
                    </div>
                    @endif

                </div>
            </div>
            <div class="col-md-12 col-sm-12">
                <div class="row">
                    <div class="form-bottom-btn">
                        <a href="{{URL::previous()}}" class="simple-hrf"><span>Back</span></a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
@endsection