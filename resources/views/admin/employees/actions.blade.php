<div class="action-icon">
     <a href="{{url('admin/employee/view/'.$employee->id)}}" class="btn btn-secondary item red-tooltip" data-toggle="tooltip" data-placement="bottom" title="View"><span class="icon-dark-eye"></span></a>
    <a href="{{url('admin/employee/edit/'.$employee->id)}}" class="btn btn-secondary item red-tooltip" data-toggle="tooltip" data-placement="bottom" title="Edit"><span class="icon-pencil-edit-button"></span></a>
    <a href="#notesModal" onclick="addNotes({{$employee->id}})" data-toggle="modal" class="btn btn-secondary item red-tooltip trigger-btn"  data-toggle="tooltip" data-placement="bottom" title="Note"><span class="icon-contract"></span></a>
    <a href="#attachmentModal" onclick="addAttachment({{$employee->id}})" data-toggle="modal" class="btn btn-secondary item red-tooltip trigger-btn" data-toggle="tooltip" data-placement="bottom" title="Attach Document"><span class="icon-attachment"></span></a>
    @if($employee->status == '0')
    <a href="#" data-target="#employeeactiveDeactivateModal" data-toggle="modal"  data-id="{{$employee->id}}" data-status="1" class="btn btn-secondary item red-tooltip change-status"  data-toggle="tooltip" data-placement="bottom" title="Disable"><span class="icon-on disable"></span></a>
    @else
    <a href="#" data-target="#employeeactiveDeactivateModal" data-toggle="modal" class="btn btn-secondary item red-tooltip change-status" data-toggle="tooltip"   data-toggle="tooltip" data-id="{{$employee->id}}" data-status="0" data-placement="bottom" title="Enable"><span class="icon-on"></span></a>
    @endif
</div>