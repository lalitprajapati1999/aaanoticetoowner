@extends('adminlte::page')

@section('content')
<section class="work-order">
    <div class="dashboard-wrapper">
        <div class="dashboard-heading">
            <h1><span>Update Employee</span></h1>
        </div>

        <div class="dashboard-inner-body">
            <!--data-parsley-validate=""-->
            <form id="" data-parsley-validate="" class="form" action="{{ url('admin/employee/update') }}" method="post">
                {!! csrf_field() !!}
                @if (Session::get('success'))
                <div class="alert alert-success no-margin">
                    <?php echo Session::get('success'); ?>
                </div>
                @endif
                @if(session()->has('error'))
                <div class="alert alert-danger no-margin">
                    <?php echo Session::get('error'); ?>
                </div>
                @endif
                <div class="project-wrapper">
                    <div class="dash-subform">
                        <div class="row">
                           <div class="col-md-12 col-sm-12">
                                <div class="row">
                                    <div class="col-md-8 col-sm-8">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="name" value="{{ old('name')?old('name'):$employee->name}}" class="form-control" data-parsley-required="true" data-parsley-required-message="Name is required" data-parsley-trigger="change focusout" data-parsley-pattern="/^[a-zA-Z ]*$/">
                                                <label>Employee Full Name<span class="mandatory-field">*</span></label>
                                                <span></span>
                                           
                                            @if ($errors->has('name'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </p>
                                            @endif
                                             </div>
                                        </div>
                                    </div>
                                     <div class="col-md-6 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="email" name="email" value="{{ old('email')?old('email'):$employee->email}}" class="form-control" data-parsley-required="true" data-parsley-required-message="Email is required" data-parsley-trigger="change focusout" data-parsley-type="email"/>
                                                <label>Email<span class="mandatory-field">*</span></label>
                                                <span></span>
                                           
                                            @if ($errors->has('email'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </p>
                                            @endif
                                             </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-6 col-sm-6">
                                        <div class="input-wrapper full-width">
                                              <div class="styled-input">
                                                <input type="password" name="password" class="form-control" id="inputPassword"  data-parsley-required-message="Password is required" data-parsley-trigger="change focusout" data-parsley-minlength="6" data-parsley-maxlength="20">
                                                <label>Password<span class="mandatory-field">*</span></label>
                                                 <span></span>
                                          
                                             @if ($errors->has('password'))
                                                            <p class="help-block">
                                                                <strong>{{ $errors->first('password') }}</strong>
                                                            </p>
                                             @endif
                                               </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="input-wrapper full-width">
                                                 <div class="styled-input">
                                                    <input type="password" name="retype_password" class="form-control" data-parsley-required-message="Retype password is required" data-parsley-trigger="change focusout" data-parsley-equalto="#inputPassword" data-parsley-equalto-message='Not same as Password'>
                                                    <label>Retype Password<span class="mandatory-field">*</span></label>
                                                     <span></span>
                                                 @if ($errors->has('retype_password'))
                                                                <p class="help-block">
                                                                    <strong>{{ $errors->first('retype_password') }}</strong>
                                                                </p>
                                                 @endif
                                               </div>

                                        </div>
                                    </div>
<!--                                      <div class="col-md-6 col-sm-6">
                                        <div class="radio-group">
                                            <div class="readonly full-width">
                                                <label class="full-width control-label">Role<span class="mandatory-field">*</span></label>
                                                <ul class="button-holder full-width list-inline">
                                                    <li><input type="radio" id="radio-1-4" name="role" data-parsley-required="true" data-parsley-required-message="The role is required" class="regular-radio" value="2" @if($role->role_id == 2) checked="checked" @endif><label for="radio-1-4">Admin</label></li>
                                                    <li><input type="radio" id="radio-1-5" name="role" data-parsley-required="true" data-parsley-required-message="The role is required" class="regular-radio" value="3" @if($role->role_id  == 3) checked="checked" @endif><label for="radio-1-5">Account Manager</label></li>
                                                </ul>
                                                @if ($errors->has('role'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('role') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>-->
<!--                                        <div class="col-md-6 col-sm-6">
                                            <div class="form-group">
                                                <div class="readonly full-width">
                                                    <label class="full-width">Role</label>
                                                    <p class="full-width">{{ucfirst($employee_role->name)}}</p>
                                                </div>
                                            </div>
                                        </div>-->
                                             <div class="col-md-6 col-sm-6">
                                        <div class="radio-group">
                                            <div class="readonly full-width">
                                                <label class="full-width control-label">Role<span class="mandatory-field">*</span></label>
                                                <ul class="button-holder full-width list-inline">
                                                    <li><input type="radio" id="radio-1-4" name="role" data-parsley-required="true" data-parsley-required-message="The role is required" class="regular-radio" value="2" @if(old('role') == 2 || $role->role_id == 2) checked="checked"  @endif><label for="radio-1-4">Admin</label></li>
                                                    <li><input type="radio" id="radio-1-5" name="role" data-parsley-required="true" data-parsley-required-message="The role is required" class="regular-radio" value="3" @if(old('role') == 3 || $role->role_id == 3) checked="checked" @endif><label for="radio-1-5">Account Manager</label></li>
                                                </ul>
                                                @if ($errors->has('role'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('role') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                     <div class="col-md-6 col-sm-6 clear-b">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="contact_number" value="{{old('contact_number')?old('contact_number'):$employee->contact_number}}" class="form-control"  data-parsley-trigger="change focusout" id="contact_no"/>
                                                <label>Contact Number<span class="mandatory-field">*</span></label>
                                                <span></span>
                                           
                                            @if ($errors->has('contact_number'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('contact_number') }}</strong>
                                            </p>
                                            @endif
                                             </div>
                                        </div>
                                    </div>
                           
                                    <div class="col-md-6 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="emergency_contact_number" value="{{ old('emergency_contact_number')?old('emergency_contact_number'):$employee->emergency_contact}}" class="form-control"  data-parsley-trigger="change focusout" id="emergency_contact_no"/>
                                                <label>Emergency Contact Number<span class="mandatory-field">*</span></label>
                                                <span></span>
                                         
                                            @if ($errors->has('emergency_contact_number'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('emergency_contact_number') }}</strong>
                                            </p>
                                            @endif
                                               </div>
                                        </div>
                                    </div>
                                 
                                    <div class="col-md-6 col-sm-6 show_mailing_access" style="display: none">
                                        <div class="radio-group">
                                            <div class="readonly full-width">
                                                <label class="full-width control-label">Allow Access on Mailing</label>
                                                <ul class="button-holder full-width list-inline">
                                                    <li><input type="radio" id="radio-1-7" name="mailing_access" value="yes"  class="regular-radio" @if(((isset($employee->pin) && $employee->pin != '') ) == 'yes') checked="checked" @endif><label for="radio-1-7">Yes</label></li>
                                                    <li><input type="radio" id="radio-1-8" name="mailing_access" value="no"  class="regular-radio" @if( $employee->pin == '') == 'no') checked="checked" @endif><label for="radio-1-8">No</label></li>
                                                </ul>
                                                @if ($errors->has('mailing_access'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('mailing_access') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                
                                    
                                     <div class="col-md-6 col-sm-6"  id="create_pin" style="display: none">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="create_pin" id="create_pin_value" value="{{ old('create_pin')?old('create_pin'):$employee->pin}}" class="form-control" />
                                                <label>Create A Pin</label>
                                                <span></span>

                                                @if ($errors->has('create_pin'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('create_pin') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                   
<!--                                   <div class="col-md-6 col-sm-6"  id="admin_create_pin" style='display: none'>
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text" name="admin_create_pin" id="admin_create_pin_value" value="{{ isset($employee->pin)?($employee->pin):''}}" class="form-control" />
                                                    <label>Create A Pin</label>
                                                    <span></span>

                                                    @if ($errors->has('admin_create_pin'))
                                                    <p class="help-block">
                                                        <strong>{{ $errors->first('admin_create_pin') }}</strong>
                                                    </p>
                                                    @endif
                                                </div>
                                            </div>
                                   </div>-->
                                  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12">
                    <div class="row">
                        <div class="form-bottom-btn">
                            <input type="hidden" name="employee_id" value="{{$employee->id}}"/>
                            <a href="{{ url('admin/employees') }}" class="btn btn-primary custom-btn customb-btn"><span>{{ trans('backpack::base.cancel') }}</span></a>
                            <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>Save</span></button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
@endsection
@section('frontend_js')
<script type="text/javascript">
    $(document).ready(function () {
        $('input[name="role"]').on('click', function () {
             $("#create_pin").css("display", "none");
             $('input[name="mailing_access"]').prop('checked',false);
           
            if ($(this).val() == '3') {
                
                $('.show_mailing_access').show();
                  $('input[name="mailing_access"]') .attr('data-parsley-required', 'true')
                                .attr('data-parsley-required-message', 'The mailing access is required')
                                .parsley();
                        
                $('input[name="mailing_access"]').on('click', function () {
                   
                    if ($(this).val() == 'yes') {
                        $("#create_pin").css("display", "block")
                         $('#create_pin_value').attr('data-parsley-required', 'true')
                                .attr('data-parsley-required-message', 'The mailing pin is required')
                                .attr('data-parsley-trigger','keyup')
                                .attr('data-parsley-minlength','4')
                                .attr('data-parsley-minlength-message','Pin should be of exactly four digits')
                                .attr('data-parsley-maxlength-message','Pin should have exactly four characters')
                                .attr('data-parsley-maxlength','4')
                                .parsley();

                    } else {
                        $("#create_pin").css("display", "none")
                         $('#create_pin_value').removeAttr('data-parsley-required')
                                .removeAttr('data-parsley-required-message')
                                .removeAttr('data-parsley-trigger')
                                .removeAttr('data-parsley-minlength')
                                .removeAttr('data-parsley-minlength-message')
                                .removeAttr('data-parsley-maxlength')
                                .removeAttr('data-parsley-maxlength-message')
                                .parsley()
                                .destroy();
                        $('#create_pin_value').val('');
                    }
                });
            } else {
                $('.show_mailing_access').hide();
                // $("#create_pin").hide();
                
                              $("#create_pin").css("display", "block");
                         $('#create_pin_value').attr('data-parsley-trigger','keyup')
                                .attr('data-parsley-minlength','4')
                                 .attr('data-parsley-required', 'true')
                                .attr('data-parsley-required-message', 'The mailing pin is required')
                                .attr('data-parsley-minlength-message','Pin should be of exactly four digits')
                                .attr('data-parsley-maxlength-message','Pin should have exactly four characters')
                                .attr('data-parsley-maxlength','4')
                                .parsley();
            }
        });
                 
                $('input[name="mailing_access"]').on('click', function () {
                   
                    if ($(this).val() == 'yes') {
                        $("#create_pin").css("display", "block")
                         $('#create_pin_value').attr('data-parsley-required', 'true')
                                .attr('data-parsley-required-message', 'The create pin is required')
                                .attr('data-parsley-trigger','keyup')
                                .attr('data-parsley-minlength','4')
                                .attr('data-parsley-minlength-message','Pin should be of exactly four digits')
                                .attr('data-parsley-maxlength-message','Pin should have exactly four characters')
                                .attr('data-parsley-maxlength','4')
                                .parsley();

                    } else {
                        $("#create_pin").css("display", "none")
                         $('#create_pin_value').removeAttr('data-parsley-required')
                                .removeAttr('data-parsley-required-message')
                                .removeAttr('data-parsley-trigger')
                                .removeAttr('data-parsley-minlength')
                                .removeAttr('data-parsley-minlength-message')
                                .removeAttr('data-parsley-maxlength')
                                .removeAttr('data-parsley-maxlength-message')
                                .parsley()
                                .destroy();
                        $('#create_pin_value').val('');
                    }
                });
              
//        if ("{{old('mailing_access')}}" == 'yes') {
//            $("#create_pin").css("display", "block");
//        }
        if("{{$role->role_id }}" == '3' || "{{old('role')}}" == '3'){
             $('.show_mailing_access').show();
        }   
        if("{{$employee->pin != NULL}}"){
             $('#create_pin').show();
        }
        $("#contact_no").mask("(999) 999-9999");


        $("#contact_no").on("blur", function () {
            var last = $(this).val().substr($(this).val().indexOf("-") + 1);

            if (last.length == 5) {
                var move = $(this).val().substr($(this).val().indexOf("-") + 1, 1);

                var lastfour = last.substr(1, 4);

                var first = $(this).val().substr(0, 9);

                $(this).val(first + move + '-' + lastfour);
            }
        });
//    emergency_contact_no
        $("#emergency_contact_no").mask("(999) 999-9999");


        $("#emergency_contact_no").on("blur", function () {
            var last = $(this).val().substr($(this).val().indexOf("-") + 1);

            if (last.length == 5) {
                var move = $(this).val().substr($(this).val().indexOf("-") + 1, 1);

                var lastfour = last.substr(1, 4);

                var first = $(this).val().substr(0, 9);

                $(this).val(first + move + '-' + lastfour);
            }
        });
     });
</script>
@endsection