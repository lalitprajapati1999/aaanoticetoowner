@extends('adminlte::page')
@section('content')

<div>
    <section class="address-book">
        <div class="dashboard-wrapper">

            @if (Session::get('success'))
            <div class="alert alert-success">
                <?php echo Session::get('success'); ?>
            </div>
            @endif
            <div class="employee-status"></div>
            <div class="dashboard-heading">
                <h1><span>Employees</span></h1>
            </div>

            <div class="dashboard-inner-body">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <div class="add-new-btn">
                            <a  href="{{url('admin/employee/create')}}" type="button" class="btn btn-primary custom-btn"><span>Register New Employee</span></a>
                        </div>
                    </div>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-2 col-xs-12">
                                <div class="form-group">
                                    <label for="">Filter By: </label>
                                </div>
                            </div>
                            <form role="form" method="post" id="employee-name">
                                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Employee Name" name="employee_name" id="autocompleteEmployeeName">
<!--                                        <span class="form-group-btn">
                                            <button class="btn btn-search" type="submit"><span class="icon-search"></span></button>
                                        </span>-->
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
                <div class="dahboard-table table-responsive fixhead-table">
                    <table class="table table-striped" id="employees-data" datatable="" width="100%" cellspacing="0"   data-scroll-x="true"scroll-collapse="false">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col" class="full-name">Full Name</th>
                                <th scope="col" class="">Mailing Address</th>
                                <th scope="col" class="">CYO</th>
                                <th scope="col" class="">Date Applied</th>
                                <th scope="col" class="action">Options</th>
                            </tr>
                        </thead>

                    </table>

                </div>
            </div>
        </div>
    </section>
</div>
<!--Note Modal -->
<form method="post" data-parsley-validate="" action="{{ url('admin/employee/add-note') }}" id="add_note">
    {!! csrf_field() !!}
    <div id="notesModal" class="modal fade register-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Note</h4>	
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                 <span id="noteerrormessage" class="no-margin alert alert-danger alert-dismissable" style="display: none"></span>
                <span id="notesuccessmessage" class="no-margin alert alert-success alert-dismissable" style="display: none"></span>
                <div class="modal-body">
                    <input type="hidden" name="employee_id" id="employee-id" value="">    
                    <!--    <div class="input-wrapper full-width">
                         <div class="styled-input">
                             <label>Date
                             {{date('m-d-Y')}}
                             </label>
                             <span></span>
                         </div>

                     </div> -->
                    <div class="radio-group">
                        <div class="readonly full-width">
                            <label class="full-width control-label">Date:
                                {{date('m-d-Y')}}</label>
                        </div>
                    </div>
                    <div class="input-wrapper full-width">
                        <div class="styled-input">
                            <textarea  class="form-control" name="user_note" id="user_note" required data-parsley-required-message="Note is required" data-parsley-trigger="change focusout"></textarea>
                            <label>Note<span class="mandatory-field">*</span></label>
                            <span></span>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form-bottom-btn">
                        <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>Submit</span></button>
                    </div>
                </div>
                <div class="dahboard-table table-responsive">
                    <table class="table table-striped" id="employee_notes_table">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">Note</th>
                                <th scope="col">Date</th>
                                <th scope="col">Options</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</form>
<!-- Attachment Modal-->
<form method="post" data-parsley-validate="" id="add_user_attachment" enctype="multipart/form-data" action="{{ url('admin/employee/add-attachment') }}">
    {!! csrf_field() !!}
    <div id="attachmentModal" class="modal fade register-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Attach Document</h4>	
                    <button type="button" class="close attachmentClose" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <span id="errormessage" class="no-margin alert alert-danger alert-dismissable" style="display: none"></span>
                <span id="successmessage" class="no-margin alert alert-success alert-dismissable" style="display: none"></span>
                <div class="modal-body">
                    <input type="hidden" name="employee_id" id="employeeid" value="">    
                    <div class="form-btn-div doc_file">
                        <span class="btn btn-default btn-file upload-btn btn btn-primary form-btn" id="temp">
                            Choose File to Upload  <input type="file" name="file_name" id="result_file" required data-parsley-required-message="Document is required" data-parsley-trigger="change focusout">
                        </span>
                        <span>(Allow Types : doc,docx,png,jpg,jpeg,pdf)</span>
                        <label class="custom-file-label" for="result_file"></label>

                    </div>
                    <span  id="preview" style="display: none;" ></span>

                </div>
                <div class="modal-footer">
                    <div class="form-bottom-btn">
                        <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>Attach Document</span></button>
                    </div>
                </div>
                <div class="dahboard-table table-responsive">
                    <table class="table table-striped" id="employee_attachment_table">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">Attachment</th>
                                <th scope="col">Options</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</form>
<form action="" method="POST" class="remove-record-model">
    {!! csrf_field() !!}
    <div id="note-confirmation-modal" class="modal fade">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="icon-box">
                        <i class="fa fa-close"></i>
                    </div>				
                    <h4 class="modal-title">Are you sure?</h4>	
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <p>Do you really want to delete these records? This process cannot be undone.</p>
                </div>
                <div class="modal-footer">
                    <div class="form-bottom-btn">
                        <button type="button" class="btn btn-primary custom-btn customb-btn remove-data-from-delete-form" data-dismiss="modal"><span>Cancel</span></button>
                        <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>Confirm</span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<form action="" method="POST" class="remove-attachment-model">
    {!! csrf_field() !!}
    <div id="attachment-confirmation-modal" class="modal fade">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="icon-box">
                        <i class="fa fa-close"></i>
                    </div>				
                    <h4 class="modal-title">Are you sure?</h4>	
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <p>Do you really want to delete these records? This process cannot be undone.</p>
                </div>
                <div class="modal-footer">
                    <div class="form-bottom-btn">
                        <button type="button" class="btn btn-primary custom-btn customb-btn remove-attachement-from-delete-form" data-dismiss="modal"><span>Cancel</span></button>
                        <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>Confirm</span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<form action="" method="POST" class="employee_active_deactivate_modal">
        {!! csrf_field() !!}
    <div id="employeeactiveDeactivateModal" class="modal fade">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="icon-box">
                        <i class="fa fa-close"></i>
                    </div>				
                    <h4 class="modal-title">Are you sure?</h4>	
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <p>Do you really want to active or deactive the customer?</p>
                </div>
                <div class="modal-footer">
                    <div class="form-bottom-btn">
                        <button type="button" class="btn btn-primary custom-btn customb-btn" data-dismiss="modal"><span>Cancel</span></button>
                        <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>Confirm</span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@section('frontend_js')
 
<!--<script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery-ui.min.js') }}"></script>-->
<script type="text/javascript" src="{{ asset('js/jquery-ui.min.js') }}"></script>

<script type="text/javascript">

    $(document).ready(function () {
        $('textarea,input').each(function () {
                    if ($(this).val().length > 0) {
                        $(this).addClass('not-empty');
                    } else {
                        $(this).removeClass('not-empty');
                    }
                });
                 $('textarea,input').keyup(function () {
                    if ($(this).val().length > 0) {
                        $(this).addClass('not-empty');
                    } else {
                        $(this).removeClass('not-empty');
                    }
                });
        
              $('#successmessage').hide();
              $('#errormessage').hide();
              $('.custom-file-label').hide();
         
        $(document).on('click', '.remove-record', function () {
            var id = $(this).attr('data-id');
            var url = $(this).attr('data-url');
            $(".remove-record-model").attr("action", url);
        });
        $('.remove-data-from-delete-form').click(function () {
            $('.remove-record-model').modal('hide');
        });
        $('.modal').click(function () {
        });

        $(document).on('click', '.remove-attachment', function () {
            var id = $(this).attr('data-id');
            var url = $(this).attr('data-url');
            $(".remove-attachment-model").attr("action", url);
        });
        $('.remove-attachment-from-delete-form').click(function () {
            $('.remove-attachment-model').modal('hide');

        });
        $('#attachment-confirmation-modal').click(function () {
        });
         src = "{{ url('admin/customer/searchajaxemployeename') }}";

     $("#autocompleteEmployeeName").autocomplete({
        source: function(request, response) {
            $.ajax({
                url: src,
                dataType: "json",
                data: {
                    term : request.term
                },
                success: function(data) {
                    response(data);
                   
                },
                 select: function( event, ui) {
                    $("#autocompleteEmployeeName").val(ui.item.id);
                }
            });
        },
//        minLength: 3,
       
    });
    });
   
    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
         employeesTable = $('#employees-data').DataTable({
            // "pageLength":10
            processing: true,
            serverSide: true,
            bPaginate: true,
            ordering:false,
//            sPaginationType: 'full_numbers',
            ajax: {
                url: "{{url('admin/employees/custom-filter')}}",
//                data: function (d) {
//                    d.employee_name = $('input[name=employee_name]').val();
//                }
            },
            columns: [
                {data: 'name', name: 'name'},
                {data: 'email', name: 'email'},
                {data: 'cyo', name: 'cyo'},
                {data: 'date_applied', name: 'date_applied'},
                {data: 'action', name: 'action'}
            ]

        });

        $('#employee-name').on('submit', function (e) {
            employeesTable.draw();
            e.preventDefault();
        });


    });
       
    function changeStatus(employeeId, status) {
        $.ajax({
            data: {'user_id': employeeId, 'status': status},
            type: "post",
            url: "{{url('admin/employee/changestatus') }}",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (result) {
                if (result) {
                    $('.employee-status').html('Employee status successfully changed');
                    $('.employee-status').addClass('alert alert-success');
                    window.location.reload();
                }

            }
        });
    }
    function addNotes(employee_id) {
        $('#employee-id').val(employee_id);
        $('#user_note').val('');
         $('#notesuccessmessage').css('display', 'none');
          $('#notesuccessmessage').html('');
          $('#noteerrormessage').css('display', 'none');
           $('#noteerrormessage').html('');
        $.ajax({
            data: {'id': employee_id},
            type: "post",
            url: "{{url('admin/employee/getNotes') }}",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (result) {

                var table = $("#employee_notes_table tbody");
                table.html('');
                $.each($.parseJSON(result), function (idx, elem) {
                    var custom_url = '{{url("admin/employee/note/delete")}}/' + elem.id;
//              table.append("<tr><td>"+elem.note+"</td><td><a href='"+custom_url+"' class='btn btn-secondary item red-tooltip' data-toggle='tooltip' data-placement='bottom' title='Remove'><span class='icon-cross-remove-sign'></span></a></td></tr>");
                    table.append("<tr><td>" + elem.note + "</td><td>" + elem.created_at + "</td><td><a class='btn btn-secondary item red-tooltip remove-record' data-toggle='modal' data-url='" + custom_url + "' data-id='" + elem.id + "'  data-target='#note-confirmation-modal' data-placement='bottom' title='Remove'><span class='icon-cross-remove-sign'></span></a></td></tr>");
                });
            }
        });
         
    }
    function addAttachment(employee_id) {
        $('#employeeid').val(employee_id);
             $('#successmessage').css('display', 'none');
          $('#successmessage').html('');
          $('#errormessage').css('display', 'none');
           $('#errormessage').html('');
        $.ajax({
            data: {'id': employee_id},
            type: "post",
            url: "{{url('admin/employee/getAttachments') }}",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (result) {

                var table = $("#employee_attachment_table tbody");
                table.html('');
                $.each($.parseJSON(result), function (idx, elem) {
                    var custom_url = '{{url("admin/employee/attachment/delete")}}/' + elem.id;
                    var file_url = '{{ asset("attachment/users_document")}}/' + elem.file_name;
                    // table.append("<tr><td><a target='_blank' href='" + file_url + "'>" + elem.file_name + "</a></td><td><a href='" + custom_url + "'>Delete</a></td></tr>");
                    table.append("<tr><td><a target='_blank' href='" + file_url + "'>" + elem.original_file_name + "</a></td><td><a class='btn btn-secondary item red-tooltip remove-attachment' data-toggle='modal' data-url='" + custom_url + "' data-id='" + elem.id + "'  data-target='#attachment-confirmation-modal' data-placement='bottom' title='Remove'><span class='icon-cross-remove-sign'></span></a></td></tr>");

                });
            }
        });
    }
   
    $('#add_user_attachment').on('submit', function (e) {
        $('#errormessage').css('display', 'none');
        $('#successmessage').css('display', 'none');

        e.preventDefault();
        var extension = $('#result_file').val().split('.').pop().toLowerCase();

        if ($.inArray(extension, ['doc', 'docx', 'png', 'jpg','jpeg','pdf']) == -1) {
            $('#errormessage').css('display', 'block');
            $('#errormessage').html('Please Select Valid File. ');
        } else {
        $('#errormessage').css('display', 'none');
        var file_data = $('#result_file').prop('files')[0];
        console.log(file_data);
        var form_data = new FormData();
        form_data.append('file_name', file_data);
        form_data.append('employee_id', $('#employeeid').val());
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name=_token]').attr('content')
            }
        });
        var submit_url = $(this).attr("action");
        $.ajax({
            type: 'POST',
            url: submit_url,
            dataType: "json",
            async: false,
            cache: false,
            processData: false,
            contentType: false,
            data: form_data,
            success: function (response) {
              
                if (response.success) {

                    $('#successmessage').css('display', 'block');
                    $('#successmessage').html('Document uploaded successfully');
                    
                        $('#errormessage').css('display', 'none');
                        $('#errormessage').html('');
                        $.ajax({
                                data: {'id': $('#employeeid').val()},
                                type: "post",
                                url: "{{url('admin/employee/getAttachments') }}",
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                success: function (result) {
                                    $('#preview').hide();
                                    $('#result_file').val('');
                                    //$('#attachmentModal').hide();
                                    var table = $("#employee_attachment_table tbody");
                                    table.html('');
                                    $.each($.parseJSON(result), function (idx, elem) {
                                        var custom_url = '{{url("admin/employee/attachment/delete")}}/' + elem.id;
                                        var file_url = '{{ asset("attachment/users_document")}}/' + elem.file_name;
                                        // table.append("<tr><td><a target='_blank' href='" + file_url + "'>" + elem.file_name + "</a></td><td><a href='" + custom_url + "'>Delete</a></td></tr>");
                                        table.append("<tr><td><a target='_blank' href='" + file_url + "'>" + elem.original_file_name + "</a></td><td><a class='btn btn-secondary item red-tooltip remove-attachment' data-toggle='modal' data-url='" + custom_url + "' data-id='" + elem.id + "'  data-target='#attachment-confirmation-modal' data-placement='bottom' title='Remove'><span class='icon-cross-remove-sign'></span></a></td></tr>");

                                    });
                                }
                         });
                }else if(response.error) {

                    $.each(response.error, function (key, value) {

                        $('#errormessage').css('display', 'block');
                        $('#errormessage').html(value);
                    });

                }
            }
        });
        }
    });
    $('#result_file').on('change', function (e) {
        //get the file name
        var fileName = $(this).val();

        //replace the "Choose a file" label
        $('.custom-file-label').html(fileName);


          var fileName = e.target.files[0].name;
        doc_icon = fileName.split('.')[1];
        if(doc_icon == 'pdf'){
            $('#preview').html('<i class="fa fa-file-pdf-o" style="font-size:36px;margin-left: 11%;"></i>');
        }else if(doc_icon == 'doc' || doc_icon == 'docx'){
            $('#preview').html('<i class="fa fa-file-word-o" style="font-size:36px;margin-left: 11%;"></i>');
        // }else if(doc_icon == 'pdf' || doc_icon == 'png'){
        //     $('#preview').html('<i class="fa fa-file-excel-o" style="font-size:36px; margin-left: 11%;"></i>');
        }else if(doc_icon == 'jpg' || doc_icon == 'jpeg' || doc_icon == 'png'){
            $('#preview').html('<i class="fa fa-file-image-o" style="font-size:36px;margin-left: 11%;"></i>');
        }else{
            $('#preview').html('<i class="fa fa-file" style="font-size:36px;margin-left: 11%;"></i>');
        }
        // $('#preview').html(fileName);
        $('#preview').css('display','block');
        $('#remove_icon').css('display','block')
    });
//    $('.attachmentClose').on('click',function(){
//        setTimeout(function () {
//                            location.reload();
//                        }, 2000);
//    });
 $(document).on('click', '.change-status', function () {
        var url = "{{url('admin/employee/changestatus') }}";
        var user_id = $(this).attr('data-id');
        var status = $(this).attr('data-status');

        $(".employee_active_deactivate_modal").attr("action", url);
        $('body').find('.employee_active_deactivate_modal').append('<input name="user_id" type="hidden" value="' + user_id + '">');
        $('body').find('.employee_active_deactivate_modal').append('<input name="_method" type="hidden" value="POST">');
        $('body').find('.employee_active_deactivate_modal').append('<input name="status" type="hidden" value="' + status + '">');
    });
     $(function () {
           otable = $('#employees-data').dataTable();
       }); 
    $('#autocompleteEmployeeName').on('keyup keypress change', function () {
       // otable.search(this.value).draw();
       otable.fnFilter(this.value, 0, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true);
   });
    
    $('#add_note').on('submit', function (e) {

        e.preventDefault();
       
        var form_data = new FormData();
        form_data.append('user_note', $('#user_note').val());
        form_data.append('employee_id', $('#employee-id').val());
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name=_token]').attr('content')
            }
        });
        var submit_url = $(this).attr("action");
        $.ajax({
            type: 'POST',
            url: submit_url,
            dataType: "json",
            async: false,
            cache: false,
            processData: false,
            contentType: false,
            data: form_data,
            success: function (response) {
              
                if (response.success) {

                    $('#notesuccessmessage').css('display', 'block');
                    $('#notesuccessmessage').html('Note added successfully');
                     $('#noteerrormessage').css('display', 'none');
                        $('#noteerrormessage').html('');
                    $('#user_note').val('');
                     $.ajax({
                        data: {'id': $('#employee-id').val()},
                        type: "post",
                        url: "{{url('admin/employee/getNotes') }}",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function (result) {

                            var table = $("#employee_notes_table tbody");
                            table.html('');
                            $.each($.parseJSON(result), function (idx, elem) {
                                var custom_url = '{{url("admin/employee/note/delete")}}/' + elem.id;
            //              table.append("<tr><td>"+elem.note+"</td><td><a href='"+custom_url+"' class='btn btn-secondary item red-tooltip' data-toggle='tooltip' data-placement='bottom' title='Remove'><span class='icon-cross-remove-sign'></span></a></td></tr>");
                                table.append("<tr><td>" + elem.note + "</td><td>" + elem.created_at + "</td><td><a class='btn btn-secondary item red-tooltip remove-record' data-toggle='modal' data-url='" + custom_url + "' data-id='" + elem.id + "'  data-target='#note-confirmation-modal' data-placement='bottom' title='Remove'><span class='icon-cross-remove-sign'></span></a></td></tr>");
                            });
                        }
                    });    
                }else if(response.error) {

                    $.each(response.error, function (key, value) {

                        $('#noteerrormessage').css('display', 'block');
                        $('#noteerrormessage').html(value);
                    });

                }
            }
        });
        
    });
</script>
@endsection
