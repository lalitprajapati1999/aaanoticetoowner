@extends('adminlte::page')

@section('content_header')
<h1>
    <span class="text-capitalize">{{ trans('admin.notices') }}</span>
    <!--<small>{{ trans('backpack::crud.add').' '.trans('admin.notice') }}.</small>-->
</h1>
<ol class="breadcrumb">
    <li><a href="{{ url(config('backpack.base.route_prefix'), 'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>
    <li><a href="{{ url('admin/notices') }}" class="text-capitalize">{{ trans('admin.notices') }}</a></li>
    <li class="active">{{ trans('backpack::crud.add') }}</li>
</ol>
@endsection

@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <!-- Default box -->
        @can ('admin-notices')
        <a href="{{ url('admin/notices') }}"><i class="fa fa-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} {{ trans('admin.notices') }}</a><br><br>
        @endcan

        <form method="post" action="{{ url('admin/notices') }}"data-parsley-validate="" >
            {!! csrf_field() !!}
            <div class="box">

                <div class="box-header with-border">
                    <h3 class="box-title">{{ trans('backpack::crud.add_a_new') }} {{ trans('admin.notice') }}</h3>
                </div>
                <div class="box-body">

                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <label class="control-label" for="{{ trans('common.name') }}">{!! $errors->has('name') ? '<i class="fa fa-times-circle-o"></i> ' : '' !!}{{ trans('common.name') }}</label>
                        <input type="text" name="name" data-parsley-required="true" maxlength="70" data-parsley-maxlength="70"  data-parsley-required-message="Name is required" data-parsley-trigger="change focusout"  class="form-control" value="{{ old('name') }}"/>
                        @if ($errors->has('name'))
                        <p class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </p>
                        @endif
                    </div>

                    <div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
                        <label class="control-label" for="{{ trans('common.type') }}">{!! $errors->has('type') ? '<i class="fa fa-times-circle-o"></i> ' : '' !!}{{ trans('common.type') }}</label>
                        <div class="radio">
                            <label>
                                <input type="radio" name="type" value="1" checked class='notice_type'>
                                {{ trans('admin.hard_doc') }}
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="type" value="2" class='notice_type'>
                                {{ trans('admin.soft_doc') }}
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="type" value="3" id="secondary_doc" class='notice_type'>
                                {{ trans('admin.secondary_doc') }}
                            </label>
                        </div>
                        <!--						<div class="radio">
                                                                                <label>
                                                                                        <input type="radio" name="type" value="3">
                                                                                        {{ trans('admin.secondary_doc') }}
                                                                                </label>
                                                                        </div>-->
                        @if ($errors->has('type'))
                        <span class="help-block">
                            <strong>{{ $errors->first('type') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('allow_cyo') ? 'has-error' : '' }}">
                        <label class="col-md-12 control-label" for="textinput">State</label>
                        <select name="state_id" class="selectpicker"data-parsley-required="true"   data-parsley-required-message="State is required" data-parsley-trigger="change focusout" >
                            <option value="">Select State</option>
                            @foreach($states as $state)
                            <option value="{{$state->id}}"> {{$state->name}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('state_id'))
                        <p class="help-block">
                            <strong>{{ $errors->first('state_id') }}</strong>
                        </p>
                        @endif
                    </div>
                    <div class="form-group">
                        <label class="col-md-12 control-label" for="textinput">Copy notice fields from</label>
                        <select name="notice" class="selectpicker" id="all_notices"  data-parsley-required="true"  data-parsley-required-message="Notice is required" data-parsley-trigger="change focusout" >


                        </select>
                        @if ($errors->has('previous_notice'))
                        <p class="help-block">
                            <strong>{{ $errors->first('notice') }}</strong>
                        </p>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('allow_cyo') ? 'has-error' : '' }} allow_cyo">
                        <label class="control-label" for="{{ trans('common.allow_cyo') }}">{!! $errors->has('allow_cyo') ? '<i class="fa fa-times-circle-o"></i> ' : '' !!}{{ trans('common.allow_cyo') }}</label>
                        <div class="radio">
                            <label>
                                <input type="radio" name="allow_cyo" id="enable_allow_cyo" value="1" checked>
                                {{ trans('admin.allow_cyo_enable') }}
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="allow_cyo" value="0">
                                {{ trans('admin.allow_cyo_disable') }}
                            </label>
                        </div>

                        @if ($errors->has('allow_cyo'))
                        <span class="help-block">
                            <strong>{{ $errors->first('allow_cyo') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('allow_deadline_calculator') ? 'has-error' : '' }}">
                        <label class="control-label" for="{{ trans('common.allow_deadline_calculator') }}">{!! $errors->has('allow_cyo') ? '<i class="fa fa-times-circle-o"></i> ' : '' !!}{{ trans('common.allow_deadline_calculator') }}</label>
                        <div class="radio">
                            <label>
                                <input type="radio" name="allow_deadline_calculator" value="1">
                                {{ trans('admin.allow_deadline_enable') }}
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="allow_deadline_calculator" value="0" checked>
                                {{ trans('admin.allow_deadline_disable') }}
                            </label>
                        </div>

                        @if ($errors->has('allow_deadline_calculator'))
                        <span class="help-block">
                            <strong>{{ $errors->first('allow_deadline_calculator') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('is_claim_of_lien') ? 'has-error' : '' }}">
                        <label class="control-label" for="{{ trans('common.is_claim_of_lien') }}">{!! $errors->has('is_claim_of_lien') ? '<i class="fa fa-times-circle-o"></i> ' : '' !!}{{ trans('common.is_claim_of_lien') }}</label>
                        <div class="radio">
                            <label>
                                <input type="radio" name="is_claim_of_lien" value="1">
                                {{ trans('admin.is_claim_of_lien_enable') }}
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="is_claim_of_lien" value="0" checked>
                                {{ trans('admin.is_claim_of_lien_disable') }}
                            </label>
                        </div>

                        @if ($errors->has('is_claim_of_lien'))
                        <span class="help-block">
                            <strong>{{ $errors->first('is_claim_of_lien') }}</strong>
                        </span>
                        @endif
                    </div>

                </div><!-- /.box-body -->
                <div class="box-footer">

                                    <div class="form-bottom-btn">
                                         <a href="{{url('admin/notices')}}" class="btn btn-primary custom-btn customb-btn"><span>Cancel</span></a>
                                        <button type="submit" class="btn btn-primary custom-btn customc-btn"><span class="fa fa-save" role="presentation" aria-hidden="true"></span> &nbsp;
                                        <span data-value="{{ trans('common.save') }}">{{ trans('common.save') }}</span></button>
                            </div>
                  <!--   <a href="{{url('admin/notice_templates')}}" class="btn btn-primary custom-btn customb-btn"><span>Cancel</span></a>
                    <button type="submit" class="btn btn-success">
                        <span class="fa fa-save" role="presentation" aria-hidden="true"></span> &nbsp;
                        <span data-value="{{ trans('common.save') }}">{{ trans('common.save') }}</span>
                    </button>
 -->
                </div>

            </div><!-- /.box -->
        </form>
    </div>
</div>

@endsection
@section('frontend_js')
<script type='text/javascript'>

    $(document).ready(function () {
        $('#all_notices').find('option').remove().end().append('<option value="">Select Notice</option>');

        /* $.ajax({
         type: "get",
         url: "{{url('admin/notices/get_all_notices/1')}}",
         headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         },
         success: function (response) {
         
         //  console.log(response);
         var notice_name = '';
         $.each(response, function (e, val) {
         
         if (val.state_name != null) {
         notice_name = val.name + '(' + val.state_name + ')';
         } else {
         notice_name = val.name;
         }
         
         $("#all_notices").append("<option value='" + val.id + "'>" + notice_name + "</option>");
         $("#all_notices").selectpicker("refresh");
         });
         
         }
         });*/
        $('.notice_type').on('change', function () {
            $('#all_notices').find('option').remove().end().append('<option value="">Select Notice</option>');

            var url_type = "<?php echo url(''); ?>" + "/admin/notices/get_all_notices/";
            $.ajax({
                type: "get",
                url: url_type + $('input[name=type]:checked').val(),
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                    console.log(response);
                    $("#all_notices").val('');
                    $("#all_notices").selectpicker('refresh');
                    //  console.log(response);
                    var notice_name = '';
                    $.each(response, function (e, val) {

                        if (val.state_name != null) {
                            notice_name = val.name + '(' + val.state_name + ')';
                        } else {
                            notice_name = val.name;
                        }

                        $("#all_notices").append("<option value='" + val.id + "'>" + notice_name + "</option>");

                    });
                    $("#all_notices").selectpicker("refresh");

                }
            });

        });
        $('.notice_type').trigger('change');
    });
    //disable allow cyo
    /*$('#secondary_doc').on('click', function () {
        $(".allow_cyo").css("display","none")

        //$('').display: none;
    });*/
    $(document).ready(function() {
       $('input[type="radio"]').click(function() {
           if($(this).attr('id') == 'secondary_doc') {
                $('.allow_cyo').hide(); 
                $('#enable_allow_cyo').val(0);

            }

           else {
                $('.allow_cyo').show();  
                $('#enable_allow_cyo').val(1);
               
           }
       });
    });

</script>
@endsection