@extends('adminlte::page')

@section('content_header')
<h1>
    <span class="text-capitalize">{{ trans('admin.notices') }}</span>
    <small>{{ trans('backpack::crud.edit').' '.trans('admin.notice') }}.</small>
</h1>
<ol class="breadcrumb">
    <li><a href="{{ url(config('backpack.base.route_prefix'),'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>
    <li><a href="{{ url('admin/notices') }}" class="text-capitalize">{{ trans('admin.notices') }}</a></li>
    <li class="active">{{ trans('backpack::crud.edit') }}</li>
</ol>
@endsection

@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <!-- Default box -->
        @can ('admin-notices')
        <a href="{{ url('admin/notices') }}"><i class="fa fa-angle-double-left"></i> {{ trans('backpack::crud.back_to_all') }} <span>{{ trans('admin.notices') }}</span></a><br><br>
        @endcan

        <form method="post" action="{{ url('admin/notices/'.$notice->getKey()) }}" data-parsley-validate="" > 
            {!! csrf_field() !!}
            {!! method_field('PUT') !!}
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ trans('backpack::crud.edit') }} {{ trans('admin.notice') }}</h3>
                </div>
                <div class="box-body">

                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <label class="control-label" for="{{ trans('common.name') }}">{!! $errors->has('name') ? '<i class="fa fa-times-circle-o"></i> ' : '' !!}{{ trans('common.name') }}</label>
                        <input type="text" name="name" maxlength="70" class="form-control"  data-parsley-required="true" data-parsley-maxlength="70" data-parsley-required-message="Name is required" data-parsley-trigger="change focusout" value="{{ $notice->name }}">
                        @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
                        <label class="control-label" for="{{ trans('common.type') }}">{!! $errors->has('type') ? '<i class="fa fa-times-circle-o"></i> ' : '' !!}{{ trans('common.type') }}</label>
                        <div class="radio">
                            <label>
                                <input type="radio" name="type" disabled="" value="1" {{ ($notice->type == 1)?'checked':'' }}>
                                {{ trans('admin.hard_doc') }}
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="type" disabled=""value="2" {{ ($notice->type == 2)?'checked':'' }}>
                                {{ trans('admin.soft_doc') }}
                            </label>
                        </div>
                         <div class="radio">
                            <label>
                                <input type="radio" name="type" value="3" class='notice_type' {{ ($notice->type == 3)?'checked':'' }}>
                                {{ trans('admin.secondary_doc') }}
                            </label>
                        </div>
                        <!--					<div class="radio">
                                                                        <label>
                                                                                <input type="radio" name="type" value="3" {{ ($notice->type == 2)?'checked':'' }}>
                                                                                {{ trans('admin.secondary_doc') }}
                                                                        </label>
                                                                </div>-->
                        @if ($errors->has('type'))
                        <span class="help-block">
                            <strong>{{ $errors->first('type') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('state_id') ? 'has-error' : '' }}">
                        <label class="col-md-12 control-label" for="textinput">State</label>
                        <select name="state_id" disabled="" class="selectpicker" data-parsley-required="true"   data-parsley-required-message="State is required" data-parsley-trigger="change focusout">
                            <option value="">Select Notice</option>
                            @foreach($states as $state)
                            <option value="{{$state->id}}" {{$state->id==$notice->state_id ? 'selected="selected"' :' '}}> {{$state->name}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('state_id'))
                        <p class="help-block">
                            <strong>{{ $errors->first('state_id') }}</strong>
                        </p>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('allow_cyo') ? 'has-error' : '' }}">
                        <label class="control-label" for="{{ trans('common.allow_cyo') }}">{!! $errors->has('allow_cyo') ? '<i class="fa fa-times-circle-o"></i> ' : '' !!}{{ trans('common.allow_cyo') }}</label>
                        <div class="radio">
                            <label>
                                <input type="radio" name="allow_cyo" value="1" {{ ($notice->allow_cyo == 1)?'checked':'' }} >
                                {{ trans('admin.allow_cyo_enable') }}
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="allow_cyo" value="0" {{ ($notice->allow_cyo == 0)?'checked':'' }} >
                                {{ trans('admin.allow_cyo_disable') }}
                            </label>
                        </div>

                        @if ($errors->has('allow_cyo'))
                        <span class="help-block">
                            <strong>{{ $errors->first('allow_cyo') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('allow_deadline_calculator') ? 'has-error' : '' }}">
                        <label class="control-label" for="{{ trans('common.allow_deadline_calculator') }}">{!! $errors->has('allow_deadline_calculator') ? '<i class="fa fa-times-circle-o"></i> ' : '' !!}{{ trans('common.allow_deadline_calculator') }}</label>
                        <div class="radio">
                            <label>
                                <input type="radio" name="allow_deadline_calculator" value="1" <?php echo ($notice->allow_deadline_calculator == '1') ? 'checked' : '' ?>>
                                {{ trans('admin.allow_deadline_enable') }}
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="allow_deadline_calculator" value="0" <?php echo ($notice->allow_deadline_calculator == '0') ? 'checked' : '' ?> >
                                {{ trans('admin.allow_deadline_disable') }}
                            </label>
                        </div>

                        @if ($errors->has('allow_deadline_calculator'))
                        <span class="help-block">
                            <strong>{{ $errors->first('allow_deadline_calculator') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group {{ $errors->has('is_claim_of_lien') ? 'has-error' : '' }}">
                        <label class="control-label" for="{{ trans('common.is_claim_of_lien') }}">{!! $errors->has('is_claim_of_lien') ? '<i class="fa fa-times-circle-o"></i> ' : '' !!}{{ trans('common.is_claim_of_lien') }}</label>
                        <div class="radio">
                            <label>
                                <input type="radio" name="is_claim_of_lien" value="1" <?php echo ($notice->is_claim_of_lien == '1') ? 'checked' : '' ?>>
                                {{ trans('admin.is_claim_of_lien_enable') }}
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="is_claim_of_lien" value="0" <?php echo ($notice->is_claim_of_lien == '0') ? 'checked' : '' ?> >
                                {{ trans('admin.is_claim_of_lien_disable') }}
                            </label>
                        </div>

                        @if ($errors->has('is_claim_of_lien'))
                        <span class="help-block">
                            <strong>{{ $errors->first('is_claim_of_lien') }}</strong>
                        </span>
                        @endif
                    </div>

                </div><!-- /.box-body -->

                <div class="box-footer">
                    <div class="form-bottom-btn">
                    <a href="{{url('admin/notices')}}" class="btn btn-primary custom-btn customb-btn"><span>Cancel</span></a>
                    <button type="submit" class="btn btn-primary custom-btn customc-btn">
                        <span class="fa fa-save" role="presentation" aria-hidden="true"></span> &nbsp;
                        <span data-value="{{ trans('common.save') }}">{{ trans('common.save') }}</span>
                    </button>
                </div>
                </div><!-- /.box-footer-->
            </div><!-- /.box -->
        </form>
    </div>
</div>
@endsection
