@extends('adminlte::page')



@section('content')
<section class="address-book">
    <div class="dashboard-wrapper">
        @if(Session::get('success'))
        <div class="no-margin alert alert-success">{{ Session::get('success') }}</div>
        @endif
        @if(Session::has('error'))
        <div class="no-margin alert alert-danger">{{ Session::get('error') }}</div>
        @endif
        <div class="dashboard-heading">
            <h1><span>Notice</span></h1>
        </div>
        <div class="dashboard-inner-body">
            <div class="box-header with-border">

                @can('admin-notices-create')
                <div class="col-md-6 col-sm-6">
                    <a href="{{ url('admin/notices/create') }}" class="btn btn-default ladda-button" data-style="zoom-in"><span class="ladda-label"><i class="fa fa-plus"></i> {{ trans('backpack::crud.add') }} {{ trans('admin.notice') }}</span></a>
                </div>
                @endcan
                <div class="col-md-6 col-sm-6">
                    <form class="form"  id="statewise_notice" action="{{url('admin/notices')}}" method="get">
                        {!! csrf_field() !!}
                        <div class="select-opt">
                            <?php $old_state = isset($request['state']) ? $request['state'] : '' ?>
                            <select name="state" class="selectpicker" id="allstates">
                                <option value="">Select</option>
                                @foreach($states as $each_state)
                                <option value="{{$each_state->id}}" {{$old_state == $each_state->id ?'selected="selected"':''}}> {{ucfirst(trans($each_state->name))}}</option>
                                @endforeach
                            </select>

                        </div>

                    </form>
                </div>
            </div>


            @if($notices->count() > 0)
            <div class="dahboard-table table-responsive fixhead-table">
                <table class="table table-striped" id="datatable1">
                    <thead class="thead-dark">
                        <tr>
                            <th>{{ trans('common.sn') }}</th>
                            <th>{{ trans('admin.notice') }}</th>
                            <th>{{ trans('common.type') }}</th>
                            <th>{{ trans('common.state') }}</th>
                            <th>{{ trans('common.status') }}</th>
                            <th>{{ trans('common.options') }}</th>
                        </tr>
                    </thead>
                    @foreach($notices as $index => $notice)
                    <tr>
                        <td>{{ $index+1 }}</td>
                        <td>{{ $notice->name }}</td>
                        <td>{{ $notice->display_type }}</td>
                        <td>{{ $notice->state->name }}</td>
                        <td>
                            <span class="label {{ ($notice->status == 1)?'label-success':'label-danger' }}">{{ $notice->display_status }}</span>
                        </td>

                        <td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-xs">Action</button>
                                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                    <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <ul class="dropdown-menu" role="menu">
                                    @can('admin-notices-edit')
                                    <li><a href="{{ url('admin/notices/'.$notice->getKey().'/edit') }}"><i class="fa fa-edit"></i> {{ trans('backpack::crud.edit') }}</a></li>
                                    @endcan
                                    <li><a href="javascript:void(0);" onclick="changeStatus('{{ $notice->getKey() }}', '{{ ($notice->status == 1)?'Inactive':'Active' }}')" ><i class="fa fa-edit"></i> {{ trans('common.make') }} {{ ($notice->status == 1)?'Inactive':'Active' }}</a></li>
                                    <li class="divider"></li>
                                    <li><a href="{{ url('admin/notices/'.$notice->getKey().'/'.$notice->state_id) }}"><i class="fa fa-navicon"></i> {{ trans('admin.manage_forms') }}</a></li>
                                    {{--
								<li><a href="#">Something else here</a></li>
								--}}
                                </ul>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
            {{-- $notices->links() --}}
            @else
            <h4 class="text-center">{{ trans('common.not_records') }}</h3>
                @endif


        </div>
    </div>
</section>
@endsection

@section('frontend_js')
    <script type="text/javascript">
        $('#allstates').on('change', function(){
        $('#statewise_notice').submit();
        });
        // Change status of selected record from list
        function changeStatus(id, status){

        var result = confirm("Are you sure you want to change status to " + status + "?");
        if (!result){
        return false;
        }

        $.ajax({
        type:'POST',
                url: "{{route('admin.notices.status')}}",
                data : {"_token":"{{ csrf_token() }}", id:id},
                cache: false,
                success:function(res){
                location.reload();
                },
                error:function(xhr, status, error) {
                new PNotify({
                // title: 'Regular Notice',
                text: '{{ trans('backpack::base.error_saving') }}',
                        type: "error",
                        icon: false
                });
                }
        });
        };
        $(document).ready(function () {
        $('#datatable1').DataTable({
            order: [[0, 'desc' ]],
        pageResize: true, // enable page resize
                processing: true,
                'fixedHeader': {
                'header': true,
                        'footer': true
                },
            dom: "<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-6'p>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
        });
        });
    </script>
    @endsection