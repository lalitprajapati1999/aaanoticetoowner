<div class="action-icon">
     <a href="{{url('admin/testimonial/view/'.$testimonial->id)}}" class="btn btn-secondary item red-tooltip" data-toggle="tooltip" data-placement="bottom" title="View"><span class="icon-dark-eye"></span></a>
    <a href="{{url('admin/testimonial/edit/'.$testimonial->id)}}" class="btn btn-secondary item red-tooltip" data-toggle="tooltip" data-placement="bottom" title="Edit"><span class="icon-pencil-edit-button"></span></a>
   <!--  <a href="#notesModal" onclick="addNotes({{$testimonial->id}})" data-toggle="modal" class="btn btn-secondary item red-tooltip trigger-btn"  data-toggle="tooltip" data-placement="bottom" title="Note"><span class="icon-contract"></span></a> -->
   <a href="#" data-target="#attachment-confirmation-modal" onclick="removeTestimonial(<?php echo $testimonial->id; ?>)" 
           data-toggle="modal" class="btn btn-secondary item red-tooltip" data-toggle="tooltip" data-placement="bottom" data-original-title="Remove"><span class="icon-cross-remove-sign"></span></a>
   <!--  <a href="{{url('admin/testimonial/'.$testimonial->id)}}" class="btn btn-secondary item red-tooltip" data-toggle="tooltip" data-placement="bottom" data-original-title="Remove"><span class="icon-cross-remove-sign"></span></a> -->
    @if($testimonial->status == '0')
    <a href="#" class="btn btn-secondary item red-tooltip" onclick="changeStatus({{$testimonial->id}},1)" data-toggle="tooltip" data-placement="bottom" title="Disable"><span class="icon-on disable"></span></a>
    @else
    <a href="#" class="btn btn-secondary item red-tooltip" data-toggle="tooltip" onclick="changeStatus({{$testimonial->id}},0)" data-placement="bottom" title="Enable"><span class="icon-on"></span></a>
    @endif
</div>