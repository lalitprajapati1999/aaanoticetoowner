@extends('adminlte::page')

@section('content')
<section class="work-order">
    <div class="dashboard-wrapper">
        <div class="dashboard-heading">
            <h1><span>Update Testimonial</span></h1>
        </div>

        <div class="dashboard-inner-body">
            <!--data-parsley-validate=""-->
            <form id="" data-parsley-validate="" class="form" action="{{ url('admin/testimonial/update') }}" method="post" enctype="multipart/form-data">
                {!! csrf_field() !!}
                @if (Session::get('success'))
                <div class="no-margin alert alert-success">
                   <?php echo Session::get('success'); ?>
                </div>
                @endif
                @if(session()->has('error'))
                <div class="no-margin alert alert-danger">
                    <?php echo Session::get('error'); ?>
                </div>
                @endif
                <div class="project-wrapper">
                    <div class="dash-subform">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="row">
                                    <!--                                    <div class="col-md-8 col-sm-8">
                                                                            <div class="input-wrapper full-width">
                                                                                <div class="styled-input">
                                                                                    <input type="text" name="content" value="{{ old('content')?old('content'):$testimonial->content}}" class="form-control" required data-parsley-required-message="Content is required" data-parsley-trigger="change focusout" data-parsley-pattern="/^[a-zA-Z ]*$/">
                                                                                    <label>Content<span class="mandatory-field">*</span></label>
                                                                                    <span></span>
                                                                               
                                                                                @if ($errors->has('content'))
                                                                                <p class="help-block">
                                                                                    <strong>{{ $errors->first('content') }}</strong>
                                                                                </p>
                                                                                @endif
                                                                                 </div>
                                                                            </div>
                                                                        </div>-->
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="row">
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="input-wrapper full-width">
                                                        <label class="col-md-12 control-label">Content<span class="mandatory-field">*</span></label>
                                                         <div class="row">
                                                        <div class="col-md-12">
                                                            <textarea name="content"  id="testimonial" rows="30" cols="50"  data-parsley-required="true" data-parsley-required-message='Content is required' data-parsley-trigger="change focusout" placeholder="Content">{{ old('content')?old('content'):$testimonial->content}}</textarea>

                                                            @if ($errors->has('content'))
                                                            <p class="help-block">
                                                                <strong>{{ $errors->first('content') }}</strong>
                                                            </p>
                                                            @endif
                                                        </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="row">
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="input-wrapper full-width">
                                                        <div class="styled-input">
                                                            <input type="text" name="client_name" value="{{ old('client_name')?old('client_name'):$testimonial->client_name}}" class="form-control" required data-parsley-required-message="Email is required" data-parsley-trigger="change focusout"/>
                                                            <label>Client Name<span class="mandatory-field">*</span></label>
                                                            <span></span>

                                                            @if ($errors->has('client_name'))
                                                            <p class="help-block">
                                                                <strong>{{ $errors->first('client_name') }}</strong>
                                                            </p>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 
                                        </div>
                                    </div>
<!--                                    @if($testimonial->image!='')
                                      <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="form-group">
                                            <div class="readonly full-width">
                                                <label class="full-width">Profile Image</label>
                                                <p class="full"><img src="{{ asset('images/'.$testimonial->image) }}"></p>
                                            </div>
                                        </div>
                                    </div>
                                      </div>
                                    @endif-->
                                    <!--                                    <div class="col-md-6 col-sm-6">
                                                                            <div class="form-group">
                                                                                <div class="readonly full-width">
                                                                                    <label class="full-width">Profile Image</label>
                                                                                    <input type="file" class="form-control" name="image" />
                                                                                </div>
                                                                            </div>
                                                                        </div>-->
                                    <div class="col-md-8 col-sm-8">

                                        <div class="form-btn-div doc_file">
                                            <span class="btn btn-default btn-file upload-btn btn btn-primary form-btn" id="temp">
                                                <!--data-parsley-required="true" data-parsley-required-message="File is required" data-parsley-trigger="change focusout"-->
                                                Choose File to Upload  <input type="file" name="image" >
                                            </span>
                                             (Allowed Types:jpeg,png,jpg,gif,svg)
                                             @if ($errors->has('image'))
                                                            <p class="help-block">
                                                                <strong>{{ $errors->first('image') }}</strong>
                                                            </p>
                                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12">
                                <div class="row">
                                    <div class="form-bottom-btn">
                                        <input type="hidden" name="testimonial_id" value="{{$testimonial->id}}"/>
                                        <a href="{{ url('admin/testimonial') }}" class="btn btn-primary custom-btn customb-btn"><span>{{ trans('backpack::base.cancel') }}</span></a>
                                        <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>Submit</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
</section>
@endsection
@section('frontend_js')
    <script src="{{ asset( ((config('app.env') == 'local') ? '' : 'public').'/vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset( ((config('app.env') == 'local') ? '' : 'public').'/vendor/unisharp/laravel-ckeditor/adapters/jquery.js') }}"></script>
<script>
$('#testimonial').ckeditor();
</script>
@stop