@extends('adminlte::page')
@section('content')
<section class="address-book-add-readonly">
    <div class="dashboard-wrapper">
        <div class="dashboard-heading">
            <h1><span>View Testimonial Details</span></h1>
        </div>
        <div class="dashboard-inner-body">
            <div class="dash-subform">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <div class="readonly full-width">
                                        <label class="full-width">Content</label>
                                        <p class="full-width">{!! $testimonial->content !!}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">Client Name</label>
                                <p class="full-width">{{$testimonial->client_name}}</p>
                            </div>
                        </div>
                    </div>
                    
                    @if(isset($testimonial->image) && $testimonial->image != '')
                      <div class="col-md-12 col-sm-12">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">Profile Image</label>
                                <p class="full-width"><img src="{{ asset('images/'.$testimonial->image) }}" width="200px" class="img-circle"></p>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
            <div class="col-md-12 col-sm-12">
                <div class="row">
                    <div class="form-bottom-btn">
                        <a href="{{URL::previous()}}" class="simple-hrf"><span>Back</span></a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
@endsection