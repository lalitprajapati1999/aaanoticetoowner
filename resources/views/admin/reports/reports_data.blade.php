@extends('adminlte::page')
<style>
    .display-if{
        display: inline-flex;
    }
    .to-mt{
        top:43px;
    }
    .date-mt{
     top: -9px;
    }

</style>
@section('content')

<div>
    <section class="address-book reports">
        <div class="dashboard-wrapper">

            @if (Session::get('success'))
            <div class="alert alert-success">
               <?php echo Session::get('success'); ?>
            </div>
            @endif
            <div class="employee-status"></div>
            <div class="dashboard-heading">
                <h1><span>Reports</span></h1>
            </div>

            <div class="dashboard-inner-body">
                <div class="row">
                    <form role="form" method="post" id="search-data" class="full-width">
                        <div class="col-md-7 col-sm-10 col-xs-10">
                            <div class="search-by">
                                <div class="form-group full-width">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-xs-6 report_type_html">
                                            <div class="select-opt full-width pull-left">
                                                <label for="" class="display-ib">Reports Type: </label>
                                                <select class="selectpicker" name="report_type" class="report_type" id="report_type">
                                                    <!--                                                <option value="">Select</option>-->
                                                    <option value="daily">Daily</option>
                                                    <option value="weekly">Weekly</option>
                                                    <option value="monthly">Monthly</option>
                                                    <option value="yearly">Yearly</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6 datewise_html "></div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <div class="select-opt full-width pull-left">
                                                <label for="" class="display-ib">Account Manager: </label>
                                                <select class="selectpicker name" name="account_manager" id="name">
                                                    <option value="all">All</option>
                                                    @if(isset($account_managers) && count($account_managers)>0)
                                                    @foreach($account_managers as $manager)
                                                    <option value="{{$manager->id}}">{{$manager->name}}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1 col-sm-1 col-xs-1">
                            <span class="form-group-btn">
                                <button class="btn btn-search" id="search_info" type="submit"><span class="icon-search"></span></button>
                            </span>
                        </div>
                    </form>
                    <form id="reportstable1" action ="{{url('admin/reports/exportExcel')}}" method="POST" class="full-width">
                        {!! csrf_field() !!}
                        <div class="col-md-5 col-md-offset-2 text-right pull-right">
                            <div class="add-new-btn display-ib">
<!--                                <button  type="submit" class="btn btn-primary custom-btn"><span>Email</span></button>-->
                                <!--<a href="#mailModal" id="sendReportEmail"  data-toggle="modal" class="btn btn-primary custom-btn" data-toggle="tooltip" data-placement="bottom" title="Email"><span>Email</span></a>-->
                            </div>
                            <div id="searchdata"></div>
                            <div class="add-new-btn display-ib">
                                <button  name="reporttype"  id="printdata" value="printdata" class="btn btn-primary custom-btn"><span>Print</span></button>
                            </div>
                            <div class="add-new-btn display-ib">
                                <button  type="submit" name="reporttype" value="reportexceldata" id="reportexceldata1" class="btn btn-primary custom-btn"><span>Export to Excel</span></button>
                            </div>
                        </div>
                    </form>

                </div>
                <div class="dahboard-table table-responsive overflow-visi fixhead-table">
                    <table class="table table-striped" id="report-data" datatable="" width="100%" cellspacing="0"   data-scroll-x="true"scroll-collapse="false">
                        <thead class="thead-dark">
                            <tr>
                               <!-- <th scope="col" class="action"><input class="chk" name="select_all" id="select-all" type="checkbox">Select All</th> <!-- value="1"-->                                <th scope="col" class="action">Account Manager</th>
                                <th scope="col" class="action">Request</th>
                                <th scope="col" class="action">Processing</th>
                                <th scope="col" class="action">Completed</th>
                                <th scope="col" class="action">Mailed</th>
                                <th scope="col" class="action">Cancelled</th>
                                <th scope="col" class="action">Goal</th>
                                <th scope="col" class="action">Total</th>
                            </tr>
                        </thead>

                    </table>

                </div>

            </div>
        </div>
    </section>
</div>
<!-- Send Email Modal-->
<form method="post" data-parsley-validate="" action="{{ url('admin/reports/sendMailExcel') }}">
    {!! csrf_field() !!}
    <div id="mailModal" class="modal fade register-modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Send Email</h4>	
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <div class="input-wrapper full-width">
                          <div id="searchmaildata"></div>
                        <div class="styled-input">
                            <input type="text" name="email" value="" class="form-control" data-parsley-required="true" data-parsley-required-message="Email is required">
                            <label>Email</label>
                            <span></span>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="form-bottom-btn">
                        <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>Send Email</span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<div id='DivIdToPrint'>
</div>
@stop
@section('frontend_js')
<script src="{{ asset('vendor/adminlte/plugins/datepicker/bootstrap-datepicker.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('js/buttons.html5.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/buttons.print.min.js')}}"></script>
    <script src="https://printjs-4de6.kxcdn.com/print.min.js"></script>

 <link rel="stylesheet" type="text/css" href="https://printjs-4de6.kxcdn.com/print.min.css">
<script type="text/javascript">
//$('#name').on('change', function () {
//    if ($(this).val() == 'all') {
//        location.reload();
//    } else {
//        // otable.search(this.value).draw();
//        rtable.fnFilter(this.value, 1, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true);
//    }
//});
$('body').on('focus', ".datepicker", function () {
    $(this).datepicker();

});
//
// Updates "Select all" control in a data table
//
function updateDataTableSelectAllCtrl(table) {
    var $table = table.table().node();
    var $chkbox_all = $('tbody input[type="checkbox"]', $table);
    var $chkbox_checked = $('tbody input[type="checkbox"]:checked', $table);
    var chkbox_select_all = $('thead input[name="select_all"]', $table).get(0);

    // If none of the checkboxes are checked
    if ($chkbox_checked.length === 0) {
        chkbox_select_all.checked = false;
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = false;
        }

        // If all of the checkboxes are checked
    } else if ($chkbox_checked.length === $chkbox_all.length) {
        chkbox_select_all.checked = true;
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = false;
        }

        // If some of the checkboxes are checked
    } else {
        chkbox_select_all.checked = true;
        if ('indeterminate' in chkbox_select_all) {
            chkbox_select_all.indeterminate = true;
        }
    }
}
$(document).ready(function () {

    var rows_selected = [];

    var reportsTable = $('#report-data').DataTable({
        // "pageLength":10
        processing: true,
        serverSide: true,
        bPaginate: true,
//        sPaginationType: 'full_numbers',
        ajax: {
            url: "{{url('admin/reports/custom-filter')}}",
            data: function (d) {
                d.report_type = $('#report_type').val();
                d.acc_name = $('#name').val();
                d.from_date = $('#report_from_date').val();
                d.to_date = $('#report_to_date').val();
                d.month = $('#month').val();
                d.year = $('#year').val();
                d.year_only = $('#year_only').val();
            }
        },
        columns: [
            /*  {data: 'account_manager_id', render: function (data, id, row) {
             return '<input type="checkbox" name="acc_id[]" class="chk" value="' + $('<div/>').text(data).html() + '"></input>';
             }},*/
            {data: 'name', render: function (data, id, row) {
                    return data;
                }},
            {data: 'request_count', name: 'request_count'},
            {data: 'processing_count', name: 'processing_count'},
            {data: 'completed_count', name: 'completed_count'},
            {data: 'mailed_count', name: 'mailed_count'},
            {data: 'cancelled_count', name: 'cancelled_count'},
            {data: 'goal', name: 'goal'},
            {data: 'total', name: 'total'},
        ]
                //  select:true

    });


    $('#search-data').on('submit', function (e) {
        reportsTable.draw();
        e.preventDefault();
    });
    // Handle click on checkbox
    $('#report-data tbody').on('click', 'input[type="checkbox"]', function (e) {
        var $row = $(this).closest('tr');

        // Get row data
        var data = reportsTable.row($row).data();

        // Get row ID
        var rowId = data[0];

        // Determine whether row ID is in the list of selected row IDs 
        var index = $.inArray(rowId, rows_selected);

        // If checkbox is checked and row ID is not in list of selected row IDs
        if (this.checked && index === -1) {
            rows_selected.push(rowId);

            // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
        } else if (!this.checked && index !== -1) {
            rows_selected.splice(index, 1);
        }

        if (this.checked) {
            $row.addClass('selected');
        } else {
            $row.removeClass('selected');
        }

        // Update state of "Select all" control
        updateDataTableSelectAllCtrl(reportsTable);

        // Prevent click event from propagating to parent
        e.stopPropagation();
    });


    // Handle click on table cells with checkboxes
    $('#report-data').on('click', 'tbody td, th:first-child', function (e) {
        $(this).parent().find('input[type="checkbox"]').trigger('click');
    });
    // Handle click on "Select all" control
    $('input[name="select_all"]', reportsTable.table().container()).on('click', function (e) {
        if (this.checked) {
            $('#report-data tbody input[type="checkbox"]:not(:checked)').trigger('click');
        } else {
            $('#report-data tbody input[type="checkbox"]:checked').trigger('click');
        }

        // Prevent click event from propagating to parent
        e.stopPropagation();
    });

    $('#reportexceldata').on('click', function (e) {
        e.preventDefault();
        var form_action = "{{url('admin/reports/exportExcel')}}";
//        var chkArray = [];
//
//        $(".chk:checked").each(function () {
//            chkArray.push($(this).val());
//        });
//        var selected;
//        selected = chkArray.join(',');
//        if (selected.length == 0) {
//            alert("Please at least check one of the checkbox");
//        }
//        $(reportsTable.$('input[type="checkbox"]:checked').map(function () {
//            
//        }));
//
//        // Prevent actual form submission
//        e.preventDefault();
//search-data
        $.ajax({
            type: 'POST',
            url: form_action,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: "json",
            async: false,
            cache: false,
//                beforeSend: function () {
//                    $('#pleaseWait').modal('show');
//                },
//                complete: function () {
//                    $('#pleaseWait').modal('hide');
//                },
            data: $("#search-data").serialize(),
            success: function (response) {
                if (response.status == 'success') {
                    $.each(response.message, function (key, value) {
                        window.open(value);
                    });
                } else {
                    alert(response.message);
                }
            }
        });
    });

$('#printdata').on('click', function (e) {
        e.preventDefault();
        var form_action = "{{url('admin/reports/exportExcel')}}";
//       
        $.ajax({
            type: 'POST',
            url: form_action,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            dataType: "json",
            // async: false,
            // cache: false,
//                beforeSend: function () {
//                    $('#pleaseWait').modal('show');
//                },
//                complete: function () {
//                    $('#pleaseWait').modal('hide');
//                },
            data: {reporttype: 'printdata'},
            success: function (response) {
                     if (response.status == 'success') {
                                printJS({

                                    printable: response.message,
                                    type: 'pdf',
                                    onPrintDialogClose: closedWindow,
                                })
                                function closedWindow() {
                                        //location.reload();
                                    }
                            } else {
                                alert(response.message);
                            }
                }
        });
    });

 });
$('.datewise_html').html('');

$('#report_type').on('change', function () {
      $(".datewise_html").addClass("date-mt");

    if ($(this).val() == 'weekly') {
        var weekly_html = '<div class="calender-div display-if pull-left">' +
                '<div class="form-group">' +
                '<div class="input-wrapper full-width">' +
                '<div class="styled-input">' +
                '<input class="form-control datepicker" required="" type="text" name="from_date" id="report_from_date" autocomplete="off">' +
                '<span></span>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<p class="display-ib pull-left to-label col-md-2 to-mt">to</p>' +
                '<div class="form-group">' +
                '<div class="input-wrapper full-width">' +
                '<div class="styled-input">' +
                '<input class="form-control datepicker" required="" type="text" name="to_date" id="report_to_date" autocomplete="off">' +
                '<span></span>' +
                '</div>' +
                '</div>' +
                '</div>' ;
        $('.datewise_html').html(weekly_html);
    } else if ($(this).val() == 'monthly') {
      $(".datewise_html").removeClass("date-mt");

        var monthly_html = '<div class="select-opt full-width pull-left">' +
                '<select class="selectpicker" name="month" class="month" id="month">' +
                '<option value="">Select</option>' +
                '<option value="01">Jan</option>' +
                '<option value="02">Feb</option>' +
                '<option value="03">Mar</option>' +
                '<option value="04">Apr</option>' +
                '<option value="05">May</option>' +
                '<option value="06">June</option>' +
                '<option value="07">Jul</option>' +
                '<option value="08">Aug</option>' +
                '<option value="09">Sep</option>' +
                '<option value="10">Oct</option>' +
                '<option value="11">Nov</option>' +
                '<option value="12">Dec</option>' +
                '</select>' +
                '</div>'+
                '</div>';


        var minOffset = 0, maxOffset = 2; // Change to whatever you want
        var thisYear = (new Date()).getFullYear();
        var html = '';
        for (var i = minOffset; i <= maxOffset; i++) {
            var year = thisYear - i;
            html += '<option value="' + year + '">' + year + '</option>';
        }

        var select = $('<div class="select-opt full-width pull-left year_html"><select class="selectpicker" name="year" class="year" id="year"><option value="">Select</option>' + html);



        $('.datewise_html').html(monthly_html);
        $('.datewise_html').append(select);
        $('#month').selectpicker('refresh');
        $('#year').selectpicker('refresh');
    } else if ($(this).val() == 'yearly') {
        $(".datewise_html").removeClass("date-mt");
        var minOffset = 0, maxOffset = 2; // Change to whatever you want
        var thisYear = (new Date()).getFullYear();
        var html = '';
        for (var i = minOffset; i <= maxOffset; i++) {
            var year = thisYear - i;
            html += '<option value="' + year + '">' + year + '</option>';
        }

        var select_yr = $('<div class="select-opt full-width pull-left yr_html"><select class="selectpicker" name="year" class="year" id="year_only">' + html);



        $('.datewise_html').html(select_yr);
        $('#year_only').selectpicker('refresh');
    } else {
        $('.datewise_html').html('');
    }
});
$('#search_info').on('click', function () {
    $("#searchdata").html("");
    
    $.each($('#search-data').serializeArray(), function (i, field) {
        console.log(field);
        $("#searchdata").append('<input type="hidden" name="' + field.name + '" value="' + field.value + '">');
    });
});
$('#sendReportEmail').on('click',function(){
    $("#searchmaildata").html("");
    
    $.each($('#search-data').serializeArray(), function (i, field) {
        console.log(field);
        $("#searchmaildata").append('<input type="hidden" name="' + field.name + '" value="' + field.value + '">');
    });
});
$(document).ready(function () {
     $('#report_from_date').datepicker({
      format: "dd/mm/yyyy"
    }).on('change', function(){
        $('.datepicker').hide();
    });
});

</script>
@stop