
        <table class="table table-striped" id="report-data" datatable="" width="100%" cellspacing="0"   data-scroll-x="true"scroll-collapse="false">
            <thead class="thead-dark">
                <tr>
                    <th scope="col" class="action">Account Manager</th>
                    <th scope="col" class="action">Request</th>
                    <th scope="col" class="action">Processing</th>
                    <th scope="col" class="action">Completed</th>
                    <th scope="col" class="action">Mailed</th>
                    <th scope="col" class="action">Cancelled</th>
                    <th scope="col" class="action">Goal</th>
                    <th scope="col" class="action">Total</th>
                </tr>
            </thead>
            <tbody>
                @if(isset($user) && !empty($user))
                    @foreach($user AS $k=>$val)
                    <tr>
                        <td>{{(isset($val['name']) && $val['name']!= NULL)?$val['name']:'N/A'}}</td>
                        <td>{{(isset($val['request_count']))?$val['request_count']:'0'}}</td>
                        <td>{{(isset($val['processing_count']))?$val['processing_count']:'0'}}</td>
                         <td>{{(isset($val['completed_count']))?$val['completed_count']:'0'}}</td>
                        <td>{{(isset($val['mailed_count']))?$val['mailed_count']:'0'}}</td>
                        <td>{{(isset($val['cancelled_count']))?$val['cancelled_count']:'0'}}</td>
                        <td>{{(isset($val['goal']))?$val['goal']:'0'}}</td>
                        <td>{{(isset($val['total']))?$val['total']:'0'}}</td>
                    </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
<script type="text/javascript">
   window.print();
    </script>