@extends('adminlte::page')
@section('content')

<div>
    <section class="address-book">
        <div class="dashboard-wrapper">

            @if (Session::get('success'))
            <div class="alert alert-success">
             <?php echo Session::get('success'); ?>
            </div>
            @endif
            <div class="customer-status"></div>
            <div class="dashboard-heading">
                <h1><span>All Customer</span></h1>
            </div>

            <div class="dashboard-inner-body">
                <div class="row">
                    <div class="col-lg-6 col-md-8 col-sm-8 col-xs-12">
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-5">
                                <div class="form-group">
                                    <label for="">Filter By: </label>
                                </div>
                            </div>
                            <form role="form" method="post" id="customer-name">
                                <div class="col-lg-5 col-md-6 col-sm-6 col-xs-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Customer Name" name="customer_name" id="autocompleteCustomerName">
<!--                                        <span class="form-group-btn">
                                            <button class="btn btn-search" type="submit"><span class="icon-search"></span></button>
                                        </span>-->
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
                <div class="dahboard-table table-responsive fixhead-table">
                    <table class="table table-striped" id="customer-data" datatable="" width="100%" cellspacing="0"   data-scroll-x="true"scroll-collapse="false">
                        <thead class="thead-dark">
                            <tr>
                                <th class="id"> ID</th>
                                <th scope="col" class="">Customer Name</th>
                                <th scope="col" class="full-name">Company Name</th>
                                <th scope="col" class="">Mailing Address</th>
                                <th scope="col" class="">CYO</th>
                                <th scope="col" class="">Date Applied</th>
                                <th scope="col" class="action">Options</th>
                            </tr>
                        </thead>

                    </table>

                </div>
            </div>
        </div>
    </section>
</div>
<form action="" method="POST" class="active_deactivate_modal">
    {!! csrf_field() !!}
    <div id="activeDeactivateModal" class="modal fade">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="icon-box">
                        <i class="fa fa-close"></i>
                    </div>				
                    <h4 class="modal-title">Are you sure?</h4>	
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <p>Do you really want to active or deactive the customer?</p>
                </div>
                <div class="modal-footer">
                    <div class="form-bottom-btn">
                        <button type="button" class="btn btn-primary custom-btn customb-btn" data-dismiss="modal"><span>Cancel</span></button>
                        <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>Confirm</span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@section('frontend_js')
<!--<script type="text/javascript" src="{{ asset('js/jquery.js') }}"></script>-->
<script type="text/javascript" src="{{ asset('js/jquery-ui.min.js') }}"></script>
<script type="text/javascript">
$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var employeesTable = $('#customer-data').DataTable({
        // "pageLength":10
         order: [[0, 'desc' ]],
        "search": {
            "caseInsensitive": false
        },
        dom: "<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-6'p>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
        processing: true,
        serverSide: true,

        ajax: {
            url: "{{url('admin/customer/custom-filter')}}",
            data: function (d) {

                d.company_name = $('input[name=customer_name]').val();

            }
        },
        columns: [
            {data: 'id', name: 'id'},
           
            {data: 'name', name: 'customer_name'},
            {data: 'company_name', name: 'company_name'},
            {data: 'mailing_address', name: 'mailing_address'},
            {data: 'cyo', name: 'cyo'},
            {data: 'date_applied', name: 'date_applied'},
            {data: 'action', name: 'action'}
        ]

    });

    $('#customer-name').on('submit', function (e) {

        employeesTable.draw();
        e.preventDefault();
    });


});
//    function changeStatus(customerId, status) {
//        $.ajax({
//            data: {'customer_id': customerId, 'status': status},
//            type: "post",
//            url: "{{url('admin/customer/changestatus') }}",
//            headers: {
//                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//            },
//            success: function (response) {
//               
//                if (response.status == true) {
//                        var div = $('<div/>', {
//                            class: 'alert alert-success',
//                            title: 'now this div has a title!'
//                        });
//                        div.html(response.message);
//                        $(".customer-status").html(div);
//                        $('html, body').animate({
//                            'scrollTop': $("body").position().top
//                        });
////                        setTimeout(function () {
////                            location.reload();
////                        }, 2000);
//                    }
//            }
//        });
//    }
$(document).on('click', '.change-status', function () {
    var url = "{{url('admin/customer/changestatus') }}";
    var customerId = $(this).attr('data-id');
    var status = $(this).attr('data-status');

    $(".active_deactivate_modal").attr("action", url);
    $('body').find('.active_deactivate_modal').append('<input name="customer_id" type="hidden" value="' + customerId + '">');
    $('body').find('.active_deactivate_modal').append('<input name="_method" type="hidden" value="POST">');
    $('body').find('.active_deactivate_modal').append('<input name="status" type="hidden" value="' + status + '">');
});
$(document).ready(function () {
    src = "{{ url('admin/customer/searchajaxcustomername') }}";
//     $("#autocompleteCustomerName").autocomplete({
//         source: function (request, response) {
//             $.ajax({
//                 url: src,
//                 dataType: "json",
//                 data: {
//                     term: request.term
//                 },
//                 success: function (data) {
//                     response(data);

//                 },
//                 select: function (event, ui) {
//                     $("#autocompleteCustomerName").val(ui.item.id);
//                 }
//             });
//         },
// //        minLength: 3,

//     });
});
$(function () {
    otable = $('#customer-data').dataTable();
});
$('#autocompleteCustomerName').on('keyup keypress change', function () {
     otable.fnFilter(this.value);
  //  otable.fnFilter(this.value, 0, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true);
});
</script>
@endsection
