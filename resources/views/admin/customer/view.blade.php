@extends('adminlte::page')
@section('content')
<section class="address-book-add-readonly">
    <div class="dashboard-wrapper">
            @if(Session::get('success'))
                <div class="alert alert-success">
                   <?php echo Session::get('success'); ?>
                </div>
            @endif
            @if(Session::get('error'))
                <div class="alert alert-error">
                  <?php echo Session::get('error'); ?>
                </div>
            @endif
        <div class="dashboard-heading">
            <h1><span>Customer Details</span></h1>
        </div>
        <div class="dashboard-inner-body">
            <div class="dash-subform">

                <div class="row">
                    <h2>Login Details</h2>
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">Name</label>
                                <p class="full-width">{{$user_details->name}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">Email</label>
                                <p class="full-width">{{$user_details->email}}</p>
                            </div>
                        </div>
                    </div>
                    @if (!Auth::user()->hasRole('account-manager')) 

                    <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">Password</label>
                                <p class="full-width">{{$user_details->password_text}}</p>
                            </div>
                        </div>
                    </div>
                    @endif
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">Account Manager</label>
                                <p class="full-width">{{$customer->account_manager_name?$customer->account_manager_name:'N/A'}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="dash-subform">
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <div class="readonly full-width">
                                        <label class="full-width">Company Name</label>
                                        <p class="full-width">{{$customer->company_name}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">Contact Person</label>
                                <p class="full-width">{{$customer->contact_person}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="dash-subform">
                <div class="row">
                    <h2>Mailing Address</h2>

                    <div class="col-md-12 col-sm-12">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">Company Mailing address</label>
                                <p class="full-width">{{$customer->mailing_address}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">City</label>
                                <p class="full-width">{{$customer->mailing_city_name}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">State</label>
                                <p class="full-width">{{$customer->mailing_state_name}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">Zip Code</label>
                                <p class="full-width">{{$customer->mailing_zip}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="dash-subform">
                <div class="row">
                    <h2>Physical Address</h2>

                    <div class="col-md-12 col-sm-12">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">Company Physical Address</label>
                                <p class="full-width">{{$customer->physical_address}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">City</label>
                                <p class="full-width">{{$customer->physical_city_name}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">State</label>
                                <p class="full-width">{{$customer->physical_state_name}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">Zip Code</label>
                                <p class="full-width">{{$customer->physical_zip}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width"> {{ trans('adminlte::adminlte.hear_about') }}</label>
                                <p class="full-width">{{$customer->hear_about}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width"> {{ trans('adminlte::adminlte.office_number') }}</label>
                                <p class="full-width">{{$customer->office_number}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">   {{ trans('adminlte::adminlte.fax') }}</label>
                                <p class="full-width">{{$customer->fax_number}}</p>
                            </div>
                        </div>
                    </div>
                    <?php
                    if (!empty($customer->company_email)) {
                        $company_arr = explode(',', $customer->company_email);
                    }
                    ?>
                    @if(!empty($company_arr))
                    @foreach($company_arr AS $e=>$each_company_email)
                    <div class="col-md-8 col-sm-8">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">Company Email</label>
                                <p class="full-width">{{$each_company_email}}</p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @endif

                </div>
            </div>

            <div class="dash-subform">  

                <div class="row">
                    <h2>Officer/Director</h2>
                    @if(isset($agents) && count($agents)>0)
                    @foreach($agents AS $agent)
                    <div class="col-md-4 col-sm-4">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">First Name</label>
                                <p class="full-width">{{$agent->first_name}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">Last Name</label>
                                <p class="full-width">{{$agent->last_name}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">Title</label>
                                <p class="full-width">{{$agent->title}}</p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @endif
                </div>
            </div>
          
                @if(isset($representatives) && count($representatives)>0)
                  <div class="dash-subform">  
                <div class="row">
                    <h2>Representative</h2>
                </div>
                @foreach($representatives AS $each_representative)
                <div class="row">

                    <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">Contact Person</label>
                                <p class="full-width">{{$each_representative->contact_person}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">Position</label>
                                <p class="full-width">{{$each_representative->branch_name}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">Email Address</label>
                                <p class="full-width">{{$each_representative->email}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">Mobile Number</label>
                                <p class="full-width">{{$each_representative->mobile_number}}</p>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                 </div>
                @endif

            @if(isset($customer->no_of_offices) && $customer->no_of_offices != NULL)
            <div class="dash-subform">
                <div class="row">


                    <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">How many branches throughout the united states?</label>
                                <p class="full-width">{{$customer->no_of_offices}}</p>
                            </div>
                        </div>
                    </div>
                </div>   
            </div>
             @endif

            @if(isset($customer_subscription) && $customer_subscription != NULL)
            <div class="dash-subform">
                <div class="row">
                    <h2>Subscription Details</h2>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">Subscribed Package</label>
                                <p class="full-width">{{$customer_subscription['name']}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">Amount</label>
                                <p class="full-width">${{$customer_subscription['charge']}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">Subscription Start Date</label>
                                <p class="full-width">{{date('d M Y',strtotime($customer_subscription['start_date']))}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">Subscription End Date</label>
                                <p class="full-width">{{date('d M Y',strtotime($customer_subscription['end_date']))}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">Subscription Mode</label>
                                <p class="full-width">{{($customer_subscription['type'] == '1')?'Card Payment':'Check'}}</p>
                            </div>
                        </div>
                    </div>
                    @if($customer_subscription['type'] == '2')
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">Check Details</label>
                                <p class="full-width">Check Number : {{$customer_subscription['cheque_no']}}</p>
                                <p class="full-width">Bank Details : {{$customer_subscription['bank_details']}}</p>                           
                            </div>
                        </div>
                    </div>
                    @endif
                    @if($customer_subscription['type'] == '2' && $customer_subscription['status'] == '0')
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <button class="btn custom-btn custom-c" data-toggle="modal" data-target="#myModal">Activate Subscription</button>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>   
            </div>
             @endif
            @if(isset($branches) && count($branches)>0)
            <div class="dash-subform">  
                <div class="row">
                    <h2>Branches</h2>
                </div>
                @foreach($branches AS $each_branch)
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">Branch Name</label>
                                <p class="full-width">{{$each_branch->name?$each_branch->name:'N/A'}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">Phone Number</label>
                                <p class="full-width">{{$each_branch->phone?$each_branch->phone:'N/A'}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">Contact Person</label>
                                <p class="full-width">{{$each_branch->contact_person?$each_branch->contact_person:'N/A'}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12">
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <div class="readonly full-width">
                                        <label class="full-width">Address</label>
                                        <p class="full-width">{{$each_branch->address?$each_branch->address:'N/A'}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">City</label>
                                <p class="full-width">{{$each_branch->city?$each_branch->city:'N/A'}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">State</label>
                                <p class="full-width">{{$each_branch->state?$each_branch->state:'N/A'}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">Contact Person</label>
                                <p class="full-width">{{$each_branch->contact_person?$each_branch->zip:'N/A'}}</p>
                            </div>
                        </div>
                    </div>
                    <h2 class="mtb-20 full-width">Registered Agents</h2>
                    <div class="col-md-4 col-sm-4">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">First Name</label>
                                <p class="full-width">{{$each_branch->first_name?$each_branch->first_name:'N/A'}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">Last Name</label>
                                <p class="full-width">{{$each_branch->last_name?$each_branch->last_name:'N/A'}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">Title</label>
                                <p class="full-width">{{$each_branch->title?$each_branch->title:'N/A'}}</p>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach

            </div>
            @endif
            <div class="row">
                @if(!empty($user_cards))
                
                <h2>Saved Cards</h2>
                
                <div class="col-md-12 col-sm-12">
                    <div class="input-wrapper full-width">
                        <div class="styled-input">
                            @foreach($user_cards as $card)
                           <?php
                                $ico = 'credit-card';
                                if($ico == 'American Express')
                                    $ico = 'amex';
                                else
                                    $ico = 'cc-'.$card['cardType'];
                            ?>
                                <div class="col-md-4">
                                    <div class="thumbnail row">
                                        <div class="caption">
                                            <div class='col-lg-12'><h4>{{ucwords($card['name'])}}</h4>
                                            </div>
                                            <div class='col-lg-12 well well-add-card'>
                                                <h4>{{$card['number']}}
                                                    <span class="fa fa-{{$ico}} pull-right"></span>
                                                    <span class="pull-right">{{$card['expMonth']}}-{{$card['expYear']}}</span>
                                                </h4>
                                            </div>
<!--                                             <button type="button" class="btn btn-danger remove-card"  >Remove</button>
 -->                                            @if(date('Y')< $card['expYear'] && date('m') < $card['expMonth'])
                                                <span class='text-danger pull-right'>Expired</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                @endif
                </div>
           <!--  <div class="col-md-12 col-sm-12">

                <div class="row">
                    @if(!empty($user_cards))

                    <h2>Saved Cards</h2>
                        <div class="col-md-12 col-sm-12">
                            <div class="input-wrapper full-width">
                                <div class="styled-input">
                                @foreach($user_cards as $card)
                                    <div class="dashboard-text full-width"><p class="display-ib pull-left">{{$card['cardType']}} </p>
                                    <span class="display-ib pull-left">{{ $card['number']}}</span> </div>     
                                @endforeach
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div> -->
            <div class="col-md-12 col-sm-12">
                <div class="row">
                    <div class="form-bottom-btn">
                        <a href="{{URL::previous()}}" class="simple-hrf"><span>Back</span></a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Activate Subscription</h4>
      </div>
      <form method="POST" action="{{url('admin/customer/activate-subscription')}}">
      <div class="modal-body">
         {{csrf_field()}}
         <input type="hidden" name="customer_id" value="{{$customer->id}}">
         <p>Do you want to activate this customer for create your own?</p>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-success" id="yes_btn">Yes</button>
        <button type="button" class="btn btn-warning" data-dismiss="modal">No</button>
      </div>
      </form>
    </div>

  </div>
</div>
@endsection