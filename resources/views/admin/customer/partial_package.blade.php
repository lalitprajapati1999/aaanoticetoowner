
<?php
if ($package['id'] != '1') {
    $title = "Monthly";
    if ($package['id'] == "3") {
        $title = "Yearly";
    }
    ?>
    <div class="row">

        <div class="col-md-4 col-sm-4">
            <div class="input-wrapper full-width">
                <div class="styled-input">
                    <input type="text"  min="1" name="create_own_charge" class="form-control" data-parsley-required="true" data-parsley-required-message="Charges is required" data-parsley-trigger="change focusout" data-parsley-type="number" id="create_own_charge" value="{{ $package['charge'] }}" />
                    <label><?php echo($title) ?> Subscriptions Charge</label>
                    <span></span>
                    <p class="help-block errorsection" data-name="create_own_charge" data-index=""></p>
                </div>
            </div>
        </div>
        <div class="col-md-5 col-sm-5">
            <div class="input-wrapper full-width">
                <div class="styled-input">
                    <textarea name="description" class="form-control" data-parsley-required="true" data-parsley-required-message="description is required" data-parsley-trigger="change focusout">{{ $package['description'] }}</textarea>
                    <label>Description</label>
                    <span></span>
                    <p class="help-block errorsection" data-name="description" data-index=""></p>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<?php foreach ($notices as $notices_val) { ?>

    <div class="row">
        <div class="col-md-12">
            <h3 class="" for="textinput">Notice : <?php echo $notices_val['name'] ?></h3>
            <input type="hidden" name="notice[{{$notices_val['id']}}][notice_id]" value="{{$notices_val['id']}}">
        </div>
    </div>
    <div class="full-width">
        <div class="col-md-3 col-sm-6">
            <div class="input-wrapper full-width">
                <div class="styled-input">
                    <label>Description</label>
                </div>
            </div>
        </div>
        <?php if ($notices_val['type']==2 && $package['id'] == '1') { ?>
            <div class="col-md-2 col-sm-6">
                <div class="input-wrapper full-width">
                    <div class="styled-input">
                        <label class="">From<span class="mandatory-field">*</span></label>
                        <span></span>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-6">
                <div class="input-wrapper full-width">
                    <div class="styled-input">
                        <label class="">To<span class="mandatory-field">*</span></label>
                        <span></span>
                    </div>
                </div>
            </div>
        <?php } ?>
        <?php
        if($notices_val['type'] == 1  ){
            if($notices_val['master_notice_id'] == config('constants.MASTER_NOTICE')['COL']['ID'] || $notices_val['master_notice_id'] == config('constants.MASTER_NOTICE')['BCOL']['ID']){?>
               <div class="col-md-2 col-sm-6">
                        <div class="input-wrapper full-width">
                            <div class="styled-input">
                                <label class="">From Unpaid Amount <span class="mandatory-field">*</span></label>
                                <span></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 col-sm-6">
                        <div class="input-wrapper full-width">
                            <div class="styled-input">
                                <label class="">To Unpaid amount<span class="mandatory-field">*</span></label>
                                <span></span>
                            </div>
                        </div>
                    </div>
                   
           <?php }
        }
        ?>
        <div class="col-md-2 col-sm-6">
            <div class="input-wrapper full-width">
                <div class="styled-input">
                    <label>Charges<span class="mandatory-field">*</span></label>
                    <span></span>
                </div>
            </div>
        </div>
        <div class="col-md-2 col-sm-6">
            <div class="input-wrapper full-width">
                <div class="styled-input">
                    <label>Cancellation Charge<span class="mandatory-field">*</span></label>
                    <span></span>
                </div>
            </div>
        </div>
    </div>
    
<?php
    if ($notices_val['type'] == 2 ) {
        if(!empty($price_range)){
        foreach ($price_range as $key=>$pricing_info) { 

            ?>
            <input type="hidden" name="notice[{{$notices_val['id']}}][additional_address][]" value="{{$pricing_info['additional_address']}}">
            <input type="hidden" name="notice[{{$notices_val['id']}}][id][]" value="{{ (($price_range_id == 'default') ? 0 : $pricing_info['id']) }}" >
            <div class="full-width">
                <div class="col-md-3 col-sm-6">
                    <div class="input-wrapper full-width">
                        <div class="styled-input">
                            <textarea name="notice[{{$notices_val['id']}}][description][]" class="form-control" ><?php echo isset($pricing_info['description']) ? $pricing_info['description'] : "" ?></textarea>
                        </div>
                    </div>
                </div>
                <?php    if($package['id'] == '1') {
                ?>
                <div class="col-md-2 col-sm-6">
                    <div class="input-wrapper full-width">
                        <div class="styled-input">
                            <input type="text" min="1" name="notice[{{$notices_val['id']}}][low_limit][]" value="{{$pricing_info['lower_limit']}}"  class="form-control" @if($key==0) data-parsley-required="true" @endif data-parsley-required-message="Limit is required" data-parsley-trigger="chavnge focusout" data-parsley-type="digits" >
                            <span></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6">
                    <div class="input-wrapper full-width">
                        <div class="styled-input">
                            <input type="text"   name="notice[{{$notices_val['id']}}][high_limit][]" value="{{(in_array( $pricing_info['upper_limit'], config('constants.PACKAGE_RANGE'),TRUE)) ?  'Above': $pricing_info['upper_limit'] }}" class="form-control" @if($key==0) data-parsley-required="true" @endif data-parsley-required-message="Limit is required" data-parsley-trigger="change focusout"   />
                            <span></span>
                        </div>
                    </div>
                </div>
            <?php } ?>
              <div class="col-md-2 col-sm-6">
                    <div class="input-wrapper full-width">
                        <div class="styled-input">
                            <input type="text"  min="0" name="notice[{{$notices_val['id']}}][charges][]" value="<?php echo isset($pricing_info['charge']) ? $pricing_info['charge'] : "0" ?>" class="form-control" @if($key==0) data-parsley-required="true" @endif data-parsley-required-message="charges is required" data-parsley-trigger="change focusout" data-parsley-type="number">
                            <span></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6">
                    <div class="input-wrapper full-width">
                        <div class="styled-input">
                            <input type="text"  min="0" name="notice[{{$notices_val['id']}}][nto_cancellation_charge][]" value="<?php echo isset($pricing_info['cancelation_charge']) ? $pricing_info['cancelation_charge'] : "0" ?>"class="form-control" data-parsley-trigger="change focusout" data-parsley-type="number" id="nto_cancellation_charge"/>

                            <span></span>
                        </div>
                    </div>
                </div>
                
            </div>
         <?php 
            }
                                 
            }else{?>

            <input type="hidden" name="notice[{{$notices_val['id']}}][additional_address][]" value="">
            <input type="hidden" name="notice[{{$notices_val['id']}}][id][]" value="0">
             <div class="full-width">
                <div class="col-md-3 col-sm-6">
                    <div class="input-wrapper full-width">
                        <div class="styled-input">
                            <textarea name="notice[{{$notices_val['id']}}][description][]" class="form-control" ></textarea>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6">
                    <div class="input-wrapper full-width">
                        <div class="styled-input">
                            <input type="text" min="1" name="notice[{{$notices_val['id']}}][low_limit][]" value=""  class="form-control" data-parsley-required="true" data-parsley-required-message="Limit is required" data-parsley-trigger="chavnge focusout" data-parsley-type="digits" >
                            <span></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6">
                    <div class="input-wrapper full-width">
                        <div class="styled-input">
                            <input type="text"   name="notice[{{$notices_val['id']}}][high_limit][]" value="" class="form-control" data-parsley-required="true" data-parsley-required-message="Limit is required" data-parsley-trigger="change focusout"   />
                            <span></span>
                        </div>
                    </div>
                </div>

                <div class="col-md-2 col-sm-6">
                    <div class="input-wrapper full-width">
                        <div class="styled-input">
                            <input type="text"  min="0" name="notice[{{$notices_val['id']}}][charges][]" value="0" class="form-control" data-parsley-required="true" data-parsley-required-message="charges is required" data-parsley-trigger="change focusout" data-parsley-type="number">
                            <span></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6">
                    <div class="input-wrapper full-width">
                        <div class="styled-input">
                            <input type="text"  min="0" name="notice[{{$notices_val['id']}}][nto_cancellation_charge][]" value="0" class="form-control" data-parsley-trigger="change focusout" data-parsley-type="number" id="nto_cancellation_charge"/>

                            <span></span>
                        </div>
                    </div>
                </div>
            </div>

            <?php
        }          if($package['id'] == '1') {

        echo '<a type="button" name="add_range" id="add_range" class="btn btn-primary custom-btn input-btn add_range" value="+ More" data-id='.$notices_val['id'].' data-additional-address=""><span>+ More</span></a>';
        }

        echo '<div class="row">
                <div class="col-md-12" >
                    <h4 class="" for="textinput">ADDITIONAL ADDRESS CHARGES</h4>
                </div>
            </div>';
                    if(!empty($additional_address)){
                foreach ($additional_address as $key=>$pricing_info) {
                 //dd($additional_address);
                ?>
                <input type="hidden" name="notice[{{$notices_val['id']}}][id][]" value="{{$pricing_info['id']}}" >
                <input type="hidden" name="notice[{{$notices_val['id']}}][additional_address][]" value="{{$pricing_info['additional_address']}}">
                 <div class="full-width">
                <div class="col-md-3 col-sm-6">
                    <div class="input-wrapper full-width">
                        <div class="styled-input">
                            <textarea name="notice[{{$notices_val['id']}}][description][]" class="form-control" ><?php echo isset($pricing_info['description']) ? $pricing_info['description'] : "" ?></textarea>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6">
                    <div class="input-wrapper full-width">
                        <div class="styled-input">
                            <input type="text" min="1" name="notice[{{$notices_val['id']}}][low_limit][]" value="{{$pricing_info['lower_limit']}}"  class="form-control" @if($key==0) data-parsley-required="true" @endif data-parsley-required-message="Limit is required" data-parsley-trigger="chavnge focusout" data-parsley-type="digits" >
                            <span></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6">
                    <div class="input-wrapper full-width">
                        <div class="styled-input">
                            <input type="text"  name="notice[{{$notices_val['id']}}][high_limit][]" value="{{(in_array( $pricing_info['upper_limit'], config('constants.PACKAGE_RANGE'),TRUE)) ?  'Above': $pricing_info['upper_limit'] }}" class="form-control" @if($key==0) data-parsley-required="true" @endif data-parsley-required-message="Limit is required" data-parsley-trigger="change focusout"  />

                            <span></span>
                        </div>
                    </div>
                </div>

                <div class="col-md-2 col-sm-6">
                    <div class="input-wrapper full-width">
                        <div class="styled-input">
                            <input type="text"  min="0" name="notice[{{$notices_val['id']}}][charges][]" value="<?php echo isset($pricing_info['charge']) ? $pricing_info['charge'] : "0" ?>" class="form-control" @if($key==0) data-parsley-required="true" @endif data-parsley-required-message="charges is required" data-parsley-trigger="change focusout" data-parsley-type="number">
                            <span></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6" hidden=""> 
                    <div class="input-wrapper full-width" >
                        <div class="styled-input">
                            <input type="text"  min="0" name="notice[{{$notices_val['id']}}][nto_cancellation_charge][]" value="<?php echo isset($pricing_info['cancelation_charge']) ? $pricing_info['cancelation_charge'] : "0" ?>"class="form-control" data-parsley-trigger="change focusout" data-parsley-type="number" id="nto_cancellation_charge"/>

                            <span></span>
                        </div>
                    </div>
                </div>
            </div>
        <?php }}else{ ?>
           
            <input type="hidden" name="notice[{{$notices_val['id']}}][id][]" value="0">
            <input type="hidden" name="notice[{{$notices_val['id']}}][additional_address][]" value="additional_address">
                 <div class="full-width">
                <div class="col-md-3 col-sm-6">
                    <div class="input-wrapper full-width">
                        <div class="styled-input">
                            <textarea name="notice[{{$notices_val['id']}}][description][]" class="form-control" ></textarea>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6">
                    <div class="input-wrapper full-width">
                        <div class="styled-input">
                            <input type="text" min="1" name="notice[{{$notices_val['id']}}][low_limit][]" value=""  class="form-control" data-parsley-required="true" data-parsley-required-message="Limit is required" data-parsley-trigger="chavnge focusout" data-parsley-type="digits" >
                            <span></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6">
                    <div class="input-wrapper full-width">
                        <div class="styled-input">
                            <input type="text"   name="notice[{{$notices_val['id']}}][high_limit][]" value="" class="form-control" data-parsley-required="true" data-parsley-required-message="Limit is required" data-parsley-trigger="change focusout"   />
                            <span></span>
                        </div>
                    </div>
                </div>

                <div class="col-md-2 col-sm-6">
                    <div class="input-wrapper full-width">
                        <div class="styled-input">
                            <input type="text"  min="0" name="notice[{{$notices_val['id']}}][charges][]" value="" class="form-control" data-parsley-required="true" data-parsley-required-message="charges is required" data-parsley-trigger="change focusout" data-parsley-type="number">
                            <span></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6" hidden="">
                    <div class="input-wrapper full-width">
                        <div class="styled-input">
                            <input type="text"  min="0" name="notice[{{$notices_val['id']}}][nto_cancellation_charge][]" value="0"class="form-control" data-parsley-trigger="change focusout" data-parsley-type="number" id="nto_cancellation_charge"/>

                            <span></span>
                        </div>
                    </div>
                </div>
            </div>
        <?php }
            echo '<a type="button" name="add_range" id="add_range" class="btn btn-primary custom-btn input-btn add_range" value="+ More" data-id='.$notices_val['id'].' data-additional-address="additional_address"><span>+ More</span></a>';

            } else {
            if($notices_val['master_notice_id'] == config('constants.MASTER_NOTICE')['COL']['ID']){
            if(!empty($col_price_range)){
            foreach ($col_price_range as $key=>$pricing_info) {
            ?>
            <input type="hidden" name="notice[{{$notices_val['id']}}][id][]" value="{{$pricing_info['id']}}" > 
            <input type="hidden" name="notice[{{$notices_val['id']}}][additional_address][]" value="{{$pricing_info['additional_address']}}">
            <div class="full-width">
                <div class="col-md-3 col-sm-6">
                    <div class="input-wrapper full-width">
                        <div class="styled-input">
                            <textarea name="notice[{{$notices_val['id']}}][description][]" class="form-control" ><?php echo isset($pricing_info['description']) ? $pricing_info['description'] : "" ?></textarea>
                        </div>
                    </div>
                </div>

                <div class="col-md-2 col-sm-6">
                    <div class="input-wrapper full-width">
                        <div class="styled-input">
                            <input type="text" min="1" name="notice[{{$notices_val['id']}}][low_limit][]" value="{{$pricing_info['lower_limit']}}"  class="form-control" @if($key==0) data-parsley-required="true" @endif data-parsley-required-message="Limit is required" data-parsley-trigger="chavnge focusout" data-parsley-type="digits" >
                            <span></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6">
                    <div class="input-wrapper full-width">
                        <div class="styled-input">
                            <input type="text"   name="notice[{{$notices_val['id']}}][high_limit][]" value="{{(in_array( $pricing_info['upper_limit'], config('constants.PACKAGE_RANGE'),TRUE)) ?  'Above': $pricing_info['upper_limit'] }}" class="form-control" @if($key==0) data-parsley-required="true" @endif data-parsley-required-message="Limit is required" data-parsley-trigger="change focusout"   />
                            <span></span>
                        </div>
                    </div>
                </div>

                <div class="col-md-2 col-sm-6">
                    <div class="input-wrapper full-width">
                        <div class="styled-input">
                            <input type="text"  min="0" name="notice[{{$notices_val['id']}}][charges][]" value="<?php echo isset($pricing_info['charge']) ? $pricing_info['charge'] : "0" ?>" class="form-control" @if($key==0) data-parsley-required="true" @endif data-parsley-required-message="charges is required" data-parsley-trigger="change focusout" data-parsley-type="number">
                            <span></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6">
                    <div class="input-wrapper full-width">
                        <div class="styled-input">
                            <input type="text"  min="0" name="notice[{{$notices_val['id']}}][nto_cancellation_charge][]" value="<?php echo isset($pricing_info['cancelation_charge']) ? $pricing_info['cancelation_charge'] : "0" ?>"class="form-control" data-parsley-trigger="change focusout" data-parsley-type="number" id="nto_cancellation_charge"/>

                            <span></span>
                        </div>
                    </div>
                </div>
            </div>
        <?php }
    }else{ ?>
             <div class="full-width">
                <div class="col-md-3 col-sm-6">
                    <div class="input-wrapper full-width">
                        <div class="styled-input">
                            <textarea name="notice[{{$notices_val['id']}}][description][]" class="form-control" ></textarea>
                        </div>
                    </div>
                </div>

                <div class="col-md-2 col-sm-6">
                    <div class="input-wrapper full-width">
                        <div class="styled-input">
                            <input type="text" min="1" name="notice[{{$notices_val['id']}}][low_limit][]" value=""  class="form-control" data-parsley-required="true" data-parsley-required-message="Limit is required" data-parsley-trigger="chavnge focusout" data-parsley-type="digits" >
                            <span></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6">
                    <div class="input-wrapper full-width">
                        <div class="styled-input">
                            <input type="text"  name="notice[{{$notices_val['id']}}][high_limit][]" value="" class="form-control" data-parsley-required="true" data-parsley-required-message="Limit is required" data-parsley-trigger="change focusout"   />
                            <span></span>
                        </div>
                    </div>
                </div>

                <div class="col-md-2 col-sm-6">
                    <div class="input-wrapper full-width">
                        <div class="styled-input">
                            <input type="text"  min="0" name="notice[{{$notices_val['id']}}][charges][]" value="0" class="form-control" data-parsley-required="true" data-parsley-required-message="charges is required" data-parsley-trigger="change focusout" data-parsley-type="number">
                            <span></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6">
                    <div class="input-wrapper full-width">
                        <div class="styled-input">
                            <input type="text"  min="0" name="notice[{{$notices_val['id']}}][nto_cancellation_charge][]" value="0"class="form-control" data-parsley-trigger="change focusout" data-parsley-type="number" id="nto_cancellation_charge"/>

                            <span></span>
                        </div>
                    </div>
                </div>
            </div>
    <?php }
        echo '<a type="button" name="add_range" id="add_range" class="btn btn-primary custom-btn input-btn add_range" value="+ More" data-id='.$notices_val['id'].' data-additional-address=""><span>+ More</span></a>';
        }else if($notices_val['master_notice_id'] == config('constants.MASTER_NOTICE')['BCOL']['ID']){
            if(!empty($bcol_price_range)){
            foreach ($bcol_price_range as $key=>$pricing_info) {
           ?>
            <input type="hidden" name="notice[{{$notices_val['id']}}][id][]" value="{{$pricing_info['id']}}" >
            <input type="hidden" name="notice[{{$notices_val['id']}}][additional_address][]" value="{{$pricing_info['additional_address']}}">
              <div class="full-width">
                <div class="col-md-3 col-sm-6">
                    <div class="input-wrapper full-width">
                        <div class="styled-input">
                            <textarea name="notice[{{$notices_val['id']}}][description][]" class="form-control" ><?php echo isset($pricing_info['description']) ? $pricing_info['description'] : "" ?></textarea>
                        </div>
                    </div>
                </div>

                <div class="col-md-2 col-sm-6">
                    <div class="input-wrapper full-width">
                        <div class="styled-input">
                            <input type="text" min="1" name="notice[{{$notices_val['id']}}][low_limit][]" value="{{$pricing_info['lower_limit']}}"  class="form-control" @if($key==0) data-parsley-required="true" @endif data-parsley-required-message="Limit is required" data-parsley-trigger="chavnge focusout" data-parsley-type="digits" >
                            <span></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6">
                    <div class="input-wrapper full-width">
                        <div class="styled-input">
                            <input type="text"  name="notice[{{$notices_val['id']}}][high_limit][]" value="{{(in_array( $pricing_info['upper_limit'], config('constants.PACKAGE_RANGE'),TRUE)) ?  'Above': $pricing_info['upper_limit'] }}" class="form-control" @if($key==0) data-parsley-required="true" @endif data-parsley-required-message="Limit is required" data-parsley-trigger="change focusout"   />
                            <span></span>
                        </div>
                    </div>
                </div>

                <div class="col-md-2 col-sm-6">
                    <div class="input-wrapper full-width">
                        <div class="styled-input">
                            <input type="text"  min="0" name="notice[{{$notices_val['id']}}][charges][]" value="<?php echo isset($pricing_info['charge']) ? $pricing_info['charge'] : "0" ?>" class="form-control" @if($key==0) data-parsley-required="true" @endif data-parsley-required-message="charges is required" data-parsley-trigger="change focusout" data-parsley-type="number">
                            <span></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6">
                    <div class="input-wrapper full-width">
                        <div class="styled-input">
                            <input type="text"  min="0" name="notice[{{$notices_val['id']}}][nto_cancellation_charge][]" value="<?php echo isset($pricing_info['cancelation_charge']) ? $pricing_info['cancelation_charge'] : "0" ?>"class="form-control" data-parsley-trigger="change focusout" data-parsley-type="number" id="nto_cancellation_charge"/>

                            <span></span>
                        </div>
                    </div>
                </div>
            </div>
        <?php }
    }else{ ?>
                     <div class="full-width">
                <div class="col-md-3 col-sm-6">
                    <div class="input-wrapper full-width">
                        <div class="styled-input">
                            <textarea name="notice[{{$notices_val['id']}}][description][]" class="form-control" ></textarea>
                        </div>
                    </div>
                </div>

                <div class="col-md-2 col-sm-6">
                    <div class="input-wrapper full-width">
                        <div class="styled-input">
                            <input type="text" min="1" name="notice[{{$notices_val['id']}}][low_limit][]" value=""  class="form-control" data-parsley-required="true" data-parsley-required-message="Limit is required" data-parsley-trigger="chavnge focusout" data-parsley-type="digits" >
                            <span></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6">
                    <div class="input-wrapper full-width">
                        <div class="styled-input">
                            <input type="text"  name="notice[{{$notices_val['id']}}][high_limit][]" value="" class="form-control" data-parsley-required="true" data-parsley-required-message="Limit is required" data-parsley-trigger="change focusout"  />
                            <span></span>
                        </div>
                    </div>
                </div>

                <div class="col-md-2 col-sm-6">
                    <div class="input-wrapper full-width">
                        <div class="styled-input">
                            <input type="text"  min="0" name="notice[{{$notices_val['id']}}][charges][]" value="0" class="form-control" data-parsley-required="true" data-parsley-required-message="charges is required" data-parsley-trigger="change focusout" data-parsley-type="number">
                            <span></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-6">
                    <div class="input-wrapper full-width">
                        <div class="styled-input">
                            <input type="text"  min="0" name="notice[{{$notices_val['id']}}][nto_cancellation_charge][]" value="0"class="form-control" data-parsley-trigger="change focusout" data-parsley-type="number" id="nto_cancellation_charge"/>

                            <span></span>
                        </div>
                    </div>
                </div>
            </div>
   <?php }
        echo '<a type="button" name="add_range" id="add_range" class="btn btn-primary custom-btn input-btn add_range" value="+ More" data-id='.$notices_val['id'].' data-additional-address=""><span>+ More</span></a>';
    }
        else{
        $pricing_info = getCustomerPricings($customer_id,$notices_val['id'], $package['id'],NULL, NULL);
        if(empty($pricing_info)){
            $id = 0;
            $pricing_info = getOtherPricings($notices_val['id'], $package['id'], NULL, NULL);
        } else {
            $id = $pricing_info['id'];
        }
         ?>
        <input type="hidden" name="notice[{{$notices_val['id']}}][id][]" value="{{$id}}" >
        <input type="hidden" name="notice[{{$notices_val['id']}}][additional_address][]" value="{{$pricing_info['additional_address']?$pricing_info['additional_address']:''}}">
        <div class="full-width">
            <div class="col-md-3 col-sm-6">
                <div class="input-wrapper full-width">
                    <div class="styled-input">
                        <textarea name="notice[{{$notices_val['id']}}][description][]" class="form-control" ><?php echo isset($pricing_info['description']) ? $pricing_info['description'] : "" ?></textarea>
                    </div>
                </div>
            </div>


            <div class="col-md-2 col-sm-6">
                <div class="input-wrapper full-width">
                    <div class="styled-input">
                        <input type="text"  min="0" name="notice[{{$notices_val['id']}}][charges][]" value="<?php echo isset($pricing_info['charge']) ? $pricing_info['charge'] : "0" ?>" class="form-control" data-parsley-required="true" data-parsley-required-message="charges is required" data-parsley-trigger="change focusout" data-parsley-type="number">

                        <span></span>
                    </div>
                </div>
            </div>
            <div class="col-md-2 col-sm-6">
                <div class="input-wrapper full-width">
                    <div class="styled-input">
                        <input type="text"  min="0" name="notice[{{$notices_val['id']}}][nto_cancellation_charge][]" value="<?php echo isset($pricing_info['cancelation_charge']) ? $pricing_info['cancelation_charge'] : "0" ?>" class="form-control" data-parsley-trigger="change focusout" data-parsley-type="number" id="nto_cancellation_charge"  />

                        <span></span>
                    </div>
                </div>
            </div>
        </div>

    <?php } }?>
<?php } ?>


