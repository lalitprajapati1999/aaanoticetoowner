@extends('adminlte::page')


@section('content')
<section class="address-book-add">
    <div class="dashboard-wrapper">
        <div class="dashboard-heading">
            <h1><span>Update Customer</span></h1>
        </div>

        <div class="dashboard-inner-body">
            <form class="form" id="edit-customer" data-parsley-validate="" action="{{url('admin/customer/edit/'.$id)}}" method="post">

                {!! csrf_field() !!}
                @if (Session::get('success'))
                <div class="alert alert-success no-margin">
                   <?php echo Session::get('success'); ?>
                </div>
                @endif
                @if(session()->has('error'))
                <div class="alert alert-danger no-margin">
                  <?php echo Session::get('error'); ?>
                </div>
                @endif

                <div class="project-wrapper">

                       <div class="dash-subform">
                        <div class="row">
                             <h2>Login Details</h2>
                            <div class="col-md-6 col-sm-6">
                                <div class="input-wrapper full-width">
                                    <input type="hidden" name="user_id" value="{{$user_info->id}}"/>
                                    <div class="styled-input">
                                      <input type="text" name="name" class="form-control" value="{{ old('name')?old('name'):$user_info->name}}" data-parsley-required="true" data-parsley-required-message="Full Name is required" data-parsley-trigger="change focusout" >
                                        <label>{{ trans('adminlte::adminlte.full_name') }}<span class="mandatory-field">*</span></label>
                                        <span></span>

                                        @if ($errors->has('name'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </p>
                                            @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="input-wrapper full-width">
                                    <div class="styled-input">
                                         <input type="email" name="email" class="form-control"  value="{{ old('email')?old('email'):$user_info->email }}" data-parsley-required="true" data-parsley-required-message="Email is required" data-parsley-trigger="change focusout" data-parsley-type="email">
                                            <label>{{ trans('adminlte::adminlte.email') }}<span class="mandatory-field">*</span></label>
                                            <span></span>

                                            @if ($errors->has('email'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </p>
                                            @endif
                                    </div>
                                </div>
                            </div>
                             <div class="col-md-6 col-sm-6">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                            <input type="text" name="password" value="{{old('password')?old('password'):$user_info->password_text}}" class="form-control" id="inputPassword" data-parsley-required="true" data-parsley-required-message="Password is required" data-parsley-trigger="change focusout" data-parsley-minlength="6" data-parsley-maxlength="20">
                                            <label>{{ trans('adminlte::adminlte.password') }}<span class="mandatory-field">*</span></label>
                                            <span></span>

                                            @if ($errors->has('password'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                    <?php $account_manager = $customer->account_manager_id ?>
                    <div class="dash-subform">
                    <div class="row">
                     <div class="col-md-6 col-sm-12">
                                <?php $mailing_state1 = $customer->mailing_state_id ?>
                                <div class="select-opt">
                                    <label class="col-md-12 control-label" for="textinput">Change Account Manager<span class="mandatory-field"></span></label>  
                                    <select name="account_manager_id" class="selectpicker" data-live-search="true">
                                        <option value="">Select</option>
                                        @foreach($account_managers as $manager)
                                        <option value="{{$manager->id}}"  @if ($account_manager == $manager->id) selected="selected" @endif > {{ucfirst(trans($manager->name))}}</option>
                                        @endforeach
                                    </select>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="dash-subform">
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="input-wrapper full-width">
                                    <div class="styled-input">
                                        <input type="text" name="company_name" value="{{ old('company_name')?old('company_name'):$customer->company_name}}" class="form-control" data-parsley-required="true" data-parsley-required-message="Company Name is required" data-parsley-trigger="change focusout">
                                        <label>{{ trans('adminlte::adminlte.company_name') }}<span class="mandatory-field">*</span></label>
                                        <span></span>

                                        @if ($errors->has('company_name'))
                                        <p class="help-block">
                                            <strong>{{ $errors->first('company_name') }}</strong>
                                        </p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="input-wrapper full-width">
                                    <div class="styled-input">
                                        <input type="text" name="contact_person" value="{{ old('contact_person')?old('contact_person'):$customer->contact_person}}" class="form-control" data-parsley-required="true" data-parsley-required-message="Contact person is required" data-parsley-trigger="change focusout">
                                        <label>{{ trans('adminlte::adminlte.contact_person') }}<span class="mandatory-field">*</span></label>
                                        <span></span>

                                        @if ($errors->has('contact_person'))
                                        <p class="help-block">
                                            <strong>{{ $errors->first('contact_person') }}</strong>
                                        </p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="dash-subform addcontactperson">

                        <div class="row">
                            <h2>Mailing Address</h2>

                            <div class="col-md-12 col-sm-12">
                                <div class="input-wrapper full-width">
                                    <div class="styled-input">
                                        <input type="text" class="form-control" data-parsley-required="true" name="mailing_address"  value="{{ old('mailing_address')?old('mailing_address'):$customer->mailing_address}}"  data-parsley-required-message="Mailing Address is required" data-parsley-trigger="change focusout"/>
                                        <label>{{ trans('adminlte::adminlte.mailing_address') }}<span class="mandatory-field">*</span></label>
                                        <span></span>
                                        @if ($errors->has('mailing_address'))
                                        <p class="help-block">
                                            <strong>{{ $errors->first('mailing_address') }}</strong>
                                        </p>
                                        @endif
                                    </div>

                                </div>
                            </div>

                            <?php $mailing_city1 = $customer->mailing_city_id ?>
                            <div class="col-md-4 col-sm-4">
                                <div class="input-wrapper full-width">
                                    <div class="styled-input">
                                        <input class="form-control"  type="text" name="mailing_city" value="{{ucfirst(trans($customer->mailing_city_name))}}" id="customer_mailing_city_id" data-parsley-required="true" data-parsley-required-message="City is required" data-parsley-trigger="change focusout">
                                        <label>City<span class="mandatory-field">*</span></label> 
                                        <span></span>

                                    </div>
                                    <div id="mailingcityloader" style="display: none">
                                        <img width="150px" src="{{asset('images/loader.gif')}}"/>
                                    </div>
                                    <input type="hidden" name="mailing_city_value_id" id="mailing_city_value_id" value="{{old('mailing_city_value_id')?old('mailing_city_value_id'):$customer->mailing_city_id}}"/>

                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <?php $mailing_state1 = $customer->mailing_state_id ?>
                                <div class="select-opt">
                                    <label class="col-md-12 control-label" for="textinput">State<span class="mandatory-field">*</span></label>  
                                    <select name="mailing_state" class="selectpicker" required data-parsley-required-message="State is required" data-parsley-trigger="change focusout" id="customer_mailing_state_id" data-show-subtext="true" data-live-search="true">
                                        <option value="">Select</option>
                                        @foreach($states as $mailing_state)
                                        <option value="{{$mailing_state->id}}" {{ old('mailing_state')?old('mailing_state'):($mailing_state1== $mailing_state->id ? 'selected="selected"' : '' )}} > {{ucfirst(trans($mailing_state->name))}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('mailing_state'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('mailing_state') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-4">
                                <div class="input-wrapper full-width">
                                    <div class="styled-input">
                                        <input type="text" name="mailing_zip" class="form-control" value="{{ old('mailing_zip') ? old('mailing_zip'):$customer->mailing_zip}}" 
                                               placeholder="" required data-parsley-required-message="Zip code is required" data-parsley-trigger="change focusout" data-parsley-type="number">
                                        <label>{{ trans('adminlte::adminlte.zip') }}<span class="mandatory-field">*</span></label>
                                        <span></span>
                                        @if ($errors->has('mailing_zip'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('mailing_zip') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="dash-subform addcontactperson">
                        <div class="row">
                            <h2>Physical Address</h2>
                            <div class="col-md-12 col-sm-12">
                                <div class="input-wrapper full-width">
                                    <div class="styled-input">
                                        <input type="text" name="company_physical_address"  class="form-control" data-parsley-required="true" value="{{ old('company_physical_address')?old('company_physical_address'):$customer->physical_address}}" data-parsley-required-message="Physical address is required" data-parsley-trigger="change focusout" id="customer_edit_physical_address"/>
                                        <label>{{ trans('adminlte::adminlte.company_physical_address') }}<span class="mandatory-field">*</span></label>

                                        @if ($errors->has('company_physical_address'))
                                        <p class="help-block">
                                            <strong>{{ $errors->first('company_physical_address') }}</strong>
                                        </p>
                                        @endif
                                    </div>
                                </div>
                            </div>


                            <?php $physical_city1 = $customer->physical_city_id ?>
                            <div class="col-md-4 col-sm-4">
                                <div class="input-wrapper full-width">
                                    <div class="styled-input">
                                        <input class="form-control"  type="text" name="physical_city" value="{{ucfirst(trans($customer->physical_city_name))}}" id="customer_edit_physical_city" data-parsley-required="true" data-parsley-required-message="City is required" data-parsley-trigger="change focusout">
                                        <label>City<span class="mandatory-field">*</span></label> 
                                        <span></span>

                                    </div>
                                    <div id="physicalcityloader" style="display: none">
                                        <img width="150px" src="{{asset('images/loader.gif')}}"/>
                                    </div>
                                    <input type="hidden" name="physical_city_value_id" id="physical_city_value_id" value="{{old('physical_city_value_id')?old('physical_city_value_id'):$customer->physical_city_id}}"/>

                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <?php $physical_state1 = $customer->physical_state_id ?>
                                <div class="select-opt">
                                    <label class="col-md-12 control-label" for="textinput">State<span class="mandatory-field">*</span></label>  
                                    <select  name="physical_state"  class="selectpicker" data-parsley-required="true" data-parsley-required-message="State is required" data-parsley-trigger="change focusout" id='customer_edit_physical_state' data-show-subtext="true" data-live-search="true">
                                        <option value="">Select</option>
                                        @foreach($states as $physical_state)
                                        <option value="{{$physical_state->id}}"
                                                @if ($physical_state1 == $physical_state->id) selected="selected" @endif > {{ucfirst(trans($physical_state->name))}}</option>
                                        @endforeach

                                    </select>

                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="input-wrapper full-width">
                                    <div class="styled-input">
                                        <input type="text" name="physical_zip" id="physical_zip_id" value="{{ old('physical_zip') ? old('physical_zip'):$customer->physical_zip}}" class="form-control" data-parsley-required="true" data-parsley-required-message="Zip Code is required" data-parsley-trigger="change focusout" data-parsley-type="number"/>
                                        <label>{{ trans('adminlte::adminlte.zip') }}<span class="mandatory-field">*</span></label>
                                        <span></span>

                                        @if ($errors->has('physical_zip'))
                                        <p class="help-block">
                                            <strong>{{ $errors->first('physical_zip') }}</strong>
                                        </p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12">
                                <div class="input-wrapper full-width">
                                    <div class="styled-input">
                                        <input type="text" name="hear_about" value="{{ old('hear_about')?old('hear_about'):$customer->hear_about}}" class="form-control" />
                                        <label>{{ trans('adminlte::adminlte.hear_about') }}</label>
                                        <span></span>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="input-wrapper full-width">
                                    <div class="styled-input">
                                        <input type="text"  name="office_number" class="form-control" id="office_number" data-parsley-required="true" value="{{ old('office_number')?old('office_number'):$customer->office_number}}" data-parsley-required-message="Office number is required" data-parsley-trigger="change focusout"/>
                                        <label>{{ trans('adminlte::adminlte.office_number') }}<span class="mandatory-field">*</span></label>
                                        <span></span>


                                        @if ($errors->has('office_number'))
                                        <p class="help-block">
                                            <strong>{{ $errors->first('office_number') }}</strong>
                                        </p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="input-wrapper full-width">
                                    <div class="styled-input">
                                        <input type="text" name="fax" id="fax_number" class="form-control"  value="{{ old('fax')?old('fax'):$customer->fax_number}}" data-parsley-trigger="change focusout" />
                                        <label>{{ trans('adminlte::adminlte.fax') }}</label>
                                        <span></span>

                                        @if ($errors->has('fax'))
                                        <p class="help-block">
                                            <strong>{{ $errors->first('fax') }}</strong>
                                        </p>
                                        @endif
                                    </div>
                                </div>
                            </div>


                            <?php
                            if (!empty($customer->company_email)) {
                                $company_arr = explode(',', $customer->company_email);
                            }
                            ?>

                            @if(!empty($company_arr))
                            <div class="col-md-8 col-sm-8">
                                <div id="html-div" class="input-wrapper full-width additional_email">
                                    @foreach($company_arr AS $e=>$each_company_email)

                                    <div class="styled-input">
                                        <input type="email" name="company_email[]" id="company_email"  class="form-control"  value="{{$each_company_email}}" required data-parsley-required-message="Email is required" data-parsley-trigger="change focusout" data-parsley-type="email"/>
                                        <label>Company Email<span class="mandatory-field">*</span></label>
                                        <span></span>
                                    </div>
                                    @if ($errors->has('company_email.'.$e))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('company_email.'.$e) }}</strong>
                                    </span>
                                    @endif
                                    @endforeach

                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="dash-subform addcontactperson">

                        <div class="row" id="reg_agents">
                            <h2>Officer/Director</h2>

                            @if(!empty($customer_agents))
                            @foreach($customer_agents As $k=>$each_agent)
                            <div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                            <input type="text" name="agent_first_name[]" class="form-control" value="{{old('agent_first_name.'.$k)?old('agent_first_name.'.$k):$each_agent->first_name}}" 
                                                   placeholder="" required data-parsley-required-message="First Name is required" data-parsley-trigger="change focusout" >
                                            <label>{{ trans('adminlte::adminlte.agent_first_name') }}<span class="mandatory-field">*</span></label>
                                            <span></span>
                                        </div>
                                        @if ($errors->has('agent_first_name.'.$k))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('agent_first_name.'.$k) }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                            <input type="text" name="agent_last_name[]" class="form-control" value="{{old('agent_last_name.'.$k)?old('agent_last_name.'.$k):$each_agent->last_name}}" 
                                                   placeholder="" required data-parsley-required-message="Last Name is required" data-parsley-trigger="change focusout" >
                                            <label>{{ trans('adminlte::adminlte.agent_last_name') }}<span class="mandatory-field">*</span></label>
                                            <span></span>
                                        </div>
                                        @if ($errors->has('agent_last_name.'.$k))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('agent_last_name.'.$k) }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                            <input type="text" name="agent_title[]" class="form-control" value="{{old('agent_title.'.$k)?old('agent_title.'.$k):$each_agent->title}}" 
                                                   placeholder="" required data-parsley-required-message="Title is required" data-parsley-trigger="change focusout" >
                                            <label>{{ trans('adminlte::adminlte.agent_title') }}<span class="mandatory-field">*</span></label>
                                            <span></span>
                                        </div>
                                        @if ($errors->has('agent_title.'.$k))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('agent_title.'.$k) }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="rghtagent"><i data-id="{{$each_agent->id}}" class="fa fa-remove removeAgent removeExistingAgent"></i></div>
                            
                            @endforeach
                            @endif
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="form-btn-div">
                                    <a class="btn btn-primary custom-btn form-btn agent-duplicate-button"><span>+</span> Add More Agents</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="dash-subform addcontactperson">

                        @if(!empty($customer_repres) && count($customer_repres) > 0)
                        <div class="row" id="representative">
                            <h2>Representative</h2>
                            @foreach($customer_repres As $k=>$each_repres)
                            <div class="representativeDivs full-width">
                                <div class="col-md-6 col-sm-6">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                            <input type="text" name="repres_contact_person[]" class="form-control" value="{{old('repres_contact_person.'.$k)?old('repres_contact_person.'.$k):$each_repres->contact_person}}" required data-parsley-required-message="Contact person is required" data-parsley-trigger="change focusout">
                                            <label>{{ trans('adminlte::adminlte.contact_person') }}</label>
                                            <span></span>
                                        </div>
                                        @if ($errors->has('repres_contact_person.'.$k))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('repres_contact_person.'.$k) }}</strong>
                                        </span>
                                        @endif

                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                            <input type="text" name="repres_company_branch_name[]" class="form-control" value="{{old('repres_company_branch_name.'.$k)?old('repres_company_branch_name.'.$k):$each_repres->branch_name}}" required data-parsley-required-message="Company branch name is required" data-parsley-trigger="change focusout">
                                            <label>{{ trans('adminlte::adminlte.repres_company_branch_name') }}</label>
                                            <span></span>
                                        </div>
                                        @if ($errors->has('repres_company_branch_name.'.$k))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('repres_company_branch_name.'.$k) }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                            <input type="email" name="repres_email[]" class="form-control" value="{{old('repres_company_branch_name.'.$k)?old('repres_company_branch_name.'.$k):$each_repres->email}}" required data-parsley-required-message="Email is required" data-parsley-trigger="change focusout" data-parsley-type="email">
                                            <label>{{ trans('adminlte::adminlte.repres_email') }}</label>
                                            <span></span>
                                        </div>
                                        @if ($errors->has('repres_email.'.$k))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('repres_email.'.$k) }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-6 col-sm-6">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                            <input type="text" name="repres_mobile_number[]" class="form-control mobile_no_masking"  value="{{old('repres_mobile_number.'.$k)?old('repres_mobile_number.'.$k):$each_repres->mobile_number}}" required data-parsley-trigger="change focusout"/>
                                            <label>{{ trans('adminlte::adminlte.mobile_number') }}</label>
                                            <span></span>

                                            @if ($errors->has('mobile_number'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('mobile_number') }}</strong>
                                            </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="rght"><i data-id="{{$each_repres->id}}" class="fa fa-remove removeRepresentative removeExistingRepresentative "></i></div>
                            @endforeach
                        </div>
                        @else
                        <div class="row" id="representative">
                            <h2>Representative</h2>

                            <div class="representativeDivs">
                                <div class="col-md-6 col-sm-6">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                            <input type="text" name="repres_contact_person[]" class="form-control" value="{{old('repres_contact_person.0')}}"  data-parsley-required-message="Contact person is required" data-parsley-trigger="change focusout">
                                            <label>{{ trans('adminlte::adminlte.contact_person') }}</label>
                                            <span></span>
                                        </div>
                                        @if ($errors->has('repres_contact_person.'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('repres_contact_person.') }}</strong>
                                        </span>
                                        @endif

                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                            <input type="text" name="repres_company_branch_name[]" class="form-control" value="{{old('repres_company_branch_name.0')}}"  data-parsley-required-message="Company branch name is required" data-parsley-trigger="change focusout" >
                                            <label>{{ trans('adminlte::adminlte.repres_company_branch_name') }}</label>
                                            <span></span>
                                        </div>
                                        @if ($errors->has('repres_company_branch_name.'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('repres_company_branch_name.') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                            <input type="email" name="repres_email[]" class="form-control" value="{{old('repres_email.0')}}"  data-parsley-required-message="Email is required" data-parsley-trigger="change focusout" data-parsley-type="email">
                                            <label>{{ trans('adminlte::adminlte.repres_email') }}</label>
                                            <span></span>
                                        </div>
                                        @if ($errors->has('repres_email.'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('repres_email.') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-6 col-sm-6">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                            <input type="text" name="repres_mobile_number[]" class="form-control mobile_no_masking"  value="{{ old('repres_mobile_number.0')}}"  data-parsley-trigger="change focusout"/>
                                            <label>{{ trans('adminlte::adminlte.mobile_number') }}</label>
                                            <span></span>

                                            @if ($errors->has('mobile_number'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('mobile_number') }}</strong>
                                            </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        @endif
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="form-btn-div">
                                    <a class="btn btn-primary custom-btn form-btn representative-duplicate-button"><span>+</span> Add Another Representative</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="dash-subform">
                        <div class="row">

                            <div class="col-md-6 col-sm-12">
                                <div class="input-wrapper full-width">
                                    <div class="styled-input {{ $errors->has('no_of_offices') ? 'has-error' : '' }}">
                                        <input type="text" name="no_of_offices" class="form-control" value="{{ old('no_of_offices') ? old('no_of_offices'):$customer['no_of_offices']}}"
                                               id="number_offices"  data-parsley-required-message="No of offices is required" data-parsley-trigger="change focusout" data-parsley-type="digits">    
                                        <label>How many branches throughout the united states?</label>
                                        <span></span>
                                    </div>
                                </div>
                            </div>
                        </div>   
                    </div>
                    
                     
                    <div id="showBranchDiv">
                        @if(!empty($branches))
                        @foreach($branches AS $k=>$each_branch)
                        <input type="hidden" name="branch_city_ids[]" id="branch_city_ids" value="{{$each_branch->city_id}}" />
                        <input type="hidden" name="branch_county_ids[]" id="branch_county_ids" class='branch_county_ids' value="{{$each_branch->county}}" />

                        <div class="subdiv">
                            <div class="dash-subform">
                                <div class="row">
                                    <div class="col-md-4 col-sm-4">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="branch_name[]" value="{{old('branch_name.'.$k)?old('branch_name.'.$k):$each_branch->name}}" 
                                                       id="branch_name" class="form-control" />
                                                <label>{{ trans('adminlte::adminlte.branch_name') }}</label>
                                                <span></span>
                                                @if ($errors->has('branch_name.'.$k))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('branch_name.'.$k) }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="branch_phone[]"  value="{{old('branch_phone.'.$k)?old('branch_phone.'.$k):$each_branch->phone}}"  class="form-control branch_phone" data-parsley-trigger="change focusout" />
                                                <label>{{ trans('adminlte::adminlte.branch_phone') }}</label>
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="branch_contact_person[]" value="{{old('branch_contact_person.'.$k)?old('branch_contact_person.'.$k):$each_branch->contact_person}}" class="form-control" />
                                                <label>{{ trans('adminlte::adminlte.contact_person') }}</label>
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-8 col-sm-8">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="branch_address[]"  value="{{old('branch_address.'.$k)?old('branch_address.'.$k):$each_branch->address}}"  class="form-control" />
                                                <label>{{ trans('adminlte::adminlte.branch_address') }}</label>
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-4 col-sm-4">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input class="form-control branch_city_val"  type="text" name="branch_city[]" value="{{old('branch_city.'.$k)?old('branch_city.'.$k):$each_branch->branch_city_name}}" id="city_val{{$k}}">
                                                <label>City</label> 
                                                <span></span>

                                            </div>
                                            <div id="branchcityloader" style="display: none">
                                                <img width="150px" src="{{asset('images/loader.gif')}}"/>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="col-md-4 col-sm-4">
                                        <div class="select-opt">
                                            <label class="col-md-12 control-label" for="textinput">State</label>  
                                            <select name="branch_state[]"  class="selectpicker" id="edit_branch_state_id" data-show-subtext="true" data-live-search="true">
                                                <option value="">Select</option>
                                                @foreach($states as $branch_state)
                                                <option value="{{$branch_state->id}}" {{old('branch_state.'.$k)?old('branch_state.'.$k):($each_branch->state_id == $branch_state->id?'selected="selected"':'')}}> {{ucfirst(trans($branch_state->name))}}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('branch_state'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('branch_state') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-sm-4">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" class="form-control branch_zip" name="branch_zip[]" value="{{old('branch_zip.'.$k)?old('branch_zip.'.$k):$each_branch->zip}}" placeholder="" data-parsley-trigger="change focusout" data-parsley-type="digits" data-parsley-minlength="5" data-parsley-maxlength="5"/>
                                                <label>{{ trans('adminlte::adminlte.zip') }}</label>
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-sm-4">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input class="form-control branch_country"  type="text" name="branch_country[]" value="{{old('branch_country.'.$k)?old('branch_country.'.$k):$each_branch->branch_county_name}}" id="county_val{{$k}}">
                                                <label>County</label> 
                                                <span></span>

                                            </div>
                                            <div id="branchcountyloader" style="display: none">
                                                <img width="150px" src="{{asset('images/loader.gif')}}"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">

                                    <div class="col-md-4 col-sm-4">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="branch_first_name[]" value="{{old('branch_first_name.'.$k)?old('branch_first_name.'.$k):$each_branch->first_name}}" 
                                                       id="branch_first_name" class="form-control" />
                                                <label>{{ trans('adminlte::adminlte.agent_first_name') }}</label>
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="branch_last_name[]"  value="{{old('branch_last_name.'.$k)?old('branch_last_name.'.$k):$each_branch->last_name}}"  class="form-control"  />
                                                <label>{{ trans('adminlte::adminlte.agent_last_name') }}</label>
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="branch_title[]" value="{{old('branch_title.'.$k)?old('branch_title.'.$k):$each_branch->title}}" class="form-control" />
                                                <label>{{ trans('adminlte::adminlte.agent_title') }}</label>
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        @endforeach
                        @endif
                    </div>
                </div>
        </div>
    </div>
</div>
<div class="col-md-12 col-sm-12">
    <div class="row">
        <div class="form-bottom-btn">
            <a href="{{ url('admin/customer')}}" class="btn btn-primary custom-btn customb-btn"><span>{{ trans('backpack::base.cancel') }}</span></a>
            <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>Save</span></button>
        </div>
    </div> 
</div>
</div>

</form>

</div>
</section>

@endsection
@section('frontend_js')
<script type="text/javascript" src="{{ asset('js/jquery-ui.min.js') }}"></script>

<script type="text/javascript">

$(document).ready(function () {
    src_city = "{{ url('customer/city/autocompletecityname') }}";
    remove_agent = "{{ url('admin/remove_agent') }}";
    remove_representative = "{{ url('admin/remove_representative') }}";

    //Mailing city value autocomplete
    $("#customer_mailing_city_id").autocomplete({

        source: function (request, response) {
            $.ajax({
                url: src_city,
                dataType: "json",
                data: {
                    term: request.term
                },
                beforeSend: function () {
                    $('#mailingcityloader').show();
                },
                complete: function () {
                    $('#mailingcityloader').hide();
                },
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            value: item.value,
                            id: item.id     // EDIT
                        }
                    }));

                },

            });
        },
        select: function (event, ui) {

            $("#mailing_city_value_id").val(ui.item.id);
            $("#customer_mailing_city_id").val(ui.item.value);
        }
    });
    //Physical city value autocomplete
    $("#customer_edit_physical_city").autocomplete({

        source: function (request, response) {
            $.ajax({
                url: src_city,
                dataType: "json",
                data: {
                    term: request.term
                },
                beforeSend: function () {
                    $('#physicalcityloader').show();
                },
                complete: function () {
                    $('#physicalcityloader').hide();
                },
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            value: item.value,
                            id: item.id     // EDIT
                        }
                    }));

                },

            });
        },
        select: function (event, ui) {

            $("#physical_city_value_id").val(ui.item.id);
            $("#customer_edit_physical_city").val(ui.item.value);
        }
    });
    $("#office_number").mask("(999) 999-9999");
    $('.mobile_no_masking').mask("(999) 999-9999");
    $("#fax_number").mask("(999) 999-9999");
    $(".branch_phone").mask("(999) 999-9999");
 
    
    var exist_county_arr = [];
    var exist_city_arr = [];
    $('input[id^="city_val"]').on("focus.autocomplete", null, function () {
        $(this).autocomplete({

            source: function (request, response) {
                $.ajax({
                    url: src_city,
                    dataType: "json",
                    data: {
                        term: request.term
                    },
                    beforeSend: function () {
                        $('.bcityloader').show();
                    },
                    complete: function () {
                        $('.bcityloader').hide();
                    },
                    success: function (data) {

                        response($.map(data, function (item) {
                            return {
                                value: item.value,
                                id: item.id     // EDIT
                            }
                        }));

                    },

                });
            },
            select: function (event, ui) {
                var cityid = ui.item.id;

                exist_city_arr.push(cityid);
                
                $("#branch_city_ids").val(exist_city_arr);
                
                $(this).val(ui.item.value);
            }
        });
    });
    src_county_name = "{{ url('customer/city/autocompletecountyname') }}";
    $('input[id^="county_val"]').on("focus.autocomplete", null, function () {

        $(this).autocomplete({

            source: function (request, response) {
                $.ajax({
                    url: src_county_name,
                    dataType: "json",
                    data: {
                        term: request.term
                    },
                    beforeSend: function () {
                        $('.bcountyloader').show();
                    },
                    complete: function () {
                        $('.bcountyloader').hide();
                    },
                    success: function (data) {

                        response($.map(data, function (item) {
                            return {
                                value: item.value,
                                id: item.id     // EDIT
                            }
                        }));

                    },

                });
            },
            select: function (event, ui) {
                var countyid = ui.item.id;

                exist_county_arr.push(countyid);

                $("#branch_county_ids").val(exist_county_arr);
                $(this).val(ui.item.value);
            }
        });
    }); 

    //Registered Agents
var agentCount = 1;

$('.agent-duplicate-button').click(function () {
    var agentDiv = '<div>' +
            '<div class="col-md-4 col-sm-4">' +
            '<div class="input-wrapper full-width">' +
            '<div class="styled-input">' +
            '<input type="text" name="agent_first_name[]" class="form-control" value=""placeholder=""  data-parsley-trigger="change focusout" data-parsley-pattern="/^[a-zA-Z ]*$/">' +
            '<label>First Name</label>' +
            '<span></span>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-4 col-sm-4">' +
            '<div class="input-wrapper full-width">' +
            '<div class="styled-input">' +
            '<input type="text" name="agent_last_name[]" class="form-control" value="" placeholder="" data-parsley-trigger="change focusout" data-parsley-pattern="/^[a-zA-Z ]*$/">' +
            '<label>Last Name</label>' +
            '<span></span>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div class="col-md-4 col-sm-4">' +
            '<div class="input-wrapper full-width">' +
            '<div class="styled-input">' +
            '<input type="text" name="agent_title[]" class="form-control" value="" placeholder=""  data-parsley-trigger="change focusout" data-parsley-pattern="/^[a-zA-Z ]*$/">' +
            '<label>Title</label>' +
            '<span></span>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>';

    if (agentCount < agent_limit) {
        var eleLength = $('#reg_agents')[0].childElementCount;
        var element = $('#reg_agents')[0].children;

        $(element).each(function (index) {

            if (index === eleLength - 1) {
                // $(this).attr('id','quetions_'+ counter);
                $(this).after(agentDiv + '<div class="rghtagent"><i id="agent' + agentCount + '" class="fa fa-remove removeAgent" /></div>');

            }
            $('input,textarea').keyup(function () {
                if ($(this).val().length > 0) {
                    $(this).addClass('not-empty');

                } else {
                    $(this).removeClass('not-empty');

                }
            });
        });
        agentCount++;
    } else {
        alert('You can add officers upto 3 only');
    }
    $('input,textarea').keyup(function () {
        if ($(this).val().length > 0) {
            $(this).addClass('not-empty');

        } else {
            $(this).removeClass('not-empty');

        }
    });

});

    var counter = 1;
    var x = 1;
    var company_limit = 3;
    var agent_limit = 3;
    var repres_limit = 3;
    // var questions = $('#html-div').html();


    $('.screens-duplicate-button').click(function ()
    {
        var questions = '<div class="styled-input">' +
                '<input type="email" name="company_email[]" id="company_email"  class="form-control email_validation"  value="" data-parsley-trigger="change focusout" data-parsley-type="email"/>'
        '<label></label> <span></span>' +
                '</div>';

        if (x < company_limit)
        {
            counter++;
            //$('#html-div').append(questions);
            var eleLength = $('#html-div')[0].childElementCount;
            var element = $('#html-div')[0].children;
            var i = 0;
            $(element).each(function (index) {

                if (index === eleLength - 1) {
                    // $(this).attr('id','quetions_'+ counter);
                    $(this).after(questions + '<i id="additionalEmail' + x + '" class="fa fa-remove removeAdditionalEmail" />');

                }
                i++;
            });
            x++;
        } else {
            alert('You can add company email upto 3 only');
        }
        $('input').keyup(function () {
            if ($(this).val().length > 0) {
                $(this).addClass('not-empty');
                $('.email_validation').attr('data-parsley-type', 'email')
                        .attr('data-parsley-trigger', 'change focusout')
                        .parsley();
            } else {
                $(this).removeClass('not-empty');
                $('.email_validation').removeAttr('data-parsley-type', 'email')
                        .removeAttr('data-parsley-trigger', 'change focusout')
                        .parsley().destroy();
            }
        });
    });

    //Representative
    var represCount = 1;
    // var representativeDiv = $('#representative').html();

    $('.representative-duplicate-button').click(function () {
        var representativeDiv = '<div class="representativeDivs full-width">' +
                '<div class="col-md-6 col-sm-6">' +
                '<div class="input-wrapper full-width">' +
                '<div class="styled-input">' +
                '<input type="text" name="repres_contact_person[]" class="form-control" value="" data-parsley-trigger="change focusout" data-parsley-pattern="/^[a-zA-Z ]*$/">' +
                '<label>Contact Person</label>' +
                '<span></span>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="col-md-6 col-sm-6">' +
                '<div class="input-wrapper full-width">' +
                '<div class="styled-input">' +
                '<input type="text" name="repres_company_branch_name[]" class="form-control" value="" data-parsley-trigger="change focusout" data-parsley-pattern="/^[a-zA-Z ]*$/">' +
                '<label>Position</label>' +
                '<span></span>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="col-md-6 col-sm-6">' +
                '<div class="input-wrapper full-width">' +
                '<div class="styled-input">' +
                '<input type="email" name="repres_email[]" class="form-control email_validation" value="" data-parsley-trigger="change focusout" data-parsley-type="email">' +
                '<label>Email Address</label>' +
                '<span></span>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '<div class="col-md-6 col-sm-6">' +
                '<div class="input-wrapper full-width">' +
                '<div class="styled-input">' +
                '<input type="text" name="repres_mobile_number[]" class="form-control mobile_validation mobile_no_masking" value="" data-parsley-trigger="change focusout">' +
                '<label>Mobile Number</label>' +
                '<span></span>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>';


        if (represCount < repres_limit) {
            var eleLength = $('#representative')[0].childElementCount;
            var element = $('#representative')[0].children;
            $(element).each(function (index) {

                if (index === eleLength - 1) {
                    // $(this).attr('id','quetions_'+ counter);
                    $(this).after(representativeDiv + '<div class="rght"><i id="repres' + represCount + '" class="fa fa-remove removeRepresentative" /></div>');
                    // $(this).append();
                }
                $('input').keyup(function () {
                    if ($(this).val().length > 0) {
                        $(this).addClass('not-empty');
                        $('.email_validation').attr('data-parsley-type', 'email')
                                .attr('data-parsley-trigger', 'change focusout')
                                .parsley();

    //                    $('.mobile_validation').attr('data-parsley-type','digits')
    //                                        .attr('data-parsley-trigger','change focusout')
    //                                        .parsley();
                    } else {
                        $(this).removeClass('not-empty');
                        $('.email_validation').removeAttr('data-parsley-type', 'email')
                                .removeAttr('data-parsley-trigger', 'change focusout')
                                .parsley().destroy();

    //                     $('.mobile_validation').removeAttr('data-parsley-type','digits')
    //                                        .removeAttr('data-parsley-trigger','change focusout')
    //                                        .parsley().destroy();
                    }
                });
                $('.mobile_no_masking').mask("(999) 999-9999");
            });
            represCount++;
        }
    });

    $(document).on('click', '.removeAdditionalEmail', function () {
        $('#' + $(this).attr('id')).parent("div").remove();
        //$('#' + $(this).attr('id')).remove();
        company_limit++;
    });
    $(document).on('click', '[id^="agent"].removeAgent', function () {
        $('#' + $(this).attr('id')).parent().prev("div").remove();
        $('#' + $(this).attr('id')).remove();
        agent_limit++;
    });

    //remove existing agents from db
    $(document).on('click', '.removeExistingAgent', function () {
        var agentId = $(this).data('id');
        var parentDiv = $(this).parent();

        if (confirm("You are removing Officer/Director record. Are you sure?") == true) {

            $.ajax({
                url: remove_agent+'/'+agentId,
                success: function (data) {
                    if(data.result == true) {
                        parentDiv.prev("div").remove();
                        parentDiv.remove();
                    } else {
                        alert('Error while removing...');
                    }
                },
            });
        } 
    });

    $(document).on('click', '[id^="repres"].removeRepresentative', function () {
        $('#' + $(this).attr('id')).parent().prev("div").remove();
        $('#' + $(this).attr('id')).parent().remove();
        repres_limit++;
    });
    //remvoe existing representative from db
    $(document).on('click', '.removeExistingRepresentative', function () {
        var agentId = $(this).data('id');
        var parentDiv = $(this).parent();

        if (confirm("You are removing representative record. Are you sure?") == true) {
            $.ajax({
                url: remove_representative+'/'+agentId,
                success: function (data) {
                    if(data.result == true) {
                        parentDiv.prev("div").remove();
                        parentDiv.remove();
                    } else {
                        alert('Error while removing...');
                    }
                },
            });
        } 
    });
});

</script>
@stop
