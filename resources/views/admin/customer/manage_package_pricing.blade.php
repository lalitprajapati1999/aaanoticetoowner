@extends('adminlte::page')
@section('content')

<div>
    <section class="address-book network manage-subscr">
        <div class="dashboard-wrapper">

            <div class="customer-status"></div>
            <div class="dashboard-heading">
                <h1><span>Customer Subscriptions Packages</span></h1>
            </div>

            <div class="dashboard-inner-body">
                <form method="POST" class="form" id="customer-subscription-form" data-parsley-validate action="{{ url('admin/customer/save-package-pricing') }}">
                    {!! csrf_field() !!}
                    <input type="hidden" name="id" value="<?php echo($id); ?>" />
                    <div class="project-wrapper">
                        <div class="dash-subform">
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="row">
                                        <div class="col-md-5 col-sm-6">
                                            <div class="select-opt">
                                                <label class="col-md-12 control-label" for="package_type">Package Type</label>

                                                <select name="package_type" class="selectpicker" data-parsley-required='true'  data-parsley-required-message="Package Type is required" data-parsley-trigger="change focusout" id="package_type">


                                                    <option value="">Select Package Type</option>
                                                    @if(isset($package) && !empty($package))


                                                    @foreach($package As $eachpackage)
                                                        
                                                        
                                                    <option value="{{$eachpackage->id}}"  @if(isset($selected_pkg[0]['id']) && $selected_pkg[0]['id']== $eachpackage->id) selected @endif>{{$eachpackage->name}}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                                @if ($errors->has('package_type'))
                                                <p class="help-block"> <strong>{{ $errors->first('package_type') }}</strong> </p>
                                                @endif
                                            </div>
                                        </div>
                                        <!--div class="col-md-6 col-sm-6 hide">
                                            <div class="select-opt">
                                                <label class="col-md-12 control-label" for="state">State</label>
                                                <select name="state" class="selectpicker" data-parsley-required='true'  data-parsley-required-message="State is required" data-parsley-trigger="change focusout" id="state">
                                                    @if(isset($state) && !empty($state))
                                                    @foreach($state As $eachState)
                                                    <option value="{{$eachState->id}}">{{$eachState->name}}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                                @if ($errors->has('state'))
                                                <p class="help-block"> <strong>{{ $errors->first('state') }}</strong> </p>
                                                @endif
                                            </div>
                                        </div -->
                                    </div>

                                    <div id="loadinghtml"></div>

                                    <div class="form-bottom-btn" id="savesubscription" style="display: none;">
                                        <a href="{{url('admin/dashboard')}}" class="btn btn-primary custom-btn customb-btn" ><span>Cancel</span></a>
                                        <button  type="submit" name="savesubscription" class="btn btn-primary custom-btn customc-btn" value="Save Subscription"><span>Save Subscription</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>
@stop
@section('frontend_js')
<script type="text/javascript">
    $(document).ready(function () {
    $(document).on("click", ".add_range", function () {
            var notice_id = $(this).attr("data-id");
            var additional_address = $(this).attr("data-additional-address");
            if(!additional_address){
            var price_range_row = '<input type="hidden" value="0" name="notice['+notice_id+'][id][]">'+'<div class="full-width">'+ '<input type="hidden" name="notice['+notice_id+'][additional_address][]" value="'+additional_address+'">'+
                                        '<div class="col-md-3 col-sm-6">'+
                                            '<div class="input-wrapper full-width">'+
                                                '<div class="styled-input">'+
                                                    '<textarea name="notice['+notice_id+'][description][]" class="form-control" autocomplete="off" ></textarea>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-2 col-sm-6">'+
                                            '<div class="input-wrapper full-width">'+
                                                '<div class="styled-input">'+
                                                    '<input type="text" min="1" name="notice['+notice_id+'][low_limit][]" value=""  class="form-control not-empty" data-parsley-trigger="change focusout" data-parsley-type="digits" autocomplete="off" >'+
                                                    '<span></span>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-2 col-sm-6">'+
                                            '<div class="input-wrapper full-width">'+
                                                '<div class="styled-input">'+
                                                    '<input type="text"  name="notice['+notice_id+'][high_limit][]" value=""  class="form-control not-empty"  data-parsley-trigger="change focusout" autocomplete="off">'+
                                                    '<span></span>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-2 col-sm-6">'+
                                            '<div class="input-wrapper full-width">'+
                                                '<div class="styled-input">'+
                                                    '<input type="text" min="0" name="notice['+notice_id+'][charges][]" value="0" class="form-control not-empty" data-parsley-trigger="change focusout" data-parsley-type="number" autocomplete="off">'+
                                                    '<span></span>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-2 col-sm-6">'+
                                            '<div class="input-wrapper full-width">'+
                                                '<div class="styled-input">'+
                                                    '<input type="text" min="0" name="notice['+notice_id+'][nto_cancellation_charge][]" value="0" class="form-control not-empty" data-parsley-trigger="change focusout" data-parsley-type="number" id="nto_cancellation_charge" autocomplete="off">'+
                                                    '<span></span>'+
                          

                                                '</div>'+
                                            '</div>'+
                                        '</div>'+ 
                                    '</div>';
                                }else{
                                     var price_range_row = '<input type="hidden" value="0" name="notice['+notice_id+'][id][]">'+'<div class="full-width">'+ '<input type="hidden" name="notice['+notice_id+'][additional_address][]" value="'+additional_address+'">'+
                                        '<div class="col-md-3 col-sm-6">'+
                                            '<div class="input-wrapper full-width">'+
                                                '<div class="styled-input">'+
                                                    '<textarea name="notice['+notice_id+'][description][]" class="form-control" autocomplete="off" ></textarea>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-2 col-sm-6">'+
                                            '<div class="input-wrapper full-width">'+
                                                '<div class="styled-input">'+
                                                    '<input type="text" min="1" name="notice['+notice_id+'][low_limit][]" value=""  class="form-control not-empty" data-parsley-trigger="change focusout" data-parsley-type="digits" autocomplete="off" >'+
                                                    '<span></span>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-2 col-sm-6">'+
                                            '<div class="input-wrapper full-width">'+
                                                '<div class="styled-input">'+
                                                    '<input type="text" name="notice['+notice_id+'][high_limit][]" value=""  class="form-control not-empty"  data-parsley-trigger="change focusout"  autocomplete="off">'+
                                                    '<span></span>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-2 col-sm-6">'+
                                            '<div class="input-wrapper full-width">'+
                                                '<div class="styled-input">'+
                                                    '<input type="text" min="0" name="notice['+notice_id+'][charges][]" value="0" class="form-control not-empty" data-parsley-trigger="change focusout" data-parsley-type="number" autocomplete="off">'+
                                                    '<span></span>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-2 col-sm-6" hidden>'+
                                            '<div class="input-wrapper full-width">'+
                                                '<div class="styled-input">'+
                                                    '<input type="text" min="0" name="notice['+notice_id+'][nto_cancellation_charge][]" value="0" class="form-control not-empty" data-parsley-trigger="change focusout" data-parsley-type="number" id="nto_cancellation_charge" autocomplete="off">'+
                                                    '<span></span>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+ 
                                    '</div>'+
                                    '</div>';
                                }
            $(price_range_row).insertBefore($(this));
    });
     $(document).ready(function () {
            var id = '<?php echo($id); ?>';
            var packageId = $("#package_type").val();
            if (packageId > 0 && id > 0) {
                loadPackageDetails(packageId, id);
            }
            function checkForInput(element) {
                if ($(element).val().length > 0) {
                    $(element).addClass('not-empty');
                } else {
                    $(element).removeClass('not-empty');
                }
            }
             $('input, textarea').each(function () {
                checkForInput(this);
            });

            // The lines below (inside) are executed on change & keyup
            $('input, textarea').on('change keyup', function () {
                checkForInput(this);
            });
        });
        $(document).on("change", "#package_type", function () {
            var id = '<?php echo($id); ?>';
            var packageId = $("#package_type").val();
            if (packageId > 0 && id > 0) {
                loadPackageDetails(packageId, id);
            }
            // $('#customer-subscription-form').parsley().destroy();
            function checkForInput(element) {
                if ($(element).val().length > 0) {
                    $(element).addClass('not-empty');
                } else {
                    $(element).removeClass('not-empty');
                }
            }
            $('input, textarea').each(function () {
                checkForInput(this);
            });

            // The lines below (inside) are executed on change & keyup
            $('input, textarea').on('change keyup', function () {
                checkForInput(this);
            });
        });
        $(document).on("change", "#state", function () {
            var id = '<?php echo($id); ?>';
            var packageId = $("#package_type").val();
            if (packageId > 0 && id > 0) {
                loadPackageDetails(packageId, id);
            }
        });

        /*  $('#customer-subscription-form').on('submit', function (event) {
         event.preventDefault();
         var submit_url = $(this).attr("action");
         $(".errorsection").each(function () {
         $(this).html("");
         });
         $.ajax({
         type: 'POST',
         url: submit_url,
         dataType: "json",
         async: false,
         cache: false,
         data: $("#customer-subscription-form").serialize(),
         success: function (response) {
         if (response.status == true) {
         var div = $('<div/>', {
         class: 'alert alert-success',
         title: 'now this div has a title!'
         });
         div.html(response.message);
         $(".customer-status").html(div);
         $('html, body').animate({
         'scrollTop': $("body").position().top
         });
         setTimeout(function () {
         location.reload();
         }, 2000);
         } else {
         var div = $('<div/>', {
         class: 'alert alert-error',
         title: 'now this div has a title!'
         });
         div.html(response.message);
         $(".customer-status").html(div);
         $('html, body').animate({
         'scrollTop': $(".customer-status").position().top
         });
         
         }
         }, error: function (response) {
         if (response.status === 404 && response.responseJSON.status == false) {
         $.each(response.responseJSON.error, function (key, value) {
         $(".errorsection").each(function () {
         var name = $(this).data('name');
         var index = $(this).data('index');
         if (index >= 0) {
         index = "." + index;
         }
         if (name + index == key) {
         $(this).html(value).show();
         }
         });
         });
         }
         }
         });
         
         });*/


    });

    function loadPackageDetails(packageId, id) {
        $("#loadinghtml").html("");
        var widget_url = '<?php echo URL('admin/customer/load-package-pricing'); ?>';
        $.ajax({
            type: 'GET',
            url: widget_url,
            dataType: "json",
            async: false,
            cache: false,
            data: {package_id: packageId, id: id},
            success: function (response) {
                if (response.status == true) {
                    $("#loadinghtml").html(response.html);

                    $('#loadinghtml').find(".selectpicker").each(function () {
                        $(this).selectpicker('refresh')
                    });
                    $("#savesubscription").show();
                    $("#customer-subscription-form").parsley();
                } else {
                    $("#savesubscription").hide();
                }
            }
        });
        return true;
    }
</script>
<!--
<script type="text/javascript">
    $(document).ready(function () {
        $(document).on("click", ".add_basepackage", function () {
            var clone = $(this).parents("#parent_basepackage").find("#makeclone_basepackage").clone(true);
            clone.attr("id", "");
            clone.find("input[type='text']").val("");
            clone.find("#nto_pricing_id").val(0);
            clone.find(".remove_basepackage").removeClass('hide');
            clone.find("textarea").val("");
            $(this).parents("#parent_basepackage").append(clone);

            /*setting the idexes for the server side*/
            var index = 0;
            $(this).parents("#parent_basepackage").find(".base_package").each(function () {
                $(this).find(".errorsection").each(function () {
                    $(this).attr('data-index', index);
                });
                index = +index + +1;
            });

        });

        $(document).on("click", ".remove_basepackage", function () {
            $(this).parents(".base_package").remove();
            /*setting the idexes for the server side*/
            var index = 0;
            $(this).parents("#parent_basepackage").find(".base_package").each(function () {
                $(this).find(".errorsection").each(function () {
                    $(this).attr('data-index', index);
                });
                index = +index + +1;
            });
        });
        // var counter = 1;
        $(document).on("click", ".add_noticecharge", function () {
            // counter += 1;
            $(this).parents("#parent_noticecharge").find("#makeclone_noticecharge").find('.selectpicker').selectpicker('destroy', true);
            var clone = $(this).parents("#parent_noticecharge").find("#makeclone_noticecharge").clone(true);
            clone.attr("id", "");
            clone.find("input[type='text']").val("");
            clone.find("#per_notice_pricing_id").val(0);
            clone.find(".remove_noticecharge").removeClass('hide');
            clone.find("textarea").val("");
            //  clone.attr('id', 'select' + counter);
            //clone.find('select').selectpicker();
            $(this).parents("#parent_noticecharge").append(clone);
//            clone.find('.selectpicker').selectpicker('destroy',true);
//           clone.find('.selectpicker').selectpicker();
            /*setting the idexes for the server side*/
            var index = 0;
            $(this).parents("#parent_noticecharge").find(".noticechargeparent").each(function () {
                $(this).find(".errorsection").each(function () {
                    $(this).attr('data-index', index);
                });
                index = +index + +1;
            });
            $(this).parents("#parent_noticecharge").find('select').each(function () {
                $(this).addClass('selectpicker').selectpicker();
            });
        });

        $(document).on("click", ".remove_noticecharge", function () {
            $(this).parents(".noticechargeparent").remove();
            /*setting the idexes for the server side*/
            var index = 0;
            $(this).parents("#parent_noticecharge").find(".noticechargeparent").each(function () {
                $(this).find(".errorsection").each(function () {
                    $(this).attr('data-index', index);
                });
                index = +index + +1;
            });
        });

        $(document).on("change", "#monthly_charge", function () {
            var totalCharges = 0;
            var monthlyCharges = $(this).val();
            if (monthlyCharges > 0) {
                totalCharges = +monthlyCharges * 12;
            }
            $('#total_charge').val(totalCharges);
        });
    });

    $(document).ready(function () {
        function checkForInput(element) {
            console.log($(element).val().length);
            if ($(element).val().length > 0) {
                console.log('in if');
                $(element).addClass('not-empty');
            } else {
                $(element).removeClass('not-empty');
            }
        }


    });

</script>
-->
@stop

