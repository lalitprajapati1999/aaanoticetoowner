<div class="action-icon">
         <a href="{{url('admin/customer/view/'.$customer->id)}}" class="btn btn-secondary item red-tooltip" data-toggle="tooltip" data-placement="bottom" title="View"><span class="icon-dark-eye"></span></a>

    @if(Auth::user()->hasRole('account-manager'))
        <a href="{{url('contacts/'.encrypt($customer->id))}}" class="btn btn-secondary item red-tooltip" data-toggle="tooltip" data-placement="bottom" title="Address Book"><span class="dashboard-icon icon-receipt"></span></a>
    @endif

    @if($customer->status == '0')
    <a href="#" class="btn btn-secondary item red-tooltip change-status" data-id="{{$customer->id}}" data-status="1" data-target="#activeDeactivateModal" data-toggle="modal"  data-toggle="tooltip" data-placement="bottom" title="Disable"><span class="icon-on disable"></span></a>
    @else
    <a href="#" class="btn btn-secondary item red-tooltip change-status" data-id="{{$customer->id}}" data-status="0" data-target="#activeDeactivateModal" data-toggle="modal" data-toggle="tooltip" data-placement="bottom" title="Enable"><span class="icon-on"></span></a>
    @endif
    @if(Auth::user()->hasRole('admin'))
    <a href="{{url('admin/customer/'.$customer->id.'/edit')}}" class="btn btn-secondary item red-tooltip" data-toggle="tooltip" data-placement="bottom" title="Edit"><span class="icon-pencil-edit-button"></span></a>
    
    
    <a href="{{url('admin/customer/'.$customer->id.'/manage-package-pricing')}}" class="btn btn-secondary item red-tooltip" data-toggle="tooltip" data-placement="bottom" title="Manage Package"><span class="icon-box"></span></a>
    @endif

    
</div>