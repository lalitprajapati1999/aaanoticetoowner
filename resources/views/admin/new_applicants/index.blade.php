@extends('adminlte::page')

@section('content')
<section class="address-book new-applicants">
    <div class="dashboard-wrapper">
        @if (Session::get('success'))
        <div class="alert alert-success">
            <?php echo Session::get('success'); ?>
        </div>
        @endif
        <div class="newapplicants-success"></div>
        <div class="dashboard-heading">
            <h1><span>New Applicants</span></h1>
        </div>
        <div class="dashboard-inner-body">
            <div class="dahboard-table table-responsive">
                <form action="" enctype="multipart/form-data" method="post">
                    <meta name="_token" content="{{ csrf_token() }}" />
                    <table class="table table-striped">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">Customer Name</th>
                                <th scope="col">Company Name</th>
                                <th scope="col">Mailing Address</th>
                                <th scope="col">CYO</th>
                                <th scope="col">Date Applied</th>
                                <th scope="col">Status</th>
                                <th scope="col">Account Manager</th>
                            </tr>
                        </thead>
                        @if(count($customers)>0)
                        @foreach($customers AS $customer)
                        <tr id="customer_{{$customer->id}}">
                            <td>{{$customer->name}}</td>
                            
                            <td>
                                <a href="{{url('admin/customer/view/'.$customer->id)}}" >
                                    <!--{{$customer->name}}-->
                                     {{$customer->company_name}}
                                </a>
                            </td>
                            <td>{{$customer->mailing_address}}</td>
                            <td>{{$customer->cyo}}</td>
                            <td>{{date('M d,Y', strtotime($customer->created_at))}}</td>
                            
                            <td>
                                <div class="action-icon">
                                    @if($customer->status == '0')
                                    <a href="#" data-target="#activeDeactivateModal" data-toggle="modal" class="btn btn-secondary item  change-status" data-id="{{$customer->id}}" data-status="1"   title="Enable"><span class="icon-on disable"></span></a>
                                    @else
                                    <a href="#" data-target="#activeDeactivateModal" data-toggle="modal" class="btn btn-secondary item  change-status"  data-id="{{$customer->id}}" data-status="0"  title="Disable"><span class="icon-on"></span></a>
                                    @endif

                                </div>
                            </td>
                            <td class="select-opt dashboard-select"> 
                                <select name='acc_manager' class="select_acc_manager selectpicker">
                                    <option value="">Select</option>
                                    @foreach($account_managers AS $account_manager)
                                    <option value='{{$account_manager->id}}'>{{$account_manager->name}}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <td class="text-center" colspan="4">{{'No records found'}}</td>
                        @endif
                        </tbody>
                    </table>
                    <div class="col-md-12">
                        <div class="pagination-div text-right">

                            <ul class="pagination">
                                <!--<img src="{{ URL::to('/') }}/images/left-arrow.png">-->
                                {{ $customers->links() }}
                                <!--<img src="{{ URL::to('/') }}/images/right-arrow.png">-->
                            </ul>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<form action="" method="POST" class="active_deactivate_modal">
        {!! csrf_field() !!}
    <div id="activeDeactivateModal" class="modal fade">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="icon-box">
                        <i class="fa fa-close"></i>
                    </div>				
                    <h4 class="modal-title">Are you sure?</h4>	
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <p>Do you really want to active or deactive the customer?</p>
                </div>
                <div class="modal-footer">
                    <div class="form-bottom-btn">
                        <button type="button" class="btn btn-primary custom-btn customb-btn" data-dismiss="modal"><span>Cancel</span></button>
                        <button type="submit" class="btn btn-primary custom-btn customc-btn" id="confirmChangeStatus"><span>Confirm</span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

@stop
@section('frontend_js')
<script type="text/javascript">
    $(document).on('click', '.remove-attachment', function () {
        var id = $(this).attr('data-id');
        var url = $(this).attr('data-url');
        $(".remove-attachment-model").attr("action", url);
    });
    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var newApplicantsTable = $('#new-applicantsdata').DataTable({
            // "pageLength":10
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{url('admin/new-applicants/custom-filter')}}"
            },
            columns: [
                {data: 'name', name: 'name'},
                {data: 'created_date', name: 'created_date'},
                {data: 'status', name: 'status'},
                {data: 'acc_manager', name: 'acc_manager'}
            ]

        });
    });
    $('.select_acc_manager').change(function () {
        var selectedAccManager = $(this).val();
        if (selectedAccManager != '') {
            var current_row_value = $(this).closest('tr').attr('id');
            var customer_arr = current_row_value.split('_');
            var customer_id = customer_arr[1];
            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': $('meta[name=_token]').attr('content')
                }
            });
            $.ajax({
                url: "{{url('admin/new-applicants/assign/accountmanager')}}", // point to server-side PHP script
                data: {acc_manager: selectedAccManager, customer_id: customer_id},
                type: 'POST',
                // contentType: false, // The content type used when sending data to the server.
                // cache: false, // To unable request pages to be cached
                // processData: false,
                success: function (response) {

                    if (response.status == true) {
                        var div = $('<div/>', {
                            class: 'alert alert-success',
                            title: 'now this div has a title!'
                        });
                        div.html(response.message);
                        $(".newapplicants-success").html(div);
                        $('html, body').animate({
                            'scrollTop': $("body").position().top
                        });
                    //    setTimeout(function () {
                    //        location.reload();
                    //    }, 2000);
                    }
                }
            });
        }
    });
//    function changeStatus(customerId, status) {
//    $.ajax({
//    data: {'customer_id': customerId, 'status': status},
//            type: "post",
//            url: "{{url('admin/customer/changestatus') }}",
//            headers: {
//            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//            },
//            success: function (response) {
//
//            if (response.status == true) {
//            var div = $('<div/>', {
//            class: 'alert alert-success',
//                    title: 'now this div has a title!'
//            });
//            div.html(response.message);
//            $(".customer-status").html(div);
//            $('html, body').animate({
//            'scrollTop': $("body").position().top
//            });
////                        setTimeout(function () {
////                            location.reload();
////                        }, 2000);
//            }
//
//            }
//    });

//    }

    $(document).on('click', '.change-status', function () {
        var url = "{{url('admin/customer/changestatus') }}";
        var customerId = $(this).attr('data-id');
        var status = $(this).attr('data-status');

        $(".active_deactivate_modal").attr("action", url);
        $('body').find('.active_deactivate_modal').append('<input name="customer_id" type="hidden" value="' + customerId + '">');
        $('body').find('.active_deactivate_modal').append('<input name="_method" type="hidden" value="POST">');
        $('body').find('.active_deactivate_modal').append('<input name="status" type="hidden" value="' + status + '">');
    });
//    $('#confirmChangeStatus').on('submit', function (e) {
//        e.preventDefault();
//       
//        var form_data = new FormData();
//        form_data.append('customer_id', $("input[name=customer_id]").val());
//        form_data.append('status', $("input[name=status]").val());
//        $.ajaxSetup({
//            headers: {
//                'X-CSRF-Token': $('meta[name=_token]').attr('content')
//            }
//        });
//        var submit_url = $(this).attr("action");
//        $.ajax({
//            type: 'POST',
//            url: submit_url,
//            dataType: "json",
//            async: false,
//            cache: false,
//            processData: false,
//            contentType: false,
//            data: form_data,
//            success: function (response) {
//              
//                if (response.success) {
//
//                   $('.newapplicants-success').html('Customer status successfully changed')
//                }else if(response.error) {
//
//                    $.each(response.error, function (key, value) {
//
//                        $('#noteerrormessage').css('display', 'block');
//                        $('#noteerrormessage').html(value);
//                    });
//
//                }
//            }
//        });
//        
//    });
</script>
@stop