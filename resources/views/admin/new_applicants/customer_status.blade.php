<div class="action-icon">
@if($customer->status == '0')
    <a href="#" class="btn btn-secondary item red-tooltip" onclick="changeStatus({{$customer->id}},1)" data-toggle="tooltip" data-placement="bottom" title="Disable"><span class="icon-on disable"></span></a>
    @else
    <a href="#" class="btn btn-secondary item red-tooltip" data-toggle="tooltip" onclick="changeStatus({{$customer->id}},0)" data-placement="bottom" title="Enable"><span class="icon-on"></span></a>
    @endif
</div>