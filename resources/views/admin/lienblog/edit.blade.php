@extends('adminlte::page')

@section('content')
<section class="work-order">
    <div class="dashboard-wrapper">
        <div class="dashboard-heading">
            <h1><span>Update Lien Blog</span></h1>
        </div>

        <div class="dashboard-inner-body">
            <!--data-parsley-validate=""-->
            <form id="" data-parsley-validate="" class="form" action="{{ url('admin/lien_blog/update') }}" method="post">
                {!! csrf_field() !!}
                @if (Session::get('success'))
                <div class="no-margin alert alert-success">
                    <?php echo Session::get('success'); ?>
                </div>
                @endif
                @if(session()->has('error'))
                <div class="no-margin alert alert-danger">
                   <?php echo Session::get('error'); ?>
                </div>
                @endif
                <div class="project-wrapper">
                    <div class="dash-subform">
                        <div class="row">
                           <div class="col-md-12 col-sm-12">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="question" value="{{ old('question')?old('question'):$lienblog->question}}" class="form-control" required data-parsley-required-message="Content is required" data-parsley-trigger="change focusout">
                                                <label>Question<span class="mandatory-field">*</span></label>
                                                <span></span>
                                           
                                            @if ($errors->has('question'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('question') }}</strong>
                                            </p>
                                            @endif
                                             </div>
                                        </div>
                                    </div>
<!--                                     <div class="col-md-6 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="answer" value="{{ old('answer')?old('answer'):$lienblog->answer}}" class="form-control" required data-parsley-required-message="Answer is required" data-parsley-trigger="change focusout"/>
                                                <label>Answer<span class="mandatory-field">*</span></label>
                                                <span></span>
                                           
                                            @if ($errors->has('answer'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('answer') }}</strong>
                                            </p>
                                            @endif
                                             </div>
                                        </div>
                                    </div>-->
                                       </div> 
<div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                    <label class="col-md-12 control-label">Answer<span class="mandatory-field">*</span></label>
                                                 <div class="row">
                                                        <div class="col-md-12">
                                                    <textarea name="answer"  class="notice_template" id="lienblogs" rows="30" cols="50"  data-parsley-required="true" data-parsley-required-message='Answer is required' data-parsley-trigger="change focusout" placeholder="Answer">{{ old('answer')?old('answer'):$lienblog->answer}}"</textarea>

                                                    @if ($errors->has('answer'))
                                                    <p class="help-block">
                                                        <strong>{{ $errors->first('answer') }}</strong>
                                                    </p>
                                                    @endif
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                  
                              
                            </div>
                            <div class="col-md-12 col-sm-12">
                    <div class="row">
                        <div class="form-bottom-btn">
                            <input type="hidden" name="lienblog_id" value="{{$lienblog->id}}"/>
                            <a href="{{ url('admin/lien_blog') }}" class="btn btn-primary custom-btn customb-btn"><span>{{ trans('backpack::base.cancel') }}</span></a>
                            <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>Submit</span></button>
                        </div>
                    </div>
                </div>
                        </div>
                    </div>
                </div>
                
            </form>
        </div>
    </div>
</section>
@endsection
@section('frontend_js')
    <script src="{{ asset( ((config('app.env') == 'local') ? '' : 'public').'/vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset( ((config('app.env') == 'local') ? '' : 'public').'/vendor/unisharp/laravel-ckeditor/adapters/jquery.js') }}"></script>
<script>
$('#lienblogs').ckeditor();
</script>
@stop

