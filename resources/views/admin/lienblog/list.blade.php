@extends('adminlte::page')
@section('content')

<div>
    <section class="address-book">
        <div class="dashboard-wrapper">

            @if (Session::get('success'))
            <div class="alert alert-success">
               <?php echo Session::get('success'); ?>
            </div>
            @endif
            <div class="employee-status"></div>
            <div class="dashboard-heading">
                <h1><span>Lien Blog</span></h1>
            </div>

            <div class="dashboard-inner-body">
                 <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                        <div class="add-new-btn">
                            <a  href="{{url('admin/lien_blog/create')}}" type="button" class="btn btn-primary custom-btn"><span>Add Lien Blog</span></a>
                        </div>
                    </div>
                    </div>
                <div class="dahboard-table table-responsive fixhead-table">
                    <table class="table table-striped" id="employees-data" datatable="" width="100%" cellspacing="0"   data-scroll-x="true"scroll-collapse="false">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">Question</th>
<!--                                <th scope="col">Answer</th>-->
                                <th scope="col">Options</th>
                            </tr>
                        </thead>

                    </table>

                </div>
            </div>
        </div>
    </section>
</div>
<form action="{{url('admin/lien_blog/delete')}}" method="get" class="remove-attachment-model">
       {!! csrf_field() !!}
<div id="attachment-confirmation-modal" class="modal fade">
    <div class="modal-dialog modal-confirm">
        <div class="modal-content">
            <div class="modal-header">
                <div class="icon-box">
                    <i class="fa fa-close"></i>
                </div>
                <input type="text" name="lienblog_id" id="lienblog_id" hidden="">              
                <h4 class="modal-title">Are you sure?</h4>  
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
                <p>Do you really want to delete these records? This process cannot be undone.</p>
            </div>
            <div class="modal-footer">
                <div class="form-bottom-btn">
                      <button type="button" class="btn btn-primary custom-btn customb-btn remove-attachement-from-delete-form" data-dismiss="modal"><span>Cancel</span></button>
                    <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>Confirm</span></button>
                </div>
            </div>
        </div>
    </div>
</div>
</form>
@endsection

@section('frontend_js')
<script type="text/javascript">
     function removelienblog(id){
        $('#lienblog_id').val(id);
    }

    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var employeesTable = $('#employees-data').DataTable({
            // "pageLength":10
            processing: true,
            serverSide: true,
            ordering:false,
            ajax: {
                url: "{{url('admin/lien_blog/custom-filter')}}",
                data: function (d) {
                    d.employee_name = $('input[name=question]').val();
                }
            },
            columns: [
                {data: 'question', name: 'question'},
//                {data: 'answer', name: 'answer'},
                {data: 'action', name: 'action'}
            ]

        });

        $('#employee-name').on('submit', function (e) {
            employeesTable.draw();
            e.preventDefault();
        });


    });
    function changeStatus(lienblogId, status) {
        $.ajax({
            data: {'lienblog_id': lienblogId, 'status': status},
            type: "post",
            url: "{{url('admin/lien_blog/changestatus') }}",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (result) {
                if (result) {
                    $('.employee-status').html('Lien Blog status successfully changed');
                    $('.employee-status').addClass('alert alert-success');
                    window.location.reload();
                }

            }
        });
    }
</script>
@endsection
