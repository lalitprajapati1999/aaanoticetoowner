@extends('adminlte::page')

@section('content')
<section class="work-order">
    <div class="dashboard-wrapper">
        <div class="dashboard-heading">
            <h1><span>Add Lien Blog</span></h1>
        </div>

        <div class="dashboard-inner-body">
            <!--data-parsley-validate=""-->
            <form id="" class="form"  data-parsley-validate="" action="{{ url('admin/lien_blog/store') }}" method="post">
                {!! csrf_field() !!}
                @if (Session::get('success'))
                <div class="no-margin alert alert-success">
                    <?php echo Session::get('success'); ?>
                </div>
                @endif
                @if(session()->has('error'))
                @if ($errors->count())
                <div class="no-margin alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $e)
                        <li>{{ $e }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @endif
                <div class="project-wrapper">
                    <div class="dash-subform">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="question" value="{{ old('question')}}" class="form-control" required data-parsley-required-message="Question is required" data-parsley-trigger="change focusout">
                                                <label>Question<span class="mandatory-field">*</span></label>
                                                <span></span>
                                                @if ($errors->has('question'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('question') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
<!--                                    <div class="col-md-6 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text" name="answer" value="{{ old('answer')}}" class="form-control" required data-parsley-required-message="Answer is required" data-parsley-trigger="change focusout">
                                                <label>Answer<span class="mandatory-field">*</span></label>
                                                <span></span>
                                                 @if ($errors->has('answer'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('answer') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    -->
                                   
                                </div>
                                <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                    <label class="col-md-12 control-label">Answer<span class="mandatory-field">*</span></label>
                                                <div class="row">
                                                        <div class="col-md-12">
                                                    <textarea name="answer"  class="notice_template" id="lienblogs" rows="30" cols="50"  data-parsley-required="true" data-parsley-required-message='Answer is required' data-parsley-trigger="change focusout" placeholder="Answer">{{ old('answer')}}</textarea>

                                                    @if ($errors->has('answer'))
                                                    <p class="help-block">
                                                        <strong>{{ $errors->first('answer') }}</strong>
                                                    </p>
                                                    @endif
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            </div>
                             <div class="col-md-12 col-sm-12">
                    <div class="row">
                        <div class="form-bottom-btn">
                            <a href="{{ url('admin/lien_blog') }}" class="btn btn-primary custom-btn customb-btn"><span>{{ trans('backpack::base.cancel') }}</span></a>
                            <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>Create New Lien Blog</span></button>
                        </div>
                    </div>
                </div>
                        </div>
                    </div>
                </div>
               
            </form>
        </div>
    </div>
</section>
@endsection
@section('frontend_js')
    <script src="{{ asset( ((config('app.env') == 'local') ? '' : 'public').'/vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset( ((config('app.env') == 'local') ? '' : 'public').'/vendor/unisharp/laravel-ckeditor/adapters/jquery.js') }}"></script>
<script>
$('#lienblogs').ckeditor();
</script>
@stop