<div class="action-icon">
     <a href="{{url('admin/lien_blog/view/'.$lienblog->id)}}" class="btn btn-secondary item red-tooltip" data-toggle="tooltip" data-placement="bottom" title="View"><span class="icon-dark-eye"></span></a>
    <a href="{{url('admin/lien_blog/edit/'.$lienblog->id)}}" class="btn btn-secondary item red-tooltip" data-toggle="tooltip" data-placement="bottom" title="Edit"><span class="icon-pencil-edit-button"></span></a>
    @if($lienblog->id != 1)
   <a href="#"  data-target="#attachment-confirmation-modal" onclick="removelienblog(<?php echo $lienblog->id; ?>)" 
           data-toggle="modal"class="btn btn-secondary item red-tooltip" data-toggle="tooltip" data-placement="bottom" data-original-title="Remove"><span class="icon-cross-remove-sign"></span></a>
   @endif
    @if($lienblog->status == '0')
    <a href="#" class="btn btn-secondary item red-tooltip" onclick="changeStatus({{$lienblog->id}},1)" data-toggle="tooltip" data-placement="bottom" title="Disable"><span class="icon-on disable"></span></a>
    @else
    <a href="#" class="btn btn-secondary item red-tooltip" data-toggle="tooltip" onclick="changeStatus({{$lienblog->id}},0)" data-placement="bottom" title="Enable"><span class="icon-on"></span></a>
    @endif

</div>