@extends('adminlte::page')
@section('content')
<section class="address-book-add-readonly">
    <div class="dashboard-wrapper">
        <div class="dashboard-heading">
            <h1><span>View Lien Blog Details</span></h1>
        </div>
        <div class="dashboard-inner-body">
            <div class="dash-subform">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="form-group">
                                    <div class="readonly full-width">
                                        <label class="full-width">Question</label>
                                        <p class="full-width">{{$lienblog->question}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="form-group">
                            <div class="readonly full-width">
                                <label class="full-width">Answer</label>
                                <p class="full-width">{!! $lienblog->answer !!}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-sm-12">
                <div class="row">
                    <div class="form-bottom-btn">
                        <a href="{{URL::previous()}}" class="simple-hrf"><span>Back</span></a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
@endsection