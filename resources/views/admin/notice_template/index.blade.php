@extends('adminlte::page')
@section('content')

<div>
   
    <section class="address-book">
        <div class="dashboard-wrapper">
             @if(Session::get('success'))
        <div class="no-margin alert alert-success">{{ Session::get('success') }}</div>
        @endif
        @if(Session::has('error'))
        <div class="no-margin alert alert-danger">{{ Session::get('error') }}</div>
        @endif
            <div class="dashboard-heading">
                <h1><span>Notice Template</span></h1>
            </div>
            <div class="dashboard-inner-body">
                <form class="" action="" method="post">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                            <div class="add-new-btn">
                                <a  href="{{url('admin/notice_template/create')}}" type="button" class="btn btn-primary custom-btn">
                                    <span>+ Add Notice Template</span></a>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="row">
                     <div class="col-md-6 col-sm-6">
                                  {!! csrf_field() !!}
                               <div class="select-opt">
                                   <?php $old_state = isset($request['state']) ? $request['state'] :''?>
                         <select name="state" class="selectpicker" id="allstates">
                                        <option value="">Select</option>
                                        @foreach($states as $each_state)
                                        <option value="{{$each_state->name}}" {{$old_state == $each_state->id ?'selected="selected"':''}}> {{ucfirst(trans($each_state->name))}}</option>
                                        @endforeach
                                    </select>
                                    
                               </div>
                                  
        </div>
                </div>
               
                <div class="dahboard-table table-responsive fixhead-table">
                    <table class="table table-striped" id="notice-template">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">Notice Name</th>
                                <th scope="col">State</th>
                                <th scope="col">Options</th>
                            </tr>
                        </thead>
                        
                    </table>

                </div>
            </div>
        </div>
    </section>
</div>
<form action="{{url('admin/notice_template/destroy')}}" method="get" class="remove-record-model">
    {!! csrf_field() !!}
    <div id="notice-template-confirmation-modal" class="modal fade">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="icon-box">
                        <i class="fa fa-close"></i>
                    </div>				
                    <h4 class="modal-title">Are you sure?</h4>	
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <input type="text" name="noticetemplate_id" id="noticetemplate_id" hidden="">              
                    <p>Do you really want to delete these records? This process cannot be undone.</p>
                </div>
                <div class="modal-footer">
                    <div class="form-bottom-btn">
                        <button type="button" class="btn btn-primary custom-btn customb-btn remove-data-from-delete-form" data-dismiss="modal"><span>Cancel</span></button>
                        <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>Confirm</span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@stop

@section('frontend_js')
<!-- DATA TABLES SCRIPT -->
<!--<script src="{{ asset('vendor/adminlte/plugins/datatables/jquery.dataTables.js') }}" type="text/javascript"></script>

<script src="{{ asset('vendor/backpack/crud/js/crud.js') }}"></script>
<script src="{{ asset('vendor/backpack/crud/js/form.js') }}"></script>
<script src="{{ asset('vendor/backpack/crud/js/list.js') }}"></script>-->


<!--<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.bootstrap.min.js" type="text/javascript"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js" type="text/javascript"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js" type="text/javascript"></script>
<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js" type="text/javascript"></script>
<script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js" type="text/javascript"></script>
<script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js" type="text/javascript"></script>
<script src="//cdn.datatables.net/buttons/1.2.2/js/buttons.colVis.min.js" type="text/javascript"></script>
-->

<!--<script src="{{ asset('vendor/adminlte/plugins/datatables/dataTables.bootstrap.js') }}" type="text/javascript"></script>-->

<script type="text/javascript">
      $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var noticetemplatesTable = $('#notice-template').DataTable({
            // "pageLength":10
             dom: "<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-6'p>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
            processing: true,
            serverSide: true,
              lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
            ajax: {
                url: "{{url('admin/notice_templates/datatable')}}",
            },
            columns: [
                {data: 'name', name:'name'},
                {data: 'state_name', name:'state_name'},
                {data: 'action', name: 'action'}
            ]

        });

        

    });
    $(function () {
        otable = $('#notice-template').dataTable();
    });
    
      $('#allstates').on('keyup keypress change', function () {
            // otable.search(this.value).draw();
            otable.fnFilter(this.value, 1, true );
        });

 function removenotice(id){
        $('#noticetemplate_id').val(id);
    }
</script>

<!-- CRUD LIST CONTENT - crud_list_scripts stack -->

@endsection
