<div class="action-icon">

    <a href="{{url('admin/notice_template/edit/'.$template->id)}}" class="btn btn-secondary item red-tooltip" data-toggle="tooltip" data-placement="bottom" title="Edit"><span class="icon-pencil-edit-button"></span></a>

    <a onclick="removenotice({{$template->id}})" href="#" class="btn btn-secondary item red-tooltip remove-record" data-toggle='modal' data-toggle="tooltip" data-target='#notice-template-confirmation-modal' data-placement="bottom" title="Remove"><span class="icon-cross-remove-sign"></span></a>
</div>