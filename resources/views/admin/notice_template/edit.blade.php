@extends('adminlte::page')

@section('content')

<section class="notice_template">
    <div class="dashboard-wrapper">
        <div class="dashboard-heading">
            <h1><span>Notice Template</span></h1>
        </div>
        <div class="dashboard-inner-body">
            @if(Session::get('success'))
            <div class="alert alert-success">
                {{ Session::get('success') }}
            </div>
            @endif
            <form method="post" action="{{url('admin/notice_template/update')}}">
                {!! csrf_field() !!}
                <input type="hidden" name="template_id" id="notice-form-edit" data-parsley-validate="" value="{{$notice_template->id}}"/>
                <div class="project-wrapper">
                    <div class="dash-subform">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">

                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="select-opt {{ $errors->has('state') ? 'has-error' : '' }}">
                                            <label class="col-md-12 control-label" for="textinput">State<span class="mandatory-field">*</span></label>
                                            <select name="state" class="selectpicker" data-parsley-required="true"  data-parsley-required-message="State is required" data-parsley-trigger="change focusout" id="notice_state_id">
                                                @foreach($states as $state)
                                                <option value="{{$state->id}}" @if ($state->id == $notice_template->state_id) selected="selected" @endif > {{ucfirst(trans($state->name))}}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('state'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('state') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="select-opt {{ $errors->has('notices') ? 'has-error' : '' }}">
                                            <label class="col-md-12 control-label" for="textinput">Notice<span class="mandatory-field">*</span></label>
                                            <select name="notice_name" class="selectpicker" data-parsley-required="true"  data-parsley-required-message="Notice name is required" data-parsley-trigger="change focusout" id="notice_name">

                                            </select>
                                            @if ($errors->has('notice_name'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('notice_name') }}</strong>
                                            </p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="select-opt {{ $errors->has('state') ? 'has-error' : '' }}">
                                            <label class="col-md-12 control-label" for="textinput">Is Parent Work Order<span class="mandatory-field">*</span></label>
                                            <select name="is_parent_work_order" class="selectpicker" data-parsley-required="true"  data-parsley-required-message="Is parent work order is required" data-parsley-trigger="change focusout" >
                                                <option value="">Select</option>
                                                <option value="1" @if (1 == $notice_template->is_parent_work_order) selected="selected" @endif >Yes</option>
                                                <option value="0"  @if (0 == $notice_template->is_parent_work_order) selected="selected" @endif >No</option>

                                            </select>

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6">
                                                <div class="input-wrapper full-width">
                                                    <div class="styled-input {{ $errors->has('name') ? 'has-error' : '' }}">
                                                        <input type="text" name="notice_custom_name" value="{{ $notice_template->name}}" class="form-control" data-parsley-required="true" data-parsley-required-message="Notice name is required" data-parsley-trigger="change focusout">
                                                        <label>Name<span class="mandatory-field">*</span></label>
                                                        <span></span>
                                                        @if ($errors->has('notice_custom_name'))
                                                        <p class="help-block">
                                                            <strong>{{ $errors->first('notice_custom_name') }}</strong>
                                                        </p>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6">
                                                <div class="input-wrapper full-width">
                                                    <div class="styled-input {{ $errors->has('no_of_days') ? 'has-error' : '' }}">
                                                        <input type="text" name="no_of_days" value="{{ $notice_template->no_of_days}}" class="form-control" data-parsley-required="true" data-parsley-required-message='No of days is required' data-parsley-trigger="change focusout">
                                                        <label>No of Days<span class="mandatory-field">*</span></label>
                                                        <span></span>
                                                        @if ($errors->has('no_of_days'))
                                                        <p class="help-block">
                                                            <strong>{{ $errors->first('no_of_days') }}</strong>
                                                        </p>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12">
                                                <div class="input-wrapper full-width">
                                                    <label class="col-md-12 control-label">Template Content<span class="mandatory-field">*</span></label>
                                                    <div class="row">
                                                    <div class="col-md-12">
                                                        <textarea name="notice_template_content"  class="notice_template" id="notice_template" row="4" cols="50" data-parsley-required="true" data-parsley-required-message='Template content is required' data-parsley-trigger="change focusout">
                                                         {{$notice_template->content}}
                                                        </textarea>

                                                        @if ($errors->has('notice_template_content'))
                                                        <p class="help-block">
                                                            <strong>{{ $errors->first('notice_template_content') }}</strong>
                                                        </p>
                                                        @endif
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12">
                                    <div class="row">
                                        <div class="form-bottom-btn">
                                            <a href="{{url('admin/notice_templates')}}" class="btn btn-primary custom-btn customb-btn"><span>Cancel</span></a>
                                            <input type="submit" class="btn btn-primary custom-btn customc-btn" value="Update Notice Template"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>

@stop

@section('frontend_js')
    <script src="{{ asset( ((config('app.env') == 'local') ? '' : 'public').'/vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset( ((config('app.env') == 'local') ? '' : 'public').'/vendor/unisharp/laravel-ckeditor/adapters/jquery.js') }}"></script>
<script>
$('#notice_template').ckeditor();
$('#notice_state_id').on('change', function () {
var state_id = $('#notice_state_id').val();

$.ajax({
    data: {'state': state_id, },
    type: "post",
    url: "{{ url('admin/notice_template/notices') }}",
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    success: function (res) {
        if (res != '') {
            $.each(res, function (i, obj)
            {

                var div_data = "<option value=" + obj.id + ">" + obj.name + "</option>";

                $(div_data).appendTo('#notice_name');
                $('#notice_name').selectpicker('refresh');
            });
        }
    }
});
});


$(document).ready(function () {
var state_id = $('#notice_state_id').val();
$.ajax({
    data: {'state': state_id, },
    type: "post",
    url: "{{ url('admin/notice_template/notices') }}",
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    success: function (res) {
        console.log(res);
        if (res != '') {
            $.each(res, function (i, obj)
            {
                if('{{$notice_template->notice_id}}' == obj.id){
                var div_data = "<option selected='selected' value=" + obj.id + ">" + obj.name + "</option>";
            }else{
                var div_data = "<option value=" + obj.id + ">" + obj.name + "</option>";
            }
                $(div_data).appendTo('#notice_name');
                $('#notice_name').selectpicker('refresh');
            });
        }
    }
});
});
</script>
@stop
