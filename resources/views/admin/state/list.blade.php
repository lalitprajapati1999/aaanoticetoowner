@extends('adminlte::page')
@section('content')

<div>
    <section class="address-book">
        <div class="dashboard-wrapper">

            @if (Session::get('success'))
            <div class="alert alert-success">
               <?php echo Session::get('success'); ?>
            </div>
            @endif
            <div class="customer-status"></div>
            <div class="dashboard-heading">
                <h1><span>All States</span></h1>
            </div>
            <div class="dashboard-inner-body">
                <div class="row">
                    <div class="col-lg-6 col-md-8 col-sm-8 col-xs-12">
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-5">
                                <div class="form-group">
                                    <label for="">Filter By: </label>
                                </div>
                            </div>
                            <form role="form" method="post" id="state-name">
                                <div class="col-lg-5 col-md-6 col-sm-6 col-xs-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Search State" name="state_name" id="autocompleteStateName">
<!--                                        <span class="form-group-btn">
                                            <button class="btn btn-search" type="submit"><span class="icon-search"></span></button>
                                        </span>-->
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>

            <div class="dashboard-inner-body">
                <div class="row">
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <!--                        <div class="row">
                                                    <div class="col-lg-4 col-md-4 col-sm-2 col-xs-12">
                                                        <div class="form-group">
                                                            <label for="">Filter By: </label>
                                                        </div>
                                                    </div>
                                                    <form role="form" method="post" id="customer-name">
                                                        <div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" placeholder="Customer Name" name="customer_name">
                                                                <span class="form-group-btn">
                                                                    <button class="btn btn-search" type="submit"><span class="icon-search"></span></button>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </form>
                        
                                                </div>-->
                        <div class="col-md-6 col-sm-6">
                            <a href="{{ url('admin/states/create') }}" class="btn btn-default ladda-button" data-style="zoom-in"><span class="ladda-label"><i class="fa fa-plus"></i> {{ trans('backpack::crud.add') }} {{ trans('admin.states') }}</span></a>
                        </div>
                    </div>
                </div>
                <div class="dahboard-table table-responsive">
                    <table class="table table-striped" id="states-data">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">Name</th>
                                <th scop="col">Status</th>
                                <th scope="col">Options</th>
                            </tr>
                        </thead>

                    </table>

                </div>
            </div>
        </div>
    </section>
</div>

@endsection

@section('frontend_js')
<script type="text/javascript">
    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var employeesTable = $('#states-data').DataTable({
            // "pageLength":10
            dom: "<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-6'p>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{url('admin/states/list')}}",
                data: function (d) {
                d.city_name = $('input[name=state_name]').val();
                }
            },
            columns: [
                {data: 'name', name: 'name'},
                {data: 'status', name: 'status'},
                {data: 'action', name: 'action'}
            ]

        });

        $('#state-name').on('submit', function (e) {

            employeesTable.draw();
            e.preventDefault();
        });

    });
    $(function () {
       otable = $('#states-data').dataTable();
    });
    $('#autocompleteStateName').on('keyup keypress change', function () {
     otable.fnFilter(this.value);
    //  otable.fnFilter(this.value, 0, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true);
    });
    function changeStatus(stateId, status) {

        $.ajax({
            data: {'State_id': stateId, 'status': status},
            type: "post",
            url: "{{url('admin/state/changestatus') }}",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (result) {
                if (result) {
                    $('.customer-status').html('State status successfully changed.');
                    $('.customer-status').addClass('alert alert-success');
                  
                    var table = $('#states-data').DataTable();
                    //var dt = $("#table").DataTable();
                    table.ajax.reload(null, false);
                }

            }
        });
    }

</script>
@endsection
