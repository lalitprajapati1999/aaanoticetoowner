@extends('adminlte::page')

@section('content')

<section class="">
    <div class="dashboard-wrapper">
        <div class="dashboard-heading">
            <h1><span>Add States </span></h1>
        </div>
        <div class="dashboard-inner-body">
            @if(Session::get('success'))
            <div class="alert alert-success">
                {{ Session::get('success') }}
            </div>
            @endif
            <form method="post" class="form" id="state-form" data-parsley-validate="" action="{{url('admin/states/store')}}">
                {!! csrf_field() !!}
                <div class="project-wrapper">
                    <div class="dash-subform">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input class="form-control"  type="text" value="{{old('name')}}" name="name" id="name" data-parsley-required="true" data-parsley-required-message="State name is required" data-parsley-trigger="change focusout">
                                                <label>State Name<span class="mandatory-field">*</span></label>
                                                <span></span>
                                                @if ($errors->has('name'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input class="form-control"  type="text" value="{{old('abbr')}}" name="abbr" id="abbr" data-parsley-required="true" data-parsley-required-message="Abbreviation name is required" data-parsley-trigger="change focusout">
                                                <label>Abbreviation<span class="mandatory-field">*</span></label>
                                                <span></span>
                                                @if ($errors->has('abbr'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('abbr') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-md-12 col-sm-12">
                                    <div class="row">
                                        <div class="form-bottom-btn">
                                            <a href="{{url('admin/states')}}" class="btn btn-primary custom-btn customb-btn"><span>Cancel</span></a>
                                            <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>Save State   </span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>

@stop

@section('frontend_js')


</script>
@stop
