<div class="action-icon">
    @if($states->status == '0')
    <a href="#" class="btn btn-secondary item red-tooltip off" onclick="changeStatus({{$states->id}},1)" data-toggle="tooltip" data-placement="bottom" title="Disable" ><span class="icon-on disable" id="{{$states->id}}"></span></a>
    @else
    <a href="#" class="btn btn-secondary item red-tooltip on" data-toggle="tooltip" onclick="changeStatus({{$states->id}},0)" data-placement="bottom" title="Enable"><span class="icon-on" id="{{$states->id}}"></span></a>
    @endif
</div>