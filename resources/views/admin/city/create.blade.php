@extends('adminlte::page')

@section('content')

<section class="">
    <div class="dashboard-wrapper">
        <div class="dashboard-heading">
            <h1><span>Add Cities </span></h1>
        </div>
        <div class="dashboard-inner-body">
            @if(Session::get('success'))
            <div class="alert alert-success">
                {{ Session::get('success') }}
            </div>
            @endif
            <form method="post" class="form" id="city-form" data-parsley-validate="" action="{{url('admin/cities/store')}}">
                {!! csrf_field() !!}
                <div class="project-wrapper">
                    <div class="dash-subform">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="input-wrapper full-width">

                                            <label class="col-md-12 control-label" for="textinput">State<span class="mandatory-field">*</span></label>
                                            <select name="state_id" id="state_id" class= 'selectpicker'  data-parsley-required="true" data-parsley-required-message="States is required">
                                                <option value="">Select States</option>
                                                @foreach($states  as $key=>$value)
                                                <option value="{{$value->id}}">{{$value->name}}</option>
                                                @endforeach
                                            </select>

                                            @if ($errors->has('name'))
                                            <p class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </p>
                                            @endif

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input class="form-control"  type="text" value="{{old('name')}}" name="name" id="name" data-parsley-required="true" data-parsley-required-message="City name is required" data-parsley-trigger="change focusout">
                                                <label>City Name<span class="mandatory-field">*</span></label>
                                                <span></span>
                                                @if ($errors->has('name'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input class="form-control"  type="text" value="{{old('county')}}" name="county" id="county" data-parsley-required="true" data-parsley-required-message="County is required" data-parsley-trigger="change focusout">
                                                <label>County<span class="mandatory-field">*</span></label>
                                                <span></span>
                                                @if ($errors->has('county'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('county') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input class="form-control zip_validation"  type="text" value="{{old('zip_code')}}" name="zip_code" id="zip_code" data-parsley-required="true" data-parsley-required-message="Zip Code is required" data-parsley-trigger="change focusout">
                                                <label>Zip Code<span class="mandatory-field">*</span></label>
                                                <span></span>
                                                @if ($errors->has('zip_code'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('zip_code') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12">
                                    <div class="row">
                                        <div class="form-bottom-btn">
                                            <a href="{{url('admin/cities')}}" class="btn btn-primary custom-btn customb-btn"><span>Cancel</span></a>
                                            <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>Save City   </span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>

@stop

@section('frontend_js')
<script>
 $('input,textarea,select').on('focus', function(e) {
            e.preventDefault();
            $(this).attr("autocomplete", "nope");  
        });
    $(".zip_validation").attr("data-parsley-trigger", "change focusout")
            .attr("data-parsley-type", "digits")
            .attr("data-parsley-minlength", "5")
            .attr("data-parsley-maxlength", "5")
            .parsley();
</script>
@stop
