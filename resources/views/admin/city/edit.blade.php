@extends('adminlte::page')

@section('content')

<section class="">
    <div class="dashboard-wrapper">
        <div class="dashboard-heading">
            <h1><span>Add Cities </span></h1>
        </div>
        <div class="dashboard-inner-body">
            @if(Session::get('success'))
            <div class="alert alert-success">
                {{ Session::get('success') }}
            </div>
            @endif
            <form method="post" class="form" id="city-form" data-parsley-validate="" action="{{url('admin/cities/update/'.$cities->id)}}">
                {!! csrf_field() !!}
                <div class="project-wrapper">
                    <div class="dash-subform">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="input-wrapper full-width">

                                              <div class="styled-input">
                                                <input class="form-control"  type="text" value="{{$cities->states->name}}" name="state" id="state" data-parsley-required="true" data-parsley-required-message="State name is required" data-parsley-trigger="change focusout" readonly="">
                                                <label>State Name</label>
                                                <span></span>
                                                @if ($errors->has('state'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('state') }}</strong>
                                                </p>
                                                @endif
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input class="form-control"  type="text" value="{{$cities->name}}" name="name" id="name" data-parsley-required="true" data-parsley-required-message="City name is required" data-parsley-trigger="change focusout" readonly="">
                                                <label>City Name</label>
                                                <span></span>
                                                @if ($errors->has('name'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input class="form-control"  type="text" value="{{$cities->county}}" name="county" id="county" data-parsley-required="true" data-parsley-required-message="County is required" data-parsley-trigger="change focusout" readonly="">
                                                <label>County</label>
                                                <span></span>
                                                @if ($errors->has('county'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('county') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input class="form-control zip_validation"  type="text" value="{{$cities->zip_code ?$cities->zip_code :'' }}" name="zip_code" id="zip_code" data-parsley-required="true" data-parsley-required-message="Zip Code is required" data-parsley-trigger="change focusout">
                                                <label>Zip Code</label>
                                                <span></span>
                                                @if ($errors->has('zip_code'))
                                                <p class="help-block">
                                                    <strong>{{ $errors->first('zip_code') }}</strong>
                                                </p>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12">
                                    <div class="row">
                                        <div class="form-bottom-btn">
                                            <a href="{{url('admin/cities')}}" class="btn btn-primary custom-btn customb-btn"><span>Cancel</span></a>
                                            <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>Save City   </span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>

@stop

@section('frontend_js')
<script>
    $(".zip_validation").attr("data-parsley-trigger", "change focusout")
            .attr("data-parsley-type", "digits")
            .attr("data-parsley-minlength", "5")
            .attr("data-parsley-maxlength", "5")
            .parsley();
</script>
@stop
