@extends('adminlte::page')
@section('content')

<div>
    <section class="address-book">
        <div class="dashboard-wrapper">

            @if (Session::get('success'))
            <div class="alert alert-success">
               <?php echo Session::get('success'); ?>
            </div>
            @endif
            <div class="customer-status"></div>
            <div class="dashboard-heading">
                <h1><span>All Cities</span></h1>
            </div>
            <div class="dashboard-inner-body">
                <div class="row">
                    <div class="col-lg-6 col-md-8 col-sm-8 col-xs-12">
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-5">
                                <div class="form-group">
                                    <label for="">Filter By: </label>
                                </div>
                            </div>
                            <form role="form" method="post" id="city-name">
                                <div class="col-lg-5 col-md-6 col-sm-6 col-xs-6">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Search City" name="city_name" id="autocompleteCityName">
<!--                                        <span class="form-group-btn">
                                            <button class="btn btn-search" type="submit"><span class="icon-search"></span></button>
                                        </span>-->
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>

            <div class="dashboard-inner-body">
                <div class="row">
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">

                        <div class="col-md-6 col-sm-6">
                            <a href="{{ url('admin/cities/create') }}" class="btn btn-default ladda-button" data-style="zoom-in"><span class="ladda-label"><i class="fa fa-plus"></i> {{ trans('backpack::crud.add') }} City</span></a>
                        </div>
                    </div>
                </div>
                <div class="dahboard-table table-responsive">
                    <table class="table table-striped" id="cities-data">
                        <thead class="thead-dark">
                            <tr>

                                <th scope="col">City</th>
                                <th scope="col">State</th>
                                <th scop="col">County</th>
                                <th scop="col">Zip code</th>
                                <th scop="col">Action</th>
                            </tr>
                        </thead>

                    </table>

                </div>
            </div>
        </div>
    </section>
</div>

@endsection

@section('frontend_js')
<script type="text/javascript">
    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var employeesTable = $('#cities-data').DataTable({
            // "pageLength":10
            dom: "<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-6'p>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{url('admin/get_city/')}}",
                data: function (d) {
                d.city_name = $('input[name=city_name]').val();
                }
            },
            columns: [
                {data: 'name', name: 'name'},
                {data: 'states', name: 'states'},

                {data: 'county', name: 'county'},
                {data: 'zip_code', name: 'zip_code'},
                {data: 'action', name: 'action'}
            ]

        });
        $('#city-name').on('submit', function (e) {

            employeesTable.draw();
            e.preventDefault();
            });
    });
    $(function () {
       otable = $('#cities-data').dataTable();
    });
    $('#autocompleteCityName').on('keyup keypress change', function () {
     otable.fnFilter(this.value);
  //  otable.fnFilter(this.value, 0, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true);
});
</script>
@endsection
