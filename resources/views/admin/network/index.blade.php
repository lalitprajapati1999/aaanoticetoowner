@extends('adminlte::page')

@section('content')
<div role="tabpanel" class="tab-pane active" id="address">
    <section class="network">
        <div class="dashboard-wrapper">
            @if (Session::get('success'))
            <div class="alert alert-success">
               <?php echo Session::get('success'); ?>
            </div>
            @endif
            <div class="network-success"></div>
            <div class="dashboard-heading">
                <h1><span>Network</span></h1>
            </div>
            <div class="dashboard-inner-body">
                <div class="dash-subform">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="sub-section-title">
                                <h2>Account Managers</h2>
                            </div>
                        </div>
                        <div class="netwok-form full-width">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="search-by full-width">
                                    <div class="form-group display-ib pull-left">
                                        <label for="">Search By :</label>
                                    </div>
                                    <div class="form-group display-ib pull-left search-box">
                                        <form method="get" action="{{url('admin/network')}}">
                                         <meta name="csrf-token" content="{{ csrf_token() }}">
                                        <input type="text" name="search_network" class="form-control" placeholder="Name" value="{{(isset($search_network)&&!empty($search_network))?$search_network:""}}" id="autocompleteAccountManager">
                                        <span class="form-group-btn">
                                            <button class="btn btn-search" type="submit"><span class="icon-search"></span></button>
                                        </span>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="sec-netwok">
                                    <div class="row">
                                        <div class="col-md-6 text-left">
                                            <div class="account_manager network-list">
                                                <ul class="checkbox-holder full-width">
                                                   @if(isset($users) && !empty($users))
                                                   @foreach($users AS $user)
                                                    <li id="div_{{$user->id}}">
                                                        <input type="checkbox" class="regular-checkbox" id="{{$user->id}}" name="network_status" value="{{$user->name}}">
                                                        <label for="{{$user->id}}">{{$user->name}}</label>
                                                    </li>
                                                    @endforeach
                                                    @endif
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-md-6 text-left">
                                            <div class="allow-network-list">
                                                <h5>Secondary Network Allowed</h5>
                                                <ul class="secondary_network full-width">
                                                    @if(isset($allowed_network) && !empty($allowed_network))
                                                    @foreach($allowed_network AS $k=>$val)
                                                    <li class="" id="{{$val->id}}">
                                                        {{$val->name}}
                                                        <a onclick="unchecked_network({{$val->id}},'{{$val->name}}')">
                                                            <span>
                                                                <i class="fa fa-window-close"></i>
                                                                <span></span>
                                                            </span>
                                                        </a>
                                                    </li>
                                                    @endforeach
                                                    @endif
                                                </ul>
                                            </div>
                                        </div>
                                         <div class="col-md-12">
                                            <div class="controls form-bottom-btn">
                                            <!--<span class="allow_network_manager">&#62;</span>-->
                                                <button class="btn btn-primary custom-btn customc-btn" name="allow_secondary_network" id="allow_secondary_network"><span>Allow Secondary Network</span></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<!--<div class="row">
    <div class="col-md-12">
        <div class="col-md-5">
            <div class="box">
                <div class="box-header with-border">
                    <div><small><span>Account Managers</span></small></div>
                </div>
                <div class="box-body">
                    <div class="row account_manager">
                        @foreach($users AS $user)
                        <div class="text-center" id="div_{{$user->id}}">
                            <input type="checkbox" id="{{$user->id}}" name="network_status" value="{{$user->name}}">
                            {{$user->name}}
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="box">
                <div class="box-header with-border">
                </div>
                <div class="box-body">
                    <div class="text-center">
                        <span class="allow_network_manager">&#62;</span>
                        <button name="allow_secondary_network" id="allow_secondary_network">Submit </button>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="box">
                <div class="box-header with-border">
                    <div><small><span>Allow Secondary Newtwork</span></small></div>
                </div>
                <div class="box-body">
                    <div class="row secondary_network">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <meta name="csrf-token" content="{{ csrf_token() }}">
</div>-->
@stop
@section('frontend_js')
<script type="text/javascript" src="{{ asset('js/jquery-ui.min.js') }}"></script>

<script type="text/javascript">

    $(document).ready(function () {
        $('#allow_secondary_network').click(function () {
           if($('input:checkbox[name=network_status]:checked').length == 0){
               alert('Please select at least one user.');
           }
            $('input:checkbox[name=network_status]:checked').each(function (index, value) {
                var userid = $(this).attr('id');

                var acc_manager_name = $(this).val();
                $.ajax({
                    data: {'userId': userid},
                    type: "post",
                    url: "{{url('admin/network/allowsecondarynetwork')}}",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (result) {
                        if (result) {
                            $('div #div_' + userid).remove();
                            $('.secondary_network').append("<li class='' id='" + userid + "'>" + acc_manager_name + "<a onclick='unchecked_network(\"" + userid + "\",\"" + acc_manager_name + "\")'><span><i class='fa fa-window-close'></i><span></a></li>");
                            $('.network-success').addClass('alert alert-success');
                            $('.network-success').html('Network allowed successfully');
                        }
                    }
                });

            });
        });
        src = "{{ url('admin/network/searchajaxaccountmanager') }}";
        $("#autocompleteAccountManager").autocomplete({
        source: function(request, response) {
            $.ajax({
                url: src,
                dataType: "json",
                data: {
                    term : request.term
                },
                success: function(data) {
                    response(data);
                },
                 select: function( event, ui) {
                    
                    $("#autocompleteAccountManager").val(ui.item.id);
                }
            });
        },
//        minLength: 3,
       
    });
    });

    function unchecked_network(user_id, acc_manager_name) {
        $.ajax({
            data: {'userId': user_id},
            type: "post",
            url: "{{url('admin/network/removesecondarynetwork')}}",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (result) {
                if (result) {
                    $('.secondary_network #' + user_id).remove();
                    $('.account_manager ul').append(
                        '<li class="" id="div_' + user_id + '">' +
                            '<input type="checkbox" class="regular-checkbox" id="' + user_id + '" name="network_status" value="' + acc_manager_name + '">' +
                            '<label for="' +user_id+ '">' + acc_manager_name + '</label>' +
                        '</li>');
                   $('.network-success').addClass('alert alert-success');
                    $('.network-success').html('Network disallowed successfully');
                     setTimeout(function () {
                            location.reload();
                        }, 2000);
                    }

                }
            });
        }

</script>
@stop