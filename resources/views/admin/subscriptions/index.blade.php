@extends('adminlte::page')
@section('content')

<div>
    <section class="address-book">
        <div class="dashboard-wrapper">

            @if (Session::get('success'))
            <div class="alert alert-success">
                <?php echo Session::get('success'); ?>
            </div>
            @endif
            <div class="customer-status"></div>
            <div class="dashboard-heading">
                <h1><span>Manage Subscriptions Packages</span></h1>
            </div>

            <div class="dashboard-inner-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            {{-- <div class="col-md-4">
                                <div class="form-btn-div">
                                    <a class="btn btn-primary custom-btn form-btn" href="{{url('admin/manage-subscriptions/add-package')}}"><span>+</span>Add Package</a>
                                </div>
                            </div> --}}
                            <div class="col-md-4">  
                                <div class="form-btn-div">
                                    <a class="btn btn-primary custom-btn form-btn" href="{{url('admin/cheque-subscriptions')}}">Check Subscription</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="dahboard-table table-responsive fixhead-table">
                            <table class="table table-striped" id="subscriptions-data" datatable="" width="100%" cellspacing="0"   data-scroll-x="true"scroll-collapse="false">
                                <thead class="thead-dark">
                                    <tr>
                                        <th scope="col">Package Name</th>
                                        <th scope="col">Package Price</th>
                                        <th scope="col">Options</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($package as $item)
                                        
                                   
                                    <tr>
                                        <td>{{$item['name']}}</td>
                                        <td>@if($item['charge'] != null) {{'$'.$item['charge']}} @else {{'NA'}} @endif</td>
                                        <td><div class="action-icon"><a href="{{url('admin/manage-subscriptions/add-package/'.$item['id'])}}" class="btn btn-secondary item red-tooltip" data-toggle="tooltip" data-placement="bottom" title="Edit"><span class="icon-pencil-edit-button"></span></a></div></td>
                                    </tr> 
                                    @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection

@section('frontend_js')
<script type="text/javascript">
    // $(function () {
//        $.ajaxSetup({
//            headers: {
//                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//            }
//        });
//        var manageSubscriptionsTable = $('#subscriptions-data').DataTable({
//            // "pageLength":10
//            processing: true,
//            serverSide: true,
//            ajax: {
//                url: "{{url('admin/manage-subscription/datatable')}}"
//            },
//            columns: [
//                {data: 'package_name', name:'package_name'},
//                {data: 'type', name: 'type'},
//                {data: 'notices_included', name: 'action'},
//                {data: 'option', name: 'option'}
//            ]
//
//        });
//
//    });
//  
</script>
@endsection
