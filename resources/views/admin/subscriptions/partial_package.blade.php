@if(isset($nto) && !empty($nto) && isset($package_id) && (!empty($package_id)) && ($package_id == 1))

    <?php
    $type1 = array();
    $type1[] = array('lower' => '', 'upper' => '', 'charges' => '', 'id' => 0, 'nto_cancellation_charge'=>'', 'description' => '');
    if (isset($result) && (!empty($result)) && ($package_id == 1)) {
        $type1result = array();
        foreach ($result as $r) {
            if ($r->notice_id == 9) {
                $type1result[] = array('lower' => $r->lower_limit, 'upper' => $r->upper_limit, 'charges' => $r->charge, 'id' => $r->id, 'nto_cancellation_charge' => $r->cancelation_charge, 'description' => $r->description);
            }
        }
        if (!empty($type1result)) {
            $type1 = $type1result;
        }
    }
    ?>

    <div id="base_package_view">
        <div id="parent_basepackage">
            <?php
            $x = 0;
            foreach ($type1 as $t) {
                ?>
                <div class="row base_package" id="<?php echo(($x == 0) ? 'makeclone_basepackage' : ''); ?>">
                    <input type="hidden" name="nto_pricing_id[]" value="<?php echo($t['id']) ?>" id="nto_pricing_id" />
                    <div class="col-md-6 col-sm-6">
                        <div class="select-opt">
                            <label class="col-md-12 control-label" for="textinput">Notice<span class="mandatory-field">*</span></label>
                            <select name="notice[]" class="selectpicker" data-parsley-required='true'  data-parsley-required-message="Notice is required" data-parsley-trigger="change focusout" >
                                @foreach($nto As $each_notice)
                                <option value="{{$each_notice->id}}">{{$each_notice->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="input-wrapper full-width">
                            <div class="styled-input">
                                <textarea name="description" class="form-control" ><?php echo($t['description']) ?></textarea>
                                <label>Description<span class="mandatory-field">*</span></label>
                                <span></span>
                                <p class="help-block errorsection" data-name="description" data-index=""></p>
                            </div>
                        </div>
                    </div>
                    <div class="full-width">
                        <div class="col-md-2 col-sm-6">
                            <div class="input-wrapper full-width">
                                <div class="styled-input">
                                        <input type="text" min="1" name="low_limit[]" value="{{ $t['lower'] }}" class="form-control" data-parsley-required="true" data-parsley-required-message="Limit is required" data-parsley-trigger="change focusout" data-parsley-type="digits" >
                                        <label class="">From<span class="mandatory-field">*</span></label>
                                        <span></span>
                                        <p class="help-block errorsection" data-name="low_limit" data-index="<?php echo($x); ?>"></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-6">
                            <div class="input-wrapper full-width">
                                <div class="styled-input">
                                        <input type="text" min="1"  name="high_limit[]" value="{{ $t['upper']}}" class="form-control" data-parsley-required="true" data-parsley-required-message="Limit is required" data-parsley-trigger="change focusout"  data-parsley-type="digits" />
                                        <label class="">To<span class="mandatory-field">*</span></label>
                                        <span></span>
                                        <p class="help-block errorsection" data-name="high_limit" data-index="<?php echo($x); ?>"></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-6">
                            <div class="input-wrapper full-width">
                                <div class="styled-input">
                                    <input type="text"  min="1" name="charges[]" value="{{ $t['charges'] }}" class="form-control" data-parsley-required="true" data-parsley-required-message="charges is required" data-parsley-trigger="change focusout" data-parsley-type="digits">
                                    <label>Charges<span class="mandatory-field">*</span></label>
                                    <span></span>
                                    <p class="help-block errorsection" data-name="charges" data-index="<?php echo($x); ?>"></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-6">
                            <div class="input-wrapper full-width">
                                <div class="styled-input">
                                    <input type="text"  min="0" name="nto_cancellation_charge[]" class="form-control" data-parsley-trigger="change focusout" data-parsley-type="digits" id="nto_cancellation_charge" value="{{ $t['nto_cancellation_charge'] }}" />
                                    <label>Cancellation Charge<span class="mandatory-field">*</span></label>
                                    <span></span>
                                    <p class="help-block errorsection" data-name="nto_cancellation_charge" data-index="<?php echo($x); ?>"></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-6">
                            <a href="javascript:void(0);" class="add_basepackage"><i class="fa fa-plus-square"></i></a>
                            <a href="javascript:void(0);" class="remove_basepackage <?php echo(($x == 0) ? 'hide' : ''); ?>"><i class="fa fa-minus-square"></i></a>
                        </div>
                    </div>
                </div>
                <?php
                $x++;
            }
            ?>
        </div>
    </div>
    @endif

    @if(isset($nto) && !empty($nto) && isset($package_id) && (!empty($package_id)) && ($package_id == 2 || $package_id == 3))

    <?php
    $create_own_charge = "";
    $total = "";
    $description = "";
    $id = 0;
    $x = 0;
    $cancellationCharge = 0;
    $title = "Monthly";
    if (isset($package) && (!empty($package)) && ($package_id == 2 || $package_id == 3)) {
        if ($package_id == 3) {
            $title = "Yearly";
        }

        $create_own_charge = $package->charge;
        $description = $package->description;
        $description = $package->description;
        $id = $package->id;
        $cancellationCharge = $package->cancellation_charge;
    }
    ?>


    <div class="row">
        <input type="hidden" name="own_pricing_id" value="{{ $id }}" />
        <div class="col-md-4 col-sm-4">
            <div class="input-wrapper full-width">
                <div class="styled-input">
                    <input type="text"  min="1" name="create_own_charge" class="form-control" data-parsley-required="true" data-parsley-required-message="Charges is required" data-parsley-trigger="change focusout" data-parsley-type="digits" id="create_own_charge" value="{{ $create_own_charge }}" />
                    <label><?php echo($title) ?> Subscriptions Charge</label>
                    <span></span>
                    <p class="help-block errorsection" data-name="create_own_charge" data-index=""></p>
                </div>
            </div>
        </div>
        <div class="col-md-5 col-sm-5">
            <div class="input-wrapper full-width">
                <div class="styled-input">
                    <textarea name="description" class="form-control" data-parsley-required="true" data-parsley-required-message="description is required" data-parsley-trigger="change focusout">{{ $description }}</textarea>
                    <label>Description</label>
                    <span></span>
                    <p class="help-block errorsection" data-name="description" data-index=""></p>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-3">
            <div class="input-wrapper full-width">
                <div class="styled-input">
                    <input type="text"  min="0" name="cancellation_charge" class="form-control" data-parsley-trigger="change focusout" data-parsley-type="digits" id="cancellation_charge" value="{{ $cancellationCharge }}" />
                    <label>Cancellation Charge</label>
                    <span></span>
                    <p class="help-block errorsection" data-name="cancellation_charge" data-index=""></p>
                </div>
            </div>

        </div>
    </div>
    @endif


    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="sub-section-title">
                <h2 style="text-align: left">Notice Charges </h2>
            </div>
        </div>
    </div>

    <?php
    $pernotice = array();
    $pernotice[] = array('notice_id' => '', 'charges' => '', 'id' => 0, 'description' => '');
    if (isset($result) && (!empty($result))) {
        $type1result = array();
        foreach ($result as $r) {
            if (empty($r->lower_limit) && empty($r->upper_limit) && (!empty($r->notice_id))) {
                $type1result[] = array('notice_id' => $r->notice_id, 'charges' => $r->charge, 'id' => $r->id, 'description' => $r->description);
            }
        }
        if (!empty($type1result)) {
            $pernotice = $type1result;
        }
    }
    ?>

    <div class="basepackage_notice_charges">
        <div id="parent_noticecharge">
            <?php
            $x = 0;
            foreach ($pernotice as $p) {
                ?>
                <div class="noticechargeparent row" id="<?php echo(($x == 0) ? 'makeclone_noticecharge' : ''); ?>">
                    <input type="hidden" name="per_notice_pricing_id[]" value="<?php echo($p['id']) ?>" id="per_notice_pricing_id" />
                    <div class="col-md-4 col-sm-6">
                       <div class="select-opt">
                            <label class="col-md-12 control-label" for="noticechange">Notice<span class="mandatory-field">*</span></label>
                            <select name="noticechange[]" class="selectpicker" data-parsley-required='true'  data-parsley-required-message="Notice is required" data-parsley-trigger="change focusout" >
                                <option value="">Select Notice</option>
                                @if(isset($notice) && !empty($notice) && (isset($nto_id)) && (!empty($nto_id)))
                                
                                @if($package_id == 1)
                                @foreach($notice As $each_notice)
                                @if($each_notice->id != $nto_id)
                                <option value="{{$each_notice->id}}" <?php echo(($p['notice_id'] == $each_notice->id) ? 'selected="selected"' : ''); ?> >{{$each_notice->name}}</option>
                                @endif
                            @endforeach
                                @else 
                                @foreach($notice As $each_notice)
                                    <option value="{{$each_notice->id}}" <?php echo(($p['notice_id'] == $each_notice->id) ? 'selected="selected"' : ''); ?> >{{$each_notice->name}}</option>
                            @endforeach
                                @endif
                                @endif
                            </select>
                            <p class="help-block errorsection" data-name="noticechange" data-index="<?php echo($x); ?>"></p>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="input-wrapper full-width">
                            <div class="styled-input">
                                <input type="text"  min="1" name="noticecharges[]" value="{{ $p['charges'] }}" class="form-control" data-parsley-required="true" data-parsley-required-message="charges is required" data-parsley-trigger="change focusout" data-parsley-type="digits">
                                <label>Charges<span class="mandatory-field">*</span></label>
                                <span></span>
                                <p class="help-block errorsection" data-name="noticecharges" data-index="<?php echo($x); ?>"></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="input-wrapper full-width">
                            <div class="styled-input">
                                <textarea name="post_sub_description[]" class="form-control" data-parsley-required="true" data-parsley-required-message="description is required" data-parsley-trigger="change focusout">{{ $p['description'] }}</textarea>
                                <label>Description<span class="mandatory-field">*</span></label>
                                <span></span>
                                <p class="help-block errorsection" data-name="post_sub_description" data-index="<?php echo($x); ?>"></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1 col-sm-6">
                        <a href="javascript:void(0);" class="add_noticecharge"><i class="fa fa-plus-square"></i></a>
                        <a href="javascript:void(0);" class="remove_noticecharge <?php echo(($x == 0) ? 'hide' : ''); ?>"><i class="fa fa-minus-square"></i></a>
                    </div>
                </div>
                <?php
                $x++;
            }
            ?>
        </div>
    </div>
</div>

