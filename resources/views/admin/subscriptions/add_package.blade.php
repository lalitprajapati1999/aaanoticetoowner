@extends('adminlte::page')
@section('content')

<div>
    <section class="address-book network manage-subscr">
        <div class="dashboard-wrapper">
            <div class="customer-status"></div>
            <div class="dashboard-heading">
                <h1><span>Manage Subscriptions Packages</span></h1>
            </div>
            <div class="dashboard-inner-body">
                <form method="POST" class="form" id="subscription-form" data-parsley-validate="" action="{{ url('admin/manage-subscriptions/save-package') }}">
                    {!! csrf_field() !!}
                    <div class="dash-subform business_phone_wrapper">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="sub-section-title">
                                    <h2 style="text-align: left">
                                        {{$package['name']}}
                                    </h2>
                                    <input type="hidden" name="package_type" value="{{$package['id']}}">
                                </div>
                            </div>
                        </div>
                        <?php
                        if ($package['id'] != '1') {
                            $title = "Monthly";
                            if ($package['id'] == "3") {
                                $title = "Yearly";
                            }
                            ?>
                            <div class="row">

                                <div class="col-md-4 col-sm-4">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                            <input type="text"  min="1" name="create_own_charge" class="form-control" data-parsley-required="true" data-parsley-required-message="Charges is required" data-parsley-trigger="change focusout" data-parsley-type="number" id="create_own_charge" value="{{ $package['charge'] }}" />
                                            <label><?php echo($title) ?> Subscriptions Charge</label>
                                            <span></span>
                                            <p class="help-block errorsection" data-name="create_own_charge" data-index=""></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5 col-sm-5">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                            <textarea name="description" class="form-control" data-parsley-required="true" data-parsley-required-message="description is required" data-parsley-trigger="change focusout">{{ $package['description'] }}</textarea>
                                            <label>Description</label>
                                            <span></span>
                                            <p class="help-block errorsection" data-name="description" data-index=""></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        <?php foreach ($notices as $notices_val) { ?>

                            <div class="row">
                                <div class="col-md-12" >
                                    <h3 class="" for="textinput">Notice : <?php echo $notices_val['name'] ?></h3>
                                    <input type="hidden" name="notice[{{$notices_val['id']}}][notice_id]" value="{{$notices_val['id']}}">
                                </div>
                            </div>
                            <div class="full-width">
                                <div class="col-md-3 col-sm-6">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                            <label>Description</label>
                                        </div>
                                    </div>
                                </div>
                            <?php if($notices_val['type'] == 2 && $package['id'] == '1') { ?>
                                    <div class="col-md-2 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <label class="">From<span class="mandatory-field">*</span></label>
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <label class="">To<span class="mandatory-field">*</span></label>
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                            <?php } ?>
                            <?php
                                if($notices_val['type'] == 1 && $package['id'] == '1'){
                                    if($notices_val['master_notice_id'] == config('constants.MASTER_NOTICE')['COL']['ID'] || $notices_val['master_notice_id'] == config('constants.MASTER_NOTICE')['BCOL']['ID']){?>
                                    <div class="col-md-2 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <label class="">From Unpaid Amount <span class="mandatory-field">*</span></label>
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <label class="">To Unpaid Amount<span class="mandatory-field">*</span></label>
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                           
                                   <?php }
                                }
                                ?>
                                <?php
                                if($notices_val['type'] == 1 && $package['id'] == '2'){
                                    if($notices_val['master_notice_id'] == config('constants.MASTER_NOTICE')['COL']['ID']|| $notices_val['master_notice_id'] == config('constants.MASTER_NOTICE')['BCOL']['ID']){?>
                                    <div class="col-md-2 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <label class="">From Unpaid Amount<span class="mandatory-field">*</span></label>
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <label class="">To Unpaid Amount<span class="mandatory-field">*</span></label>
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                           
                                   <?php }
                                }
                                ?>
                                <?php
                                if($notices_val['type'] == 1 && $package['id'] == '3'){
                                    if($notices_val['master_notice_id'] == config('constants.MASTER_NOTICE')['COL']['ID']||$notices_val['master_notice_id'] == config('constants.MASTER_NOTICE')['BCOL']['ID']){?>
                                    <div class="col-md-2 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <label class="">From Unpaid Amount<span class="mandatory-field">*</span></label>
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <label class="">To Unpaid Amount<span class="mandatory-field">*</span></label>
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                           
                                   <?php }
                                }
                                ?>

                                <div class="col-md-2 col-sm-6">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                            <label>Charges<span class="mandatory-field">*</span></label>
                                            <span></span>
                                        </div>
                                    </div>
                                </div>
                                
                          
                           
                                <div class="col-md-2 col-sm-6">
                                    <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                            <label>Cancellation Charge<span class="mandatory-field">*</span></label>
                                            <span></span>
                                        </div>
                                    </div>
                                </div>
                           
                                </div>
                                

                            <?php
                            if ($notices_val['type'] == 2 && $package['id'] == 1) {
                            if(!empty($price_range)){
                             foreach ($price_range as $key=>$price_range_val) {

                                    $pricing_info = getPricings($notices_val['id'], $package['id'], $price_range_val['id']); //dd($pricing_info);
                                    ?>
                                    <input type="hidden" name="notice[{{$notices_val['id']}}][additional_address][]" value="{{$pricing_info['additional_address']}}">
                                    <input type="hidden" name="notice[{{$notices_val['id']}}][id][]" value="{{$pricing_info['id']}}">
                                    
                                    <div class="full-width">
                                        <div class="col-md-3 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <textarea name="notice[{{$notices_val['id']}}][description][]" class="form-control" ><?php echo isset($pricing_info['description']) ? $pricing_info['description'] : "" ?></textarea>
                                                </div>
                                            </div>
                                    </div>
                
                                        <div class="col-md-2 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text" min="1" name="notice[{{$notices_val['id']}}][low_limit][]" value="{{$price_range_val['lower_limit']}}" class="form-control" @if($key==0) data-parsley-required="true" @endif data-parsley-required-message="Limit is required" data-parsley-trigger="chavnge focusout" data-parsley-type="digits" >
                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text"   name="notice[{{$notices_val['id']}}][high_limit][]" value="{{(in_array( $price_range_val['upper_limit'], config('constants.PACKAGE_RANGE'),TRUE)) ?  'Above': $price_range_val['upper_limit'] }}" class="form-control" @if($key==0) data-parsley-required="true" @endif data-parsley-required-message="Limit is required" data-parsley-trigger="change focusout"  />

                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text"  min="0" name="notice[{{$notices_val['id']}}][charges][]" value="<?php echo isset($pricing_info['charge']) ? $pricing_info['charge'] : "0" ?>" class="form-control" @if($key==0) data-parsley-required="true" @endif data-parsley-required-message="charges is required" data-parsley-trigger="change focusout" data-parsley-type="number">
                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text"  min="0" name="notice[{{$notices_val['id']}}][nto_cancellation_charge][]" value="<?php echo isset($pricing_info['cancelation_charge']) ? $pricing_info['cancelation_charge'] : "0" ?>"class="form-control" data-parsley-trigger="change focusout" data-parsley-type="number" id="nto_cancellation_charge"/>

                                                    <span></span>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>

                                   

                                    <?php
                                }
                                 
                                }else{?>
                                    <input type="hidden" name="notice[{{$notices_val['id']}}][additional_address][]" value="">
                                    <input type="hidden" name="notice[{{$notices_val['id']}}][id][]" value="0">
                                    
                                    <div class="full-width">
                                        <div class="col-md-3 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <textarea name="notice[{{$notices_val['id']}}][description][]" class="form-control" ></textarea>
                                                </div>
                                            </div>
                                    </div>
                
                                        <div class="col-md-2 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text" min="1" name="notice[{{$notices_val['id']}}][low_limit][]" value="" class="form-control" data-parsley-required="true" data-parsley-required-message="Limit is required" data-parsley-trigger="chavnge focusout" data-parsley-type="digits" >
                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text"   name="notice[{{$notices_val['id']}}][high_limit][]" value="" class="form-control" data-parsley-required="true" data-parsley-required-message="Limit is required" data-parsley-trigger="change focusout" />
                                                   
                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text"  min="0" name="notice[{{$notices_val['id']}}][charges][]" value="0" class="form-control" data-parsley-required="true" data-parsley-required-message="charges is required" data-parsley-trigger="change focusout" data-parsley-type="number">
                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text"  min="0" name="notice[{{$notices_val['id']}}][nto_cancellation_charge][]" value="0"class="form-control" data-parsley-trigger="change focusout" data-parsley-type="number" id="nto_cancellation_charge"/>

                                                    <span></span>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                
                               <?php }
                               echo '<a type="button" name="add_range" id="add_range" class="btn btn-primary custom-btn input-btn add_range" value="+ More" data-id='.$notices_val['id'].' data-additional-address=""><span>+ More</span></a>';
                                echo '<div class="row">
                                        <div class="col-md-12" >
                                            <h4 class="" for="textinput">ADDITIONAL ADDRESS CHARGES</h4>
                                        </div>
                                    </div>';
                                if(!empty($additional_address)){
                                    foreach ($additional_address as $key=>$price_range_val) {

                                    $pricing_info = getPricings($notices_val['id'], $package['id'], $price_range_val['id']);
                                    ?>
                                    <input type="hidden" name="notice[{{$notices_val['id']}}][id][]" value="{{$pricing_info['id']}}">
                                    <input type="hidden" name="notice[{{$notices_val['id']}}][additional_address][]" value="{{$pricing_info['additional_address']}}">
                                
                                    <div class="full-width">
                                        <div class="col-md-3 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <textarea name="notice[{{$notices_val['id']}}][description][]" class="form-control" ><?php echo isset($pricing_info['description']) ? $pricing_info['description'] : "" ?></textarea>
                                                </div>
                                            </div>
                                    </div>
                
                                        <div class="col-md-2 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text" min="1" name="notice[{{$notices_val['id']}}][low_limit][]" value="{{$price_range_val['lower_limit']}}" class="form-control" @if($key==0) data-parsley-required="true" @endif data-parsley-required-message="Limit is required" data-parsley-trigger="chavnge focusout" data-parsley-type="digits" >
                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                     <input type="text"   name="notice[{{$notices_val['id']}}][high_limit][]" value="{{(in_array( $price_range_val['upper_limit'], config('constants.PACKAGE_RANGE'),TRUE)) ?  'Above': $price_range_val['upper_limit'] }}" class="form-control" @if($key==0) data-parsley-required="true" @endif data-parsley-required-message="Limit is required" data-parsley-trigger="change focusout"   />

                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text"  min="0" name="notice[{{$notices_val['id']}}][charges][]" value="<?php echo isset($pricing_info['charge']) ? $pricing_info['charge'] : "0" ?>" class="form-control" @if($key==0) data-parsley-required="true" @endif data-parsley-required-message="charges is required" data-parsley-trigger="change focusout" data-parsley-type="number">
                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="hidden"  min="0" name="notice[{{$notices_val['id']}}][nto_cancellation_charge][]" value="<?php echo isset($pricing_info['cancelation_charge']) ? $pricing_info['cancelation_charge'] : "0" ?>"class="form-control" data-parsley-trigger="change focusout" data-parsley-type="number" id="nto_cancellation_charge"/>

                                                    <span></span>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                <?php }}else{ ?>
                                   
                                    <input type="hidden" name="notice[{{$notices_val['id']}}][id][]" value="0">
                                    <input type="hidden" name="notice[{{$notices_val['id']}}][additional_address][]" value="additional_address">
                                    
                                    <div class="full-width">
                                        <div class="col-md-3 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <textarea name="notice[{{$notices_val['id']}}][description][]" class="form-control" ></textarea>
                                                </div>
                                            </div>
                                    </div>
                                        
                                    

                                        <div class="col-md-2 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text" min="1" name="notice[{{$notices_val['id']}}][low_limit][]" value="" class="form-control" data-parsley-required="true" data-parsley-required-message="Limit is required" data-parsley-trigger="chavnge focusout" data-parsley-type="digits" >
                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text"   name="notice[{{$notices_val['id']}}][high_limit][]" value="" class="form-control" data-parsley-required="true" data-parsley-required-message="Limit is required" data-parsley-trigger="change focusout"  />
                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text"  min="0" name="notice[{{$notices_val['id']}}][charges][]" value="0" class="form-control" data-parsley-required="true" data-parsley-required-message="charges is required" data-parsley-trigger="change focusout" data-parsley-type="number">
                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-idsm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="hidden"  min="0" name="notice[{{$notices_val['id']}}][nto_cancellation_charge][]" value="0"class="form-control" data-parsley-trigger="change focusout" data-parsley-type="number" id="nto_cancellation_charge"/>

                                                    <span></span>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                               <?php }
                                echo '<a type="button" name="add_range" id="add_range" class="btn btn-primary custom-btn input-btn add_range" value="+ More" data-id='.$notices_val['id'].' data-additional-address="additional_address"><span>+ More</span></a>';

                             } else {
                             if($notices_val['master_notice_id'] == config('constants.MASTER_NOTICE')['COL']['ID']){
                                if(!empty($col_price_range)){
                                foreach ($col_price_range as $key=>$price_range_val) {
                                    $pricing_info = getPricings($notices_val['id'], $package['id'], $price_range_val['id']);
                                    
                                    ?>
                                    <input type="hidden" name="notice[{{$notices_val['id']}}][id][]" value="{{$pricing_info['id']}}">
                                    <input type="hidden" name="notice[{{$notices_val['id']}}][additional_address][]" value="{{$pricing_info['additional_address']}}">

                                 <div class="full-width">
                                   
                                        <div class="col-md-3 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <textarea name="notice[{{$notices_val['id']}}][description][]" class="form-control" ><?php echo isset($pricing_info['description']) ? $pricing_info['description'] : "" ?></textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text" min="1" name="notice[{{$notices_val['id']}}][low_limit][]" value="{{$price_range_val['lower_limit']}}" class="form-control" @if($key==0)  data-parsley-required="true" @endif data-parsley-required-message="Limit is required" data-parsley-trigger="chavnge focusout" data-parsley-type="digits" >
                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                     <input type="text"   name="notice[{{$notices_val['id']}}][high_limit][]" value="{{(in_array( $price_range_val['upper_limit'], config('constants.PACKAGE_RANGE'),TRUE)) ?  'Above': $price_range_val['upper_limit'] }}" class="form-control" @if($key==0) data-parsley-required="true" @endif data-parsley-required-message="Limit is required" data-parsley-trigger="change focusout"  />

                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text"  min="0" name="notice[{{$notices_val['id']}}][charges][]" value="<?php echo isset($pricing_info['charge']) ? $pricing_info['charge'] : "0" ?>" class="form-control" @if($key==0) data-parsley-required="true" @endif data-parsley-required-message="charges is required" data-parsley-trigger="change focusout" data-parsley-type="number">
                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text"  min="0" name="notice[{{$notices_val['id']}}][nto_cancellation_charge][]" value="<?php echo isset($pricing_info['cancelation_charge']) ? $pricing_info['cancelation_charge'] : "0" ?>"class="form-control" data-parsley-trigger="change focusout" data-parsley-type="number" id="nto_cancellation_charge"/>

                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            <?php 
                        }
                        }else{?>
                            <div class="full-width">
                                    <input type="hidden" name="notice[{{$notices_val['id']}}][additional_address][]" value="">
                                     <input type="hidden" name="notice[{{$notices_val['id']}}][id][]" value="0">
                                        <div class="col-md-3 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <textarea name="notice[{{$notices_val['id']}}][description][]" class="form-control" ></textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text" min="1" name="notice[{{$notices_val['id']}}][low_limit][]" value="" class="form-control" data-parsley-required="true" data-parsley-required-message="Limit is required" data-parsley-trigger="chavnge focusout" data-parsley-type="digits" >
                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text"   name="notice[{{$notices_val['id']}}][high_limit][]" value="" class="form-control" data-parsley-required="true" data-parsley-required-message="Limit is required" data-parsley-trigger="change focusout" />
                                                    
                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text"  min="0" name="notice[{{$notices_val['id']}}][charges][]" value="0" class="form-control" data-parsley-required="true" data-parsley-required-message="charges is required" data-parsley-trigger="change focusout" data-parsley-type="number">
                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text"  min="0" name="notice[{{$notices_val['id']}}][nto_cancellation_charge][]" value="0"class="form-control" data-parsley-trigger="change focusout" data-parsley-type="number" id="nto_cancellation_charge"/>

                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            
                       <?php }
                       echo '<a type="button" name="add_range" id="add_range" class="btn btn-primary custom-btn input-btn add_range" value="+ More" data-id='.$notices_val['id'].' data-additional-address=""><span>+ More</span></a>';
                        }else if($notices_val['master_notice_id'] == config('constants.MASTER_NOTICE')['BCOL']['ID']){
                            if(!empty($bcol_price_range)){
                                 //dd($bcol_price_range,$notices_val['id']);
                                 foreach ($bcol_price_range as $key=>$price_range_val) {
                                    $pricing_info = getPricings($notices_val['id'], $package['id'], $price_range_val['id']);
                                    //   dd($pricing_info);    
                                    ?>
                                  <input type="hidden" name="notice[{{$notices_val['id']}}][id][]" value="{{$pricing_info['id']}}">
                                    <input type="hidden" name="notice[{{$notices_val['id']}}][additional_address][]" value="{{$pricing_info['additional_address']}}">
                                    <div class="full-width">
                                    
                                        <div class="col-md-3 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <textarea name="notice[{{$notices_val['id']}}][description][]" class="form-control" ><?php echo isset($pricing_info['description']) ? $pricing_info['description'] : "" ?></textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text" min="1" name="notice[{{$notices_val['id']}}][low_limit][]" value="{{$price_range_val['lower_limit']}}"  class="form-control" @if($key==0) data-parsley-required="true" @endif data-parsley-required-message="Limit is required" data-parsley-trigger="chavnge focusout" data-parsley-type="digits" >
                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                     <input type="text"   name="notice[{{$notices_val['id']}}][high_limit][]" value="{{(in_array( $price_range_val['upper_limit'], config('constants.PACKAGE_RANGE'),TRUE)) ?  'Above': $price_range_val['upper_limit'] }}" class="form-control" @if($key==0) data-parsley-required="true" @endif data-parsley-required-message="Limit is required" data-parsley-trigger="change focusout"  />

                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text"  min="0" name="notice[{{$notices_val['id']}}][charges][]" value="<?php echo isset($pricing_info['charge']) ? $pricing_info['charge'] : "0" ?>" class="form-control" @if($key==0) data-parsley-required="true" @endif data-parsley-required-message="charges is required" data-parsley-trigger="change focusout" data-parsley-type="number">
                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text"  min="0" name="notice[{{$notices_val['id']}}][nto_cancellation_charge][]" value="<?php echo isset($pricing_info['cancelation_charge']) ? $pricing_info['cancelation_charge'] : "0" ?>"class="form-control" data-parsley-trigger="change focusout" data-parsley-type="number" id="nto_cancellation_charge"/>

                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            <?php }
                               
                            }else{?>
                                <input type="hidden" name="notice[{{$notices_val['id']}}][id][]" value="0">
                                <input type="hidden" name="notice[{{$notices_val['id']}}][additional_address][]" value="">
                                  <div class="full-width">
                                        <div class="col-md-3 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <textarea name="notice[{{$notices_val['id']}}][description][]" class="form-control" ></textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text" min="1" name="notice[{{$notices_val['id']}}][low_limit][]" value=""  class="form-control" data-parsley-required="true" data-parsley-required-message="Limit is required" data-parsley-trigger="chavnge focusout" data-parsley-type="digits" >
                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text"   name="notice[{{$notices_val['id']}}][high_limit][]" value="" class="form-control" data-parsley-required="true" data-parsley-required-message="Limit is required" data-parsley-trigger="change focusout"  data-parsley-type="digits" />
                                                   
                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text"  min="0" name="notice[{{$notices_val['id']}}][charges][]" value="0" class="form-control" data-parsley-required="true" data-parsley-required-message="charges is required" data-parsley-trigger="change focusout" data-parsley-type="number">
                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text"  min="0" name="notice[{{$notices_val['id']}}][nto_cancellation_charge][]" value="0"class="form-control" data-parsley-trigger="change focusout" data-parsley-type="number" id="nto_cancellation_charge"/>

                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                
                           <?php }
                           echo '<a type="button" name="add_range" id="add_range" class="btn btn-primary custom-btn input-btn add_range" value="+ More" data-id='.$notices_val['id'].' data-additional-address=""><span>+ More</span></a>';
                        }else{
                                if($notices_val['master_notice_id'] == config('constants.MASTER_NOTICE')['NTO']['ID']){
                                    $pricing_info = getOtherPricings($notices_val['id'], $package['id'],NULL,NULL);

                                    ?>

                                    <input type="hidden" name="notice[{{$notices_val['id']}}][additional_address][]" value="{{$pricing_info['additional_address']}}">
                                    
                                     <input type="hidden" name="notice[{{$notices_val['id']}}][id][]" value="{{$pricing_info['id']}}" hidden="">
                                     <textarea name="notice[{{$notices_val['id']}}][low_limit][]" hidden="">{{$pricing_info['lower_limit']}}</textarea>
                                     <textarea name="notice[{{$notices_val['id']}}][high_limit][]" hidden="">{{$pricing_info['upper_limit']}}</textarea>
                                    <div class="full-width">
                                    <div class="col-md-3 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <textarea name="notice[{{$notices_val['id']}}][description][]" class="form-control" ><?php echo isset($pricing_info['description']) ? $pricing_info['description'] : "" ?></textarea>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-2 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text"  min="0" name="notice[{{$notices_val['id']}}][charges][]" value="<?php echo isset($pricing_info['charge']) ? $pricing_info['charge'] : "0" ?>" class="form-control" data-parsley-required="true" data-parsley-required-message="charges is required" data-parsley-trigger="change focusout" data-parsley-type="number">

                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-2 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text"  min="0" name="notice[{{$notices_val['id']}}][nto_cancellation_charge][]" value="<?php echo isset($pricing_info['cancelation_charge']) ? $pricing_info['cancelation_charge'] : "0" ?>" class="form-control" data-parsley-trigger="change focusout" data-parsley-type="number" id="nto_cancellation_charge"  />

                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                <?php if(!empty($cyo_additional_address)){ ?>
                                     <div class="row">
                                        <div class="col-md-12" >
                                            <h4 class="" for="textinput">ADDITIONAL ADDRESS CHARGES</h4>
                                        </div>
                                    </div>
                                <div class="col-md-3 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <label class="">Description<span class="mandatory-field">*</span></label>
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <label class="">From<span class="mandatory-field">*</span></label>
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <label class="">To<span class="mandatory-field">*</span></label>
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <label class="">Charge<span class="mandatory-field">*</span></label>
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-6" hidden="">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <label class="">Cancellation Charge<span class="mandatory-field">*</span></label>
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                   

                                <?php   
                                foreach($cyo_additional_address as $key=>$data){
                                        $pricing_info = getPricings($notices_val['id'], $package['id'], $data['id']);?>
                                   
                                    <input type="hidden" name="notice[{{$notices_val['id']}}][additional_address][]" value="{{$pricing_info['additional_address']}}">
                                    
                                     <input type="hidden" name="notice[{{$notices_val['id']}}][id][]" value="{{$pricing_info['id']}}" hidden="">
                                    
                                <div class="full-width">
                                    <div class="col-md-3 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <textarea name="notice[{{$notices_val['id']}}][description][]" class="form-control" ><?php echo isset($pricing_info['description']) ? $pricing_info['description'] : "" ?></textarea>
                                            </div>
                                        </div>
                                    </div>

                                        <div class="col-md-2 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text" min="1" name="notice[{{$notices_val['id']}}][low_limit][]" value="{{$pricing_info['lower_limit']}}"  class="form-control" @if($key==0) data-parsley-required="true" @endif data-parsley-required-message="Limit is required" data-parsley-trigger="chavnge focusout" data-parsley-type="digits" >
                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                               
                                                     <input type="text"   name="notice[{{$notices_val['id']}}][high_limit][]" value="{{(in_array( $pricing_info['upper_limit'], config('constants.PACKAGE_RANGE'),TRUE)) ?  'Above': $pricing_info['upper_limit'] }}" class="form-control" @if($key==0) data-parsley-required="true" @endif data-parsley-required-message="Limit is required" data-parsley-trigger="change focusout"   />
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text"  min="0" name="notice[{{$notices_val['id']}}][charges][]" value="{{$pricing_info['charge']}}" class="form-control" @if($key==0) data-parsley-required="true" @endif data-parsley-required-message="charges is required" data-parsley-trigger="change focusout" data-parsley-type="number">
                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="hidden"  min="0" name="notice[{{$notices_val['id']}}][nto_cancellation_charge][]" value="{{$pricing_info['cancelation_charge']}}"class="form-control" data-parsley-trigger="change focusout" data-parsley-type="number" id="nto_cancellation_charge"/>

                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php }
                                    }else{?>

                                    <div class="row">
                                        <div class="col-md-12" >
                                            <h4 class="" for="textinput">ADDITIONAL ADDRESS CHARGES</h4>
                                        </div>
                                    </div>
                                      <input type="hidden" name="notice[{{$notices_val['id']}}][additional_address][]" value="additional_address">
                                    
                                     <input type="hidden" name="notice[{{$notices_val['id']}}][id][]" value="0">
                                    <div class="col-md-3 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <label class="">Description<span class="mandatory-field">*</span></label>
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <label class="">From<span class="mandatory-field">*</span></label>
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <label class="">To<span class="mandatory-field">*</span></label>
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <label class="">Charge<span class="mandatory-field">*</span></label>
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-6" hidden="">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <label class="">Cancellation Charge<span class="mandatory-field">*</span></label>
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                    
                                   
                                    <input type="hidden" name="notice[{{$notices_val['id']}}][id][]" value="" hidden="">
                                   
                                <div class="full-width">
                                    <div class="col-md-3 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <textarea name="notice[{{$notices_val['id']}}][description][]" class="form-control" ></textarea>
                                            </div>
                                        </div>
                                    </div>

                                        <div class="col-md-2 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text" min="1" name="notice[{{$notices_val['id']}}][low_limit][]" value=""  class="form-control" data-parsley-required="true" data-parsley-required-message="Limit is required" data-parsley-trigger="chavnge focusout" data-parsley-type="digits" >
                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text"   name="notice[{{$notices_val['id']}}][high_limit][]" value="" class="form-control" data-parsley-required="true" data-parsley-required-message="Limit is required" data-parsley-trigger="change focusout"  />
                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="text"  min="0" name="notice[{{$notices_val['id']}}][charges][]" value="0" class="form-control" data-parsley-required="true" data-parsley-required-message="charges is required" data-parsley-trigger="change focusout" data-parsley-type="number">
                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input type="hidden"  min="0" name="notice[{{$notices_val['id']}}][nto_cancellation_charge][]" value="0"class="form-control" data-parsley-trigger="change focusout" data-parsley-type="number" id="nto_cancellation_charge"/>

                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <?php }
                                     echo '<a type="button" name="add_range" id="add_range" class="btn btn-primary custom-btn input-btn add_range" value="+ More" data-id='.$notices_val['id'].' data-additional-address="additional_address"><span>+ More</span></a>';
                                    
                                  }else{
                                $pricing_info = getOtherPricings($notices_val['id'], $package['id'], NULL, NULL);?>
                                <input type="hidden" name="notice[{{$notices_val['id']}}][id][]" value="{{$pricing_info['id']}}">
                                <input type="hidden" name="notice[{{$notices_val['id']}}][additional_address][]" value="{{$pricing_info['additional_address']}}">
                               
                                <div class="full-width">
                                    <div class="col-md-3 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <textarea name="notice[{{$notices_val['id']}}][description][]" class="form-control" ><?php echo isset($pricing_info['description']) ? $pricing_info['description'] : "" ?></textarea>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-2 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text"  min="0" name="notice[{{$notices_val['id']}}][charges][]" value="<?php echo isset($pricing_info['charge']) ? $pricing_info['charge'] : "0" ?>" class="form-control" data-parsley-required="true" data-parsley-required-message="charges is required" data-parsley-trigger="change focusout" data-parsley-type="number">

                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input type="text"  min="0" name="notice[{{$notices_val['id']}}][nto_cancellation_charge][]" value="<?php echo isset($pricing_info['cancelation_charge']) ? $pricing_info['cancelation_charge'] : "0" ?>" class="form-control" data-parsley-trigger="change focusout" data-parsley-type="number" id="nto_cancellation_charge"  />

                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                

                            <?php }}} ?>
                        <?php } ?>


                        <!--   <div id="loadinghtml"></div>-->

                        <div class="form-bottom-btn" id="savesubscription" >
                             <a href="{{url('admin/dashboard')}}" class="btn btn-primary custom-btn customb-btn" style="margin: 0;"><span>Cancel</span></a>
                            <button  type="submit" name="savesubscription" class="btn btn-primary custom-btn customc-btn" value="Save Subscription"><span>Save Subscription</span></button>
                        </div>
                    </div>
                </form>
            </div>

        </div>

    </section>
</div>
@stop
@section('frontend_js')
<script type="text/javascript">


    $(document).ready(function () {
/*<?php if ($id != null) { ?>
            loadPackageDetails(<?php echo $id; ?>);
            $("#package_type").val(<?php echo $id; ?>);
<?php } ?>*/
       /* $(document).on("change", "#package_type", function () {
            var packageId = $("#package_type").val();
            if (packageId > 0) {
                loadPackageDetails(packageId);
                $('#subscription-form').parsley().destroy();
            }
        });
        $(document).on("change", "#state", function () {
            var packageId = $("#package_type").val();
            if (packageId > 0) {
                loadPackageDetails(packageId);
            }
        });*/
    $('.add_range').click(function ()
        {
            var notice_id = $(this).attr("data-id");
            var additional_address = $(this).attr("data-additional-address");
            if(!additional_address){
            var price_range_row = '<input type="hidden" value="0" name="notice['+notice_id+'][id][]">'+'<div class="full-width">'+ '<input type="hidden" name="notice['+notice_id+'][additional_address][]" value="'+additional_address+'">'+
                                        '<div class="col-md-3 col-sm-6">'+
                                            '<div class="input-wrapper full-width">'+
                                                '<div class="styled-input">'+
                                                    '<textarea name="notice['+notice_id+'][description][]" class="form-control" autocomplete="off" ></textarea>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-2 col-sm-6">'+
                                            '<div class="input-wrapper full-width">'+
                                                '<div class="styled-input">'+
                                                    '<input type="text" min="1" name="notice['+notice_id+'][low_limit][]" value=""  class="form-control not-empty" data-parsley-trigger="change focusout" data-parsley-type="digits" autocomplete="off" >'+
                                                    '<span></span>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-2 col-sm-6">'+
                                            '<div class="input-wrapper full-width">'+
                                                '<div class="styled-input">'+
                                                    '<input type="text"  name="notice['+notice_id+'][high_limit][]" value=""  class="form-control not-empty"  data-parsley-trigger="change focusout"  autocomplete="off">'+
                                                    '<span></span>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-2 col-sm-6">'+
                                            '<div class="input-wrapper full-width">'+
                                                '<div class="styled-input">'+
                                                    '<input type="text" min="0" name="notice['+notice_id+'][charges][]" value="0" class="form-control not-empty" data-parsley-trigger="change focusout" data-parsley-type="number" autocomplete="off">'+
                                                    '<span></span>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-2 col-sm-6">'+
                                            '<div class="input-wrapper full-width">'+
                                                '<div class="styled-input">'+
                                                    '<input type="text" min="0" name="notice['+notice_id+'][nto_cancellation_charge][]" value="0" class="form-control not-empty" data-parsley-trigger="change focusout" data-parsley-type="number" id="" autocomplete="off">'+
                                                    '<span></span>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+ 
                                    '</div>';
                                }else{
                                     var price_range_row = '<input type="hidden" value="0" name="notice['+notice_id+'][id][]">'+'<div class="full-width">'+ '<input type="hidden" name="notice['+notice_id+'][additional_address][]" value="'+additional_address+'">'+
                                        '<div class="col-md-3 col-sm-6">'+
                                            '<div class="input-wrapper full-width">'+
                                                '<div class="styled-input">'+
                                                    '<textarea name="notice['+notice_id+'][description][]" class="form-control" autocomplete="off" ></textarea>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-2 col-sm-6">'+
                                            '<div class="input-wrapper full-width">'+
                                                '<div class="styled-input">'+
                                                    '<input type="text" min="1" name="notice['+notice_id+'][low_limit][]" value=""  class="form-control not-empty" data-parsley-trigger="change focusout" data-parsley-type="digits" autocomplete="off" >'+
                                                    '<span></span>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-2 col-sm-6">'+
                                            '<div class="input-wrapper full-width">'+
                                                '<div class="styled-input">'+
                                                    '<input type="text" name="notice['+notice_id+'][high_limit][]" value=""  class="form-control not-empty"  data-parsley-trigger="change focusout" autocomplete="off">'+
                                                    '<span></span>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-2 col-sm-6">'+
                                            '<div class="input-wrapper full-width">'+
                                                '<div class="styled-input">'+
                                                    '<input type="text" min="0" name="notice['+notice_id+'][charges][]" value="0" class="form-control not-empty" data-parsley-trigger="change focusout" data-parsley-type="number" autocomplete="off">'+
                                                    '<span></span>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'+
                                        '<div class="col-md-2 col-sm-6" hidden>'+
                                            '<div class="input-wrapper full-width">'+
                                                '<div class="styled-input">'+
                                                    '<input type="text" min="0" name="notice['+notice_id+'][nto_cancellation_charge][]" value="0" class="form-control not-empty" data-parsley-trigger="change focusout" data-parsley-type="number" id="" autocomplete="off">'+
                                                    '<span></span>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>'
                                        '</div>'+
                                    '</div>';
                                }
            //$('#add_range').parent(price_range_row);
            $(price_range_row).insertBefore($(this));

            // $("#business_phone_count_"+business_phone_count).attr('data-parsley-trigger','change focusout')
            //                             .attr('data-parsley-pattern','(\(\d{3})\)\s\d{3}\-\d{4}$').parsley();
    });

        /*  $('#subscription-form').on('submit', function (event) {
         $('#subscription-form').parsley().destroy();
         //event.preventDefault();
         var submit_url = $(this).attr("action");
         $(".errorsection").each(function () {
         $(this).html("");
         });
         $.ajax({
         type: 'POST',
         url: submit_url,
         dataType: "json",
         async: false,
         cache: false,
         data: $("#subscription-form").serialize(),
         success: function (response) {
         if (response.status == true) {
         var div = $('<div/>', {
         class: 'alert alert-success',
         title: 'now this div has a title!'
         });
         div.html(response.message);
         $(".customer-status").html(div);
         $('html, body').animate({
         'scrollTop': $("body").position().top
         });
         setTimeout(function () {
         location.reload();
         }, 2000);
         } else {
         var div = $('<div/>', {
         class: 'alert alert-error',
         title: 'now this div has a title!'
         });
         div.html(response.message);
         $(".customer-status").html(div);
         $('html, body').animate({
         'scrollTop': $(".customer-status").position().top
         });
         
         }
         }, error: function (response) {
         if (response.status === 404 && response.responseJSON.status == false) {
         $.each(response.responseJSON.error, function (key, value) {
         $(".errorsection").each(function () {
         var name = $(this).data('name');
         var index = $(this).data('index');
         if (index >= 0) {
         index = "." + index;
         }
         if (name + index == key) {
         $(this).html(value).show();
         }
         });
         });
         }
         }
         });
         
         });
         */

    });

    /*function loadPackageDetails(packageId) {
     $("#loadinghtml").html("");
     var widget_url = '<?php echo URL('admin/manage-subscriptions/load-package'); ?>';
     $.ajax({
     type: 'GET',
     url: widget_url,
     dataType: "json",
     async: false,
     cache: false,
     data: {package_id: packageId},
     success: function (response) {
     if (response.status == true) {
     $("#loadinghtml").html(response.html);
     
     $('#loadinghtml').find(".selectpicker").each(function () {
     $(this).selectpicker('refresh')
     });
     $("#savesubscription").show();
     $("#subscription-form").parsley();
     } else {
     alert(response.message);
     $("#savesubscription").hide();
     }
     }
     });
     return true;
     }*/

</script>

<script type="text/javascript">
    /*  $(document).ready(function () {
     $(document).on("click", ".add_basepackage", function () {
     var clone = $(this).parents("#parent_basepackage").find("#makeclone_basepackage").clone(true);
     clone.attr("id", "");
     clone.find("input[type='text']").val("");
     clone.find("#nto_pricing_id").val(0);
     clone.find(".remove_basepackage").removeClass('hide');
     clone.find("textarea").val("");
     $(this).parents("#parent_basepackage").append(clone);
     
     //setting the idexes for the server side
     var index = 0;
     $(this).parents("#parent_basepackage").find(".base_package").each(function () {
     $(this).find(".errorsection").each(function () {
     $(this).attr('data-index', index);
     });
     index = +index + +1;
     });
     
     });
     
     $(document).on("click", ".remove_basepackage", function () {
     $(this).parents(".base_package").remove();
     //setting the idexes for the server side
     var index = 0;
     $(this).parents("#parent_basepackage").find(".base_package").each(function () {
     $(this).find(".errorsection").each(function () {
     $(this).attr('data-index', index);
     });
     index = +index + +1;
     });
     });
     
     $(document).on("click", ".add_noticecharge", function () {
     $(this).parents("#parent_noticecharge").find("#makeclone_noticecharge").find('.selectpicker').selectpicker('destroy', true);
     var clone = $(this).parents("#parent_noticecharge").find("#makeclone_noticecharge").clone(true);
     clone.attr("id", "");
     clone.find("input[type='text']").val("");
     clone.find("#per_notice_pricing_id").val(0);
     clone.find(".remove_noticecharge").removeClass('hide');
     clone.find("textarea").val("");
     $(this).parents("#parent_noticecharge").append(clone);
     //setting the idexes for the server side
     var index = 0;
     $(this).parents("#parent_noticecharge").find(".noticechargeparent").each(function () {
     $(this).find(".errorsection").each(function () {
     $(this).attr('data-index', index);
     });
     index = +index + +1;
     });
     $(this).parents("#parent_noticecharge").find('select').each(function () {
     $(this).addClass('selectpicker').selectpicker();
     });
     });
     
     $(document).on("click", ".remove_noticecharge", function () {
     $(this).parents(".noticechargeparent").remove();
     //setting the idexes for the server side
     var index = 0;
     $(this).parents("#parent_noticecharge").find(".noticechargeparent").each(function () {
     $(this).find(".errorsection").each(function () {
     $(this).attr('data-index', index);
     });
     index = +index + +1;
     });
     });
     
     $(document).on("change", "#monthly_charge", function () {
     var totalCharges = 0;
     var monthlyCharges = $(this).val();
     if (monthlyCharges > 0) {
     totalCharges = +monthlyCharges * 12;
     }
     $('#total_charge').val(totalCharges);
     });
     });
     
     
     $(document).ready(function () {
     function checkForInput(element) {
     if ($(element).val().length > 0) {
     $(element).addClass('not-empty');
     } else {
     $(element).removeClass('not-empty');
     }
     }
     
     $('input, textarea').each(function () {
     checkForInput(this);
     });
     
     // The lines below (inside) are executed on change & keyup
     $('input, textarea').on('change keyup', function () {
     checkForInput(this);
     });
     });
     */
</script>

@stop

