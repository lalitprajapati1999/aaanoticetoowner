@extends('adminlte::page')
@section('content')

<div>
    <section class="address-book">
        <div class="dashboard-wrapper">

            @if (Session::get('success'))
            <div class="alert alert-success">
               <?php echo Session::get('success'); ?>
            </div>
            @endif
            <div class="customer-status"></div>
            <div class="dashboard-heading">
                <h1><span>Check Subscriptions List</span></h1>
            </div>

            <div class="dashboard-inner-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="dahboard-table table-responsive">
                            <table class="table table-striped" id="subscriptions-data">
                                <thead class="thead-dark">
                                    <tr>
                                        <th scope="col">Company Name</th>
                                        <th scope="col">Package Name</th>
                                        <th scope="col">Check Number</th>
                                        <th scope="col">Charges</th>
                                        <th scope="col">Option</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                   
                                    if (isset($cheque_subscription) && (!empty($cheque_subscription->toArray()))) {
                                        foreach ($cheque_subscription as $cs) {
                                            ?>
                                            <tr>
                                                <td><?php echo($cs->customer->company_name); ?></td>
                                                <td><?php echo($cs->package->name); ?></td>
                                                <td><?php echo($cs->cheque_no); ?></td>
                                                <td><?php echo("$" . $cs->charges); ?></td>
                                                <td>
                                                    <input type="button" class="btn btn-primary approveChequeSubscription" data-url="{{url('admin/approve-cheque-subscriptions')}}" data-id="<?php echo($cs->id); ?>" value="Approve" style="color: #696969;" />
                                                </td>
                                            </tr>
                                        <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection

@section('frontend_js')
<script type="text/javascript">
    $(function () {
        $('#subscriptions-data').DataTable();

        $(document).on("click", ".approveChequeSubscription", function () {
            var url = $(this).attr('data-url');
            var id = $(this).attr('data-id');
            var token = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                type: 'POST',
                url: url,
                dataType: "json",
                async: false,
                cache: false,
                data: {'id': id, '_token' : token},
                success: function (response) {
                    if (response.status == true) {
                        var div = $('<div/>', { class: 'alert alert-success' });
                        div.html(response.message);
                        $(".customer-status").html(div);
                        $('html, body').animate({
                            'scrollTop': $("body").position().top
                        });
                        setTimeout(function () {
                            location.reload();
                        }, 2000);
                    } else {
                        var div = $('<div/>', { class: 'alert alert-error' });
                        div.html(response.message);
                        $(".customer-status").html(div);
                        $('html, body').animate({
                            'scrollTop': $(".customer-status").position().top
                        });
                        
                    }
                }
            });
            return true;
        });
    });
    // $(function () {
//        $.ajaxSetup({
//            headers: {
//                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//            }
//        });
//        var manageSubscriptionsTable = $('#subscriptions-data').DataTable({
//            // "pageLength":10
//            processing: true,
//            serverSide: true,
//            ajax: {
//                url: "{{url('admin/manage-subscription/datatable')}}"
//            },
//            columns: [
//                {data: 'package_name', name:'package_name'},
//                {data: 'type', name: 'type'},
//                {data: 'notices_included', name: 'action'},
//                {data: 'option', name: 'option'}
//            ]
//
//        });
//
//    });
//  
</script>
@endsection
