@extends('adminlte::page')

@section('css')
    <link rel="stylesheet" href="{{ asset('vendor/adminlte/plugins/jquery-sortable/jquery-sortable.css') }}">
@endsection

@section('content_header')
  	  <h1>
  	    <span class="text-capitalize">{{ $notice->name }}</span>
  	    <small>{{ trans('admin.manage_state_notice_form',['notice' => $notice->name, 'state' => $state_name]) }}.</small>
  	  </h1>
  	  <ol class="breadcrumb">
  	    <li><a href="{{ url(config('backpack.base.route_prefix'), 'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>
  	    <li><a href="{{ url('admin/notices') }}" class="text-capitalize">{{ trans('admin.notices') }}</a></li>
        <li><a href="{{ url('admin/states') }}" class="text-capitalize">{{ trans('admin.states') }}</a></li>
  	    <li class="active">{{ trans('admin.form') }}</li>
  	  </ol>

@endsection

@section('content')
 @if (Session::get('success'))
                <div class="alert alert-success no-margin">
                   <?php echo Session::get('success'); ?>
                </div>
                @endif
<!-- Default box -->
  <div class="row">

    <!-- THE ACTUAL CONTENT -->
    <div class="col-md-6 manage-form-section">

      <form method="post" action="{{ url('admin/notices/'.$notice->getKey().'/states/'.$state_id) }}">
      {!! csrf_field() !!}
      {!! method_field('PUT') !!}
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">{{ isset($noticeField)?trans('admin.edit_field'):trans('admin.add_field') }}</h3>
        </div>
        <div class="box-body">

          @if(isset($noticeField))

              <div class="form-group {{ $errors->has('sort_order') ? 'has-error' : '' }}">
              <label class="control-label" for="{{ trans('common.sort_order') }}">{!! $errors->has('sort_order') ? '<i class="fa fa-times-circle-o"></i> ' : '' !!}{{ trans('common.sort_order') }}</label>
              <input type="text" name="sort_order" class="form-control" value="{{ $noticeField->sort_order }}">
              @if ($errors->has('order'))
                <span class="help-block">
                  <strong>{{ $errors->first('sort_order') }}</strong>
                </span>
              @endif
            </div>

            @if(isset($notice_type) && $notice_type==2)
              <div class="form-group {{ $errors->has('section') ? 'has-error' : '' }}">
              <label class="control-label" for="{{ trans('common.section') }}">{!! $errors->has('section') ? '<i class="fa fa-times-circle-o"></i> ' : '' !!}{{ trans('common.section') }}</label>
              <select class="form-control" name="section">
                @foreach(config('constants.section_types') as $key => $value)
                <option value="{{ $key }}" {{ old('type') == $key?'selected':'' }} >{{ $value }}</option>
                @endforeach
              </select>
              @if ($errors->has('type'))
                <span class="help-block">
                  <strong>{{ $errors->first('section') }}</strong>
                </span>
              @endif
            </div>
            @endif
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
              <label class="control-label" for="{{ trans('common.name') }}">{!! $errors->has('name') ? '<i class="fa fa-times-circle-o"></i> ' : '' !!}{{ trans('common.name') }}</label>
              <input type="text" name="name" class="form-control" value="{{ $noticeField->name }}">
              @if ($errors->has('name'))
                <span class="help-block">
                  <strong>{{ $errors->first('name') }}</strong>
                </span>
              @endif
            </div>

            <div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
              <label class="control-label" for="{{ trans('common.type') }}">{!! $errors->has('type') ? '<i class="fa fa-times-circle-o"></i> ' : '' !!}{{ trans('common.type') }}</label>
              <select class="form-control" name="type">
                @foreach(config('constants.field_types') as $key => $value)
                <option value="{{ $key }}" {{ ($noticeField->type == $key)?'selected':'' }} >{{ $value }}</option>
                @endforeach
              </select>
            </div>
             <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
              <label class="control-label" for="{{ trans('common.status') }}">{!! $errors->has('status') ? '<i class="fa fa-times-circle-o"></i> ' : '' !!}{{ trans('common.status') }}</label>
                <div class="radio">
                  <label>
                  <input type="radio" name="status" value="1" class='status' {{($noticeField->status == 1) ? 'checked' : ''}}>
                                Active
                  </label>
                </div>
                <div class="radio">
                  <label>
                  <input type="radio" name="status" value="0" class='status' {{($noticeField->status == 0) ? 'checked' : ''}}>
                                Inactive
                  </label>
                </div>
              @if ($errors->has('status'))
              <span class="help-block">
                <strong>{{ $errors->first('status') }}</strong>
              </span>
              @endif
            </div>
            <div class="form-group {{ $errors->has('validation') ? 'has-error' : '' }}">
              <label class="control-label" for="{{ trans('admin.validation') }}">{!! $errors->has('validation') ? '<i class="fa fa-times-circle-o"></i> ' : '' !!}{{ trans('admin.validation') }}</label>
              <input type="text" name="validation" class="form-control" value="{{ $noticeField->validation }}">
              @if ($errors->has('validation'))
                <span class="help-block">
                  <strong>{{ $errors->first('validation') }}</strong>
                </span>
              @endif
            </div>

            <div class="form-group {{ $errors->has('is_required') ? 'has-error' : '' }}">
              <div class="checkbox">
                <label>
                  <input type="checkbox" name="is_required" value="1" {{ $noticeField->is_required == 1?'checked':'' }}>
                  {{ trans('admin.is_required') }}
                </label>
              </div>
              @if ($errors->has('is_required'))
              <span class="help-block">
                <strong>{{ $errors->first('is_required') }}</strong>
              </span>
              @endif
            </div>
            <input type="hidden" name="id" value="{{ $noticeField->id }}">
          @else
             <div class="form-group {{ $errors->has('sort_order') ? 'has-error' : '' }}">
              <label class="control-label" for="{{ trans('common.sort_order') }}">{!! $errors->has('sort_order') ? '<i class="fa fa-times-circle-o"></i> ' : '' !!}{{ trans('common.sort_order') }}</label>
              <input type="text" name="sort_order" class="form-control" value="{{ old('order') }}">
              @if ($errors->has('sort_order'))
                <span class="help-block">
                  <strong>{{ $errors->first('sort_order') }}</strong>
                </span>
              @endif
            </div>
            @if(isset($notice_type) && $notice_type==2)
             <div class="form-group {{ $errors->has('section') ? 'has-error' : '' }}">
              <label class="control-label" for="{{ trans('common.section') }}">{!! $errors->has('section') ? '<i class="fa fa-times-circle-o"></i> ' : '' !!}{{ trans('common.section') }}</label>
              <select class="form-control" name="section">
                @foreach(config('constants.section_types') as $key => $value)
                <option value="{{ $key }}" {{ old('type') == $key?'selected':'' }} >{{ $value }}</option>
                @endforeach
              </select>
              @if ($errors->has('type'))
                <span class="help-block">
                  <strong>{{ $errors->first('section') }}</strong>
                </span>
              @endif
            </div>
            @endif
            <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
              <label class="control-label" for="{{ trans('common.name') }}">{!! $errors->has('name') ? '<i class="fa fa-times-circle-o"></i> ' : '' !!}{{ trans('common.name') }}</label>
              <input type="text" name="name" class="form-control" value="{{ old('name') }}">
              @if ($errors->has('name'))
                <span class="help-block">
                  <strong>{{ $errors->first('name') }}</strong>
                </span>
              @endif
            </div>


            <div class="form-group {{ $errors->has('type') ? 'has-error' : '' }}">
              <label class="control-label" for="{{ trans('common.type') }}">{!! $errors->has('type') ? '<i class="fa fa-times-circle-o"></i> ' : '' !!}{{ trans('common.type') }}</label>
              <select class="form-control" name="type">
                @foreach(config('constants.field_types') as $key => $value)
                <option value="{{ $key }}" {{ old('type') == $key?'selected':'' }} >{{ $value }}</option>
                @endforeach
              </select>
              @if ($errors->has('type'))
                <span class="help-block">
                  <strong>{{ $errors->first('type') }}</strong>
                </span>
              @endif
            </div>
            <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
              <label class="control-label" for="{{ trans('common.status') }}">{!! $errors->has('status') ? '<i class="fa fa-times-circle-o"></i> ' : '' !!}{{ trans('common.status') }}</label>
                <div class="radio">
                  <label>
                  <input type="radio" name="status" value="1" checked class='status'>
                                Active
                  </label>
                </div>
                <div class="radio">
                  <label>
                  <input type="radio" name="status" value="0" class='status'>
                                Inactive
                  </label>
                </div>
              @if ($errors->has('status'))
              <span class="help-block">
                <strong>{{ $errors->first('status') }}</strong>
              </span>
              @endif
            </div>

            <div class="form-group {{ $errors->has('validation') ? 'has-error' : '' }}">
              <label class="control-label" for="{{ trans('admin.validation') }}">{!! $errors->has('validation') ? '<i class="fa fa-times-circle-o"></i> ' : '' !!}{{ trans('admin.validation') }}</label>
              <input type="text" name="validation" class="form-control" value="{{ old('validation') }}">
              @if ($errors->has('validation'))
                <span class="help-block">
                  <strong>{{ $errors->first('validation') }}</strong>
                </span>
              @endif
            </div>

           
            
            <div class="form-group {{ $errors->has('is_required') ? 'has-error' : '' }}">
              <div class="checkbox">
                <label>
                  <input type="checkbox" name="is_required" value="1" {{ old('is_required') == 1?'checked':'' }}>
                  {{ trans('admin.is_required') }}
                </label>
              </div>
              @if ($errors->has('is_required'))
              <span class="help-block">
                <strong>{{ $errors->first('is_required') }}</strong>
              </span>
              @endif
            </div>

          @endif

        </div>

        <div class="box-footer">

          <button type="submit" class="btn btn-success">
              <span class="fa fa-save" role="presentation" aria-hidden="true"></span> &nbsp;
              <span data-value="{{ trans('common.save') }}">{{ trans('common.save') }}</span>
          </button>

        </div><!-- /.box-footer-->
      </div><!-- /.box -->

    </div>
    <div class="col-md-6 manage-form-section">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">{{ trans('admin.manage_form') }}</h3>
        </div>
        <div class="box-body">

          @if($fields->count() > 0)
            <ul class='sortable list-group'>
              @foreach($fields as $field)
                <li data-id="{{ $field->id }}" data-order="{{ $field->sort_order }}" class="list-group-item">
                  <label>{{ trans('common.name') }}:</label> {{ $field->name }}
                  <br/>
                  <label>{{ trans('common.type') }}:</label> {{ $field->display_type }}<br/>
                  <label>{{ trans('common.section') }}:</label> {{ $field->section }}
                  <br/>
                  <label>{{ trans('common.sort_order') }}:</label> {{ $field->sort_order }}
                  <br/>
                  <label>{{ trans('common.status') }}:</label> {{($field->status==0) ? 'Inactive' : 'Active'}}
                  <br/>
                 <p class="manageform-edit"> 
                 
                  <a onclick="removenotice({{$field->getKey()}})" href="#" class="btn btn-danger btn-xs pull-right" data-toggle='modal' data-toggle="tooltip" data-target='#notice-template-confirmation-modal' data-placement="bottom" title="Remove" ><i class="fa fa-remove"></i> {{ trans('common.delete') }}</a>
                 <a href="{{ url('admin/notices/'.$notice->getKey().'/states/'.$state_id.'/fields/'.$field->getKey()) }}" class="btn btn-success btn-xs pull-right" ><i class="fa fa-edit"></i> {{ trans('common.edit') }}</a></p>
                </li>
              @endforeach
            </ul>
          @else
            <h4 class="text-center">{{ trans('common.not_records') }}</h3>
          @endif

        </div>
      </div><!-- /.box -->
       </form>
    </div>

  </div>
 <form action="{{url('admin/notice-field/destroy')}}" method="get" class="remove-record-model">
    {!! csrf_field() !!}
    <div id="notice-template-confirmation-modal" class="modal fade">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="icon-box">
                        <i class="fa fa-close"></i>
                    </div>        
                    <h4 class="modal-title">Are you sure?</h4>  
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <input type="text" name="notice_field_id" id="notice_field_id" hidden="">              
                    <p>Do you really want to delete these records? This process cannot be undone.</p>
                </div>
                <div class="modal-footer">
                    <div class="form-bottom-btn">
                        <button type="button" class="btn btn-primary custom-btn customb-btn remove-data-from-delete-form" data-dismiss="modal"><span>Cancel</span></button>
                        <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>Confirm</span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

@endsection

@section('js')
    <script src="{{ asset('vendor/adminlte/plugins/jquery-sortable/jquery-sortable.js') }}"></script>

    <script type="text/javascript">
   
      /*$(function  () {
var adjustment;

var group = $("ul.sortable").sortable({
  group: 'sortable',
  pullPlaceholder: false,
  // animation on drop
  onDrop: function  ($item, container, _super) {
    var $clonedItem = $('<li/>').css({height: 0});
    $item.before($clonedItem);
    $clonedItem.animate({'height': $item.height()});

    $item.animate($clonedItem.position(), function  () {
      $clonedItem.detach();
      _super($item, container);
    });
  
    var data = group.sortable("serialize").get();

    var jsonString = JSON.stringify(data, null, ' ');

    console.log(jsonString);

    $.ajax({
      type:'PUT',
      url: "{{ url('admin/notices/'.$notice->getKey().'/states/'.$state_id.'/order') }}",
      data : {"_token":"{{ csrf_token() }}", data:jsonString},
      cache: false,
      success:function(res){
          new PNotify({
              // title: 'Regular Notice',
              text: '{{ trans('backpack::crud.update_success') }}',
              type: "success",
              icon: false
          });
      },
      error:function(xhr, status, error) {
          new PNotify({
              // title: 'Regular Notice',
              text: '{{ trans('backpack::base.error_saving') }}',
              type: "error",
              icon: false
          });
      }
    });
  },

  // set $item relative to cursor position
  onDragStart: function ($item, container, _super) {
    var offset = $item.offset(),
        pointer = container.rootGroup.pointer;

    adjustment = {
      left: pointer.left - offset.left,
      top: pointer.top - offset.top
    };

    _super($item, container);
  },
  onDrag: function ($item, position) {
    $item.css({
      left: position.left - adjustment.left,
      top: position.top - adjustment.top
    });
  }
});
      });*/
    function removenotice(id){
      $('#notice_field_id').val(id);
    }
    </script>
@endsection