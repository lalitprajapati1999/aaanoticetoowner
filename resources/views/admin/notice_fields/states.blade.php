@extends('adminlte::page')

@section('content_header')
  	  <h1>
  	    <span class="text-capitalize">{{ $notice->name }}</span>
  	    <small>{{ trans('admin.select_states_notice',['notice' => $notice->name]) }}.</small>
  	  </h1>
  	  <ol class="breadcrumb">
  	    <li><a href="{{ url(config('backpack.base.route_prefix'), 'dashboard') }}">{{ trans('backpack::crud.admin') }}</a></li>
  	    <li><a href="{{ url('admin/notices') }}" class="text-capitalize">{{ trans('admin.notices') }}</a></li>
        <li><a href="{{ url('admin/notices/'.$notice->getKey().'/states') }}" class="text-capitalize">{{ trans('admin.states') }}</a></li>
  	    <li class="active">{{ trans('backpack::crud.list') }}</li>
  	  </ol>
@endsection

@section('content')
<!-- Default box -->
  <div class="row">

    <!-- THE ACTUAL CONTENT -->
    <div class="col-md-8 col-md-offset-2">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">{{ trans('admin.states') }}</h3>
        </div>
        <div class="box-body">
          @if($states->count() > 0)
            <table class="table table-hover">
              <tr>
                <th>{{ trans('common.sn') }}</th>
                <th>{{ trans('admin.state') }}</th>
              </tr>
              @foreach($states as $index => $state)
              <tr>
                <td>{{ $states->firstItem() + $index }}</td>
                <td><a href="{{ url('admin/notices/'.$notice->getKey().'/states/'.$state->getKey()) }}">{{ $state->name }}</a></td>
              </tr>
              @endforeach
            </table>
            {{ $states->links() }}
          @else
            <h4 class="text-center">{{ trans('common.not_records') }}</h3>
          @endif
        </div>
      </div><!-- /.box -->
    </div>

  </div>
@endsection