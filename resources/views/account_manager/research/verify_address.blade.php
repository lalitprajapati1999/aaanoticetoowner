<style type="text/css">
    .parsley-errors-list {
            position:static;
    }
    .work-order .parsley-errors-list {
        bottom: 74px;
    }
    .custom_div{
        margin-top: 2%;
    }

</style>
<div role="tabpanel" class="tab-pane" id="verify_address">
    <div class="project-wrapper">
        <div class="dash-subform">
            <div class="row">
                <div class="col-md-12"  style="padding:0;">
                     @if(!isset($is_rescind))
                    @php
                    $is_rescind = 0;
                    @endphp
                    @endif

                    <div class="sub-section-title">
                        <div class="col-md-3" style="padding:0;">
                            @if($is_rescind==0)
                            <h2>VERIFY MAIL DETAILS FOR {{strtoupper($notice['name'])}}
                            </h2>
                            @else
                            <h2>VERIFY MAIL DETAILS FOR {{strtoupper($notice['name'])}} amendment
                            </h2>
                            @endif
                        </div>
                         <?php $not_mailed_out_master_ids= [config('constants.MASTER_NOTICE')['SBC']['ID'],config('constants.MASTER_NOTICE')['WRL']['ID']];
                         ?>
                        <div class="col-md-9 pull-right text-right no-padding">
                            @if(isset($notice['is_claim_of_lien']) && $notice['is_claim_of_lien'] == 1)
                            <button type="button" name="recording" class="btn btn-primary custom-btn customc-btn" VALUE="Recording" onclick="change_secording_pending_sign_status('3')"><span>Recording</span></button>            
                            <button type="button" name="pending_signature" class="btn btn-primary custom-btn customc-btn" VALUE="Recording" onclick="change_secording_pending_sign_status('4')"><span>Pending Signature</span></button>            
                            @endif
                            <a href="javascript:void(0)" data-url="{{url('account-manager/printpdf/'.$id.'/work_order/'.'Notice')}}" class="btn btn-primary custom-btn customc-btn generate_pdf" title="Generate PDF" id="generate_pdf" name="generate" style="margin:0">Generate PDF</a>
                            @if(!in_array($notice['master_notice_id'], $not_mailed_out_master_ids))
                            <a href="#" id="createLabel1" class="createLabel1 btn btn-primary custom-btn customc-btn" title="Proceed to Mailing" name="mailing"  style="margin:0">Proceed to Mailing</a>
                            @endif
                            <button type="button" name="save_for_later" class=" save_labels_for_later btn btn-primary custom-btn customc-btn" VALUE="Save For Later" id="save_labels_for_later"  style="margin:0"><span>Save For Later</span></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="dahboard-table table-responsive research-table">
                        <form action="{{url('account-manager/research/createLabel')}}" method="POST" id="creating_labels_form" data-parsley-validate>
                            {{ csrf_field() }}  
                            <table class="table table-bordered">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>Role</th>
                                        <th>Recipient Name</th>
                                        <th>USPS Verified Address</th>
                                        <th>Type of Mailing</th>
                                        <th>Add ons</th>
                                        <th>Label</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php($cnt = 1)
                                    @foreach($recipients as $recipient)
                                    <!-- below line will check if stamps_label table record has been created, if yes then fields cannot be changed anymore -->
                                    @php ($label_generated = ($recipient->label_generated == 'yes')?'disabled':'')
                                    @php ($label_generated = '')
                                    @php ($usps_addr = array(
                                    'address'=>($recipient->usps_address)?$recipient->usps_address:'',
                                    'city' =>($recipient->usps_city)?$recipient->usps_city:'',
                                    'state'=>($recipient->usps_state)?$recipient->usps_state:'',
                                    'zipcode'=>$recipient->zipcode,
                                    'zipcode_add_on'=>$recipient->zipcode_add_on,
                                    'dpb'=>$recipient->dpb,
                                    'check_digit'=>$recipient->check_digit
                                    ))
                                    @php ($usps_address = array_slice($usps_addr,0,3) )

                                    @php ($addr = array(
                                    'address'=>($recipient->address)?$recipient->address:'',
                                    'city' =>($recipient->city->name)?$recipient->city->name:'',
                                    'state'=>($recipient->state->abbr)?$recipient->state->abbr:'',
                                    'zipcode'=>$recipient->zip,
                                    'zipcode_add_on'=>($recipient->zipcode_add_on)?$recipient->zipcode_add_on:'',
                                    'dpb'=>$recipient->dpb,
                                    'check_digit'=>$recipient->check_digit
                                    ))
                                    @php ($cyo_address = array_slice($addr,0,3) )
                                <input type="hidden" name="notice_id" value="{{$notice_id}}">
                                <input type="hidden" name="workorder_id" value="{{$id}}">
                                <input type="hidden" name="recipient_ids[]" value="{{ $recipient->id}}">
                                <tr>
                                    <td>{{$recipient->category->name}}</td>
                                    <td>{{$recipient->name}}</td>
                                    <td>
                                        @if($recipient->usps_address != '')
                                        <!-- <input type="checkbox" name="usps_addr_{{$cnt}}" id="usps_addr_{{$cnt}}" class="verify_address" value="{{ json_encode($usps_addr) }}"> -->
                                        <input type="hidden" name="usps_addr_{{$cnt}}" id="usps_addr_{{$cnt}}" class="verify_address" value="{{ json_encode($usps_addr) }}">
                                        <div id="<?php echo 'usps-address-' . $recipient->id ?>">
                                            {!! html_entity_decode(implode('<br/>',$usps_address)) !!}       
                                        </div>
                                        <!-- <span style="color:red">{{ ($recipient->comment)?$recipient->comment:''}}</span> -->
                                        @else
                                            {!! html_entity_decode(implode('<br/>',$cyo_address)) !!}  
                                        @endif
                                    </td>
                                    <td class="mailing-type">
                                    @if(($notice['master_notice_id'] == config('constants.MASTER_NOTICE')['NPN']['ID'] || $notice['master_notice_id'] == config('constants.MASTER_NOTICE')['BCOL']['ID']) && ($recipient->category->name == 'Owner 1' || $recipient->category->name == 'Owner 2') )
                                        <div class="form-group {{ $errors->has('type_of_mailing_'.$cnt) ? 'has-error' : ''}}">
                                             <input <?php echo $label_generated; ?> type="radio"  onChange="checkForInputs(this)" data-parsley-group="valtab" data-value="Manual" name="type_of_mailing_{{$cnt}}" value="manual" id='{{ "firm_mail_".$cnt}}' <?php echo (isset($recipient->recipient_shipping_labels) && $recipient->recipient_shipping_labels != '' && $recipient->recipient_shipping_labels->type_of_label == 'manual') ? 'checked' : '' ?>>Manual<br/>
                                            <input <?php echo $label_generated; ?> type="radio"  onChange="checkForInputs(this)" data-parsley-group="valtab" data-value="FM" name="type_of_mailing_{{$cnt}}" value="firm mail" id='{{ "firm_mail_".$cnt}}' <?php echo (isset($recipient->recipient_shipping_labels) && $recipient->recipient_shipping_labels != '' && $recipient->recipient_shipping_labels->type_of_label == 'firm mail') ? 'checked' : '' ?>>Firm Mail<br/>
                                            <!-- <input <?php echo $label_generated; ?> id='{{ "next_day_".$cnt}}' type="radio"  onChange="checkForInputs(this)" data-parsley-required="true"  data-parsley-group="valtab" data-value="ND" name="type_of_mailing_{{$cnt}}" value="next day delivery" <?php echo (isset($recipient->recipient_shipping_labels) && $recipient->recipient_shipping_labels != '' && $recipient->recipient_shipping_labels->type_of_label == 'next day delivery') ? 'checked' : '' ?>>Next Day Delivery<br/> -->
                                           <!--  <input <?php echo $label_generated; ?> id='{{ "next_day_".$cnt}}' type="radio"  onChange="checkForInputs(this)" data-parsley-required="true"  data-parsley-group="valtab" data-value="CERT USPS" name="type_of_mailing_{{$cnt}}" value="Cert. USPS" <?php echo (isset($recipient->recipient_shipping_labels) && $recipient->recipient_shipping_labels != '' && $recipient->recipient_shipping_labels->type_of_label == 'Cert. USPS') ? 'checked' : '' ?>>Cert. USPS<br/>
                                            <input <?php echo $label_generated; ?> id='{{ "next_day_".$cnt}}' type="radio"  onChange="checkForInputs(this)" data-parsley-required="true"  data-parsley-group="valtab" data-value="CERT RR" name="type_of_mailing_{{$cnt}}" value="Cert. RR" <?php echo (isset($recipient->recipient_shipping_labels) && $recipient->recipient_shipping_labels != '' && $recipient->recipient_shipping_labels->type_of_label == 'Cert. RR') ? 'checked' : '' ?>>Cert. RR<br/>
                                            <input <?php echo $label_generated; ?> id='{{ "next_day_".$cnt}}' type="radio"  onChange="checkForInputs(this)" data-parsley-required="true"  data-parsley-group="valtab" data-value="CERT ESRR" name="type_of_mailing_{{$cnt}}" value="Cert. ER" <?php echo (isset($recipient->recipient_shipping_labels) && $recipient->recipient_shipping_labels != '' && $recipient->recipient_shipping_labels->type_of_label == 'Cert. ER') ? 'checked' : '' ?>>Cert. ESRR<br/> -->
                                          <!--  <input hidden  <?php echo $label_generated; ?>  type="radio" id='{{"stamp_out_field_".$cnt}}' data-parsley-required="true"  data-parsley-group="valtab" name="type_of_mailing_{{$cnt}}" value="stamps" <?php echo (isset($recipient->recipient_shipping_labels) && $recipient->recipient_shipping_labels != '' && $recipient->recipient_shipping_labels->type_of_label == 'stamps') ? 'checked' : '' ?>> 

                                            @php($selected_add_ons = explode(',',$recipient->recipient_shipping_labels['add_ons']))
                                            @php($cnt1 = 0)
                                            @php($add_ons = isset($recipient->add_ons)?$recipient->add_ons:$add_ons)
                                            @foreach($add_ons as $key=>$val)
                                            <input <?php echo $label_generated; ?> id='{{ "add_on_id_".$cnt."_". $cnt1}}' onChange="checkForStamp({{$cnt}})"  data-prohibited='{{ $val["prohibited"] }}' value="{{ $val['id'] }}" type="radio" name="{{'add_ons_'.$cnt}}[]" <?php echo (isset($recipient->recipient_shipping_labels) && $recipient->recipient_shipping_labels != '' && !empty($selected_add_ons) && in_array($val['id'], $selected_add_ons)) ? 'checked' : '' ?>>{{$val['description']}}<br/>
                                            @endforeach 
 -->
                                            @if($recipient->usps_address != '')
                                            <input <?php echo $label_generated; ?> type="radio"  onChange="checkForInputs(this)"  data-parsley-group="valtab" name="type_of_mailing_{{$cnt}}" value="stamps" <?php echo (isset($recipient->recipient_shipping_labels) && $recipient->recipient_shipping_labels != '' && $recipient->recipient_shipping_labels->type_of_label == 'stamps') ? 'checked' : '' ?>>Using Stamps.com
                                            @endif
                                            
                                        </div>
                                        <span></span>
                                    @else
                                        <div class="form-group {{ $errors->has('type_of_mailing_'.$cnt) ? 'has-error' : ''}}">
                                            <input <?php echo $label_generated; ?> type="radio"  onChange="checkForInputs(this)" data-parsley-required="true" data-parsley-group="valtab" data-value="Manual" name="type_of_mailing_{{$cnt}}" value="manual" id='{{ "firm_mail_".$cnt}}' <?php echo (isset($recipient->recipient_shipping_labels) && $recipient->recipient_shipping_labels != '' && $recipient->recipient_shipping_labels->type_of_label == 'manual') ? 'checked' : '' ?>>Manual<br/>
                                            <input <?php echo $label_generated; ?> type="radio"  onChange="checkForInputs(this)" data-parsley-required="true" data-parsley-group="valtab" data-value="FM" name="type_of_mailing_{{$cnt}}" value="firm mail" id='{{ "firm_mail_".$cnt}}' <?php echo (isset($recipient->recipient_shipping_labels) && $recipient->recipient_shipping_labels != '' && $recipient->recipient_shipping_labels->type_of_label == 'firm mail') ? 'checked' : '' ?>>Firm Mail<br/>
                                            <!-- <input <?php echo $label_generated; ?> id='{{ "next_day_".$cnt}}' type="radio"  onChange="checkForInputs(this)" data-parsley-required="true"  data-parsley-group="valtab" data-value="ND" name="type_of_mailing_{{$cnt}}" value="next day delivery" <?php echo (isset($recipient->recipient_shipping_labels) && $recipient->recipient_shipping_labels != '' && $recipient->recipient_shipping_labels->type_of_label == 'next day delivery') ? 'checked' : '' ?>>Next Day Delivery<br/> -->
                                           <!--  <input <?php echo $label_generated; ?> id='{{ "next_day_".$cnt}}' type="radio"  onChange="checkForInputs(this)" data-parsley-required="true"  data-parsley-group="valtab" data-value="CERT USPS" name="type_of_mailing_{{$cnt}}" value="Cert. USPS" <?php echo (isset($recipient->recipient_shipping_labels) && $recipient->recipient_shipping_labels != '' && $recipient->recipient_shipping_labels->type_of_label == 'Cert. USPS') ? 'checked' : '' ?>>Cert. USPS<br/>
                                            <input <?php echo $label_generated; ?> id='{{ "next_day_".$cnt}}' type="radio"  onChange="checkForInputs(this)" data-parsley-required="true"  data-parsley-group="valtab" data-value="CERT RR" name="type_of_mailing_{{$cnt}}" value="Cert. RR" <?php echo (isset($recipient->recipient_shipping_labels) && $recipient->recipient_shipping_labels != '' && $recipient->recipient_shipping_labels->type_of_label == 'Cert. RR') ? 'checked' : '' ?>>Cert. RR<br/>
                                            <input <?php echo $label_generated; ?> id='{{ "next_day_".$cnt}}' type="radio"  onChange="checkForInputs(this)" data-parsley-required="true"  data-parsley-group="valtab" data-value="CERT ESRR" name="type_of_mailing_{{$cnt}}" value="Cert. ER" <?php echo (isset($recipient->recipient_shipping_labels) && $recipient->recipient_shipping_labels != '' && $recipient->recipient_shipping_labels->type_of_label == 'Cert. ER') ? 'checked' : '' ?>>Cert. ESRR<br/> -->
                                          <!--  <input hidden  <?php echo $label_generated; ?>  type="radio" id='{{"stamp_out_field_".$cnt}}' data-parsley-required="true"  data-parsley-group="valtab" name="type_of_mailing_{{$cnt}}" value="stamps" <?php echo (isset($recipient->recipient_shipping_labels) && $recipient->recipient_shipping_labels != '' && $recipient->recipient_shipping_labels->type_of_label == 'stamps') ? 'checked' : '' ?>> 

                                            @php($selected_add_ons = explode(',',$recipient->recipient_shipping_labels['add_ons']))
                                            @php($cnt1 = 0)
                                            @php($add_ons = isset($recipient->add_ons)?$recipient->add_ons:$add_ons)
                                            @foreach($add_ons as $key=>$val)
                                            <input <?php echo $label_generated; ?> id='{{ "add_on_id_".$cnt."_". $cnt1}}' onChange="checkForStamp({{$cnt}})"  data-prohibited='{{ $val["prohibited"] }}' value="{{ $val['id'] }}" type="radio" name="{{'add_ons_'.$cnt}}[]" <?php echo (isset($recipient->recipient_shipping_labels) && $recipient->recipient_shipping_labels != '' && !empty($selected_add_ons) && in_array($val['id'], $selected_add_ons)) ? 'checked' : '' ?>>{{$val['description']}}<br/>
                                            @endforeach 
 -->
                                            @if($recipient->usps_address != '')
                                            <input <?php echo $label_generated; ?> type="radio"  onChange="checkForInputs(this)" data-parsley-required="true" data-parsley-group="valtab" name="type_of_mailing_{{$cnt}}" value="stamps" <?php echo (isset($recipient->recipient_shipping_labels) && $recipient->recipient_shipping_labels != '' && $recipient->recipient_shipping_labels->type_of_label == 'stamps') ? 'checked' : '' ?>>Using Stamps.com
                                            @endif
                                            
                                        </div>
                                        <span></span>
                                    @endif
                                    </td>
                                    <td>
                                    <!-- <div style="<?php //if(isset($recipient->recipient_shipping_labels) && $recipient->recipient_shipping_labels['type_of_label']!='stamps'){ echo 'display:none';}else{echo 'display:block';}                                   ?>"> -->
                                        <div>
                                         @if(isset($recipient->usps_address) && $recipient->usps_address != '')
                                            <div class="form-group">
                                                <div id="service_type_div_{{$cnt}}"  style="<?php echo (isset($recipient->recipient_shipping_labels) && $recipient->recipient_shipping_labels != '' && $recipient->recipient_shipping_labels->type_of_label == 'stamps') ? 'display: none;' : 'display: none;' ?>">
                                           
                                                    <label for="service_type">Service type</label><br/>
                                                    <input class="form-control" type="text" disabled value="USPS First Class Mail">
                                                    <label for="service_type">Package type</label><br/>
                                                    <input class="form-control" type="text" disabled value="Letter">
                                                    <div id="filtered_add_ons_{{$cnt}}">
                                                        <label id='alrt{{$cnt}}' style="color:red"></label><br/>
                                                        <label>Add Ons <i class = "fa fa-info-circle" title = "Please note that clashing add ons will be automatically removed"></i></label><br/>

                                                        @php($selected_add_ons = explode(',',$recipient->recipient_shipping_labels['add_ons']))
                                                        @php($cnt1 = 0)
                                                        @php($add_ons = isset($recipient->add_ons)?$recipient->add_ons:[])
                                                        @foreach($add_ons as $key=>$val)
                                                        
                                                        <input <?php echo $label_generated; ?> id='{{ "add_on_id_".$cnt."_". $cnt1}}' onChange='getProhibited(this)'   data-prohibited='{{ $val["prohibited"] }}' value="{{ $val['id'] }}" type="checkbox" name="{{'add_ons_'.$cnt}}[]" <?php echo (isset($recipient->recipient_shipping_labels) && $recipient->recipient_shipping_labels != '' && !empty($selected_add_ons) && in_array($val['id'], $selected_add_ons)) ? 'checked' : '' ?>>{{$val['description']}}<br/>
                                                        @endforeach                                                
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                         @endif
                                        <div class="form-group {{ ($errors->has('manual_inputs'.$cnt)) ? 'has-error' : ''}}">
                                            <!-- <input <?php echo $label_generated; ?> style="<?php
                                            if ($recipient->recipient_shipping_labels['type_of_label'] != 'stamps') {
                                                echo 'display:block';
                                            } else {
                                                echo 'display:none';
                                            }
                                            ?>" type="text" name="manual_inputs{{$cnt}}" value="{{$recipient->recipient_shipping_labels['tracking_number']}}" placeholder="Enter mailing label here">
                                            <div style="<?php
                                            if ((isset($recipient->recipient_shipping_labels) && $recipient->recipient_shipping_labels != '' && $recipient->recipient_shipping_labels->type_of_label == 'stamps') || $recipient->recipient_shipping_labels == '') {
                                                echo 'display:block';
                                            } else {
                                                echo 'display:none';
                                            }
                                            ?>">
                                                <input placeholder="Enter shipping date here" <?php echo $label_generated; ?> value="{{$recipient->recipient_shipping_labels['ShipDate']}}" type="text" name="shipping_date{{$cnt}}" class="shipping_date">
                                            </div> -->

                                            <input type="text" name="manual_inputs{{$cnt}}" value="{{$recipient->recipient_shipping_labels['tracking_number']}}" placeholder="Enter mailing label here" >
                                            <div>
                                            <div class="form-group ">
                                            <input type="text" name="manual_amount{{$cnt}}" value="<?php echo (isset($recipient->recipient_shipping_labels['Amount']) && $recipient->recipient_shipping_labels ['Amount']!= '') ? $recipient->recipient_shipping_labels ['Amount'] : '00.00' ?>" required=""placeholder="Enter amount here" data-parsley-required="true">
                                            </div>
                                             <span></span>
                                                <input placeholder="Enter shipping date here" <?php echo $label_generated; ?> value="<?php if(!isset($recipient->recipient_shipping_labels) && $recipient->recipient_shipping_labels['ShipDate']=="" ) echo date('Y-m-d', strtotime(' +1 day'));         else $recipient->recipient_shipping_labels['ShipDate'];   ?>" type="text" name="shipping_date{{$cnt}}" class="shipping_date">
                                         <span id="shipping_dt_alert{{$cnt}}" style="display:none">Enter future shipping date which is not more than 30 days</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        @if($recipient->recipient_shipping_labels['type_of_label'] == 'stamps' && $recipient->label_generated == 'yes')
                                        <a target="_blank" href="{{$recipient->recipient_shipping_labels->URL}}">Shipping Label Image</a>
                                        @else
                                        {{ ($recipient->recipient_shipping_labels['type_of_label'] == 'stamps')?'Label not generated yet':'' }}
                                        @endif
                                    </td>
                                </tr>
                                @php ($cnt++)
                                @endforeach
                                </tbody>
                            </table>
                            <input type="hidden" name="workorder_id" value="{{$id}}" />
                            <input type="hidden" name="notice_id" value="{{$notice_id}}" />
                            <input type="hidden" name="saveLabelData" id="saveLabelData" value="no" />


                            <input type="hidden" name="recording_pending_signature" id="recording_pending_signature" value="0">
                            <input type="hidden" name="generate_pdf_flag" id="generate_pdf_flag" value="0">

                            <input type="checkbox" class="" name="rush_hour" id="rush_hour" <?php echo (isset($rush_hour) && $rush_hour[0]['rush_hour_charges'] == 'yes') ? 'checked' : '' ?>>Include Expedite Fee
                            @if(isset($rush_hour) && $rush_hour[0]['rush_hour_charges'] == 'yes')
                            <input type="text" name="rush_hour_charges" id="rush_hour_charges" value="{{$rush_hour[0]['rush_hour_amount']}}"placeholder="Enter rush hour amount here" data-parsley-type="number" data-parsley-required="true" data-parsley-trigger=" change focusout">
                            @else
                            <input type="text" name="rush_hour_charges" id="rush_hour_charges" hidden="" value="00.00" placeholder="Enter rush hour amount here" data-parsley-type="number" data-parsley-required="true" data-parsley-trigger="change focusout">

                            @endif
                            <br/>

                                <!--<input id="createLabel1" name="create-label" type="button" value="Save Rates" class="btn btn-primary custom-btn customc-btn" title="Please fill mandatory fields of project info">-->
                            <div class="custom_div">
                            @if(isset($notice['is_claim_of_lien']) && $notice['is_claim_of_lien'] == 1)
                            <!--  <a href="#" data-url="{{url('account-manager/recording/pending_signature/updateStatus/3/'.$id)}}" class="btn btn-primary custom-btn customc-btn updateworkorderstatus" title="Recording" name="recording">Recording</a>
                                  <a  href="#"  data-url="{{url('account-manager/recording/pending_signature/updateStatus/4/'.$id)}}" class="btn btn-primary custom-btn customc-btn updateworkorderstatus" title="Pending Signature" name="pending_signature">Pending Signature</a>-->
                            <button type="button" name="recording" class="btn btn-primary custom-btn customc-btn" VALUE="Recording" onclick="change_secording_pending_sign_status('3')"><span>Recording</span></button>            
                            <button type="button" name="pending_signature" class="btn btn-primary custom-btn customc-btn" VALUE="Recording" onclick="change_secording_pending_sign_status('4')"><span>Pending Signature</span></button>            

                            @endif
                            <!--   <a target="_blank" href="{{url('account-manager/printpdf/'.$id.'/work_order/'.'Notice')}}" class="btn btn-primary custom-btn customc-btn" title="Please fill mandatory fields of project info" name="generate">Generate PDF</a>-->
                            <a href="javascript:void(0)" data-url="{{url('account-manager/printpdf/'.$id.'/work_order/'.'Notice')}}" class="btn btn-primary custom-btn customc-btn generate_pdf" title="Generate PDF" id="generate_pdf" name="generate">Generate PDF</a>
                            @if(!in_array($notice['master_notice_id'], $not_mailed_out_master_ids))
                            <a href="#" id="createLabel1" class="createLabel1 btn btn-primary custom-btn customc-btn" title="Proceed to Mailing" name="mailing">Proceed to Mailing</a>
                            @endif
                            <button type="button" name="save_for_later" class="save_labels_for_later btn btn-primary custom-btn customc-btn" VALUE="Save For Later" id="save_labels_for_later"><span>Save For Later</span></button>            
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="address_verification_modal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Verified Addresses</h4>
            </div>
            <div class="modal-body" style="height: 400px;overflow-y:auto;">
                <div id="alternate_addresses"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="choose_address">Use chosen address</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Address Verification Message</h4>
            </div>
            <div class="modal-body">
                <div id="address_verification_message"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<div id="successModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Work Order Status Message</h4>
            </div>
            <div class="modal-body">
                <div id="work_order_update_message"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>  

<!-- Modal -->
<div id="recipientVerifiedCountModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Recipient Verified Count Message</h4>
            </div>
            <div class="modal-body">
                <div>
                    <h5>One or more recipient addresses are still not verified.</h5>
                    <h5>Creating labels using stamps.com will not be available for these recipients.</h5>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="acceptance_btn">Accept</button>
                <button type="button" class="btn btn-warning" id="rejection_btn" data-dismiss="modal">Reject</button>
            </div>
        </div>

    </div>
</div>

