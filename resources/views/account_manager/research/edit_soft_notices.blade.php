<!-- <style type="text/css">
    .parsley-errors-list  {
      position: absolute !important;
  }
   
</style> -->
<style>
  .bootstrap-select :focus{
    outline: 0;
    border-bottom : 1px solid #032c61 !important;
 }
  
 
.select2class :focus{
    outline: 0;
    height: 47px !important;
    border-bottom : 1px solid #032c61 !important;
}

.select2-container--default .select2-selection--single, .select2-selection .select2-selection--single {
    border-radius: 0;
    padding: 6px 12px;
    height: 34px;
    border: none !important;
    border-bottom: 1px solid #aaa !important;
}

.select2-container--default .select2-selection--single {
    background: none !important;
    border: 1px solid #aaa;
    border-radius: 4px;
}
</style>
@extends('adminlte::page')
@section('content')
<div role="tabpanel" class="tab-pane active" id="workorder">
    <section class="work-order address-book-add-readonly">
        <div class="dashboard-wrapper">
            @if (Session::get('success'))
            <div class="alert alert-success">
                <?php echo Session::get('success'); ?>
            </div>
            @endif
            @if (Session::get('error'))
            <div class="alert alert-error">
                <?php echo Session::get('error'); ?>
            </div>
            @endif
            @if(!isset($is_rescind))
            @php
            $is_rescind = 0;
            @endphp
            @endif
            <div class="dashboard-heading">
                @if($is_rescind==0)
                <h1><span>{{strtoupper($notice['name'])}}</span></h1>
                @else
                <h1><span>{{strtoupper($notice['name'])}} amendment</span></h1>
                @endif
            </div>
            <div class="dashboard-inner-body">
                <div class="order-by">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="order-details full-width list-inline">
                                <li><p>Company Name:</p> <span>{{$customer_details->company_name}}</span></li>
                                <li><p>Contact Name:</p> <span>{{$customer_details->contact_person}}</span></li>
                                <li><p>Phone Number:</p> <span> {{$customer_details->office_number}}</span></li>
                            </ul>
                            <ul class="order-details full-width list-inline">
                                <li><p>Mailing Address:</p> <span>{{$customer_details->mailing_address}}</span></li>
                                @if($id) 
                                <li><p>Work Order #: </p> <span>{{$id}}</span></li>
                                @endif
                                @if(isset($parent_work_order)  && !empty($parent_work_order))
                                <li><p>Parent Work Order #: </p> <span>{{$parent_work_order}}</span></li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div> 


                <div class="notice-tabs">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs display-ib pull-left" role="tablist">
                        <li role="presentation" id="project_tab" class="{{ ($tab == 'project')?'active':''}}"><a href="#project" aria-controls="project" role="tab" data-toggle="tab">Project</a></li>
                        <li role="presentation" id="recipient_tab" class="{{ ($tab == 'recipients')?'active':''}}"><a href="#recepients" aria-controls="recepients" role="tab" data-toggle="tab" >Recipients</a></li>
                        <li role="presentation" id="address_tab" class="{{ ($tab == 'address_verify')?'active':''}}"><a href="#verify_address" aria-controls="verify_address" role="tab" data-toggle="tab" >Address Verification</a></li>
                    </ul>
                    <div class="correction-div">
                        <div class="display-ib pull-right">
                            <a href="#" class="btn btn-default corr-btn">Corrections</a>
                            <a href="#" class="btn btn-default notice-btn">Notes</a>
                            <div class="corr-block notice-block mCustomScrollbar" data-mcs-theme="light">
                                <div class="corr-block-wrapper" >
                                    <h4>Corrections</h4>
                                    <form id="edit_work_order_correction">
                                        <div class="correction-success alert alert-success no-margin" style="display: none"></div>
                                        <div class="correction-error alert alert-danger no-margin" style="display:none"></div>
                                        <div class="row">
                                            <input type="text" name="work_order" value="{{$id}}" id="work_order" hidden="">
                                            <div class="col-md-12 col-sm-12">
                                                <div class="input-wrapper full-width">
                                                    <div class="styled-input">
                                                        <input class="form-control"  type="text" value="{{old('correction')}}" name="correction" data-parlsey-required="true" data-parsley-required-message="Correction is required">
                                                        <label>Correction<span class="mandatory-field">*</span></label>
                                                        <span></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12">
                                                <div class="select-opt">
                                                    <label class="col-md-12 control-label" for="textinput"  name="Email*"></label> 
                                                    <select class="selectpicker" name="email" id="correction_email">
                                                        <option value="">Select</option>
                                                        @foreach($note_emails as $email)
                                                        <option value="{{$email}}" @if (old('email') == $email) selected="selected" @endif > {{$email}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div id="correctionloader" style="display: none">
                                                <img width="150px" src="{{asset('images/loader.gif')}}"/>
                                            </div>
                                            <div class="form-bottom-btn">
                                                <a href="#" class="btn btn-primary custom-btn customc-btn submitCorrection"><span>Submit</span></a>
                                            </div>
                                        </div>
                                    </form>
                                    <ul class="correction_div">
                                        @if(isset($corrections) && !empty($corrections))
                                        @foreach($corrections AS $each_correction)
                                        <li>
                                            {{$each_correction->correction}}<br>
                                            Added by:{{$each_correction->name}}<br>
                                            Created at:{{date('m-d-Y',strtotime($each_correction->created_at))}}
                                        </li>
                                        @endforeach
                                        @endif
                                    </ul>
                                    <a class="closebtn">&times;</a>
                                </div>
                            </div>
                            <div class="notes-block notice-block mCustomScrollbar" data-mcs-theme="light">
                                <div class="notes-block-wrapper">
                                    <form id="edit_work_order_note">
                                        <div class="note-success alert alert-success" style="display: none"></div>
                                        <div class="note-error alert alert-danger" style="display:none"></div>
                                        <h4>Notes</h4>
                                        <p>Order Number: {{$id}}</p>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <ul class="button-holder full-width list-inline">
                                                    <li class=""><a href="#note-public"><input type="radio" id="radio-2-4" name="visibility" class="regular-radio" value="0" checked /><label for="radio-2-4">Public</label></a></li>
                                                    <li class=""><a href="#note-private"><input type="radio" id="radio-2-5" name="visibility" class="regular-radio" value="1"/><label for="radio-2-5">Private</label></a></li>
                                                </ul>
                                            </div>
                                            <input type="text" name="work_order" value="{{$id}}" id="work_order" hidden="">
                                            <div class="col-md-12 col-sm-12">
                                                <div class="input-wrapper full-width">
                                                    <div class="styled-input">
                                                        <input class="form-control"  type="text" value="{{old('note')}}" name="note" data-parlsey-required="true" data-parsley-required-message="Note is required">
                                                        <label>Note<span class="mandatory-field">*</span></label>
                                                        <span></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12">
                                                <div class="select-opt">
                                                    <label class="col-md-12 control-label" for="textinput"  name="Email*"></label> 
                                                    <select class="selectpicker" name="email" id="note_email">
                                                        <option value="">Select</option>
                                                        @foreach($note_emails as $email)
                                                        <option value="{{$email}}" @if (old('email') == $email) selected="selected" @endif > {{$email}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div id="noteloader" style="display: none">
                                                <img width="150px" src="{{asset('images/loader.gif')}}"/>
                                            </div>
                                            <div class="form-bottom-btn">
                                                <a href="#" class="btn btn-primary custom-btn customc-btn submitNote"><span>Submit</span></a>
                                            </div>
                                        </div>
                                    </form>
                                    <ul class="notes_div">
                                        @if(isset($notes) && !empty($notes))
                                        @foreach($notes AS $each_note)
                                        <li>
                                            {{$each_note->note}}<br>

                                            Added by:{{$each_note->name}}<br>
                                            Created at:{{date('m-d-Y',strtotime($each_note->created_at))}}

                                        </li>
                                        @endforeach
                                        @endif
                                    </ul>
                                    <a class="closebtn1">&times;</a>
                                </div>
                            </div>
                        </div>
                        <!--                                <div id="loaderDiv">
                                                        <img id="loading-image" src="{{ asset('loader.gif')}}" style="display:none;"/>
                                                    </div>-->
                    </div>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane {{ ($tab=='project')?'active':''}}" id="project">
                            <form method="post" id="create_work_order" data-parsley-validate="" action="{{ url('account-manager/research/edit/'.$id.'/'.$notice_id.'/'.$customer_id.'/'.$is_rescind) }}">

                                <div class="project-wrapper">
                                    <div class="dash-subform">
                                        <div class="row">
                                            <div class="col-md-12">


                                                    <div class="col-md-5"><div class="sub-section-title">
                                                            <h2>Project Information</h2></div>
                                                    </div>
                                                    <div class="col-md-7 pull-right text-right" >
                                                        <a href="{{url('account-manager/research')}}" class="btn btn-primary custom-btn customb-btn" ><span class="mobile-hide">Back</span></a>
                                                        <a href="{{url('admin/dashboard')}}" class="btn btn-primary custom-btn customb-btn" ><span class="mobile-hide">Cancel</span></a>
                                                        <input type="submit" name="continue" class="btn btn-primary custom-btn customb-btn save_for_later" value="Save For Later" id="save_for_later" />
                                                        <input type="submit" name="continue" class="btn btn-primary custom-btn customc-btn"  value="Continue" />
                                                    </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6">
                                                <div class="form-group">
                                                    <div class="readonly full-width">
                                                        <label class="full-width">Work order no</label>
                                                        <p class="full-width">#{{$id}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            @if(isset($parent_work_order) && $parent_work_order != '')
                                            <div class="col-md-6 col-sm-6">
                                                <div class="form-group">
                                                    <div class="readonly full-width">
                                                        <label class="full-width">Parent work order</label>
                                                        <p class="full-width">{{$parent_work_order}}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            @endif
                                            {!! csrf_field() !!}
                                            <input type="text" name="notice_id" value="{{$notice['id']}}" hidden="">
                                            @if(isset($duplicate) && !empty($duplicate))
                                            <input type="text" name="duplicate" value="{{$duplicate}}" hidden="">
                                            @endif
                                            @if($notice_fields_section1)
                                            @foreach($notice_fields_section1 as $fields)


                                            @if($fields->type==6 && $fields->name == 'last_date_on_the_job')
                                            <div class="col-md-6 col-sm-6">
                                                <div class="date-box full-width">
                                                    <div class="input-wrapper full-width">
                                                        <div class="styled-input">
                                                            <?php $label = str_replace("_", " ", $fields->name); ?>
                                                            <input class="form-control datepicker"  type="text" name="{{$fields->notice_field_id.' '.$fields->name}}" id="{{$fields->name}}" value="{{$fields->value}}">
                                                            <label>{{ucfirst(trans($label))}}</label>
                                                            <span></span>
                                                        </div>
                                                    </div>
                                                    <!--  <button type="button" class="btn btn-default" data-original-title="" title=""><span class="icon-calendar"></span></button> -->
                                                </div>
                                            </div>

                                            @elseif($fields->type==6 && $fields->name=='owner_received_nto_date')
                                            <div class="col-md-6 col-sm-6">
                                                <div class="date-box full-width">
                                                    <div class="input-wrapper full-width">
                                                        <div class="styled-input">
                                                            <?php $label = str_replace("_", " ", $fields->name); ?>
                                                            <input class="form-control datepicker" type="text" name="{{$fields->notice_field_id.' '.$fields->name}}" id="{{$fields->name}}" value="{{$fields->value}}" data-parsley-required="true" data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout" value="{{$fields->value}}">
                                                            <label>{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>
                                                            <span></span>
                                                        </div>
                                                    </div>
                                                    <!--   <button type="button" class="btn btn-default" data-original-title="" title=""><span class="icon-calendar"></span></button> -->
                                                </div>
                                            </div>
                                            @elseif($fields->type==6 && $fields->name=='general_contractor_received_nto_date')
                                            <div class="col-md-6 col-sm-6">
                                                <div class="date-box full-width">
                                                    <div class="input-wrapper full-width">
                                                        <div class="styled-input">
                                                            <?php $label = str_replace("_", " ", $fields->name); ?>
                                                            <input class="form-control datepicker" type="text" name="{{$fields->notice_field_id.' '.$fields->name}}" id="{{$fields->name}}" value="{{$fields->value}}" data-parsley-required="true" data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout" value="{{$fields->value}}">
                                                            <label>{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>
                                                            <span></span>
                                                        </div>
                                                    </div>
                                                    <!--   <button type="button" class="btn btn-default" data-original-title="" title=""><span class="icon-calendar"></span></button> -->
                                                </div>
                                            </div>
                                            @elseif($fields->type==6 && $fields->name=='subcontractor_received_nto_date')
                                            <div class="col-md-6 col-sm-6">
                                                <div class="date-box full-width">
                                                    <div class="input-wrapper full-width">
                                                        <div class="styled-input">
                                                            <?php $label = str_replace("_", " ", $fields->name); ?>
                                                            <input class="form-control datepicker" type="text" name="{{$fields->notice_field_id.' '.$fields->name}}" id="{{$fields->name}}" value="{{$fields->value}}" data-parsley-required="true" data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout" value="{{$fields->value}}">
                                                            <label>{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>
                                                            <span></span>
                                                        </div>
                                                    </div>
                                                    <!--  <button type="button" class="btn btn-default" data-original-title="" title=""><span class="icon-calendar"></span></button> -->
                                                </div>
                                            </div>
                                            @elseif($fields->type==6)
                                            <div class="col-md-6 col-sm-6">
                                                <div class="date-box full-width">
                                                    <div class="input-wrapper full-width">
                                                        <div class="styled-input">
                                                            <?php $label = str_replace("_", " ", $fields->name); ?>
                                                            <input class="form-control datepicker" required="" type="text" name="{{$fields->notice_field_id.' '.$fields->name}}" id="{{$fields->name}}" data-parsley-required="true" data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout" value="{{$fields->value}}">
                                                            <label>{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>
                                                            <span></span>
                                                        </div>
                                                    </div>
                                                    <!--  <button type="button" class="btn btn-default" data-original-title="" title=""><span class="icon-calendar"></span></button> -->
                                                </div>
                                            </div>
                                            @elseif($fields->type==1 && $fields->name == 'contracted_by' )
                                            <div class="col-md-6 col-sm-6">
                                                <div class="select-opt select2-container select2class">
                                                    <input type="hidden" value="1" name="contracted_by_exist">
                                                    <?php $label = str_replace("_", " ", $fields->name); ?>
                                                    <label class="col-md-12 control-label" for="textinput">{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>
                                                    <select class="js-example-tags form-control contracted_bys"  name="{{$fields->notice_field_id.'_'.$fields->name}}"  id="contracted_by_select" data-parsley-required="true" data-parsley-required-message="Contrcted by is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true">
                                                        <option value="">Select</option>
                                                        @if(isset($names) && !empty($names))
                                                        @foreach($names as $each_name)
                                                        <option value="{{$each_name->company_name}}" {{$fields->value==$each_name->company_name ? 'selected="selected"' : ''}}>{{$each_name->company_name}}</option>
                                                        @endforeach
                                                        @endif
                                                    </select>
                                                    <span></span>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 clear-b" >
                                                <div class="date-box full-width">
                                                    <div class="input-wrapper full-width" id="hidden-panel" style="display:none">
                                                        <div class="styled-input">
                                                            <label class="lableall" for="textinput">Contract Address</label>
                                                            <textarea id="addname" class="form-control" name="addname" data-parsley-trigger="change focusout"  rows="1"></textarea>
                                                        </div>
                                                     </div>
                                                 </div>
                                            </div>
                                            <div class="col-md-6 clear-b">
                                                <div class="add-new-btn inner-dash-btn full-width">
                                                    <button type="button" class="btn btn-primary custom-btn add_contact_details contracted_by_recipient" name="add_contact_submit" value="contacts" id="add_contact_contracted_by" data-toggle="modal" data-target="#contactModal"><span>+ Add/Update Address Book</span></a>
                                                </div>
                                            </div>
                                            @elseif($fields->type==1 && $fields->name == 'owner_received_type_mailing' )
                                            <?php $label = str_replace("_", " ", $fields->name); ?>
                                            <div class="col-md-6 col-sm-6">
                                                <div class="date-box  full-width">
                                                    <div class="input-wrapper full-width">
                                                        <div class="styled-input">
                                                            <input class="form-control"  type="text" name="{{$fields->notice_field_id.' '.$fields->name}}" value="{{$fields->value}}">
                                                            <label class="full-width">{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @elseif($fields->type==1 && $fields->name == 'contractor_received_type_mailing' )
                                            <?php $label = str_replace("_", " ", $fields->name); ?>
                                            <div class="col-md-6 col-sm-6">
                                                <div class="date-box  full-width">
                                                    <div class="input-wrapper full-width">
                                                        <div class="styled-input">
                                                            <input class="form-control"  type="text" name="{{$fields->notice_field_id.' '.$fields->name}}" id="{{$fields->name}}" value="{{$fields->value}}" >
                                                            <label class="full-width">{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @elseif($fields->type==1 && $fields->name == 'subcontractor_received_type_mailing' )
                                            <?php $label = str_replace("_", " ", $fields->name); ?>
                                            <div class="col-md-6 col-sm-6">
                                                <div class="date-box  full-width">
                                                    <div class="input-wrapper full-width">
                                                        <div class="styled-input">
                                                            <input class="form-control"  type="text" name="{{$fields->notice_field_id.' '.$fields->name}}" id="{{$fields->name}}" value="{{$fields->value}}" >
                                                            <label class="full-width">{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @elseif($fields->type==1 && $fields->name == 'date_request' )
                                            <?php $label = str_replace("_", " ", $fields->name); ?>
                                            <div class="col-md-6 col-sm-6 clear-b">
                                                <div class="date-box  full-width">
                                                    <div class="input-wrapper full-width">
                                                        <div class="styled-input">
                                                            <input class="form-control"  type="text" name="{{$fields->notice_field_id.' '.$fields->name}}" value="{{date('m-d-Y')}}" data-parsley-required="false" data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout" readonly="" >
                                                            <label class="full-width">{{ucfirst(trans($label))}}</label>
                                                            <span></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @elseif($fields->type==1 && $fields->name == 'parent_work_order' )
                                            <div class="col-md-6 col-sm-6">
                                                <div class="select-opt">
                                                    <?php $label = str_replace("_", " ", $fields->name); ?>
                                                    <label class="col-md-12 control-label" for="textinput">{{ucfirst(trans($label))}}</label>
                                                    <input type="hidden" id="parent_work_order_id" value="{{$fields->value}}"/>
                                                    <input type="hidden" name="parent_work_order_previous" id="parent_work_order_previous" value="{{$fields->value}}"/>
                                                    <!-- <select class="selectpicker" name="{{$fields->notice_field_id.' '.$fields->name}}" data-show-subtext="true" data-live-search="true" id="soft_doc_parent_work_order" data-parsley-required="false" data-parsley-required-message="Parent Work Order number is required" data-parsley-trigger="change focusout">
                                                    </select> -->
                                                    <select class="form-control"  name="{{$fields->notice_field_id.' '.$fields->name}}" id="soft_doc_parent_work_order"  data-show-subtext="true" data-live-search="true">
                                                        <?php if(!empty($fields->value)){ ?>
                                                            <option value="{{$fields->value}}" selected="selected">{{$fields->value}}</option>   
                                                        <?php }else{ ?>
                                                            <option value=" ">Select</option>
                                                        <?php } ?>                                 
                                                    </select>

                                                    <!-- {!! Form::select($fields->notice_field_id.' '.$fields->name, $parent_wo_list, $parent_work_order ?? 0, [
                                                        'class' => "selectpicker",
                                                        'data-show-subtext' => "true",
                                                        'data-live-search' => "true" ,
                                                        'id' => "soft_doc_parent_work_order",
                                                        'data-parsley-required' => "false",
                                                        'data-parsley-required-message' => "Parent Work Order number is required",
                                                        'data-parsley-trigger' => "change focusout"]) !!} -->
                                                </div>
                                            </div>
                                            @elseif($fields->type==1 && $fields->name == 'project_name' )
                                            <?php $label = str_replace("_", " ", $fields->name); ?>
                                            <div class="col-md-6 col-sm-6 clear-b">
                                                <div class="input-wrapper full-width">
                                                    <div class="styled-input">
                                                        <?php $label = str_replace("_", " ", $fields->name); ?>
                                                        <input class="form-control" type="text" name="{{$fields->notice_field_id.' '.$fields->name}}" id="{{$fields->name}}" value="{{$fields->value}}">
                                                        <label>{{ucfirst(trans($label))}}</label>
                                                        <span></span>
                                                    </div>
                                                </div>
                                            </div>
                                            @elseif($fields->type==1  && $fields->name == 'your_job_reference_no' )
                                            <?php $label = str_replace("_", " ", $fields->name); ?>
                                            <div class="col-md-6 col-sm-6">
                                                <div class="input-wrapper full-width">
                                                    <div class="styled-input">
                                                        <?php $label = str_replace("_", " ", $fields->name); ?>
                                                        <input class="form-control"  type="text" value="{{$fields->value}}" name="{{$fields->notice_field_id.' '.$fields->name}}" id="{{$fields->name}}" data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout">
                                                        <label>{{ucfirst(trans($label))}}</label>
                                                        <span></span>
                                                    </div>
                                                </div>
                                            </div>
                                            @elseif($fields->type==1 && $fields->name =='county')
                                            <div class="col-md-6 col-sm-6">
                                                <?php $label = str_replace("_", " ", $fields->name);
                                                ?>

                                                <div class="input-wrapper full-width">
                                                    <div class="styled-input">
                                                        <input class="form-control" value="{{getCounty($fields->value)}}"  type="text" name="recipient_county_id" id="county" data-parsley-required="true" data-parsley-required-message="Select County from the list" data-parsley-trigger="change focusout">
                                                        <input type="hidden" name="{{$fields->notice_field_id.' '.$fields->name}}"  value="{{$fields->value}}"id="recipient_county_id"/>
                                                        <label class="col-md-12 control-label" for="textinput">{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label> 
                                                        <span></span>                                                                
                                                    </div>
                                                    <div id="recipientcountyloader" style="display: none">
                                                        <img width="150px" src="{{asset('images/loader.gif')}}"/>
                                                    </div>
                                                </div>
                                            </div>
                                            @elseif($fields->type==1 && $fields->name =='notary_county')
                                            <div class="col-md-6 col-sm-6">
                                                <?php $label = str_replace("_", " ", $fields->name); ?>
                                                <div class="input-wrapper full-width">
                                                    <div class="styled-input">
                                                        <input class="form-control"  type="text" value="{{getCounty($fields->value)}}" name="recipient_notary_county_id" id="notary_county" data-parsley-required="true" data-parsley-required-message="Select County from the list" data-parsley-trigger="change focusout">
                                                        <input type="hidden" name="{{$fields->id.' '.$fields->name}}" value="{{$fields->value}}" id="recipient_notary_county_id"/>
                                                        <label class="col-md-12 control-label" for="textinput">{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label> 
                                                        <span></span>                                                                
                                                    </div>
                                                    <div id="recipientnotary_countyloader" style="display: none">
                                                        <img width="150px" src="{{asset('images/loader.gif')}}"/>
                                                    </div>
                                                </div>
                                            </div>
                                            @elseif($fields->type==1 && $fields->name =='project_county')
                                            <div class="col-md-6 col-sm-6">
                                                <?php $label = str_replace("_", " ", $fields->name); ?>
                                                <div class="input-wrapper full-width">
                                                    <div class="styled-input">
                                                        <input class="form-control"  type="text" value="{{getCounty($fields->value)}}"name="recipient_project_county_id" id="project_county" data-parsley-required="true" data-parsley-required-message="Select County from the list" data-parsley-trigger="change focusout">
                                                        <input type="hidden" name="{{$fields->id.' '.$fields->name}}" value="{{$fields->value}}" id="recipient_project_county_id"/>
                                                        <label class="col-md-12 control-label" for="textinput">{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label> 
                                                        <span></span>                                                                
                                                    </div>
                                                    <div id="recipientproject_countyloader" style="display: none">
                                                        <img width="150px" src="{{asset('images/loader.gif')}}"/>
                                                    </div>
                                                </div>
                                            </div>
                                            @elseif($fields->type==1 && $fields->name =='recorded_county')
                                            <?php $label = str_replace("_", " ", $fields->name); ?>
                                            <div class="col-md-6 col-sm-6">
                                                <div class="input-wrapper full-width">
                                                    <div class="styled-input">
                                                        <input class="form-control"  type="text" value="{{getCounty($fields->value)}}"name="recipient_recorded_county_id" id="recorded_county" data-parsley-required="true" data-parsley-required-message="Select County from the list" data-parsley-trigger="change focusout">
                                                        <input type="hidden" name="{{$fields->id.' '.$fields->name}}"  value="{{$fields->value}}"id="recipient_recorded_county_id"/>
                                                        <label class="col-md-12 control-label" for="textinput">{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label> 
                                                        <span></span>                                                                
                                                    </div>
                                                    <div id="recipientrecorded_countyloader" style="display: none">
                                                        <img width="150px" src="{{asset('images/loader.gif')}}"/>
                                                    </div>
                                                </div>
                                            </div>
                                            @elseif($fields->type==1 && $fields->name=="project_address_count")
                                            @if(!Auth::user()->hasRole('customer'))

                                            <div class="col-md-6 col-sm-6">
                                                <div class="input-wrapper full-width" @if(strtoupper($notice['name']) =="NOTICE TO OWNER/PRELIMINARY NOTICE")
                                                        style="position: absolute;"
                                                    @endif>
                                                    <div class="styled-input">
                                                        <input class="form-control"  type="text" name="{{$fields->notice_field_id.' '.$fields->name}}" value="{{$project_address_count}}" id="{{$fields->name}}" data-parsley-required="true" data-parsley-required-message="Project Address Count is required" data-parsley-trigger="change focusout">
                                                        <label>Project Address Count</label>
                                                        <span></span>
                                                    </div>
                                                </div>
                                            </div>
                                            @else
                                            <input class="form-control"  type="hidden" name="{{$fields->notice_field_id.' '.$fields->name}}" value="1" id="{{$fields->name}}" data-parsley-required="true" data-parsley-required-message="Project Address Count is required" data-parsley-trigger="change focusout">
                                            @endif
                                             @elseif($fields->type==1 && $fields->name =='total_amount_satisfied')
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="input-wrapper full-width">
                                                            <div class="styled-input">
                                                                <?php $label = str_replace("_", " ", $fields->name); ?>
                                                                <input class="form-control"  type="text" name="{{$fields->notice_field_id.' '.$fields->name}}" value="{{$fields->value}}" id="{{$fields->name}}" data-parsley-required="true"  data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout"  data-parsley-type="digits"  min="1" >
                                                                <label>($){{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>
                                                                <span></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                            @elseif($fields->type==1 )
                                            <div class="col-md-6 col-sm-6">
                                                <div class="input-wrapper full-width">
                                                    <div class="styled-input">
                                                        <?php $label = str_replace("_", " ", $fields->name); ?>
                                                        <input class="form-control" value="{{$fields->value}}" type="text" name="{{$fields->notice_field_id.' '.$fields->name}}" id="{{$fields->name}}" data-parsley-required="true" data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout" data-parsley-pattern="/^[a-zA-Z0-9 ]*$/">
                                                        <label>{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>
                                                        <span></span>
                                                    </div>
                                                </div>
                                            </div>
                                            @elseif($fields->type==2 && $fields->name=="project_address")
                                            <div class="col-md-6 col-sm-6 clear-b">
                                                <div class="input-wrapper full-width">
                                                    <label class="col-md-12 control-label">Project Address<span class="mandatory-field">*</span> (Please enter multiple addresses separated by **)</label>
                                                    <div class="styled-input">
                                                        <?php $label = str_replace("_", " ", $fields->name);
                                                              $project_address = preg_replace("/<br \/>/", "<p>", $fields->value); 
                                                        ?>
                                                        <textarea 
                                                        onkeyup="countAddress(this)" 
                                                        id="{{$fields->name}}" class="form-control projects_address" name="{{$fields->notice_field_id.' '.$fields->name}}" data-parsley-required="true" data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout" rows="5">{{$project_address}}</textarea>
                                                        <span></span>
                                                    </div>
                                                </div>
                                            </div>

                                            @elseif($fields->type==2)
                                            <div class="col-md-6 col-sm-6">
                                                <div class="input-wrapper full-width">
                                                    <div class="styled-input">
                                                        <?php $label = str_replace("_", " ", $fields->name);
                                                             $label = $label."/Correction";
                                                        ?>
                                                        <textarea id="{{$fields->name}}" class="form-control" name="{{$fields->notice_field_id.' '.$fields->name}}" data-parsley-required="true" data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout">{{strip_tags($fields->value)}}</textarea>
                                                        <label>{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>
                                                        <span></span>
                                                    </div>
                                                </div>
                                            </div>
                                            @elseif($fields->type==3 && $fields->name == 'project_type')
                                            <?php $project_type = $fields->value; ?>
                                            <div class="col-md-6 col-sm-6">
                                                <div class="select-opt">
                                                    <?php $label = str_replace("_", " ", $fields->name); ?>
                                                    <label class="col-md-12 control-label" for="textinput">{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>  
                                                    <select class="selectpicker"  name="{{$fields->notice_field_id.' '.$fields->name}}" data-parsley-required="true" data-parsley-required-message="Project type is required" data-parsley-trigger="change focusout">
                                                        <option value="" >Select Type</option>
                                                        @foreach($projectTypes as $type)
                                                        <option value="{{$type->id}}" {{$fields->value==$type->id ? 'selected="selected"' : ''}}>{{$type->type}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            @elseif($fields->type==3 && $fields->name == 'your_role')
                                            <?php $your_role = $fields->value; ?>
                                            <div class="col-md-6 col-sm-6 clear-b">
                                                <div class="select-opt select-custom-opt">
                                                    <?php $label = str_replace("_", " ", $fields->name); ?>
                                                    <label class="col-md-12 control-label" for="textinput">{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>
                                                    <select class="selectpicker" name="{{$fields->notice_field_id.' '.$fields->name}}" data-parsley-required="true" data-parsley-required-message="Your role is required" data-parsley-trigger="change focusout">
                                                        <option value="">Select Role</option>
                                                        @foreach($project_roles as $category)
                                                        <option value="{{$category->id}}" {{ $fields->value == $category->id ? 'selected="selected"' : '' }}>{{$category->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            @elseif($fields->type==3 && $fields->name =='city')
                                            <?php $city_id = $fields->value; ?>
                                            <div class="col-md-6 col-sm-6">
                                                <div class="input-wrapper full-width">
                                                    <div class="styled-input">
                                                        <?php $label = str_replace("_", " ", $fields->name); ?>
                                                        <label class="col-md-12 control-label" for="textinput">{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>  
                                                        <input class="form-control"  type="text" name="city" value="{{old('city')}}" id="recipient_city" data-parsley-required="true" data-parsley-required-message="Select City from the list" data-parsley-trigger="change focusout">
                                                        <div class="mailingcityloader" style="display: none">
                                                            <img width="150px" src="{{asset('images/loader.gif')}}"/>
                                                        </div>
                                                        <input type="hidden" name="recipient_city_id" id="recipient_city_id" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @elseif($fields->type==3 && $fields->name =='state')
                                        <?php $state_id = $fields->value; ?>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="select-opt ">
                                                <?php $label = str_replace("_", " ", $fields->name); ?>
                                                <label class="col-md-12 control-label" for="textinput" >{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>  
                                                <select class="selectpicker" id="soft_notice_state_value"  name="{{$fields->notice_field_id.' '.$fields->name}}" id="soft_notice_state_value" data-parsley-required="true" data-parsley-required-message="State is required" data-parsley-trigger="change focusout">
                                                    <option value="">Select State</option>
                                                    @foreach($states as $state)
                                                    <option value="{{$state->id}}" @if ($fields->value == $state->id) selected="selected" @endif >{{$state->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        @elseif($fields->type==3 && $fields->name =='county')
                                        <?php $country_id = $fields->value; ?>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="select-opt">
                                                <?php $label = str_replace("_", " ", $fields->name); ?>
                                                <label class="col-md-12 control-label" for="textinput"  >{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>  
                                                <select class="selectpicker" name="{{$fields->notice_field_id.' '.$fields->name}}" value="{{$fields->value}}" data-parsley-required="true" data-parsley-required-message="Country is required" data-parsley-trigger="change focusout" if="country_id">
                                                    @foreach($countries as $country)
                                                    <option value="{{$country->id}}" {{ $country_id == $country->id ? 'selected="selected"' : '' }}>{{$country->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        @elseif($fields->type==3 && $fields->name =='authorise_agent')
                                        <div class="col-md-6 col-sm-6">
                                            <div class="select-opt">
                                                <?php $label = str_replace("_", " ", $fields->name); ?>
                                                <label class="col-md-12 control-label" for="textinput">{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>  
                                                <select 
                                                    <select class="selectpicker" name="{{$fields->notice_field_id.' '.$fields->name}}" id="{{$fields->name}}" class="selectpicker"  data-parsley-required="true" data-parsley-required-message="Authorise signature is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true">
                                                        <option value="">Select</option>
                                                        @if(isset($officers_directors) && count($officers_directors)>0)
                                                        @foreach($officers_directors as $officer)
                                                        <option value="{{$officer->first_name.' '.$officer->last_name}}" @if ($fields->value == $officer->first_name.' '.$officer->last_name) selected="selected" @endif > {{ucfirst(trans($officer->first_name.' '.$officer->last_name))}}</option>
                                                        @endforeach
                                                        @endif
                                                    </select>
                                            </div>
                                        </div>
                                        @elseif($fields->type==3 && $fields->name =='title')
                                        <div class="col-md-6 col-sm-6">
                                            <div class="select-opt">
                                                <?php $label = str_replace("_", " ", $fields->name); ?>
                                                <label class="col-md-12 control-label" for="textinput">{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>  
                                                <select 
                                                    <select class="selectpicker" name="{{$fields->notice_field_id.' '.$fields->name}}" id="{{$fields->name}}" class="selectpicker"  data-parsley-required="true" data-parsley-required-message="Authorise signature is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true">
                                                        <option value="">Select</option>
                                                        @if(isset($officers_directors) && count($officers_directors)>0)
                                                        @foreach($officers_directors as $officer)
                                                        <option value="{{$officer->title}}" @if ($fields->value == $officer->title) selected="selected" @endif > {{ucfirst(trans($officer->title))}}</option>
                                                        @endforeach
                                                        @endif
                                                    </select>
                                            </div>
                                        </div>
                                        @elseif($fields->type==4)
                                        <?php $label = str_replace("_", " ", $fields->name); ?>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">      
                                                    <input class="checkbox" type="checkbox" name=""  value="1" id="attorneys_fees">
                                                    <label>
                                                        Attorneys Fees
                                                    </label>  
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6" id="showattorneysfee">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input class="form-control" type="textbox" name="{{$fields->notice_field_id.' '.$fields->name}}"  value="" id="{{$fields->name}}" placeholder="%">
                                                    <label>{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>
                                                </div>
                                            </div>
                                        </div>
                                        @elseif($fields->type==5)
                                        <?php $label = str_replace("_", " ", $fields->name); ?>
                                        <div class="col-md-4 col-sm-6">
                                            <div class="date-box full-width">
                                                <div class="input-wrapper full-width">
                                                    <div class="styled-input">
                                                        <input class="form-control datepicker" required=""  type="radio" name="{{$fields->notice_field_id.' '.$fields->name}}" value="{{$fields->value}}" data-parsley-required="true" data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout">
                                                        <label>{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>
                                                        <span></span>
                                                    </div>
                                                </div>
                                                <!-- <button type="submit" class="btn btn-default" data-original-title="" title=""><span class="icon-calendar"></span></button> -->
                                            </div>
                                        </div>
                                        @endif
                                        @endforeach
                                        @endif
                                    </div>
                                </div>
                                <div class="dash-subform">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="sub-section-title">
                                                <h2>Service/Labor Furnished</h2>
                                            </div>      
                                        </div>
                                        @if($notice_fields_section2)
                                        @foreach($notice_fields_section2 as $fields)
                                        @if($fields->type==2 && $fields->name=="folio_s"||$fields->name == 'legal_description')
                                        <div class="col-md-6 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <?php $label = str_replace("_", " ", $fields->name); ?>
                                                    <textarea class="form-control"  type="text"  name="{{$fields->notice_field_id.' '.$fields->name}}" id="{{$fields->name}}" data-parsley-required="false" data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout">{{strip_tags($fields->value)}}</textarea>
                                                    <label>{{ucfirst(trans($label))}} </label>
                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>
                                        @elseif($fields->type==2 && $fields->name == 'labor_service_furnished')
                                        <div class="col-md-6 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <?php $label = str_replace("_", " ", $fields->name); ?>
                                                    <textarea class="form-control"  type="text" name="{{$fields->notice_field_id.' '.$fields->name}}" id="{{$fields->name}}" data-parsley-required="true" data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout">{{strip_tags($fields->value)}}</textarea>
                                                    <label>{{ucfirst(trans($label))}}<span class="mandatory-field">*</span> </label>
                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>
                                        @elseif($fields->type==1)
                                        <div class="col-md-6 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <?php $label = str_replace("_", " ", $fields->name); ?>
                                                    <input class="form-control"  type="text" name="{{$fields->notice_field_id.' '.$fields->name}}" value="{{$fields->value}}"  data-parsley-required="true" data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout" data-parsley-pattern="/^[a-zA-Z0-9 ]*$/">
                                                    <label>{{ucfirst(trans($label))}}* </label>
                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>
                                        @elseif($fields->type==2 && $fields->name=="project_address")
                                        <div class="col-md-6 col-sm-6 clear-b">
                                            <div class="input-wrapper full-width">
                                                <label class="col-md-12 control-label">Project Address & Legal Description<span class="mandatory-field">*</span></label>
                                                <div class="styled-input">
                                                    <?php $label = str_replace("_", " ", $fields->name); ?>
                                                    <textarea id="{{$fields->name}}" class="form-control projects_address" name="{{$fields->notice_field_id.' '.$fields->name}}" data-parsley-required="true" data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout" rows="5">{{$fields->value}}</textarea>
                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>
                                        @elseif($fields->type==2)
                                        <div class="col-md-6 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <?php $label = str_replace("_", " ", $fields->name); ?>
                                                    <textarea id="{{$fields->name}}" class="form-control" name="{{$fields->notice_field_id.' '.$fields->name}}" data-parsley-required="true" data-parsley-required-message="{{ucfirst(trans($label))}} is required" data-parsley-trigger="change focusout">{{strip_tags($fields->value)}}</textarea>
                                                    <label>{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>
                                                    <span></span>
                                                </div>
                                            </div>
                                        </div>
                                        @elseif($fields->type==3 && $fields->name =='city')
                                        <input type="hidden" name="hidden_city_val" id="hidden_city_val"value="{{$fields->value}}">
                                        <div class="col-md-6 col-sm-6">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <?php $label = str_replace("_", " ", $fields->name); ?>
                                                    <label class="col-md-12 control-label" for="textinput">{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>  
                                                    <input class="form-control"  type="text" name="city" value="{{old('city')}}" id="city_value_2" data-parsley-required="true" data-parsley-required-message="Select City from the list" data-parsley-trigger="change focusout">
                                                </div>
                                                <div class="mailingcityloader" style="display: none">
                                                    <img width="150px" src="{{asset('images/loader.gif')}}"/>
                                                </div>
                                                <input type="hidden" name="city_value_id" id="mailing_city_value_id_2" />
                                            </div>
                                        </div>
                                    </div>
                                    @elseif($fields->type==3 && $fields->name =='state')
                                    <div class="col-md-6 col-sm-6">
                                        <div class="select-opt">
                                            <?php $label = str_replace("_", " ", $fields->name); ?>
                                            <label class="col-md-12 control-label" for="textinput"  >{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>  
                                            <select class="selectpicker" name="{{$fields->notice_field_id.' '.$fields->name}}" data-parsley-required="true" data-parsley-required-message="State is required" data-parsley-trigger="change focusout">
                                                <option value="">Select State</option>
                                                @foreach($states as $state)
                                                <option value="{{$state->id}}">{{$state->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    @elseif($fields->type==3 && $fields->name =='country')
                                    <div class="col-md-6 col-sm-6">
                                        <div class="select-opt">
                                            <?php $label = str_replace("_", " ", $fields->name); ?>
                                            <label class="col-md-12 control-label" for="textinput"  >{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>  
                                            <select class="selectpicker" name="{{$fields->notice_field_id.' '.$fields->name}}" data-parsley-required="true" data-parsley-required-message="Country is required" data-parsley-trigger="change focusout">
                                                <option value="0">Select Country</option>
                                                @foreach($countries as $country)
                                                <option value="{{$country->id}}">{{$country->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    @elseif($fields->type==4)
                                    <div class="col-md-6 col-sm-6">
                                        <div class="date-box full-width">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input class="form-control datepicker" required="" type="checkbox" name="{{$fields->notice_field_id.' '.$fields->name}}" value="{{$fields->value}}" data-parsley-required="true" data-parsley-required-message="Country is required" data-parsley-trigger="change focusout">
                                                    <label>{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>
                                                    <span></span>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-default" data-original-title="" title=""><span class="icon-calendar"></span></button>
                                        </div>
                                    </div>
                                    @elseif($fields->type==5)
                                    <div class="col-md-4 col-sm-6">
                                        <div class="date-box full-width">
                                            <div class="input-wrapper full-width">
                                                <div class="styled-input">
                                                    <input class="form-control datepicker" required=""  type="radio" name="{{$fields->notice_field_id.' '.$fields->name}}" value="{{$fields->value}}" data-parsley-required="true" data-parsley-required-message="Country is required" data-parsley-trigger="change focusout">
                                                    <label>{{ucfirst(trans($label))}}<span class="mandatory-field">*</span></label>
                                                    <span></span>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-default" data-original-title="" title=""><span class="icon-calendar"></span></button>
                                        </div>
                                    </div>
                                    @endif
                                    @endforeach
                                    @endif
                                    <?php
                                    $document_types = getDocumentTypes();
                                    ?>
                                    <div class="col-md-12 col-sm-12">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-12">
                                                <div class="select-opt mt-top">
                                                    <label class="col-md-12 control-label" for="textinput">Document Type</label>  
                                                    <select class="selectpicker" name="type" id="edit_document_type">
                                                        <option value="">-Select-</option>
                                                        @foreach ($document_types as $key => $value) 
                                                        <option value="{{$key}}">{{$value}}</option>
                                                        @endforeach
                                                    </select>
                                                    @if ($errors->has('type'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('type') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                          <!--   <div class="col-md-12 doc_visibility">
                                                <ul class="nav nav-tabs radio-tabs button-holder full-width public-tabs" role="tablist">
                                                    <li class="active col-md-3"><a href="#hard-doc"><input type="radio" id="radio-1-4" name="visibility" class="regular-radio document_visibility" value="0" checked /><label for="radio-1-4">Public</label></a></li>
                                                    <li class="col-md-3"><a href="#soft-doc"><input type="radio" id="radio-1-5" name="visibility" class="regular-radio document_visibility" value="1"/><label for="radio-1-5">Private</label></a></li>
                                                </ul>
                                            </div> -->
                                            <div class="col-md-12 col-sm-12 doc_field">
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-6">
                                                        <div class="input-wrapper full-width">
                                                            <div class="styled-input">
                                                                <input class="form-control" name="doc_input" type="text" id="edit_doc_input_id">
                                                                <label id="doc_field"></label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <input type="hidden" name="edit_document_name" id="edit_document_name">
                                            <input type="hidden" name="edit_doc_id" id="edit_doc_id">
                                            <input type="hidden" id="row_id">
                                            <div class="form-btn-div doc_file">
                                                <span class="btn btn-default btn-file upload-btn btn btn-primary form-btn" id="temp">
                                                    Choose File to Upload  <input type="file" name="file_name" id="edit_result_file">
                                                </span>
                                                (Allow Types:csv,xls,xlsx,pdf,doc,docx, jpg, jpeg)
                                                <span id="errormessage" style="display: none"></span>
                                            </div>
                                            <div class="form-btn-div doc_file col-md-12 col-sm-12 col-xs-12" >
                                                        <div style="float: left;width: 10"> 
                                                        <a href="javascript:;" onClick="openTab(this)" id="open_doc" name=""><span  id="preview" style="display: none;" ></span></a></div>
                                                        <div style="float: left ;width: 10;padding-left: 20px"> 
                                                        <a  href="javascript:;" ><span id="remove_icon" style="display: none;"  >Delete</span></a></div>
                                                       
                                                        <input type="hidden" name="remove_file" id="remove_file" value="0">
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12 doc_file">

                                                <div class="add-new-btn inner-dash-btn full-width ">
                                                    <button  name="submit_doc" type="button" value="Add" id="submit_document" class="btn btn-primary custom-btn">
                                                        <span class="update_doc">Add</span>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12">
                                                <div class="row">
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <div class="dahboard-table doc-table full-width table-responsive">
                                                            <table class="table table-striped" id="edit_document_table">
                                                                <thead class="thead-dark">
                                                                    <tr>
                                                                        <th scope="col">Document Type</th>
                                                                        <th scope="col">Document Description</th>
                                                                        <th scope="col">Name</th>
                                                                        <th scope="col">Date & Time</th>
                                                                        <th scope="col">Type of Attachment</th>
                                                                        <th class="col">Options</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    @if(isset($attachment) && !empty($attachment) && count($attachment)>0)
                                                                    @foreach($attachment AS $each_attachment)
                                                                    <?php $file_name = explode('.', $each_attachment->file_name) ?>
                                                                    <tr id="refreshdelete{{$each_attachment->id}}">
                                                                        <td>
                                                                            @foreach ($document_types as $key => $value) 
                                                                            @if ($each_attachment->type == $key) 
                                                                            {{$value}}
                                                                            @endif
                                                                            @endforeach

                                                                        </td>
                                                                        <td>{{$each_attachment->title}}</td>
                                                                        <td> <?php
                                                                            $extension = explode('.', $each_attachment->original_file_name);
                                                                            if ($each_attachment->original_file_name != "" && isset($extension[1])) {
                                                                                ?>{{$each_attachment->original_file_name}}<?php
                                                                            } else {
                                                                                echo "";
                                                                            }
                                                                            ?></td>
                                                                        <td>{{date('d M y h:i A',strtotime($each_attachment->created_at))}}</td>
                                                                        <td>
                                                                            <?php
                                                                            $extension = explode('.', $each_attachment->original_file_name);
                                                                            if ($each_attachment->original_file_name != "" && isset($extension[1])) {
                                                                                echo $ext=$extension[1];
                                                                            } else {
                                                                                echo $ext="";
                                                                            }
                                                                            ?>

                                                                        </td>
                                                                        <td>
                                                                            @if(isset($duplicate) && !empty($duplicate))
                                                                            <a class='btn btn-secondary item red-tooltip remove-duplicate-attachment' data-toggle='modal' 
                                                                               data-url='{{url('customer/work-order/removeattachment')}}'   data-file='{{$each_attachment->file_name}}' 
                                                                               data-target='#attachment-confirmation-modal' data-placement='bottom' title='Remove'>
                                                                                <span class='icon-cross-remove-sign'></span></a>
                                                                            @else
                                                                             <a class='btn btn-secondary item red-tooltip remove-attachment' data-toggle='modal' 
                                                                                       data-url='{{url('customer/edit/work-ordwer/removeattachment')}}'  data-id="{{$each_attachment->id}}" data-file='{{$each_attachment->file_name}}' 
                                                                                       data-target='#attachment-confirmation-modal' data-placement='bottom' title='Remove'>
                                                                                        <span class='icon-cross-remove-sign'></span></a>
                                                                            @endif
                                                                                <a class='btn btn-secondary item red-tooltip edit-attachment' 
                                                                                 data-id="{{$each_attachment->id}}" data-file='{{$each_attachment->file_name}}' data-type="{{$each_attachment->type}}" data-title="{{$each_attachment->title}}" data-file-original-name="{{$each_attachment->original_file_name}}" data-file-extension="{{$ext}}"
                                                                              >
                                                                                <span class='icon-pencil-edit-button'></span></a>

                                                                                @if(!empty($each_attachment->file_name))
                                                                                <a href="{{asset('attachment/work_order_document/'.$each_attachment->file_name)}}" target="_blank" class="btn btn-secondary item red-tooltip"><i class="fa fa-eye" style=""></i></a>
                                                                                @endif
                                                                                </td>
                                                                            </tr>
                                                                    @endforeach
                                                                    @else
                                                                <td class="text-center norecords" colspan="6">No record found</td>
                                                                @endif
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12 ">
                                        <input type="hidden" name="edit_soft_notices_attachment[]" id="edit_soft_notices_attachment"/>
                                        <input type="hidden" name="remove_doc[]" id="remove_doc"/>
                                        <div class="form-bottom-btn save_btn">
                                            <a href="{{url('account-manager/research')}}" class="btn btn-primary custom-btn customb-btn"><span>Back</span></a>
                                            <a href="{{url('admin/dashboard')}}" class="btn btn-primary custom-btn customb-btn"><span>Cancel</span></a>
                                            <input type="submit" name="continue" class="btn btn-primary custom-btn customb-btn save_for_later" value="Save For Later" id="save_for_later"/>
                                            <input type="submit" name="continue" class="btn btn-primary custom-btn customc-btn"  value="Continue"/>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane  {{ ($tab=='recipients')?'active':''}}" id="recepients">
                    <div class="project-wrapper">
                        <form  id="store_recipients" action="{{url('account-manager/recipients/'.$id.'/'.$notice_id.'/'.$customer_id)}}" method="post" data-parsley-validate="">
                            {!! csrf_field() !!}   
                            <div class="dash-subform mt-0">
                                <div class="row">

                                <div class="col-md-12 pull-right text-right" >
                                    <a type="button" id="editBack" name="editBack" class="btn btn-primary custom-btn customb-btn not-empty" value="Back" style="margin:0;"><span>Back</span></a>
                                    <a href="{{url('account-manager/research')}}" class="btn btn-primary custom-btn customb-btn" style="margin:0;"><span>Cancel</span></a>
                                    <a href="{{url('account-manager/updateStatus/'.$id)}}" class="btn btn-primary custom-btn customb-btn" style="margin:0;"><span>Save for Later</span></a>
                                    <input type="button" id="createLabel" name="submit_continue" class="btn btn-primary custom-btn customc-btn" value="Continue" style="margin:0;"/>
                                </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="sub-section-title">
                                                    <h2>Recipients</h2>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="dahboard-table table-responsive">
                                                    <table class="table table-striped">
                                                        <thead class="thead-dark">
                                                            <tr>
                                                                <th scope="col">Category</th>
                                                                <th scope="col">Recipients</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($count as $key => $count)
                                                            <tr>
                                                                <td class="checkbox-holder">
                                                                    <input type="checkbox" id="{{$key}}" class="regular-checkbox"><label for="{{$key}}">{{$count->name}}</label>
                                                                </td>
                                                                <td>{{$count->total}}</td>
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-md-4" style="padding: 0;">
                                                <div class="sub-section-title"  style="padding-right: 0;">
                                                    <h2>Selected Recipients</h2>
                                                </div>
                                            </div>
                                           
                                            <div class="col-md-12">
                                                <div class="dahboard-table table-responsive">
                                                    <table class="table table-striped">
                                                        <thead class="thead-dark">
                                                            <tr>
                                                                <th scope="col">Role</th>
                                                                <th scope="col">Company Name</th>
                                                                <!--                                                    <th scope="col">Contact Person</th>-->
                                                                <!--                                                    <th scope="col">Phone Number</th>-->
                                                                <th scope="col">Address</th>
                                                                <th scope="col">City</th>
                                                                <th scope="col">State</th>
                                                                <th scope="col">Zipcode</th>
                                                                <th scope="col">Address Verified</th>
                                                                <th scope="col">Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @if(isset($recipients) && count($recipients) > 0)
                                                            @foreach($recipients  as $recipient)
                                                            <tr>
                                                                <td class="cl-blue">{{$recipient->category->name}}</td>
                                                                <td>{{$recipient->name}}</td>
                                                                <!--                                                                            <td>{{$recipient->contact}}</td>-->
                                                                <!--                                                                            <td>{{$recipient->mobile}}</td>-->
                                                                <td>
                                                                    {{$recipient->address}}
                                                                </td>
                                                                <td>{{$recipient->city->name}}</td>
                                                                <td>{{$recipient->state->name}}</td>
                                                                <td>{{$recipient->zip}}</td>
                                                                <td>
                                                                    {{ ((isset($recipient->usps_address) && $recipient->usps_address != '')?'Yes':'No') }}
                                                                </td>
                                                                <td>
                                                                    <a href="#recipient" data-id="{{$recipient->id}}" class="edit_recipient"><span class="icon-pencil-edit-button"></span></a>
                                                                    <a data-id="{{$recipient->id}}" data-notice-id="{{$notice_id}}" data-workorder-id="{{$id}}" data-customer="{{$customer_id}}" href="#" class="remove-record" data-toggle='modal' data-toggle="tooltip" data-target='#contact-confirmation-modal' data-placement="bottom" title="Remove"><span class="icon-cross-remove-sign"></span></a>
                                                                </td>
                                                            </tr>
                                                            @endforeach
                                                            @else
                                                            <tr>
                                                                <td></td>
                                                                <td></td>
                                                                <td>{{'No records found'}}</td>
                                                            </tr>
                                                            @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="dash-subform" id="recipient">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="sub-section-title">
                                            <h2>Recipients</h2>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6">
                                                <div class="select-opt">
                                                    <label class="col-md-12 control-label" for="textinput">Type of Recipient<span class="mandatory-field">*</span></label>  
                                                    <input type="text" name="work_order_id" value="{{$id}}" hidden="">
                                                    <input type="hidden" name="recipient_id" id="recipient_id">
                                                    <select class="selectpicker" name="category_id" data-parsley-required="true" data-parsley-required-message="Type of recipient is required" id="category_id">
                                                        <option value="">Select</option>
                                                        @foreach($categories as $category)
                                                        <option value="{{$category->id}}">{{$category->name}}</option>
                                                        @endforeach
                                                    </select>
                                                  
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="select-opt select2-container select2class">
                                            <label class="col-md-12 control-label" for="textinput">Name<span class="mandatory-field">*</span></label>  
                                            <select class="js-example-tags form-control recipient_name"  name="name" id="recipient_name" data-parsley-required="true" data-parsley-required-message="Name is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true">
                                                <option value="">Select</option>
                                                @if(isset($names) && !empty($names))
                                                @foreach($names as $each_name)
                                                <option  data-id="{{$each_name->id}}" value="{{$each_name->company_name}}">{{$each_name->company_name}}</option>
                                                @endforeach
                                                @endif
                                            </select>
                                            <input type="hidden" name="recipt_id"  value="" id="recipt_id">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-bottom-btn pd-25">
                                            @if($tab == 'project')
                                            <button type="button" name="submit_add_contact2" class="btn btn-primary custom-btn customc-btn add_contact_details" value="contacts" id="add_contact" data-toggle="modal" data-target="#contactModal"><span>Add/Update Address Book</span></button>
                                            <button type="button" name="add_contact_submit" class="btn btn-primary custom-btn customc-btn update_contact" value="contacts" id="update_contact"><span>Update Address Book</span></button>
                                            @elseif($tab == 'recipients')
                                            <button type="button" name="submit_add_contact2" class="btn btn-primary custom-btn customc-btn add_contact_details" value="contacts" id="add_contact" data-toggle="modal" data-target="#contactModal"><span>Add/Update Address Book</span></button>
                                            <button type="button" name="add_contact_submit" class="btn btn-primary custom-btn customc-btn update_contact" value="contacts" id="update_contact" ><span>Update Address Book</span></button>

                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <label class="col-md-12 control-label" for="textarea">Attn/Additional Recipient Name<span class="mandatory-field"></span></label> 
                                            <div class="styled-input">
                                                <textarea class="form-control recipient_attns"  type="text" name="attn" id="recipient_attn" data-parsley-trigger="change focusout" ></textarea>
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input class="form-control business_phone_validation"  type="text" name="contact" id="recipient_contact" data-parsley-trigger="change focusout" data-parsley-pattern="(\(\d{3})\)\s\d{3}\-\d{4}$">
                                                <label>Telephone</label>
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                    <!--                                    <div class="col-md-6 col-sm-6">
                                        <div class="input-wrapper full-width">
                                        <div class="styled-input">
                                        <input class="form-control"  type="text" name="mobile"  id="recipient_mobile">
                                        <label>Mobile Number</label>
                                        <span></span>
                                        </div>
                                        </div>
                                        </div>-->
                                    <div class="col-md-6 col-sm-6">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input class="form-control email_validation" type="text" name="email" id="recipient_email">
                                                <label>Email</label>
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <div class="input-wrapper col-md-4 col-sm-4">
                                            <div class="styled-input">
                                                <textarea type="text" class="form-control"  name="address" id="recipient_address" data-parsley-required="true" data-parsley-required-message="Address is required" data-parsley-trigger="change focusout" data-parsley-trigger="change focusout" data-parsley-maxlength="50" data-parsley-maxlength-message="Address should have 50 characters or fewer."></textarea>
                                                <label>Address 1<span class="mandatory-field">*</span></label>
                                                <span></span>
                                            </div>
                                        </div>
                                    
                                        <div class="input-wrapper col-md-4 col-sm-4">
                                            <div class="styled-input">
                                                <textarea type="text" class="form-control"  name="address2" id="recipient_address2"  data-parsley-trigger="change focusout" data-parsley-maxlength="50" data-parsley-maxlength-message="Address 2 should have 50 characters or fewer."></textarea>
                                                <label>Address 2</label>
                                                <span></span>
                                            </div>
                                        </div>
                                        
                                        <div class="input-wrapper col-md-4 col-sm-4">
                                            <div class="styled-input">
                                                <textarea type="text" class="form-control"  name="address3" id="recipient_address3"  data-parsley-trigger="change focusout" data-parsley-maxlength="50" data-parsley-maxlength-message="Address 3 should have 50 characters or fewer."></textarea>
                                                <label>Address 3</label>
                                                <span></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-6 ">
                                        <div class="input-wrapper full-width">
                                            <div class="styled-input">
                                                <input class="form-control new_reipient_zip"  type="text" name="new_reipient_zip" value="{{old('new_reipient_zip')}}" id="new_addresscontact_zip">
                                                <input class="form-control zip_validation recipient_zip" type="hidden" name="zip" id="recipient_zip" data-parsley-required="true" data-parsley-required-message="Zip code is required" data-parsley-trigger="change focusout">
                                                <label>Zip Code<span class="mandatory-field">*</span></label>
                                                <span></span>
                                            </div>
                                        </div>
                                        <div id="zipreciptloader" style="display: none">
                                            <img width="150px" src="{{asset('images/loader.gif')}}"/>
                                        </div>
                                    </div>
                                    <!--<div class="col-md-6 col-sm-6">-->
                                    <!--    <div class="select-opt select2-container select2class  " style="position: absolute;">-->
                                    <!--    <label class="col-md-12 control-label" for="textinput">City<span class="mandatory-field">*</span></label> -->
                                    <!--        <select class="select2-hidden-accessible load_ajax_cities addresscontact_city_id"  name="city_id" id="recipientcity" style="width: 200px;" data-parsley-required="true" data-parsley-required-message="City is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true">-->
                                    <!--            <option value="">Select</option>    -->
                                    <!--        </select>-->
                                    <!--    </div>-->
                                    <!--</div>-->
                                    <!--<div class="col-md-6 col-sm-6 clear-b">-->
                                    <!--    <div class="select-opt">-->
                                    <!--        <label class="col-md-12 control-label" for="textinput">State<span class="mandatory-field">*</span></label>  -->
                                    <!--        <select class="selectpicker" id="recipient_state" name="state_id" data-parsley-required="true" data-parsley-required-message="State is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true">-->
                                    <!--            <option value="">Select</option>-->
                                    <!--            @foreach($states as $state)-->
                                    <!--            <option value="{{$state->id}}">{{$state->name}}</option>-->
                                    <!--            @endforeach-->
                                    <!--        </select>-->
                                    <!--    </div>-->
                                    <!--</div>-->
                                    <input class="new_recipientcity"  type="hidden" name="city_id" id="recipientcity" value="">
                                    <input class=""  type="hidden" name="state_id" id="recipient_state" value="">
                                
                                    <input type="hidden" name="verified_usps_address" id="verified_usps_address">
                                    <!--                                    <div class="col-md-6 col-sm-6 ">
                                        <div class="select-opt">
                                        <label class="col-md-12 control-label" for="textinput">County</label>  
                                        <select name="country_id" class="selectpicker" id="recipient_county" data-show-subtext="true" data-live-search="true">
                                            <option value="">Select</option>
                                        @foreach($countries as $county)
                                        <option value="{{$county->id}}">{{$county->name}}</option>
                                        @endforeach
                                        </select>
                                        </div>
                                        </div>-->
                                    <div class="col-md-12">
                                        <div class="form-bottom-btn pd-25">
                                            <input type="hidden" name="verified_addr" id="verified_addr" value="false">
                                            <input type="hidden" name="continue_recipient" id="continue_recipient">
                                            <button id="add_recipients" type="button" name="continue_add_recipient" class="btn btn-primary custom-btn customc-btn add_recipient" value="add"><span>Add Recipient</span></button>
                                            <button id="update_recipients" type="submit" name="continue"  class="btn btn-primary custom-btn customc-btn update_recipient" value="update" disabled=""><span>Update Recipient</span></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="form-bottom-btn save_btn_rec">
                                            <!--<input type="button" id="editBack" name="editBack" class="btn btn-primary custom-btn customb-btn" value="Back"/>-->
                                            <a type="button" id="editBack" name="editBack" class="btn btn-primary custom-btn customb-btn not-empty" value="Back"><span>Back</span></a>
                                            <a href="{{url('admin/dashboard')}}" class="btn btn-primary custom-btn customb-btn"><span>Cancel</span></a>
                                            <a href="{{url('account-manager/updateStatus/'.$id)}}" class="btn btn-primary custom-btn customb-btn"><span>Save for Later</span></a>
                                            <input type="button" id="createLabel" name="submit_continue" class="btn btn-primary custom-btn customc-btn" value="Continue"/>
                                        </div>
                                    </div>
                                </div>
                                <input type="text" name="" id="contact_customer_id" value="{{$customer_id}}" hidden="">
                            </div>
                        </form>
                        <input type="text" value="{{$id}}"  name="wo_id" id="wo_id" hidden=""></input>
                    </div>
                </div>
              @include('account_manager.research.verify_address')
                <!-- add new contacts-->
                <div id="contactModal" class="modal fade register-modal addcon-modal">
                              <div class="modal-dialog">
                                  <div class="modal-content">
                                      <div class="modal-header">
                                          <div id="success-msg"class="alert alert-success" style="display: none"></div>
                                          <div id="error-msg"class="alert alert-error" style="display: none"></div>
                                          <h4 class="modal-title">Add/Update Address Book</h4>
                                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                      </div>
                                      <div class="modal-body">
                                          <form method="POST" id="add_recipient_contact">
                                              {{csrf_field()}}
                                              <div class="col-md-6 col-sm-6 col-xs-12">
                                                  <div class="input-wrapper full-width">
                                                      <div class="styled-input">
                                                          <input class="form-control"  type="text" name="contact_name" value="{{old('contact_name')}}" id="contact_name">
                                                          <input type="hidden" name="contact_id" value="" id="contact_id">
                                                          <label>Name<span class="mandatory-field">*</span></label>
                                                          <span></span>
                                                          <p class="help-block">
                                                              <strong id="name-error"></strong>
                                                              </span>  
                                                      </div>
                                                  </div>
                                              </div>
                                              <div class="col-md-6 col-sm-6 col-xs-12 ">
                                                  <div class="input-wrapper full-width">
                                                    <label class="col-md-12 control-label" for="textarea">Attn/Additional Recipient Name<span class="mandatory-field"></span></label> 
                                                      <div class="styled-input">
                                                          <textarea class="form-control attn recipient_attns addressattn"   name="attn" value="{{old('attn')}}" id="attn" maxlength="50"></textarea>
                                                          <p class="help-block">
                                                              <strong id="attn-error"></strong>
                                                          </p>
                                                      </div>
                                                  </div>
                                              </div>
                                              <div class="col-md-6 col-sm-6 col-xs-12">
                                                  <div class="input-wrapper full-width">
                                                      <div class="styled-input">
                                                          <label class="lableall">Telephone</label>
                                                          <input class="form-control business_phone_validation addresscontact_no"  type="text" name="contact_no" value="{{old('contact_no')}}" id="contact_no">
                                                          <span></span>
                                                          <p class="help-block">
                                                              <strong id="contact-error"></strong>
                                                          </p>
                                                      </div>
                                                  </div>
                                              </div>
                                              <div class="col-md-6 col-sm-6 col-xs-12">
                                                  <div class="input-wrapper full-width">
                                                      <div class="styled-input">
                                                          <label class="lableall">Email</label>
                                                          <input class="form-control email_validation addresscontact_email"  type="email" name="contact_email" value="{{old('contact_email')}}" id="contact_email">
                                                          <span></span>
                                                          <p class="help-block">
                                                              <strong id="email-error"></strong>
                                                              </span>
                                                      </div>
                                                  </div>
                                              </div>
                                              <div class="col-md-12 col-sm-12 col-xs-12">
                                                  <div class="input-wrapper full-width">
                                                      <div class="styled-input">
                                                          <label class="lableall">Address<span class="mandatory-field">*</span></label>
                                                          <textarea class="form-control addresscontact_address"   name="contact_address" id="contact_address">{{old('contact_address')}}</textarea>
                                                          <span></span>
                                                          <p class="help-block">
                                                              <strong id="address-error"></strong>
                                                          </p>
                                                      </div>
                                                  </div>
                                              </div>
                                              &nbsp;
                                                <div class="col-md-6 col-sm-6 col-xs-12">
                                                    <div class="input-wrapper full-width">
                                                        <div class="styled-input">
                                                            <label class="lableall">Zip Code<span class="mandatory-field">*</span></label>
                                                            <input class="form-control"  type="hidden" name="selected_address_book" value="" id="selected_address_book">
                                                            <input class="form-control new_addresscontact_zip"  type="text" name="new_addresscontact_zip" value="{{old('new_addresscontact_zip')}}" id="new_addresscontact_zip">
                                                            <input class="form-control addresscontact_zip"  type="hidden" name="contact_zip" value="{{old('contact_zip')}}" id="contact_zip">
                                                            <!-- <input type="text" id='employeeid' readonly> -->
                                                            <input class="contact_zipurl"  type="hidden" name="contact_zipurl" id="contact_zipurl" value="{{ url('admin/ZipCodeLookups') }}">
                                                            <input class="contact_stateurl"  type="hidden" name="contact_stateurl" id="contact_stateurl" value="{{ url('admin/getStateName') }}">
                                                            <span></span>
                                                            <p class="help-block" >
                                                                <strong id="zip-error"></strong>
                                                            </p>
                                                        </div>
                                                        <div id="zippopuploader" style="display: none">
                                                            <img width="150px" src="{{asset('images/loader.gif')}}"/>
                                                        </div>
                                                    </div>
                                                </div>
                                              <!--  <div class="col-md-6 col-sm-6 col-xs-12">-->
                                              <!--            <div class="select-opt select2-container select2class  col-xs-12">-->
                                              <!--            <label class="col-md-12 control-label lableall" for="textinput">City<span class="mandatory-field">*</span></label> -->
                                              <!--                <select class="js-example-tags form-control load_ajax_cities addresscontact_city_id"  name="city_id" id="recipient_city"  data-parsley-required-message="City is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true">-->
                                              <!--                    <option value="">Select</option>    -->
                                              <!--                </select>-->
                                              <!--                <p class="help-block" style="margin:0">-->
                                              <!--                    <strong id="city-error"></strong>-->
                                              <!--                </p> -->
                                              <!--            </div>-->
                                              <!--        </div>-->
                                              <!--<div class="col-md-6 col-sm-6 col-xs-12">-->
                                              <!--    <div class="select-opt">-->
                                              <!--        <label class="col-md-12 control-label lableall">State<span class="mandatory-field">*</span></label>-->
                                              <!--        <select class="js-example-tags form-control addresscontact_state_id"  name="contact_state_id" data-parsley-required="true" data-parsley-required-message="State is required" data-parsley-trigger="change focusout" data-show-subtext="true" data-live-search="true" id="contact_state_id">-->
                                              <!--            <option value="">Select</option>-->
                                              <!--            @foreach($states as $state)-->
                                              <!--            <option value="{{$state->id}}" @if(old('contact_state_id')) selected="selected" @endif>{{$state->name}}</option>-->
                                              <!--            @endforeach-->
                                              <!--        </select>-->
                                              <!--        <p class="help-block">-->
                                              <!--            <strong id="state-error"></strong>-->
                                              <!--        </p>-->
                                              <!--    </div>-->
                                              <!--</div>-->
                                               <input class="newaddresscontactcityid"  type="hidden" name="city_id" id="recipient_city" value="">
                                              <input class="addresscontact_state_id"  type="hidden" name="contact_state_id" id="contact_state_id" value="">
                                           
                                           
                                  </form>
                              </div>
  
                              <div class="modal-footer">
                                  <div class="form-bottom-btn">
                                      <input type="hidden" name="edit_secondary_doc_id" value="{{$id}}" id="doc_id_recipient"/>
                                      <button type="submit" class="btn btn-primary custom-btn customc-btn" id="submit_contact"><span>Add/Update Address Book</span></button>
                                  </div>
                              </div>
                          </div>
                      </div>
                       <div id="contactModal" class="modal fade register-modal addcon-modal">
                  </div> 
  
  
  
              </div>
        </div>

    </section>
</div>

<form action="" method="POST" class="remove-attachment-model" id="remove-attachment-model">
    {!! csrf_field() !!}
    <div id="attachment-confirmation-modal" class="modal fade">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="icon-box">
                        <i class="fa fa-close"></i>
                    </div>
                    <h4 class="modal-title">Are you sure?</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <p>Do you really want to delete these records? This process cannot be undone.</p>
                </div>
                <div class="modal-footer">
                    <div class="form-bottom-btn">
                        <button type="button" class="btn btn-primary custom-btn customb-btn remove-attachement-from-delete-form" data-dismiss="modal"><span>Cancel</span></button>
                        <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>Confirm</span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<form action="" method="GET" class="remove-record-model">
    {!! csrf_field() !!}
    <div id="contact-confirmation-modal" class="modal fade">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <div class="modal-header">
                    <div class="icon-box">
                        <i class="fa fa-close"></i>
                    </div>
                    <h4 class="modal-title">Are you sure?</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                </div>
                <div class="modal-body">
                    <input type="text" name="contact_id" id="contact_id" hidden="">              
                    <p>Do you really want to delete these records? This process cannot be undone.</p>
                </div>
                <div class="modal-footer">
                    <div class="form-bottom-btn">
                        <button type="button" class="btn btn-primary custom-btn customb-btn remove-data-from-delete-form" data-dismiss="modal"><span>Cancel</span></button>
                        <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>Confirm</span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

@endsection

@section('adminlte_js')
<script>
    /*county */
    src_county = "{{ url('customer/city/autocompletecountyname/'.$notice_id) }}";

</script>
<script type="text/javascript" src="{{ asset('js/jquery-ui.min.js') }}"></script>
<script src="{{ asset('vendor/adminlte/plugins/datepicker/bootstrap-datepicker.js') }}" type="text/javascript"></script> 
<script type="text/javascript" src="{{ asset('js/account-manager.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/autocomplete_county.js') }}"></script>

<script type="text/javascript">
    src_city = "{{ url('customer/city/autocompletecityname') }}";
    var cityUrl = "{{ url('customer/city-list') }}";
     $(document).ready(function () {
        $('input,textarea,select').on('focus', function(e) {
            e.preventDefault();
            $(this).attr("autocomplete", "nope");  
        });
          
        //$('#rush_hour_charges').hidden();
        $('#rush_hour').click(function(){
            if ($(this).is(':checked'))
                $('#rush_hour_charges').show();
            else
                $('#rush_hour_charges').hide();
        });
         $(".load_ajax_cities").select2({
                tags:true,
              ajax: { 
               url: cityUrl,
               type: "get",
               dataType: 'json',
               //delay: 250,
               data: function (params) {
                return {
                  name: params.term // search term
                };
               },
               processResults: function (response) {
                 return {
                    results: response
                 };
               },
               cache: true
              }
             });   
    });
       
    $(document).ready(function () {
   
 
        var tomorrow = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
        $('#job_start_date').attr('readonly', 'true');
       // $('#last_date_on_the_job').attr('readonly', 'true');

        $("#job_start_date").datepicker({
            todayBtn: 1,
            autoclose: true,
        }).on('changeDate', function (selected) {
            var minDate = new Date(selected.date.valueOf());
            $('#last_date_on_the_job').datepicker('setStartDate', minDate);
        });

        $("#last_date_on_the_job").datepicker()
                .on('changeDate', function (selected) {
                    var maxDate = new Date(selected.date.valueOf());
                    $('#job_start_date').datepicker('setEndDate', maxDate);
                });
        $('.shipping_date').datepicker({dateFormat: 'yy-mm-dd',
            minDate: 0});
        $('#myModal').modal('hide');
        $('.selectpicker').selectpicker("refresh");
        //Mailing city value autocomplete

       
    });

//contact address dispaly
    $('.contracted_bys').on('change', function () {
            document.getElementById('hidden-panel').style.display = 'block';
            var name = $('.contracted_bys').val();
            var recipt_id = $(this).find(':selected').attr('data-id');
            var customer_id = $('.remove-record').attr('data-customer');
            $.ajax({
                data: {'name': name, 'recipt_id': recipt_id,'customer_id' : customer_id},
                type: "post",
                url: "{{ url('customer/get-contacts') }}",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (res) {   
                   // console.log(res); 
                    $('#addname').val(res.company_address);
                },
    
            });
        });
//Start to submit note
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
//save for later
    $(".save_for_later").on('click', function () {
        $('#create_work_order').parsley().destroy();
    });

    //Start to fetch parent work order number of customers
    $('#soft_doc_parent_work_order').select2({
            minimumInputLength: 1,
            ajax: {
                url : "{{ url('account-manager/research/autocomplete_V1?work_order_id='.$id) }}",
                dataType: 'json',
                
                delay: 250,
                type: "get",
                processResults: function (data) {
                return {
                    results:  $.map(data, function (item) {
                        return {
                            text: item,
                            id: item
                        }
                    })
                };
                },
                cache: true
            }
        });

    /*$('#soft_doc_parent_work_order').find('option').remove().end().append('<option value="">Select</option>');
        var work_ordid=$('#doc_id_recipient').val();
        @if(!Request::isMethod('post'))
            $.ajax({
                type: "get",
                data: {work_order_id:work_ordid},
                url: "{{url('account-manager/research/autocomplete')}}",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                cache : true,
                //async: false,
                success: function (response) {
                //console.log(response);
                     $.each(response, function (e, val) {
                        $("#soft_doc_parent_work_order").append("<option value='" + val.id + "'>" + val.id + "</option>");
                        $("#soft_doc_parent_work_order").selectpicker("refresh");
                    });
                    if ($('#parent_work_order_id').val()) {
                        $("#soft_doc_parent_work_order").val($('#parent_work_order_id').val());
                        $("#soft_doc_parent_work_order").selectpicker("refresh");
                    }
                }
            }); 
        @endif*/

        //End to fetch parent work order number of customers

//Start to fetch data of related work order

    $('#soft_doc_parent_work_order').on('change', function () {
        $(".delete_parent_attachment").remove();
        $.ajax({
            data: {
                'work_order_no': $('#soft_doc_parent_work_order').val()
            },
            type: 'post',
            url: "{{url('customer/work-order-document/work_order_details')}}",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                if (response.success) {
                    var stateid = "";
                    $.each(response.success, function (key, val) {
                        /* if (val.name != 'state' && val.name != 'country') {
                         var field_name = val.name;
                         $("#" + field_name).val(val.value);
                         } else if (val.name == 'state') {
                         $('#soft_notice_state_value').val(val.value);
                         $("#soft_notice_state_value").selectpicker("refresh");
                         
                         
                         } else if (val.name = 'city') {
                         $('#soft_notice_city_value').val(val.value);
                         $("#soft_notice_city_value").selectpicker("refresh");
                         
                         } else if (val.name == 'county') {
                         $('#country_id').val(val.value);
                         }*/
                        if (val.name == 'state') {
                            $('#soft_doc_state_value').val(val.value);
                            $("#soft_doc_state_value").selectpicker("refresh");


                        } else if (val.name == 'city') {
                            $('#soft_doc_city_value').val(val.value);
                            $("#soft_doc_city_value").selectpicker("refresh");

                        } else if (val.name == 'county') {

                            $('#recipient_county_id').val(val.value);
                            $("#county").val(val.county_name);
                        } else if (val.name == 'project_type') {

                            $('#project_type').val(val.value);
                            $("#project_type").selectpicker("refresh");

                        } else if (val.name == 'your_role') {

                            $('#your_role').val(val.value);
                            $("#your_role").selectpicker("refresh");

                        } else if (val.name == 'contracted_by') {
                            $("#contracted_by_select").val(val.value).trigger('change');
                            //    $('#contracted_by_select').val(val.value);
                            //    $("#contracted_by_select").selectpicker("refresh");

                        } else if (val.name == 'job_start_date') {
                            $('#job_start_date').val(val.value);
                            $('#enter_into_agreement').val(val.value);
                        }else if (val.name == 'last_date_on_the_job') {
                        $('#last_date_of_labor_service_furnished').val(val.value);
                        $('#last_date_on_the_job').val(val.value);

                            }  else {
                            var field_name = val.name;
                            var vars = val.value;
                            let result = vars.replace(/<br \/>/gi, "<p>");
                            $("#" + field_name).val(result);
                        }
                    });
                }
                if (response.parent_attachment) {
                    // console.log(response.parent_attachment);
                    $.each(response.parent_attachment, function (k, v) {

                        var f1 = v.db_arr[2];
                        var f2 = f1.split('.')[0];
                        var row = '<tr class="delete_parent_attachment" id="refreshdelete' + v.db_arr[4] + '">';
                        $.each((v.ajax_arr), function (i, d) {
                            $('.norecords').closest('tr').remove();
                            var display_url = '{{asset("attachment/work_order_document/parent_document")}}/' + v.db_arr[2];
                           /* if (i == 2) {
                                row += '<td><a target="_blank" href="' + display_url + '">' + d + '</a></td>';
                            } else {*/
                                row += '<td>' + d + '</td>';
                          //  }
                        });
                        var attachment_url = '{{url("customer/work-order-document/parent_document/removeattachment")}}';
                         var display_url = '{{asset("attachment/work_order_document/parent_document")}}/' + v.db_arr[2];
                        /*row += "<td><a class='btn btn-secondary item red-tooltip remove-attachment' data-toggle='modal' data-url='" + attachment_url + "'  data-file='" + v.db_arr[2] + "' data-target='#attachment-confirmation-modal' data-placement='bottom' title='Remove'><span class='icon-cross-remove-sign'></span></a><a class='btn btn-secondary item red-tooltip edit-attachment' data-file='" + v.db_arr[2] + "' data-type='" + v.db_arr[0] + "'data-title='" + v.db_arr[1] + "'data-id='" + v.db_arr[4] + "'><span class='icon-pencil-edit-button'></span></a><a target='_blank' href='" + display_url + "' class='btn btn-secondary item red-tooltip'><i class='fa fa-eye'></i></a></td>";*/
                        if(v.db_arr[2].length>0){
                        row += "<td><a target='_blank' href='" + display_url + "' class='btn btn-secondary item red-tooltip'><i class='fa fa-eye'></i></a></td>";
                    }else{
                        row += "<td></td>";
                    }

                        row += '</tr>';
                        $('#edit_document_table tbody').append(row);

                    });

                }
                $('input').each(function () {
                    if ($(this).val().length > 0) {
                        $(this).addClass('not-empty');
                    } else {
                        $(this).removeClass('not-empty');
                    }
                });
                $('textarea').each(function () {
                    if ($(this).val().length > 0) {
                        $(this).addClass('not-empty');
                    } else {
                        $(this).removeClass('not-empty');
                    }
                });

            }
        });
    });
//End to fetch data of related work order
//End to submit note
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip({
            trigger: 'click',
            placement: 'bottom'
        });
          $('textarea').each(function () {
            if ($(this).val().length > 0) {
                $(this).addClass('not-empty');
            } else {
                $(this).removeClass('not-empty');
            }
        });
        $('textarea').each(function () {
            if ($(this).val().length > 0) {
                $(this).addClass('not-empty');
            } else {
                $(this).removeClass('not-empty');
            }
        });
        $('input,textarea').change(function () {
            if ($(this).val().length > 0) {
                $(this).addClass('not-empty');
            } else {
                $(this).removeClass('not-empty');
            }
        });

        $('#showattorneysfee').hide();
        //collection_for_attorneys_fees
        $('#attorneys_fees').click(function () {

            $('#showattorneysfee').toggle();
        }) //end
        //net to address varification
        var verified_address_count = '{{(isset($verified_address_count) && count($recipients) == $verified_address_count)?"yes":"no"}}';
          var checkFieldsUrl = "{{ url('account-manager/research/checkFields') }}";
          $('#createLabel,#address_tab').click(function (e) {
              var this_id = $(this).attr('id');
              if (this_id == 'address_tab')
              {
                  e.preventDefault();
                  if (verified_address_count == 'no')
                  {
                      $('#recipientVerifiedCountModal').modal('show');
                      return false;
                  } else
                  {
                      return true;
                  }
              } else
              {
                  if (verified_address_count == 'no')
                  {
                      $('#recipientVerifiedCountModal').modal('show');
                  } else
                  {
                      $('#acceptance_btn').click();
                  }
              }
          })
        $('#rejection_btn').click(function () {
            $('#recipientVerifiedCountModal').modal('hide');
        });

        $(document).on('click', '#acceptance_btn', function () {
            var id = $('#wo_id').val();
            $.ajax({
                data: {'id': id},
                type: "post",
                url: checkFieldsUrl,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                    if (response == 0) {
                        //$("button[name=generate]").addAttr("disabled");
                        $('.display-ib li').removeClass('active');
                        $('#address_tab').addClass('active');
                        $('.notice-tabs .tab-pane').removeClass('active');
                        $('#verify_address').addClass('active');
                    } else if (response == 1) {
                        $("a[name=generate]").removeAttr("disabled");
                        $("a[name=generate]").removeAttr("title");
                        $("a[name=generate]").removeAttr("style");
                        $("input[name=create-label]").removeAttr("disabled");
                        $("input[name=create-label]").removeAttr("title");
                        $('.display-ib li').removeClass('active');
                        $('#address_tab').addClass('active');
                        $('.notice-tabs .tab-pane').removeClass('active');
                        $('#verify_address').addClass('active');
                    }
                }
            });
            $('#recipientVerifiedCountModal').modal('hide');
        });
        //back to project tab
        $('#editBack').click(function () {
            $('.display-ib li').removeClass('active');
            $('.display-ib li:first-child').addClass('active');
//            $('#project_tab').addClass('active');
            $('.notice-tabs .tab-pane').removeClass('active');
            $('#project').addClass('active');
        })

        $('.datepicker').datepicker({
            autoclose: true
        });
        //End of document section

        //Start Document Functionality

        $('.doc_field').hide();
        $('.doc_file').hide();
        $('.doc_visibility').hide();
        $('#edit_document_type').change(function () {

            var selected_doc_type = $(this).val();
            $('#edit_doc_input_id').val("");
            if (selected_doc_type == 1) {
                $('.doc_field').show();
                $('.doc_file').show();
                $('.doc_visibility').show();
                $('#doc_field').text('Bond Number');
            } else if (selected_doc_type == 2) {
                $('.doc_field').show();
                $('.doc_file').show();
                $('.doc_visibility').show();
                $('#doc_field').text('NOC Book/Page');
            } else if (selected_doc_type == 3) {
                $('.doc_field').show();
                $('.doc_file').show();
                $('.doc_visibility').show();
                $('#doc_field').text('Permit No');
            } else if (selected_doc_type == 5) {
                $('.doc_field').show();
                $('.doc_file').show();
                $('.doc_visibility').show();
                $('#doc_field').text('Invoice No.');
            } else if (selected_doc_type == 4 || selected_doc_type == 6 ||
                    selected_doc_type == 7 || selected_doc_type == 8 ||
                    selected_doc_type == 9 || selected_doc_type == 13) {
                $('.doc_field').show();
                $('.doc_file').show();
                $('.doc_visibility').show();
                $('#doc_field').text('Description');
            }else if (selected_doc_type == 10) {
                $('.doc_field').show();
                $('.doc_file').show();
                $('.doc_visibility').show();
                $('#doc_field').text('Folio No.');
            }else if (selected_doc_type == 11) {
                $('.doc_field').show();
                $('.doc_file').show();
                $('.doc_visibility').show();
                $('#doc_field').text('COL Signed.');
            }else if (selected_doc_type == 12) {
                $('.doc_field').show();
                $('.doc_file').show();
                $('.doc_visibility').show();
                $('#doc_field').text('Recorded COL/SCOL.');
            } else if (!selected_doc_type) {
                $('.doc_field').hide();
                $('.doc_file').hide();
                $('.doc_visibility').hide();
            }

        });
        var result_arr = [];
        $("#submit_document").on("click", function (e) {
        if($('.update_doc').html()=='Add')  {
            e.preventDefault();
            var extension = $('#edit_result_file').val().split('.').pop().toLowerCase();

            if ($('#edit_result_file').val() !== "" && $.inArray(extension, ['csv', 'xls', 'xlsx', 'pdf', 'doc', 'docx', 'jpg', 'jpeg']) == -1) {
                $('#errormessage').html('Please Select Valid File... ');
                $('#errormessage').css('display', 'block');
            } else {

                $('#errormessage').html('');
                var file_data = $('#edit_result_file').prop('files')[0];
                var selected_document_type = $('#edit_document_type').val();
                var doc_input = $('#edit_doc_input_id').val();
                var doc_visibile = $("input[name='visibility']:checked").val();
                var remove_file = $('#remove_file').val();
                var form_data = new FormData();

                form_data.append('file', file_data);
                form_data.append('document_type', selected_document_type);
                form_data.append('doc_value', doc_input);
                form_data.append('visibility', doc_visibile);
                form_data.append('remove_file', remove_file);
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-Token': $('meta[name=_token]').attr('content')
                    }
                });
                $.ajax({
                    url: "{{url('customer/work-order/attachments')}}", // point to server-side PHP script
                    data: form_data,
                    type: 'POST',
                    contentType: false, // The content type used when sending data to the server.
                    cache: false, // To unable request pages to be cached
                    processData: false,
                    success: function (response) {
                        var data = jQuery.parseJSON(response);
                        var ran_no = Math.random() * (100 - 1) + 1;
                        ran_no = String(ran_no).replace('.','_');
                        data.db_arr.push('refreshdelete'+ran_no);
                        result_arr.push(data.db_arr);
                        result_arr.push('|');
                        // console.log(result_arr);
                        $('#edit_soft_notices_attachment').val(result_arr);

                        var f1 = data.db_arr[2];
                        var f2 = f1.split('.')[0];
                        /*var ran_no = Math.random() * (100 - 1) + 1;
                                ran_no = String(ran_no).replace('.','_')*/
                        var row = '<tr id="refreshdelete' + ran_no + '">';
                        $.each((data.ajax_arr), function (i, d) {
                            $('.norecords').closest('tr').remove();
                           
                            /*if (i == 1) {
                                row += '<td><a target="_blank" href="' + display_url + '">' + d + '</a></td>';
                            } else {*/
                                row += '<td>' + d + '</td>';
                           // }
                            //hide fields after add 
                        $('.doc_field').hide();
                        $('.doc_file').hide();
                        $('.doc_visibility').hide();
                        $('#edit_document_type').val('');
                        $('#edit_document_type').selectpicker('refresh');
                        $('#edit_result_file').val('');
                        $('#edit_document_name').val('');
                        $('#edit_doc_input_id').val('');
                        $('#remove_file').val('0');
                        $('#preview').css('display','none');
                        $('#remove_icon').css('display','none');
                        });
                        var display_url = '{{asset("attachment/work_order_document")}}/' + data.db_arr[2];
                        var attachment_url = '{{url("customer/work-order/removeattachment")}}';
                        row += "<td><a class='btn btn-secondary item red-tooltip remove-attachment' data-toggle='modal' data-url='" + attachment_url + "'  data-file='" + data.db_arr[2] + "' data-target='#attachment-confirmation-modal' data-placement='bottom' title='Remove'><span class='icon-cross-remove-sign'></span></a><a class='btn btn-secondary item red-tooltip edit-attachment' data-file='" + data.db_arr[2] + "' data-type='" + data.db_arr[0] + "'data-title='" + data.db_arr[1] + "'  data-file-original-name='" + data.db_arr[3] + "'data-file-extension='" + data.ajax_arr[4] +"'><span class='icon-pencil-edit-button'></span></a>";
                        if(data.db_arr[2].length>0){
                        row += "<a target='_blank' href='" + display_url + "' class='btn btn-secondary item red-tooltip'><i class='fa fa-eye'></i></a>";
                        }
                        row += '</td></tr>';
                        $('#edit_document_table tbody').append(row);
                    }

                });
            }
            }else{

                    e.preventDefault();
                    var extension = $('#edit_result_file').val().split('.').pop().toLowerCase();

                    if ($('#edit_result_file').val() != "" && $.inArray(extension, ['csv', 'xls', 'xlsx', 'pdf', 'doc', 'docx', 'jpg', 'jpeg']) == -1) {
                        $('#errormessage').html('Please Select Valid File... ');
                        $('#errormessage').css('display', 'block');
                } else {

                        $('#errormessage').html('');
                        var file_data = $('#edit_result_file').prop('files')[0];
                        var selected_document_type = $('#edit_document_type').val();
                        var doc_input = $('#edit_doc_input_id').val();
                        var doc_id = $('#edit_doc_id').val();
                        var doc_visibile = $("input[name='visibility']:checked").val();
                        var work_order_id = $('#work_order').val();
                        var remove_file = $('#remove_file').val();
                        var edit_document_name = $('#edit_document_name').val();
                        var edit_document_original_name = $('#edit_document_name').attr('data-file-original-name');
                        var edit_document_extension = $('#edit_document_name').attr('data-file-extension');
                        var row_id = $('#row_id').val();
                        //remove array value
                        var result;
//console.log(result_arr);
                        for( var i = 0, len = result_arr.length; i < len; i++ ) {
                           // console.log(len);console.log(result_arr[i]);
                            for( var j = 0, len1 = result_arr[i].length; j < len1; j++ ){
                              //  console.log(result_arr[j]);
                                if( result_arr[i][j] === row_id ) {
                                    result = i;
                                    break;
                                    }
                            }
                        }
                        result_arr.splice(result, 1);
                        if(result_arr.length==1){
                            result_arr.splice(0, 1);
                        }
                        $('#edit_soft_notices_attachment').val(result_arr);
                        //console.log(result_arr,'hi');
                       console.log(result_arr);
                        if(doc_id==''){
                           var doc_id = 0;
                        }

                       
//console.log(edit_document_name,edit_document_original_name,edit_document_extension);
                        var form_data = new FormData();
                         if(file_data)
                        {
                            form_data.append('file', file_data);
                        }else{
                            form_data.append('file_name',edit_document_name);
                            form_data.append('file_original_name',edit_document_original_name);
                            form_data.append('extension',edit_document_extension);
                        }
                        //form_data.append('file', file_data);
                        form_data.append('document_type', selected_document_type);
                        form_data.append('doc_value', doc_input);
                        form_data.append('visibility', doc_visibile);
                        form_data.append('doc_id', doc_id);
                        form_data.append('work_order_id',work_order_id);
                        form_data.append('remove_file',remove_file);

                        
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-Token': $('meta[name=_token]').attr('content')
            }
            });
            $.ajax({
                            url: "{{url('customer/work-order/edit/attachments')}}", // point to server-side PHP script
                            data: form_data,
                            type: 'POST',
                            contentType: false, // The content type used when sending data to the server.
                            cache: false, // To unable request pages to be cached
                            processData: false,
                            success: function (response) {
                                var data = jQuery.parseJSON(response);
                               // result_arr.push(data.db_arr);
                                //result_arr.push('|');
                                //$('#edit_soft_notices_attachment').val(result_arr);
                                //delete current row
                                var del_var = $('#row_id').val();
                                //console.log(data.db_arr);
                                $("#" + del_var).remove();
                                //add id to new row
                                console.log(data.ajax_arr,'hi');
                                if(data.db_arr[2] != null){
                                var f1 = data.db_arr[2];//console.log(f1);
                                var f2 = f1.split('.')[0];

                            }

                                if(data.ajax_arr[2] == null){
                                    data.db_arr[2] = '';
                                }
                                var row = '<tr id="refreshdelete' + data.db_arr[5] + '">';
                                $.each((data.ajax_arr), function (i, d) {
                                    $('.norecords').closest('tr').remove();
                                    var display_url = '{{asset("attachment/work_order_document")}}/' + data.db_arr[2];
                                   /* if (i == 1) {
                                        row += '<td><a target="_blank" href="' + display_url + '">' + d + '</a></td>';
                                    } else {*/
                                    if(d==null){
                                        row += '<td>' + " " + '</td>';
                                        }else{
                                        row += '<td>' + d + '</td>';
                                    }
                        //}
                        //
                        $('.doc_field').hide();
                        $('.doc_file').hide();
                        $('.doc_visibility').hide();
                        $('#edit_document_type').val('');
                        $('#edit_document_type').selectpicker('refresh');
                       if($('#submit_document span').html()=="Update"){
                            $('#submit_document span').html('Add');
                       }
                        $('#edit_result_file').val('');
                        $('#edit_doc_input_id').val('');
                        $('#remove_file').val('0');
                        $('#preview').css('display','none');
                        $('#remove_icon').css('display','none');
                        $('#edit_document_name').val('');

                            
             });
            var display_url = '{{asset("attachment/work_order_document")}}/' + data.db_arr[2];
            var attachment_url = '{{url("customer/edit/work-ordwer/removeattachment")}}';
            row += "<td><a class='btn btn-secondary item red-tooltip remove-attachment' data-toggle='modal' data-url='" + attachment_url + "'  data-file='" + data.db_arr[2] + "' data-target='#attachment-confirmation-modal' data-placement='bottom' title='Remove' data-id='" + data.db_arr[5]+"'><span class='icon-cross-remove-sign'></span></a><a class='btn btn-secondary item red-tooltip edit-attachment' data-file='" + data.db_arr[2] + "' data-type='" + data.db_arr[6] + "' data-title='" + data.db_arr[1] + "' data-id='" + data.db_arr[5] + "'data-file-original-name='" + data.db_arr[3] + "'data-file-extension='" + data.db_arr[7] + "'><span class='icon-pencil-edit-button'></span></a>";

            if(data.db_arr[2].length>0){
                        row += "<a target='_blank' href='" + display_url + "' class='btn btn-secondary item red-tooltip'><i class='fa fa-eye'></i></a>";
                    }
            row += '</td></tr>';
                                $('#edit_document_table tbody').append(row);

                            }
                        });
                    }

            }

        });

        //End Document Functionality

        //Add recipient contact dynamic

    })
    

    $(document).on('click', '.remove-attachment', function () {
        var url = $(this).attr('data-url');
        var file = $(this).attr('data-file');
        var id = $(this).attr('data-id');
        var row_id = $(this).closest('tr').attr('id');
        $(".remove-attachment-model").attr("action", url);
        $("input[name='file']").remove();
        $("input[name='id']").remove();
        $("input[name='row_id']").remove();
        $('body').find('.remove-attachment-model').append('<input name="file" type="hidden" value="' + file + '">');
        $('body').find('.remove-attachment-model').append('<input name="id" type="hidden" value="' + id + '">');
        $('body').find('.remove-attachment-model').append('<input name="_method" type="hidden" value="POST">');
        $('body').find('.remove-attachment-model').append('<input name="row_id" type="hidden" value="' + row_id + '">');
    });
    function openTab(th)
            {
                window.open(th.name,'_blank');
            }
    $('#remove_icon').click(function(){
        $('#preview').css('display','none');
        $('#remove_icon').css('display','none');
        $('#remove_file').val('1');
         $('#edit_result_file').val('');


    })
     $('#edit_result_file').change(function(e){
            var fileName = e.target.files[0].name;
            doc_icon = fileName.split('.')[1];
             if(doc_icon == 'pdf'){
             $('#preview').html('<i class="fa fa-file-pdf-o" style="font-size:36px"></i>');
                }else if(doc_icon == 'doc' || doc_icon == 'docx'){
                    $('#preview').html('<i class="fa fa-file-word-o" style="font-size:36px"></i>');
                }else if(doc_icon == 'xls' || doc_icon == 'xlsx'){
                    $('#preview').html('<i class="fa fa-file-excel-o" style="font-size:36px"></i>');
                }else if(doc_icon == 'jpg' || doc_icon == 'jpegs'){
                    $('#preview').html('<i class="fa fa-file-image-o" style="font-size:36px"></i>');
                }else{
                     $('#preview').html('<i class="fa fa-file" style="font-size:36px"></i>');
                }
           // $('#preview').html(fileName);
            $('#preview').css('display','block');
            $('#remove_icon').css('display','block');
            //alert('The file "' + fileName +  '" has been selected.');
        });
     $(document).on('click', '.edit-attachment', function () {
        var row_id = $(this).closest('tr').attr('id');
       // $('#submit_document').removeClass('old_class').addClass('new_class');
        var type = $(this).attr('data-type');
        var file = $(this).attr('data-file');
        var title = $(this).attr('data-title');
        var id = $(this).attr('data-id');
        $('#edit_document_name').val(file);
        $('#edit_document_name').attr('data-file-original-name',$(this).attr('data-file-original-name'));
        $('#edit_document_name').attr('data-file-extension',$(this).attr('data-file-extension'));
        $('#edit_doc_input_id').val(title);
        $('#edit_doc_id').val(id);
        $('#row_id').val(row_id);
        $('#preview').css('display','block');
        $('#remove_icon').css('display','block');

        //console.log(file);
        var APP_URL = {!! json_encode(url('/')) !!}
       // console.log(APP_URL);
        $("#open_doc").attr('name',APP_URL+'/attachment/work_order_document/'+file);
        doc_icon = file.split('.')[1];
        if(doc_icon == 'pdf'){
             $('#preview').html('<i class="fa fa-file-pdf-o" style="font-size:36px"></i>');
        }else if(doc_icon == 'doc' || doc_icon == 'docx'){
            $('#preview').html('<i class="fa fa-file-word-o" style="font-size:36px"></i>');
        }else if(doc_icon == 'xls' || doc_icon == 'xlsx'){
            $('#preview').html('<i class="fa fa-file-excel-o" style="font-size:36px"></i>');
        }else if(doc_icon == 'jpg' || doc_icon == 'jpegs'){
            $('#preview').html('<i class="fa fa-file-image-o" style="font-size:36px"></i>');
        }else if (doc_icon == 'csv'){
            $('#preview').html('<i class="fa fa-file" style="font-size:36px"></i>');
        }else{
            $('#preview').html('');
            $('#preview').css('display','none');
            $('#remove_icon').css('display','none');
        }
       
        //$('#edit_result_file').val(APP_URL+'/attachment/work_order_document/'+file);
        var id1 = file.split('.')[0];

        var id = $(this).attr('data-id');
        $('.doc_field').show();
        $('.doc_file').show();
      //  $('.doc_visibility').show();
        $('.update_doc').text('Update');
        $('#edit_document_type').val(type);
        $('#edit_document_type').selectpicker('refresh');
       /* $('#errormessage').text(file);
        $('#errormessage').css('display', 'block');*/
        var selected_doc_type = type;
            //$('#edit_doc_input_id').val("");
            if (selected_doc_type == 1) {
                $('.doc_field').show();
                $('.doc_file').show();
                $('.doc_visibility').show();
                $('#doc_field').text('Bond Number');
            } else if (selected_doc_type == 2) {
                $('.doc_field').show();
                $('.doc_file').show();
                $('.doc_visibility').show();
                $('#doc_field').text('NOC Book/Page');
            } else if (selected_doc_type == 3) {
                $('.doc_field').show();
                $('.doc_file').show();
                $('.doc_visibility').show();
                $('#doc_field').text('Permit No');
            } else if (selected_doc_type == 5) {
                $('.doc_field').show();
                $('.doc_file').show();
                $('.doc_visibility').show();
                $('#doc_field').text('Invoice No.');
            } else if (selected_doc_type == 4 || selected_doc_type == 6 ||
                    selected_doc_type == 7 || selected_doc_type == 8 ||
                    selected_doc_type == 9) {
                $('.doc_field').show();
                $('.doc_file').show();
                $('.doc_visibility').show();
                $('#doc_field').text('Description');
            } else if (selected_doc_type == 10) {
                $('.doc_field').show();
                $('.doc_file').show();
                $('.doc_visibility').show();
                $('#doc_field').text('Folio No.');
            } else if (selected_doc_type == 11) {
                $('.doc_field').show();
                $('.doc_file').show();
                $('.doc_visibility').show();
                $('#doc_field').text('COL Signed.');
            } else if (selected_doc_type == 12) {
                $('.doc_field').show();
                $('.doc_file').show();
                $('.doc_visibility').show();
                $('#doc_field').text('Recorded COL/SCOL.');
            } else if (!selected_doc_type) {
                $('.doc_field').hide();
                $('.doc_file').hide();
                $('.doc_visibility').hide();
            }
         
    });

    $(document).on('click', '.remove-duplicate-attachment', function () {
        var file = $(this).attr('data-file');
        var url = $(this).attr('data-url');
        $(".remove-attachment-model").attr("action", url);
        $("input[name='file']").remove();
        $('body').find('.remove-attachment-model').append('<input name="file" type="hidden" value="' + file + '">');
        $('body').find('.remove-attachment-model').append('<input name="_method" type="hidden" value="POST">');
    });

    var remove_arr = [];
    $('#remove-attachment-model').submit(function (event) {
        event.preventDefault();
        var file = $('input[name=file]').val();
        var id = file.split('.')[0];
        var id = $('input[name=id]').val();
        var row_id = $('input[name=row_id]').val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name=_token]').attr('content')
            }
        });
        var submit_url = $(this).attr("action");
        $.ajax({
            type: 'POST',
            url: submit_url,
            data: $('#remove-attachment-model').serialize(),
            success: function (response) {
                if (response.success) {
                    $('#attachment-confirmation-modal').modal('hide');
                    remove_arr.push(row_id);
                    $('#remove_doc').val(remove_arr);
                    var del_var = "refreshdelete" + id;

                    $("#" + row_id).remove();
                }
            }
        });
    });
    $(document).on('click', '.remove-record', function () {
        //alert( window.location.href + '#recipient');
        var recipient_id = $(this).attr('data-id');
        var notice_id = $(this).attr('data-notice-id');
        var work_order_id = $(this).attr('data-workorder-id');

        var customer_id = $(this).attr('data-customer');
        var url = "{{url('account-manager/recipient/delete/')}}" + '/' + recipient_id + '/' + notice_id + '/' + work_order_id + '/' + customer_id;
 
        if (url.indexOf("#") ==-1){
            url += '#recipient'; 
        }
        $(".remove-record-model").attr("action", url);
    });

    var src = "{{ url('customer/city/autocompletecityname') }}";
    var note_url = '{{url("customer/work-order/store_note")}}';
    var correction_url = '{{url("customer/work-order/store_correction")}}';
    var get_contact = "{{ url('customer/get-contacts') }}";
    var get_recipient_data = "{{url('customer/work-order/get_recipient_data')}}"
    var contact_add = '{{url("customer/contact_workorderrecipient/store")}}';
    var get_recipient_contacts = "{{url('customer/getAllWorkOrderContacts')}}";
    var update_address_book = "{{url('customer/wo_update_recipient_contact')}}";
    var customer_ids = '{{$customer_id}}';

</script>
<script type="text/javascript">
//Verify address code
    $(document).ready(function () {


        //Check if all labels are generated
        var chk_all_label_generated = '<?php
                                                                            if ($label_generated_count == count($recipients)) {
                                                                                echo "yes";
                                                                            } else {
                                                                                echo "no";
                                                                            }
                                                                            ?>';
        if (chk_all_label_generated == 'yes')
        {
            $('.save_labels_for_later').hide();
        }
        //Check if work order status is mailing then don't show proceed to mailing button
        var chk_work_order_status = '<?php
                                                                            if ($work_order_status == 5) {
                                                                                echo 'yes';
                                                                            } else {
                                                                                echo 'no';
                                                                            }
                                                                            ?>';
        if (chk_work_order_status == 'yes')
        {
            $('.createLabel1').hide();
        }

        $('.shipping_date').datepicker({dateFormat: 'yy-mm-dd'});
        $('#myModal').modal('hide');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('#add_recipients').on('click', function (event) {
            $('#continue_recipient').val('add');
        });
        $('#update_recipients').on('click', function (event) {
            $('#continue_recipient').val('update');
        });
        /* code for verifying recipient address */
        $('#add_recipients,#update_recipients').on('click', function (event) {
            event.preventDefault();
            var form_valid = $('#store_recipients').parsley().validate();
            var url ='';
            if ($('#store_recipients').attr('action').indexOf("#") == -1 ){
                url += '#recipient';
            } 
            $('#store_recipients').attr('action',$('#store_recipients').attr('action') + url );
            if (form_valid != false)
            {
                $('#pleaseWait').modal('show');
                var address = $('#recipient_address').val();
                var state = $('#recipient_state').val();
                // var city = $('#recipient_city_id').val();
                var city = $('#recipientcity').val();
                var zip = $('#recipient_zip').val();
                var name = $('#recipient_name').val();

                //Updated Fields
                var address2 = $('#recipient_address2').val();
                var address3 = $('#recipient_address3').val();
                var recipient_department = $('#recipient_department').val();
                var recipient_contact = $('#recipient_contact').val();
                var recipient_email = $('#recipient_email').val();
                var recipient_attn = $('#recipient_attn').val();
                //Updated Fields


                $.ajax({
                    type: 'POST',
                    url: '{{url("account-manager/research/getVerifiedAddress")}}',
                    data: {address: address, state: state, city: city, zip: zip, name: name, address2:address2,address3:address3, recipient_department: recipient_department, recipient_contact: recipient_contact, recipient_email: recipient_email, attn: recipient_attn },
                    success: function (data1) {
                        $('#pleaseWait').modal('hide');
                        var data = JSON.parse(data1);
                        var alternate_addresses = '';
                        if (data.response == 'success') {
                            var message = "This address is fully verified";
                            $('#verified_usps_address').val(data.data);
                            $('#address_verification_message').html(message);
                            $('#myModal').modal('show');
                            $('#store_recipients').submit();
                        } else if (data.response == 'error' && data.alternate_addresses) {
                            $('#alternate_addresses').html("<span style='color:red'>" + data.message + "</span><br/>" + data.alternate_addresses);
                            $('input[name="select_addr"]').attr('required', 'required');
                            $('#address_verification_modal').modal('show');
                        } else {
                            if (data.overridehashpresent == true)
                            {
                                $('#verified_usps_address').val(data.data);
                                var message = data.message + 'Do you accept this address?';
                                $('#address_verification_message').html(message);
                                $('#myModal').modal('show');
                                $('#myModal .modal-footer').html('<button type="button" class="btn btn-success" id="accept_address">Accept</button><button type="button" class="btn btn-warning" id="reject_address" data-dismiss="modal">Reject</button>');
                            } else
                            {
                                $('#address_verification_message').html(data.message);
                                $('#myModal').modal('show');
                            }
                        }
                    },
                    error: function (data) {
                        $('#pleaseWait').modal('hide');
                        console.log('error with AJAX call for addr verification');
                    }
                });
            } else
            {
                console.log('error in form');
                return false;
            }
        });

        $('#reject_address').click(function () {
            var message = "You need to try again with more specific address details";
            $('#address_verification_message').html(message);
            $('#myModal .modal-footer').html('<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');
            $('#myModal').modal('show');
        });

        $(document).on('click', '#accept_address', function () {
            $('#myModal .modal-footer').html('<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');
              if ($('#store_recipients').attr('action').indexOf("#") == -1 ){
                    $('#store_recipients').attr('action', $('#store_recipients').attr('action')+ '#recipient');
                } 
            $('#store_recipients').submit();
        });

        $('.createLabel1, .save_labels_for_later').click(function () {
            var selected_addr = '';
            //  var valid_form = $('#creating_labels_form').parsley().validate();
            $('#creating_labels_form').parsley().destroy();
            var result_val = true;
            if ($(this).attr('id') == 'save_labels_for_later')
            {
                $('#saveLabelData').val('yes');
            } else
            {
                $('#saveLabelData').val('no');
                $('[data-parsley-group="valtab"]').each(function () {
                    $(this).parsley().validate({force: true});
                    if ($(this).parsley().isValid({force: true}) == false) {
                        result_val = false;
                    }
                });
                /*** if rush hour checked then validate it **/
                if ($('#rush_hour').is(":checked"))
                {
                    $("#rush_hour_charges").parsley().validate({force: true});
                    if ($("#rush_hour_charges").parsley().isValid({force: true}) == false) {
                        result_val = false;
                    }
                } 
            }

            if (result_val == true)
            {
                $('#pleaseWait').modal('show');
                $('#creating_labels_form').submit();
            } else
            {
                var failingFields = [],
                        parsleyForm = $('#creating_labels_form').parsley();

                for (var i = 0; i < parsleyForm.fields.length; i++)
                    if (true !== parsleyForm.fields[i].validationResult && parsleyForm.fields[i].validationResult.length)
                        failingFields.push(parsleyForm.fields[i].$element);

                console.log(failingFields);
            }
        });

        $('#choose_address').click(function (e) {
            e.preventDefault();
            var selected_addr = '';
            if ($('input:radio[name="select_addr"]:checked').length > 0) {
                if($("#rad_select_addr").is(':checked')){
                    var url ='';
                    if ($('#store_recipients').attr('action').indexOf("#") == -1 ){
                        url += '#recipient';
                    } 
                    $('#store_recipients').attr('action',$('#store_recipients').attr('action') + url );
                    $('#store_recipients').submit();
                }
                $('#verified_usps_address').val($('input:radio[name="select_addr"]:checked').attr('data-usps-addr'));
                selected_addr = $('input:radio[name="select_addr"]:checked').val();
                console.log(selected_addr);
                selected_addr = selected_addr.split('**');
                var city = selected_addr[1].charAt(0).toUpperCase() + selected_addr[1].toLocaleLowerCase().slice(1);
                var state = selected_addr[2].charAt(0).toUpperCase() + selected_addr[2].toLocaleLowerCase().slice(1);

                $('#recipient_address').val(selected_addr[0]);
                $('#recipient_zip').val(selected_addr[3]);
                $("#recipient_state option").each(function () {
                    if ($(this).text() == state) {
                        $(this).attr('selected', 'selected');
                    }
                });
                $("#recipient_state").selectpicker("refresh");
                var option = new Option(city, city,null, true, true);
                $("#recipient_city").append(option).trigger('change');

                /*$("#recipient_city option").each(function () {
                    if ($(this).text() == city) {
                        $(this).attr('selected', 'selected');
                    }
                });
                $("#recipient_city").selectpicker("refresh");*/

                $('#address_verification_modal').modal('hide');
            } else {
                var message = "Please select one address";
                $('#address_verification_message').html(message);
                $('#myModal').modal('show');
            }
        });
    })
    function checkForInputs(sel)
    {
      //  $('input[name="add_ons_' + cnt + '"[]]').prop('checked',false);
        var val = sel.getAttribute('value');
        var name = sel.getAttribute('name');
        var data_value = sel.getAttribute('data-value');
        var cnt = name.split('_');
        cnt = cnt[3];
        var id_name = "add_ons_" + cnt + "[]";
        $('input[name="'+id_name+'"]').prop('checked',false);
        switch (val)
        {    case 'manual':
                $('input[name="manual_inputs' + cnt + '"]').attr('placeholder', 'Enter manual label here');
                $('input[name="manual_inputs' + cnt + '"]').val(data_value);
                $('input[name="shipping_date' + cnt + '"]').removeAttr('required');
                $('#service_type_' + cnt).removeAttr('required');
                $('#service_type_div_' + cnt).hide();
                $('input[name="manual_amount' + cnt + '"]').show();
                $('input[name="manual_inputs' + cnt + '"]').show();
                $('input[name="shipping_date' + cnt + '"]').hide();
                $('#shipping_dt_alert'+cnt).hide();
                break;
            case 'firm mail':

            case 'next day delivery':
                $('input[name="manual_inputs' + cnt + '"]').attr('placeholder', 'Enter mailing label here');
                $('input[name="manual_inputs' + cnt + '"]').val( data_value);
                $('input[name="manual_inputs' + cnt + '"]').attr('required', true);
                $('input[name="shipping_date' + cnt + '"]').removeAttr('required');
                $('#service_type_' + cnt).removeAttr('required');
                $('#service_type_div_' + cnt).hide();
                $('input[name="manual_inputs' + cnt + '"]').show();
                $('input[name="shipping_date' + cnt + '"]').hide();
                $('input[name="manual_amount' + cnt + '"]').hide();
                $('#shipping_dt_alert'+cnt).hide();
                break;
            case 'Cert. USPS':
                $('input[name="manual_inputs' + cnt + '"]').attr('placeholder', 'Enter mailing label here');
                $('input[name="manual_inputs' + cnt + '"]').attr('value', data_value);
                $('input[name="manual_inputs' + cnt + '"]').attr('required', true);
                $('input[name="shipping_date' + cnt + '"]').removeAttr('required');
                $('#service_type_' + cnt).removeAttr('required');
                $('#service_type_div_' + cnt).hide();
                $('input[name="manual_inputs' + cnt + '"]').show();
                $('input[name="shipping_date' + cnt + '"]').hide();
                $('input[name="manual_amount' + cnt + '"]').hide();
                $('#shipping_dt_alert'+cnt).hide();
                break;
            case 'Cert. RR':
                $('input[name="manual_inputs' + cnt + '"]').attr('placeholder', 'Enter mailing label here');
                $('input[name="manual_inputs' + cnt + '"]').attr('value', data_value);
                $('input[name="manual_inputs' + cnt + '"]').attr('required', true);
                $('input[name="shipping_date' + cnt + '"]').removeAttr('required');
                $('#service_type_' + cnt).removeAttr('required');
                $('#service_type_div_' + cnt).hide();
                $('input[name="manual_inputs' + cnt + '"]').show();
                $('input[name="shipping_date' + cnt + '"]').hide();
                $('input[name="manual_amount' + cnt + '"]').hide();
                $('#shipping_dt_alert'+cnt).hide();
                break
            case 'Cert. ER':
                $('input[name="manual_inputs' + cnt + '"]').attr('placeholder', 'Enter mailing label here');
                $('input[name="manual_inputs' + cnt + '"]').attr('value', data_value);
                $('input[name="manual_inputs' + cnt + '"]').attr('required', true);
                $('input[name="shipping_date' + cnt + '"]').removeAttr('required');
                $('#service_type_' + cnt).removeAttr('required');
                $('#service_type_div_' + cnt).hide();
                $('input[name="manual_inputs' + cnt + '"]').show();
                $('input[name="shipping_date' + cnt + '"]').hide();
                $('input[name="manual_amount' + cnt + '"]').hide();
                $('#shipping_dt_alert'+cnt).hide();
                break;

            default:
                $('input[name="manual_inputs' + cnt + '"]').hide();
                $('input[name="manual_amount' + cnt + '"]').hide();
                $('input[name="manual_inputs' + cnt + '"]').removeAttr('required');
                $('input[name="manual_inputs' + cnt + '"]').attr('value', data_value);
                $('input[name="shipping_date' + cnt + '"]').attr('placeholder', 'Enter shipping date here');
                $('input[name="shipping_date' + cnt + '"]').attr('required', true);
                $('input[name="shipping_date' + cnt + '"]').show();
                $('#service_type_' + cnt).attr('required', true);
                $('#service_type_div_' + cnt).show();
                $('#shipping_dt_alert'+cnt).show();
        }
        $(".selectpicker").selectpicker('refresh');
    }

//stamps service types,package types and add ons related code
    function getPackageTypes(sel) {
        $('#filtered_add_ons_' + cnt).html('');
        var cnt = sel.getAttribute('id');
        cnt = cnt.split('_');
        cnt = cnt[2];
        var service_type = sel.options[sel.selectedIndex].value;
        $('#filtered_package_types_' + cnt).html('');
        if (service_type.length > 0) {
            $.ajax({
                type: 'POST',
                async: false,
                url: '{{url("get-package-types")}}',
                data: {service_type: service_type, counter: cnt},
                success: function (data) {
                    $('#filtered_package_types_' + cnt).html(data);
                    $(".selectpicker").selectpicker('refresh');
                },
                error: function (response)
                {
                    console.log('error getting package types for stamps');
                }
            });
        }
    }

    function getAddOns(sel) {
        var cnt = sel.getAttribute('id');
        cnt = cnt.split('_');
        cnt = cnt[2];
        console.log(cnt + " " + sel.selectedIndex);
        var package_type = sel.options[sel.selectedIndex].value;
        var service_type = $('#service_type_' + cnt).val();
        $('#filtered_add_ons_' + cnt).html('');
        console.log(service_type + " " + package_type);

        if (package_type.length > 0) {
            $.ajax({
                type: 'POST',
                async: false,
                url: '{{url("get-add-ons")}}',
                data: {service_type: service_type, package_type: package_type, counter: cnt},
                success: function (data) {
                    $('#filtered_add_ons_' + cnt).html(data);
                    $(".selectpicker").selectpicker('refresh')
                },
                error: function (response)
                {
                    console.log('error getting add ons for stamps');
                }
            });
        }
    }
    function checkForStamp(cnt){
        $('#stamp_out_field_'+cnt).prop( 'checked', true);
        $('input[name="manual_inputs' + cnt + '"]').hide();
        $('input[name="manual_inputs' + cnt + '"]').removeAttr('required');
        $('input[name="shipping_date' + cnt + '"]').attr('placeholder', 'Enter shipping date here');
        $('input[name="shipping_date' + cnt + '"]').attr('required', true);
        $('input[name="shipping_date' + cnt + '"]').show();
        $('#service_type_' + cnt).removeAttr('required');
        $('#service_type_div_' + cnt).hide();
    }
    function getProhibited(sel) {console.log(sel);
        var required = sel.getAttribute('data-required');
        var prohibited = JSON.parse(sel.getAttribute('data-prohibited'));
        var val = sel.getAttribute('id');
        var ele = sel.getAttribute('name');
        var cnt = val;
        cnt = cnt.split('_');
        cnt = cnt[3];

        //Check if any of the prohibited are selected, if yes inform and uncheck them
        $('input[name="' + ele + '"]').each(function (idx, value) {
            if ($(this).is(':checked') && $(this).val() != $('#' + val).val())
            {
                if (($(this).val() == 13 || $(this).val() == 21) && !$('input[name="' + ele + '"]:eq(0)').is(':checked'))
                {   //$(this).prop("checked",true);
                    $('input[name="' + ele + '"]:eq(0)').prop("checked", true);
                    var alert_id = 'alrt' + cnt;
                    $('#' + alert_id).html('<b>Return receipt requires the mail to be certified</b>');
                    setTimeout(function () {
                        $('#' + alert_id).html('');
                    }, 5000);
                }

                var i;
                for (i = 0; i < prohibited.length; i++) {
                    if (prohibited[i] == $(this).val())
                    {
                        $(this).prop("checked", false);
                    }
                }


            }
        });
    }
//stamps service types,package types and add ons related code end
//Verify address code end
//Parsley validation config
    $('form').parsley({
        successClass: "has-success",
        errorClass: "has-error",
        errors: {
            classHandler: function (elem) {
                return $(elem).closest('.form-group');
            },
            errorsWrapper: "<span class='help-block'></span>",
            errorTemplate: '<div class="alert alert-danger parsley" role="alert"></div>'
        },
        trigger: 'focusout change'
    });
    //check all inputs on page load to check selections and hide/show form elements accordingly
    $(document).ready(function () {
        var cnt;
        var no_of_elements = '{{  count($recipients) }}';
        for (cnt = 1; cnt <= no_of_elements; cnt++) {

            if ($('input[name="type_of_mailing_' + cnt + '"]:checked').val() == 'firm mail' || $('input[name="type_of_mailing_' + cnt + '"]:checked').val() == 'next day delivery')
            {
                $('input[name="manual_inputs' + cnt + '"]').attr('placeholder', 'Enter mailing label heres');
                // $('input[name="manual_inputs' + cnt + '"]').attr('required', true);
                // $('input[name="shipping_date' + cnt + '"]').removeAttr('required');
                // $('#service_type_' + cnt).removeAttr('required');
                $('#service_type_div_' + cnt).hide();
                $('input[name="manual_inputs' + cnt + '"]').show();
                 $('input[name="manual_amount' + cnt + '"]').hide();
                $('input[name="shipping_date' + cnt + '"]').hide();
            }else if ($('input[name="type_of_mailing_' + cnt + '"]:checked').val() == 'manual') {
               $('input[name="manual_inputs' + cnt + '"]').attr('placeholder', 'Enter mailing label here');
                // $('input[name="manual_inputs' + cnt + '"]').attr('required', true);
                // $('input[name="shipping_date' + cnt + '"]').removeAttr('required');
                // $('#service_type_' + cnt).removeAttr('required');
                $('#service_type_div_' + cnt).hide();
                $('input[name="manual_inputs' + cnt + '"]').show();
                $('input[name="manual_amount' + cnt + '"]').show();
                $('input[name="shipping_date' + cnt + '"]').hide();
            } else if ($('input[name="type_of_mailing_' + cnt + '"]:checked').val() == 'stamps') {
                $('input[name="manual_inputs' + cnt + '"]').hide();
                 $('input[name="manual_amount' + cnt + '"]').hide();
                // $('input[name="manual_inputs' + cnt + '"]').removeAttr('required');
                $('input[name="shipping_date' + cnt + '"]').attr('placeholder', 'Enter shipping date here');
                // $('input[name="shipping_date' + cnt + '"]').attr('required', true);
                $('input[name="shipping_date' + cnt + '"]').show();

                // $('#service_type_' + cnt).attr('required', true);
                $('#service_type_div_' + cnt).show();
            } else
            {

                $('input[name="manual_inputs' + cnt + '"]').hide();
                // $('input[name="manual_inputs' + cnt + '"]').removeAttr('required');
                $('input[name="shipping_date' + cnt + '"]').attr('placeholder', 'Enter shipping date here');
                // $('input[name="shipping_date' + cnt + '"]').attr('required', true);
                $('input[name="shipping_date' + cnt + '"]').hide();
                $('input[name="manual_inputs' + cnt + '"]').hide();
                 $('input[name="manual_amount' + cnt + '"]').hide();
                // $('#service_type_' + cnt).attr('required', true);
                $('#service_type_div_' + cnt).hide();
            }
        }

        /*generate PDF */

        $('.generate_pdf').click(function () {
            $('#creating_labels_form').parsley().destroy();
            $('#generate_pdf_flag').val('1');
            $('#saveLabelData').val('yes');
            var data_url = $(this).attr('data-url');
            $('#pleaseWait').modal('show');
            $.ajax({
                type: "POST",
                url: '{{url("account-manager/research/createLabel")}}',
                data: $('#creating_labels_form').serialize(),
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                    $('#generate_pdf_flag').val('0');
                    $('#saveLabelData').val('no');
                    $('#pleaseWait').modal('hide');
                    //document.location.href = "{{env('APP_URL')}}"+"/account-manager/research";
                    window.open(data_url, '_blank');
                }
            });


        });

    });
    function change_secording_pending_sign_status(status) {
        $('#creating_labels_form').parsley().destroy();
        $('#recording_pending_signature').val(status);
        $('#saveLabelData').val('yes');
        $('#pleaseWait').modal('show');
        $.ajax({
            type: "POST",
            url: '{{url("account-manager/research/createLabel")}}',
            data: $('#creating_labels_form').serialize(),
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                $('#recording_pending_signature').val(0);
                $('#saveLabelData').val('no');
                $('#pleaseWait').modal('hide');
                response = $.parseJSON(response);
                if (response.status == 'success') {
                    $('.display-ib li').removeClass('active');
                    $('#address_tab').addClass('active');
                    $('.notice-tabs .tab-pane').removeClass('active');
                    $('#verify_address').addClass('active');

                } else if (response.status == 'error') {

                    $('.display-ib li').removeClass('active');
                    $('#address_tab').addClass('active');
                    $('.notice-tabs .tab-pane').removeClass('active');
                    $('#verify_address').addClass('active');

                }

                $('#work_order_update_message').html(response.message);
                $('#successModal').modal('show');

            }
        });

    }
    var autocompletecityzip_src = "{{ url('customer/city/autocompletecityzip') }}";

/****************** Notes- Public-private email addresses ***********/
$('input[type=radio][name=visibility]').change(function() {
    var customer_id = $('#contact_customer_id').val();
    $('#note_email').val('');
    $.ajax({
                type: "POST",
                url: '{{url("account-manager/research/email-notes")}}',
                data: {visibility: this.value, customer_id: customer_id},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                    console.log(response[0][1]);
                    var options = '' ;
                    options +='<option value="">Select</option>';
                    for (var i = 0; i < response[0].length; i++) {
                     options +='<option val="'+response[0][i]+'">'+response[0][i]+'</option>';
                    }
                   $('#note_email').html(options);
                   $("#note_email").selectpicker("refresh");


                }
    });
});

$(window).on('load', function() {
    var self_url = window.location.href;
    if (self_url.indexOf("#")!=-1 ){
        var tab_name = self_url.split("#").pop();

        $("#" +tab_name + "_tab a ").trigger( "click" );
    }

});

</script>
<!-- <script type="text/javascript" src="{{ asset('js/autocomplete_zip.js') }}"></script> -->
@endsection

