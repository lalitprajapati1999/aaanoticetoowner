@if($workorder['status']==5)
<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">...</button>
<ul class="dropdown-menu">

    <!--  <li><a href="{{url('account-manager/research/restrict-work-order/'.$workorder['workorder_id'])}}" >Do Not Mail This Work Order</a></li> -->

    <li><a href="{{url('account-manager/research/send-back-to-proccesing/'.$workorder['workorder_id'])}}">Send Back To Proccessing</a></li>

    <li><a href="#" value="Add note"  data-target="#favoritesModal" 

           data-toggle="modal" onclick="notes(<?php echo $workorder['workorder_id']; ?>)">Add Note</a></li>
    <li><a href="" data-target="#AccountManagerModal" 

           data-toggle="modal" onclick="getAccountManager(<?php echo $workorder['workorder_id']; ?>)">Change Account Manager For  Work Order</a></li>

    <li><a href="{{url('account-manager/research/duplicate/'.$workorder['workorder_id'].'/'.$workorder['notice_id'])}}">Duplicate  Work Order</a></li>
    <li><a href="#">Rescind   work order</a></li>

</ul>
@elseif($workorder['status']==0)
<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">...</button>
<ul class="dropdown-menu">
    <li>
      <a href="#" value="Add note"  data-target="#favoritesModal" 
           data-toggle="modal" onclick="notes(<?php echo $workorder['workorder_id']; ?>)">Add Note</a></li>  
    </li>


</ul>
@elseif($workorder['status']==6)
<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">...</button>
<ul class="dropdown-menu">

    <li><a href="{{url('account-manager/research/duplicate/'.$workorder['workorder_id'].'/'.$workorder['notice_id'])}}">Duplicate  Work Order</a></li>

    <li><a href="#" value="Add note"  data-target="#favoritesModal" 

           data-toggle="modal" onclick="notes(<?php echo $workorder['workorder_id']; ?>)">Add Note</a></li>


    <li><a href="{{url('account-manager/research/printpdf/'.$workorder['workorder_id'])}}">View Completed Work Order</a></li>



</ul>
@elseif($workorder['status']==1)

<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">...</button>
<ul class="dropdown-menu">

    <li><a href="{{url('account-manager/research/restrict-work-order/'.$workorder['workorder_id'])}}" >Do Not Mail  Work Order</a></li>

    <li><a href="#" value="Add note"  data-target="#favoritesModal" 

           data-toggle="modal" onclick="notes(<?php echo $workorder['workorder_id']; ?>)">Add Note</a></li>
    <li><a href="" data-target="#AccountManagerModal" 

           data-toggle="modal" onclick="getAccountManager(<?php echo $workorder['workorder_id']; ?>)">Change Account Manager For  Work Order</a></li>

    <li><a href="{{url('account-manager/research/duplicate/'.$workorder['workorder_id'].'/'.$workorder['notice_id'])}}">Duplicate  Work Order</a></li>
    <li><a href="{{url('account-manager/research/proceed/'.$workorder['workorder_id'].'/'.$workorder['notice_id'])}}">Proceed with  work order</a></li>
</ul>


@elseif($workorder['status']==2)

<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">...</button>
<ul class="dropdown-menu">

    <li><a href="{{url('account-manager/research/restrict-work-order/'.$workorder['workorder_id'])}}" >Do Not Mail  Work Order</a></li>

    <li><a href="#" value="Add note"  data-target="#favoritesModal" 

           data-toggle="modal" onclick="notes(<?php echo $workorder['workorder_id']; ?>)">Add Note</a></li>
    <li><a href="" data-target="#AccountManagerModal" 

           data-toggle="modal" onclick="getAccountManager(<?php echo $workorder['workorder_id']; ?>)">Change Account Manager For  Work Order</a></li>

    <li><a href="{{url('account-manager/research/duplicate/'.$workorder['workorder_id'].'/'.$workorder['notice_id'])}}">Duplicate  Work Order</a></li>
    <li><a href="{{url('account-manager/research/edit/'.$workorder['workorder_id'].'/'.$workorder['notice_id'])}}">Continue working on  work order</a></li>
</ul>

@elseif($workorder['status']==8)
<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">...</button>
<ul class="dropdown-menu">

    <li><a href="#" value="Add note"  data-target="#favoritesModal" 

           data-toggle="modal" onclick="notes(<?php echo $workorder['workorder_id']; ?>)">Add Note</a></li>
    <li><a href="" data-target="#AccountManagerModal" 

           data-toggle="modal" onclick="getAccountManager(<?php echo $workorder['workorder_id']; ?>)">Change Account Manager For  Work Order</a></li>

    <li><a href="{{url('account-manager/research/duplicate/'.$workorder['workorder_id'].'/'.$workorder['notice_id'])}}">Duplicate  Work Order</a></li>
</ul>
@elseif($workorder['status']==7)
<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">...</button>
<ul class="dropdown-menu">

    <li><a href="#" value="Add note"  data-target="#favoritesModal" 

           data-toggle="modal" onclick="notes(<?php echo $workorder['workorder_id']; ?>)">Add Note</a></li>
    <li><a href="" data-target="#AccountManagerModal" 

           data-toggle="modal" onclick="getAccountManager(<?php echo $workorder['workorder_id']; ?>)">Change Account Manager For  Work Order</a></li>

    <li><a href="{{url('account-manager/research/duplicate/'.$workorder['workorder_id'].'/'.$workorder['notice_id'])}}">Duplicate  Work Order</a></li>
</ul>
@else
<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">...</button>
<ul class="dropdown-menu">

    <li><a href="{{url('account-manager/research/restrict-work-order/'.$workorder['workorder_id'])}}" >Do Not Mail Work Order</a></li>

    <li><a href="{{url('account-manager/research/send-back-to-proccesing/'.$workorder['workorder_id'])}}">Send Back To Proccessing</a></li>

    <li><a href="#" value="Add note"  data-target="#favoritesModal" 

           data-toggle="modal" onclick="notes(<?php echo $workorder['workorder_id']; ?>)">Add Note</a></li>
    <li><a href="" data-target="#AccountManagerModal" 

           data-toggle="modal" onclick="getAccountManager(<?php echo $workorder['workorder_id']; ?>)">Change Account Manager For  Work Order</a></li>

    <li><a href="{{url('account-manager/research/duplicate/'.$workorder['workorder_id'].'/'.$workorder['notice_id'])}}">Duplicate  Work Order</a></li>
    <li><a href="#">Rescind  work order</a></li>

    <li><a href="{{url('account-manager/research/proceed/'.$workorder['workorder_id'])}}">Proceed with  work order</a></li>
    <li><a href="{{url('account-manager/research/edit/'.$workorder['workorder_id'].'/'.$workorder['notice_id'])}}">Continue working on  work order</a></li>




</ul>
@endif










