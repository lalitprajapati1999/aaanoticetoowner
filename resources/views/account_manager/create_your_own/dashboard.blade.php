@extends('adminlte::page')
@section('content')
<div role="tabpanel" >
						    		<section class="create-own">
						    			<div class="dashboard-wrapper">
						    				<div class="dashboard-heading">
												<h1><span>Create Your Own</span></h1>
											</div>
											<div class="dashboard-inner-body">
												<div class="createown-wrapper">
													
													<div class="grid">
														<figure class="effect-sadie">
															<figcaption>

															     <a href="{{url('account-manager/create-your-own/work-order-history')}}" ><h2>CYO Research</h2></a>
                                                                <a href="{{url('account-manager/create-your-own/work-order-history')}}"><p>CYO Research</p></a>
															</figcaption>			
														</figure>
													</div>
													<div class="grid">
                                                        <figure class="effect-sadie">
                                                            <figcaption>
                                                                <a href="#" id="cyo_mailing_module" data-toggle="modal" data-target="#cyomailingModal"><h2>CYO Mailing</h2></a>
                                                                <a href="#" id="cyo_mailing_module" data-toggle="modal" data-target="#cyomailingModal"><p>CYO Mailing</p></a>
                                                            </figcaption>           
                                                        </figure>
                                                    </div>
										
												</div>
											</div>
						    			</div>
						    		</section>
						    	</div>
						    	@stop
@section('frontend_js')
@section('frontend_js')
<script type="text/javascript">
	/*function cyo_info(notice_type,notice_id){
		$('#notice_type').val(notice_type);
		$('#notice_id').val(notice_id);

	}*/
</script>
@stop
@stop