@extends('adminlte::page')
@section('content')
<div role="tabpanel" class="tab-pane active" id="workorder">
    <section class="work-order address-book-add-readonly">
        <div class="dashboard-wrapper">
            @if(!isset($is_rescind))
            @php
            $is_rescind = 0;
            @endphp
            @endif
            <div class="dashboard-heading">
                @if($is_rescind==0)
                <h1><span>CREATE YOUR OWN - {{strtoupper($notice['name'])}}</span></h1>
                @else
                <h1><span>CREATE YOUR OWN - {{strtoupper($notice['name'])}} amendment</span></h1>
                @endif
            </div>

            <div class="dashboard-inner-body">
                <div class="order-by">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="order-details full-width list-inline">
                                <li><p>Company Name:</p> <span>{{$customer_details->company_name}}</span></li>
                                <li><p>Contact Name:</p> <span>{{$customer_details->contact_person}}</span></li>
                                <li><p>Phone Number:</p> <span> {{$customer_details->office_number}}</span></li>
                            </ul>
                            <ul class="order-details full-width list-inline">
                                <li><p>Mailing Address:</p> <span>{{$customer_details->mailing_address}}</span></li>
                                @if($id) 
                                <li><p>Work Order #: </p> <span>{{$id}}</span></li>
                                @endif
                                @if(isset($parent_work_order)  && !empty($parent_work_order))
                                <li><p>Parent Work Order #: </p> <span>{{$parent_work_order}}</span></li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="notice-tabs">
                    <!-- Nav tabs -->
                    <div class="full-width text-right">
                        <ul class="nav nav-tabs display-ib pull-left" role="tablist">
                            @if($tab=="project")
                            <li class="active" id="project_tab" role="presentation" ><a href="#project" aria-controls="project" role="tab" data-toggle="tab">Project</a></li>
                            <li role="presentation" id="recipient_tab"><a href="#recepients" aria-controls="recepients" role="tab" data-toggle="tab" >Recipients</a></li>

                            @else 
                            <li role="presentation"><a href="#project" aria-controls="project" role="tab" data-toggle="tab">Project</a></li>
                            <li role="presentation" class="active" id="recipient_tab"><a href="#recepients" aria-controls="recepients" role="tab" data-toggle="tab" >Recipients</a></li>

                            @endif

                        </ul> 
                        <a href="{{URL::previous()}}" class="btn btn-primary custom-btn customb-btn"><span>Back</span></a> 
                        <a href="{{url('account-manager/create-your-own/view/printworkorder/'.$id)}}" target="_blank"name="printWO" class="btn btn-primary custom-btn form-btn" VALUE="Save For Later"><span>Print</span></a>

                    </div>

                    <div class="correction-div">
                        <div class="display-ib pull-right">
                            <a href="#" class="btn btn-default corr-btn">Corrections</a>
                            <a href="#" class="btn btn-default notice-btn">Notes</a>
                            <div class="corr-block notice-block mCustomScrollbar" data-mcs-theme="light">
                                <div class="corr-block-wrapper" >
                                    <h4>Corrections</h4>
                                    <form id="edit_work_order_correction">
                                        <div class="correction-success alert alert-success" style="display: none"></div>
                                        <div class="correction-error alert alert-danger" style="display:none"></div>
                                        <div class="row">
                                            <input type="text" name="work_order" value="{{$id}}" id="work_order" hidden="">
                                            <div class="col-md-12 col-sm-12">
                                                <div class="input-wrapper full-width">
                                                    <div class="styled-input">
                                                        <input class="form-control"  type="text" value="{{old('correction')}}" name="correction" data-parlsey-required="true" data-parsley-required-message="Correction is required">
                                                        <label>Correction<span class="mandatory-field">*</span></label>
                                                        <span></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12">
                                                <div class="select-opt">
                                                    <label class="col-md-12 control-label" for="textinput"  name="Email*"></label> 
                                                    <select class="selectpicker" name="email" id="correction_email">
                                                        <option value="">Select</option>
                                                        @foreach($note_emails as $email)
                                                        <option value="{{$email}}" @if (old('email') == $email) selected="selected" @endif > {{$email}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div id="correctionloader" style="display: none">
                                                <img width="150px" src="{{asset('images/loader.gif')}}"/>
                                            </div>
                                            <div class="form-bottom-btn">
                                                <a href="#" class="btn btn-primary custom-btn customc-btn submitCorrection"><span>Submit</span></a>
                                            </div>
                                        </div>
                                    </form>
                                    <ul class="correction_div">
                                        @if(isset($corrections) && !empty($corrections))
                                        @foreach($corrections AS $each_correction)
                                        <li>
                                            {{$each_correction->correction}}<br>
                                            Added by:{{$each_correction->name}}<br>
                                           Created at:{{date('m-d-Y',strtotime($each_correction->created_at))}}
                                        </li>
                                        @endforeach
                                        @endif
                                    </ul>
                                    <a class="closebtn">&times;</a>
                                </div>
                            </div>
                            <div class="notes-block notice-block mCustomScrollbar" data-mcs-theme="light">
                                <div class="notes-block-wrapper">
                                    <form id="edit_work_order_note">
                                        <div class="note-success alert alert-success" style="display: none"></div>
                                        <div class="note-error alert alert-danger" style="display:none"></div>
                                        <h4>Notes</h4>
                                        <p>Order Number: {{$id}}</p>
                                        <div class="row">
                                            <div class="col-md-12">
                                                 <ul class="button-holder full-width list-inline">
                                                    <li class=""><input type="radio" id="radio-2-4" name="visibility" class="regular-radio active" value="0"/><label for="radio-2-4">Public</label></li>
                                                    <li class=""><input type="radio" id="radio-2-5" name="visibility" class="regular-radio" value="1"/><label for="radio-2-5">Private</label></li>
                                                </ul>
                                            </div>
                                            <input type="text" name="work_order" value="{{$id}}" id="work_order" hidden="">
                                            <div class="col-md-12 col-sm-12">
                                                <div class="input-wrapper full-width">
                                                    <div class="styled-input">
                                                        <input class="form-control"  type="text" value="{{old('note')}}" name="note" data-parlsey-required="true" data-parsley-required-message="Note is required">
                                                        <label>Note<span class="mandatory-field">*</span></label>
                                                        <span></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12">
                                                <div class="select-opt">
                                                    <label class="col-md-12 control-label" for="textinput"  name="Email*"></label> 
                                                    <select class="selectpicker" name="email" id="note_email" >
                                                        <option value="">Select</option>
                                                        @foreach($note_emails as $email)
                                                        <option value="{{$email}}" @if (old('email') == $email) selected="selected" @endif > {{$email}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div id="noteloader" style="display: none">
                                                <img width="150px" src="{{asset('images/loader.gif')}}"/>
                                            </div>
                                            <div class="form-bottom-btn">
                                                <a href="#" class="btn btn-primary custom-btn customc-btn submitNote"><span>Submit</span></a>
                                            </div>
                                        </div>
                                    </form>
                                    <ul class="notes_div">
                                        @if(isset($notes) && !empty($notes))
                                        @foreach($notes AS $each_note)
                                        <li>
                                            {{$each_note->note}}<br>
                                            Added by:{{$each_note->name}}<br>
                                            Created at:{{date('m-d-Y',strtotime($each_note->created_at))}}
                                        </li>
                                        @endforeach
                                        @endif
                                    </ul>
                                    <a class="closebtn1">&times;</a>
                                </div>
                            </div>
                        </div>
                        <!--                                <div id="loaderDiv">
                                                        <img id="loading-image" src="{{ asset('loader.gif')}}" style="display:none;"/>
                                                    </div>-->
                    </div>
                    <div class="tab-content">
                        @if($tab=="project")
                        <div role="tabpanel" class="tab-pane active" id="project">
                            @elseif($tab=='recipients')
                            <div role="tabpanel" class="tab-pane" id="project">
                                @endif
                                <div class="project-wrapper">
                                    <div class="dash-subform">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="sub-section-title">
                                                    <h2>Project Information</h2>
                                                </div>
                                            </div>
                                            <form method="post" action="{{url('account-manager/create-your-own/view/uploadreceipt')}}">
                                                {!! csrf_field() !!}
                                                <input type="text" name="work_order_id" value="{{$id}}"  hidden="">
                                                <input type="text" name="notice_id" value="{{$notice['id']}}" hidden="">
                                                @if($notice_fields_section1)
                                                @foreach($notice_fields_section1 as $fields)
                                                @if($fields->type==1 && $fields->name == 'parent_work_order' && $fields->value!="" ) 
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="readonly full-width">

                                                            <?php $label = str_replace("_", " ", $fields->name); ?>
                                                            <label class="full-width">{{ucfirst(trans($label))}}</label>
                                                            <p class="full-width">#{{$fields->value}}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                @else
                                                <div class="col-md-6 col-sm-6">
                                                    <div class="form-group">
                                                        <div class="readonly full-width">

                                                            <?php $label = str_replace("_", " ", $fields->name); ?>
                                                            <label class="full-width">{{ucfirst(trans($label))}}</label>
                                                            <p class="full-width"><?php echo html_entity_decode($fields->value) ?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endif
                                                @endforeach
                                                @endif

                                        </div>
                                    </div>
                                    <div class="dash-subform">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="sub-section-title">
                                                    <h2>Service/Labor Furnished</h2>
                                                </div>	
                                            </div>
                                            @if($notice_fields_section2)
                                            @foreach($notice_fields_section2 as $fields)
                                            <div class="col-md-6 col-sm-6">
                                                <div class="form-group">
                                                    <div class="readonly full-width">

                                                        <?php $label = str_replace("_", " ", $fields->name); ?>
                                                        <label class="full-width">{{ucfirst(trans($label))}}</label>
                                                        <p class="full-width"><?php echo html_entity_decode($fields->value) ?></p>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                            @endif
                                            <div class="col-md-12 col-sm-12">
                                                <div class="row">
                                                    <?php
                                                    $document_types = getDocumentTypes();
                                                    ?>
                                                    
                                                    <div class="col-lg-6 col-md-6 col-sm-12"  >
                                                        <div class="select-opt mt-top">
                                                            <label class="col-md-12 control-label" for="textinput">Document Type</label>  
                                                            <select class="selectpicker" name="type" id="edit_document_type">
                                                                <option value="">-Select-</option>
                                                                @foreach ($document_types as $key => $value) 
                                                                <option value="{{$key}}">{{$value}}</option>
                                                                @endforeach
                                                            </select>
                                                            @if ($errors->has('type'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('type') }}</strong>
                                                            </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                               <!--      <div class="col-md-12 doc_visibility">
                                                        <ul class="nav nav-tabs radio-tabs button-holder full-width public-tabs" role="tablist">
                                                            <li class="active col-md-3"><a href="#hard-doc"><input type="radio" id="radio-1-4" name="visibility" class="regular-radio document_visibility" value="0" checked /><label for="radio-1-4">Public</label></a></li>
                                                            <li class="col-md-3"><a href="#soft-doc"><input type="radio" id="radio-1-5" name="visibility" class="regular-radio document_visibility" value="1"/><label for="radio-1-5">Private</label></a></li>
                                                        </ul>
                                                    </div> -->
                                                    
                                                    <div class="col-md-12 col-sm-12 doc_field">
                                                        <div class="row">
                                                            <div class="col-md-12 col-sm-12">
                                                                <div class="input-wrapper full-width">
                                                                    <div class="styled-input">
                                                                        <input class="form-control" name="doc_input"  type="text" id="edit_doc_input_id">
                                                                        <label id="doc_field"></label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                        <input type="hidden" name="edit_document_name" id="edit_document_name">
                                                        <input type="hidden" name="edit_doc_id" id="edit_doc_id">
                                                        <input type="hidden" id="row_id">
                                                    <div class="col-md-12 col-sm-12 col-xs-12">

                                                        <div class="form-btn-div doc_file">
                                                            <span class="btn btn-default btn-file upload-btn btn btn-primary form-btn" id="temp">
                                                                Choose File to Upload  <input type="file" name="file_name" id="edit_result_file">
                                                            </span>
                                                            (Allow Types:csv,xls,xlsx,pdf,doc,docx, jpg, jpeg)
                                                            <span id="errormessage" style="display: none"></span>
                                                        </div>
                                                    </div>
                                                         <div class="form-btn-div doc_file col-md-12 col-sm-12 col-xs-12" >
                                                            <div style="float: left;width: 10">
                                                                <a href="javascript:;" onClick="openTab(this)" id="open_doc" name=""><span  id="preview" style="display: none;" ></span></a></div>
                                                            <div style="float: left ;width: 10;padding-left: 20px">
                                                                <a  href="javascript:;" ><span id="remove_icon" style="display: none;"  >Delete</span></a></div>

                                                            <input type="hidden" name="remove_file" id="remove_file" value="0">
                                                        </div>
                                                    <div class="col-md-12 col-sm-12 col-xs-12 doc_file">

                                                        <div class="add-new-btn inner-dash-btn full-width ">
                                                            <button  name="submit_doc" type="button" value="Add" id="submit_document" class="btn btn-primary custom-btn">
                                                                <span class="update_doc">Add</span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 col-sm-12">
                                                        <div class="row">
                                                            <div class="col-lg-12 col-md-12 col-sm-12">
                                                                <div class="dahboard-table doc-table full-width table-responsive">
                                                                    <table class="table table-striped" id="edit_document_table">
                                                                        <thead class="thead-dark">
                                                                            <tr>
                                                                                <th scope="col">Document Type</th>
                                                                                <th scope="col">Document Description</th>
                                                                                <th scope="col">Name</th>
                                                                                <th scope="col">Date & Time</th>
                                                                                <th scope="col">Type of Attachment</th>
                                                                               
                                                                                <th scope="col">Options</th>
                                                                               
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            @if(isset($attachment) && !empty($attachment) && count($attachment)>0)
                                                                            @foreach($attachment AS $each_attachment)
                                                                            <?php $file_name = explode('.', $each_attachment->file_name) ?>
                                                                            <tr id="refreshdelete{{$each_attachment->id}}">
                                                                                <td>
                                                                                    @foreach ($document_types as $key => $value) 
                                                                                    @if ($each_attachment->type == $key) 
                                                                                    {{$value}}
                                                                                    @endif
                                                                                    @endforeach
                                                                                </td>
                                                                                <td>{{$each_attachment->title}}</td>
                                                                                <td>{{$each_attachment->original_file_name}}</td>
                                                                                <td>{{date('d M y h:i A',strtotime($each_attachment->created_at))}}</td>
                                                                                <td>
                                                                                    <?php
                                                                                    $extension = explode('.', $each_attachment->original_file_name);
                                                                                    echo $ext=$each_attachment->original_file_name != "" ? $extension[1] : "";
                                                                                    ?>

                                                                                </td>

                                                                              
                                                                                <td>
                                                                                       <a class='btn btn-secondary item red-tooltip remove-attachment' data-toggle='modal' 
                                                                                           data-url='{{url('customer/edit/cyo-work-ordwer/removeattachment')}}'  data-id="{{$each_attachment->id}}" data-file='{{$each_attachment->file_name}}' 
                                                                                           data-target='#attachment-confirmation-modal' data-placement='top' title='Remove'>
                                                                                            <span class='icon-cross-remove-sign'></span></a>
                                                                                        <a class='btn btn-secondary item red-tooltip edit-attachment'
                                                                                                   data-id="{{$each_attachment->id}}" data-file='{{$each_attachment->file_name}}' data-type="{{$each_attachment->type}}" data-title="{{$each_attachment->title}}" data-file-original-name="{{$each_attachment->original_file_name}}" data-file-extension="{{$ext}}">
                                                                                                <span class='icon-pencil-edit-button'></span>
                                                                                             
                                                                                         @if(!empty($each_attachment->file_name))
                                                                                            <a href="{{asset('attachment/cyo_work_order_document/'.$each_attachment->file_name)}}" target="_blank" class="btn btn-secondary item red-tooltip"><i class="fa fa-eye" style=""></i></a>
                                                                                            @endif  
                                                                                </td>
                                                                               

                                                                            </tr>
                                                                            @endforeach
                                                                            @else
                                                                        <td class="text-center norecords" colspan="6">No record found</td>
                                                                        @endif
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-bottom-btn">
                                                <a href="{{url('account-manager/create-your-own/work-order-history')}}" class="btn btn-primary custom-btn customb-btn"><span>Back</span></a>
                                                <input type="hidden" name="attachments[]" id="upload_attachments"/>
                                                <input type="hidden" name="remove_doc[]" id="remove_doc"/>

                                                <button type="submit" name="continue" class="btn btn-primary custom-btn customc-btn" VALUE="Continue"><span>Continue</span></button>

                                            </div>
                                        </div>
                                    </div>
                                </div></form>
                            </div>
                            @if($tab=="project")
                            <div role="tabpanel" class="tab-pane" id="recepients">
                                @elseif($tab=='recipients')
                                <div role="tabpanel" class="tab-pane active" id="recepients">
                                    @endif
                                    <div class="project-wrapper">
                                        <form>
                                            {!! csrf_field() !!}   
                                            <div class="dash-subform mt-0">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="sub-section-title">
                                                                    <h2>Recipients</h2>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="dahboard-table table-responsive">
                                                                    <table class="table table-striped">
                                                                        <thead class="thead-dark">
                                                                            <tr>
                                                                                <th scope="col">Category</th>
                                                                                <th scope="col">Recipients</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>

                                                                            @if(isset($count) && count($count)>0)

                                                                            @foreach($count as $key => $count)
                                                                            <tr>

                                                                                <td class="checkbox-holder">
                                                                                    <input type="checkbox" id="{{$key}}" class="regular-checkbox"><label for="{{$key}}">{{$count->name}}</label>
                                                                                </td>
                                                                                <td>{{$count->total}}</td>
                                                                            </tr>
                                                                            @endforeach
                                                                            @else
                                                                        <td class="text-center" colspan="4">{{'No records found'}}</td>
                                                                        @endif

                                                                        </tbody>
                                                                    </table>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="sub-section-title">
                                                                    <h2>Selected Recipients</h2>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="dahboard-table table-responsive">
                                                                    <table class="table table-striped">
                                                                        <thead class="thead-dark">
                                                                            <tr>
                                                                                <th scope="col">Role</th>
                                                                                <th scope="col">Company Name</th>
        <!--                                                    <th scope="col">Contact Person</th>-->
        <!--                                                    <th scope="col">Phone Number</th>-->
                                                                                <th scope="col">Address</th>
                                                                                <th scope="col">City</th>
                                                                                <th scope="col">State</th>
                                                                                <th scope="col">Zipcode</th>

                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>


                                                                            @if(isset($recipients) && count($recipients) > 0)

                                                                            @foreach($recipients  as $recipient)
                                                                            <tr>
                                                                                <td class="cl-blue">{{$recipient->category->name}}</td>
                                                                                <td>{{$recipient->name}}</td>
    <!--                                                                            <td>{{$recipient->contact}}</td>-->
    <!--                                                                            <td>{{$recipient->mobile}}</td>-->
                                                                                <td>
                                                                                    {{$recipient->address}}
                                                                                    
                                                                                </td>
                                                                                <td>{{$recipient->city->name}}</td>
                                                                                <td>{{$recipient->state->name}}</td>
                                                                                <td>{{$recipient->zip}}</td>

                                                                            </tr>
                                                                            @endforeach

                                                                            @else
                                                                        <td class="text-center" colspan="4">{{'No records found'}}</td>
                                                                        @endif




                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="row">
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <div class="form-bottom-btn">
                                                        <input type="button" id="editBack" name="editBack" class="btn btn-primary custom-btn customb-btn" value="Back"/>

                                                        <a  href="{{url('account-manager/create-your-own/work-order-history')}}"  class="btn btn-primary custom-btn customc-btn" name="submit" value="submit"  ><span>Work Order History <i class="fa fa-chevron-circle-right" style="font-size:24px"></i></span></a>
 <!--  <a  href="{{url('customer/work-order/printpdf/'.$id)}}" name="submit" class="btn btn-primary custom-btn customc-btn" value="submit" target="_blank"><span>Submit</span></a> -->
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>




                            </div>
                            <!-- add new contacts-->


                        </div>
                    </div>
                </div>
                </section>
            </div>

            <form action="" method="POST" class="remove-attachment-model" id="remove-attachment-model">
                {!! csrf_field() !!}
                <div id="attachment-confirmation-modal" class="modal fade">
                    <div class="modal-dialog modal-confirm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <div class="icon-box">
                                    <i class="fa fa-close"></i>
                                </div>				
                                <h4 class="modal-title">Are you sure?</h4>	
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body">
                                <p>Do you really want to delete these records? This process cannot be undone.</p>
                            </div>
                            <div class="modal-footer">
                                <div class="form-bottom-btn">
                                    <button type="button" class="btn btn-primary custom-btn customb-btn remove-attachement-from-delete-form" data-dismiss="modal"><span>Cancel</span></button>
                                    <button type="submit" class="btn btn-primary custom-btn customc-btn"><span>Confirm</span></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>


            @stop
            @section('frontend_js')
            <script type="text/javascript" src="{{ asset('js/jquery-ui.min.js') }}"></script>

            <script src="{{ asset('vendor/adminlte/plugins/datepicker/bootstrap-datepicker.js') }}" type="text/javascript"></script>
            <script type="text/javascript">
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
//back to project tab
$('#editBack').click(function () {
    $('.display-ib li').removeClass('active');
    $('#project_tab').addClass('active');
    $('.notice-tabs .tab-pane').removeClass('active');
    $('#project').addClass('active');
})
$('.notice-btn').click(function (e) {

    $('input[name=note]').val('');
    $('input[name=email]').val('');
    $('.note-success').empty();
    $('.note-success').css('display', 'none');
    $('.note-error').empty();
    $('.note-error').css('display', 'none');
});
$(".submitNote").click(function (e) {
    e.preventDefault();
    var note = $("input[name=note]").val();
    var email = $("#note_email").val();
    var secondary_doc_id = $('#work_order').val();
    $.ajax({
        type: 'POST',
        url: '{{url("customer/create-your-own/store_note")}}',
        //data: {note: note, email: email, work_order: secondary_doc_id},
        data: $('#edit_work_order_note').serialize(),

        beforeSend: function () {
            $('#pleaseWait').modal('show');
        },
        complete: function () {
            $('#pleaseWait').modal('hide');
        },
        success: function (data) {
            var d = new Date();
            var strDate = (d.getMonth() + 1) + "-" + d.getDate() + "-" + d.getFullYear();
            if (data.success) {
                $('.note-error').empty();
                $('.note-success').empty();
                $("input[name=note]").val('');
                $("#note_email option").prop("selected", false);
                $('#note_email').selectpicker('refresh');
                $('.note-error').css('display', 'none');
                $('.note-success').show();
                $('.note-success').append('<p>' + data.success + '</p>');
                // $('.notes_div').append('<li>' + data.notes.note + '<br>' + data.notes.email + '</li>');
                $('.notes_div').prepend('<li>' + data.notes.note +  '</br>Added by:'+ data.notes.name + '</br>Created at:' + strDate +'</li>');

            } else if (data.errors) {
                $('.note-error').empty();
                $.each(data.errors, function (key, value) {
                    $('.note-success').empty();
                    $('.note-success').css('display', 'none');
                    $('.note-error').show();
                    $('.note-error').append('<p>' + value + '</p>');
                });
            }
        }
    });
});
$(".submitCorrection").click(function (e) {
    e.preventDefault();
    $('.correction-error').hide();
    $('.correction-success').hide();
    $.ajax({
        type: 'POST',
        url: '{{url("customer/create-your-own/store_correction")}}',
        data: $('#edit_work_order_correction').serialize(),
        beforeSend: function () {
            $('#pleaseWait').modal('show');
        },
        complete: function () {
            $('#pleaseWait').modal('hide');
        },
        success: function (data) {
            var d = new Date();
            var strDate = (d.getMonth() + 1) + "-" + d.getDate() + "-" + d.getFullYear();
            if (data.success) {
                $('.correction-error').empty();
                $('.correction-success').empty();
                $("input[name=correction]").val('');
                $("#correction_email option").prop("selected", false);
                $('#correction_email').selectpicker('refresh');
                $('.correction-error').css('display', 'none');
                $('.correction-success').show();

                $('.correction-success').append('<p>' + data.success + '</p>');
//                $('.correction_div').append('<li>' + data.corrections.correction + '<br>' + data.corrections.email + '</li>');
                $('.correction_div').prepend('<li>' + data.corrections.correction +  '</br>Added by:'+ data.corrections.name + '</br>Created at:' + strDate +'</li>');

            } else if (data.errors) {
                $('.correction-error').empty();
                $.each(data.errors, function (key, value) {
                    $('. correction-success').empty();
                    $('.correction-success').css('display', 'none');
                    $('.correction-error').show();
                    $('.correction-error').append('<p>' + value + '</p>');
                });
            }
        }
    });
});
//End to submit note
$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip({
        trigger: 'click',
        placement: 'bottom'
    });
    $('textarea').each(function () {
        if ($(this).val().length > 0) {
            $(this).addClass('not-empty');
        } else {
            $(this).removeClass('not-empty');
        }
    });
    $('textarea').keyup(function () {
        if ($(this).val().length > 0) {
            $(this).addClass('not-empty');
        } else {
            $(this).removeClass('not-empty');
        }
    });

    $('#showattorneysfee').hide();
    //collection_for_attorneys_fees
    $('#attorneys_fees').click(function () {

        $('#showattorneysfee').toggle();
    });//end
    $('.doc_field').hide();
    $('.doc_file').hide();
    $('#edit_document_type').change(function () {

        var selected_doc_type = $(this).val();
        $('#edit_doc_input_id').val("");
        if (selected_doc_type == 1) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('#doc_field').text('Bond Number');
        } else if (selected_doc_type == 2) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('#doc_field').text('NOC Book/Page');
        } else if (selected_doc_type == 3) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('#doc_field').text('Permit No');
        } else if (selected_doc_type == 5) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('#doc_field').text('Invoice No.');
        } else if (selected_doc_type == 4 || selected_doc_type == 6 ||
                selected_doc_type == 7 || selected_doc_type == 8
                || selected_doc_type == 9 || selected_doc_type == 13) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('#doc_field').text('Description');
        } else if (selected_doc_type == 10) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('#doc_field').text('Folio No.');
        }else if (selected_doc_type == 11) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('#doc_field').text('COL Signed.');
        }else if (selected_doc_type == 12) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('#doc_field').text('Recorded COL/SCOL.');
        }else if (!selected_doc_type) {
            $('.doc_field').hide();
            $('.doc_file').hide();
        }

    });

$(document).on('click', '.edit-attachment', function () {
        var row_id = $(this).closest('tr').attr('id');
        // $('#submit_document').removeClass('old_class').addClass('new_class');
        var type = $(this).attr('data-type');
        var file = $(this).attr('data-file');
        $('#edit_document_name').attr('data-file-original-name',$(this).attr('data-file-original-name'));
        $('#edit_document_name').attr('data-file-extension',$(this).attr('data-file-extension'));
        var title = $(this).attr('data-title');
        var id = $(this).attr('data-id');
        $('#edit_document_name').val(file);
        $('#edit_doc_input_id').val(title);
        if (title != ''){$('#edit_doc_input_id').addClass('not-empty');}
        $('#edit_doc_id').val(id);
        $('#row_id').val(row_id);
        $('#preview').css('display','block');
        $('#remove_icon').css('display','block');

        //console.log(file);
        var APP_URL = {!! json_encode(url('/')) !!}
            // console.log(APP_URL);
            $("#open_doc").attr('name',APP_URL+'/attachment/work_order_document/'+file);
        doc_icon = file.split('.')[1];
        if(doc_icon == 'pdf'){
            $('#preview').html('<i class="fa fa-file-pdf-o" style="font-size:36px"></i>');
        }else if(doc_icon == 'doc' || doc_icon == 'docx'){
            $('#preview').html('<i class="fa fa-file-word-o" style="font-size:36px"></i>');
        }else if(doc_icon == 'xls' || doc_icon == 'xlsx'){
            $('#preview').html('<i class="fa fa-file-excel-o" style="font-size:36px"></i>');
        }else if(doc_icon == 'jpg' || doc_icon == 'jpeg'){
            $('#preview').html('<i class="fa fa-file-image-o" style="font-size:36px"></i>');
        }else if (doc_icon == 'csv'){
            $('#preview').html('<i class="fa fa-file" style="font-size:36px"></i>');
        }else{
            $('#preview').html('');
            $('#preview').css('display','none');
            $('#remove_icon').css('display','none');
        }

        //$('#edit_result_file').val(APP_URL+'/attachment/work_order_document/'+file);
        var id1 = file.split('.')[0];

        var id = $(this).attr('data-id');
        $('.doc_field').show();
        $('.doc_file').show();
        $('.doc_visibility').show();
        $('.update_doc').text('Update');
        $('#edit_document_type').val(type);
        $('#edit_document_type').selectpicker('refresh');
        /* $('#errormessage').text(file);
         $('#errormessage').css('display', 'block');*/
        var selected_doc_type = type;
        //$('#edit_doc_input_id').val("");
        if (selected_doc_type == 1) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('.doc_visibility').show();
            $('#doc_field').text('Bond Number');
        } else if (selected_doc_type == 2) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('.doc_visibility').show();
            $('#doc_field').text('NOC Book/Page');
        } else if (selected_doc_type == 3) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('.doc_visibility').show();
            $('#doc_field').text('Permit No');
        } else if (selected_doc_type == 5) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('.doc_visibility').show();
            $('#doc_field').text('Invoice No.');
        } else if (selected_doc_type == 4 || selected_doc_type == 6 ||
            selected_doc_type == 7 || selected_doc_type == 8 ||
            selected_doc_type == 9) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('.doc_visibility').show();
            $('#doc_field').text('Description');
        }else if (selected_doc_type == 10) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('.doc_visibility').show();
            $('#doc_field').text('Folio No.');
        }else if (selected_doc_type == 11) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('.doc_visibility').show();
            $('#doc_field').text('COL Signed.');
        }else if (selected_doc_type == 12) {
            $('.doc_field').show();
            $('.doc_file').show();
            $('.doc_visibility').show();
            $('#doc_field').text('Recorded COL/SCOL.');
        }else if (!selected_doc_type) {
            $('.doc_field').hide();
            $('.doc_file').hide();
            $('.doc_visibility').hide();
        }
    });

    var result_arr = [];
    $("#submit_document").on("click", function (e) {
    if($('.update_doc').html()=='Add')  {
        e.preventDefault();
        var extension = $('#edit_result_file').val().split('.').pop().toLowerCase();

        if ($('#edit_result_file').val() != "" && $.inArray(extension, ['csv', 'xls', 'xlsx', 'pdf', 'doc', 'docx', 'jpg', 'jpeg']) == -1) {
            $('#errormessage').html('Please Select Valid File... ');
            $('#errormessage').css('display', 'block');
        } else {
            var file_data = $('#edit_result_file').prop('files')[0];
            var selected_document_type = $('#edit_document_type').val();
            var doc_input = $('#edit_doc_input_id').val();

            var form_data = new FormData();

            form_data.append('file', file_data);
            form_data.append('document_type', selected_document_type);
            form_data.append('doc_value', doc_input)
            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': $('meta[name=_token]').attr('content')
                }
            });
            $.ajax({
                url: "{{url('customer/create-your-own/attachments')}}", // point to server-side PHP script
                data: form_data,
                type: 'POST',
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                processData: false,
                success: function (response) {
                    var data = jQuery.parseJSON(response);
                    var ran_no = Math.random() * (100 - 1) + 1;
                    ran_no = String(ran_no).replace('.','_');
                    data.db_arr.push('refreshdelete'+ran_no);
                    result_arr.push(data.db_arr);
                    result_arr.push('|');

                    $('#upload_attachments').val(result_arr);
                    var f1 = data.db_arr[2];
                    var f2 = f1.split('.')[0];
                    var row = '<tr id="refreshdelete' + ran_no + '">';
                    $.each((data.ajax_arr), function (i, d) {
                        $('.norecords').closest('tr').remove();
                        var display_url = '{{asset("attachment/cyo_work_order_document")}}/' + data.db_arr[2];
                        row += '<td>' + d + '</td>';
                         $('.doc_field').hide();
                            $('.doc_file').hide();
                            $('.doc_visibility').hide();
                            $('#edit_document_type').val('');
                            $('#edit_document_type').selectpicker('refresh');
                            $('#edit_result_file').val('');
                            $('#edit_document_name').val('');
                            $('#edit_doc_input_id').val('');
                            $('#remove_file').val('0');
                            $('#preview').css('display','none');
                            $('#remove_icon').css('display','none');
                    });
                    var display_url = '{{asset("attachment/cyo_work_order_document")}}/' + data.db_arr[2];
                    var attachment_url = '{{url("customer/create-your-own/removeattachment")}}';

                     row += "<td><a class='btn btn-secondary item red-tooltip remove-attachment' data-toggle='modal' data-url='" + attachment_url + "'  data-file='" + data.db_arr[2] + "' data-target='#attachment-confirmation-modal' data-placement='bottom' title='Remove'><span class='icon-cross-remove-sign'></span></a><a class='btn btn-secondary item red-tooltip edit-attachment' data-file='" + data.db_arr[2] + "' data-type='" + data.db_arr[0] + "'data-title='" + data.db_arr[1] +"'data-file-original-name='" + data.db_arr[3] + "'data-file-extension='" + data.ajax_arr[4]+"'><span class='icon-pencil-edit-button'></span></a>";
                    if(data.db_arr[2].length>0){
                                    row += "<a target='_blank' href='" + display_url + "' class='btn btn-secondary item red-tooltip'><i class='fa fa-eye'></i></a>";
                                    }
                                    row += '</td></tr>';
                    $('#edit_document_table tbody').append(row);
                }
            });
        }
    }else{
        e.preventDefault();
        var extension = $('#edit_result_file').val().split('.').pop().toLowerCase();

        if ($('#edit_result_file').val() != "" && $.inArray(extension, ['csv', 'xls', 'xlsx', 'pdf', 'doc', 'docx', 'jpg', 'jpeg']) == -1) {
            $('#errormessage').html('Please Select Valid File... ');
            $('#errormessage').css('display', 'block');
        } else {
            var file_data = $('#edit_result_file').prop('files')[0];
            var selected_document_type = $('#edit_document_type').val();
            var doc_input = $('#edit_doc_input_id').val();
            var doc_id = $('#edit_doc_id').val();
            var doc_visibile = $("input[name='visibility']:checked").val();
            var work_order_id = $('#work_order').val();
            var remove_file = $('#remove_file').val();
            var edit_document_name = $('#edit_document_name').val();
            var edit_document_original_name = $('#edit_document_name').attr('data-file-original-name');
            var edit_document_extension = $('#edit_document_name').attr('data-file-extension');
            var row_id = $('#row_id').val();
            //var result="";
            for( var i = 0, len = result_arr.length; i < len; i++ ) {
                    // console.log(len);console.log(result_arr[i]);
                    for( var j = 0, len1 = result_arr[i].length; j < len1; j++ ){
                         //console.log(result_arr[j]);
                        if( result_arr[i][j] === row_id ) {
                            var result = i;
                            break;
                        }
                    }
            }
            result_arr.splice(result, 1);
                if(result_arr.length==1){
                    result_arr.splice(0, 1);
            }
            $('#upload_attachments').val(result_arr);
            if(doc_id==''){
                var doc_id = 0;
            }
            var form_data = new FormData();

                if(file_data)
                {
                    form_data.append('file', file_data);
                }else{
                            form_data.append('file_name',edit_document_name);
                            form_data.append('file_original_name',edit_document_original_name);
                            form_data.append('extension',edit_document_extension);
                }
                form_data.append('document_type', selected_document_type);
                form_data.append('doc_value', doc_input);
                form_data.append('visibility', doc_visibile);
                form_data.append('doc_id', doc_id);
                form_data.append('work_order_id',work_order_id);
                form_data.append('remove_file',remove_file);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': $('meta[name=_token]').attr('content')
                }
            });
            $.ajax({
                url: "{{url('customer/create-your-own/edit/attachments')}}", // point to server-side PHP script
                data: form_data,
                type: 'POST',
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                processData: false,
                success: function (response) {
                    var data = jQuery.parseJSON(response);
                    var del_var = $('#row_id').val();
                        //console.log(data.db_arr);
                        $("#" + del_var).remove();
                        //add id to new row
                        if(data.db_arr[2] != null){
                            var f1 = data.db_arr[2];//console.log(f1);
                            var f2 = f1.split('.')[0];

                        }
                        if(data.ajax_arr[2] == null){
                            data.db_arr[2] = '';
                        }
                        var row = '<tr id="refreshdelete' + data.db_arr[4] + '">';
                    $.each((data.ajax_arr), function (i, d) {
                        $('.norecords').closest('tr').remove();
                        var display_url = '{{asset("attachment/work_order_document")}}/' + data.db_arr[2];
                                    if(d==null){
                                        row += '<td>' + " " + '</td>';
                                        }else{
                                        row += '<td>' + d + '</td>';
                                    }
                         $('.doc_field').hide();
                            $('.doc_file').hide();
                            $('.doc_visibility').hide();
                            $('#edit_document_type').val('');
                            $('#edit_document_type').selectpicker('refresh');
                            $('#edit_result_file').val('');
                            $('#edit_document_name').val('');
                            $('#edit_doc_input_id').val('');
                            $('#remove_file').val('0');
                            $('#preview').css('display','none');
                            $('#remove_icon').css('display','none');
                    });
                    var display_url = '{{asset("attachment/work_order_document")}}/' + data.db_arr[2];
                    var attachment_url = '{{url("customer/edit/cyo-work-ordwer/removeattachment")}}';

                    row += "<td><a class='btn btn-secondary item red-tooltip remove-attachment' data-toggle='modal' data-url='" + attachment_url + "'  data-file='" + data.db_arr[2] + "'data-id='" + data.db_arr[4] + "' data-target='#attachment-confirmation-modal' data-placement='bottom' title='Remove'><span class='icon-cross-remove-sign'></span></a><a class='btn btn-secondary item red-tooltip edit-attachment' data-file='" + data.db_arr[2] + "' data-type='" + data.db_arr[5] + "'data-title='" + data.db_arr[1] + "'data-id='" + data.db_arr[4]+ "'data-file-original-name='" + data.db_arr[3] + "'data-file-extension='" + data.db_arr[6]+"'><span class='icon-pencil-edit-button'></span></a>";

                   if(data.db_arr[2].length>0){
                            row += "<a target='_blank' href='" + display_url + "' class='btn btn-secondary item red-tooltip'><i class='fa fa-eye'></i></a>";
                            }
                            row += '</td></tr>';
                    $('#edit_document_table tbody').append(row);
                }
            });
        }
    }


    });

});

//Edit Document Start
    function openTab(th)
    {
        window.open(th.name,'_blank');
    }
    $('#remove_icon').click(function(){
        $('#preview').css('display','none');
        $('#remove_icon').css('display','none');
        $('#remove_file').val('1');
        $('#edit_result_file').val('');
    })
    $('#edit_result_file').change(function(e){
        var fileName = e.target.files[0].name;
        doc_icon = fileName.split('.')[1];
        if(doc_icon == 'pdf'){
            $('#preview').html('<i class="fa fa-file-pdf-o" style="font-size:36px"></i>');
        }else if(doc_icon == 'doc' || doc_icon == 'docx'){
            $('#preview').html('<i class="fa fa-file-word-o" style="font-size:36px"></i>');
        }else if(doc_icon == 'xls' || doc_icon == 'xlsx'){
            $('#preview').html('<i class="fa fa-file-excel-o" style="font-size:36px"></i>');
        }else if(doc_icon == 'jpg' || doc_icon == 'jpeg'){
            $('#preview').html('<i class="fa fa-file-image-o" style="font-size:36px"></i>');
        }else{
            $('#preview').html('<i class="fa fa-file" style="font-size:36px"></i>');
        }
        // $('#preview').html(fileName);
        $('#preview').css('display','block');
        $('#remove_icon').css('display','block');
        //alert('The file "' + fileName +  '" has been selected.');
    });

//End to fetch data of related work order
$(document).on('click', '.remove-attachment', function () {
    var url = $(this).attr('data-url');
    var file = $(this).attr('data-file');
    var id = $(this).attr('data-id');
    var row_id = $(this).closest('tr').attr('id');
    $(".remove-attachment-model").attr("action", url);
    $("input[name='file']").remove();
    $("input[name='id']").remove();
    $("input[name='row_id']").remove();
    $('body').find('.remove-attachment-model').append('<input name="file" type="hidden" value="' + file + '">');
    $('body').find('.remove-attachment-model').append('<input name="id" type="hidden" value="' + id + '">');
    $('body').find('.remove-attachment-model').append('<input name="_method" type="hidden" value="POST">');
    $('body').find('.remove-attachment-model').append('<input name="row_id" type="hidden" value="' + row_id + '">');
});
$(document).on('click', '.remove-parent-attachment', function () {
    var file = $(this).attr('data-file');
    var url = $(this).attr('data-url');
    $(".remove-attachment-model").attr("action", url);
    $("input[name='file']").remove();
    $('body').find('.remove-attachment-model').append('<input name="file" type="hidden" value="' + file + '">');
    $('body').find('.remove-attachment-model').append('<input name="_method" type="hidden" value="POST">');
});
$(document).on('click', '.remove-duplicate-attachment', function () {
    var file = $(this).attr('data-file');
    var url = $(this).attr('data-url');
    $(".remove-attachment-model").attr("action", url);
    $("input[name='file']").remove();
    $('body').find('.remove-attachment-model').append('<input name="file" type="hidden" value="' + file + '">');
    $('body').find('.remove-attachment-model').append('<input name="_method" type="hidden" value="POST">');
});
var remove_arr = [];
$('#remove-attachment-model').submit(function (event) {
    event.preventDefault();
    var file = $('input[name=file]').val();
    var id = file.split('.')[0];
     var row_id = $('input[name=row_id]').val();
    $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name=_token]').attr('content')
        }
    });
    var submit_url = $(this).attr("action");
    $.ajax({
        type: 'POST',
        url: submit_url,
        data: $('#remove-attachment-model').serialize(),
        success: function (response) {
            if (response.success) {
                $('#attachment-confirmation-modal').modal('hide');
                remove_arr.push(row_id);
                $('#remove_doc').val(remove_arr);
                var del_var = "refreshdelete" + id;

                $("#" + row_id).remove();
            }
        }
    });
});
$(document).on('click', '.remove-record', function () {

    var recipient_id = $(this).attr('data-id');
    var notice_id = $(this).attr('data-notice-id');
    var work_order_id = $(this).attr('data-workorder-id');

    var url = "{{url('customer/recipient/delete/')}}" + '/' + recipient_id + '/' + notice_id + '/' + work_order_id;
    if (url.indexOf("#") ==-1){
            url += '#recipient'; 
    }
        $(".remove-record-model").attr("action", url);
    
});

/****************** Notes- Public-private email addresses ***********/
$('input[type=radio][name=visibility]').change(function() {
    var customer_id = '{{$customer_id}}';
    $('#note_email').val('');
    $.ajax({
                type: "POST",
                url: '{{url("account-manager/research/email-notes")}}',
                data: {visibility: this.value, customer_id: customer_id},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                    console.log(response[0][1]);
                    var options = '' ;
                    options +='<option value="">Select</option>';
                    for (var i = 0; i < response[0].length; i++) {
                     options +='<option val="'+response[0][i]+'">'+response[0][i]+'</option>';
                    }
                   $('#note_email').html(options);
                   $("#note_email").selectpicker("refresh");


                }
    });
});

            </script>
            @endsection