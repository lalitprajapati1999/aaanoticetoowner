@extends('adminlte::page')
@section('content')
    <div>
        <section class="view-order secondary-doc">
            <div class="dashboard-wrapper">

                @if (Session::get('success'))
                    <div class="alert alert-success no-margin col-md-10">
                        <?php echo Session::get('success'); ?>
                    </div>
                @endif
                @if (Session::get('errors'))
                    <div class="alert alert-danger no-margin col-md-10">
                        @foreach ($errors->all() as $e)
                            {{ $e }}
                        @endforeach
                    </div>
                @endif
                @if (Session::get('error'))
                    <div class="alert alert-danger no-margin col-md-10">
                        {{Session::get('error')}}
                    </div>
                @endif
                @if (Session::get('debugLog') && !empty(Session::get('debugLog')))
                    <a href="javascript:;" class="btn btn-info col-md-2" id="btnViewInfo">View Debug Info</a>
                    <div class="alert alert-info col-md-12 no-margin" id="sectionDebugInfo" style="display:none;">{!! implode('<br />', Session::get('debugLog')) !!}</div>
                @endif
                <div class="alert alert-success" hidden="" id="success_msg" >
                   
                </div>
                <div class="alert alert-error" hidden="" id="show_msg">
                    <span id="alert_select_checkbox"></span>
                </div>
                <div class="dashboard-heading col-md-12">
                    <h1><span> Create Your Own Mailing</span></h1>
                </div>
                <div class="dashboard-inner-body">
                    <div class="row">

                        <div class="col-md-12">

                            <div class="search-by full-width">
                                <div class="form-group col-md-7">
                                    <label for="" class="display-ib">Search By: </label>
                                    <div class="calender-div display-ib pull-left">
                                        <p class="display-ib pull-left">Date Range</p>
                                        <form class="date-box display-ib pull-left">
                                            <div class="form-group">
                                                <div class="input-wrapper full-width">
                                                    <div class="styled-input">
                                                        <input class="form-control " id="from-date"
                                                               type="text" name="from_date"autocomplete="off">
                                                        <!-- <label>Start Date</label> -->
                                                        <span></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-default"><span
                                                        class="icon-calendar"></span></button>
                                        </form>
                                        <p class="display-ib pull-left">to</p>
                                        <form class="date-box display-ib pull-left">
                                            <div class="form-group">
                                                <div class="input-wrapper full-width">
                                                    <div class="styled-input">
                                                        <input class="form-control " id="to-date" type="text"
                                                               name="to_date" autocomplete="off">
                                                        <!-- <label>End Date</label> -->
                                                        <span></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-default"><span
                                                        class="icon-calendar"></span></button>
                                        </form>

                                    </div>
                                </div>

                                <div class="form-group col-md-2">

                                    <form role="form">
                                        <div class="form-group display-ib pull-left">
                                            <input class="form-control" name="work_order_no" id="work_order_no"
                                                   placeholder="Search WO Number" type="text">
                                          
                                        </div>

                                    </form>
                                </div>


                                <!--   <input type="button" name="reset" value="Reset"> -->
                                <div class="form-group col-md-1">
                                    <div class="row">
                                        <div class="col-md-12 text-right">
                                            <div class="form-btn-div full-width">
                                                <button id="resetall" class="btn btn-primary custom-btn form-btn"><span>Reset</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                              
                        </div>
                    </div>
                       
                    <form role="form" method="post" action="" id="printoption">
                        <div class="row">
                            <div class="col-md-6">
                                <!-- <div class="col-md-6">
                                    <div class="checkbox-holder">
                                        <a href="#"><input type="checkbox" class="regular-checkbox not-empty type_of_label" id="next_day_delivery_value"
                                                            name="next_day_delivery_value" value="next day delivery"
                                                            >
                                            <label for="next_day_delivery_value" class="form-check-label2">Next Day
                                                Delivery</label>
                                        </a>
                                    </div>
                                </div> -->
                                <div class="col-md-6">
                                    <div class="checkbox-holder">
                                    
                                       <!--  <a href="#"><input type="checkbox" class="regular-checkbox not-empty type_of_label" id="return_receipt_requested"
                                                            name="return_receipt_requested" value="Cert. RR"
                                                        > -->
                                        <a href="#"><input type="checkbox" class="regular-checkbox not-empty type_of_mailing" id="return_receipt_requested"
                                                            name="return_receipt_requested" value="Certified Mail Return Receipt Requested"
                                                        >
                                            <label for="return_receipt_requested" class="form-check-label2">Cert. RR</label>
                                        </a>
                                    </div>
                                </div> 
                                <div class="col-md-6">
                                    <div class="checkbox-holder">
                                        <a href="#"><input type="checkbox" class="regular-checkbox not-empty type_of_label" id="firm_mail_value"
                                                            name="firm_mail_value" value="firm mail"
                                                            >
                                            <label for="firm_mail_value" class="form-check-label2">Firm Mail</label>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                        <div class="checkbox-holder">
                                        <!--  <a href="#"><input type="checkbox" class="regular-checkbox not-empty type_of_mailing" id="certified_mail_value"
                                                                name="certified_mail_value" value="Certified Mail"
                                                            > -->
                                            <!-- <a href="#"><input type="checkbox" class="regular-checkbox not-empty type_of_label" id="certified_mail_value"
                                                                name="certified_mail_value" value="Cert. USPS"
                                                            > -->
                                            <a href="#"><input type="checkbox" class="regular-checkbox not-empty type_of_mailing" id="certified_mail_value"
                                                            name="certified_mail_value" value="Certified Mail"
                                                        >
                                                <label for="certified_mail_value" class="form-check-label2">Cert. USPS </label>
                                            </a>
                                        </div>
                                </div>

                                <!-- <div class="col-md-6">
                                    <div class="checkbox-holder">
                                        <a href="#"><input type="checkbox" class="regular-checkbox not-empty type_of_mailing" id="priority_mail_1_2_day"
                                                            name="priority_mail_1_2_day" value="Priority Mail ( 2-4 days)"
                                                            >
                                            <label for="priority_mail_1_2_day" class="form-check-label2">Priority Mail ( 2-4 days)</label>
                                        </a>
                                    </div>
                                </div>  -->
                                <div class="col-md-6">
                                    <div class="checkbox-holder">
                                        <a href="#"><input type="checkbox" class="regular-checkbox not-empty type_of_label" id="manual"
                                                            name="manual" value="manual"
                                                        >
                                            <label for="manual" class="form-check-label2">Manual</label>
                                        </a>
                                    </div>
                                </div> 
                                
                                <div class="col-md-6">
                                    <div class="checkbox-holder">
                                        <!-- <a href="#"><input type="checkbox" class="regular-checkbox not-empty type_of_label" id="electronic_return_receipt"
                                                            name="electronic_return_receipt" value="Cert. ER"
                                                            > -->
                                         <a href="#"><input type="checkbox" class="regular-checkbox not-empty type_of_mailing" id="electronic_return_receipt"
                                                            name="electronic_return_receipt" value="Certified Mail Electronic Return Receipt"
                                                        >
                                            <label for="electronic_return_receipt" class="form-check-label2">Cert. ESRR</label>
                                        </a>
                                    </div>
                                </div> 

                                <!-- <div class="col-md-6">
                                    <div class="checkbox-holder">
                                        <a href="#"><input type="checkbox" class="regular-checkbox not-empty type_of_mailing" id="priority_mail_1_day"
                                                            name="priority_mail_1_day" value="Priority Mail Express (1 day)"
                                                            >
                                            <label for="priority_mail_1_day" class="form-check-label2">Priority Mail (1 day)</label>
                                        </a>
                                    </div>
                                </div>  -->
                                
                            </div>

                        <div class="col-md-6 text-right pull-right">
                                <input type="hidden" name="type_work_order" value="cyo">
                                <div class="form-btn-div">
                                    <button type="button" name="createLabel"
                                            class="btn btn-primary form-btn"
                                            VALUE="Create Shipping Labels" id="createShippingLabels"><span>Generate Shipping Labels</span>
                                    </button>
                                          <button type="button" name="printWO" class="btn btn-primary form-btn printcyoworkordernc printcyoworkorder"
                                            VALUE="Save For Later" id="printcyoworkorder"><span>Print WO</span>
                                    </button>
                                      <button type="button" name="printWO"  style="display:none;" class="btn btn-primary form-btn printcyoworkorderc printcyoworkorder" 
                                        VALUE="" id="printcyoworkorder"><span>Print WO</span>
                                    </button>
                                      <button type="button" name="complete_wo"
                                            class="btn btn-primary form-btn"
                                            VALUE="complete wo" id="complete_wo"><span>Complete WO</span>
                                    </button>
                                    <button type="button" name="printfirmmail"
                                            class="btn btn-primary form-btn print_mail"
                                            VALUE="firm mail" id="printfirmmail" title="Print Manual & Firm Mail Lables"><span>Print Labels</span>
                                    </button>
                                    <button type="button" name="Completedwolist"
                                            class="btn btn-primary form-btn Completedwolist"
                                            VALUE="" id="Completedwolist" title="Completed WO List"><span>Completed WO List</span>
                                    </button>
                                  
                                </div>
                            </div>
                        </div>

                        
                        {!! csrf_field() !!}
                        
                            <input type="hidden" name="type_of_label_value[]" value="" id="type_of_label_value">
                            <input type="hidden" name="type_of_mailing_value[]" value="" id="type_of_mailing_value">
                        <div class="col-md-12">
                            <div class="dahboard-table table-responsive fixhead-table">
                                <table class="table table-striped" id="mailingdatatable" datatable="" width="100%"
                                        cellspacing="0" data-scroll-x="true" scroll-collapse="false">


                                    <thead class="thead-dark">
                                    <tr>
                                        <th scope="col"><input type="checkbox" name="select_all" id="select-all"
                                                                class="chk">Select All
                                        </th>
                                        <th scope="col">Work Order</th>
                                        <th scope="col">Created At</th>
                                        <th scope="col">Due Date</th>
                                        <th scope="col">Next Day Delivery</th>
                                        <!--                                                <th scope="col">2 Days</th>
                                                                                        <th scope="col">Certified</th>-->
                                        <th scope="col">Template Name</th>
                                        <th scope="col">Firm Mail</th>
                                        <th scope="col">Print WO</th>
                                        <th scope="col">Print Firm Mail</th>
                                    </tr>
                                    </thead>

                                </table>
                            </div>
                        </div>
                        </form>    
                    </div>
                </div>
            </div>
            <meta name="csrf-token" content="{{ csrf_token() }}">
        </section>
    </div>
@endsection
<!--Note Modal -->

@section('frontend_js')
    <script src="{{ asset('vendor/adminlte/plugins/datepicker/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script type="text/javascript" src="{{asset('js/dataTables.buttons.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/jszip.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/buttons.html5.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/buttons.print.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/dataTables.select.min.js')}}"></script>
    <link type="text/css" rel="stylesheet" href='{{asset('css/buttons.dataTables.min.css')}}'>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.js"></script>
    <script src="https://printjs-4de6.kxcdn.com/print.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://printjs-4de6.kxcdn.com/print.min.css">
    <script type="text/javascript">

        var mailing_checkbox = new Array();
        var flag = {!! isset($flag) ?$flag :0  !!};
        var mailing_checkbox = new Array();
        var date = new Date();
        var minutes = 15;
        date.setTime(date.getTime() + 1 * 60 * 60 * 1000);
        //unset cookie on page refresh
        if (flag == '1') {
            $.cookie('mailing_checkbox', null, {path: '/'});
            $(".select_mail_create_session").prop("checked", false);
            // $.cookie('mailing_checkbox', '', time() - 3600, '/');
            $.cookie('select_all_mail', 0, {path: '/'});
        }
        if ($.cookie('select_all_mail') == 1 && flag == 0) {
            $("#select-all").prop("checked", true);

        }
        if (flag == 0) {
            var empString = $.cookie("mailing_checkbox");//retrieving data from cookie
            mailing_checkbox = empString.split(',');
        }
        $(document).on('change', '.select_mail_create_session', function () {
            if ($(this).is(':checked')) {
                mailing_checkbox.push($(this).val());
            } else {

                mailing_checkbox.splice($.inArray($(this).val(), mailing_checkbox), 1);
                /*$.cookie('select_all_mail', '0',{ expires: date , path:'/'});*/
                $(':checkbox#select-all').prop('checked', false);

            }
            $.cookie('mailing_checkbox', mailing_checkbox, {expires: date, path: '/'});
            //console.log(mailing_checkbox);
        });
        $(document).on('change', '#select-all', function () {
            if ($(this).is(':checked')) {
                $.cookie('select_all_mail', '1', {path: '/'});
            } else {
                $.cookie('select_all_mail', '0', {path: '/'});
            }

        });
        $('.workorder_list').on('change', function () {
            $('.workorder_list').not(this).prop('checked', false);
        });

         // complete work order
$('#complete_wo').click(function () {
    var form_action = "{{url('account-manager/complete-wo')}}";
    if ($('.workorder_list').length != 0 && $('.workorder_list:checked').length > 0) {
    if (typeof $.cookie('mailing_checkbox') != 'undefined' && $.cookie('mailing_checkbox') != "" && $.cookie('mailing_checkbox') != null) {
        $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        $.ajax({
            type: 'POST',
            url: form_action,
            dataType: "json",
            async: false,
            cache: false,
            beforeSend: function () {
                $('#pleaseWait').modal('show');
            },
            complete: function () {
                $('#pleaseWait').modal('hide');
            },
            data: (mailing_checkbox.map(key => 'select_work_orders[]=' + key).join('&')) + "&work_order_no=" + $('#work_order_no').val()+"&type_work_order=cyo",
            success: function (response) {
            if (response.status == 'success') {
                location.reload();
                $('#success_msg').css('display','block');
                var message = "Selected Work orders get completed.";
                $('#success_msg').html(message);    
                } else {
                    alert(response.message);
                }
            }
        });
    } 
    }else {
        alert("Please select at least one work order.");
                }

            });

        function filterStatus() {
            //build a regex filter string with an or(|) condition
            var types = $('input:checkbox[name="next_day_delivery"]:checked').map(function () {
                return this.value;
            }).get().join('|');
            //console.log(types);
            //filter in column 0, with an regex, no smart filtering, no inputbox,not case sensitive
            otable.fnFilter(types, 4);


        }
        var type_of_label = [];
        $(".type_of_label").click(function(){
            if($(this).prop("checked") == true){
                type_of_label.push(this.value);
                $('#type_of_label_value').val(type_of_label);
            }
            else if($(this).prop("checked") == false){
               /* type_of_label = jQuery.grep(type_of_label, function(value) {
                  return value == this.value;
                });*/
                type_of_label.splice($.inArray(this.value, type_of_label),1);
                $('#type_of_label_value').val(type_of_label);
                
            }
        })

         var type_of_mailing = [];
        $(".type_of_mailing").click(function(){
            if($(this).prop("checked") == true){
                type_of_mailing.push(this.value);
                $('#type_of_mailing_value').val(type_of_mailing);
            }
            else if($(this).prop("checked") == false){
               /* type_of_label = jQuery.grep(type_of_label, function(value) {
                  return value == this.value;
                });*/
                type_of_mailing.splice($.inArray(this.value, type_of_mailing),1);
                $('#type_of_mailing_value').val(type_of_mailing);
                
            }
        })

        function updateDataTableSelectAllCtrl(table) {
            var $table = table.table().node();
            var $chkbox_all = $('tbody input[type="checkbox"]', $table);
            var $chkbox_checked = $('tbody input[type="checkbox"]:checked', $table);
            var chkbox_select_all = $('thead input[name="select_all"]', $table).get(0);

            // If none of the checkboxes are checked
            if ($chkbox_checked.length === 0) {
                chkbox_select_all.checked = false;
                if ('indeterminate' in chkbox_select_all) {
                    chkbox_select_all.indeterminate = false;
                }
                //$.cookie('select_all_mail', '0');

                // If all of the checkboxes are checked
            } else if ($chkbox_checked.length === $chkbox_all.length) {
                chkbox_select_all.checked = true;
                if ('indeterminate' in chkbox_select_all) {
                    chkbox_select_all.indeterminate = false;
                }
                /*select all cookie*/
                // $.cookie('select_all_mail', '1');
                // If some of the checkboxes are checked
            } else {
                chkbox_select_all.checked = true;
                if ('indeterminate' in chkbox_select_all) {
                    chkbox_select_all.indeterminate = true;
                }

            }
            //  $.cookie('mailing_checkbox', "");
        }

        $(document).ready(function () {

            $('#btnViewInfo').click(function() {
                $('#sectionDebugInfo').toggle();
            })

            $('#select').parsley();
            //code for checking whether atleast one checkbox is checked to proceed
//                                            if ($('.workorder_list').length == 0)
//                                            {
//                                                $('#createShippingLabels').attr('disabled', true);
//                                            } else
//                                            {
//                                                $('#createShippingLabels').attr('disabled', false);
//                                            }
            //code for checking whether atleast one checkbox is checked to proceed

            $('#show_msg').hide();
            $('#select').click(function () {
                checked = $("input[type=checkbox]:checked").length;

                if (!checked) {
                    $('#show_msg').show();
                    $('#show_msg').html('<span>' + "Please Select Work Order." + '</span');
                    //alert("You must check at least one checkbox.");
                    return false;
                }

            });
            //date sorting
            /*  $('#from-date').on('submit', function (e) {

             otable.fnFilter(this.value, 2, true, true, true, true, true, true, true, true);
             });
             $('#to-date').on('submit', function (e) {
             docTable.draw();
             e.preventDefault();
             });*/
            //datepicker
            $('.datepicker').datepicker({
                dateFormat: 'dd-mm-yy'
            });

            //datatable for list
            var docTable = $('#mailingdatatable').DataTable({
                order: [[0, 'desc' ]],
                "language": {

                    "zeroRecords": "No Record Found",
                    "paginate": {
                        "previous": '<img src="../../images/left-arrow.png">',
                        "next": '<img src="../../images/right-arrow.png">'

                    }

                },
                processing: true,
                serverSide: true,
                pagingType: "simple_numbers",
                dom: "<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-6'p>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
                columnDefs: [
                    {"width": "90px", "targets": 0},
                    {"width": "150px", "targets": 1},
                    {"width": "250px", "targets": 2, "sortable": false},
                    {"width": "150px", "targets": 3},
                    {"width": "100px", "targets": 4},
                    {"width": "100px", "targets": 5},
                ],
                ajax: {
                    url: "{{url('account-manager/get-cyo-mailing-workorders')}}",
                    data: function (d) {
                        d.from_date = $('input[name=from_date]').val();
                        d.to_date = $('input[name=to_date]').val();
                        d.work_order_no = $('input[name=work_order_no]').val();
                        d.Completedwolist = $('button[name=Completedwolist]').val();

                    }
                },
                select: {
                    style: 'os',
                    selector: 'td:first-child'
                },
                columns: [

                    {data: 'actions', name: 'actions'},
                    {data: 'workorder_id', name: 'workorder_id'},
                    {
                        data: 'created_at', render: function (data, status, row) {
                            if (data) {
                                var date = new Date(data);
                                var month = date.getMonth() + 1;
                                return (month.length > 1 ? month : month) + "/" + date.getDate() + "/" + date.getFullYear();
                            } else
                                return "NA";
                        }
                    },
                    {
                        data: 'due_date', render: function (data, status, row) {
                            if (data) {
                                var date = new Date(data);
                                var month = date.getMonth() + 1;
                                return (month.length > 1 ? month : month) + "/" + date.getDate() + "/" + date.getFullYear();
                            } else
                                return "NA";
                        }
                    },

                    {
                        data: 'next_day_Delivery', render: function (data, status, row) {
                            return data;
                        }
                    },
//            {data: "default", render: function (data, status, row) {
//                    if (data) {
//                        return data;
//                    } else
//                        return "No";
//                }},
//            {data: "default", render: function (data, status, row) {
//                    if (data) {
//                        return data;
//                    } else
//                        return "NA";
//                }},
                    {
                        data: "notice_name", class: 'col pro-temp', render: function (data, notice_id, row) {
                            return data;


                        }
                    },
                    {
                        data: 'firm_mail', render: function (data, status, row) {
                            return data;
                        }
                    },
                    {
                        data: 'status', render: function (data, status, row) {
                            if (data == '5') {
                                return 'No';
                            } else {
                                return 'Yes';
                            }

                        }
                    },
                    {
                        data: 'print_firm_mail', render: function (data, status, row) {
                            return data;
                        }
                    },
                ]

            });
            $.fn.dataTable.ext.search.push(
                function (settings, data, dataIndex) {
                    var min = $('#from-date').datepicker("getDate");
                    var max = $('#to-date').datepicker("getDate");
                    var startDate = new Date(data[4]);
                    if (min == null && max == null) {
                        return true;
                    }
                    if (min == null && startDate <= max) {
                        return true;
                    }
                    if (max == null && startDate >= min) {
                        return true;
                    }
                    if (startDate <= max && startDate >= min) {
                        return true;
                    }
                    return false;
                }
            );
            $.fn.dataTable.ext.search.push(
                function (settings, data, dataIndex) {
                    var min = $('#from-date').datepicker("getDate");
                    var max = $('#to-date').datepicker("getDate");
                    var startDate = new Date(data[4]);
                    if (min == null && max == null) {
                        return true;
                    }
                    if (min == null && startDate <= max) {
                        return true;
                    }
                    if (max == null && startDate >= min) {
                        return true;
                    }
                    if (startDate <= max && startDate >= min) {
                        return true;
                    }
                    return false;
                }
            );
            var rows_selected = [];
            // Handle click on checkbox
            $('#mailingdatatable tbody').on('click', 'input[type="checkbox"]', function (e) {
                var $row = $(this).closest('tr');

                // Get row data
                var data = docTable.row($row).data();

                // Get row ID
                var rowId = data[0];

                // Determine whether row ID is in the list of selected row IDs
                var index = $.inArray(rowId, rows_selected);

                // If checkbox is checked and row ID is not in list of selected row IDs
                if (this.checked && index === -1) {
                    rows_selected.push(rowId);

                    // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
                } else if (!this.checked && index !== -1) {
                    rows_selected.splice(index, 1);
                }

                if (this.checked) {
                    $row.addClass('selected');
                } else {
                    $row.removeClass('selected');
                }

                // Update state of "Select all" control
                updateDataTableSelectAllCtrl(docTable);

                // Prevent click event from propagating to parent
                e.stopPropagation();
            });
            $('#mailingdatatable').on('click', 'tbody td, th:first-child', function (e) {
                $(this).parent().find('input[type="checkbox"]').trigger('click');
            });
            $('input[name="select_all"]', docTable.table().container()).on('click', function (e) {
                if (this.checked) {
                    $('#mailingdatatable tbody input[type="checkbox"]:not(:checked)').trigger('click');
                } else {
                    $('#mailingdatatable tbody input[type="checkbox"]:checked').trigger('click');
                }

                // Prevent click event from propagating to parent
                e.stopPropagation();
            });
            $("#from-date").datepicker({
                onSelect: function () {
                    docTable.draw();
                }, changeMonth: true, changeYear: true
            });
            $("#to-date").datepicker({
                onSelect: function () {
                    docTable.draw();
                }, changeMonth: true, changeYear: true
            });
            $('#from-date, #to-date').on('change', function () {
                docTable.draw();
            });
            $(function () {
                otable = $('#mailingdatatable').dataTable();
            });
            //sorted by workorder
           /* $('#work_order_no').on('keyup', function () {

                otable.fnFilter(this.value, 1, true, true, true, true, true, true, true, true);
            });*/
            $('#work_order_no').on('keyup', function () {
                docTable.draw();
            });
            $('#Completedwolist').on('click', function () {
                $('.workorder_list').each(function() {
                     this.checked = false; 
                    $('.workorder_list').val('');
                });
                $('#createShippingLabels').attr('disabled','disabled');
                $('#complete_wo').attr('disabled','disabled');
                $('#printfirmmail').attr('disabled','disabled');
                $(".printcyoworkordernc").hide();
                $(".printcyoworkorderc").show();
                $(".printcyoworkorderc").val(1);
                $("#Completedwolist").attr('value', '1'); 
                docTable.draw();
            });

            //Over Night data sorting
            /*$('#next_day_delivery').on('change', function () {

                otable.fnFilter(this.value, 4, true, true, true, true, true, true, true, true);
            });*/
            $('#next_day_delivery').on('change', function () {
                if ($('#next_day_delivery').is(':checked')) {
                    otable.fnFilter('Yes', 4, true, true, true, true, true, true, true, true);
                } else {
                  var table = $('#mailingdatatable').DataTable();
                    table
                     .search( '' )
                     .columns().search( '' )
                     .draw();
                }
            });
             

            //2 Days data sorting
//    $('#2_days').on('change', function () {
//
//        otable.fnFilter(this.value, 4, true, true, true, true, true, true, true, true);
//    });

            $('.printcyoworkorder').click(function () {
                var pwo = $(".printcyoworkorderc").val();
                if(pwo == 1){
                    if ($('.workorder_list').length != 0 && $('.workorder_list:checked').length > 0) {
                      if (typeof $.cookie('mailing_checkbox') != 'undefined' && $.cookie('mailing_checkbox') != "" && $.cookie('mailing_checkbox') != null) {
                        var form_action ="{{url('account-manager/mailing/completedwoprintpdf')}}";
                        var work_order_type ="cyo";
                        var work_order_id =mailing_checkbox;
                        $.ajaxSetup({
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                }
                        });
                            
                        $.ajax({
                            url:form_action,
                            method:'POST',
                            beforeSend: function () {
                                $('.printcyoworkorder').prop('disabled', true);
                                $('#pleaseWait').modal('show');
                            },
                            complete: function () {
                               setTimeout(function() {
                                    $('.printcyoworkorder').prop('disabled', false);
                                    $('#pleaseWait').modal('hide');
                                }, 1000);
                            },
                            data: {
                                work_order_type:work_order_type,
                                work_order_id:work_order_id
                            },
                            success: function (response) {
                                    if (response.status == 'success') {
                                        printJS({
    
                                            printable: response.message,
                                            type: 'pdf',
                                            onPrintDialogClose: closedWindow,
                                        })
                                        function closedWindow() {
                                                //location.reload();
                                            }
                                    } else {
                                        alert(response.message);
                                    }
                            },
                            error:function(error){
                                console.log(error)
                            }
                        });
                    }    
                  }else{
                    alert("Please select at least one work order.");
                  }
                }else{
                var form_action = "{{url('account-manager/mailing/printpdf')}}";
                if ($('#next_day_delivery').is(':checked')) {
                    var next_day_delivery = "Yes";
                } else {
                    var next_day_delivery = "No";
                }
                if ($('.workorder_list').length != 0 && $('.workorder_list:checked').length > 0) {
                if (typeof $.cookie('mailing_checkbox') != 'undefined' && $.cookie('mailing_checkbox') != "" && $.cookie('mailing_checkbox') != null) {
                    var type_of_label = $('#type_of_label_value').val();
                    var type_of_mailing = $('#type_of_mailing_value').val();
                    var result  = type_of_label.split(',');
                    var mailing_type_result  = type_of_mailing.split(',');
                    
                    result = $.grep(result,function(n){
                        return(n);
                    });
                    mailing_type_result = $.grep(mailing_type_result,function(n){
                        return(n);
                    });
                    var final_result = result.concat(mailing_type_result);
                    $('#pleaseWait').modal('show');
                    $('#printworkorder').prop('disabled', true);
                    $.ajax({
                        type: 'POST',
                        url: form_action,
                        dataType: "json",
                        // async: false,
                        // cache: false,
                        beforeSend: function () {
                            $('#pleaseWait').modal('show');
                        },
                        complete: function () {
                            $('#pleaseWait').modal('hide');
                        },
                        data: $("#printoption").serialize() + "&from-date=" + $('#from-date').val() + "&to-date=" + $('#to-date').val() + "&work_order_no=" + $('#work_order_no').val()+ "&type_of_mailing=" + final_result,
                        success: function (response) {
                            if (response.status == 'success') {
                                 printJS({
                                    printable: response.message,
                                    type: 'pdf',
                                    onPrintDialogClose: closedWindow,
                                })
                                function closedWindow() {
                                        //location.reload();
                                    }
                            } else {
                                alert(response.message);
                            }
                        }
                    });
                }}else{
                    alert("Please select at least one work order.");
                }
                }
            });
            //Print firm mail
            $('.print_mail').click(function (e) {
                e.preventDefault();
                var print_type = $(this).text(); 
                var form_action = "{{url('account-manager/mailing/printfirmmail')}}"+"/"+print_type;
                //   $('#printoption').attr('action', form_action);
                //   $('#printoption').submit();
                // location.reload();
                if ($('#next_day_delivery').is(':checked')) {
                    var next_day_delivery = "Yes";
                } else {
                    var next_day_delivery = "No";
                }

                if ($('.workorder_list').length != 0 && $('.workorder_list:checked').length > 0) {
                if (typeof $.cookie('mailing_checkbox') != 'undefined' && $.cookie('mailing_checkbox') != "" && $.cookie('mailing_checkbox') != null) {
                    $.ajax({
                        type: 'POST',
                        url: form_action,
                        dataType: "json",
                        async: false,
                        cache: false,
                        beforeSend: function () {
                            $('#printcyoworkorder').prop('disabled', true);
                            $('#pleaseWait').modal('show');
                        },
                        complete: function () {
                            $('#printcyoworkorder').prop('disabled', false);
                            $('#pleaseWait').modal('hide');
                        },
                        data: $("#printoption").serialize() + "&from-date=" + $('#from-date').val() + "&to-date=" + $('#to-date').val() + "&work_order_no=" + $('#work_order_no').val() + "&next_day_delivery=" + next_day_delivery,
                        success: function (response) {
                            if (response.status == 'success') {
                                $.each(response.message, function (key, value) {
                                    window.open(value);
                                });
                            } else {
                                alert(response.message);
                            }
                        }
                    });
                }}else {
                    alert("Please select at least one work order.");
                }

            });
        });


        //to check if atleast one checkbox is checked to enable button
        $(document).on('change', '.workorder_list', function () {
            // if ($('.workorder_list').length == 0 || $('.workorder_list:checked').length <= 0) {
            //     $('#createShippingLabels').attr('disabled', true);
            // } else {
            //     $('#createShippingLabels').attr('disabled', false);
            // }
        });
        //to check if atleast one checkbox is checked to enable button
        $('#createShippingLabels').click(function () {
            if ($('#select-all').is(':checked')) {
                $.cookie('select_all_mail', '1', {path: '/'});
            } else {
                $.cookie('select_all_mail', '0', {path: '/'});
            }
            if (typeof $.cookie('mailing_checkbox') != 'undefined' && $.cookie('mailing_checkbox') != "" && $.cookie('mailing_checkbox') != null) {

                $('#pleaseWait').modal('show');
                //console.log('asfasfsaf');
                var form_action = '{{url("account-manager/cyo-mailing/createShippingLabels")}}';
                $('#printoption').attr('action', form_action);
                $("#printoption").removeAttr("target", "_blank");
                $('#printoption').submit();
            } else {
                alert("Please select at least one work order.");
            }
        });
    </script>
@endsection