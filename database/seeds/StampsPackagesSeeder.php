<?php

use Illuminate\Database\Seeder;

class StampsPackagesSeeder extends Seeder {

    public function run()
    {
        DB::table('stamps_packages')->delete();
        $insert_array = [
            ['name'=>'Postcard'],
            ['name'=>'Letter','status'=>'1'],
            ['name'=>'Large Envelope or Flat','status'=>'1'],
            ['name'=>'Thick Envelope','status'=>'1'],
            ['name'=>'Package'],
            ['name'=>'Small Flat Rate Box'],
            ['name'=>'Flat Rate Box'],
            ['name'=>'Large Flat Rate Box'],
            ['name'=>'Flat Rate Envelope','status'=>'1'],
            ['name'=>'Flat Rate Padded Envelope'],
            ['name'=>'Large Package'],
            ['name'=>'Oversized Package'],
            ['name'=>'Regional Rate Box A'],
            ['name'=>'Regional Rate Box B'],
            ['name'=>'Regional Rate Box C'],
            ['name'=>'Legal Flat Rate Envelope'],
            ['name'=>'Express Envelope','status'=>'1'],
            ['name'=>'Documents','status'=>'1'],
            ['name'=>'Envelope','status'=>'1'],
            ['name'=>'Pak']
        ];
        foreach($insert_array as $val)
        {
            \App\Models\StampsPackages::insert(['name'=>$val]);
        }        
    }

}