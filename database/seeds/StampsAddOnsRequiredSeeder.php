<?php

use Illuminate\Database\Seeder;

class StampsAddOnsRequiredSeeder extends Seeder {

    public function run()
    {
        DB::table('stamps_add_ons_required')->delete();
        $insert_array = [
            ['add_on'=>11,'required'=>5],
            ['add_on'=>11,'required'=>6],
            ['add_on'=>11,'required'=>9],
            ['add_on'=>11,'required'=>15],
            ['add_on'=>13,'required'=>5],
            ['add_on'=>13,'required'=>6],
            ['add_on'=>13,'required'=>9],
            ['add_on'=>13,'required'=>12],
            ['add_on'=>13,'required'=>15],
            ['add_on'=>13,'required'=>27],
            ['add_on'=>13,'required'=>28],
            ['add_on'=>20,'required'=>6],
            ['add_on'=>21,'required'=>5],
            ['add_on'=>21,'required'=>6],
            ['add_on'=>21,'required'=>9],
            ['add_on'=>21,'required'=>12],
        ];
        foreach($insert_array as $val)
        {
            \App\Models\StampsAddOnsRequired::insert($val);
        }
    }

}