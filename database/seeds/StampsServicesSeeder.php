<?php

use Illuminate\Database\Seeder;

class StampsServicesSeeder extends Seeder {

    public function run()
    {
        DB::table('stamps_service_type')->delete();
        $insert_array = [
            ['abbr'=>'US-FC','description'=>'USPS First Class Mail','status'=>'1'],
            ['abbr'=>'US-MM','description'=>'USPS Media Mail','status'=>'1'],
            ['abbr'=>'US-PP','description'=>'USPS Parcel Post','status'=>'1'],
            ['abbr'=>'US-PM','description'=>'USPS Priority Mail','status'=>'1'],
            ['abbr'=>'US-XM','description'=>'USPS Priority Mail Express','status'=>'1'],
            ['abbr'=>'US-EMI','description'=>'USPS Priority Mail Express International','status'=>'1'],
            ['abbr'=>'US-PMI','description'=>'USPS Priority Mail International','status'=>'1'],
            ['abbr'=>'US-FCI','description'=>'USPS First Class Mail International','status'=>'1'],
            ['abbr'=>'US-PS','description'=>'USPS Parcel Select Ground','status'=>'1'],
            ['abbr'=>'US-LM','description'=>'USPS Library Mail'],
            ['abbr'=>'DHL-PE','description'=>'','status'=>'0'],
            ['abbr'=>'DHL-PG','description'=>'','status'=>'0'],
            ['abbr'=>'DHL-PPE','description'=>'','status'=>'0'],
            ['abbr'=>'DHL-PPG','description'=>'','status'=>'0'],
            ['abbr'=>'DHL-BPME','description'=>'','status'=>'0'],
            ['abbr'=>'DHL-BPMG','description'=>'','status'=>'0'],
            ['abbr'=>'DHL-MPE','description'=>'','status'=>'0'],
            ['abbr'=>'DHL-MPG','description'=>'','status'=>'0'],
            ['abbr'=>'AS-IPA','description'=>'','status'=>'0'],
            ['abbr'=>'AS-ISAL','description'=>'','status'=>'0'],
            ['abbr'=>'AS-EPKT','description'=>'','status'=>'0'],
            ['abbr'=>'DHL-PIPA','description'=>'','status'=>'0'],
            ['abbr'=>'DHL-PISAL','description'=>'','status'=>'0'],
            ['abbr'=>'GG-IPA','description'=>'','status'=>'0'],
            ['abbr'=>'GG-ISAL','description'=>'','status'=>'0'],
            ['abbr'=>'GG-EPKT','description'=>'','status'=>'0'],
            ['abbr'=>'IBC-IPA','description'=>'','status'=>'0'],
            ['abbr'=>'IBC-ISAL','description'=>'','status'=>'0'],
            ['abbr'=>'IBC-EPKT','description'=>'','status'=>'0'],
            ['abbr'=>'RRD-IPA','description'=>'','status'=>'0'],
            ['abbr'=>'RRD-ISAL','description'=>'','status'=>'0'],
            ['abbr'=>'RRD-EPKT','description'=>'','status'=>'0'],
            ['abbr'=>'AS-GNRC','description'=>'','status'=>'0'],
            ['abbr'=>'GG-GNRC','description'=>'','status'=>'0'],
            ['abbr'=>'RRD-GNRC','description'=>'','status'=>'0'],
            ['abbr'=>'SC-GPE','description'=>'','status'=>'0'],
            ['abbr'=>'SC-GPP','description'=>'','status'=>'0'],
            ['abbr'=>'SC-GPESS','description'=>'','status'=>'0'],
            ['abbr'=>'SC-GPPSS','description'=>'','status'=>'0'],
            ['abbr'=>'DHL-EWW','description'=>'','status'=>'0'],
            ['abbr'=>'FX-GD','description'=>'','status'=>'0'],
            ['abbr'=>'FX-HD','description'=>'','status'=>'0'],
            ['abbr'=>'FX-2D','description'=>'','status'=>'0'],
            ['abbr'=>'FX-ES','description'=>'','status'=>'0'],
            ['abbr'=>'FX-SO','description'=>'','status'=>'0'],
            ['abbr'=>'FX-PO','description'=>'','status'=>'0'],
            ['abbr'=>'FX-GDI','description'=>'','status'=>'0'],
            ['abbr'=>'FX-EI','description'=>'','status'=>'0'],
            ['abbr'=>'FX-PI','description'=>'','status'=>'0']
        ];
        foreach($insert_array as $val)
        {
            \App\Models\StampsServices::insert($val);
        }        
    }

}