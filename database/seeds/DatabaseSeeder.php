<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->delete();
    	DB::table('categories')->delete();  
        DB::table('master_notices')->delete();
        DB::table('cities')->delete();
        DB::table('states')->delete();
        DB::table('countries')->delete();
        DB::table('pricings')->delete();
        DB::table('packages')->delete();
        DB::table('notices')->delete();
    	DB::table('notice_fields')->delete();
		DB::table('notice_lienblog')->delete();
		DB::table('notice_templates')->delete();
		DB::table('notice_testimonial')->delete();
		DB::table('permission_roles')->delete();
		DB::table('project_roles')->delete();
		DB::table('project_types')->delete();
		DB::table('roles')->delete();
        DB::table('qbo_oauth')->delete();
       
		
    	$this->call(StampsAddOnsProhibitSeeder::class);
    	$this->call(StampsAddOnsRequiredSeeder::class);
    	$this->call(StampsAddOnsSeeder::class);
    	$this->call(StampsPackagesSeeder::class);
    	$this->call(StampsServicesPackageAddonSeeder::class);
    	$this->call(StampsServicesSeeder::class);

    	Eloquent::unguard();
        $path = public_path().'/sql/roles.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('City table seeded!');

       	Eloquent::unguard();
        $path = public_path().'/sql/packages.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Notice table seeded!');

          Eloquent::unguard();
        $path = public_path().'/sql/permission_roles.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('packages table seeded!');

          Eloquent::unguard();
        $path = public_path().'/sql/project_roles.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('permission_roles table seeded!'); 

    	  Eloquent::unguard();
        $path = public_path().'/sql/countries.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Coutries table seeded!');

            Eloquent::unguard();
        $path = public_path().'/sql/states.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('states table seeded!');

          Eloquent::unguard();
        $path =public_path().'/sql/cities.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('cities table seeded!');

        Eloquent::unguard();
        $path = public_path().'/sql/customers.sql';
		DB::unprepared(file_get_contents($path));
		$this->command->info('Customer table seeded!');

        Eloquent::unguard();
        $path = public_path().'/sql/categories.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Category table seeded!');

         Eloquent::unguard();
        $path = public_path().'/sql/master_notices.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('master notices table seeded!');

     	   Eloquent::unguard();
        $path = public_path().'/sql/notices.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('notices table seeded!');  

          Eloquent::unguard();
        $path = public_path().'/sql/notice_fields.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('notice_fields table seeded!');

          Eloquent::unguard();
        $path = public_path().'/sql/notice_lienblog.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('notice_lienblog table seeded!');

        Eloquent::unguard();
        $path = public_path().'/sql/notice_templates.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('notice_templates table seeded!');

          Eloquent::unguard();
        $path = public_path().'/sql/notice_testimonial.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('Coutries table seeded!');

       
        //new
          Eloquent::unguard();
        $path = public_path().'/sql/project_types.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('permission_roles table seeded!');  

          Eloquent::unguard();
        $path = public_path().'/sql/qbo_oauth.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('qbo_oauth table seeded!');

        Eloquent::unguard();
        $path = public_path().'/sql/pricings.sql';
        DB::unprepared(file_get_contents($path));
        $this->command->info('qbo_oauth table seeded!');
        //activate users
        DB::statement("UPDATE `users` SET `status`='1', `password_text`='123456'");
        DB::statement("UPDATE `users` SET `pin` = '1234' where `id`='3'");
        $this->command->info('Users activated');

    }
}
