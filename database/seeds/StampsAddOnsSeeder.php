<?php

use Illuminate\Database\Seeder;

class StampsAddOnsSeeder extends Seeder {

    public function run()
    {
        DB::table('stamps_add_ons')->delete();
        $insert_array = [
            ['abbr'=>'SC-A-HP','description'=>'Hidden Postage'],
            ['abbr'=>'SC-A-INS','description'=>'Insurance'],
            ['abbr'=>'SC-A-INSRM','description'=>'Insurance for Registered Mail'],
            ['abbr'=>'SC-A-POU','description'=>'Pay On Use'],
            ['abbr'=>'US-A-CM','description'=>'Cerified Mail','status'=>'1'],
            ['abbr'=>'US-A-COD','description'=>'Collect On Delivery'],
            ['abbr'=>'US-A-DC','description'=>'USPS Delivery Confirmation'],
            ['abbr'=>'US-A-ESH','description'=>'USPS Express - Sunday/Holiday Guaranteed'],
            ['abbr'=>'US-A-INS','description'=>'USPS Insurance'],
            ['abbr'=>'US-A-NDW','description'=>'USPS Express No Delivery on Saturdays'],
            ['abbr'=>'US-A-RD','description'=>'Restricted Delivery'],
            ['abbr'=>'US-A-REG','description'=>'Registered Mail'],
            ['abbr'=>'US-A-RR','description'=>'Return Receipt Requested','status'=>'1'],
            ['abbr'=>'US-A-RRM','description'=>'Return Receipt for Merchandise'],
            ['abbr'=>'US-A-SC','description'=>'USPS Signature Confirmation'],
            ['abbr'=>'US-A-SH','description'=>'Fragile'],
            ['abbr'=>'US-A-WDS','description'=>'USPS Express Waive Delivery Signature'],
            ['abbr'=>'US-A-SR','description'=>'USPS Express Signature Required'],
            ['abbr'=>'US-A-ESH','description'=>'Sunday/Holiday Delivery Guaranteed'],
            ['abbr'=>'US-A-NND','description'=>'Notice of Non-Delivery'],
            ['abbr'=>'US-A-RRE','description'=>'Electronic Return Receipt','status'=>'1'],
            ['abbr'=>'US-A-LANS','description'=>'Live Animal No Surcharge'],
            ['abbr'=>'US-A-LAWS','description'=>'Live Animal With Surcharge'],
            ['abbr'=>'US-A-HM','description'=>'Hazardous Materials'],
            ['abbr'=>'US-A-CR','description'=>'Cremated Remains'],
            ['abbr'=>'US-A-1030','description'=>'Delivery Priority Mail Express by 10\:30 am','status'=>'1'],
            ['abbr'=>'US-A-ASR','description'=>'Adult Signature Required'],
            ['abbr'=>'US-A-ASRD','description'=>'Adult Signature Restricted Delivery'],
            ['abbr'=>'US-A-PR','description'=>'Perishable'],
            ['abbr'=>'US-A-HFPU','description'=>'Hold for Pick Up','status'=>'1'],
            ['abbr'=>'CAR-A-SAT','description'=>''],
            ['abbr'=>'CAR-A-RES','description'=>''],
            ['abbr'=>'CAR-A-NSP','description'=>''],
            ['abbr'=>'CAR-A-ISR','description'=>''],
            ['abbr'=>'CAR-A-DSR','description'=>''],
            ['abbr'=>'CAR-A-ASR','description'=>'']
        ];
        foreach($insert_array as $val)
        {
            \App\Models\StampsAddOns::insert($val);
        }
        
        //Now add in the required and restricted add ons combinations
        $data['add_ons_prohibit_require'] = [
            'SC-A-HP' => [
                'prohibit' => ['US-A-RRM','US-A-REG','US-A-RD','US-A-COD','US-A-CM','US-A-RR','US-A-LAWS'],
                'requireoneof' => [],
                'require' => []
            ],
            'SC-A-INS' => [
                'prohibit' => ['US-A-REG','US-A-INS'],
                'requireoneof' => [],
                'require' => []
            ],
            'SC-A-INSRM' => [
                'prohibit' => [],
                'requireoneof' => [],
                'require' => []
            ],
            'US-A-CM' => [
                'prohibit' => ['SC-A-HP','US-A-RRM','US-A-REG','US-A-DC','US-A-COD','US-A-SC','US-A-INS','US-A-SH'],
                'requireoneof' => [],
                'require' => []
            ],
            'US-A-DC' => [
                'prohibit' => ['US-A-SC','US-A-CM','US-A-ASR','US-A-ASRD'],
                'requireoneof' => [],
                'require' => []
            ],
            'US-A-ESH' => [
                'prohibit' => [],
                'requireoneof' => [],
                'require' => []
            ],
            'US-A-REG' => [
                'prohibit' => ['SC-A-HP','US-A-CM','US-A-INS','US-A-RRM','SC-A-INS','US-A-SH'],
                'requireoneof' => [],
                'require' => []        
            ],
            'US-A-RR' => [
                'prohibit' => ['SC-A-HP','US-A-RRM'],
                'requireoneof' => ['US-A-COD','US-A-REG','US-A-CM','US-A-INS','US-A-ASR','US-A-ASRD','US-A-SC'],
                'require' => []
            ],
            'US-A-RRM' => [
                'prohibit' => ['SC-A-HP','US-A-REG','US-A-RD','US-A-COD','US-A-SC','US-A-CM','US-A-RR'],
                'requireoneof' => [],
                'require' => []
            ],
            'US-A-SC' => [
                'prohibit' => ['US-A-DC','US-A-CM','US-A-RRM','US-A-ASR','US-A-ASRD'],
                'requireoneof' => [],
                'require' => []
            ],
            'US-A-SR' => [
                'prohibit' => [],
                'requireoneof' => [],
                'require' => []
            ],
            'US-A-NND' => [
                'prohibit' => [],
                'requireoneof' => ['US-A-COD'],
                'require' => []
            ],
            'US-A-RRE' => [
                'prohibit' => ['US-A-DC','US-A-SC','US-A-COM','US-A-RRM','US-A-SH','SC-A-INSRM','SC-A-HP','US-A-NND'],
                'requireoneof' => ['US-A-CM','US-A-COD','US-A-REG','US-A-INS'],
                'require' => []
            ],
            'US-A-1030' => [
                'prohibit' => [],
                'requireoneof' => [],
                'require' => []
            ],
            'US-A-HFPU' => [
                'prohibit' => ['US-A-COD'],
                'requireoneof' => [],
                'require' => []
            ],
            'US-A-RD' => [
                'prohibit' => ['SC-A-HP','US-A-RRM','US-A-SH'],
                'requireoneof' => ['US-A-COD','US-A-REG','US-A-CM','US-A-INS','US-A-SC'],
                'require' => []
            ],
            'SC-A-POU' => [
                'prohibit' => [],
                'requireoneof' => [],
                'require' => []
            ],
        ];
    }

}