<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkOrderNotesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'work_order_notes';

    /**
     * Run the migrations.
     * @table work_order_notes
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('work_order_id');
            $table->unsignedInteger('customer_id');
            $table->text('note')->nullable()->default(null);
            $table->string('email')->nullable()->default(null);
            $table->tinyInteger('visibility')->default('0')->comment('1 - Public | 2 - Private');
            $table->integer('user_id')->nullable()->default(null);

            $table->index(["customer_id"], 'work_order_notes_customer_id_foreign');

            $table->index(["work_order_id"], 'work_order_notes_work_order_id_foreign');

            $table->index(["user_id"], 'user_id');
            $table->nullableTimestamps();


            $table->foreign('work_order_id', 'work_order_notes_work_order_id_foreign')
                ->references('id')->on('work_orders')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('customer_id', 'work_order_notes_customer_id_foreign')
                ->references('id')->on('customers')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
