<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNoticeTemplatesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'notice_templates';

    /**
     * Run the migrations.
     * @table notice_templates
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('state_id');
            $table->tinyInteger('is_parent_work_order')->default('0');
            $table->unsignedInteger('notice_id');
            $table->string('name')->comment('Template name');
            $table->unsignedInteger('no_of_days')->default('0');
            $table->text('content')->comment('Template Data');

            $table->index(["notice_id"], 'notice_templates_notice_id_foreign');

            $table->index(["state_id"], 'notice_templates_state_id_foreign');
            $table->nullableTimestamps();


            $table->foreign('notice_id', 'notice_templates_notice_id_foreign')
                ->references('id')->on('notices')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
