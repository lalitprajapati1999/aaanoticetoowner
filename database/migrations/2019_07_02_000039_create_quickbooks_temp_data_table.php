<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuickbooksTempDataTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'quickbooks_temp_data';

    /**
     * Run the migrations.
     * @table quickbooks_temp_data
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('quickbooks_data');
            $table->string('quickbooks_task', 100);
            $table->string('scope', 100);
            $table->unsignedInteger('customer_id');

            $table->index(["customer_id"], 'quickbooks_temp_data_customer_id_index');


            $table->foreign('customer_id', 'quickbooks_temp_data_customer_id_index')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
