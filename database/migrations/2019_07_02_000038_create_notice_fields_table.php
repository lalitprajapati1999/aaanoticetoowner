<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNoticeFieldsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'notice_fields';

    /**
     * Run the migrations.
     * @table notice_fields
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('state_id');
            $table->unsignedInteger('notice_id');
            $table->string('section')->nullable()->default(null);
            $table->string('name')->comment('name attribute of input or label');
            $table->tinyInteger('type')->default('0')->comment('0 - label|1 - text|2 - textarea|3 - select|4 - checkbox|5 - radio|6 - datepicker');
            $table->string('validation')->nullable()->default(null)->comment('Custom Validation related to format');
            $table->tinyInteger('is_required')->default('0')->comment('0 - Not required | 1 - Required');
            $table->integer('sort_order')->default('0')->comment('Sequence order of the field on form page.');

            $table->index(["state_id"], 'notice_fields_state_id_foreign');

            $table->index(["notice_id"], 'notice_fields_notice_id_foreign');
            $table->nullableTimestamps();


            $table->foreign('notice_id', 'notice_fields_notice_id_foreign')
                ->references('id')->on('notices')
                ->onDelete('cascade')
                ->onUpdate('no action');
            $table->enum('status', ['0', '1'])->default('1');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
