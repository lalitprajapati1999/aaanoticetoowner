<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'subscriptions';

    /**
     * Run the migrations.
     * @table subscriptions
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->tinyInteger('type')->comment('1 - Price | 2 - Duration');
            $table->tinyInteger('duration_type')->default('0')->comment('0 - Monthly | 1 - Yearly');
            $table->string('charge');
            $table->text('description')->nullable()->default(null);
            $table->tinyInteger('status')->default('0')->comment('0 - Inactive | 1 - Active');
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
