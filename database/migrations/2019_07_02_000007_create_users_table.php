<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'users';

    /**
     * Run the migrations.
     * @table users
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('email', 250);
            $table->string('password');
            $table->string('password_text')->nullable()->default(null);
            $table->string('contact_number')->nullable()->default(null);
            $table->string('emergency_contact')->nullable()->default(null);
            $table->string('pin')->nullable()->default(null)->comment('Mailing Pin');
            $table->tinyInteger('allow_secondary_network')->default('0')->comment('0 - Deny | 1 - Allow');
            $table->tinyInteger('status')->default('0')->comment('0 - Disable, 1 - Enable');
            $table->rememberToken();
            $table->integer('qbo_customer_id')->nullable()->default(null);

            $table->unique(["email"], 'users_email_unique');
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
