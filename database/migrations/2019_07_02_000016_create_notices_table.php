<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNoticesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'notices';

    /**
     * Run the migrations.
     * @table notices
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            /*$table->unsignedInteger('master_notice_id');
            $table->index(["master_notice_id"], 'master_notices_master_notice_id_foreign');
            $table->foreign('master_notice_id', 'master_notices_master_notice_id_foreign')
                ->references('id')->on('master_notices')
                ->onDelete('cascade')
                ->onUpdate('no action');*/
           
            $table->tinyInteger('type')->default('0')->comment('1 - Hard Document 2 - Soft Document');
            $table->tinyInteger('status')->default('0')->comment('0 - Inactive 1 - Active');
            $table->integer('state_id')->nullable()->default(null);
            $table->integer('allow_cyo')->default('0');
            $table->tinyInteger('allow_deadline_calculator')->default('0')->comment('0 - disable 1 - enable');
            $table->string('short_name', 10)->nullable()->default(null);
            $table->integer('notice_sequence');
            $table->tinyInteger('is_claim_of_lien')->default('0');
            $table->tinyInteger('owners_html_in_pdf')->default('0');
            $table->tinyInteger('is_bond_of_claim')->default('0');
            $table->tinyInteger('is_rescind')->nullable()->default('0');
            $table->unsignedInteger('master_notice_id');
            $table->index(["master_notice_id"], 'master_notices_master_notice_id_foreign');
            $table->foreign('master_notice_id', 'master_notices_master_notice_id_foreign')
                ->references('id')->on('master_notices')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
