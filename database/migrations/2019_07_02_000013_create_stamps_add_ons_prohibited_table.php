<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStampsAddOnsProhibitedTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'stamps_add_ons_prohibited';

    /**
     * Run the migrations.
     * @table stamps_add_ons_prohibited
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('add_on');
            $table->unsignedInteger('prohibited');

            $table->index(["prohibited"], 'stamps_add_ons_prohibited_prohibited_foreign');

            $table->index(["add_on"], 'stamps_add_ons_prohibited_add_on_foreign');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
