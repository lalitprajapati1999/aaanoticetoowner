<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSecondaryDocumentRecipientsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'secondary_document_recipients';

    /**
     * Run the migrations.
     * @table secondary_document_recipients
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('secondary_document_id');
            $table->unsignedInteger('category_id')->nullable()->default(null);
            $table->string('name');
            $table->string('contact')->nullable()->default(null);
            $table->string('mobile');
            $table->text('address')->nullable()->default(null);
            $table->unsignedInteger('city_id')->nullable()->default(null);
            $table->unsignedInteger('state_id')->nullable()->default(null);
            $table->string('zip')->nullable()->default(null);
            $table->unsignedInteger('country_id')->nullable()->default(null);
            $table->string('fax')->nullable()->default(null);
            $table->string('email')->nullable()->default(null);
            $table->text('attn')->nullable()->default(null);

            $table->index(["secondary_document_id"], 'secondary_document_recipients_secondary_document_id_foreign');

            $table->index(["city_id"], 'secondary_document_recipients_city_id_foreign');

            $table->index(["category_id"], 'secondary_document_recipients_category_id_foreign');

            $table->index(["country_id"], 'secondary_document_recipients_country_id_foreign');

            $table->index(["state_id"], 'secondary_document_recipients_state_id_foreign');
            $table->nullableTimestamps();


            $table->foreign('secondary_document_id', 'secondary_document_recipients_secondary_document_id_foreign')
                ->references('id')->on('secondary_documents')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('category_id', 'secondary_document_recipients_category_id_foreign')
                ->references('id')->on('categories')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('city_id', 'secondary_document_recipients_city_id_foreign')
                ->references('id')->on('cities')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('state_id', 'secondary_document_recipients_state_id_foreign')
                ->references('id')->on('states')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
