<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCyoStampsLabelsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'cyo__stamps_labels';

    /**
     * Run the migrations.
     * @table cyo__stamps_labels
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('tracking_number');
            $table->string('StampsTxID');
            $table->text('URL');
            $table->date('ShipDate');
            $table->date('DeliveryDate');
            $table->double('Amount');
            $table->unsignedInteger('recipient_id');
            $table->enum('type_of_label', ['firm mail', 'next day delivery', 'stamps'])->default('firm mail');
            /*$table->enum('type_of_mailing', ['certified mail', 'priority mail'])->nullable()->default(null);*/
            $table->string('type_of_mailing');
            $table->enum('generated_label', ['yes', 'no'])->default('no');
            $table->unsignedInteger('rates_data_id')->nullable()->default(null);
            $table->string('add_ons');
            $table->integer('service_type');
            $table->integer('package_type');

            $table->index(["rates_data_id"], 'cyo__stamps_labels_rates_data_id_foreign');

            $table->index(["package_type"], 'cyo__stamps_labels_package_type_index');

            $table->index(["service_type"], 'cyo__stamps_labels_service_type_index');

            $table->index(["recipient_id"], 'cyo__stamps_labels_recipient_id_index');
            $table->nullableTimestamps();


            $table->foreign('recipient_id', 'cyo__stamps_labels_recipient_id_index')
                ->references('id')->on('cyo__work_order__recipients')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('rates_data_id', 'cyo__stamps_labels_rates_data_id_foreign')
                ->references('id')->on('cyo_stamps_rates_data')
                ->onDelete('restrict')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
