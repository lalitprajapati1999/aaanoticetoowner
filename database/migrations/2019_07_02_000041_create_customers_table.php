<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'customers';

    /**
     * Run the migrations.
     * @table customers
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('account_manager_id')->nullable()->default(null);
            $table->string('company_name');
            $table->string('contact_person');
            $table->string('mailing_address')->nullable()->default(null);
            $table->unsignedInteger('mailing_city_id')->nullable()->default(null);
            $table->unsignedInteger('mailing_state_id')->nullable()->default(null);
            $table->string('mailing_zip')->nullable()->default(null);
            $table->string('physical_address')->nullable()->default(null);
            $table->unsignedInteger('physical_city_id')->nullable()->default(null);
            $table->unsignedInteger('physical_state_id')->nullable()->default(null);
            $table->string('physical_zip')->nullable()->default(null);
            $table->text('hear_about')->nullable()->default(null);
            $table->string('office_number')->nullable()->default(null);
            $table->string('fax_number')->nullable()->default(null);
            $table->string('mobile_number')->nullable()->default(null);
            $table->text('company_email')->nullable()->default(null)->comment('Comma seprated multiple email');
            $table->unsignedInteger('no_of_offices')->nullable()->default(null);
            $table->tinyInteger('status')->default('0')->comment('0 - Inactive, 1 - Active');
            $table->integer('qbo_customer_id');

            $table->index(["physical_city_id"], 'customers_physical_city_id_foreign');

            $table->index(["mailing_city_id"], 'customers_mailing_city_id_foreign');

            $table->index(["user_id"], 'customers_user_id_foreign');

            $table->index(["mailing_state_id"], 'customers_mailing_state_id_foreign');

            $table->index(["physical_state_id"], 'customers_physical_state_id_foreign');
            $table->nullableTimestamps();


            $table->foreign('mailing_city_id', 'customers_mailing_city_id_foreign')
                ->references('id')->on('cities')
                ->onDelete('restrict')
                ->onUpdate('no action');

            $table->foreign('mailing_state_id', 'customers_mailing_state_id_foreign')
                ->references('id')->on('states')
                ->onDelete('restrict')
                ->onUpdate('no action');

            $table->foreign('user_id', 'customers_user_id_foreign')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
