<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerRepresentativesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'customer_representatives';

    /**
     * Run the migrations.
     * @table customer_representatives
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('customer_id');
            $table->string('contact_person')->nullable()->default(null);
            $table->string('branch_name')->nullable()->default(null)->comment('Branch Name');
            $table->string('email')->nullable()->default(null);
            $table->string('mobile_number')->nullable()->default(null);

            $table->index(["customer_id"], 'customer_representatives_customer_id_foreign');
            $table->nullableTimestamps();


            $table->foreign('customer_id', 'customer_representatives_customer_id_foreign')
                ->references('id')->on('customers')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
