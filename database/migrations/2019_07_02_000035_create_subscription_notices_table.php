<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionNoticesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'subscription_notices';

    /**
     * Run the migrations.
     * @table subscription_notices
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('subscription_id');
            $table->unsignedInteger('notice_id');
            $table->string('limit')->nullable()->default(null);
            $table->string('charge')->nullable()->default(null);
            $table->string('post_subscription_charge')->nullable()->default(null);

            $table->index(["subscription_id"], 'subscription_notices_subscription_id_foreign');

            $table->index(["notice_id"], 'subscription_notices_notice_id_foreign');
            $table->nullableTimestamps();


            $table->foreign('subscription_id', 'subscription_notices_subscription_id_foreign')
                ->references('id')->on('subscriptions')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
