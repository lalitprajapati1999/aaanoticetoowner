<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerSubscriptionsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'customer_subscriptions';

    /**
     * Run the migrations.
     * @table customer_subscriptions
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('customer_id');
            $table->unsignedInteger('package_id');
            $table->unsignedInteger('charges');
            $table->tinyInteger('type')->nullable()->default(null)->comment('1=>credit card, 2=>Cheque');
            $table->date('start_date')->nullable()->default(null);
            $table->date('end_date')->nullable()->default(null);
            $table->string('cheque_no', 100)->nullable()->default(null);
            $table->text('bank_details')->nullable()->default(null);
            $table->tinyInteger('status')->nullable()->default(null)->comment('1=>active, 0=>inactive');

            $table->index(["customer_id"], 'customer_subscriptions_customer_id_foreign');

            $table->index(["package_id"], 'customer_subscriptions_package_id_foreign');
            $table->softDeletes();
            $table->timestamps();


            $table->foreign('customer_id', 'customer_subscriptions_customer_id_foreign')
                ->references('id')->on('customers')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('package_id', 'customer_subscriptions_package_id_foreign')
                ->references('id')->on('packages')
                ->onDelete('restrict')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
