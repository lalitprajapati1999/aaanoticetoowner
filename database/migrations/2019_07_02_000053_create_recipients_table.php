<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipientsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'recipients';

    /**
     * Run the migrations.
     * @table recipients
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('work_order_id');
            $table->unsignedInteger('category_id')->nullable()->default(null);
            $table->string('name');
            $table->string('contact')->nullable()->default(null);
            $table->string('mobile')->nullable()->default(null);
            $table->text('address')->nullable()->default(null);
            $table->unsignedInteger('city_id')->nullable()->default(null);
            $table->unsignedInteger('state_id')->nullable()->default(null);
            $table->string('zip')->nullable()->default(null);
            $table->unsignedInteger('country_id')->nullable()->default(null);
            $table->string('fax')->nullable()->default(null);
            $table->string('email')->nullable()->default(null);
            $table->integer('contact_id');
            $table->text('attn')->nullable()->default(null);
            $table->integer('parent_work_order')->nullable()->default(null);
            $table->index(["city_id"], 'recipients_city_id_foreign');

            $table->index(["category_id"], 'recipients_category_id_foreign');

            $table->index(["state_id"], 'recipients_state_id_foreign');

            $table->index(["work_order_id"], 'recipients_work_order_id_foreign');

            $table->index(["country_id"], 'recipients_country_id_foreign');
            $table->nullableTimestamps();


            $table->foreign('work_order_id', 'recipients_work_order_id_foreign')
                ->references('id')->on('work_orders')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('category_id', 'recipients_category_id_foreign')
                ->references('id')->on('categories')
                ->onDelete('restrict')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
