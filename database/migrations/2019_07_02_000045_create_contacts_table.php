<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'contacts';

    /**
     * Run the migrations.
     * @table contacts
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('customer_id');
            $table->string('company_name');
            $table->text('company_address')->nullable()->default(null);
            $table->text('mailing_address')->nullable()->default(null);
            $table->unsignedInteger('city_id')->nullable()->default(null);
            $table->unsignedInteger('state_id')->nullable()->default(null);
            $table->string('zip')->nullable()->default(null);
            $table->text('phone')->nullable()->default(null)->comment('Comma seprated Business Phone');
            $table->string('email', 250)->nullable()->default(null)->comment('Business Email');
            $table->unsignedInteger('country_id')->nullable()->default(null);
            $table->text('attn')->nullable()->default(null);

            $table->index(["city_id"], 'contacts_city_id_foreign');

            $table->index(["state_id"], 'contacts_state_id_foreign');

            $table->index(["country_id"], 'contacts_country_id_foreign');

            $table->index(["customer_id"], 'contacts_customer_id_foreign');
            $table->nullableTimestamps();


            $table->foreign('city_id', 'contacts_city_id_foreign')
                ->references('id')->on('cities')
                ->onDelete('restrict')
                ->onUpdate('no action');

            $table->foreign('state_id', 'contacts_state_id_foreign')
                ->references('id')->on('states')
                ->onDelete('restrict')
                ->onUpdate('no action');

            $table->foreign('customer_id', 'contacts_customer_id_foreign')
                ->references('id')->on('customers')
                ->onDelete('cascade')
                ->onUpdate('no action');
            $table->enum('added_form', ['address_book', 'work_order'])->default('work_order');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
