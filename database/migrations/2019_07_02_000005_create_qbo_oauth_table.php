<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQboOauthTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'qbo_oauth';

    /**
     * Run the migrations.
     * @table qbo_oauth
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('access_token');
            $table->string('refresh_token');
            $table->string('company_id');
            $table->unsignedInteger('customer_id');
            $table->timestamp('token_expires_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->timestamp('refresh_token_expires_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
            $table->string('scope', 150)->nullable()->default(null)->comment('payment or accounting scope');

            $table->index(["customer_id"], 'qbo_oauth_customer_id_foreign');
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
