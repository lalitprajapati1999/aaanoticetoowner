<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactUsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'contact_us';

    /**
     * Run the migrations.
     * @table contact_us
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('email', 250);
            $table->text('query')->nullable()->default(null);
            $table->tinyInteger('status')->default('1')->comment('0 - Disable, 1 - Enable');
            $table->unsignedInteger('customer_id')->nullable()->default(null);
            $table->string('company_name')->nullable()->default(null);
            $table->text('phone')->nullable()->default(null);
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
