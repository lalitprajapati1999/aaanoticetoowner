<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSecondaryDocumentsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'secondary_documents';

    /**
     * Run the migrations.
     * @table secondary_documents
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('customer_id');
            $table->date('date')->comment('Date');
            $table->string('customer')->nullable()->default(null)->comment('Your Customer');
            $table->string('work_order_no')->nullable()->default('0')->comment('Work Order Number');
            $table->text('address')->nullable()->default(null)->comment('Specific Job Address');
            $table->string('project_name')->nullable()->default(null);
            $table->unsignedInteger('city_id')->nullable()->default(null);
            $table->unsignedInteger('state_id')->nullable()->default(null);
            $table->string('zip')->nullable()->default(null);
            $table->unsignedInteger('country_id')->nullable()->default(null);
            $table->float('amount')->nullable()->default(null)->comment('Amount/Value of Payoff');
            $table->string('due_date', 50)->nullable()->default(null)->comment('Due date amount to be paid prior to');
            $table->string('first_name')->nullable()->default(null)->comment('Your First Name');
            $table->string('signature')->nullable()->default(null)->comment('Authorise Signature');
            $table->string('title')->nullable()->default(null);
            $table->text('comment')->nullable()->default(null)->comment('Comment');
            $table->tinyInteger('type')->default('0')->comment('0 - Partial | 1 - Final');
            $table->string('file_name')->nullable()->default(null)->comment('Created document name');
            $table->string('amount_in_text')->nullable()->default(null);
            $table->longText('legal_description')->nullable()->default(null);
            $table->string('service_labour_furnished')->nullable()->default(null);
            $table->tinyInteger('status')->default('0');
            $table->string('contracted_by')->nullable()->default(null);
            $table->string('project_owner')->nullable()->default(null);
            
            $table->tinyInteger('notary_seal')->default('0');

            $table->index(["customer_id"], 'secondary_documents_customer_id_foreign');

            $table->index(["state_id"], 'secondary_documents_state_id_foreign');

            $table->index(["city_id"], 'secondary_documents_city_id_foreign');

            $table->index(["country_id"], 'secondary_documents_country_id_foreign');
            $table->nullableTimestamps();


            $table->foreign('customer_id', 'secondary_documents_customer_id_foreign')
                ->references('id')->on('customers')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('city_id', 'secondary_documents_city_id_foreign')
                ->references('id')->on('cities')
                ->onDelete('restrict')
                ->onUpdate('no action');

            $table->foreign('state_id', 'secondary_documents_state_id_foreign')
                ->references('id')->on('states')
                ->onDelete('restrict')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
