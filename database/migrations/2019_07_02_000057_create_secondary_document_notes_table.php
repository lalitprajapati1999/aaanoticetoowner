<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSecondaryDocumentNotesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'secondary_document_notes';

    /**
     * Run the migrations.
     * @table secondary_document_notes
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('secondary_document_id');
            $table->unsignedInteger('customer_id');
            $table->text('note')->nullable()->default(null);
            $table->string('email')->nullable()->default(null);
            $table->tinyInteger('visibility')->default('0')->comment('1 - Public | 2 - Private');
            $table->integer('user_id')->nullable()->default(null);

            $table->index(["secondary_document_id"], 'secondary_document_notes_secondary_document_id_foreign');

            $table->index(["customer_id"], 'secondary_document_notes_customer_id_foreign');

            $table->index(["user_id"], 'user_id');
            $table->nullableTimestamps();


            $table->foreign('secondary_document_id', 'secondary_document_notes_secondary_document_id_foreign')
                ->references('id')->on('secondary_documents')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('customer_id', 'secondary_document_notes_customer_id_foreign')
                ->references('id')->on('customers')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
