<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactDetailsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'contact_details';

    /**
     * Run the migrations.
     * @table contact_details
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('contact_id');
            $table->tinyInteger('type')->default('0')->comment('0 - contact_person');
            $table->string('name')->nullable()->default(null);
            $table->text('email')->nullable()->default(null)->comment('Comma separted emails');
            $table->text('mobile')->nullable()->default(null)->comment('Comma separted mobile');
            $table->string('fax')->nullable()->default(null);
            $table->string('designation')->nullable()->default(null);

            $table->index(["contact_id"], 'contact_details_contact_id_foreign');
            $table->nullableTimestamps();


            $table->foreign('contact_id', 'contact_details_contact_id_foreign')
                ->references('id')->on('contacts')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
