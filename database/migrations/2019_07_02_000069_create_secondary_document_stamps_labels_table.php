<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSecondaryDocumentStampsLabelsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'secondary_document_stamps_labels'; 

    /**
     * Run the migrations.
     * @table secondary_document_stamps_labels
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('tracking_number');
            $table->string('StampsTxID');
            $table->text('URL');
            $table->date('ShipDate');
            $table->date('DeliveryDate');
            $table->double('Amount');
            $table->unsignedInteger('recipient_id');

            $table->index(["recipient_id"], 'secondary_document_stamps_labels_recipient_id_index');
            $table->nullableTimestamps();


            $table->foreign('recipient_id', 'secondary_document_stamps_labels_recipient_id_index')
                ->references('id')->on('secondary_document_recipients')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
