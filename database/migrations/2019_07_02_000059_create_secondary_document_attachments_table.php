<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSecondaryDocumentAttachmentsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'secondary_document_attachments';

    /**
     * Run the migrations.
     * @table secondary_document_attachments
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('secondary_document_id')->nullable()->default(null);
            $table->tinyInteger('type')->default('0')->comment('1 - bond | 2 - Notice to commencement | 3 - Permit | 4 - Contrat | 5 - Invoice | 6 - Final Release | 7 - Partial Release | 8 - Misc. Document | 9 - Signed Document');
            $table->string('title')->comment('Title');
            $table->string('file_name')->comment('File Name');
            $table->tinyInteger('visibility')->default('0');
            $table->string('original_file_name');

            $table->index(["secondary_document_id"], 'secondary_document_attachments_secondary_document_id_foreign');
            $table->nullableTimestamps();


            $table->foreign('secondary_document_id', 'secondary_document_attachments_secondary_document_id_foreign')
                ->references('id')->on('secondary_documents')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
