<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOauth2InvoiceResponseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oauth2_invoice_response', function (Blueprint $table) {
            $table->increments('id');            
            $table->integer('invoice_id');
            $table->text('bill_addr');
            $table->text('sales_term_ref');
            $table->date('tranx_date');
            $table->date('due_date');
            $table->text('invoice_status');
            $table->text('job_ref_no');
            $table->text('notice_type');
            $table->text('job_location');
            $table->text('total_amt');
            $table->text('balance');
            $table->text('doc_number');
            $table->text('other_details');
            $table->integer('qbo_user_id');
                        
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oauth2_invoice_response');
    }
}
