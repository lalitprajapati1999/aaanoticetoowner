<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStampsServicePackageAddonTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'stamps_service_package_addon';

    /**
     * Run the migrations.
     * @table stamps_service_package_addon
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('package');
            $table->unsignedInteger('service_type');
            $table->unsignedInteger('add_on');

            $table->index(["service_type"], 'stamps_service_package_addon_service_type_foreign');

            $table->index(["add_on"], 'stamps_service_package_addon_add_on_foreign');

            $table->index(["package"], 'stamps_service_package_addon_package_foreign');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
