<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCyoWorkOrdersTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'cyo__work_orders';

    /**
     * Run the migrations.
     * @table cyo__work_orders
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('parent_id')->nullable()->default(null);
            $table->string('order_no')->comment('random string generated');
            $table->unsignedInteger('customer_id');
            $table->unsignedInteger('account_manager_id')->nullable()->default(null);
            $table->unsignedInteger('user_id')->nullable()->default(null)->comment('Account Manager Id');
            $table->unsignedInteger('notice_id');
            $table->tinyInteger('status')->default('0')->comment('0 - draft |1 - request | 2 - processing | 3 - recording | 4 - pending_signature | 5 - mailed | 6 - completed | 7 - cancelled |8-restricted');
            $table->string('discontinue_reminder')->default('0');
            $table->timestamp('completed_at')->nullable()->default(null);
            $table->timestamp('cancelled_at')->nullable()->default(null);
            $table->enum('invoice_generated', ['Yes', 'No'])->default('No');
            $table->enum('rush_hour_charges', ['yes', 'no'])->default('no');
            $table->integer('project_address_count')->default('1');
            $table->string('file_name')->nullable()->default(null)->comment('Created document name');
            $table->decimal('rush_hour_amount', 10,2);
            $table->enum('print_firm_mail', ['Yes', 'No'])->default('No');
            $table->tinyInteger('is_rescind')->nullable()->default('0');

            $table->index(["parent_id"], 'cyo__work_orders_parent_id_foreign');

            $table->index(["notice_id"], 'cyo__work_orders_notice_id_foreign');

            $table->index(["customer_id"], 'cyo__work_orders_customer_id_foreign');

            $table->index(["user_id"], 'cyo__work_orders_user_id_foreign');
            $table->nullableTimestamps();


            $table->foreign('customer_id', 'cyo__work_orders_customer_id_foreign')
                ->references('id')->on('customers')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('user_id', 'cyo__work_orders_user_id_foreign')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('notice_id', 'cyo__work_orders_notice_id_foreign')
                ->references('id')->on('notices')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
