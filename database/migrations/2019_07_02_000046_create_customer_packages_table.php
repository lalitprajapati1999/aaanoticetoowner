<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerPackagesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'customer_packages';

    /**
     * Run the migrations.
     * @table customer_packages
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('package_id');
            $table->unsignedInteger('notice_id')->nullable()->default(null);
            $table->unsignedInteger('state_id')->nullable()->default(null);
            $table->integer('lower_limit')->nullable()->default(null);
            $table->integer('upper_limit')->nullable()->default(null);
            $table->string('charge');
            $table->unsignedInteger('customer_id');
            $table->string('cancelation_charge')->nullable()->default(null);
            $table->text('description')->nullable()->default(null);
            $table->tinyInteger('status')->nullable()->default(null);

            $table->index(["state_id"], 'customer_packages_state_id_foreign');

            $table->index(["package_id"], 'customer_packages_package_id_foreign');

            $table->index(["notice_id"], 'customer_packages_notice_id_foreign');

            $table->index(["customer_id"], 'customer_packages_customer_id_foreign');
            $table->timestamps();


            $table->foreign('package_id', 'customer_packages_package_id_foreign')
                ->references('id')->on('packages')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('notice_id', 'customer_packages_notice_id_foreign')
                ->references('id')->on('notices')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('state_id', 'customer_packages_state_id_foreign')
                ->references('id')->on('states')
                ->onDelete('restrict')
                ->onUpdate('no action');

            $table->foreign('customer_id', 'customer_packages_customer_id_foreign')
                ->references('id')->on('customers')
                ->onDelete('cascade')
                ->onUpdate('no action');
            $table->string('additional_address')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
