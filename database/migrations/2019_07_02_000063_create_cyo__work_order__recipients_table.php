<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCyoWorkOrderRecipientsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'cyo__work_order__recipients';

    /**
     * Run the migrations.
     * @table cyo__work_order__recipients
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('work_order_id');
            $table->unsignedInteger('category_id')->nullable()->default(null);
            $table->string('name');
            $table->string('contact')->nullable()->default(null);
            $table->string('mobile')->nullable()->default(null);
            $table->text('address')->nullable()->default(null);
            $table->unsignedInteger('city_id')->nullable()->default(null);
            $table->unsignedInteger('state_id')->nullable()->default(null);
            $table->string('zip')->nullable()->default(null);
            $table->string('fax')->nullable()->default(null);
            $table->string('email')->nullable()->default(null);
            $table->integer('contact_id');
            $table->text('attn')->nullable()->default(null);
            $table->integer('parent_work_order')->nullable()->default(null);;
            $table->index(["state_id"], 'cyo__work_order__recipients_state_id_foreign');

            $table->index(["city_id"], 'cyo__work_order__recipients_city_id_foreign');

            $table->index(["work_order_id"], 'cyo__work_order__recipients_work_order_id_foreign');

            $table->index(["category_id"], 'cyo__work_order__recipients_category_id_foreign');
            $table->nullableTimestamps();


            $table->foreign('work_order_id', 'cyo__work_order__recipients_work_order_id_foreign')
                ->references('id')->on('cyo__work_orders')
                ->onDelete('cascade')
                ->onUpdate('no action');

            $table->foreign('category_id', 'cyo__work_order__recipients_category_id_foreign')
                ->references('id')->on('categories')
                ->onDelete('restrict')
                ->onUpdate('no action');

            $table->foreign('city_id', 'cyo__work_order__recipients_city_id_foreign')
                ->references('id')->on('cities')
                ->onDelete('restrict')
                ->onUpdate('no action');

            $table->foreign('state_id', 'cyo__work_order__recipients_state_id_foreign')
                ->references('id')->on('states')
                ->onDelete('restrict')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
