<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerAgentsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'customer_agents';

    /**
     * Run the migrations.
     * @table customer_agents
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('customer_id');
            $table->string('title')->nullable()->default(null)->comment('Registered Agent Title');
            $table->string('first_name')->nullable()->default(null)->comment('Registered Agent First Name');
            $table->string('last_name')->nullable()->default(null)->comment('Registered Agent Last Name');

            $table->index(["customer_id"], 'customer_agents_customer_id_foreign');
            $table->nullableTimestamps();


            $table->foreign('customer_id', 'customer_agents_customer_id_foreign')
                ->references('id')->on('customers')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
