<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStampsLabelTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'stamps_label';

    /**
     * Run the migrations.
     * @table stamps_label
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('tracking_number')->nullable()->default(null);
            $table->string('StampsTxID')->nullable()->default(null);
            $table->text('URL')->nullable()->default(null);
            $table->date('ShipDate')->nullable()->default(null);
            $table->date('DeliveryDate')->nullable()->default(null);
            $table->double('Amount')->nullable()->default(null);
            $table->unsignedInteger('recipient_id');
            $table->enum('type_of_label', ['firm mail', 'next day delivery', 'stamps'])->default('firm mail');
           /* $table->enum('type_of_mailing', ['Certified Mail','Priority Mail','Return Receipt Requested','Electronic Return Receipt'])->nullable()->default(null);*/
            $table->string('type_of_mailing');
            $table->enum('generated_label', ['yes', 'no'])->default('no');
            $table->unsignedInteger('rates_data_id')->nullable()->default(null);
            $table->string('add_ons');
            $table->integer('service_type');
            $table->integer('package_type');
            $table->double('base_amount_charges');

            $table->index(["service_type"], 'stamps_label_service_type_index');

            $table->index(["recipient_id"], 'stamps_label_recipient_id_index');

            $table->index(["rates_data_id"], 'stamps_label_rates_data_id_foreign');

            $table->index(["package_type"], 'stamps_label_package_type_index');
            $table->nullableTimestamps();


            $table->foreign('recipient_id', 'stamps_label_recipient_id_index')
                ->references('id')->on('recipients')
                ->onDelete('restrict')
                ->onUpdate('no action');

            $table->foreign('rates_data_id', 'stamps_label_rates_data_id_foreign')
                ->references('id')->on('stamps_rates_data')
                ->onDelete('restrict')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
