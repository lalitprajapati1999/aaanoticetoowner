<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePricingsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'pricings';

    /**
     * Run the migrations.
     * @table pricings
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('package_id');
            $table->unsignedInteger('notice_id')->nullable()->default(null);
            $table->unsignedInteger('state_id')->nullable()->default(null);
            $table->integer('lower_limit')->nullable()->default(null);
            $table->integer('upper_limit')->nullable()->default(null);
            $table->string('charge');
            $table->string('cancelation_charge')->nullable()->default(null);
            $table->text('description')->nullable()->default(null);
            $table->tinyInteger('status')->nullable()->default(null);
            $table->integer('pricing_copied_id');

            $table->index(["package_id"], 'pricings_package_id_foreign');

            $table->index(["state_id"], 'pricings_state_id_foreign');

            $table->index(["notice_id"], 'pricings_notice_id_foreign');

            $table->timestamps();
            $table->string('additional_address')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
