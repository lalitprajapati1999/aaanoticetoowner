<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCyoWorkOrderAttachmentsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'cyo__work_order__attachments';

    /**
     * Run the migrations.
     * @table cyo__work_order__attachments
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('work_order_id')->nullable()->default(null);
            $table->tinyInteger('type')->default('0')->comment('1 - bond | 2 - Notice to commencement | 3 - Permit | 4 - Contrat | 5 - Invoice | 6 - Final Release | 7 - Partial Release | 8 - Misc. Document | 9 - Signed Document');
            $table->string('title');
            $table->string('file_name');
            $table->tinyInteger('visibility')->default('0');
            $table->string('original_file_name');

            $table->index(["work_order_id"], 'cyo__work_order__attachments_work_order_id_foreign');
            $table->nullableTimestamps();


            $table->foreign('work_order_id', 'cyo__work_order__attachments_work_order_id_foreign')
                ->references('id')->on('cyo__work_orders')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
