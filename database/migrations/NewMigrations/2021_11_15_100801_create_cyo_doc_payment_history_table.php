<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCyoDocPaymentHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public $tableName = 'cyo_doc_payment_historys';
    public function up()
    {
        Schema::create('cyo_doc_payment_historys', function (Blueprint $table) {
            $table->increments('id');         
            $table->string('work_order_id')->nullable()->default(null);           
            $table->string('customer_id')->nullable()->default(null);
            $table->string('package_id')->nullable()->default(null);            
            $table->string('charges')->nullable()->default(null);
            $table->string('type')->nullable()->default(null);
            $table->string('status')->nullable()->default(null)->comment('1=>active, 0=>inactive');
            $table->date('start_date')->nullable()->default(null);
            $table->date('end_date')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cyo_doc_payment_history');
    }
}
