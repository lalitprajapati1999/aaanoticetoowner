<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCyoUspsAddressesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'cyo__usps_addresses';

    /**
     * Run the migrations.
     * @table cyo__usps_addresses
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('address');
            $table->string('city');
            $table->string('state');
            $table->string('zipcode');
            $table->string('zipcode_add_on');
            $table->string('dpb');
            $table->string('check_digit');
            $table->string('country');
            $table->unsignedInteger('recipient_id');
            $table->text('comment');
            $table->string('override_hash');

            $table->index(["recipient_id"], 'cyo__usps_addresses_recipient_id_foreign');
            $table->nullableTimestamps();


            $table->foreign('recipient_id', 'cyo__usps_addresses_recipient_id_foreign')
                ->references('id')->on('cyo__work_order__recipients')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
