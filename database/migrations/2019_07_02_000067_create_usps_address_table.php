<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUspsAddressTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'usps_address';

    /**
     * Run the migrations.
     * @table usps_address
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('address');
            $table->string('city');
            $table->string('state');
            $table->string('zipcode');
            $table->string('zipcode_add_on');
            $table->string('dpb');
            $table->string('check_digit');
            $table->string('country');
            $table->unsignedInteger('recipient_id');
            $table->text('comment');
            $table->string('override_hash');

            $table->index(["recipient_id"], 'usps_address_recipient_id_foreign');
            $table->nullableTimestamps();


            $table->foreign('recipient_id', 'usps_address_recipient_id_foreign')
                ->references('id')->on('recipients')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
