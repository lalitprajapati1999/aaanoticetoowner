<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBranchesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'branches';

    /**
     * Run the migrations.
     * @table branches
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('customer_id')->nullable()->default(null);
            $table->string('name')->nullable()->default(null)->comment('Branch Name');
            $table->string('contact_person')->nullable()->default(null);
            $table->string('phone')->nullable()->default(null);
            $table->string('email')->nullable()->default(null);
            $table->string('title')->nullable()->default(null)->comment('Registered Agent Title');
            $table->string('first_name')->nullable()->default(null)->comment('Registered Agent First Name');
            $table->string('last_name')->nullable()->default(null)->comment('Registered Agent Last Name');
            $table->string('address')->nullable()->default(null);
            $table->unsignedInteger('city_id')->nullable()->default(null);
            $table->unsignedInteger('state_id')->nullable()->default(null);
            $table->string('zip')->nullable()->default(null);
            $table->unsignedInteger('country')->nullable()->default(null);

            $table->index(["customer_id"], 'branches_customer_id_foreign');

            $table->index(["country"], 'branches_country_foreign');

            $table->index(["city_id"], 'branches_city_id_foreign');

            $table->index(["state_id"], 'branches_state_id_foreign');
            $table->nullableTimestamps();


            $table->foreign('city_id', 'branches_city_id_foreign')
                ->references('id')->on('cities')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('state_id', 'branches_state_id_foreign')
                ->references('id')->on('states')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
