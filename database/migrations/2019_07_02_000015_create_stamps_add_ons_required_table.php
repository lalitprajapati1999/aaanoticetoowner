<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStampsAddOnsRequiredTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'stamps_add_ons_required';

    /**
     * Run the migrations.
     * @table stamps_add_ons_required
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->unsignedInteger('add_on');
            $table->unsignedInteger('required');

            $table->index(["add_on"], 'stamps_add_ons_required_add_on_foreign');

            $table->index(["required"], 'stamps_add_ons_required_required_foreign');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
