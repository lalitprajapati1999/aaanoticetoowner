<?php

return [
    /*
      | Backpack/PermissionManager configs.
     */

    /*
      |--------------------------------------------------------------------------
      | User Fully-Qualified Class Name
      |--------------------------------------------------------------------------
      |
     */
    'user_model' => 'App\Models\User',
    /*
      |--------------------------------------------------------------------------
      | Disallow the user interface for creating/updating permissions or roles.
      |--------------------------------------------------------------------------
      | Roles and permissions are used in code by their name
      | - ex: $user->hasPermissionTo('edit articles');
      |
      | So after the developer has entered all permissions and roles, the administrator should either:
      | - not have access to the panels
      | or
      | - creating and updating should be disabled
     */
    'allow_permission_create' => false,
    'allow_permission_update' => true,
    'allow_permission_delete' => false,
    'allow_role_create' => false,
    'allow_role_update' => true,
    'allow_role_delete' => false,
    /*
      | -------------------------------------------------------------------------
      | Default User, Role and Permission Data.
      | -------------------------------------------------------------------------
     */
    'data' => [
        'permissions' => [
            'super-admin' => [
//                'acl-management-users',
                'admin-dashboard',
                'admin-notices',
                'admin-notices-create',
                'admin-notices-edit',
                'admin-notices-delete',
                'admin-notices-status',
                'admin-notices-states',
                'admin-notices-states-fields',
                'admin-mailing',
                //'log-viewer',
                'admin-notice-templates',
                'admin-notice-template-create',
                'admin-notice-template-destroy',
                'admin-notice-template-edit',
//                'acl-management-users',
                'admin-states',
                'admin-states-list',
                'admin-states-changestatus',
                'admin-customer-subscription',
                'admin-states-create',
                'admin-cities',
                'admin-edit-account-info',
            ],
            'admin' => [
            'admin-dashboard',
                'acl-management-users',
               
                'account-manager-research',
                'admin-change-password',
                'admin-edit-account-info',
                'admin-customer',
                'admin-customer-edit',
                'admin-network',
                'admin-network-allow-secondary',
                'admin-new-applicants',
                'admin-employees',
                'admin-employee-create',
                'admin-employees-note',
                'admin-employees-attachment',
                'admin-employee-edit',
                'admin-employee-view',
                'admin-customer-status',
                'admin-manage-subscriptions',
                'admin-testimonial',
                'admin-lien_blog',
                'admin-category',
                'admin-project-type',
                'admin-reports',
                'secondary-document',
                'admin-customer-subscription',
                'admin-Mailing',
                'account-manager-mailing',
                'account-manager-cyo-mailing',
                'admin-project-roles'
            ],
            'account-manager' => [
                'admin-dashboard',
                'admin-customer',
                'account-manager-research',
                'account-manager-hard-document',
                'secondary-document',
                'account-manager-administration',
                'admin-Mailing',
                'account-manager-mailing',
                'account-manager-cyo-mailing',
                'admin-customer-edit',
                'admin-change-password',
                'admin-edit-account-info',
                'admin-customer-status',
                'customer-deadline-calculator',
                'account-manager-create-your-own',
            //  'admin-manage-subscriptions',
                  'customer-contacts',
//                'customer-contacts-create',
//                'customer-contacts-edit',
//                'customer-contacts-delete',
            ],
            'customer' => [
                'customer-dashboard',
                'admin-change-password',
                'admin-edit-account-info',
                'customer-contacts',
                'customer-contacts-create',
                'customer-contacts-edit',
                'customer-contacts-delete',
                'new-work-order',
                'new-work-order-create',
                'secondary-document',
                'customer-view-work-order',
                'customer-create-your-own',
                'customer-deadline-calculator',
                'customer-statement-invoices',
                'admin-edit-account-info',
                'customer-invoice',
            ]
        ],
        'default_user' => [
            'super-admin' => [
                'name' => 'Super Admin',
                'email' => 'super@nto.com',
                'password' => '123456',
                'status' => '1',
            ],
            'admin' => [
                'name' => 'Admin',
                'email' => 'admin@nto.com',
                'password' => '123456',
                'status' => '1',
            ],
            'customer' => [
                'name' => 'Customer Neosoft',
                'email' => 'customer@nto.com',
                'password' => '123456',
                'status' => '1',
            ],
            'account-manager' => [
                'name' => 'Manager Neosoft',
                'email' => 'am@nto.com',
                'password' => '123456',
                'status' => '1',
                'pin' => '1234',
            ]

        ]
    ]
];
