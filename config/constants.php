<?php

return [
	'contact_detail_type' => [
		0 => 'contact_person',
		1 => 'sales_person'
	],
	'notice_type' => [
		0 => 'NA',
		1 => 'Hard Document',
		2 => 'Soft Document',
		3 => 'Secondary Document'
	],
	'notice_status' => [
		0 => 'Inactive',
		1 => 'Active'
	],
	'field_types' => [
		/*0 => 'label',*/
		1 => 'text',
		2 => 'textarea',
		3 => 'select',
		4 => 'checkbox', 
		/*5 => 'radio',*/
		6 => 'datepicker'
	],

	'section_types' => [
		1 => 'section 1',
		2 => 'section 2',
		
	],
	'MASTER_NOTICE'=> [
		'ITL' => ['ID'=>1,'NAME'=>'ITL'],
		'NPN' => ['ID'=>2,'NAME'=>'NPN'],
		'COL' => ['ID'=>3,'NAME'=>'COL'],
		'SOL' => ['ID'=>4,'NAME'=>'SOL'],
		'BCOL' => ['ID'=>5,'NAME'=>'BCOL'],
		'SBC'  => ['ID'=>6,'NAME'=>'SBC'],
		'PROL' => ['ID'=>7,'NAME'=>'PROL'],
		'NTO'  => ['ID'=>8,'NAME'=>'NTO'],
		'WRL'  => ['ID'=>9,'NAME'=>'WRL'],
		'FROL' => ['ID'=>10,'NAME'=>'FROL'],
		'FUW'  => ['ID'=>11,'NAME'=>'FUW'],
		'PUBW' => ['ID'=>12,'NAME'=>'PUBW'],

	],
	
	'PACKAGE_RANGE' => [
		'Above' => 99999999,
		'Over'  => 99999999,
		'above' => 99999999,
		'over'  => 99999999,
	],	

	'SIGNED_DOC_EMAILS' =>[
		0 => 'bondclaim@aaanoticetoowner.com',
		1 => 'signdoc@aaanoticetoowner.com',
	],
	

	//CONSTANT => DATABASE
	'PDF_WATERMARK_CSS' => '',//file_get_contents('./public/css/pdf-watermark.css'),
	'PDF_WATERMARK_HTML' => '',//'<div id="pdf_watermark"><h2>COPY – NOT A LEGAL DOCUMENT</h2></div>',

	'QBO' => [	'CONSUMER_KEY' => env('QBO_OAUTH_CONSUMER_KEY'),
				'CONSUMER_SECRET' => env('QBO_CONSUMER_SECRET'),
				'COMPANY_ID' => env('QBO_COMPANY_ID'),
				'ENVIRONMENT' => env('QBO_ENVIRONMENT'),
				'APP_URL' => env('QBO_API_URL'),
				'APP_URL2' => env('QBO_API_URL2'),
				'CALLBACK_URL' => env('QBO_REDIRECT_URL'),
				'SCOPE' => env('QBO_ACCOUNTING_SCOPE'),
				'PAYMENT_SCOPE' => env('QBO_PAYMENT_SCOPE'),
				'AUTH_REQ_URL' => env('QBO_AUTHORIZATION_REQUEST_URL'),
				'TOK_ENDPOINT_URL' => env('QBO_TOKEN_ENPOINT_URL'),
				'NoticeBaseAmount' => env('NoticeBaseAmount'),
				'FirmMail' => env('FirmMail'),
				'CertifiedMail' => env('CertifiedMail'),
				'ElectronicReturnReceipt' => env('ElectronicReturnReceipt'),
				'ReturnReceiptRequested' => env('ReturnReceiptRequested'),
				'NextDayDelivery' => env('NextDayDelivery'),
				'PriorityMail' => env('PriorityMail'),
				'RushHourCharges' => env('RushHourCharges'),
				'AdditionalAddress' => env('AdditionalAddress'),
				'ManualCharges' => env('ManualCharges'),

				'MARGIN_CERTIFIED' => env('MARGIN_CERTIFIED'),
				'MARGIN_CERTIFIED_ERR' => env('MARGIN_CERTIFIED_ERR'),
				'MARGIN_CERTIFIED_RR' => env('MARGIN_CERTIFIED_RR'),
			],
	'TRANSACTION_EMAILS' => ['SALES_EMAIL' => env('SALES_EMAIL'), 
							'CLIENT_MAIL' => env('CLIENT_MAIL'),
							'CONTACT_US_EMAIL' => env('CONTACT_US_EMAIL'),
		],
	'STAMPS' => ['WSDL' => env('STAMPS_WSDL'), 
				'INTEGRATION_ID' => env('STAMPS_INTEGRATION_ID'),
				'USERNAME' => env('STAMPS_USERNAME'),
				'PASSWORD' => env('STAMPS_PASSWORD'),
		],

];