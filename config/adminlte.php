<?php

return [
    /*
      |--------------------------------------------------------------------------
      | Title
      |--------------------------------------------------------------------------
      |
      | The default title of your admin panel, this goes into the title tag
      | of your page. You can override it per page with the title section.
      | You can optionally also specify a title prefix and/or postfix.
      |
     */

    'title' => env('APP_NAME', 'Notice to Owner'),
    'title_prefix' => '',
    'title_postfix' => '',
    /*
      |--------------------------------------------------------------------------
      | Logo
      |--------------------------------------------------------------------------
      |
      | This logo is displayed at the upper left corner of your admin panel.
      | You can use basic HTML here if you want. The logo has also a mini
      | variant, used for the mini side bar. Make it 3 letters or so
      |
     */
    'logo' => '<img src="' . env('APP_URL') . '/img/logo.png" class="img-fluid img-logo" alt="Logo"/>',
    'logo_mini' => '<img src="' . env('APP_URL') . '/img/logo.png" class="img-fluid img-logo" alt="Logo"/>',
    /*
      |--------------------------------------------------------------------------
      | Skin Color
      |--------------------------------------------------------------------------
      |
      | Choose a skin color for your admin panel. The available skin colors:
      | blue, black, purple, yellow, red, and green. Each skin also has a
      | ligth variant: blue-light, purple-light, purple-light, etc.
      |
     */
    'skin' => 'blue-dark',
    /*
      |--------------------------------------------------------------------------
      | Layout
      |--------------------------------------------------------------------------
      |
      | Choose a layout for your admin panel. The available layout options:
      | null, 'boxed', 'fixed', 'top-nav'. null is the default, top-nav
      | removes the sidebar and places your menu in the top navbar
      |
     */
    'layout' => 'fixed',
    /*
      |--------------------------------------------------------------------------
      | Collapse Sidebar
      |--------------------------------------------------------------------------
      |
      | Here we choose and option to be able to start with a collapsed side
      | bar. To adjust your sidebar layout simply set this  either true
      | this is compatible with layouts except top-nav layout option
      |
     */
    'collapse_sidebar' => false,
    /*
      |--------------------------------------------------------------------------
      | URLs
      |--------------------------------------------------------------------------
      |
      | Register here your dashboard, logout, login and register URLs. The
      | logout URL automatically sends a POST request in Laravel 5.3 or higher.
      | You can set the request to a GET or POST with logout_method.
      | Set register_url to null if you don't want a register link.
      |
     */
    'dashboard_url' => 'login', // Login action will redirect tpo respective dashboard
    'logout_url' => 'logout',
    'logout_method' => null,
    'login_url' => 'login',
    'register_url' => 'register',
    /*
      |--------------------------------------------------------------------------
      | Menu Items
      |--------------------------------------------------------------------------
      |
      | Specify your menu items to display in the left sidebar. Each menu item
      | should have a text and and a URL. You can also specify an icon from
      | Font Awesome. A string instead of an array represents a header in sidebar
      | layout. The 'can' is a filter on Laravel's built in Gate functionality.
      |
     */
    'menu' => [
        // 'MAIN NAVIGATION',
        [
            'text' => 'Dashboard',
            'url' => 'home',
            'can' => 'customer-dashboard',
            'icon' => 'dashboard-icon icon-speedometer'
        ],
          [
            'text' => 'Dashboard',
            'url' => 'admin/dashboard',
            'can' => 'admin-dashboard',
            'icon' => 'dashboard-icon icon-speedometer'
        ],
        [
            'text' => 'New Applicants',
            'url' => 'admin/new-applicants',
            'can' => 'admin-new-applicants',
            'icon' => 'dashboard-icon icon-New-applicants'
        ],
      
        [
            'text' => 'Customer',
            'url' => 'admin/customer',
            'can' => 'admin-customer',
            'icon' => 'dashboard-icon icon-Customer'
        ],
        // [
        //     'text' => 'Dashboard',
        //     'url' => 'admin/dashboard',
        //     'can' => 'admin-notices',
        //     'icon' => 'dashboard-icon icon-Administration'
        // ],
        [
            'text' => 'Manage Notices',
            'url' => 'admin/notices',
            'can' => 'admin-notices',
            'icon' => 'dashboard-icon icon-Customer'
        ],
         
        [
            'text' => 'Employees',
            'url' => 'admin/employees',
            'can' => 'acl-management-users',
            'icon' => 'dashboard-icon icon-Employee'
        ],
        [
            'text' => 'States',
            'url' => 'admin/states',
            'can' => 'admin-states',
            'icon' => 'dashboard-icon icon-world '
        ],
        [
            'text' => 'Cities',
            'url' => 'admin/cities',
            'can' => 'admin-cities',
            'icon' => 'dashboard-icon icon-world'
        ],
        [
            'text' => 'New Work Order',
            'url' => 'customer/new-work-order',
            'can' => 'new-work-order',
            'icon' => 'dashboard-icon icon-add-to-queue-button'
        ],
        [
            'text' => 'Secondary Document',
            'url' => 'customer/secondary-document',
            'can' => 'secondary-document',
            'icon' => 'dashboard-icon icon-Hard-document'
        ],
        [
            'text' => 'Address Book',
            'url' => 'customer/contacts',
            'can' => 'customer-contacts',
            'icon' => 'dashboard-icon icon-receipt'
        ],
        [
            'text' => 'Network',
            'url' => 'admin/network',
            'can' => 'admin-network',
            'icon' => 'dashboard-icon icon-Network'
        ],
        // 'ACCOUNT SETTINGS',
        [
            'text' => 'Roles',
            'url' => config('backpack.base.route_prefix', 'admin') . '/role',
            'can' => 'acl-management-roles',
            'icon' => 'dashboard-icon icon-Employee'
        ],
        [
            'text' => 'Permissions',
            'url' => config('backpack.base.route_prefix', 'admin') . '/permission',
            'can' => 'acl-management-permissions',
            'icon' => 'dashboard-icon '
        ],
        [
            'text' => 'Notice Template',
            'url' => 'admin/notice_templates',
            'can' => 'admin-notice-templates',
            'icon' => 'dashboard-icon icon-Notice'
        ],
        [
            'text' => 'View Work Order',
            'url' => 'customer/view-work-order',
            'can' => 'customer-view-work-order',
            'icon' => 'dashboard-icon icon-dark-eye'
        ],
        /*[
            'text' => 'Statement',
            'url' => 'customer/statement-invoices',
            'can' => 'customer-statement-invoices',
            'icon' => 'dashboard-icon icon-agenda'
        ],*/
        [
            'text' => 'Invoice',
            'url' => 'customer/invoice',
            'can' => 'customer-invoice',
            'icon' => 'dashboard-icon icon-agenda'
        ],
        [
            'text' => 'Account Settings',
            'url' => 'admin/edit-account-info',
            'can' => 'admin-edit-account-info',
            'icon' => 'dashboard-icon icon-settings-work-tool'
        ],
        [
            'text' => 'Create Your Own',
            'url' => 'customer/create-your-own',
            'can' => 'customer-create-your-own',
            'icon' => 'dashboard-icon icon-add-to-queue-button'
        ],
        [
            'text' => 'Research',
            'url' => 'account-manager/research',
            'can' => 'account-manager-research',
            'icon' => 'dashboard-icon icon-Research',
        ],
//        [
//            'text' => 'Administration',
//            'url'  => 'customer/view-work-order',
//            'can'  => 'account-manager-administration',
//            'icon' => 'dashboard-icon icon-Administration'
//        ],
//        [
//            'text' => 'Hard Document',
//            'url'  => 'customer/view-work-order',
//            'can'  => 'account-manager-hard-document',
//            'icon' => 'dashboard-icon icon-document'
//        ],
        [
            'text' => 'Mailing',
            'url' => 'account-manager/mailing',
            'can' => 'account-manager-mailing',
            'icon' => 'dashboard-icon icon-black-envelope'
        ],
        [
            'text' => 'Create Your Own',
            'url' => 'account-manager/create-your-own',
            'can' => 'account-manager-mailing',
            'icon' => 'dashboard-icon icon-add-to-queue-button'
        ],
        [
            'text' => 'Testimonial',
            'url' => 'admin/testimonial',
            'can' => 'admin-testimonial',
            'icon' => 'dashboard-icon icon-testimonial'
        ],
        [
            'text' => 'Lien blog',
            'url' => 'admin/lien_blog',
            'can' => 'admin-lien_blog',
            'icon' => 'dashboard-icon icon-blog'
        ],
        [
            'text' => 'Customer Role',
            'url' => 'admin/category',
            'can' => 'admin-category',
            'icon' => 'dashboard-icon icon-role'
        ],
        [
            'text' => 'Project Type',
            'url' => 'admin/project-type',
            'can' => 'admin-project-type',
            'icon' => 'dashboard-icon icon-Project-type'
        ],
        // 'LOGS',
        [
            'text' => 'Error Logs',
            'url' => 'log-viewer',
            'icon' => 'warning',
            'can' => 'log-viewer',
        ],
        [
            'text' => 'Manage Subscriptions',
            'url' => 'admin/manage-subscriptions',
            'can' => 'admin-manage-subscriptions',
            'icon' => 'dashboard-icon icon-Manage-Subscription'
        ],
        [
            'text' => 'Reports',
            'url' => 'admin/reports',
            'can' => 'admin-reports',
            'icon' => 'dashboard-icon icon-Reports'
        ],
         [
            'text' => 'Project Roles',
            'url' => 'admin/project-roles',
            'can' => 'admin-project-roles',
            'icon' => 'dashboard-icon icon-role'
        ],
    ],
    /*
      |--------------------------------------------------------------------------
      | Menu Filters
      |--------------------------------------------------------------------------
      |
      | Choose what filters you want to include for rendering the menu.
      | You can add your own filters to this array after you've created them.
      | You can comment out the GateFilter if you don't want to use Laravel's
      | built in Gate functionality
      |
     */
    'filters' => [
        JeroenNoten\LaravelAdminLte\Menu\Filters\HrefFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ActiveFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\SubmenuFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\ClassesFilter::class,
        JeroenNoten\LaravelAdminLte\Menu\Filters\GateFilter::class,
    ],
    /*
      |--------------------------------------------------------------------------
      | Plugins Initialization
      |--------------------------------------------------------------------------
      |
      | Choose which JavaScript plugins should be included. At this moment,
      | only DataTables is supported as a plugin. Set the value to true
      | to include the JavaScript file from a CDN via a script tag.
      |
     */
    'plugins' => [
        'datatables' => false,
        'select2' => false,
    ],
    /*
      |--------------------------------------------------------------------------
      | Footer Note
      |--------------------------------------------------------------------------
      |
      | Configure footer content to be displayed on Dashboard ,
      |
     */
    'footer_note' => 'All Rights Reserved by AAA Business Association Corp DBA AAA Notice to Owner',
];
