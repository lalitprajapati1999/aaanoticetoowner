FROM composer:2.3 as build
COPY . /app/
# RUN composer install --ignore-platform-reqs --no-plugins --prefer-dist --no-dev --optimize-autoloader --no-interaction
FROM php:7.4-apache-buster as dev

ENV APP_ENV=dev
ENV APP_DEBUG=true
ENV COMPOSER_ALLOW_SUPERUSER=1

RUN sed -i 's/deb.debian.org/10.10.0.12:9999/g' /etc/apt/sources.list
RUN apt-get update && \
    apt-get install -y git 
RUN docker-php-ext-install mysqli pdo pdo_mysql
RUN docker-php-ext-enable mysqli
RUN docker-php-ext-install bcmath
RUN docker-php-ext-enable bcmath
RUN docker-php-ext-install exif
RUN docker-php-ext-enable exif
RUN apt-get install nano
RUN apt-get update && apt-get install -y nano 
RUN apt-get update && apt-get install -y libpng-dev libjpeg-dev
RUN docker-php-ext-configure gd --with-jpeg
RUN docker-php-ext-install gd

COPY . /var/www/html/
COPY --from=build /usr/bin/composer /usr/bin/composer
# RUN composer install --ignore-platform-reqs --no-plugins --prefer-dist --no-interaction

# COPY docker/config/vhosts/default.conf /etc/apache2/sites-available/000-default.conf
COPY .env.example /var/www/html/.env

RUN sed -i '/<Directory \/var\/www\/>/,/<\/Directory>/ s/AllowOverride None/AllowOverride All/' /etc/apache2/apache2.conf
RUN a2enmod rewrite
# Set the ServerName globally in Apache configuration
RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf
RUN service apache2 restart
# RUN php artisan config:cache && \
#     php artisan route:cache && \
#     chmod 777 -R /var/www/html/storage/ && \
#     chown -R www-data:www-data /var/www/ && \
#     a2enmod rewrite

FROM php:8.1-apache-buster as production

ENV APP_ENV=production
ENV APP_DEBUG=false

RUN docker-php-ext-configure opcache --enable-opcache && \
    docker-php-ext-install pdo pdo_mysql
COPY docker/php/conf.d/opcache.ini /usr/local/etc/php/conf.d/opcache.ini

COPY --from=build /app /var/www/html
COPY docker/apache/000-default.conf /etc/apache2/sites-available/000-default.conf
COPY .env.example /var/www/html/.env

RUN php artisan config:cache && \
    php artisan route:cache && \
    chmod 777 -R /var/www/html/storage/ && \
    chown -R www-data:www-data /var/www/ && \
    a2enmod rewrite
