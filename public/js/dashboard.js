
function countAddress(obj) {
    // console.log(obj.value);
    var str = obj.value;
    var array = str.split("**");
    // console.log(array.length);
    $("#project_address_count").val(array.length);
    $("#project_address_count").addClass('not-empty');
}
//$('input').addClass('not-empty');
/****************** autoclose datepicker ***********/
// $('.datepicker').datepicker().on('changeDate', function(ev){                 
//     $('.datepicker').datepicker('hide');
// });


/**********************Create your own  notes and correction**************/
$(".submitNote").click(function (e) {
    e.preventDefault();
    $('.note-success').hide();
    $('.note-error').hide();
//   var note = $("input[name=note]").val();
//   var email = $("#note_email").val();
//   var work_order_id = $('#work_order').val();
//   data: {note: note, email: email, doc_id: work_order_id},
    $.ajax({
        type: 'POST',
        url: note_url,
        data: $('#edit_work_order_note').serialize(),
        beforeSend: function () {
            $('#pleaseWait').modal('show');

        },
        complete: function () {
            $('#pleaseWait').modal('hide');
        },
        success: function (data) {
            if (data.success) {
                var d = new Date();
                var strDate = (d.getMonth() + 1) + "-" + d.getDate() + "-" + d.getFullYear();
                $('.note-error').empty();
                $('.note-success').empty();
                $("input[name=note]").val('');
                $("#note_email option").prop("selected", false);
                $('#note_email').selectpicker('refresh');
                $('.note-error').css('display', 'none');
                $('.note-success').show();
                $('.note-success').append('<p>' + data.success + '</p>');
                // $('.notes_div').append('<li>' + data.notes.note + '<br>' + data.notes.email + '</li>');
                $('.notes_div').prepend('<li>' + data.notes.note +'</br>Added by:'+ data.notes.name + '</br>Created at:' + strDate + '</li>');

            } else if (data.errors) {
                $('.note-error').empty();
                $.each(data.errors, function (key, value) {
                    $('.note-success').empty();
                    $('.note-success').css('display', 'none');
                    $('.note-error').show();
                    $('.note-error').append('<p>' + value + '</p>');
                });
            }

        }

    });
});
$(".submitCorrection").click(function (e) {
    e.preventDefault();
    $('.correction-error').hide();
    $('.correction-success').hide();
    $.ajax({
        type: 'POST',
        url: correction_url,
        data: $('#edit_work_order_correction').serialize(),
        beforeSend: function () {
            $('#pleaseWait').modal('show');
        },
        complete: function () {
            $('#pleaseWait').modal('hide');
        },
        success: function (data) {
            if (data.success) {
                var d = new Date();
                var strDate = (d.getMonth() + 1) + "-" + d.getDate() + "-" + d.getFullYear();
                $('.correction-error').empty();
                $('.correction-success').empty();
                $("input[name=correction]").val('');
                $("#correction_email option").prop("selected", false);
                $('#correction_email').selectpicker('refresh');
                $('.correction-error').css('display', 'none');
                $('.correction-success').show();

                $('.correction-success').append('<p>' + data.success + '</p>');
//                $('.correction_div').append('<li>' + data.corrections.correction + '<br>' + data.corrections.email + '</li>');
                $('.correction_div').prepend('<li>' + data.corrections.correction + '</br>Added by:'+ data.corrections.name+'</br>Created at:' + strDate + '</li>');

            } else if (data.errors) {
                $('.correction-error').empty();
                $.each(data.errors, function (key, value) {
                    $('.correction-success').empty();
                    $('.correction-success').css('display', 'none');
                    $('.correction-error').show();
                    $('.correction-error').append('<p>' + value + '</p>');
                });
            }
        }
    });
});
/**********************Create your own notes and correction**************/

/**********************Create your own Recipient City Autocomplete and Name Autopopulate************************************/
//Recipient city

$("#recipient_city").autocomplete({
    source: function (request, response) {
        $.ajax({
            url: src,
            dataType: "json",
            data: {
                term: request.term
            },
            beforeSend: function () {
                $('#recipientloader').show();
            },
            complete: function () {
                $('#recipientloader').hide();
            },
            success: function (data) {

                response($.map(data, function (item) {
                    return {
                        value: item.value,
                        id: item.id     // EDIT
                    }
                }));

            },
        });
    },
    select: function (event, ui) {

        $("#recipient_city").val(ui.item.id);
        $("#recipient_city").val(ui.item.value);
    },
    change: function (event, ui) {
        if (ui.item == null) {
            $(this).val("");
            $(this).focusout();
            $(this).focus();
        }
    }
});


$('#recipient_name').on('change', function () {
    $('input[name=contact_name]').val('');
    $('input[name=contact_no]').val('');
    $('input[name=contact_email]').val('');
    $('#contact_address').val('');
    $('#attn').val('');
    $('input[ name=contact_zip]').val('');
    $("#contact_state_id").val('');
    $("#contact_state_id").selectpicker('refresh');
    $("#contact_city_id").val('').trigger('change');
    $('#autocomplete_city_id').val('');
    $('.new_reipient_zip').val('');
    var name = $('#recipient_name').val();
    var recipt_id = $(this).find(':selected').attr('data-id');
    $('#recipt_id').val(recipt_id);
    if(!recipt_id){
        $("#recipient_city").val("");
        $("#recipient_city").trigger('change');
    }
    $.ajax({
        data: {'name': name, 'recipt_id': recipt_id ,"customer_id": customer_ids},
        type: "post",
        url: get_contact,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (res) {

            //  $('#recipient_name').val(res.company_name);
            $('#recipient_email').val(res.email);
            $('#recipient_mobile').val(res.mobile);
            $('#recipient_contact').val(res.phone);
            $('#recipient_zip').val(res.zip);
            $('#recipient_address').val(res.mailing_address);
            $('#recipient_address2').val('');
            $('#recipient_address3').val('');
            $('#recipient_fax').val(res.fax);
            $("#recipient_state").val(res.state_id);
            $("#recipient_state").selectpicker("refresh");
             var urlzipcode = $('#contact_zipurl').val();
            $.ajax({
                url: urlzipcode,
                type: 'post',
                dataType: "json",
                data: {
                    zipcode: res.zip,
                    cityName:res.city_id
                },
                success: function(data) {
                    $(".new_reipient_zip").val(data[0].label);
                }
            });
            /*$('#recipient_city').val(res.city_name);
            $('#recipient_city_id').val(res.city_id);*/
             if(res.city_id==""){
                    $("#recipient_city").val(res.city_id);
                    $("#recipient_city").trigger('change');
                   }else{
                    var option = new Option(res.city_name, res.city_id, true, true);
                    $("#recipient_city").append(option).trigger('change');
                   }
            $('#recipient_attn').val(res.attn);
//                    $('#recipient_country').val(res.country_id);
//                    $("#recipient_country").selectpicker("refresh");

            $('input,textarea').each(function () {
                if ($(this).val().length > 0) {
                    $(this).addClass('not-empty');
                } else {
                    $(this).removeClass('not-empty');
                }
            });

        },
    });
});
/**********************Recipient City Autocomplete and Name Autopopulate************************************/
/**************************Add address book modal*******************************/
$("#contact_city_id").autocomplete({

    source: function (request, response) {
        $.ajax({
            url: src,
            dataType: "json",
            data: {
                term: request.term
            },
            beforeSend: function () {
                $('#recipientpopuploader').show();
            },
            complete: function () {
                $('#recipientpopuploader').hide();
            },
            success: function (data) {

                response($.map(data, function (item) {
                    return {
                        value: item.value,
                        id: item.id     // EDIT
                    }
                }));

            },

        });
    },
    select: function (event, ui) {

        $("#autocomplete_city_id").val(ui.item.id);
        $("#contact_city_id").val(ui.item.value);
    },
    change: function (event, ui) {
        if (ui.item == null) {
            $(this).val("");
            $(this).focusout();
            $(this).focus();
        }
    }
});
//Add recipient contact dynamic
$('#add_contact,#add_contact_contracted_by').click(function () {
//    alert($(this).hasClass('contracted_by_recipient'));
    var all_val = 1;
    $('#selected_address_book').val('');
    if ($(this).hasClass('contracted_by_recipient')) {
        var recipt_id = '';
        var customer_id = customer_ids;
        var name = $('#contracted_by_select').val();
        $.ajax({
            data: { 'name': name, 'recipt_id': recipt_id, "customer_id": customer_id },
            type: "post",
            url: get_contact,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(res) {
                $('.addressattn').val(res.attn);
                $('.addresscontact_no').val(res.phone);
                $('.addresscontact_email').val(res.email);
                $('.addresscontact_address').val(res.company_address);
                $('.addresscontact_city_id')
                    .empty()
                    .append($("<option/>")
                        .val(res.city_id)
                        .text(res.city_name))
                    .val(res.city_id)
                    .trigger("change");
                $(".addresscontact_state_id").val(res.state_id).trigger('change');
                $('.addresscontact_zip').val(res.zip);
                $('#contact_id').val(res.id);
                var urlzipcode = $('#contact_zipurl').val();
                $.ajax({
                    url: urlzipcode,
                    type: 'post',
                    dataType: "json",
                    data: {
                        zipcode: res.zip,
                        cityName:res.city_id
                    },
                    success: function(data) {
                        $(".new_addresscontact_zip").val(data[0].label);
                    }
                });
            }

        });
        $('input[name=contact_name]').val($('#contracted_by_select').val());
        $('input[name=old_contact_name]').val($('#contracted_by_select').val());
        $('input[name=contact_name]').addClass('not-empty');
        $('#selected_address_book').val('project');
    } else if ($('#recipient_name').val() != "") {
        var recipt_id = "";
        var customer_id = customer_ids;
        var name = $('#recipient_name').val();
        $.ajax({
            data: { 'name': name, 'recipt_id': recipt_id, "customer_id": customer_id },
            type: "post",
            url: get_contact,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(res) {
                var zipcode = $('#recipient_zip').val();
                var urlzipcode = $('#contact_zipurl').val();
                $.ajax({
                    url: urlzipcode,
                    type: 'post',
                    dataType: "json",
                    data: {
                        zipcode: zipcode,
                        cityName:res.city_id
                    },
                    success: function(data) {
                        $(".new_addresscontact_zip").val(data[0].label);
                    }
                });
                $('#contact_id').val(res.id);
            }
        });
        $('input[name=contact_name]').val($('#recipient_name').val());
        $('input[name=contact_no]').val($('#recipient_contact').val());
        $('input[name=contact_email]').val($('#recipient_email').val());
        $('#contact_address').val($('#recipient_address').val());
        $('input[name=contact_zip]').val($('#recipient_zip').val());
        $("#contact_state_id").val($('#recipient_state').val());
        $("#contact_state_id").selectpicker('refresh');
        $("#attn").val($('#recipient_attn').val());
        var city_val = $("#recipient_city").select2('data');
        var option = new Option(city_val[0]['text'], city_val[0]['id'], true, true);
        $("#contact_city_id").append(option).trigger('change');
        //$("#contact_city_id").val($('#recipient_city').val());
       // $('#autocomplete_city_id').val($('#recipient_city').val());
        //$("#contact_city_id").selectpicker('refresh');

        $('input,textarea').addClass('not-empty');

        all_val = 0;
    } else {
        $('input[name=contact_name]').val('');
    }
    // $('input[name=contact_name]').val('');
    if (all_val) {
        $('input[name=contact_no]').val('');
        $('input[name=contact_email]').val('');
        $('#contact_address').val('');
        $('#attn').val('');
        $('input[ name=contact_zip]').val('');
        $("#contact_state_id").val('');
        $("#contact_state_id").selectpicker('refresh');
        //$("#contact_city_id").val('');
        $("#contact_city_id").val('').trigger('change')
        $('#autocomplete_city_id').val('');
    }
//        $("#contact_country").val('');
//        $("#contact_country").selectpicker('refresh');
    $('#name-error').html('');
    $('#address-error').html('');
    $('#city-error').html('');
    $('#state-error').html('');
    $('#zip-error').html('');
    $('#success-msg').css('display', 'none');

//            $('#contactModal').modal('show');
});
$(".business_phone_validation").mask("(999) 999-9999");
$(".email_validation").attr("data-parsley-trigger", "change focusout")
        .attr("data-parsley-type", "email")
        .parsley();

$(".zip_validation").attr("data-parsley-trigger", "change focusout")
        .attr("data-parsley-type", "digits")
        .attr("data-parsley-minlength", "5")
        .attr("data-parsley-maxlength", "5")
        .parsley();

$('body').on('click', '#submit_contact', function () {
    var recipientForm = $('#add_recipient_contact');
//    var formData = recipientForm.serialize();

    $('#name-error').html("");
    $('#contact-error').html("");
    $('#email-error').html("");
    $('#address-error').html("");
    $('#city-error').html("");
    $('#state-error').html("");
    $('#zip-error').html("");
// $('#country-error').html("");
    $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name=_token]').attr('content')
        }
    });
    $.ajax({
        url: cyo_contact_add,
        type: 'POST',
        data: {
            doc_id: $('#doc_id_recipient').val(),
            contact_name: $('#contact_name').val(),
            contact_id: $('#contact_id').val(),
            contact_no: $('#contact_no').val(),
            contact_email: $('#contact_email').val(),
            contact_address: $('#contact_address').val(),
            contact_state_id: $('#contact_state_id').val(),
            contact_zip: $('#contact_zip').val(),
            contact_city_id: $('#contact_city_id').val(),
            attn: $('#attn').val(),
            //contact_country: $('#contact_country').val(),
        },
        success: function (data) {
            if (data.error_msg) {
                $('#error-msg').css('display', 'block');
                $('#error-msg').html(data.error_msg);
                $('#success-msg').css('display', 'none');
                $('#success-msg').html('');
            }
            if (data.errors) {
                if (data.errors.contact_name) {
                    $('#name-error').html(data.errors.contact_name[0]);
                }
                if (data.errors.contact_address) {
                    $('#address-error').html(data.errors.contact_address[0]);
                }
                if (data.errors.contact_city_id) {
                    $('#city-error').html(data.errors.contact_city_id[0]);
                }
                if (data.errors.contact_state_id) {
                    $('#state-error').html(data.errors.contact_state_id[0]);
                }
//                    if (data.errors.contact_country) {
//                        $('#country-error').html(data.errors.contact_country[0]);
//                    }
                if (data.errors.contact_zip) {
                    $('#zip-error').html(data.errors.contact_zip[0]);
                }
                if (data.errors.contact_no) {
                    $('#contact-error').html(data.errors.contact_no[0]);
                }
                $('#error-msg').css('display', 'none');
                $('#error-msg').html(data.errors);
                $('#success-msg').css('display', 'none');
                $('#success-msg').html('');

            }
            if (data.success) {
                // console.log(data.contact_data)
                $('#success-msg').css('display', 'block');
                $('#success-msg').html(data.success);
                $('#error-msg').css('display', 'none');
                $('#error-msg').html('');

                // console.log($('#doc_id_recipient').val());
                $.ajax({
                    data: {"doc_id": $('#doc_id_recipient').val()},
                    type: 'POST',
                    url: get_cyo_contacts, // point to server-side PHP script
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (response) {
                        var selected_address_book = $("#selected_address_book").val();
                        var recipient_name_selected = $('#recipient_name').val();
                        var contracted_by_select = $('#contracted_by_select').val();


                        $("#contracted_by_select").empty("");

                        $("<option value=''>Select</option>").appendTo('#contracted_by_select');
                        $.each(response.contacts, function (i, obj)
                        {

                            if (i == 0 && selected_address_book == "project") {
                                var d_data = "<option  value='" + obj.company_name + "' selected='selected'>" + obj.company_name + "</option>";
                            } else {
                                if (obj.company_name == data.contact_data.company_name) {
                                    var d_data = "<option value='" + obj.company_name + "' selected='selected'>" + obj.company_name + "</option>";
                                } else {
                                    var d_data = "<option  value='" + obj.company_name + "'>" + obj.company_name + "</option>";
                                }
                            }


                            $(d_data).appendTo('#contracted_by_select');
                        });
                        $("#contracted_by_select").trigger('change');



                        $("#recipient_name").empty("");
                        $("<option value=''>Select</option>").appendTo('#recipient_name');
                        $.each(response.contacts, function (i, obj)
                        {
                            if (i == 0 && selected_address_book != "project") {
                                var div_data = "<option data-id='" + obj.id + "' value='" + obj.company_name + "' selected='selected'>" + obj.company_name + "</option>";
                            } else {
                                if (obj.company_name == data.contact_data.company_name) {
                                    var div_data = "<option data-id='" + obj.id + "' value='" + obj.company_name + "' selected='selected'>" + obj.company_name + "</option>";
                                } else {
                                    var div_data = "<option data-id='" + obj.id + "' value='" + obj.company_name + "'>" + obj.company_name + "</option>";
                                }
                            }
                            $(div_data).appendTo('#recipient_name');
                        });
                        if (selected_address_book == "project") {
                            $("#recipient_name").trigger('change.select2');
                        } else {
                            $("#recipient_name").trigger('change');
                        }
                    }
                });
                //  $('#recipient_name').val(data.contact_data.company_name);
                $('#recipient_name').selectpicker('refresh');
                $('#recipient_contact').val(data.contact_data.phone);
                $('#recipient_email').val(data.contact_data.email);
                $('#recipient_address').val(data.contact_data.mailing_address);
                /*$('#recipient_city').val(data.contact_data.city_name);
                $('#recipient_city_id').val(data.contact_data.city_id);*/
                if(data.contact_data.city_id==""){
                    $("#recipient_city").val(data.contact_data.city_id);
                    $("#recipient_city").trigger('change');
                   }else{
                    var option = new Option(data.contact_data.city_name, data.contact_data.city_id, true, true);
                    $("#recipient_city").append(option).trigger('change');
                   }
                $('#recipient_state').val(data.contact_data.state_id);
                $('#recipient_state').selectpicker('refresh');
                $('#recipient_zip').val(data.contact_data.zip);
//                    $('#recipient_county').val(data.contact_data.country_id);
//                    $('#recipient_county').selectpicker('refresh');
                $('#contactModal').modal('hide');
            }
            $('input').each(function () {
                if ($(this).val().length > 0) {
                    $(this).addClass('not-empty');
                } else {
                    $(this).removeClass('not-empty');
                }
            });
        },
    });
});//Add recipient contact dynamic
/**************************Add address book modal*******************************/
/*********************Update cyo Recipiens and contacts************************/
$('.edit_recipient').on('click', function () {
    var recipient_id = $(this).attr('data-id');
    $('.msg-success').hide();
    $('.update_recipient').prop('disabled', false);
    $('.add_recipient').prop('disabled', true);
    $('.update_contact').prop('disabled', false);
    // $('.add_contact_details').prop('disabled', true);
    $.ajax({
        data: {"id": recipient_id},
        type: 'POST',
        url: get_recipient_data, // point to server-side PHP script
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            if (response.success) {
                console.log(response);
                $("#category_id").val(response.recipient.category_id);
                $('#category_id').selectpicker('refresh');
                $('#recipient_name').val(response.recipient.name).trigger('change.select2');

                $('#recipient_contact').val(response.recipient.contact);
                $('#recipient_email').val(response.recipient.email);
                $('#recipient_address').val(response.recipient.address);
                $('#recipient_address2').val(response.recipient.address2);
                $('#recipient_address3').val(response.recipient.address3);
                /*$('#recipient_city').val(response.recipient.city_name);
                $('#recipient_city_id').val(response.recipient.city_id);*/
                var option = new Option(response.recipient.city_name, response.recipient.city_id, true, true);
                $("#recipient_city").append(option).trigger('change');
                $('#recipient_state').val(response.recipient.state_id);
                $('#recipient_state').selectpicker('refresh');
                $('#recipient_zip').val(response.recipient.zip);
                $('#recipient_id').val(recipient_id);
                $('#recipient_attn').val(response.recipient.attn);
                $('#recipt_id').val(response.recipient.contact_id);
                var urlzipcode = $('#contact_zipurl').val();
                $.ajax({
                    url: urlzipcode,
                    type: 'post',
                    dataType: "json",
                    data: {
                        zipcode: response.recipient.zip,
                        cityName:response.recipient.city_id
                    },
                    success: function(data) {
                        $(".new_reipient_zip").val(data[0].label);
                    }
                });

                $('input,textarea').each(function () {
                    if ($(this).val().length > 0) {
                        $(this).addClass('not-empty');
                    } else {
                        $(this).removeClass('not-empty');
                    }
                });
            }
        }
    });
});
$('.update_recipient').on('click', function () {
    $('.msg-success').show();
});
$('.update_contact').on('click', function () {
    var recipt_id = $('#recipient_name').find(':selected').attr('data-id');

    if (recipt_id != null) {
        $.ajax({
            data: $('#store_recipients').serialize(),
            type: 'POST',
            url: update_address_book, // point to server-side PHP script
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                if (response.success) {
//                $('.msg-success').show();
//                $('.msg-success').html('Contact successfully updated');
                    alert('Contact successfully updated');
                }
            }
        });
    } else {
        alert("Name is not exist in the address book.Please add it to address book")
    }
});


$('.check_button_val').on('click', function () {
    $('#continue_val').val($(this).val());
});

/*********************Update cyo Recipiens and contacts************************/


$("#county").autocomplete({

    source: function (request, response) {
        $.ajax({
            url: src_county,
            dataType: "json",
            data: {
                term: request.term
            },
            beforeSend: function () {
                $('#recipientcountyloader').show();
            },
            complete: function () {
                $('#recipientcountyloader').hide();
            },
            success: function (data) {

                response($.map(data, function (item) {
                    return {
                        value: item.value,
                        id: item.id     // EDIT
                    }
                }));

            },

        });
    },
    select: function (event, ui) {

        $("#recipient_county_id").val(ui.item.id);
        $("#county").val(ui.item.value);
    },
    change: function (event, ui) {
        if (ui.item == null) {
            $(this).val("");
            $(this).focusout();
            $(this).focus();
        }
    }
});
$("#notary_county").autocomplete({

    source: function (request, response) {
        $.ajax({
            url: src_county,
            dataType: "json",
            data: {
                term: request.term
            },
            beforeSend: function () {
                $('#recipientnotary_countyloader').show();
            },
            complete: function () {
                $('#recipientnotary_countyloader').hide();
            },
            success: function (data) {

                response($.map(data, function (item) {
                    return {
                        value: item.value,
                        id: item.id     // EDIT
                    }
                }));
            },
        });
    },
    select: function (event, ui) {

        $("#recipient_notary_county_id").val(ui.item.id);
        $("#notary_county").val(ui.item.value);
    },
    change: function (event, ui) {
        if (ui.item == null) {
            $(this).val("");
            $(this).focusout();
            $(this).focus();
        }
    }
});
$("#project_county").autocomplete({

    source: function (request, response) {
        $.ajax({
            url: src_county,
            dataType: "json",
            data: {
                term: request.term
            },
            beforeSend: function () {
                $('#recipientproject_countyloader').show();
            },
            complete: function () {
                $('#recipientproject_countyloader').hide();
            },
            success: function (data) {

                response($.map(data, function (item) {
                    return {
                        value: item.value,
                        id: item.id     // EDIT
                    }
                }));
            },
        });
    },
    select: function (event, ui) {

        $("#recipient_project_county_id").val(ui.item.id);
        $("#project_county").val(ui.item.value);
    },
    change: function (event, ui) {
        if (ui.item == null) {
            $(this).val("");
            $(this).focusout();
            $(this).focus();
        }
    }
});
$("#recorded_county").autocomplete({

    source: function (request, response) {
        $.ajax({
            url: src_county,
            dataType: "json",
            data: {
                term: request.term
            },
            beforeSend: function () {
                $('#recipientrecorded_countyloader').show();
            },
            complete: function () {
                $('#recipientrecorded_countyloader').hide();
            },
            success: function (data) {

                response($.map(data, function (item) {
                    return {
                        value: item.value,
                        id: item.id     // EDIT
                    }
                }));
            },
        });
    },
    select: function (event, ui) {

        $("#recipient_recorded_county_id").val(ui.item.id);
        $("#recorded_county").val(ui.item.value);
    },
    change: function (event, ui) {
        if (ui.item == null) {
            $(this).val("");
            $(this).focusout();
            $(this).focus();
        }
    }
});




/* code for verifying recipient address */
//Verify address code
$(document).ready(function () {
    $('#myModal').modal('hide');
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('#add_recipients').on('click', function (event) {
        $('#continue_recipient').val('add');
    });
    $('#update_recipients').on('click', function (event) {
        $('#continue_recipient').val('update');
    });
    /* code for verifying recipient address */
    $('#add_recipients,#update_recipients').on('click', function (event) {
        event.preventDefault();
        var form_valid = $('#store_recipients').parsley().validate();
        var url ='';
            if ($('#store_recipients').attr('action').indexOf("#") == -1 ){
                url += '#recipient';
            }
        if (form_valid != false)
        {
            $('#pleaseWait').modal('show');
            var address = $('#recipient_address').val();
            var state = $('#recipient_state').val();
            var city = $('#recipient_city').val();
            var zip = $('#recipient_zip').val();
            var name = $('#recipient_name').val();

            //Updated Fields
            var address2 = $('#recipient_address2').val();
            var address3 = $('#recipient_address3').val();
            var recipient_department = $('#recipient_department').val();
            var recipient_contact = $('#recipient_contact').val();
            var recipient_email = $('#recipient_email').val();
            var recipient_attn = $('#recipient_attn').val();
            //Updated Fields

            $.ajax({
                type: 'POST',
                url: getVerifiedAddress, 
                data: {address: address, state: state, city: city, zip: zip, name: name, address2:address2,address3:address3, recipient_department: recipient_department, recipient_contact: recipient_contact, recipient_email: recipient_email, attn: recipient_attn },
                success: function (data1) {
                    $('#pleaseWait').modal('hide');
                    var data = JSON.parse(data1);
                    var alternate_addresses = '';
                    if (data.response == 'success') {
                        var message = "This address is fully verified";
                        $('#verified_cyo_usps_address').val(data.data);
                        $('#address_verification_message').html(message);
                        $('#myModal').modal('show');
                        $('#store_recipients').submit();
                    } else if (data.response == 'error' && data.alternate_addresses) {
                        $('#alternate_addresses').html("<span style='color:red'>" + data.message + "</span><br/>" + data.alternate_addresses);
                        $('input[name="select_addr"]').attr('required', 'required');
                        $('#address_verification_modal').modal('show');
                    } else {
                        if (data.overridehashpresent == true)
                        {
                            $('#verified_cyo_usps_address').val(data.data);
                            var message = data.message + 'Do you accept this address?';
                            $('#address_verification_message').html(message);
                            $('#myModal').modal('show');
                            $('#myModal .modal-footer').html('<button type="button" class="btn btn-success" id="accept_address">Accept</button><button type="button" class="btn btn-warning" id="reject_address" data-dismiss="modal">Reject</button>');
                        } else
                        {
                            $('#address_verification_message').html(data.message);
                            $('#myModal').modal('show');
                        }
                    }
                },
                error: function (data) {
                    $('#pleaseWait').modal('hide');
                    console.log('error with AJAX call for addr verification');
                }
            });
        } else
        {
            console.log('error in form');
            return false;
        }
    });

    $('#reject_address').click(function () {
        var message = "You need to try again with more specific address details";
        $('#address_verification_message').html(message);
        $('#myModal .modal-footer').html('<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');
        $('#myModal').modal('show');
    });

    $(document).on('click', '#accept_address', function () {
        $('#myModal .modal-footer').html('<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');
                if ($('#store_recipients').attr('action').indexOf("#") == -1 ){
                    $('#store_recipients').attr('action', $('#store_recipients').attr('action')+ '#recipient');
                }         
            $('#store_recipients').submit();
    });

    $('#createLabel,#address_tab').click(function (e) {
        var this_id = $(this).attr('id');
        if (this_id == 'address_tab')
        {
            e.preventDefault();
            if (verified_address_count == 'no')
            {
                $('#recipientVerifiedCountModal').modal('show');
                return false;
            } else
            {
                return true;
            }
        } else
        {
            if (verified_address_count == 'no')
            {
                $('#recipientVerifiedCountModal').modal('show');
            } else
            {
                $('#acceptance_btn').click();
            }
        }
    })
    $('#rejection_btn').click(function () {
        $('#recipientVerifiedCountModal').modal('hide');
    });

    $(document).on('click', '#acceptance_btn', function () {
        var id = $('#wo_id').val();
        $.ajax({
            data: {'id': id},
            type: "post",
            url: checkFieldsUrl,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                if (response == 0) {
                    //$("button[name=generate]").addAttr("disabled");
                    $('.display-ib li').removeClass('active');
                    $('#address_tab').addClass('active');
                    $('.notice-tabs .tab-pane').removeClass('active');
                    $('#verify_address').addClass('active');
                } else if (response == 1) {
                    $("a[name=generate]").removeAttr("disabled");
                    $("a[name=generate]").removeAttr("title");
                    $("a[name=generate]").removeAttr("style");
                    $("input[name=create-label]").removeAttr("disabled");
                    $("input[name=create-label]").removeAttr("title");
                    $('.display-ib li').removeClass('active');
                    $('#address_tab').addClass('active');
                    $('.notice-tabs .tab-pane').removeClass('active');
                    $('#verify_address').addClass('active');
                }
            }
        });
        $('#recipientVerifiedCountModal').modal('hide');
    });

    $('#createLabel1, #save_labels_for_later').click(function () {
        var selected_addr = '';
        //  var valid_form = $('#creating_labels_form').parsley().validate();
        $('#creating_labels_form').parsley().destroy();
        var result_val = true;
        if ($(this).attr('id') == 'save_labels_for_later')
        {
            $('#saveLabelData').val('yes');
        } else
        {
            $('#saveLabelData').val('no');
            $('[data-parsley-group="valtab"]').each(function () {
                $(this).parsley().validate({force: true});
                if ($(this).parsley().isValid({force: true}) == false) {
                    result_val = false;
                }
            });

            $('[data-parsley-group="checktabaddon"]').each(function () {
                if($(this).closest('.addon_div_class').attr('id') != 'undefined')
                 {
                     $(this).parsley().validate({force: true});
                     if ($(this).parsley().isValid({force: true}) == false) {
                         console.log($(this).closest('.addon_div_class').attr('id'));
                         result_val = false;
                     }
                 }
             });

            /*** if rush hour checked then validate it **/
                if ($('#rush_hour').is(":checked"))
                {
                    $("#rush_hour_charges").parsley().validate({force: true});
                    if ($("#rush_hour_charges").parsley().isValid({force: true}) == false) {
                        result_val = false;
                    }
                } 
        }
//        if (valid_form == true)
//        {
//            $('#pleaseWait').modal('show');
//            $('#creating_labels_form').submit();
//        }
        if (result_val == true)
        {
            $('#pleaseWait').modal('show');
            $('#creating_labels_form').submit();
        } else
        {
            var failingFields = [],
                    parsleyForm = $('#creating_labels_form').parsley();

            for (var i = 0; i < parsleyForm.fields.length; i++)
                if (true !== parsleyForm.fields[i].validationResult && parsleyForm.fields[i].validationResult.length)
                    failingFields.push(parsleyForm.fields[i].$element);

           // console.log(failingFields);
        }
    });

    $('#choose_address').click(function (e) {
        e.preventDefault();
        var selected_addr = '';
        if ($('input:radio[name="select_addr"]:checked').length > 0) {
            if($("#rad_select_addr").is(':checked')){
            var url ='';
                    if ($('#store_recipients').attr('action').indexOf("#") == -1 ){
                        url += '#recipient';
                    } 
                    $('#store_recipients').attr('action',$('#store_recipients').attr('action') + url );                  
            $('#store_recipients').submit();
                }
            $('#verified_cyo_usps_address').val($('input:radio[name="select_addr"]:checked').attr('data-usps-addr'));
            selected_addr = $('input:radio[name="select_addr"]:checked').val();
            selected_addr = selected_addr.split('**');
            //console.log(selected_addr);
            var city = selected_addr[1].charAt(0).toUpperCase() + selected_addr[1].toLocaleLowerCase().slice(1);
                var state = selected_addr[2].charAt(0).toUpperCase() + selected_addr[2].toLocaleLowerCase().slice(1);
            $('#recipient_address').val(selected_addr[0]);
            $('#recipient_zip').val(selected_addr[3]);
            $("#recipient_state option").each(function () {
                if ($(this).text() == state) {
                    $(this).attr('selected', 'selected');
                }
            });
            $("#recipient_state").selectpicker("refresh");
            /*$("#recipient_city option").each(function () {
                if ($(this).text() == city) {
                    $(this).attr('selected', 'selected');
                }
            });*/
             var option = new Option(city, city,null, true, true);
                $("#recipient_city").append(option).trigger('change');
            //$("#recipient_city").selectpicker("refresh");

            $('#address_verification_modal').modal('hide');
        } else {
            var message = "Please select one address";
            $('#address_verification_message').html(message);
            $('#myModal').modal('show');
        }
    });
})
function checkForInputs(sel)
{
    var val = sel.getAttribute('value');
    var name = sel.getAttribute('name');
    var data_value = sel.getAttribute('data-value');
    var cnt = name.split('_');
    cnt = cnt[3];
    switch (val)
    {   
        case 'manual':
                $('input[name="manual_inputs' + cnt + '"]').attr('placeholder', 'Enter manual label here');
                $('input[name="manual_inputs' + cnt + '"]').val(data_value);
                $('input[name="shipping_date' + cnt + '"]').removeAttr('required');
                $('#service_type_' + cnt).removeAttr('required');
                $('#service_type_div_' + cnt).hide();
                $('input[name="manual_amount' + cnt + '"]').show();
                $('input[name="manual_inputs' + cnt + '"]').show();
                $('input[name="shipping_date' + cnt + '"]').hide();
                $('#shipping_dt_alert'+cnt).hide();
                break;
        case 'firm mail':
        case 'next day delivery':
            $('input[name="manual_inputs' + cnt + '"]').attr('placeholder', 'Enter mailing label here');
            $('input[name="manual_inputs' + cnt + '"]').val(data_value);
            $('#service_type_div_' + cnt).hide();
            $('input[name="manual_inputs' + cnt + '"]').show();
            $('input[name="shipping_date' + cnt + '"]').hide();
            $('input[name="manual_amount' + cnt + '"]').hide();
            $('#shipping_dt_alert'+cnt).hide();
            break;
        case 'Cert. USPS':
                $('input[name="manual_inputs' + cnt + '"]').attr('placeholder', 'Enter mailing label here');
                $('input[name="manual_inputs' + cnt + '"]').attr('value', data_value);
                $('input[name="manual_inputs' + cnt + '"]').attr('required', true);
                $('input[name="shipping_date' + cnt + '"]').removeAttr('required');
                $('#service_type_' + cnt).removeAttr('required');
                $('#service_type_div_' + cnt).hide();
                $('input[name="manual_inputs' + cnt + '"]').show();
                $('input[name="shipping_date' + cnt + '"]').hide();
                $('input[name="manual_amount' + cnt + '"]').hide();
                $('#shipping_dt_alert'+cnt).hide();
                break;
        case 'Cert. RR':
                $('input[name="manual_inputs' + cnt + '"]').attr('placeholder', 'Enter mailing label here');
                $('input[name="manual_inputs' + cnt + '"]').attr('value', data_value);
                $('input[name="manual_inputs' + cnt + '"]').attr('required', true);
                $('input[name="shipping_date' + cnt + '"]').removeAttr('required');
                $('#service_type_' + cnt).removeAttr('required');
                $('#service_type_div_' + cnt).hide();
                $('input[name="manual_inputs' + cnt + '"]').show();
                $('input[name="shipping_date' + cnt + '"]').hide();
                $('input[name="manual_amount' + cnt + '"]').hide();
                $('#shipping_dt_alert'+cnt).hide();
                break
        case 'Cert. ER':
                $('input[name="manual_inputs' + cnt + '"]').attr('placeholder', 'Enter mailing label here');
                $('input[name="manual_inputs' + cnt + '"]').attr('value', data_value);
                $('input[name="manual_inputs' + cnt + '"]').attr('required', true);
                $('input[name="shipping_date' + cnt + '"]').removeAttr('required');
                $('#service_type_' + cnt).removeAttr('required');
                $('#service_type_div_' + cnt).hide();
                $('input[name="manual_inputs' + cnt + '"]').show();
                $('input[name="shipping_date' + cnt + '"]').hide();
                $('input[name="manual_amount' + cnt + '"]').hide();
                $('#shipping_dt_alert'+cnt).hide();
                break;
        case 'stamps':
            $('input[name="manual_inputs' + cnt + '"]').hide();
            // $('input[name="manual_inputs' + cnt + '"]').removeAttr('required');
            $('input[name="shipping_date' + cnt + '"]').attr('placeholder', 'Enter shipping date here');
            // $('input[name="shipping_date' + cnt + '"]').attr('required', true);
            $('input[name="shipping_date' + cnt + '"]').show();
            $('#service_type_div_' + cnt).addClass("addon_div_class");
            $('input[name="add_ons_'+cnt+'[]"]').attr('data-parsley-mincheck', '1').parsley();
            $('input[name="add_ons_'+cnt+'[]"]').attr('data-parsley-required', 'true').parsley();
            $('input[name="add_ons_'+cnt+'[]"]').attr('data-parsley-group', 'checktabaddon').parsley();
      
            // $('#service_type_' + cnt).attr('required', true);
            $('#service_type_div_' + cnt).show();
             $('input[name="manual_amount' + cnt + '"]').hide();
            $('#shipping_dt_alert'+cnt).show();           
            break;
        default:
            $('input[name="manual_inputs' + cnt + '"]').hide();
            $('input[name="manual_inputs' + cnt + '"]').attr('value', data_value);
            // $('input[name="manual_inputs' + cnt + '"]').removeAttr('required');
            $('input[name="shipping_date' + cnt + '"]').attr('placeholder', 'Enter shipping date here');
            //$('input[name="shipping_date' + cnt + '"]').attr('required', true);
            $('input[name="shipping_date' + cnt + '"]').hide();
            $('input[name="manual_inputs' + cnt + '"]').hide();
            $('input[name="manual_amount' + cnt + '"]').hide();
            // $('#service_type_' + cnt).attr('required', true);
            $('#service_type_div_' + cnt).hide();
            $('#shipping_dt_alert'+cnt).hide();    
        }
    $(".selectpicker").selectpicker('refresh');
}

//stamps service types,package types and add ons related code
function getPackageTypes(sel) {
    $('#filtered_add_ons_' + cnt).html('');
    var cnt = sel.getAttribute('id');
    cnt = cnt.split('_');
    cnt = cnt[2];
    var service_type = sel.options[sel.selectedIndex].value;
    $('#filtered_package_types_' + cnt).html('');
    if (service_type.length > 0) {
        $.ajax({
            type: 'POST',
            async: false,
            url: getPackageType,
            data: {service_type: service_type, counter: cnt},
            success: function (data) {
                $('#filtered_package_types_' + cnt).html(data);
                $(".selectpicker").selectpicker('refresh');
            },
            error: function (response)
            {
                console.log('error getting package types for stamps');
            }
        });
    }
}

function getAddOns(sel) {
    var cnt = sel.getAttribute('id');
    cnt = cnt.split('_');
    cnt = cnt[2];
    var package_type = sel.options[sel.selectedIndex].value;
    var service_type = $('#service_type_' + cnt).val();
    $('#filtered_add_ons_' + cnt).html('');

    if (package_type.length > 0) {
        $.ajax({
            type: 'POST',
            async: false,
            url: getAddOn,
            data: {service_type: service_type, package_type: package_type, counter: cnt},
            success: function (data) {
                $('#filtered_add_ons_' + cnt).html(data);
                $(".selectpicker").selectpicker('refresh')
            },
            error: function (response)
            {
                console.log('error getting add ons for stamps');
            }
        });
    }
}

function getProhibited(sel) {
    var required = sel.getAttribute('data-required');
    var prohibited = JSON.parse(sel.getAttribute('data-prohibited'));
    var val = sel.getAttribute('id');
    var ele = sel.getAttribute('name');
    cnt = val.split('_');
    cnt = cnt[3];

    //Check if any of the prohibited are selected, if yes inform and uncheck them
    $('input[name="' + ele + '"]').each(function (idx, value) {
        if ($(this).is(':checked') && $(this).val() != $('#' + val).val())
        {
            if (($(this).val() == 13 || $(this).val() == 21) && !$('input[name="' + ele + '"]:eq(0)').is(':checked'))
            {
                $('input[name="' + ele + '"]:eq(0)').prop("checked", true);
                var alert_id = 'alrt' + cnt;
                $('#' + alert_id).html('<b>Return receipt requires the mail to be certified</b>');
                setTimeout(function () {
                    $('#' + alert_id).html('');
                }, 5000);
            }

            var i;
            for (i = 0; i < prohibited.length; i++) {
                if (prohibited[i] == $(this).val())
                {
                    $(this).prop("checked", false);
                }
            }


        }
    });
}
//stamps service types,package types and add ons related code end
//Verify address code end

$('.updateworkorderstatus').click(function () {
    var url = $(this).attr('data-url');
    $.ajax({
        type: "get",
        url: url,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            response = $.parseJSON(response);
            if (response.status == 'success') {
                $('.display-ib li').removeClass('active');
                $('#address_tab').addClass('active');
                $('.notice-tabs .tab-pane').removeClass('active');
                $('#verify_address').addClass('active');

            } else if (response.status == 'error') {

                $('.display-ib li').removeClass('active');
                $('#address_tab').addClass('active');
                $('.notice-tabs .tab-pane').removeClass('active');
                $('#verify_address').addClass('active');

            }

            $('#work_order_update_message').html(response.message);
            $('#successModal').modal('show');
        }
    });
});

//stamps service types,package types and add ons related code end
//Verify address code end

function change_secording_pending_sign_status(status,recording) {
    $('#creating_labels_form').parsley().destroy();
    $('#recording_pending_signature').val(status);
    $('#saveLabelData').val('yes');
    $('#pleaseWait').modal('show');
   
    $.ajax({
        type: "POST",
        url: createlable_url,
        data: $('#creating_labels_form').serialize(),
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            $('#recording_pending_signature').val(0);
            $('#saveLabelData').val('no');
            $('#pleaseWait').modal('hide');
            response = $.parseJSON(response);
            if (response.status == 'success') {
                $('.display-ib li').removeClass('active');
                $('#address_tab').addClass('active');
                $('.notice-tabs .tab-pane').removeClass('active');
                $('#verify_address').addClass('active');
                $('#success-msg').css('display','block');
                $('#success-msg').text(response.message);
                if(recording==3){
                    setTimeout(function(){ window.location = base_url; }, 1000);
                }

            } else if (response.status == 'error') {

                $('.display-ib li').removeClass('active');
                $('#address_tab').addClass('active');
                $('.notice-tabs .tab-pane').removeClass('active');
                $('#verify_address').addClass('active');
                $('#success-msg').removeClass('alert-success msg-success');
                $('#success-msg').addClass('alert-danger msg-danger');

            }

            //$('#work_order_update_message').html(response.message);
            //$('#successModal').modal('show');

        }
    });

}
$(document).ready(function () {
    /*generate PDF */

    $('.generate_pdf').click(function () {
        $('#generatePDFConfirmModal').modal('show');
    });

    $('#generatePDFYes').click(function () {
        $('#generatePDFConfirmModal').modal('hide');

        $('#creating_labels_form').parsley().destroy();
            $('#generate_pdf_flag').val('1');
            $('#saveLabelData').val('yes');
            var data_url = $('.generate_pdf').attr('data-url'); 
            $('#pleaseWait').modal('show');
            $.ajax({
                type: "POST",
                url: createlable_url,
                data: $('#creating_labels_form').serialize(),
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (response) {
                    $('#generate_pdf_flag').val('0');
                    $('#saveLabelData').val('no');
                    $('#pleaseWait').modal('hide');
                    console.log('redirect to ', data_url);
                // document.location.href =base_url;
                    window.open(data_url, '_blank');
                }
            });
    });

    $('#generatePDFNo').click(function () {
        $('#generatePDFConfirmModal').modal('hide');
    });

    /*$('.generate_pdf').click(function () {
        $('#creating_labels_form').parsley().destroy();
        $('#generate_pdf_flag').val('1');
        $('#saveLabelData').val('yes');
        var data_url = $(this).attr('data-url');
        $('#pleaseWait').modal('show');
        $.ajax({
            type: "POST",
            url: createlable_url,
            data: $('#creating_labels_form').serialize(),
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                $('#generate_pdf_flag').val('0');
                $('#saveLabelData').val('no');
                $('#pleaseWait').modal('hide');
         
               // document.location.href =base_url;
                window.open(data_url, '_blank');
            }
        });
    });*/
});
$(window).on('load', function() {

    var self_url = window.location.href;
    if (self_url.indexOf("#")!=-1 ){
        var tab_name = self_url.split("#").pop();

        $("#" +tab_name + "_tab a ").trigger( "click" );
    }

});