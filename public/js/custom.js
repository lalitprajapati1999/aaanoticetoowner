$('.stories-slider').owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    navText: [
        "<i class='fa fa-chevron-left' aria-hidden='true'></i>",
        "<i class='fa fa-chevron-right' aria-hidden='true'></i>"
    ],
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 1
        },
        1000: {
            items: 1
        }
    }
});

$('.selectpicker').selectpicker({

    size: 4
});

/*--accordion---*/
function toggleIcon(e) {
    $(e.target)
            .prev('.panel-heading')
            .find(".more-less")
            .toggleClass('glyphicon-plus glyphicon-minus')
}
$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);

/*--color change--*/
$('.panel-faq').on('show.bs.collapse', function () {
    $(this).addClass('active');
});
$('.panel-faq').on('hide.bs.collapse', function () {
    $(this).removeClass('active');
});


$(document).ready(function () {
    $(window).scroll(function () {
        if ($(this).scrollTop() > 50) {
            $('.navbar-brand').addClass('fixed-brand');
        } else {
            $('.navbar-brand').removeClass('fixed-brand');
        }
    });
    $('.verify_address').change(function () {
        $('.verify_address').each(function () {
            if ($(this).is(':checked'))
            {
                var id = $(this).attr('id').split('-');
                var block_id_to_copy = id[0] + '-address-' + id[1];
                $('#selected_address_' + id[1]).html($('#' + block_id_to_copy).html());
            }
        });
    })
    $('#address_verify_button').click(function () {
        $("ul.nav-tabs li:nth-child(2)").removeClass("active");
        $("ul.nav-tabs li:nth-child(3)").addClass("active");
        $('#verify_address').addClass('active');
    })
});

// $(document).ready(function () {

function checkForInput(element) {
    if ($(element).val().length > 0) {
        $(element).addClass('not-empty');
    } else {
        $(element).removeClass('not-empty');
    }
}

$('input, textarea').each(function () {
    checkForInput(this);
});

// The lines below (inside) are executed on change & keyup
$('input, textarea').on('change keyup', function () {
    checkForInput(this);
});

// });

$(function () {
    $(window).on("load", function () {
        $(".left-sidebar .sidenav-wrapper").mCustomScrollbar();
    });
});
$(document).ready(function () {
    $(".corr-btn").click(function () {
        $(".corr-block").css("width", "415px");
        $("body").addClass("added-block");
        $("body").removeClass("adjust-body");
// $("#sidebar").addClass("adjust-sidebar");
// $("#content").addClass("adjust-content");
    });
    $(".closebtn").click(function () {
        $(".corr-block").css("width", "0");
        $("body").removeClass("added-block");
        $("body").addClass("adjust-body");
// $("#sidebar").removeClass("adjust-sidebar");
// $("#content").removeClass("adjust-content");
    });
    $(".notice-btn").click(function () {
        $(".notes-block").css("width", "415px");
        $("body").addClass("added-block");
        $("body").removeClass("adjust-body");
// $("#sidebar").addClass("adjust-sidebar");
// $("#content").addClass("adjust-content");
    });
    $(".closebtn1").click(function () {
        $(".notes-block").css("width", "0");
        $("body").removeClass("added-block");
        $("body").addClass("adjust-body");
// $("#sidebar").removeClass("adjust-sidebar");
// $("#content").removeClass("adjust-content");
    });
    $(".small-sidebar").click(function () {
        $("body").addClass("adjust-body");
        $("#sidebar").toggleClass("adjust-sidebar");
        $("#content").toggleClass("adjust-content");
    });
});

$(document).ready(function () {
    $(".js-example-tags").select2({
        tags: true
    });
    $('#resetall').on('click', function () {
        window.location.reload();
    });
});

$('.recipient_attns').ckeditor();
$('.projects_address').ckeditor();

// $('.recipient_attns').ckeditor(function() {
//     this.on("blur", function(e) {
//         var bookids = $(".recipient_attns").val();
//         let results = bookids.replace(/<br \/>/gi, "<p>");
//         $(".recipient_attns").val(results);
//     });
// });

$(".projects_address").ckeditor(function() {
    var regex = /^(&nbsp;)*[\s]*([\*]{2})(([a-zA-Z\(\)0-9\-.,_=#:'"\?`;~@$%\\\/|^&\<>+!\s])*(&nbsp;)*)*(<\/p>)$/;
    var regex2 = /^(&nbsp;)+(<\/p>)$/;

    this.on("blur", function(e) {
        var bookid = $(".projects_address").val();
        let result = bookid.replace(/<br \/>/gi, "<p>");
        $(".projects_address").val(result);
        var res = bookid.split("<p>");
        var length = res.length;
        var count = 0
        for (i = 0; i < length; i++) {
            var len = res[i].length;
            if (len > 10) {
                count++;
            }
        }
        $('#project_address_count').val(count);
        for (i = 2; i < length; i++) {
            if (!regex.test(res[i]) && !regex2.test(res[i])) {
                swal({
                    title: "Projects Address!",
                    text: "Please enter multiple addresses separated by ** !!",
                    icon: "error",
                    button: "Ok"
                });
                break;
            }
        }
    });
    $("input[name='continue']").on("click", function(event) {
        var bookid = $(".projects_address").val();
        var res = bookid.split("<p>");
        var length = res.length;
        for (i = 2; i < length; i++) {
            if (!regex.test(res[i]) && !regex2.test(res[i])) {
                swal({
                    title: "Projects Address!",
                    text: "Please enter multiple addresses separated by ** !!",
                    icon: "error",
                    button: "Ok"
                });
                event.preventDefault();
                break;
            }
        }
    });
    $("button[name='continue']").on("click", function(event) {
        var bookid = $(".projects_address").val();
        var res = bookid.split("<p>");
        var length = res.length;
        for (i = 2; i < length; i++) {
            if (!regex.test(res[i]) && !regex2.test(res[i])) {
                swal({
                    title: "Projects Address!",
                    text: "Please enter multiple addresses separated by ** !!",
                    icon: "error",
                    button: "Ok"
                });
                event.preventDefault();
                break;
            }
        }
    });
    $("button[name='submit']").on("click", function(event) {
        var bookid = $(".projects_address").val();
        var res = bookid.split("<p>");
        var length = res.length;
        for (i = 2; i < length; i++) {
            if (!regex.test(res[i]) && !regex2.test(res[i])) {
                swal({
                    title: "Projects Address!",
                    text: "Please enter multiple addresses separated by ** !!",
                    icon: "error",
                    button: "Ok"
                });
                event.preventDefault();
                break;
            }
        }
    });
    $(".val").ckeditor(function() {
        this.on("focusout", function(e) {
            var va = $(".val").val();
            let result = va.replace(/<br \/>/gi, "<p>");
            $(".val").val(result);
        });
    });
});

$(document).ready(function() {

    var s = $(".recipient_name").select2({
        tags: true,
        closeOnSelect: false,
        width: 400,
    });
    var $search = s.data('select2').dropdown.$search;
    s.on("select2:selecting", function(e) {
        $search.val(e.params.args.data.text);
    });
    
     
    $(".new_addresscontact_zip").autocomplete({
        source: function(request, response) {
            // Fetch data
            $('.addresscontact_zip').val(request.term); // save selected id to input
            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': $('meta[name=_token]').attr('content')
                }
            });
            var urlzipcode = $('#contact_zipurl').val();
            $.ajax({
                url: urlzipcode,
                type: 'post',
                dataType: "json",
                data: {
                    zipcode: request.term
                },
                success: function(data) {
                    response(data);
                }
            });
        },
        select: function(event, ui) {
            $('.new_addresscontact_zip').val(ui.item.label); // display the selected text

            return false;
        }
    });
    $('.new_addresscontact_zip').on('autocompleteselect', function(e, ui) {
        var bookid = ui.item.label;
        var res = bookid.split(",");
        $('.newaddresscontactcityid').val(res[1]);
        $('.addresscontact_zip').val(res[0]); // save selected id to input
        $('.addresscontact_city_id')
            .empty()
            .append($("<option/>")
                .val(res[1])
                .text(res[1]))
            .val(res[1])
            .trigger("change");
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': $('meta[name=_token]').attr('content')
            }
        });
        var statename = res[2];
        var contact_stateurl = $('#contact_stateurl').val();
        $.ajax({
            url: contact_stateurl,
            type: 'post',
            dataType: "json",
            data: {
                statename: statename
            },
            success: function(data) {
                $('#contact_state_id').val(data.id);
                $('#contact_state_id').selectpicker('refresh');
            }
        });
    });

    $(".new_reipient_zip").autocomplete({
        source: function(request, response) {
            // Fetch data
            $('.reipient_zip').val(request.term); // save selected id to input
            $('.recipient_zip').val(request.term); // save selected id to input
            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': $('meta[name=_token]').attr('content')
                }
            });
            var urlzipcode = $('#contact_zipurl').val();
            $.ajax({
                url: urlzipcode,
                type: 'post',
                dataType: "json",
                data: {
                    zipcode: request.term
                },
                success: function(data) {
                    response(data);
                }
            });
        },
        select: showResult,

    });

    function showResult(event, ui) {
        $('.new_reipient_zip').val(ui.item.label); // display the selected text
        return false;
    }
    $('.new_reipient_zip').on('autocompleteselect', function(e, ui) {
        var bookid = ui.item.label
        var res = bookid.split(",");
        $('.reipient_zip').val(res[0]); // save selected id to input
        $('.recipient_zip').val(res[0]); // save selected id to input
        $('.new_recipientcity').val(res[1]);
        $('.newaddresscontactcityid').val(res[1]);
        $('.addresscontact_city_id')
            .empty()
            .append($("<option/>")
                .text(res[1]))
            .val(res[1])
            .trigger("change");
        var statename = res[2];
        var contact_stateurl = $('#contact_stateurl').val();
        $.ajax({
            url: contact_stateurl,
            type: 'post',
            dataType: "json",
            data: {
                statename: statename
            },
            success: function(data) {
                $('#recipient_state').val(data.id);
                $('#recipient_state').selectpicker('refresh');
            }
        });
    });

});
