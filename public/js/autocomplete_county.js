  
$("#county").autocomplete({

    source: function (request, response) {
        $.ajax({
            url: src_county,
            dataType: "json",
            data: {
                term: request.term},
            beforeSend: function () {
                $('#recipientcountyloader').show();
            },
            complete: function () {
                $('#recipientcountyloader').hide();
            },
            success: function (data) {

                response($.map(data, function (item) {
                    return {
                        value: item.value,
                        id: item.id     // EDIT
                    }
                }));

            },

        });
    },
    select: function (event, ui) {

        $("#recipient_county_id").val(ui.item.id);
        $("#county").val(ui.item.value);
    }
});


$("#notary_county").autocomplete({

    source: function (request, response) {
        $.ajax({
            url: src_county,
            dataType: "json",
            data: {
                term: request.term},
            beforeSend: function () {
                $('#recipientnotary_countyloader').show();
            },
            complete: function () {
                $('#recipientnotary_countyloader').hide();
            },
            success: function (data) {

                response($.map(data, function (item) {
                    return {
                        value: item.value,
                        id: item.id     // EDIT
                    }
                }));

            },

        });
    },
    select: function (event, ui) {

        $("#recipient_notary_county_id").val(ui.item.id);
        $("#notary_county").val(ui.item.value);
    }
});

$("#project_county").autocomplete({

    source: function (request, response) {
        $.ajax({
            url: src_county,
            dataType: "json",
            data: {
                term: request.term},
            beforeSend: function () {
                $('#recipientproject_countyloader').show();
            },
            complete: function () {
                $('#recipientproject_countyloader').hide();
            },
            success: function (data) {

                response($.map(data, function (item) {
                    return {
                        value: item.value,
                        id: item.id     // EDIT
                    }
                }));

            },

        });
    },
    select: function (event, ui) {

        $("#recipient_project_county_id").val(ui.item.id);
        $("#project_county").val(ui.item.value);
    }
});
$("#recorded_county").autocomplete({

    source: function (request, response) {
        $.ajax({
            url: src_county,
            dataType: "json",
            data: {
                term: request.term},
            beforeSend: function () {
                $('#recipientrecorded_countyloader').show();
            },
            complete: function () {
                $('#recipientrecorded_countyloader').hide();
            },
            success: function (data) {

                response($.map(data, function (item) {
                    return {
                        value: item.value,
                        id: item.id     // EDIT
                    }
                }));

            },

        });
    },
    select: function (event, ui) {

        $("#recipient_recorded_county_id").val(ui.item.id);
        $("#recorded_county").val(ui.item.value);
    }
});