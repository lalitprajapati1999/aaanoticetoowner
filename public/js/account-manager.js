/**********************Research research notes and correction**************/
function countAddress(obj) {
    // console.log(obj.value);
    var str = obj.value;
    var array = str.split("**");
    // console.log(array.length);
    $("#project_address_count").val(array.length);
    $("#project_address_count").addClass('not-empty');
    }
/****************** autoclose datepicker ***********/
// $('.datepicker').datepicker().on('changeDate', function(ev){                 
//     $('.datepicker').datepicker('hide');
// });

$(".submitNote").click(function (e) {
    e.preventDefault();
    $('.note-success').hide();
    $('.note-error').hide();
//   var note = $("input[name=note]").val();
//   var email = $("#note_email").val();
//   var work_order_id = $('#work_order').val();
//   data: {note: note, email: email, doc_id: work_order_id},
    $.ajax({
        type: 'POST',
        url: note_url,
        data: $('#edit_work_order_note').serialize(),
        beforeSend: function () {
            $('#pleaseWait').modal('show');
        },
        complete: function () {
            $('#pleaseWait').modal('hide');
        },
        success: function (data) {
            if (data.success) {
                var d = new Date();
                var strDate = (d.getMonth() + 1) + "-" + d.getDate() + "-" + d.getFullYear();
                $('.note-error').empty();
                $('.note-success').empty();
                $("input[name=note]").val('');
                $("#note_email option").prop("selected", false);
                $('#note_email').selectpicker('refresh');
                $('.note-error').css('display', 'none');
                $('.note-success').show();
                $('.note-success').append('<p>' + data.success + '</p>');
                // $('.notes_div').append('<li>' + data.notes.note + '<br>' + data.notes.email + '</li>');
                $('.notes_div').prepend('<li>' + data.notes.note + '</br>Added by:'+ data.notes.name  + '</br>Created at:' + strDate + '</li>');

            } else if (data.errors) {
                $('.note-error').empty();
                $.each(data.errors, function (key, value) {
                    $('.note-success').empty();
                    $('.note-success').css('display', 'none');
                    $('.note-error').show();
                    $('.note-error').append('<p>' + value + '</p>');
                });
            }

        }

    });
});
$(".submitCorrection").click(function (e) {
    e.preventDefault();
    $('.correction-error').hide();
    $('.correction-success').hide();
    $.ajax({
        type: 'POST',
        url: correction_url,
        data: $('#edit_work_order_correction').serialize(),
        beforeSend: function () {
            $('#pleaseWait').modal('show');
        },
        complete: function () {
            $('#pleaseWait').modal('hide');
        },
        success: function (data) {
            if (data.success) {
                var d = new Date();
                var strDate = (d.getMonth() + 1) + "-" + d.getDate() + "-" + d.getFullYear();
                $('.correction-error').empty();
                $('.correction-success').empty();
                $("input[name=correction]").val('');
                $("#correction_email option").prop("selected", false);
                $('#correction_email').selectpicker('refresh');
                $('.correction-error').css('display', 'none');
                $('.correction-success').show();

                $('.correction-success').append('<p>' + data.success + '</p>');
//                $('.correction_div').append('<li>' + data.corrections.correction + '<br>' + data.corrections.email + '</li>');
                $('.correction_div').prepend('<li>' + data.corrections.correction +'</br>Added by:'+ data.corrections.name + '</br>Created at:' + strDate + '</li>');

            } else if (data.errors) {
                $('.correction-error').empty();
                $.each(data.errors, function (key, value) {
                    $('.correction-success').empty();
                    $('.correction-success').css('display', 'none');
                    $('.correction-error').show();
                    $('.correction-error').append('<p>' + value + '</p>');
                });
            }
        }
    });
});
/**********************Research Recipient City Autocomplete and Name Autopopulate************************************/

$("#recipient_city").autocomplete({
    source: function (request, response) {
        $.ajax({
            url: src,
            dataType: "json",
            data: {
                term: request.term
            },
            beforeSend: function () {
                $('#recipientloader').show();
            },
            complete: function () {
                $('#recipientloader').hide();
            },
            success: function (data) {

                response($.map(data, function (item) {
                    return {
                        value: item.value,
                        id: item.id     // EDIT
                    }
                }));

            },
        });
    },
    select: function (event, ui) {

        $("#recipient_city").val(ui.item.id);
        $("#recipient_city").val(ui.item.value);
    },
    change: function (event, ui) {
        if (ui.item == null) {
            $(this).val("");
            $(this).focusout();
            $(this).focus();
        }
    }
});
$('#recipient_name').on('change', function () {
    $('input[name=contact_name]').val('');
    $('input[name=contact_no]').val('');
    $('input[name=contact_email]').val('');
    $('#contact_address').val('');
    $('#attn').val('');
    $('input[ name=contact_zip]').val('');
    $("#contact_state_id").val('');
    $("#contact_state_id").selectpicker('refresh');
    $("#recipientcity").val('');//$("#recipientcity").val('').trigger('change');
    $('#autocomplete_city_id').val('');
    $('.new_reipient_zip').val('');
    var name = $('#recipient_name').val();
    var customer_id = $('#contact_customer_id').val();
    var recipt_id = $(this).find(':selected').attr('data-id');
     if(!recipt_id){
        $("#recipient_city").val("");
        $("#recipient_city").trigger('change');
    }
    $('#recipt_id').val(recipt_id);
    $.ajax({
        data: {'name': name, 'customer_id': customer_id, 'recipt_id': recipt_id},
        type: "post",
        url: get_contact,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (res) {
            // $('').empty();

            // $('#recipient_name').val(res.company_name);
            $('#recipient_email').val(res.email);
            $('#recipient_mobile').val(res.mobile);
            $('#recipient_contact').val(res.phone);
            $('#recipient_zip').val(res.zip);
            $('#recipient_address').val(res.mailing_address);
            $('#recipient_address2').val('');
            $('#recipient_address3').val('');
            $('#recipient_fax').val(res.fax);
            $("#recipient_state").val(res.state_id);
            $("#recipient_state").selectpicker("refresh");
            var urlzipcode = $('#contact_zipurl').val();
            $.ajax({
                url: urlzipcode,
                type: 'post',
                dataType: "json",
                data: {
                    zipcode: res.zip,
                    cityName:res.city_id
                },
                success: function(data) {
                    $(".new_reipient_zip").val(data[0].label);
                }
            });

            //$('#recipient_city').val(res.city_id);
           // $("#recipient_city").val(res.city_name).trigger('change')
           // $("#recipient_city").selectpicker("refresh");
           if(res.city_id==""){
            $("#recipientcity").val(res.city_id);
           // $("#recipient_city").trigger('change');
           }else{
            var option = new Option(res.city_name, res.city_id, true, true);
            $("#recipientcity").val(res.city_id) //append(option).trigger('change');
           }
        //   if(res.city_id==""){
        //     $("#recipient_city").val(res.city_id);
        //     $("#recipient_city").trigger('change');
        //   }else{
        //     var option = new Option(res.city_name, res.city_id, true, true);
        //     $("#recipient_city").append(option).trigger('change');
        //   }
          
           // $('#recipient_city').val(res.city_name);
           // $('#recipient_city_id').val(res.city_id);
            $('#recipient_attn').val(res.attn);
            $('input,textarea').each(function () {
                if ($(this).val().length > 0) {
                    $(this).addClass('not-empty');
                } else {
                    $(this).removeClass('not-empty');
                }
            });
        },
    });

});
//Edit Recipient
$('.edit_recipient').on('click', function () {
    var recipient_id = $(this).attr('data-id');
    $('.msg-success').hide();
    $('.update_recipient').prop('disabled', false);
    $('.add_recipient').prop('disabled', true);
    $('.update_contact').prop('disabled', false);
    // $('.add_contact_details').prop('disabled', true);
    $.ajax({
        data: {"id": recipient_id},
        type: 'POST',
        url: get_recipient_data, // point to server-side PHP script
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            if (response.success) {
                console.log(response);
                $("#category_id").val(response.recipient.category_id);
                $('#category_id').selectpicker('refresh');
                $('#recipient_name').val(response.recipient.name).trigger('change.select2');
//                $('#recipient_name').val(response.recipient.name);
//                $('#recipient_name').trigger('change');
                $('#recipient_contact').val(response.recipient.contact);
                $('#recipient_email').val(response.recipient.email);
                $('#recipient_address').val(response.recipient.address);
                $('#recipient_address2').val(response.recipient.address2);
                $('#recipient_address3').val(response.recipient.address3);
                /*$('#recipient_city').val(response.recipient.city_name);
                $('#recipient_city_id').val(response.recipient.city_id);*/
                var option = new Option(response.recipient.city_name, response.recipient.city_id, true, true);
                $("#recipientcity").val(response.recipient.city_id);//$("#recipientcity").append(option).trigger('change');
                $('#recipient_state').val(response.recipient.state_id);
                $('#recipient_state').selectpicker('refresh');
                $('#recipient_zip').val(response.recipient.zip);
                $('#recipient_attn').val(response.recipient.attn);
                $('#recipient_id').val(recipient_id);
                $('#recipt_id').val(response.recipient.contact_id);
                var urlzipcode = $('#contact_zipurl').val();
                $.ajax({
                    url: urlzipcode,
                    type: 'post',
                    dataType: "json",
                    data: {
                        zipcode: response.recipient.zip,
                        cityName:response.recipient.city_id
                    },
                    success: function(data) {
                        $(".new_reipient_zip").val(data[0].label);
                    }
                });
                $('input,textarea').each(function() {
                    if ($(this).val().length > 0) {
                        $(this).addClass('not-empty');
                    } else {
                        $(this).removeClass('not-empty');
                    }
                });
            }
        }
    });
});
//Add Address Book
$('#add_contact,#add_contact_contracted_by').click(function () {
    var all_val = 1;
      console.log('in');
    $('#selected_address_book').val('');
    if ($(this).hasClass('contracted_by_recipient')) {
        var recipt_id ='';
        var customer_id =customer_ids;
        var name =$('#contracted_by_select').val();
      
        $.ajax({
        data: {'name': name, 'recipt_id': recipt_id , "customer_id" : customer_id},
        type: "post",
        url: get_contact,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (res) {   
            console.log(res); 
                $('.addressattn').val(res.attn);
                $('.addresscontact_no').val(res.phone);
                $('.addresscontact_email').val(res.email);
                $('.addresscontact_address').val(res.company_address);
                $('.newaddresscontactcityid').val(res.city_id);
                //     .empty() 
                //     .append($("<option/>")
                //         .val(res.city_id)
                //         .text(res.city_name))
                //     .val(res.city_id)
                //     .trigger("change");
                $(".addresscontact_state_id").val(res.state_id); //.trigger('change');
                $('.addresscontact_zip').val(res.zip);
                var urlzipcode = $('#contact_zipurl').val();
                $.ajax({
                    url: urlzipcode,
                    type: 'post',
                    dataType: "json",
                    data: {
                        zipcode: res.zip,
                        cityName:res.city_id
                    },
                    success: function(data) {
                        $(".new_addresscontact_zip").val(data[0].label);
                    }
                });
            $('#contact_id').val(res.id);
        }

    });
        $('input[name=contact_name]').val($('#contracted_by_select').val());
        $('input[name=old_contact_name]').val($('#contracted_by_select').val());
        $('input[name=contact_name]').addClass('not-empty');
        $('#selected_address_book').val('project');
    } else if ($('#recipient_name').val() != "") {
        var recipt_id="";
        var customer_id =customer_ids;
        var name =$('#recipient_name').val();
        $.ajax({
            data: {'name': name, 'recipt_id': recipt_id, "customer_id": customer_id },
            type: "post",
            url:get_contact,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(res) {
                console.log(res);
                var zipcode = $('#recipient_zip').val();
                var urlzipcode = $('#contact_zipurl').val();
                $.ajax({
                    url: urlzipcode,
                    type: 'post',
                    dataType: "json",
                    data: {
                        zipcode: zipcode,
                        cityName:res.city_id
                    },
                    success: function(data) {
                        $(".new_addresscontact_zip").val(data[0].label);
                    }
                });
                $('#contact_id').val(res.id);
                $('#recipient_city').val(res.city_id);
      
        $('input[name=contact_name]').val($('#recipient_name').val());
        $('input[name=contact_no]').val($('#recipient_contact').val());
        $('input[name=contact_email]').val($('#recipient_email').val());
        $('#contact_address').val($('#recipient_address').val());
        $('input[name=contact_zip]').val($('#recipient_zip').val());
        $("#contact_state_id").val($('#recipient_state').val());
        $("#contact_state_id").selectpicker('refresh');
        $("#attn").val($('#recipient_attn').val());
        var city_val = $("#recipient_city").select2('data');
        var option = new Option(city_val[0]['text'], city_val[0]['id'], true, true);
         console.log(res.city_id,'----------res.city_id ------------')

         $(".newaddresscontactcityid").val(res.city_id);//$("#recipientcity").append(option).trigger('change');
        //$("#contact_city_id").val($('#recipient_city').val());
      //  $('#autocomplete_city_id').val($('#recipient_city').val());
        //$("#contact_city_id").selectpicker('refresh');
      }
        });
        $('input,textarea').addClass('not-empty');

        all_val = 0;
    } else {
        $('input[name=contact_name]').val('');
    }
    // $('input[name=contact_name]').val('');
    if (all_val) {
        $('input[name=contact_no]').val('');
        $('input[name=contact_email]').val('');
        $('#contact_address').val('');
        $('#attn').val('');
        $('input[ name=contact_zip]').val('');
        $("#contact_state_id").val('');
        $("#contact_state_id").selectpicker('refresh');
        //$("#contact_city_id").val('');
        $("#recipientcity").val('');//$("#recipientcity").val('').trigger('change')
        $('#autocomplete_city_id').val('');
    }
//        $("#contact_country").val('');
//        $("#contact_country").selectpicker('refresh');
    $('#name-error').html('');
    $('#address-error').html('');
    $('#city-error').html('');
    $('#state-error').html('');
    $('#zip-error').html('');
    $('#success-msg').css('display', 'none');

//            $('#contactModal').modal('show');
});
$(".business_phone_validation").mask("(999) 999-9999");
$(".email_validation").attr("data-parsley-trigger", "change focusout")
        .attr("data-parsley-type", "email")
        .parsley();

$(".zip_validation").attr("data-parsley-trigger", "change focusout")
        .attr("data-parsley-type", "digits")
        .attr("data-parsley-minlength", "5")
        .attr("data-parsley-maxlength", "5")
        .parsley();

$('body').on('click', '#submit_contact', function () {
    var recipientForm = $('#add_recipient_contact');
    //    var formData = recipientForm.serialize();

    $('#name-error').html("");
    $('#contact-error').html("");
    $('#email-error').html("");
    $('#address-error').html("");
    $('#city-error').html("");
    $('#state-error').html("");
    $('#zip-error').html("");
    //  $('#country-error').html("");
    $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name=_token]').attr('content')
        }
    });
    $.ajax({
        url: contact_add,
        type: 'POST',
        data: {
            doc_id: $('#doc_id_recipient').val(),
            contact_name: $('#contact_name').val(),
            contact_id: $('#contact_id').val(),
            contact_no: $('#contact_no').val(),
            contact_email: $('#contact_email').val(),
            contact_address: $('#contact_address').val(),
            contact_state_id: $('#contact_state_id').val(),
            contact_zip: $('#contact_zip').val(),
            contact_city_id: $('#recipient_city').val(),
            attn: $('#attn').val(),
            //   contact_country: $('#contact_country').val(),
        },
        success: function (data) {
            console.log(data);
            if (data.error_msg) {
                $('#error-msg').css('display', 'block');
                $('#error-msg').html(data.error_msg);
                $('#success-msg').css('display', 'none');
                $('#success-msg').html('');
            }
            if (data.errors) {

                if (data.errors.contact_name) {
                    $('#name-error').html(data.errors.contact_name[0]);
                }
                if (data.errors.contact_address) {
                    $('#address-error').html(data.errors.contact_address[0]);
                }
                if (data.errors.contact_city_id) {
                    $('#city-error').html(data.errors.contact_city_id[0]);
                }
                if (data.errors.contact_state_id) {
                    $('#state-error').html(data.errors.contact_state_id[0]);
                }
//                        if (data.errors.contact_country) {
//                            $('#country-error').html(data.errors.contact_country[0]);
//                        }
                if (data.errors.contact_zip) {
                    $('#zip-error').html(data.errors.contact_zip[0]);
                }
                if (data.errors.contact_no) {
                    $('#contact-error').html(data.errors.contact_no[0]);
                }
                $('#error-msg').css('display', 'none');
                $('#error-msg').html(data.errors);
                $('#success-msg').css('display', 'none');
                $('#success-msg').html('');

            }
            if (data.success) {
                $('#success-msg').css('display', 'block');
                $('#success-msg').html(data.success);
                $('#error-msg').css('display', 'none');
                $('#error-msg').html('');
                $.ajax({
                    data: {"doc_id": $('#doc_id_recipient').val()},
                    type: 'POST',

                    url: get_recipient_contacts, // point to server-side PHP script
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },

                    success: function (response) {
                        var selected_address_book = $("#selected_address_book").val();
                        var recipient_name_selected = $('#recipient_name').val();
                        var contracted_by_select = $('#contracted_by_select').val();


                        $("#contracted_by_select").empty("");

                        $("<option value=''>Select</option>").appendTo('#contracted_by_select');
                        $.each(response.contacts, function (i, obj)
                        {

                            if (i == 0 && selected_address_book == "project") {
                                var d_data = "<option value='" + obj.company_name + "' selected='selected'>" + obj.company_name + "</option>";
                            } else {
                                // if (selected_address_book != "project" && obj.company_name == contracted_by_select) {
                                    if (obj.company_name == data.contact_data.company_name) {
                                    var d_data = "<option value='" + obj.company_name + "' selected='selected'>" + obj.company_name + "</option>";
                                } else {
                                    var d_data = "<option value='" + obj.company_name + "'>" + obj.company_name + "</option>";
                                }
                            }


                            $(d_data).appendTo('#contracted_by_select');
                        });
                        $("#contracted_by_select").trigger('change');



                        $("#recipient_name").empty("");
                        $("<option value=''>Select</option>").appendTo('#recipient_name');
                        $.each(response.contacts, function (i, obj)
                        {
                            if (i == 0 && selected_address_book != "project") {
                                var div_data = "<option data-id='" + obj.id + "' value='" + obj.company_name + "' selected='selected'>" + obj.company_name + "</option>";
                            } else {
                                if (obj.company_name == data.contact_data.company_name) {
                                    var div_data = "<option data-id='" + obj.id + "' value='" + obj.company_name + "' selected='selected'>" + obj.company_name + "</option>";
                                } else {
                                    var div_data = "<option data-id='" + obj.id + "' value='" + obj.company_name + "'>" + obj.company_name + "</option>";
                                }
                            }
                            $(div_data).appendTo('#recipient_name');
                        });
                        if (selected_address_book == "project") {
                            $("#recipient_name").trigger('change.select2');
                        } else {
                            $("#recipient_name").trigger('change');
                        }

                    }
                });
                // $('#recipient_name').val(data.contact_data.company_name).trigger('change');
                // $('#recipient_name').selectpicker('refresh');
                $('#recipient_contact').val(data.contact_data.phone);
                $('#recipient_email').val(data.contact_data.email);
                $('#recipient_address').val(data.contact_data.mailing_address);
                /*$('#recipient_city').val(data.contact_data.city_name);
                $('#recipient_city_id').val(data.contact_data.city_id);*/
                if(data.contact_data.city_id==""){
                    $("#recipient_city").val(data.contact_data.city_id);
                    $("#recipient_city").trigger('change');
                   }else{
                    var option = new Option(data.contact_data.city_name, data.contact_data.city_id, true, true);
                    $("#recipient_city").append(option).trigger('change');
                   }
                $('#recipient_state').val(data.contact_data.state_id);
                $('#recipient_state').selectpicker('refresh');
                $('#recipient_zip').val(data.contact_data.zip);
//                        $('#recipient_country').val(data.contact_data.country_id);
//                        $('#recipient_country').selectpicker('refresh');
$('#contactModal').modal('hide');

            }
            $('input').each(function () {
                if ($(this).val().length > 0) {
                    $(this).addClass('not-empty');
                } else {
                    $(this).removeClass('not-empty');
                }
            });
            $('textarea').each(function () {
                if ($(this).val().length > 0) {
                    $(this).addClass('not-empty');
                } else {
                    $(this).removeClass('not-empty');
                }
            });
        },
    });
});
$('.update_contact').on('click', function () {
    var recipt_id = $('#recipient_name').find(':selected').attr('data-id');

    if (recipt_id != null) {
        $.ajax({
            data: $('#store_recipients').serialize(),
            type: 'POST',
            url: update_address_book, // point to server-side PHP script
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                if (response.success) { //                $('.msg-success').show();
//                $('.msg-success').html('Contact successfully updated');
                    alert('Contact successfully updated');
                }
            }
        });
    } else {
        alert("Name is not exist in the address book.Please add it to address book")
    }
});
$('.update_recipient').on('click', function () {
    $('.msg-success').show();
});
/***********************************************************************************************
 /**********************Create your own and research notes and correction**************/
/*county */
$(document).ready(function () {

    $("#contact_city_id").autocomplete({

        source: function (request, response) {
            $.ajax({
                url: src,
                dataType: "json",
                data: {
                    term: request.term
                },
                beforeSend: function () {
                    $('#recipientpopuploader').show();
                },
                complete: function () {
                    $('#recipientpopuploader').hide();
                },
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            value: item.value,
                            id: item.id     // EDIT
                        }
                    }));

                },

            });
        },
        select: function (event, ui) {

            $("#autocomplete_city_id").val(ui.item.id);
            $("#contact_city_id").val(ui.item.value);
        },
        change: function (event, ui) {
            if (ui.item == null) {
                $(this).val("");
                $(this).focusout();
                $(this).focus();
            }
        }

    });

    $('#county').select(function(){
        
    })
    $("#county").autocomplete({

        source: function (request, response) {
            $.ajax({
                url: src_county,
                dataType: "json",
                data: {
                    term: request.term
                },
                beforeSend: function () {
                    $('#recipientcountyloader').show();
                },
                complete: function () {
                    $('#recipientcountyloader').hide();
                },
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            value: item.value,
                            id: item.id     // EDIT
                        }
                    }));

                },

            });
        },
        select: function (event, ui) {

            $("#recipient_county_id").val(ui.item.id);
            $("#county").val(ui.item.value);
        },
        change: function (event, ui) {
            if (ui.item == null) {
                $(this).val("");
                $(this).focusout();
                $(this).focus();
            }
        }
    });


    $("#notary_county").autocomplete({

        source: function (request, response) {
            $.ajax({
                url: src_county,
                dataType: "json",
                data: {
                    term: request.term
                },
                beforeSend: function () {
                    $('#recipientnotary_countyloader').show();
                },
                complete: function () {
                    $('#recipientnotary_countyloader').hide();
                },
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            value: item.value,
                            id: item.id     // EDIT
                        }
                    }));

                },

            });
        },
        select: function (event, ui) {

            $("#recipient_notary_county_id").val(ui.item.id);
            $("#notary_county").val(ui.item.value);
        },
        change: function (event, ui) {
            if (ui.item == null) {
                $(this).val("");
                $(this).focusout();
                $(this).focus();
            }
        }
    });

    $("#project_county").autocomplete({

        source: function (request, response) {
            $.ajax({
                url: src_county,
                dataType: "json",
                data: {
                    term: request.term
                },
                beforeSend: function () {
                    $('#recipientproject_countyloader').show();
                },
                complete: function () {
                    $('#recipientproject_countyloader').hide();
                },
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            value: item.value,
                            id: item.id     // EDIT
                        }
                    }));

                },

            });
        },
        select: function (event, ui) {

            $("#recipient_project_county_id").val(ui.item.id);
            $("#project_county").val(ui.item.value);
        },
        change: function (event, ui) {
            if (ui.item == null) {
                $(this).val("");
                $(this).focusout();
                $(this).focus();
            }
        }
    });
    $("#recorded_county").autocomplete({

        source: function (request, response) {
            $.ajax({
                url: src_county,
                dataType: "json",
                data: {
                    term: request.term
                },
                beforeSend: function () {
                    $('#recipientrecorded_countyloader').show();
                },
                complete: function () {
                    $('#recipientrecorded_countyloader').hide();
                },
                success: function (data) {

                    response($.map(data, function (item) {
                        return {
                            value: item.value,
                            id: item.id     // EDIT
                        }
                    }));

                },

            });
        },
        select: function (event, ui) {

            $("#recipient_recorded_county_id").val(ui.item.id);
            $("#recorded_county").val(ui.item.value);
        },
        change: function (event, ui) {
            if (ui.item == null) {
                $(this).val("");
                $(this).focusout();
                $(this).focus();
            }
        }
    });
});

