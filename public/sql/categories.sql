-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 03, 2019 at 09:33 AM
-- Server version: 5.7.26-0ubuntu0.16.04.1
-- PHP Version: 7.1.28-1+ubuntu16.04.1+deb.sury.org+3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `notice-to-owner-clean`
--

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `name`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Owner 1', NULL, '2018-10-28 21:26:49'),
(2, 1, 'General Contractor', NULL, NULL),
(3, 1, 'Sub Contractor', NULL, NULL),
(4, 1, 'Sub-Sub Contractor', NULL, NULL),
(5, 1, 'Supplier', NULL, NULL),
(6, 1, 'Laborer', NULL, NULL),
(7, 1, 'Architect', NULL, NULL),
(8, 1, 'Engineer', NULL, NULL),
(9, 1, 'Surety(Bond Company)', NULL, NULL),
(10, 1, 'Designated Recipient', NULL, NULL),
(11, 1, 'Certified Recipient', NULL, NULL),
(12, 1, 'Lender', NULL, NULL),
(21, NULL, 'Contracted By', '2018-11-11 15:02:30', '2018-11-11 15:02:30'),
(22, NULL, 'Owner 2', NULL, '2018-10-28 21:26:49');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
