-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 03, 2019 at 09:35 AM
-- Server version: 5.7.26-0ubuntu0.16.04.1
-- PHP Version: 7.1.28-1+ubuntu16.04.1+deb.sury.org+3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `notice-to-owner-clean`
--

--
-- Dumping data for table `packages`
--

INSERT INTO `packages` (`id`, `name`, `charge`, `description`, `cancellation_charge`, `created_at`, `updated_at`) VALUES
(1, 'Base package', NULL, 'Base package', NULL, '2018-11-02 11:47:01', '2018-11-02 11:47:01'),
(2, 'Create your own monthly', '279.00', 'Create your own monthly for $279', NULL, '2018-11-02 11:47:01', '2019-03-28 14:02:21'),
(3, 'Create your own yearly', '229.00', 'Create your own yearly for $229.00', NULL, '2018-11-02 11:47:01', '2019-03-28 14:11:42');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
