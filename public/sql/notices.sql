-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 02, 2019 at 05:04 PM
-- Server version: 5.7.26-0ubuntu0.16.04.1
-- PHP Version: 7.1.28-1+ubuntu16.04.1+deb.sury.org+3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `notice-to-owner-clean`
--

--
-- Dumping data for table `notices`
--

INSERT INTO `notices` (`id`, `name`,`master_notice_id`, `type`, `status`, `created_at`, `updated_at`, `state_id`, `allow_cyo`, `allow_deadline_calculator`, `short_name`, `notice_sequence`, `is_claim_of_lien`, `owners_html_in_pdf`, `is_bond_of_claim`, `is_rescind`) VALUES
(1, 'Intent to Lien',1, 1, 1, NULL, '2018-10-22 19:37:22', 10, 1, 0, NULL, 0, 0, 1, 0, 1),
(2, 'Non Payment Notice',2, 1, 1, NULL, NULL, 10, 1, 0, NULL, 0, 0, 1, 0, 0),
(3, 'Claim of Lien',3, 1, 1, NULL, '2018-12-11 04:19:06', 10, 1, 1, NULL, 0, 1, 0, 0, 1),
(5, 'Satisfaction of Lien',4, 1, 1, NULL, '2018-12-27 01:20:21', 10, 1, 0, NULL, 0, 1, 0, 0, 1),
(6, 'Bond Claim of Lien',5,1, 1, NULL, '2018-12-11 00:47:01', 10, 0, 0, NULL, 0, 0, 0, 1, 0),
(7, 'Satisfaction Bond Claim',6, 1, 1, NULL, '2018-12-11 00:47:12', 10, 0, 0, NULL, 0, 0, 0, 0, 0),
(8, 'Partial Release of Lien',7, 3, 1, NULL, NULL, 10, 0, 0, NULL, 0, 0, 0, 0, 0),
(9, 'Notice to Owner/Preliminary Notice',8, 2, 1, NULL, '2018-09-10 18:53:51', 10, 1, 1, NULL, 0, 0, 1, 0, 1),
(10, 'Waiver & Release of Lien Upon Final Payment',9, 1, 1, '2018-08-27 13:00:00', '2018-12-27 06:19:58', 10, 1, 0, NULL, 0, 0, 0, 0, 0),
(13, 'Final Release of Lien',10, 3, 1, NULL, NULL, 10, 0, 0, NULL, 0, 0, 0, 0, 0),
(19, 'Colorado - Notice of Intent to Lien',1, 1, 1, '2018-12-03 13:40:37', '2018-12-04 11:26:05', 6, 0, 1, NULL, 1, 0, 0, 0, 0),
(20, 'Colorado - Preliminary Notice',8, 2, 1, '2018-12-03 14:00:33', '2018-12-03 14:00:44', 6, 0, 1, NULL, 3, 0, 0, 0, 0),
(21, 'Intent of Lien',1, 1, 1, '2018-12-04 06:19:47', '2018-12-04 06:19:59', 3, 0, 0, NULL, 2, 0, 0, 0, 0),
(22, 'Georgia - Preliminary Notice',8, 2, 1, '2018-12-04 11:31:00', '2018-12-04 11:31:31', 11, 0, 1, NULL, 4, 0, 0, 0, 0),
(23, 'Michigan - Preliminary Notice',8, 2, 1, '2018-12-04 11:34:45', '2018-12-04 11:34:56', 23, 0, 1, NULL, 5, 0, 0, 0, 0),
(24, 'Oregon - Preliminary Notice',8, 2, 1, '2018-12-04 11:43:20', '2018-12-04 11:43:36', 38, 0, 1, NULL, 6, 0, 0, 0, 0),
(25, 'Intent of Lien Calorado',1, 1, 1, '2018-12-05 04:22:09', '2018-12-05 04:22:36', 6, 0, 0, NULL, 3, 0, 0, 0, 0),
(27, 'None Payment Notice',2, 1, 0, '2018-12-18 09:14:27', '2018-12-18 09:14:27', 3, 0, 1, NULL, 5, 0, 0, 0, 0),
(28, 'Final Unconditional Waiver',11, 3, 1, '2019-01-10 18:30:00', NULL, 10, 0, 0, NULL, 0, 0, 0, 0, 0),
(29, 'Partial Unconditional Bond Waiver',12, 3, 1, '2019-01-10 18:30:00', NULL, 10, 0, 0, NULL, 0, 0, 0, 0, 0),
(32, 'CALIFORNIA PRELIMINARY 20-DAY NOTICE',8, 2, 1, '2019-03-14 10:19:55', '2019-03-14 10:20:46', 21, 1, 1, NULL, 9, 0, 0, 0, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
