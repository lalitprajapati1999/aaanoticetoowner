-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 04, 2019 at 04:03 PM
-- Server version: 5.7.26-0ubuntu0.16.04.1
-- PHP Version: 7.1.28-1+ubuntu16.04.1+deb.sury.org+3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `notice-to-owner`
--

--
-- Dumping data for table `pricings`
--

INSERT INTO `pricings` (`id`, `package_id`, `notice_id`, `state_id`, `lower_limit`, `upper_limit`, `charge`, `cancelation_charge`, `description`, `status`, `pricing_copied_id`, `created_at`, `updated_at`, `additional_address`) VALUES
(184, 1, 9, NULL, 1, 100, '150', '10', 'Notice to Owner/Preliminary Notice', 1, 0, '2019-08-07 21:54:23', '2019-08-09 06:37:32', NULL),
(185, 1, 9, NULL, 1, 10, '10', '0', 'ADDITIONAL ADDRESS CHARGES', 1, 0, '2019-08-07 21:54:23', '2019-08-09 06:37:32', 'additional_address'),
(186, 1, 1, NULL, NULL, NULL, '200', '10', 'Intent to Lien', 1, 0, '2019-08-07 21:54:23', '2019-08-09 06:37:32', NULL),
(187, 1, 2, NULL, NULL, NULL, '200', '10', 'Non Payment Notice', 1, 0, '2019-08-07 21:54:23', '2019-08-09 06:37:32', NULL),
(188, 1, 3, NULL, 1, 5000, '100', '10', 'Claim of Lien', 1, 0, '2019-08-07 21:54:23', '2019-08-09 06:37:32', NULL),
(189, 1, 6, NULL, 1, 5000, '100', '10', 'Bond Claim of Lien', 1, 0, '2019-08-07 21:54:23', '2019-08-09 06:37:32', NULL),
(190, 1, 5, NULL, NULL, NULL, '200', '10', '1', 1, 0, '2019-08-07 21:55:02', '2019-08-09 06:37:32', NULL),
(191, 1, 7, NULL, NULL, NULL, '200', '10', '1', 1, 0, '2019-08-07 21:55:02', '2019-08-09 06:37:32', NULL),
(192, 1, 10, NULL, NULL, NULL, '200', '10', '1', 1, 0, '2019-08-07 21:55:02', '2019-08-09 06:37:32', NULL),
(193, 1, 9, NULL, 101, 200, '160', '10', 'Notice to Owner/Preliminary Notice', 1, 0, '2019-08-07 21:57:00', '2019-08-09 06:37:32', NULL),
(195, 1, 3, NULL, 5001, 10000, '110', '10', 'Claim of Lien', 1, 0, '2019-08-07 21:57:01', '2019-08-09 06:37:32', NULL),
(196, 1, 6, NULL, 5001, 10000, '110', '10', 'Bond Claim of Lien', 1, 0, '2019-08-07 21:57:01', '2019-08-09 06:37:32', NULL),
(201, 1, 9, NULL, 11, 20, '8', '0', 'ADDITIONAL ADDRESS CHARGES', 1, 0, '2019-08-07 22:00:22', '2019-08-09 06:37:32', 'additional_address'),
(207, 2, 9, NULL, NULL, NULL, '200', '10', 'Notice to Owner/Preliminary Notice', 1, 0, '2019-08-07 22:21:25', '2019-08-09 06:27:18', NULL),
(208, 2, 9, NULL, 1, 100, '10', '0', 'ADDITIONAL ADDRESS CHARGES', 1, 0, '2019-08-07 22:21:25', '2019-08-09 06:27:18', 'additional_address'),
(209, 2, 1, NULL, NULL, NULL, '100', '10', 'Intent to Lien', 1, 0, '2019-08-07 22:21:25', '2019-08-09 06:27:19', NULL),
(210, 2, 2, NULL, NULL, NULL, '100', '10', 'Non Payment Notice', 1, 0, '2019-08-07 22:21:25', '2019-08-09 06:27:19', NULL),
(211, 2, 3, NULL, 1, 5000, '100', '10', 'Claim of Lien', 1, 0, '2019-08-07 22:21:25', '2019-08-09 06:27:19', NULL),
(212, 2, 5, NULL, NULL, NULL, '100', '10', 'Satisfaction of Lien', 1, 0, '2019-08-07 22:21:25', '2019-08-09 06:27:19', NULL),
(213, 2, 6, NULL, 1, 5000, '100', '10', 'Bond Claim of Lien', 1, 0, '2019-08-07 22:21:25', '2019-08-09 06:27:19', NULL),
(214, 2, 7, NULL, NULL, NULL, '100', '10', 'Satisfaction Bond Claim', 1, 0, '2019-08-07 22:21:25', '2019-08-09 06:27:19', NULL),
(215, 2, 10, NULL, NULL, NULL, '100', '10', 'Waiver & Release of Lien Upon Final Payment', 1, 0, '2019-08-07 22:21:25', '2019-08-09 06:27:19', NULL),
(216, 2, 9, NULL, 101, 99999999, '20', '0', 'ADDITIONAL ADDRESS CHARGES', 1, 0, '2019-08-07 22:24:15', '2019-08-09 06:27:18', 'additional_address'),
(217, 2, 3, NULL, 5001, 10000, '110', '10', 'Claim of Lien', 1, 0, '2019-08-07 22:24:15', '2019-08-09 06:27:19', NULL),
(218, 2, 6, NULL, 5001, 10000, '120', '10', 'Bond Claim of Lien', 1, 0, '2019-08-07 22:24:15', '2019-08-09 06:27:19', NULL),
(222, 3, 9, NULL, NULL, NULL, '100', '10', 'Notice to Owner/Preliminary Notice', 1, 0, '2019-08-07 22:28:22', '2019-08-09 06:32:39', NULL),
(223, 3, 9, NULL, 1, 100, '200', '0', 'ADDITIONAL ADDRESS CHARGES', 1, 0, '2019-08-07 22:28:22', '2019-08-09 06:32:39', 'additional_address'),
(224, 3, 1, NULL, NULL, NULL, '100', '10', 'Intent to Lien', 1, 0, '2019-08-07 22:28:22', '2019-08-09 06:32:39', NULL),
(225, 3, 2, NULL, NULL, NULL, '100', '10', 'Non Payment Notice', 1, 0, '2019-08-07 22:28:22', '2019-08-09 06:32:39', NULL),
(226, 3, 3, NULL, 1, 5000, '100', '10', 'Claim of Lien', 1, 0, '2019-08-07 22:28:22', '2019-08-09 06:32:39', NULL),
(227, 3, 5, NULL, NULL, NULL, '100', '10', 'Satisfaction of Lien', 1, 0, '2019-08-07 22:28:22', '2019-08-09 06:32:39', NULL),
(228, 3, 6, NULL, 1, 5000, '100', '10', 'Bond Claim of Lien', 1, 0, '2019-08-07 22:28:22', '2019-08-09 06:32:39', NULL),
(229, 3, 7, NULL, NULL, NULL, '100', '10', 'Satisfaction Bond Claim', 1, 0, '2019-08-07 22:28:22', '2019-08-09 06:32:40', NULL),
(230, 3, 10, NULL, NULL, NULL, '10', '10', 'Waiver & Release of Lien Upon Final Payment', 1, 0, '2019-08-07 22:28:22', '2019-08-09 06:32:40', NULL),
(231, 3, 9, NULL, 101, 99999999, '100', '0', 'ADDITIONAL ADDRESS CHARGES', 1, 0, '2019-08-07 22:29:20', '2019-08-09 06:32:39', 'additional_address'),
(232, 3, 3, NULL, 5001, 10000, '110', '10', 'Claim of Lien', 1, 0, '2019-08-07 22:29:20', '2019-08-09 06:32:39', NULL),
(233, 3, 6, NULL, 5001, 10000, '110', '10', 'Bond Claim of Lien', 1, 0, '2019-08-07 22:29:20', '2019-08-09 06:32:39', NULL),
(235, 1, 9, NULL, 200, 99999999, '200', '10', 'Notice to Owner/Preliminary Notice', 1, 0, '2019-08-07 23:02:20', '2019-08-09 06:37:32', NULL),
(236, 1, 9, NULL, 21, 99999999, '5', '0', 'ADDITIONAL ADDRESS CHARGES Above', 1, 0, '2019-08-07 23:02:20', '2019-08-09 06:37:32', 'additional_address'),
(237, 1, 6, NULL, 10001, 15000, '120', '10', 'Bond Claim of Lien', 1, 0, '2019-08-07 23:21:40', '2019-08-09 06:37:32', NULL),
(238, 1, 6, NULL, 15001, 20000, '130', '10', 'Bond Claim of Lien', 1, 0, '2019-08-07 23:21:40', '2019-08-09 06:37:32', NULL),
(239, 1, 6, NULL, 20001, 30000, '150', '10', 'Bond Claim of Lien', 1, 0, '2019-08-07 23:21:40', '2019-08-09 06:37:32', NULL),
(240, 1, 6, NULL, 30001, 40000, '160', '10', 'Bond Claim of Lien', 1, 0, '2019-08-07 23:21:40', '2019-08-09 06:37:32', NULL),
(241, 1, 6, NULL, 40001, 50000, '170', '10', 'Bond Claim of Lien', 1, 0, '2019-08-07 23:21:40', '2019-08-09 06:37:32', NULL),
(242, 1, 6, NULL, 50001, 99999999, '180', '15', 'Bond Claim of Lien', 1, 0, '2019-08-07 23:21:40', '2019-08-09 06:37:32', NULL),
(243, 2, 3, NULL, 10001, 15000, '120', '10', 'Claim of Lien', 1, 0, '2019-08-09 06:27:19', '2019-08-09 06:27:19', NULL),
(244, 2, 3, NULL, 15001, 20000, '130', '10', 'Claim of Lien', 1, 0, '2019-08-09 06:27:19', '2019-08-09 06:27:19', NULL),
(245, 2, 3, NULL, 20001, 30000, '140', '10', 'Claim of Lien', 1, 0, '2019-08-09 06:27:19', '2019-08-09 06:27:19', NULL),
(246, 2, 3, NULL, 30001, 40000, '150', '10', 'Claim of Lien', 1, 0, '2019-08-09 06:27:19', '2019-08-09 06:27:19', NULL),
(247, 2, 3, NULL, 40001, 50000, '160', '10', 'Claim of Lien', 1, 0, '2019-08-09 06:27:19', '2019-08-09 06:27:19', NULL),
(248, 2, 3, NULL, 50001, 99999999, '170', '10', 'Claim of Lien', 1, 0, '2019-08-09 06:27:19', '2019-08-09 06:27:19', NULL),
(249, 2, 6, NULL, 10001, 15000, '130', '10', 'Bond Claim of Lien', 1, 0, '2019-08-09 06:27:19', '2019-08-09 06:27:19', NULL),
(250, 2, 6, NULL, 15001, 20000, '140', '10', 'Bond Claim of Lien', 1, 0, '2019-08-09 06:27:19', '2019-08-09 06:27:19', NULL),
(251, 2, 6, NULL, 20001, 30000, '150', '10', 'Bond Claim of Lien', 1, 0, '2019-08-09 06:27:19', '2019-08-09 06:27:19', NULL),
(252, 2, 6, NULL, 30001, 40000, '160', '10', 'Bond Claim of Lien', 1, 0, '2019-08-09 06:27:19', '2019-08-09 06:27:19', NULL),
(253, 2, 6, NULL, 40001, 50000, '170', '10', 'Bond Claim of Lien', 1, 0, '2019-08-09 06:27:19', '2019-08-09 06:27:19', NULL),
(254, 2, 6, NULL, 50001, 99999999, '180', '10', 'Bond Claim of Lien', 1, 0, '2019-08-09 06:27:19', '2019-08-09 06:27:19', NULL),
(255, 3, 3, NULL, 10001, 15000, '120', '10', 'Claim of Lien', 1, 0, '2019-08-09 06:32:39', '2019-08-09 06:32:39', NULL),
(256, 3, 3, NULL, 15001, 20000, '130', '10', 'Claim of Lien', 1, 0, '2019-08-09 06:32:39', '2019-08-09 06:32:39', NULL),
(257, 3, 3, NULL, 20001, 30000, '140', '10', 'Claim of Lien', 1, 0, '2019-08-09 06:32:39', '2019-08-09 06:32:39', NULL),
(258, 3, 3, NULL, 30001, 40000, '150', '10', 'Claim of Lien', 1, 0, '2019-08-09 06:32:39', '2019-08-09 06:32:39', NULL),
(259, 3, 3, NULL, 40001, 50000, '160', '10', 'Claim of Lien', 1, 0, '2019-08-09 06:32:39', '2019-08-09 06:32:39', NULL),
(260, 3, 3, NULL, 50001, 99999999, '170', '10', 'Claim of Lien', 1, 0, '2019-08-09 06:32:39', '2019-08-09 06:32:39', NULL),
(261, 3, 6, NULL, 10001, 15000, '120', '10', 'Bond Claim of Lien', 1, 0, '2019-08-09 06:32:39', '2019-08-09 06:32:39', NULL),
(262, 3, 6, NULL, 15001, 20000, '130', '10', 'Bond Claim of Lien', 1, 0, '2019-08-09 06:32:40', '2019-08-09 06:32:40', NULL),
(263, 3, 6, NULL, 20001, 30000, '140', '10', 'Bond Claim of Lien', 1, 0, '2019-08-09 06:32:40', '2019-08-09 06:32:40', NULL),
(264, 3, 6, NULL, 30001, 40000, '150', '10', 'Bond Claim of Lien', 1, 0, '2019-08-09 06:32:40', '2019-08-09 06:32:40', NULL),
(265, 3, 6, NULL, 40001, 50000, '170', '10', 'Bond Claim of Lien', 1, 0, '2019-08-09 06:32:40', '2019-08-09 06:32:40', NULL),
(266, 3, 6, NULL, 50001, 99999999, '180', '10', 'Bond Claim of Lien', 1, 0, '2019-08-09 06:32:40', '2019-08-09 06:32:40', NULL),
(267, 1, 3, NULL, 10001, 15000, '120', '10', 'Claim of Lien', 1, 0, '2019-08-09 06:37:32', '2019-08-09 06:37:32', NULL),
(268, 1, 3, NULL, 15001, 20000, '130', '10', 'Claim of Lien', 1, 0, '2019-08-09 06:37:32', '2019-08-09 06:37:32', NULL),
(269, 1, 3, NULL, 20001, 30000, '140', '10', 'Claim of Lien', 1, 0, '2019-08-09 06:37:32', '2019-08-09 06:37:32', NULL),
(270, 1, 3, NULL, 30001, 40000, '150', '10', 'Claim of Lien', 1, 0, '2019-08-09 06:37:32', '2019-08-09 06:37:32', NULL),
(271, 1, 3, NULL, 40001, 50000, '160', '10', 'Claim of Lien', 1, 0, '2019-08-09 06:37:32', '2019-08-09 06:37:32', NULL),
(272, 1, 3, NULL, 50001, 99999999, '170', '10', 'Claim of Lien', 1, 0, '2019-08-09 06:37:32', '2019-08-09 06:37:32', NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
