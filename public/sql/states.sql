-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 03, 2019 at 09:32 AM
-- Server version: 5.7.26-0ubuntu0.16.04.1
-- PHP Version: 7.1.28-1+ubuntu16.04.1+deb.sury.org+3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `notice-to-owner-clean`
--

--
-- Dumping data for table `states`
--

INSERT INTO `states` (`id`, `name`, `abbr`, `country_id`, `status`) VALUES
(1, 'Washington', 'WA', 1, 0),
(2, 'Virginia', 'VA', 1, 0),
(3, 'Delaware', 'DE', 1, 0),
(4, 'District of Columbia', 'DC', 1, 0),
(5, 'Wisconsin', 'WI', 1, 0),
(6, 'West Virginia', 'WV', 1, 1),
(7, 'Hawaii', 'HI', 1, 0),
(8, 'New Hampshire', 'NH', 1, 0),
(9, 'Wyoming', 'WY', 1, 0),
(10, 'Florida', 'FL', 1, 1),
(11, 'New Jersey', 'NJ', 1, 0),
(12, 'New Mexico', 'NM', 1, 0),
(13, 'Texas', 'TX', 1, 1),
(14, 'Louisiana', 'LA', 1, 0),
(15, 'North Carolina', 'NC', 1, 1),
(16, 'North Dakota', 'ND', 1, 0),
(17, 'Nebraska', 'NE', 1, 0),
(18, 'Tennessee', 'TN', 1, 0),
(19, 'New York', 'NY', 1, 0),
(20, 'Pennsylvania', 'PA', 1, 0),
(21, 'California', 'CA', 1, 1),
(22, 'Nevada', 'NV', 1, 1),
(23, 'Puerto Rico', 'PR', 1, 0),
(24, 'Colorado', 'CO', 1, 1),
(25, 'Virgin Islands', 'VI', 1, 0),
(26, 'Alaska', 'AK', 1, 0),
(27, 'Alabama', 'AL', 1, 1),
(28, 'Arkansas', 'AR', 1, 0),
(29, 'Vermont', 'VT', 1, 0),
(30, 'Illinois', 'IL', 1, 0),
(31, 'Georgia', 'GA', 1, 1),
(32, 'Indiana', 'IN', 1, 0),
(33, 'Iowa', 'IA', 1, 0),
(34, 'Oklahoma', 'OK', 1, 0),
(35, 'Arizona', 'AZ', 1, 1),
(36, 'Idaho', 'ID', 1, 0),
(37, 'Connecticut', 'CT', 1, 0),
(38, 'Maine', 'ME', 1, 0),
(39, 'Maryland', 'MD', 1, 0),
(40, 'Massachusetts', 'MA', 1, 0),
(41, 'Ohio', 'OH', 1, 0),
(42, 'Utah', 'UT', 1, 1),
(43, 'Missouri', 'MO', 1, 0),
(44, 'Minnesota', 'MN', 1, 0),
(45, 'Michigan', 'MI', 1, 1),
(46, 'Rhode Island', 'RI', 1, 0),
(47, 'Kansas', 'KS', 1, 0),
(48, 'Montana', 'MT', 1, 0),
(49, 'Mississippi', 'MS', 1, 0),
(50, 'South Carolina', 'SC', 1, 0),
(51, 'Kentucky', 'KY', 1, 0),
(52, 'Oregon', 'OR', 1, 1),
(53, 'South Dakota', 'SD', 1, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
