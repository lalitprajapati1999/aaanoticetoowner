-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 02, 2019 at 05:04 PM
-- Server version: 5.7.26-0ubuntu0.16.04.1
-- PHP Version: 7.1.28-1+ubuntu16.04.1+deb.sury.org+3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `notice-to-owner-clean`
--

--
-- Dumping data for table `notices`
--

INSERT INTO `master_notices` (`id`,`flag`,`display_name`) VALUES
(1,	'ITL', 'Intent to Lien'),
(2,	'NPN', 'Non Payment Notice'),
(3, 'COL', 'Claim of Lien'),
(4, 'SOL', 'Satisfaction of Lien'),
(5, 'BCOL', 'Bond Claim of Lien'),
(6, 'SBC', 'Satisfaction Bond Claim'),
(7, 'PROL',  'Partial Release of Lien'),
(8, 'NTO', 'Notice to Owner/Preliminary Notice'),
(9, 'WRL', 'Waiver & Release of Lien Upon Final Payment'),
(10, 'FROL', 'Final Release of Lien'),
(11, 'FUW', 'Final Unconditional Waiver'),
(12, 'PUBW','Partial Unconditional Bond Waiver');


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
