-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 03, 2019 at 09:49 AM
-- Server version: 5.7.26-0ubuntu0.16.04.1
-- PHP Version: 7.1.28-1+ubuntu16.04.1+deb.sury.org+3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `notice-to-owner-clean`
--

--
-- Dumping data for table `stamps_add_ons`
--

INSERT INTO `stamps_add_ons` (`id`, `abbr`, `description`, `status`, `required_value`) VALUES
(1, 'SC-A-HP', 'Hidden Postage', '0', NULL),
(2, 'SC-A-INS', 'Insurance', '0', NULL),
(3, 'SC-A-INSRM', 'Insurance for Registered Mail', '0', NULL),
(4, 'SC-A-POU', 'Pay On Use', '0', NULL),
(5, 'US-A-CM', 'Certified Mail', '1', NULL),
(6, 'US-A-COD', 'Collect On Delivery', '0', NULL),
(7, 'US-A-DC', 'USPS Delivery Confirmation', '0', NULL),
(8, 'US-A-ESH', 'USPS Express - Sunday/Holiday Guaranteed', '0', NULL),
(9, 'US-A-INS', 'USPS Insurance', '0', NULL),
(10, 'US-A-NDW', 'USPS Express No Delivery on Saturdays', '0', NULL),
(11, 'US-A-RD', 'Restricted Delivery', '0', NULL),
(12, 'US-A-REG', 'Registered Mail', '0', NULL),
(13, 'US-A-RR', 'Return Receipt Requested', '1', NULL),
(14, 'US-A-RRM', 'Return Receipt for Merchandise', '0', NULL),
(15, 'US-A-SC', 'USPS Signature Confirmation', '0', NULL),
(16, 'US-A-SH', 'Fragile', '0', NULL),
(17, 'US-A-WDS', 'USPS Express Waive Delivery Signature', '0', NULL),
(18, 'US-A-SR', 'USPS Express Signature Required', '0', NULL),
(19, 'US-A-ESH', 'Sunday/Holiday Delivery Guaranteed', '0', NULL),
(20, 'US-A-NND', 'Notice of Non-Delivery', '0', NULL),
(21, 'US-A-RRE', 'Electronic Return Receipt', '1', NULL),
(22, 'US-A-LANS', 'Live Animal No Surcharge', '0', NULL),
(23, 'US-A-LAWS', 'Live Animal With Surcharge', '0', NULL),
(24, 'US-A-HM', 'Hazardous Materials', '0', NULL),
(25, 'US-A-CR', 'Cremated Remains', '0', NULL),
(26, 'US-A-1030', 'Delivery Priority Mail Express by 10\\:30 am', '1', NULL),
(27, 'US-A-ASR', 'Adult Signature Required', '0', NULL),
(28, 'US-A-ASRD', 'Adult Signature Restricted Delivery', '0', NULL),
(29, 'US-A-PR', 'Perishable', '0', NULL),
(30, 'US-A-HFPU', 'Hold for Pick Up', '1', NULL),
(31, 'CAR-A-SAT', '', '0', NULL),
(32, 'CAR-A-RES', '', '0', NULL),
(33, 'CAR-A-NSP', '', '0', NULL),
(34, 'CAR-A-ISR', '', '0', NULL),
(35, 'CAR-A-DSR', '', '0', NULL),
(36, 'CAR-A-ASR', '', '0', NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
