-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 03, 2019 at 09:50 AM
-- Server version: 5.7.26-0ubuntu0.16.04.1
-- PHP Version: 7.1.28-1+ubuntu16.04.1+deb.sury.org+3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `notice-to-owner-clean`
--

--
-- Dumping data for table `stamps_service_type`
--

INSERT INTO `stamps_service_type` (`id`, `abbr`, `description`, `status`) VALUES
(1, 'US-FC', 'USPS First Class Mail', '1'),
(2, 'US-MM', 'USPS Media Mail', '0'),
(3, 'US-PP', 'USPS Parcel Post', '0'),
(4, 'US-PM', 'USPS Priority Mail', '1'),
(5, 'US-XM', 'USPS Priority Mail Express', '0'),
(6, 'US-EMI', 'USPS Priority Mail Express International', '0'),
(7, 'US-PMI', 'USPS Priority Mail International', '0'),
(8, 'US-FCI', 'USPS First Class Mail International', '0'),
(9, 'US-PS', 'USPS Parcel Select Ground', '0'),
(10, 'US-LM', 'USPS Library Mail', '0'),
(11, 'DHL-PE', '', '0'),
(12, 'DHL-PG', '', '0'),
(13, 'DHL-PPE', '', '0'),
(14, 'DHL-PPG', '', '0'),
(15, 'DHL-BPME', '', '0'),
(16, 'DHL-BPMG', '', '0'),
(17, 'DHL-MPE', '', '0'),
(18, 'DHL-MPG', '', '0'),
(19, 'AS-IPA', '', '0'),
(20, 'AS-ISAL', '', '0'),
(21, 'AS-EPKT', '', '0'),
(22, 'DHL-PIPA', '', '0'),
(23, 'DHL-PISAL', '', '0'),
(24, 'GG-IPA', '', '0'),
(25, 'GG-ISAL', '', '0'),
(26, 'GG-EPKT', '', '0'),
(27, 'IBC-IPA', '', '0'),
(28, 'IBC-ISAL', '', '0'),
(29, 'IBC-EPKT', '', '0'),
(30, 'RRD-IPA', '', '0'),
(31, 'RRD-ISAL', '', '0'),
(32, 'RRD-EPKT', '', '0'),
(33, 'AS-GNRC', '', '0'),
(34, 'GG-GNRC', '', '0'),
(35, 'RRD-GNRC', '', '0'),
(36, 'SC-GPE', '', '0'),
(37, 'SC-GPP', '', '0'),
(38, 'SC-GPESS', '', '0'),
(39, 'SC-GPPSS', '', '0'),
(40, 'DHL-EWW', '', '0'),
(41, 'FX-GD', '', '0'),
(42, 'FX-HD', '', '0'),
(43, 'FX-2D', '', '0'),
(44, 'FX-ES', '', '0'),
(45, 'FX-SO', '', '0'),
(46, 'FX-PO', '', '0'),
(47, 'FX-GDI', '', '0'),
(48, 'FX-EI', '', '0'),
(49, 'FX-PI', '', '0');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
