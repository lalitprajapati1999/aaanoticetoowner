-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 03, 2019 at 09:49 AM
-- Server version: 5.7.26-0ubuntu0.16.04.1
-- PHP Version: 7.1.28-1+ubuntu16.04.1+deb.sury.org+3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `notice-to-owner-clean`
--

--
-- Dumping data for table `stamps_add_ons_prohibited`
--

INSERT INTO `stamps_add_ons_prohibited` (`id`, `add_on`, `prohibited`) VALUES
(1, 9, 2),
(2, 9, 5),
(3, 9, 6),
(4, 9, 12),
(5, 6, 1),
(6, 6, 5),
(7, 6, 9),
(8, 6, 14),
(9, 6, 30),
(10, 7, 5),
(11, 7, 15),
(12, 7, 27),
(13, 7, 28),
(14, 15, 5),
(15, 15, 7),
(16, 15, 14),
(17, 15, 27),
(18, 15, 28),
(19, 5, 1),
(20, 5, 6),
(21, 5, 7),
(22, 5, 9),
(23, 5, 12),
(24, 5, 14),
(25, 5, 15),
(26, 5, 16),
(27, 13, 1),
(28, 13, 14),
(29, 12, 1),
(30, 12, 2),
(31, 12, 5),
(32, 12, 9),
(33, 12, 14),
(34, 12, 16),
(35, 11, 1),
(36, 11, 14),
(37, 11, 16),
(38, 2, 9),
(39, 2, 12),
(40, 1, 5),
(41, 1, 6),
(42, 1, 9),
(43, 1, 11),
(44, 1, 12),
(45, 1, 13),
(46, 1, 14),
(47, 1, 23),
(48, 14, 1),
(49, 14, 5),
(50, 14, 6),
(51, 14, 11),
(52, 14, 12),
(53, 14, 13),
(54, 14, 15),
(55, 14, 15),
(56, 21, 1),
(57, 21, 3),
(58, 21, 7),
(59, 21, 14),
(60, 21, 15),
(61, 21, 16),
(62, 21, 20),
(63, 21, 6),
(64, 27, 7),
(65, 27, 15),
(66, 27, 28),
(67, 28, 7),
(68, 28, 15),
(69, 28, 27),
(70, 24, 22),
(71, 24, 23),
(72, 24, 25),
(73, 22, 23),
(74, 22, 24),
(75, 22, 25),
(76, 23, 1),
(77, 23, 22),
(78, 23, 24),
(79, 23, 25),
(80, 16, 5),
(81, 16, 11),
(82, 16, 12),
(83, 16, 21),
(84, 30, 6);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
