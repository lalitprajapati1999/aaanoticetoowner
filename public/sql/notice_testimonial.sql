-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 02, 2019 at 05:08 PM
-- Server version: 5.7.26-0ubuntu0.16.04.1
-- PHP Version: 7.1.28-1+ubuntu16.04.1+deb.sury.org+3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `notice-to-owner-clean`
--

--
-- Dumping data for table `notice_testimonial`
--

INSERT INTO `notice_testimonial` (`id`, `content`, `client_name`, `image`, `status`, `created_at`, `updated_at`) VALUES
(2, '<p class="western">Trusting AAA Notice to Owner to send our Notices was the best decision ever made.I really do enjoy the unlimited database and dashboard has everything in one place.</p>\r\n\r\n<p class="western">Create your own, it&rsquo;s easy and convenient for us to use. It has&nbsp;saved us $$$ ! Their Support team has not let us down, their team has guided us through every step and are always available to answer our questions.</p>\r\n\r\n<p class="western">&nbsp;</p>', 'AC Concrete Corp.- Danny', '', 1, '2018-09-11 18:28:31', '2018-12-12 03:54:42'),
(3, '<p class="western" style="line-height: 100%; margin-bottom: 0cm;">AAA Notice to Owner is unbelievably helpful to us when it comes to protecting our liens rights. They are involved in our projects deadline, making sure that we don&rsquo;t bypass our due dates. Any concern or questions we have, we get an answered promptly. I am a loyal and satisfied customer of AAA Notice to Owner for great services that we have received. They have always catered to our needs.</p>', 'Top Mix - Carlos', '1544509283.png', 1, '2018-09-11 18:42:40', '2018-12-11 00:51:23'),
(8, '<p>Great way to manage all your notices &nbsp;in one place and customer service&nbsp;is great!&nbsp;</p>', 'All Construction Services - Tony Sanchez', '1544509237.png', 1, '2018-11-01 11:14:35', '2018-12-11 00:50:37'),
(10, '<p>AAA Notice to owner became a asset for our company. Going into the sofware, I was able to get all the information needed pertaining to my projects. The customer support its always avaible with any questions we have. If you are looking for an all in one Lien protection, customer service,answer to your questions and getting paid on time, this is the one, they are there for you!&nbsp;</p>', 'Quente Interiors Inc- Daniel', '1544162755.png', 1, '2018-11-01 11:28:37', '2019-01-23 06:37:41');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
