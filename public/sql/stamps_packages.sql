-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 03, 2019 at 09:58 AM
-- Server version: 5.7.26-0ubuntu0.16.04.1
-- PHP Version: 7.1.28-1+ubuntu16.04.1+deb.sury.org+3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `notice-to-owner-clean`
--

--
-- Dumping data for table `stamps_packages`
--

INSERT INTO `stamps_packages` (`id`, `name`, `status`) VALUES
(1, 'Postcard', '0'),
(2, 'Letter', '1'),
(3, 'Large Envelope or Flat', '1'),
(4, 'Thick Envelope', '1'),
(5, 'Package', '0'),
(6, 'Small Flat Rate Box', '0'),
(7, 'Flat Rate Box', '0'),
(8, 'Large Flat Rate Box', '0'),
(9, 'Flat Rate Envelope', '1'),
(10, 'Flat Rate Padded Envelope', '0'),
(11, 'Large Package', '0'),
(12, 'Oversized Package', '0'),
(13, 'Regional Rate Box A', '0'),
(14, 'Regional Rate Box B', '0'),
(15, 'Regional Rate Box C', '0'),
(16, 'Legal Flat Rate Envelope', '0'),
(17, 'Express Envelope', '1'),
(18, 'Documents', '1'),
(19, 'Envelope', '1'),
(20, 'Pak', '0');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
