<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CyoDocPaymentInvoiceMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->mailData = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //return $this->view('view.name');
        return $this->subject('Invoice')->view('send_mail_invoice',$this->mailData)
                    ->attach(public_path('pdf/invoice-pdf/'.$this->mailData['file']), [
                         'as' => $this->mailData['file'],
                         'mime' => 'application/pdf',
                    ]);
    }
}
