<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmployeeMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->mailData = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data['name'] = $this->mailData['name'];
         $data['contentBody'] = '';
        $data['contentHead']  = 'Your account has been successfully registered.';
        $data['contentBody']  .= '<br>Your login details are:';
        $data['contentBody'] .= '<br>Email:'.$this->mailData['email'];
        $data['contentBody'] .= '<br>Password:'.$this->mailData['password'];
        $data['contentBody'] .= '<br>Mailing Access Pin:'.$this->mailData['pin'];
        return $this->subject('Registered Successfully')->view('mail_template',$data);
    }
}
