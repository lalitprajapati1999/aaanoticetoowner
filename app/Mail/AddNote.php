<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AddNote extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->note = 'shamal';
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {  // dd('hi');
        //$data['note']=$this->note;
        //return $this->view('users.resetpassword',$data);
        return $this->view('customer.work_order_histry.add_note');
    }
}
