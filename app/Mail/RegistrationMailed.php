<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegistrationMailed extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($customer)
    {
        $this->mailData = $customer;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data['name'] = $this->mailData->user->name;
        $data['contentHead']  = 'Your account has been successfully registered.';
        $data['contentBody']  = 'Welcome to your lien protection services!<br />Your account has been successfully registered, is pending administrative approval.<br /> Thank you for your business. ';
        return $this->subject('Registered Successfully')->view('mail_template',$data);
    }
}
