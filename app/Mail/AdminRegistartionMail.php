<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdminRegistartionMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($customer,$admin)
    {
        $this->mailData = $customer;
        $this->admin    = $admin;
        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data['name'] = $this->admin[0]['name'];
       
        $data['contentHead']  = $this->mailData->company_name.' has been successfully registered.';
        $data['contentBody']  = $this->mailData->company_name.' has been successfully registered, but pending for an admin approval. Please approve this account.';//. $this->admin['thank_msg'];

      /*  $data['contentBody']  = $this->mailData->company_name.' has been successfully registered, but pending for an admin approval. Please approve this account.
                         ';*/
        return $this->subject('Customer Registered')->view('mail_template',$data);
    }
}
