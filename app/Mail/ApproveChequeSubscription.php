<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ApproveChequeSubscription extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($customer,$cheque)
    {
        $this->mailData = $customer;
        $this->cheque    = $cheque;    
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //$data['name'] = $this->admin;
       
        $data['contentHead']  = $this->mailData['contactperson'].' Successfully subscribed.';
        $data['contentBody']  = $this->mailData['contactperson'].' successfully approved your check. Thank you for your business.';
        return $this->subject('Check Approved')->view('mail_template',$data);
    }
}
