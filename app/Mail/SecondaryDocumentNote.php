<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SecondaryDocumentNote extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($note)
    {
        $this->mailData = $note;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data['contentHead']  = "Secondary Document Id:".$this->mailData->secondary_document_id;
        $data['contentBody']  = "Note: ".$this->mailData->note;
        return $this->subject('Secondary Document Note')->view('mail_template',$data);
                    
    }
}
