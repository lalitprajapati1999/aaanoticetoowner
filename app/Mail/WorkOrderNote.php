<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Auth;
class WorkOrderNote extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($note)
    {
        $this->mailData = $note;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
       // $data['contentHead']  = "Work Order Id:".$this->mailData->work_order_id;
       // $data['contentBody']  = "Note: ".$this->mailData->note;
        $data['work_order_id'] = $this->mailData->work_order_id;
        $data['note'] = $this->mailData->note;
        $data['email'] = $this->mailData->email;
        $data['name'] = Auth::user()->name;
        return $this->subject('Work order note')->view('note_mail_tamplate',$data);
                    
    }
}
