<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class LastDateNotificationMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($work_order)
    {
        $this->mailData = $work_order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data['name'] = $this->mailData['company_name'];
        $data['contentHead']  = 'Notification Reminder';
        $data['work_order_id'] = $this->mailData['workorder_id'];
        $data['project_address'] = $this->mailData['project_address'];
        $data['contracted_by'] = $this->mailData['contracted_by'];
        $data['stopnotificationurl'] = $this->mailData['stopnotificationurl'];
        return $this->subject('Notification Reminder ')->view('last_date_notification_mail_template',$data);
    }
}
