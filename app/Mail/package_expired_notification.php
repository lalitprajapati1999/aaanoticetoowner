<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class package_expired_notification extends Mailable
{
    use Queueable, SerializesModels;

    /**
    
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($customer)
    {
        $this->mailData = $customer;
       // $this->admin    = $admin; 
        //dd($this->mailData);   
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data['name'] = $this->mailData['company_name'];
        $data['contentHead']  = 'Subscription package expired notification.';
        $data['adminContentBody']  = 'Package subscription get expired of your customer name '.$this->mailData['name'].'. Please contact and request for renew subscription.
         Thank you for associating with AAA notice to owner.';
        return $this->subject('Package Expired Notification')->view('package_expired_email_template',$data);
    }
}
