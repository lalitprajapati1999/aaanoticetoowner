<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class packageExpiredNotification extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->mailData = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data['contentBody'] = '';
        $data['name'] = $this->mailData['company_name'];
        $data['contentHead']  = 'Subscription package notification.';
        $data['contentBody']  .= 'Your subscription has expired. You can renew, Create Your Own Package through the Pricing link on website <a href="' . env('APP_URL') . '">www.aaanoticetoowner.com</a>.';
        $data['contentBody'] .= '<br>Thank you for your business.';
        return $this->subject('Package Expired Notification')->view('package_expired_email_template',$data);
    }
}