<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class PayoutChequeNotificationMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
    
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($customer)
    {
        $this->mailData = $customer;
       // $this->admin    = $admin; 
        //dd($this->mailData);   
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        //$data['contentHead']  = 'Customer Subscription';
        $data['name'] = $this->mailData->company_name;
        $data['email'] = $this->mailData->email;
        $data['package_name'] = $this->mailData->package_name;
        $data['stopnotificationurl'] = url('admin/cheque-subscriptions');
        return $this->subject('Customer Paid')->view('check_payment_approval',$data);    
    }
}
