<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactusDeadline extends Mailable
{
    use Queueable, SerializesModels;
        public $contactus;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($contactusdata)
    {
        $this->mailData = $contactusdata;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
       // return $this->view('contact_us_mail_template');
//                     ->with([
//                         
//                        'mailData' =>"Name:".$this->mailData->name.
//                                    "Email:".$this->mailData->email.
//                                    "Query:".$this->mailData->query
//                    ]);
//         $data['name'] = $this->mailData['name'];
        $data['contentBody'] = '';
//        $data['contentHead']  = 'Your account has been successfylly registered.';
        $data['contentBody']  .= '<br>Name:'.$this->mailData['name'];
         $data['contentBody']  .= '<br>Company Name:'.$this->mailData['company_name'];
        $data['contentBody'] .= '<br>Email:'.$this->mailData['email'];
        $data['contentBody'] .= '<br>Phone:'.$this->mailData['phone'];
        return $this->subject('Deadline Contact us')->view('contact_us_mail_template',$data);
    }
}
