<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CustomerActivationMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($customer)
    {
        $this->mailData = $customer;
        //dd($this->mailData);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data['contentBody'] ="";
        $data['name'] = $this->mailData->name;
        $data['contentHead']  = 'Welcome!';
        $data['contentBody']  .= 'Thanks for choosing AAA Notice to Owner, we’re happy you’re are here.';  
        $data['contentBody'] .= '<br/>Protecting your lien right will give you a better return. ';
        return $this->subject('Notice to Owner Account Approved')->view('mail_template',$data);
    }
}
