<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Contactus extends Mailable
{
    use Queueable, SerializesModels;
        public $contactus;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($contactusdata)
    {
        $this->mailData = $contactusdata;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data['contentBody'] = '';
        
        $data['contentBody']  .= '<br>Name:'.$this->mailData['name'];
        $data['contentBody'] .= '<br>Email:'.$this->mailData['email'];
        $data['contentBody'] .= '<br>Telephone:'.$this->mailData['phone'];
        $data['contentBody'] .= '<br>Query:'.$this->mailData['query'];
        
        return $this->subject('Contact us')->view('contact_us_mail_template',$data);
    }
}
