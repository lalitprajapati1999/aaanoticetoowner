<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CustomerApprovalMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($customer)
    {
        $this->mailData = $customer;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data['name'] = $this->mailData->user->name;
        $data['contentHead']  = 'Your account has been approved by admin.';
        $data['contentBody']  = ' Welcome to your Lien protection services!  We appreciate your buisiness. ';
        return $this->subject('Notice to Owner Account Approved')->view('mail_template',$data);
    }
}
