<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class payoutchequenotificationmail.php extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($customer,$admin)
    {
        $this->mailData = $customer;
        $this->admin    = $admin;    
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data['name'] = $this->admin[0]['name'];
        $data['contentHead']  = $this->mailData->user->name.' .';
        $data['contentBody']  = $this->mailData->user->name.' check has been  successfully paid, but pending an admin approval for check. Please approve this check.
                        Thank you for associating with AAA notice to owner. ';
        return $this->subject('Customer Registered')->view('mail_template',$data);    
    }
}
