<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Auth;
class WorkOrderCorrection extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($note)
    {
        $this->mailData = $note;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
       /* $data['contentHead']  = "Work Order Id:".$this->mailData->work_order_id;
        $data['contentBody']  = "Correction:".$this->mailData->correction;*/
        $data['work_order_id'] = $this->mailData->work_order_id;
        $data['correction'] = $this->mailData->correction;
        $data['email'] = $this->mailData->email;
        $data['name'] = Auth::user()->name;
        return $this->subject('Work order correction')->view('note_mail_tamplate',$data);
                    
    }
}
