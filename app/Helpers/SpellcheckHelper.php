<?php

namespace App\Helpers;


use GlSpellChecker\GlSpellChecker;
use Symfony\Component\Finder\Finder;

class SpellcheckHelper {

    public static function check() {
//language to check, define languagetool directory, and languagetool port used
//        $spellchecker = new GlSpellChecker("fr", "fr_FR", base_path().'/vendor/glicer/', 'localhost', 8000);
         $spellchecker = new GlSpellChecker("fr","fr_FR",null,'localhost',8010);
// or with docker $spellchecker = new GlSpellChecker("fr","fr_FR",null,'localhost',8010);
//construct list of local html files to check spell
       
        $finder = new Finder();
        
        $files = $finder->files()->in(base_path().'/resources/views/auth/')->name("*.blade.php");
        dd($files);
//launch html checking
        $filereport = $spellchecker->checkHtmlFiles(
                $files, function (SplFileInfo $file, $nbrsentences) {
            // called at beginning - $nbr sentences to check
        }, function ($sentence) {
            // called each sentence to check
        }, function () {
            // called at the end
        }
        );


//$filereport contain fullpath to html file report
        print_r($filereport);
    }

}
