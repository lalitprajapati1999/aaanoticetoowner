<?php

namespace App\Helpers;

use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
use QuickBooksOnline\API\Facades\Customer;
use QuickBooksOnline\API\Facades\Invoice;
use Illuminate\Http\Request;
use App\Models\QuickbooksToken;
use App\Models\Customer as Customer1;

class QuickBooksHelper {

    public function refreshToken($data) {
        $tokenEndPointUrl = config('constants.QBO.TOK_ENDPOINT_URL');
        $client_id = config('constants.QBO.CONSUMER_KEY');
        $client_secret = config('constants.QBO.CONSUMER_SECRET');

        $grant_type = 'refresh_token';
        $base_url = url('/');
       // dd($base_url,strpos($base_url,'http://127.0.0.1'),strpos($base_url,'localhost'));
       /* if(strpos($base_url,'localhost') || strpos($base_url,'http://127.0.0.1')==0)
        {
            $certFilePath = './Certificate/cacert.pem';
        }
        else
        {
            $certFilePath = $_SERVER['DOCUMENT_ROOT'];
            $certFilePath .= "/public/Certificate/cacert.pem";
        }*/

        $certFilePath = public_path()."/Certificate/cacert.pem";
        $client = new Client($client_id, $client_secret, $certFilePath);

        $result = $client->refreshAccessToken($tokenEndPointUrl, $grant_type, $data['refresh_token']);

        if (isset($result['error']) && $result['error'] != '') {
            \Log::error($result['error']);
            \Session::flash('error', $result['error']);
        } else {
            $now = date('Y-m-d H:i:s');
            $token_expiry = date('Y-m-d H:i:s', strtotime('+ ' . $result['expires_in'] . ' seconds'));
            $refresh_expiry = date('Y-m-d H:i:s', strtotime('+ ' . $result['x_refresh_token_expires_in'] . ' seconds'));

            $token_data = [
                'access_token' => $result['access_token'],
                'refresh_token' => $result['refresh_token'],
                'token_expires_at' => $token_expiry,
                'refresh_token_expires_at' => $refresh_expiry,
                'updated_at' => $now,
                'company_id' => $data['company_id'],
                'customer_id' => 1,
                'scope' => $data['scope']
            ];
            QuickbooksToken::where(['scope' => $data['scope'], 'company_id' => $data['company_id'], 'customer_id' => 1])->update($token_data);
        }
        return;
    }

    public function createCustomer($customer_id, $access_token, $refresh_token) {
        $customer_details = Customer1::where(['user_id' => $customer_id])->first();
        $city = \App\Models\City::where('id', $customer_details->physical_city_id)->pluck('name')->first();
        $state = \App\Models\State::where('id', $customer_details->physical_state_id)->pluck('name')->first();

        //Register this customer on Quickbooks
        $qbo_customer = [
            "BillAddr" => [
                "Line1" => $customer_details->mailing_address,
                "City" => $customer_details->mailingCity->name,
                //"Country" => "USA",
                "CountrySubDivisionCode" => $customer_details->mailingState->name,
                "PostalCode" => $customer_details->mailing_zip
            ],
            "ShipAddr" => [
                "Line1" => $customer_details->physical_address,
                "City" => $customer_details->physicalCity->name,
               // "Country" => "USA",
                "CountrySubDivisionCode" => $customer_details->physicalState->name,
                "PostalCode" => $customer_details->physical_zip
            ],
           
            "FullyQualifiedName" => $customer_details->company_name,
            "CompanyName" => $customer_details->company_name,
            "DisplayName" => $customer_details->company_name . '_' . time(),
            "PrimaryPhone" => [
                "FreeFormNumber" => $customer_details->office_number
            ],
            "PrimaryEmailAddr" => [
                "Address" => $customer_details->company_email
            ]
        ];
        //dd($qbo_customer);
        // Prep Data Services
        $dataService = DataService::Configure(array(
                    'auth_mode' => 'oauth2',
                    'ClientID' => config('constants.QBO.CONSUMER_KEY'),
                    'ClientSecret' => config('constants.QBO.CONSUMER_SECRET'),
                    'accessTokenKey' => $access_token,
                    'refreshTokenKey' => $refresh_token,
                    'QBORealmID' => config('constants.QBO.COMPANY_ID'),
                    'baseUrl' => config('constants.QBO.ENVIRONMENT')
        ));
        $theResourceObj = Customer::create($qbo_customer);
        $resultingObj = $dataService->Add($theResourceObj);

        $error = $dataService->getLastError();  
        
        if ($error) {
            \Log::error($error->getResponseBody());
            
            try
            {
                $refresh_data = ['refresh_token' => $refresh_token, 'company_id' => config('constants.QBO.COMPANY_ID'), 'customer_id' => 1, 'scope' => config('constants.QBO.SCOPE')];
                $this->refreshToken($refresh_data);
                //Get newly generated tokens and call createCustomer again
                $new_tokens = QuickbooksToken::where(['scope' => config('constants.QBO.SCOPE'), 'company_id' =>  config('constants.QBO.COMPANY_ID'), 'customer_id' => 1])->first();

                $this->createCustomer($customer_id,$new_tokens->access_token,$new_tokens->refresh_token);
            }
            catch(\Exception $e)
            {
                return ['response' => 'error', 'message' => 'Quickbooks API call failed. '. $e->getMessage().' at line '.$e->getLine(). ' on file '.$e->getFile()];
            }            
        } else {
            $qbo_customer_id = $resultingObj->Id;
            // session('qbo_customer_id',$qbo_customer_id);
            session(['qbo_customer_id' => $qbo_customer_id]);
            \App\Models\User::where(['id' => $customer_id])->update(['qbo_customer_id' => $qbo_customer_id]);
            return ['response' => 'success', 'message' => 'Your account has been created successfully, but pending for admin approval.'];
        }
    }

    public function addCard($qbo_customer_id, $card_details, $access_token) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => config('constants.QBO.APP_URL2')."/quickbooks/v4/customers/" . $qbo_customer_id . "/cards",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($card_details),
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer " . $access_token,
                "Cache-Control: no-cache",
                "Content-Type: application/json",
                "Request-Id:add_card_" . time()
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            return ['response' => 'error', 'message' => $err];
        } else {
            $response = json_decode($response, true);
            if (isset($response['errors']) && !empty($response['errors'])) {
                \DB::table('quickbooks_temp_data')->insert(['quickbooks_data' => json_encode($response), 'quickbooks_task' => 'addCard', 'scope' => 'payment', 'customer_id' => $qbo_customer_id]);
                return ['response' => 'error', 'message' => $response['errors'][0]['message']];
            } else {
                $now = date('Y-m-d H:i:s');
                try {
                    \DB::table('user_card')->insert(['user_id' => $qbo_customer_id, 'card_id' => $response['id'], 'created_at' => $now, 'updated_at' => $now]);
                    return ['response' => 'success', 'message' => 'Card details saved successfully', 'card_id' => $response['id']];
                } catch (\Exception $e) {
                    return ['response' => 'error', 'message' => 'This card has already been saved for this user'];
                }
            }
            return $response;
        }
    }

    public function getSavedCards($data) {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => config('constants.QBO.APP_URL2').'quickbooks/v4/customers/' . $data["customer_id"] . '/cards',
             // CURLOPT_URL => config('constants.QBO.APP_URL2').'quickbooks/v4/customers/87/cards',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            // CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer " . $data['access_token'],
                "Cache-Control: no-cache",
                "Content-Type: application/json",
                "Request-Id:get_card_" . time()
            ),
        ));

        $response = curl_exec($curl);
        $response = json_decode($response, true);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            return ['response' => 'error', 'message' => $err];
        } else {
            if (empty($response)) {
                return ['response' => 'error', 'message' => 'Error while fetching saved cards.'];
            }
            if (isset($response['errors']) && !empty($response['errors'])) {
                \DB::table('quickbooks_temp_data')->insert(['quickbooks_data' => json_encode($response), 'quickbooks_task' => 'getSavedCards', 'scope' => $data['scope'], 'customer_id' => $data['customer_id']]);
                return ['response' => 'error', 'message' => $response['errors'][0]['message']];
            } else {
                return ['response' => 'success', 'message' => 'Cards found', 'cards' => $response];
            }
            return $response;
        }
    }

    public function getProductDetails($data) {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => config('constants.QBO.APP_URL2').'/v3/company/' . config('constants.QBO.COMPANY_ID') . '/item' . '/' . $data['product_id'],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            // CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer " . $data['access_token'],
                "Cache-Control: no-cache",
                "Content-Type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            return ['response' => 'error', 'message' => $err];
        } else {
            $xml = simplexml_load_string($response, "SimpleXMLElement", LIBXML_NOCDATA);
            $json = json_encode($xml);
            $response = json_decode($json, TRUE);

            if (isset($response['Fault']) && !empty($response['Fault'])) {
                \DB::table('quickbooks_temp_data')->insert(['quickbooks_data' => json_encode($response), 'quickbooks_task' => 'getSavedCards', 'scope' => $data['scope'], 'customer_id' => $data['customer_id']]);
                return ['response' => 'error', 'message' => $response['Fault']['Error']['Message']];
            } else {
                return ['response' => 'success', 'message' => 'Item found', 'item_details' => $response];
            }
            return $response;
        }
    }

    public function createCharge($data, $access_token) {

        try{
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => config('constants.QBO.APP_URL2')."/quickbooks/v4/payments/charges",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($data['card_details']),
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer " . $access_token,
                "Cache-Control: no-cache",
                "Content-Type: application/json",
                "Request-Id:add_charge_" . time()
            ),
        ));

        $response = curl_exec($curl);
        $response = json_decode($response, true);
        $err = curl_error($curl);
        curl_close($curl);
        }catch (\Exception $e) {
            \Log::error($e->getTraceAsString());
            throw new \Exception($e->getMessage(), 1);
        }
        if ($err) {
            return ['response' => 'error', 'message' => $err['errors'][0]['message'], 'package_id' => $data['package_id']];
        } else {
            if (empty($response)) {
                // return ['response'=>'error','message'=>'Issue with token','package_id'=>$data['package_id']];
                return ['response' => 'error', 'message' => 'Error while completing payment for subscription.', 'package_id' => $data['package_id']];
            }
            if (isset($response['errors']) && !empty($response['errors'])) {
                \DB::table('quickbooks_temp_data')->insert(['quickbooks_data' => json_encode($response), 'quickbooks_task' => 'createCharge', 'scope' => $data['scope'], 'customer_id' => $data['customer_id']]);
                return ['response' => 'error', 'message' => $response['errors'][0]['message'], 'package_id' => $data['package_id']];
            } else {
                try {
                    $created_date = date('Y-m-d H:i:s');
                    //Save transaction details
                    $transaction = ['transaction_id' => $response['id'], 'amount' => $response['amount'], 'auth_code' => $response['authCode'], 'transaction_status' => $response['status'], 'user_id' => $data['user_id'], 'card_id' => $data['card_id'], 'created_at' => $created_date, 'updated_at' => $created_date];
                    \App\Models\Transaction::create($transaction);
                    // save customer subscription details
                    return ['response' => 'success', 'message' => 'Charge successful', 'package_id' => $data['package_id']];
                } catch (\Exception $e) {
                    return ['response' => 'error', 'message' => 'Error while completing payment for subscription', 'package_id' => $data['package_id']];
                }
            }
        }
    }

    public function createInvoice($data) {
        // Prep Data Services
        $dataService = DataService::Configure(array(
                    'auth_mode' => 'oauth2',
                    'ClientID' => config('constants.QBO.CONSUMER_KEY'),
                    'ClientSecret' => config('constants.QBO.CONSUMER_SECRET'),
                    'accessTokenKey' => $data['access_token'],
                    'refreshTokenKey' => $data['refresh_token'],
                    'QBORealmID' => config('constants.QBO.COMPANY_ID'),
                    'baseUrl' => config('constants.QBO.ENVIRONMENT')
        ));

        $theResourceObj = Invoice::create($data['invoice_details']['invoice_details']
        );

        $resultingObj = $dataService->Add($theResourceObj);


        $error = $dataService->getLastError();

        if ($error) {
            $xml = simplexml_load_string($error->getResponseBody(), "SimpleXMLElement", LIBXML_NOCDATA);
            $json = json_encode($xml);
            $array = json_decode($json, TRUE);
            return ['response' => 'error', 'message' => $array['Fault']['Error']['Detail']];
        } else {
            return ['response' => 'success', 'message' => 'Quickbooks invoice generated'];
        }
    }

    public function deleteCard($qbo_customer_id, $card_id, $access_token,$customer_id) {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            
            CURLOPT_URL => config('constants.QBO.APP_URL2')."/quickbooks/v4/customers/" . $customer_id . "/cards/".$card_id,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "DELETE",
            //CURLOPT_POSTFIELDS => json_encode($card_id),
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer " . $access_token,
                "Cache-Control: no-cache",
                "Content-Type: application/json",
                "Request-Id:delete_card_" . time()
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            return ['response' => 'error', 'message' => $err];
        } else {
            $response = json_decode($response, true);
            if (isset($response['errors']) && !empty($response['errors'])) {
               

                \DB::table('quickbooks_temp_data')->insert(['quickbooks_data' => json_encode($response), 'quickbooks_task' => 'deleteCard', 'scope' => 'payment', 'customer_id' => $qbo_customer_id]);
                return ['response' => 'error', 'message' => $response['errors'][0]['message']];
            } else {
                $now = date('Y-m-d H:i:s');
                try {
                     $user = \DB::table('user_card')->where('card_id','=',$card_id);    
                        $user->delete();
               
                    return ['response' => 'success', 'message' => 'Card details deleted successfully', 'card_id' => $response['id']];
                } catch (\Exception $e) {
                    return ['response' => 'error', 'message' => 'This card has already been deleted for this user'];
                }
            }
            return $response;
        }
    }

    public function queryItem($data) {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => config('constants.QBO.APP_URL2').'/v3/company/' . config('constants.QBO.COMPANY_ID') . '/query?query='.$data['query'].'&minorversion=59',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            // CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer " . $data['access_token'],
                "Cache-Control: no-cache",
                "Content-Type: application/json"
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if ($err) {
            return ['response' => 'error', 'message' => $err];
        } else {
            $xml = simplexml_load_string($response, "SimpleXMLElement", LIBXML_NOCDATA);
            $json = json_encode($xml);
            $response = json_decode($json, TRUE);
            if (isset($response['Fault']) && !empty($response['Fault'])) {
                return ['response' => 'error', 'message' => $response['Fault']['Error']['Message']];
            } else {
                return ['response' => 'success', 'message' => 'Item found', 'item_details' => $response];
            }
            return $response;
        }
    }

    public function addDocCard($qbo_customer_id, $data, $access_token) {
        $curl = curl_init();

        try{
            $curl = curl_init();       
            $headers = array(
                "Authorization: Bearer " . $access_token,
                    "Cache-Control: no-cache",
                    "Content-Type: application/json",
                    "request-id:add_charge_" . time()
            );   

            $curl = curl_init('https://api.intuit.com/quickbooks/v4/customers/'. $qbo_customer_id .'/cards');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
            $response = curl_exec($curl);
            //var_dump(($response));
            $response = json_decode($response, true);
            $err = curl_error($curl);
            curl_close($curl);
        }catch (\Exception $e) {
            dd('eror',$e->getMessage());
            \Log::error($e->getTraceAsString());
            throw new \Exception($e->getMessage(), 1);
        }        
        if ($err) {
            return ['response' => 'error', 'message' => $err];
        } else {            
            $response = json_decode($response, true);            
            if (isset($response['errors']) && !empty($response['errors'])) {
                \DB::table('quickbooks_temp_data')->insert(['quickbooks_data' => json_encode($response), 'quickbooks_task' => 'addCard', 'scope' => 'payment', 'customer_id' => $qbo_customer_id]);
                return ['response' => 'error', 'message' => $response['errors'][0]['message']];
            } else {
                $now = date('Y-m-d H:i:s');
                try {
                    \DB::table('user_card')->insert(['user_id' => $qbo_customer_id, 'card_id' => $response['id'], 'created_at' => $now, 'updated_at' => $now]);
                    return ['response' => 'success', 'message' => 'Card details saved successfully', 'card_id' => $response['id']];
                } catch (\Exception $e) {
                    return ['response' => 'error', 'message' => 'This card has already been saved for this user'];
                }
            }
            return $response;
        }
    }

    public function getDocSavedCards($data) {
       
        try{
            $curl = curl_init();       
            $headers = array(
                "Authorization: Bearer " . $data['access_token'],
                    "Cache-Control: no-cache",
                    "Content-Type: application/json",
                    "request-id:add_charge_" . time()
            );

            $curl = curl_init('https://api.intuit.com/quickbooks/v4/customers/'. $data["customer_id"] .'/cards');
            curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);                     
            $response = curl_exec($curl);
            //var_dump(($response));
            $response = json_decode($response, true);
            $err = curl_error($curl);
            curl_close($curl);
        }catch (\Exception $e) {
            dd('eror',$e->getMessage());
            \Log::error($e->getTraceAsString());
            throw new \Exception($e->getMessage(), 1);
        }

        if ($err) {
            return ['response' => 'error', 'message' => $err];
        } else {
            if (empty($response)) {
                return ['response' => 'error', 'message' => 'Error while fetching saved cards.'];
            }
            if (isset($response['errors']) && !empty($response['errors'])) {
                \DB::table('quickbooks_temp_data')->insert(['quickbooks_data' => json_encode($response), 'quickbooks_task' => 'getSavedCards', 'scope' => $data['scope'], 'customer_id' => $data['customer_id']]);
                return ['response' => 'error', 'message' => $response['errors'][0]['message']];
            } else {
                return ['response' => 'success', 'message' => 'Cards found', 'cards' => $response];
            }
            return $response;
        }
    }


    
    public function createDocPayment($data, $access_token) {              
        try{
            $curl = curl_init();       
            $headers = array(
                "Authorization: Bearer " . $access_token,
                    "Cache-Control: no-cache",
                    "Content-Type: application/json",
                    "request-id:add_charge_" . time()
            );

            $curl = curl_init('https://api.intuit.com/quickbooks/v4/payments/charges');
          
            curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data['card_details']));
            $response = curl_exec($curl); 
           // dd($response);
            $response = json_decode($response, true);            
            $err = curl_error($curl);
            //dd($response,$err);
            curl_close($curl);
        }catch (\Exception $e) {
           // dd('eror',$e->getMessage());
            \Log::error($e->getTraceAsString());
            throw new \Exception($e->getMessage(), 1);
        }        
        if ($err) {
            return ['response' => 'error', 'message' => $err['errors'][0]['message'], 'package_id' => $data['package_id']];
        } else {
            if (empty($response)) {
                // return ['response'=>'error','message'=>'Issue with token','package_id'=>$data['package_id']];
                return ['response' => 'error', 'message' => 'Error while completing payment for document.', 'package_id' => $data['package_id']];
            }
            if (isset($response['errors']) && !empty($response['errors'])) {
                \DB::table('quickbooks_temp_data')->insert(['quickbooks_data' => json_encode($response), 'quickbooks_task' => 'createCharge', 'scope' => $data['scope'], 'customer_id' => $data['customer_id']]);
                return ['response' => 'error', 'message' => $response['errors'][0]['message'], 'package_id' => $data['package_id']];
            } else {
                try {
                    $created_date = date('Y-m-d H:i:s');
                    //Save transaction details
                    $transaction = ['transaction_id' => $response['id'], 'amount' => $response['amount'], 'auth_code' => $response['authCode'], 'transaction_status' => $response['status'], 'user_id' => $data['user_id'], 'card_id' => $data['card_id'], 'created_at' => $created_date, 'updated_at' => $created_date];
                    \App\Models\Transaction::create($transaction);
                    // save customer subscription details
                    return ['response' => 'success', 'message' => 'Charge successful', 'package_id' => $data['package_id']];
                } catch (\Exception $e) {
                    if(isset($response['status'])&&$response['status']=='DECLINED'){
                        return ['response' => 'error', 'message' => 'Your card may have been declined', 'package_id' => $data['package_id']];
                    }
                    return ['response' => 'error', 'message' => 'Error while completing payment for document', 'package_id' => $data['package_id']];
                }
            }
        }
    }


}
