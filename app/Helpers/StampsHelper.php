<?php
namespace App\Helpers;

error_reporting(E_ALL);
ini_set('display_errors',1);

class StampsHelper
{
    private $authenticator;
    private $client;
    private $account;

    private $ServiceType = array(
        "US-FC"     =>  "USPS First-Class Mail",
        "US-MM"     =>  "USPS Media Mail",
        "US-PP"     =>  "USPS Parcel Post",
        "US-PM"     =>  "USPS Priority Mail",
        "US-XM"     =>  "USPS Priority Mail Express",
        "US-EMI"    =>  "USPS Priority Mail Express International",
        "US-PMI"    =>  "USPS Priority Mail International",
        "US-FCI"    =>  "USPS First Class Mail International",
        "US-CM"     =>  "USPS Critical Mail",
        "US-PS"     =>  "USPS Parcel Select",
        "US-LM"     =>  "USPS Library Mail"
    );

    public function __construct()
    {
        $wsdl           = config('constants.STAMPS.WSDL');
        $integrationID  = config('constants.STAMPS.INTEGRATION_ID');
        $username       = config('constants.STAMPS.USERNAME');
        $password       = config('constants.STAMPS.PASSWORD');
        
        $this->client = new \SoapClient($wsdl);

        $authData = [
            "Credentials"   => [
                "IntegrationID"     => $integrationID,
                "Username"          => $username,
                "Password"          => $password
            ]
        ];

        $this->makeCall('AuthenticateUser', $authData);
        $this->account = $this->makeCall('GetAccountInfo', ["Authenticator" => $this->authenticator]);
    }

    private function makeCall($method, $data) {
        try{
            $result = $this->client->$method($data);
            $this->authenticator = $result->Authenticator;
            return ['status'=>'success','result'=>$result];
        }
        catch(\Exception $e)
        {
            return ['status'=>'error','message'=>$e->getMessage()];
        }
    }

    public function PurchasePostage()
    {
        $data = [
            "Authenticator" => $this->authenticator,
            'PurchaseAmount'=>'100',
            'ControlTotal'=>'805'
        ];

        $result = $this->makeCall('PurchasePostage', $data);
        
        return $result;
    }

    public function GetPurchaseStatus($TransactionID)
    {
        $data = [
            "Authenticator" => $this->authenticator,
            'TransactionID'=>$TransactionID
        ];

        $result = $this->makeCall('GetPurchaseStatus', $data);
        
        return $result;
    }

    public function CleanseAddress($address)
    {
        if (isset($address['Company']) && !empty($address['Company'])) {
            $address['Company'] = str_limit($address['Company'], 50, '');
        }

        $data = [ 
            'Authenticator'=>$this->authenticator,
            'Address' => $address,
            'FromZIPCode'=> env('FROM_ZIPCODE')
        ];

        $result = $this->makeCall('CleanseAddress', $data);
        
        return $result;
    }

    public function CancelIndicium()
    {
        $data = [ 
            'Authenticator'=>$this->authenticator,
            'StampsTxID'=>'b1bbdc82-4a79-407b-b588-0fbe48865842'
        ];

        $result = $this->makeCall('CancelIndicium', $data);
        
        return $result;
    }

    public function GetRates($rates1,$add_ons = [])
    {       
        $new_add_ons = [];
        $cnt = 0;
        $cnt1 = 0;
        $add_ons = [];
        $add_ons = isset($rates1['AddOns'])?$rates1['AddOns']:[];
        unset($rates1['AddOns']);

        $data = [
            "Authenticator" => $this->authenticator,
            "Rate"      => $rates1  
        ];
        
        $rates = $this->makeCall('getRates', $data);
        if($rates['status'] == 'success')
        {
            $rates = $rates['result']->Rates->Rate;  
            $rates = json_decode(json_encode($rates),true);
            
            
            if(isset($rates['AddOns']) && !empty($add_ons))
            {
                foreach($rates['AddOns']['AddOnV10'] as $add_on)
                {
                    if(in_array($add_on['AddOnType'],$add_ons))
                    {                  
                        $new_add_ons[$cnt1++]['AddOnType'] = $add_on['AddOnType'];
                    }
                    $cnt++;           
                }
                unset($rates['AddOns']);
                $rates['AddOns'] = $new_add_ons;
            }
            else
            {
                unset($rates['AddOns']);
            }
            
            
            return ['status'=>'success','result'=>$rates];
        }
        else
        {
            return ['status'=>'error','message'=>$rates];
        }
    }

    // public function CreateIndicium($FromZIPCode, $ToZIPCode = null, $ToCountry = null, $WeightLb, $Length, $Width, $Height, $PackageType, $ShipDate, $InsuredValue, $ToState = null)
    public function CreateIndicium($rates,$to_addr=NULL,$from_addr)
    {
        // $ServiceType = array_search($rates['ServiceType'],$this->ServiceType);
        // $rates['ServiceType'] = $ServiceType;
        
        $data = [
            "Authenticator" => $this->authenticator,
            'IntegratorTxID' => $this->generateRandomString(),
            // 'Rate'=>[
            //     'FromZIPCode'=>'90405',
            //     'ToZIPCode'=>'90066',
            //     'Amount'=>10,
            //     'ServiceType'=>'US-PM',
            //     'DeliverDays'=>'1-1',
            //     'WeightLb'=>'12',
            //     'PackageType'=>'Package',
            //     'ShipDate'=>'2018-09-02',
            //     'InsuredValue'=>'100.00'
            // ],
            'Rate' => $rates,
            // 'From'=>[
            //     "FirstName" =>'Sender',
            //     "LastName" =>'Test',
            //     "PhoneNumber" => '123456789',
            //     'FullName'=>'Geoff Anton',
            //     'Address1'=>'3420 Ocean Park Bl',
            //     'Address2'=>'Ste 1000',
            //     'City'=>'Santa Monica',
            //     'State'=>'CA',                
            //     'ZIPCode'=>'90405'
            //     ],
           'From' => $from_addr,
            // 'To'=>[
            //     'FullName'=>'Norman J. McClain',
            //     'Address1'=>'12959 CORAL TREE PL',
            //     'City'=>'LOS ANGELES',
            //     'State'=>'CA',
            //     'ZIPCode'=>'90066',
            //     'ZIPCodeAddOn'=>'7020',
            //     'DPB'=>'59',
            //     'CheckDigit'=>'6'
            //     ]
           'To' =>$to_addr
        ];

        
        $result = $this->makeCall('CreateIndicium', $data);


        return $result;
    }

    

    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}