<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Master_notices extends Model
{
    protected $fillable = ['flag','display_name', 'qbo_product_id'];
}
