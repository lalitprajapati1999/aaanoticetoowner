<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers\Client;
use Illuminate\Support\Facades\Mail;
use App\Mail\packageExpiredNotification;
use App\Mail\package_expired_notification;
use DB;

class CheckCustomerSubscription extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'CheckCustomerSubscription:changedSubscriptionStatus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'To get new access token using refresh token';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try{
            $users  = \App\Models\Customer_subscription::select('users.email','users.name','customers.company_name')->leftJoin('customers','customers.id','customer_subscriptions.customer_id')->where('end_date', '=', date('Y-m-d',strtotime("-1 days")))->leftJoin('users','users.id','customers.user_id')->get()->toArray();
            $status =  \App\Models\Customer_subscription::where('end_date', '=', date('Y-m-d',strtotime("-1 days")))->update(['status'=>0]);
            if($status){
                foreach ($users as $key => $value) {
                    Mail::to($value['email'])->send(new packageExpiredNotification($value));
                    Mail::to(config('constants.TRANSACTION_EMAILS.CONTACT_US_EMAIL'))->send(new package_expired_notification($value));
                    # code...
                }
                $this->info('Customer subscription status chnaged successfully.');
            }else{
               $this->info('Problem in change customer subscription status.'); 
            }
        }catch (\Exception $e) {
            $this->error($e);
            \Log::error($e->getTraceAsString());
            throw new \Exception($e->getMessage(), 1);
        }
      
        

    }
}