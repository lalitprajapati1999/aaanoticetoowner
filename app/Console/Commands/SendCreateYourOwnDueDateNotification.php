<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers\Client;
use App\Models\QuickbooksToken;
use DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\DuedateNotificationMail;
use App\Mail\DuedateClaimLienNotificationMail;
class SendCreateYourOwnDueDateNotification extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'SendCreateYourOwnDueDateNotification:sendCreateYourOwnWorkOrderNotification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'To send the create your own work order due date notification to customer';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $WorkOrderFields = \App\Models\Cyo_WorkOrder_fields::select('cyo__work_orders.parent_id', 'cyo__work_orders.created_at', 'cyo__work_orders.user_id', 'cyo__work_orders.customer_id', 'cyo__work_orders.notice_id as notice_id', 'usr.name as account_manager_name', 'cyo__work_orders.status', 'cyo__work_orders.user_id', 'cyo__work_orders.account_manager_id', 'cyo__work_order_fields.workorder_id', 'cyo__work_order_fields.notice_id', DB::raw("(GROUP_CONCAT(notice_fields.name SEPARATOR '||')) as `field_names`"), DB::raw("(GROUP_CONCAT(value SEPARATOR '||')) as `field_values`"), 'users.email', 'cyo__work_orders.discontinue_reminder', 'users.name','customers.company_name')
                        ->rightjoin('notice_fields', 'notice_fields.id', '=', 'cyo__work_order_fields.notice_field_id')
                        ->join('cyo__work_orders', 'cyo__work_orders.id', '=', 'cyo__work_order_fields.workorder_id')
                        ->leftjoin('users', 'users.id', '=', 'cyo__work_orders.user_id')
                        ->leftjoin('customers', 'customers.id', '=', 'cyo__work_orders.customer_id')
                        ->leftjoin('users as usr', 'usr.id', '=', 'cyo__work_orders.account_manager_id')
                        ->groupBy('cyo__work_order_fields.workorder_id', 'cyo__work_order_fields.notice_id')
                        ->get()->toArray();


       
        $result = [];
        if (isset($WorkOrderFields) && !empty($WorkOrderFields)) {
            foreach ($WorkOrderFields as $fields_data) {
                $field_names = explode('||', $fields_data['field_names']);
                $field_values = explode('||', $fields_data['field_values']);
                $field_names_values = (count($field_names)==count($field_values))?array_combine($field_names, $field_values):[];
                $field_names_values['default'] = '';
                $result[] = array_merge($fields_data, $field_names_values);
            }
        }else{
            $this->info('No records found.');
        }

        $count = 0;
        if (isset($result) && !empty($result)) {
            foreach ($result AS $k => $each_val) {

                $notice_type = \App\Models\Notice::find($each_val['notice_id']);

                if ($notice_type->type == 2) {

                    if (isset($each_val['job_start_date']) && $each_val['job_start_date'] != '') {


                        $todays_date = date('m/d/Y');

                        $days65 = 65 . 'days';
                        $days75 = 75 . 'days';
                        $days80 = 80 . 'days';
                        $days60 = 60 . 'days';

                        $days65_date = date('m/d/Y', strtotime('+ ' . $days65, strtotime($each_val['job_start_date'])));
                        $days75_date = date('m/d/Y', strtotime('+ ' . $days75, strtotime($each_val['job_start_date'])));
                        $days80_date = date('m/d/Y', strtotime('+ ' . $days80, strtotime($each_val['job_start_date'])));
                        $days60_date = date('m/d/Y', strtotime('+ ' . $days60, strtotime($each_val['job_start_date'])));
                        $work_order_id = $each_val['workorder_id'];
                        $each_val['stopnotificationurl'] ="";

                        if (!$each_val['discontinue_reminder']) {


                            if (isset($each_val['last_date_on_the_job']) && $each_val['last_date_on_the_job'] != '') {
                                //if last job entered

                                if ($todays_date == $days65_date || $todays_date == $days75_date) {
                                  $each_val['stopnotificationurl'] = url("create_your_own/dueDate/discontinueReminder/$work_order_id");
                                    if (isset($each_val['email']) && $each_val['email'] != '') {
                                        Mail::to($each_val['email'])->send(new DuedateNotificationMail($each_val));
                                        $this->info('Notification send successfully.');
                                         $count++;
                                    } else {
                                        $this->error('Customer email is invalid.');
                                    }
                                }else if ($todays_date == $days80_date) {
                                  $each_val['stopnotificationurl'] = "";
                                    if (isset($each_val['email']) && $each_val['email'] != '') {
                                        Mail::to($each_val['email'])->send(new DuedateNotificationMail($each_val));
                                        $this->info('Notification send successfully.');
                                         $count++;
                                    } else {
                                        $this->error('Customer email is invalid.');
                                    }
                                }
                            } else {
                                //if last job is not enetered

                                $format_todays_date = date('Y-m-d', strtotime($todays_date));
                                $date1 = date_create($format_todays_date);

                                $format_60days_date = date('Y-m-d', strtotime($days60_date));
                                $date2 = date_create($format_60days_date);

                                //Calculate date diff between todays and 60 days form job start date
                                $diff = date_diff($date1, $date2);
                                $diff_days = $diff->format("%a");

                                if ($todays_date == $days60_date) {
                                    if (isset($each_val['email']) && $each_val['email'] != '') {
                                        Mail::to($each_val['email'])->send(new DuedateNotificationMail($each_val));
                                        $this->info('Notification send successfully.');
                                         $count++;
                                    } else {
                                        $this->error('Customer email is invalid.');
                                    }
                                } else {
                                    if ($todays_date > $days60_date && $diff_days % 30 == 0) {
                                        if (isset($each_val['email']) && $each_val['email'] != '') {
                                            Mail::to($each_val['email'])->send(new DuedateNotificationMail($each_val));
                                            $this->info('Notification send successfully.');
                                             $count++;
                                        } else {
                                            $this->error('Customer email is invalid.');
                                        }
                                    }
                                }
                            }
                        }
                    }
                }else{
                   if (isset($each_val['clerk_of_court_recorded_date']) && $each_val['clerk_of_court_recorded_date'] != '') {
                     
                        $todays_date = date('m/d/Y');
                        $work_order_id = $each_val['workorder_id'];
                        $each_val['stopnotificationurl']= "";
                        $tenmonth = date('m/d/Y', strtotime("+10 months", strtotime($each_val['clerk_of_court_recorded_date'])));
                        $elevenmonth =  date('m/d/Y', strtotime("+11 months", strtotime($each_val['clerk_of_court_recorded_date'])));
                         
                        if (!$each_val['discontinue_reminder']) {
                            if ($todays_date == $tenmonth) {
                                  $each_val['stopnotificationurl']= url("dueDate/discontinueReminder/$work_order_id");
                                  if(isset($each_val['email']) && $each_val['email'] !=''){
                                   
                                      
                                   Mail::to($each_val['email'])->send(new DuedateClaimLienNotificationMail($each_val));
                                   $this->info('Notification send successfully.');
                                   $count ++;
                                    }else{
                                          $this->error('Customer email is invalid.');
                                    }
                                }else if ($todays_date == $elevenmonth) {
                                  $each_val['stopnotificationurl']= "";
                                    if(isset($each_val['email']) && $each_val['email'] !=''){
                                   
                                      
                                  Mail::to($each_val['email'])->send(new DuedateClaimLienNotificationMail($each_val));
                                   $this->info('Notification send successfully.');
                                   $count ++;
                                    }else{
                                          $this->error('Customer email is invalid.');
                                    }
                                }
                        }
                       
                   }
                }
            }
        }
         if($count == 0){
            $this->info('No records found');
        }
    }

}
