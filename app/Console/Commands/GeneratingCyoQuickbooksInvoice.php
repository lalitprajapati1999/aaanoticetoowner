<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class GeneratingCyoQuickbooksInvoice extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'GeneratingCyoQuickbooksInvoice:generatingCyoInvoice';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'To generate monthly invoice for own work order(Cyo)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        try {
            $this->info('Generating invoices for workorders');
            $generated_labels = [];
            $notice = [];
            $notice_fields = [];
            $labels = [];
            $quickbooks_labels = [];
            $first_date_of_month = date('Y-m-01');
            $last_date_of_month = date('Y-m-t');

            /* get workorder status completed records */

            $all_work_order = \App\Models\Cyo_WorkOrders::join('notices', 'cyo__work_orders.notice_id', '=', 'notices.id')
            ->join('master_notices', 'master_notices.id', '=', 'notices.master_notice_id')
            ->whereIn('cyo__work_orders.status', ['6', '7'])
            ->where(['cyo__work_orders.invoice_generated' => 'No'])
            ->where(['cyo__work_orders.is_billable' => 1])
            ->orderBy('id', 'desc')
            ->get(['rush_hour_amount','rush_hour_charges','cyo__work_orders.id', 'cyo__work_orders.status as work_order_status', 'user_id', 'type as notice_type', 'notice_id','master_notice_id', 'master_notices.qbo_product_id'])
            ->toArray();
          
            if (!empty($all_work_order)) {
                $this->info('Completed work order records found');
                foreach ($all_work_order as $work_order) {
                    $additional_addr_charges = 0;
                    $basic_address_charges = 0;
                    
                    $work_order_id = $work_order['id'];
                    $qboProductId = $work_order['qbo_product_id'] ?? config('constants.QBO.NoticeBaseAmount');
                    $user = \App\Models\User::find($work_order['user_id']);
                    //Check if the user is registered in quickbooks, if no then we cannot generate invoice for that user
                    if ($user->qbo_customer_id != '') {
                        $this->info('QBO customer id found for user with id ' . $work_order['user_id']);
                        $notice = \App\Models\Notice::join('cyo__work_orders', 'cyo__work_orders.notice_id', '=', 'notices.id')
                                ->join('notice_fields', 'notice_fields.notice_id', '=', 'notices.id')
                                ->where(['cyo__work_orders.id' => $work_order_id])
                                ->get([\DB::raw('notice_fields.id as notice_field_id'), \DB::raw('cyo__work_orders.id as workorder_id'), \DB::raw('notice_fields.name as notice_field_name'), \DB::raw('notices.name as notice_name'), \DB::raw('notices.id as notice_id1'),\DB::raw('cyo__work_orders.order_no as order_no'),\DB::raw('notices.master_notice_id as master_notice_id')])
                                ->toArray();

                        $recipients = \App\Models\Cyo_WorkOrder_Recipient::join('cyo__work_orders', 'cyo__work_orders.id', '=', 'cyo__work_order__recipients.work_order_id')
                                ->where(['cyo__work_orders.id' => $work_order_id])
                                ->pluck('cyo__work_order__recipients.id');

                        /* get customer Work order count for => same month , same notice id  */
                        if ($work_order['work_order_status'] == "6") {
                            /* check date with completed_at */
                            $workorder_count = \App\Models\Cyo_WorkOrders::where('notice_id', $work_order['notice_id'])
                                            ->where('user_id', $work_order['user_id'])
                                            ->where('status', $work_order['work_order_status'])
                                            ->where('cyo__work_orders.completed_at', '>=', $first_date_of_month)
                                            ->where('cyo__work_orders.completed_at', '<=', $last_date_of_month)->count();
                        } else {
                            /* check date with cancelled at */
                            $workorder_count = \App\Models\Cyo_WorkOrders::where('notice_id', $work_order['notice_id'])
                                            ->where('user_id', $work_order['user_id'])
                                            ->where('status', $work_order['work_order_status'])
                                            ->where('cyo__work_orders.cancelled_at', '>=', $first_date_of_month)
                                            ->where('cyo__work_orders.cancelled_at', '<=', $last_date_of_month)->count();
                        }

                        /* Check customer package subscription for CYO-monthly ,yearly     */
                        $customer_subscription = \App\Models\Customer_subscription::where(['customer_id' => $user->customer->id, 'status' => 1])
                                ->orderBy('id', 'DESC')
                                ->first();

                        /* Customer package Pricing charges */
                      /*  $customer_package = \App\Models\Pricing::where(['package_id' => $customer_subscription['package_id'], 'notice_id' => $work_order['notice_id']])
                                ->first();*/
                    foreach ($notice as $key => $val) {
                        if($val['master_notice_id'] == config('constants.MASTER_NOTICE')['COL']['ID'] || $val['master_notice_id'] == config('constants.MASTER_NOTICE')['BCOL']['ID'])
                        {
                            $field_value = \App\Models\Cyo_WorkOrder_fields::where(['notice_field_id' => $val['notice_field_id'], 'workorder_id' => $val['workorder_id']])->pluck('value')->toArray();
                            $notice_fields[$val['notice_field_name']] = (isset($field_value[0])) ? $field_value[0] : '';
                            $unpaid_amount = '00';
                            if(isset($notice_fields['unpaid_amount'])) {
                                    $unpaid_amount = $notice_fields['unpaid_amount'];
                            }
                            $notice_id_info = \App\Models\Pricing::join('notices','notices.id','pricings.notice_id')->where('pricings.lower_limit', '<=', $unpaid_amount)
                                    ->where('pricings.upper_limit', '>=',$unpaid_amount)->where('notices.master_notice_id', $work_order['master_notice_id'])->where('package_id',$customer_subscription['package_id'])->select('pricings.notice_id as customer_notice_id')->first();

                            $customer_package_notice_id = $work_order['notice_id'];
                            if (!empty($notice_id_info)){
                                $customer_package_notice_id = $notice_id_info['customer_notice_id'];
                            }
                            $customer_package = \App\Models\Customer_package::where(['customer_id' => $user->customer->id, 'notice_id' => $customer_package_notice_id])->where('package_id',$customer_subscription['package_id'])
                                        ->where('lower_limit', '<=', $unpaid_amount)
                                        ->where('upper_limit', '>=', $unpaid_amount)
                                        ->first();
                             /* if customer dont have package take charges from pricing */
                                if (empty($customer_package)) {
                                    $customer_package = \App\Models\Pricing::where('notice_id',$customer_package_notice_id)->where('package_id',$customer_subscription['package_id'])
                                            ->where('lower_limit', '<=', $unpaid_amount)
                                            ->where('upper_limit', '>=', $unpaid_amount)
                                            ->first();
                                }
                             /* check workorder status for charges => Cancelled or completed */
                                if ($work_order['work_order_status'] == "6") {
                                    $charges = $customer_package['charge'];
                                } else {
                                    $charges = $customer_package['cancelation_charge'];
                                } 

                                $docPayment = \App\Models\Cyo_Doc_Payment_History::where(['work_order_id' => $val['workorder_id'] ])->get();
                                 if(isset($docPayment) && $docPayment[0]->work_order_id==$val['workorder_id']){
                                     $charges = '0';
                                 }
                          /*  $customer_package = \App\Models\Pricing::where(['package_id' => $customer_subscription['package_id'], 'notice_id' => $work_order['notice_id']])
                                ->get();*/
                          /*  $unpaid_amount = '00';
                            if(isset($notice_fields['unpaid_amount'])) {
                                    $unpaid_amount = $notice_fields['unpaid_amount'];
                                    foreach ($customer_package as $key => $pac) {
                                            if($unpaid_amount>=$pac->lower_limit && $unpaid_amount<=$pac->upper_limit){
                                            if($work_order['work_order_status'] == "6"){
                                                $charges = $pac->charge;
                                                }else{
                                                $charges = $pac->cancelation_charge;
                                                } 
                                            }
                                        }
                                    }*/
                        }else{
                            $notice_id_info = \App\Models\Pricing::join('notices','notices.id','pricings.notice_id')->where('notices.master_notice_id', $work_order['master_notice_id'])->where('package_id',$customer_subscription['package_id'])->select('pricings.notice_id as customer_notice_id')->first();
                            //dd($notice_id_info,$work_order,$customer_subscription);
                            $customer_package_notice_id = $work_order['notice_id'];
                                if (!empty($notice_id_info)){
                                $customer_package_notice_id = $notice_id_info['customer_notice_id'];
                                }
                            $customer_package = \App\Models\Customer_package::where(['customer_id' => $user->customer->id, 'notice_id' => $customer_package_notice_id])->where('package_id',$customer_subscription['package_id'])->first();
                            /* if customer dont have package take charges from pricing */
                                if(empty($customer_package)) {
                                    $customer_package = \App\Models\Pricing::where('notice_id',$customer_package_notice_id)->where('package_id',$customer_subscription['package_id'])
                                            ->first();
                                }
                                if ($work_order['work_order_status'] == "6") {
                                    $charges = $customer_package['charge'];
                                } else {
                                    $charges = $customer_package['cancelation_charge'];
                                }
                            }
                        }

                         //calculate charges for multiple addresses
                         
                            $no_of_addr = '';

                                foreach ($notice as $key => $val) {
                                    $field_value = \App\Models\Cyo_WorkOrder_fields::where(['notice_field_id' => $val['notice_field_id'], 'workorder_id' => $val['workorder_id']])->pluck('value')->toArray();
                                    $notice_fields[$val['notice_field_name']] = (isset($field_value[0])) ? $field_value[0] : '';
                                    
                                    if (isset($notice_fields['project_address']))
                                    {   
                                        $project_address = explode('**', $notice_fields['project_address']);
                                        $no_of_addr = count($project_address)-1;
                                        /*$additional_addr_charges = ($no_of_addr-1)* $basic_address_charges;*/  
                                    }
                                }

                             $notice_id_info = \App\Models\Pricing::join('notices','notices.id','pricings.notice_id')->where('pricings.lower_limit', '<=', $no_of_addr)
                                            ->where('pricings.upper_limit', '>=',$no_of_addr)->where('notices.master_notice_id', $work_order['master_notice_id'])->select('pricings.notice_id as customer_notice_id')->first();

                            $customer_package_notice_id = $work_order['notice_id'];
                            if (!empty($notice_id_info)) {
                                $customer_package_notice_id = $notice_id_info['customer_notice_id'];
                            }
                           
                            $customer_package = \App\Models\Customer_package::where(['customer_id' => $user->customer->id, 'notice_id' => $customer_package_notice_id])
                                    ->where('lower_limit', '<=', $no_of_addr)
                                    ->where('upper_limit', '>=', $no_of_addr)
                                    ->where('package_id',$customer_subscription['package_id'])
                                    ->where('additional_address','additional_address')->first();
                            /* if customer dont have package take charges from pricing */
                        if(empty($customer_package)) {
                            $customer_package = \App\Models\Pricing::where(['package_id' => $customer_subscription['package_id'], 'notice_id' => $customer_package_notice_id])
                                        ->where('lower_limit', '<=', $no_of_addr)
                                        ->where('upper_limit', '>=', $no_of_addr)
                                        ->where('additional_address','additional_address')->first();
                        }

                           /* $customer_package = \App\Models\Pricing::where(['package_id' => $customer_subscription['package_id'], 'notice_id' => $work_order['notice_id']])
                                        ->where('lower_limit', '=', 0)
                                        ->where('upper_limit', '=', 0)
                                        ->first();
                            */
                            $basic_address_charges = $customer_package['charge'];
                            if(!empty($customer_package)){
                               $additional_addr_charges =  $basic_address_charges * $no_of_addr;
                            }
                        /* check workorder status for charges => Cancelled or completed */
                        /*if ($work_order['work_order_status'] == "6") {
                            $charges = $customer_package['charge'];
                        } else {
                            $charges = $customer_package['cancelation_charge'];
                        }*/

                        if (!empty($notice) && !empty($recipients) && !empty($work_order)) {
                            $this->info('data found successfully for work order id ' . $work_order_id);
                            $generated_labels = \App\Models\Cyo_StampsLabel::whereIn('recipient_id', $recipients)
                                    ->where(['generated_label' => 'yes'])
                                    ->where('type_of_label', '!=', 'manual')
                                    ->groupBy(['type_of_label', 'type_of_mailing'])
                                    ->get([\DB::raw('count(id) as label_count'), 'tracking_number', 'ShipDate', 'DeliveryDate', \DB::raw('SUM(Amount) as total_amount'), 'type_of_label', 'type_of_mailing','Amount as unit_price'])
                                    ->toArray();

                            //generate 'manual' labels (Manual in invoice should show as a separate entry)
                            $manual_labels = \App\Models\Cyo_StampsLabel::whereIn('recipient_id', $recipients)
                            ->where(['generated_label' => 'yes'])
                            ->where('type_of_label', '=', 'manual')
                            ->get([\DB::raw('1 as label_count'), 'tracking_number', 'id', 'ShipDate', 'DeliveryDate', \DB::raw('Amount as total_amount'), 'type_of_label', 'type_of_mailing', 'Amount as unit_price'])
                            ->toArray();

                            //merge stamp labels & manual labels
                            $generated_labels = array_merge($generated_labels, $manual_labels);
                                
                           // if (!empty($generated_labels)) {
                               // $this->info('labels found');
                                $label_count = count($generated_labels);
                                $base_amount = ($charges) ? $charges : 30;

                                foreach ($notice as $key => $val) {
                                    $field_value = \App\Models\Cyo_WorkOrder_fields::where(['notice_field_id' => $val['notice_field_id'], 'workorder_id' => $val['workorder_id']])->pluck('value')->toArray();
                                    $notice_fields[$val['notice_field_name']] = (isset($field_value[0])) ? $field_value[0] : '';

                                    $description = [];
                                    $description[] = (isset($val['notice_name'])) ? $val['notice_name'] : '';

                                   /* if (isset($notice_fields['project_address'])) {
                                        $description[] = 'Project Address - ';
                                        $description[] = $notice_fields['project_address'];
                                    }*/
                                    if (isset($notice_fields['contracted_by'])) {
                                        $description[] = 'Contracted By - ' . $notice_fields['contracted_by'];
                                    }
                                    if (isset($notice_fields['parent_work_order']) && $notice_fields['parent_work_order'] != null) {
                                        $description[] = 'Parent Work Order Number - ' . $notice_fields['parent_work_order'];
                                    }
                                    if (isset($notice_fields['work_order_id'])) {
                                        $description[] = 'Current Work Order Number - ' . $notice_fields['work_order_id'];
                                    }
                                    if (isset($notice_fields['your_job_reference_no'])) {
                                     //   $description[] = 'Job Reference Number - ' . $notice_fields['your_job_reference_no'];
                                    }
                                    if (isset($notice_fields['job_start_date'])) {
                                       // $description[] = 'Project Address - ';
                                        $description[] = $notice_fields['job_start_date'];
                                    }
                                    if (isset($notice_fields['total_value'])) {
                                       // $description[] = 'Project Address - ';
                                        $total_value = $notice_fields['total_value'];


                                    }
                                    /*if (isset($notice_fields['project_address']))
                                    {
                                        if(isset($basic_address_charges) && !empty($basic_address_charges)){
                                        $project_address = explode('**', $notice_fields['project_address']);

                                        $no_of_addr = count($project_address);
                                        $additional_addr_charges = ($no_of_addr-1)* $basic_address_charges;  
                                        }
                                    }*/


                                    //dd($notice_fields['job_start_date']);
                                    $quickbooks_labels = [
                                        "Line" => [
                                            [
                                                "Amount" => $base_amount,
                                                "Description" => implode("\n", $description),
                                                "DetailType" => "SalesItemLineDetail",
                                                "SalesItemLineDetail" => [
                                                    "ItemRef" => [
                                                        "value" => $qboProductId,
                                                        "name" => "Notice"
                                                    ],
                                                    "UnitPrice" => $base_amount,
                                                    "Qty" => 1
                                                ]
                                            ],
                                        ]
                                    ];
                                    if (!empty($generated_labels) && $work_order['work_order_status'] == "6") {
                                    $label_description = ['manual' => 'Manual','next day delivery' => 'these are high priority delivery mails', 'priority mail' => 'Priority Mails', 'rush hour charges' => 'Rush Fee', 'Certified Mail' => 'Certified Mails', 'firm mail' => 'Firm Mails','Additional Address'=>'this are additional address charges','Return Receipt Requested'=>'these are return receipt requested','Electronic Return Receipt'=>'this are electronic return receipt'];
                                    $item_ref = [
                                        'Manual' => [
                                            "value" => config('constants.QBO.ManualCharges'),
                                            "name" => "Manual"
                                        ],
                                        'Cert. USPS' => [
                                            "value" => config('constants.QBO.CertifiedMail'),
                                            "name" => "Certified Mail"
                                        ],
                                        'Cert. RR' => [
                                            "value" => config('constants.QBO.ReturnReceiptRequested'),
                                            "name" => "Return Receipt Requested"
                                        ],
                                        'Cert. ER' => [
                                            "value" => config('constants.QBO.ElectronicReturnReceipt'),
                                            "name" => "Electronic Return Receipt"
                                        ],
                                        'next day delivery' => [
                                            "value" => config('constants.QBO.NextDayDelivery'),
                                            "name" => "Next Day Delivery"
                                        ],
                                        'priority mail' => [
                                            "value" => config('constants.QBO.PriorityMail'),
                                            "name" => "Priority Mail"
                                        ],
                                        'rush hour charges' => [
                                            "value" => config('constants.QBO.RushHourCharges'),
                                            "name" => "Rush Hour Charges"
                                        ],
                                        'firm mail' => [
                                            "value" => config('constants.QBO.FirmMail'),
                                            "name" => "Firm Mail"
                                        ],
                                        'Additional Address' => [
                                            "value" => config('constants.QBO.AdditionalAddress'),
                                            "name" => "Additional Address"
                                        ]
                                    ];

                                     //calculate charges with address
                                    if(isset($additional_addr_charges) && !empty($additional_addr_charges)){
                                        $quickbooks_labels["Line"][]= [
                                                'Amount' => $additional_addr_charges,
                                                'Description' => $label_description['Additional Address'],
                                                "DetailType" => "SalesItemLineDetail",
                                                "SalesItemLineDetail" => [
                                                    "ItemRef" => $item_ref['Additional Address'],
                                                    "Qty" => $no_of_addr,
                                                    "UnitPrice" => $basic_address_charges,
                                                    //"UnitPrice" => 10,
                                                ]
                                            ];
                                     }

                                    //check if we need to add rush hour charges
                                    if (isset($work_order['rush_hour_charges']) && $work_order['rush_hour_charges'] == 'yes' && $work_order['rush_hour_amount'] != 0) {
                                        $quickbooks_labels["Line"][] = [
                                            'Amount' => $work_order['rush_hour_amount'],
                                            'Description' => $label_description['rush hour charges'],
                                            "DetailType" => "SalesItemLineDetail",
                                            "SalesItemLineDetail" => [
                                                "ItemRef" => $item_ref['rush hour charges'],
                                                "Qty" => 1,
                                                "UnitPrice" => $work_order['rush_hour_amount'],
                                            ]
                                        ];
                                    }

                                    foreach ($generated_labels as $labels) {
                                        //Add ons used in stamps label creation
                                     
                                        if (isset($labels['type_of_mailing']) ) {
                                            if($labels['type_of_mailing'] == 'Certified Mail,Electronic Return Receipt'){
                                                $label_name = 'Cert. ER';
                                                }else if($labels['type_of_mailing'] == 'Certified Mail,Return Receipt Requested') {
                                                $label_name = 'Cert. RR'; 
                                                }else if($labels['type_of_mailing'] == 'Certified Mail'){
                                                $label_name = 'Cert. USPS'; 
                                                }else{
                                                $label_name = 'Cert. USPS'; 
                                                }
                                            $description = str_replace(',', ' with ', $labels['type_of_mailing']);
                                            $quickbooks_labels["Line"][] = [
                                                'Amount' => $labels['total_amount'],
                                                'Description' => ($description) ? $description : '',
                                                "DetailType" => "SalesItemLineDetail",
                                                "SalesItemLineDetail" => [
                                                    "ItemRef" => $item_ref[$label_name],
                                                    
                                                    //"UnitPrice" =>$labels['total_amount'],
                                                    "UnitPrice" => $labels['unit_price'],
                                                    "Qty" => $labels['label_count'],   
                                                ]
                                            ];
                                        }
                                       //add manual charges
                                         if (isset($labels['type_of_label']) && $labels['type_of_label'] == 'manual' ) {
                                            $description = str_replace(',', ' with ', $labels['type_of_mailing']);
                                            $quickbooks_labels["Line"][] = [
                                                'Amount' => $labels['total_amount'],
                                                'Description' => isset($labels['tracking_number']) ? $labels['tracking_number'] : 'Manual',
                                                "DetailType" => "SalesItemLineDetail",
                                                "SalesItemLineDetail" => [
                                                    "ItemRef" => $item_ref['Manual'],
                                                    "UnitPrice" => $labels['total_amount'],
                                                    "Qty" => 1,  
                                                   
                                                ]
                                            ];
                                        }

                                        //Check if firm mail or next day delivery
                                        if (isset($labels['type_of_label']) && $labels['type_of_label'] != 'stamps'&& $labels['type_of_label'] != 'manual') {
                                            $quickbooks_labels["Line"][] = [
                                                'Amount' => $labels['total_amount'],
                                                'Description' => isset($labels['type_of_label']) ? ucwords($labels['type_of_label']) : '',
                                                "DetailType" => "SalesItemLineDetail",
                                                "SalesItemLineDetail" => [
                                                    "ItemRef" => isset($item_ref[$labels['type_of_label']]) ? $item_ref[$labels['type_of_label']] : "",

                                                    "UnitPrice" => $labels['unit_price'],
                                                    "Qty" => $labels['label_count']
                                                ]
                                            ];
                                        }
                                    }
                                }
                                    $quickbooks_labels["CustomerRef"] = [
                                        "value" => $user->qbo_customer_id,
                                    ];
                                    if(isset($notice_fields['project_address'])){
                                        $project_addr = explode('**',$notice_fields['project_address']);
                                      //  dd(preg_replace('#<[^>]+>#', ' ', $project_addr[0]));
                                        $project_addr = preg_replace('#<[^>]+>#', ' ', $project_addr[0]);
                                    }
                                    $quickbooks_labels["CustomField"] = [
                                        [
                                            'DefinitionId' => '1',
                                            'Name' => 'Job Ref No',
                                            'Type' => 'StringType',
                                            'StringValue' => (isset($notice_fields['your_job_reference_no']) ? $notice_fields['your_job_reference_no'] : "")//$val['workorder_id']
                                        ],
                                        [
                                            'DefinitionId' => '2',
                                            'Name' => 'Notice Type',
                                            'Type' => 'StringType',
                                            'StringValue' => (isset($val['notice_name'])) ? substr($val['notice_name'], 0, 30) : ''
                                        ],
                                        [
                                            'DefinitionId' => '3',
                                            'Name' => 'Job Location',
                                            'Type' => 'StringType',
                                            'StringValue' => (isset($notice_fields['project_address'])) ? substr($project_addr, 0, 30) : ''
                                        ],
                                   /* [
                                            'DefinitionId' => '4',
                                            'Name' => 'Customer Email',
                                            'Type' => 'StringType',
                                            'StringValue' => $user->email
                                    ]*/
                                ];
                                    //$quickbooks_labels["DocNumber"] = $val['workorder_id'];
                                    $quickbooks_labels["DocNumber"] = $val['workorder_id'].'_CYO_'.time();

                                    $quickbooks_labels["SalesTermRef"] = [
                                        "value" => 2
                                    ];
                                   /* $quickbooks_labels["Email"] = [
                                        "value" => $user->email
                                    ];*/


                                    /*if(isset($notice_fields['job_start_date'])){
                                        $duedateDays = 40 . 'days';
                                        $due_date = date('Y-m-d', strtotime('+ ' . $duedateDays, strtotime($notice_fields['job_start_date'])));
                                        $quickbooks_labels["DueDate"] = $due_date;
                                    }*/
                                     
                                }
                            // }else {
                            //     \Log::error('Could not generate invoice as >> Generate Label != yes ');
                            // }
                            
                            if (!empty($quickbooks_labels)) {
                                $this->info('invoice generation data set for work order id ' . $work_order_id);
                                try{
                                    $qbo = new \App\Http\Controllers\QuickBooksController();
                                    $qbo_response = $qbo->generateTokens(config('constants.QBO.SCOPE'), 'createInvoice', ['customer_id' => $user->id, 'invoice_details' => $quickbooks_labels]);
     
                                    $this->info($qbo_response['message']);
                                    if ($qbo_response['response'] == 'success') {
                                        \App\Models\Cyo_WorkOrders::where(['id' => $work_order_id])->update(['invoice_generated' => 'Yes']);
                                        $this->info('Process completed for work order id ' . $work_order_id);
                                    } else {
                                        \Log::error($qbo_response['message'] . ' for work order id ' . $work_order_id);
                                    }
                                } catch (\Exception $e) {
                                    $this->info('Exception caught from QB for WO '.$work_order_id);
                                    \Log::error('Exception caught from QB for WO '.$work_order_id.' >> '. $e->getMessage(). ' at line '.$e->getLine(). ' in file '.$e->getFile());
                                    \Log::error($e->getTraceAsString());
                                }
                                // return redirect('customer/statement-invoices')->with($qbo_response['response'], $qbo_response['message']);
                            } else {
                                \Log::error('Could not generate invoice for work order id ' . $work_order_id);
                                //return redirect('customer/statement-invoices')->with('error', 'Could not generate invoice');
                            }
                        }
                    } else {
                        $this->info('Quickbooks customer id not found for user with id ' . $user->id);
                        \Log::error('Quickbooks customer id not found for user with id ' . $user->id);
                    }
                }
            } else {
                $this->info('No completed work orders found');
                \Log::error('No completed work orders found');
            }
        } catch (\Exception $e) {
            \Log::error($e->getTraceAsString());
        }
    }

}
