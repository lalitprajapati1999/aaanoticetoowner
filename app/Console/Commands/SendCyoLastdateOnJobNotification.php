<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers\Client;
use App\Models\QuickbooksToken;
use DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\LastDateNotificationMail;
use App\Mail\DuedateClaimLienNotificationMail;
class SendCyoLastDateOnJobNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'SendCyoLastDateOnJobNotification:sendWorkOrderNotification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'To send the work order last date on job notification to customer';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Get all the work order data
         $WorkOrderFields = \App\Models\Cyo_WorkOrder_fields::select('cyo__work_orders.parent_id', 'cyo__work_orders.created_at', 'cyo__work_orders.user_id', 'cyo__work_orders.customer_id', 'cyo__work_orders.notice_id as notice_id', 'usr.name as account_manager_name', 'cyo__work_orders.status', 'cyo__work_orders.user_id', 'cyo__work_orders.account_manager_id', 'cyo__work_order_fields.workorder_id', 'cyo__work_order_fields.notice_id', DB::raw("(GROUP_CONCAT(notice_fields.name SEPARATOR '||')) as `field_names`"), DB::raw("(GROUP_CONCAT(value SEPARATOR '||')) as `field_values`"), 'users.email', 'cyo__work_orders.discontinue_reminder', 'users.name','usr.email as account_manager_email','customers.company_name')
                        ->rightjoin('notice_fields', 'notice_fields.id', '=', 'cyo__work_order_fields.notice_field_id')
                        ->join('cyo__work_orders', 'cyo__work_orders.id', '=', 'cyo__work_order_fields.workorder_id')
                        ->leftjoin('users', 'users.id', '=', 'cyo__work_orders.user_id')
                        ->leftjoin('customers', 'customers.id', '=', 'cyo__work_orders.customer_id')
                        ->leftjoin('users as usr', 'usr.id', '=', 'cyo__work_orders.account_manager_id')
                        ->groupBy('cyo__work_order_fields.workorder_id', 'cyo__work_order_fields.notice_id')
                        ->get()->toArray();
        $result = [];
        if (isset($WorkOrderFields) && !empty($WorkOrderFields)) {
            foreach ($WorkOrderFields as $fields_data) {
                $field_names = explode('||', $fields_data['field_names']);
                $field_values = explode('||', $fields_data['field_values']);
                $field_names_values = (count($field_names)==count($field_values))?array_combine($field_names, $field_values):[];
                $field_names_values['default'] = '';
                $result[] = array_merge($fields_data, $field_names_values);
            }
        }else{
            $this->info('No records found.');
        }

        $count = 0;
        if (isset($result) && !empty($result)) {
            foreach ($result AS $k => $each_val) {
                 $notice_type = \App\Models\Notice::find($each_val['notice_id']);
                 /*
                  * If notice type is 2 means it is soft document
                  */
            $last_master_ids= [config('constants.MASTER_NOTICE')['NPN']['ID'],config('constants.MASTER_NOTICE')['COL']['ID']];
            if(in_array($each_val['notice_id'],$last_master_ids)){ 
                /*if(isset($each_val['last_date_on_the_job']) && $each_val['last_date_on_the_job']=="" ){*/
                    $parent_last_date_on_job = "";
                    $result1 = [];
                    if (isset($each_val['parent_work_order']) && !empty($each_val['parent_work_order'])) { 
                        $parent_WorkOrderFields = \App\Models\WorkOrderFields::select('work_orders.parent_id', 'work_orders.created_at', 'work_orders.user_id', 'work_orders.customer_id', 'work_orders.notice_id as notice_id', 'work_orders.status', 'work_orders.user_id', 'work_orders.account_manager_id', 'work_order_fields.workorder_id', 'work_order_fields.notice_id', DB::raw("(GROUP_CONCAT(notice_fields.name SEPARATOR '||')) as `field_names`"), DB::raw("(GROUP_CONCAT(value SEPARATOR '||')) as `field_values`"), 'users.email','work_orders.discontinue_reminder','users.name')
                        ->rightjoin('notice_fields', 'notice_fields.id', '=', 'work_order_fields.notice_field_id')
                        ->join('work_orders', 'work_orders.id', '=', 'work_order_fields.workorder_id')
                        ->leftjoin('users', 'users.id', '=', 'work_orders.user_id')
                        ->groupBy('work_order_fields.workorder_id', 'work_order_fields.notice_id')->where('work_orders.id',$each_val['parent_work_order'])
                        ->get()->toArray();
                        
                        if (isset($parent_WorkOrderFields) && !empty($parent_WorkOrderFields)) {
                            foreach ($parent_WorkOrderFields as $fields_data) {
                                $field_names = explode('||', $fields_data['field_names']);
                                $field_values = explode('||', $fields_data['field_values']);
                                $field_names_values = (count($field_names)==count($field_values))?array_combine($field_names, $field_values):[];
                                $field_names_values['default'] = '';
                                $result1[] = array_merge($fields_data, $field_names_values);
                            }
                        }
                    }
                        
                    if (isset($result1) && !empty($result1)) {
                        foreach ($result1 AS $k => $each_val1) {
                            if(isset($each_val1['last_date_on_the_job']) && $each_val1['last_date_on_the_job']!="" ){
                                $parent_last_date_on_job = $each_val1['last_date_on_the_job'];
                                 /* $each_val['last_date_on_the_job'] = $parent_last_date_on_job;*/
                            }
                        }
                        //}
                    }
                   if(isset($each_val['last_date_on_the_job']) && $each_val['last_date_on_the_job'] != ''){

                    }else{
                        $each_val['last_date_on_the_job'] = $parent_last_date_on_job;
                    }
                   if (isset($each_val['last_date_on_the_job']) && $each_val['last_date_on_the_job'] != '') {
                       $todays_date = date('m/d/Y');
                        $work_order_id = $each_val['workorder_id'];
                        $days70 = 70 . 'days';
                        $days80 = 80 . 'days';
                        $days70_date = date('m/d/Y', strtotime('+ ' . $days70, strtotime($each_val['last_date_on_the_job'])));
                        $days80_date = date('m/d/Y', strtotime('+ ' . $days80, strtotime($each_val['last_date_on_the_job'])));
                       /* $each_val['stopnotificationurl']= url("dueDate/discontinueReminder/$work_order_id");*/
                        
                        if (!$each_val['discontinue_reminder']) {
                            if ($todays_date == $days70_date || $todays_date== $days80_date) {
                                 $each_val['stopnotificationurl']= url("dueDate/discontinueReminder/$work_order_id");  
                                if(isset($each_val['email']) && $each_val['email'] !=''){
                                   Mail::to($each_val['email'])->send(new LastDateNotificationMail($each_val));
                                    Mail::to($each_val['account_manager_email'])->send(new LastDateNotificationMail($each_val));
                                   $this->info('Notification send successfully.');
                                   $count ++;
                                    }else{
                                          $this->error('Customer email is invalid.');
                                    }
                                }else if ($todays_date== $days80_date) {
                                $each_val['stopnotificationurl']= "";
                                if(isset($each_val['email']) && $each_val['email'] !=''){
                                   Mail::to($each_val['email'])->send(new LastDateNotificationMail($each_val));
                                    Mail::to($each_val['account_manager_email'])->send(new LastDateNotificationMail($each_val));
                                   $this->info('Notification send successfully.');
                                   $count ++;
                                    }else{
                                          $this->error('Customer email is invalid.');
                                    }
                                }
                        }
                    }
                       
                   }
            }
        }   
        if($count == 0){
            $this->info('No records found');
        }
    }
}
