<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers\Client;
use App\Models\QuickbooksToken;
use Illuminate\Support\Facades\Log;

class GetQuickbooksTokens extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'GetQuickbooksTokens:getTokens';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'To get new access token using refresh token';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::debug('in get token cron');
        $tokenEndPointUrl = config('constants.QBO.TOK_ENDPOINT_URL');
        $client_id = config('constants.QBO.CONSUMER_KEY');
        $client_secret = config('constants.QBO.CONSUMER_SECRET');
        $grant_type= 'refresh_token';
        $base_url = url('/');
        // $certFilePath = './public/Certificate/cacert.pem';
        // $certFilePath = $_SERVER['DOCUMENT_ROOT'];
        $certFilePath = public_path()."/Certificate/cacert.pem";
        Log::debug('Get accounting token starts');
        //Get accounting token
        $token_record = QuickbooksToken::where(['scope'=>config('constants.QBO.SCOPE'),'company_id'=>config('constants.QBO.COMPANY_ID'),'customer_id'=>1])->first()->toArray();
        if(!empty($token_record))
        {
            $this->comment('Generating new access token for accounting APIs...');
            try {
                
                $client = new Client($client_id, $client_secret, $certFilePath);
                Log::debug($tokenEndPointUrl);
                Log::debug($grant_type);
                Log::debug($token_record['refresh_token']);
                //dd($tokenEndPointUrl,$grant_type,$token_record['refresh_token']);
                $result = $client->refreshAccessToken($tokenEndPointUrl, $grant_type, $token_record['refresh_token']);
               
                if(isset($result['error']) && $result['error'] != '')
                {
                    Log::debug('acc error');
                    Log::debug($result['error']);
                    $this->error($result['error']);
                    $this->error('New accounting API access token could not be generated.');
                }
                else
                {
                    $now = date('Y-m-d H:i:s');
                    $token_expiry = date('Y-m-d H:i:s',strtotime('+ '.$result['expires_in'].' seconds'));
                    $refresh_expiry = date('Y-m-d H:i:s',strtotime('+ '.$result['x_refresh_token_expires_in'].' seconds'));
                    
                    $token_data = [
                        'access_token'=>$result['access_token'],
                        'refresh_token'=>$result['refresh_token'],
                        'token_expires_at'=>$token_expiry,
                        'refresh_token_expires_at'=>$refresh_expiry,
                        'updated_at'=>$now,
                        'company_id'=>$token_record['company_id'],
                        'customer_id'=>1,
                        'scope' =>$token_record['scope']
                    ];
                    Log::debug('acount token data');
                    Log::debug($token_data);
                    QuickbooksToken::where(['scope'=>$token_record['scope'],'company_id'=>$token_record['company_id'],'customer_id'=>1])->update($token_data);
                    $this->info('New access token for accounting API generated successfully.');
                }                
            } catch (\Exception $e) {
                Log::debug('in account exception');
                Log::debug($e->getMessage());
                $this->error('New access token could not be generated.');
                throw new \Exception($e->getMessage(), 1);
            }
        }
        else
        {
            Log::debug('Accounting API token record not found.');
            $this->error('Accounting API token record not found.');
        }
        Log::debug('Get payment token starts');
        //Get accounting token
        $token_record = QuickbooksToken::where(['scope'=>config('constants.QBO.PAYMENT_SCOPE'),'company_id'=>config('constants.QBO.COMPANY_ID'),'customer_id'=>1])->first()->toArray();
        if(!empty($token_record))
        {
            $this->comment('Generating new access token for payment APIs...');
            try {
                Log::debug('in payment');
                $client = new Client($client_id, $client_secret, $certFilePath);

                $result = $client->refreshAccessToken($tokenEndPointUrl, $grant_type, $token_record['refresh_token']);
                
                if(isset($result['error']) && $result['error'] != '')
                {
                    Log::debug('payment error');
                    Log::debug($result['error']);
                    $this->error($result['error']);
                    $this->error('New payment API access token could not be generated.');
                }
                else
                {
                    $now = date('Y-m-d H:i:s');
                    $token_expiry = date('Y-m-d H:i:s',strtotime('+ '.$result['expires_in'].' seconds'));
                    $refresh_expiry = date('Y-m-d H:i:s',strtotime('+ '.$result['x_refresh_token_expires_in'].' seconds'));
                    
                    $token_data = [
                        'access_token'=>$result['access_token'],
                        'refresh_token'=>$result['refresh_token'],
                        'token_expires_at'=>$token_expiry,
                        'refresh_token_expires_at'=>$refresh_expiry,
                        'updated_at'=>$now,
                        'company_id'=>$token_record['company_id'],
                        'customer_id'=>1,
                        'scope' =>$token_record['scope']
                    ];
                    Log::debug('payment token data');
                    Log::debug($token_data);
                    QuickbooksToken::where(['scope'=>$token_record['scope'],'company_id'=>$token_record['company_id'],'customer_id'=>1])->update($token_data);
                    $this->info('New access token for payment API generated successfully.');
                }                
            } catch (\Exception $e) {
                Log::debug('in payment exception');
                Log::debug($e->getMessage());
                $this->error('New access token could not be generated.');
                \Log::error($e->getTraceAsString());
                throw new \Exception($e->getMessage(), 1);
            }
        }
        else
        {
            Log::debug('Payment API token record not found.');
            $this->error('Payment API token record not found.');
        }    
        
    }
}
