<?php

namespace App\Console\Commands\Backpack;

use Illuminate\Console\Command;

class PermissionManager extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'backpack:permissionmanager';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Roles and Permissions Table Data from Backpack configuration.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Insert Roles and Permissions
        $this->comment('Syncing Roles and Permissions...');
        try {
            config('auth.providers.users.model')::updatePermissions();
            $this->info('Roles and Permissions are synced successfully.');
        } catch (\Exception $e) {
            $this->error('Roles and Permissions couldn\'t be synced.');
            throw new \Exception($e->getMessage(), 1);
        }
    }
}
