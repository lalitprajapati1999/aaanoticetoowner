<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\WorkOrder;

class GeneratingQuickbooksInvoice extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'GeneratingQuickbooksInvoice:generatingInvoice';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'To generate monthly invoice for work order';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->info('Generating invoices for workorders');
            $generated_labels = [];
            $notice = [];
            $notice_fields = [];
            $labels = [];
            $quickbooks_labels = [];
            $first_date_of_month = date('Y-m-01');
            $last_date_of_month = date('Y-m-t');

            /* get workorder status completed records */

            $all_work_order = \App\Models\WorkOrder::join('notices', 'work_orders.notice_id', '=', 'notices.id')
                ->join('master_notices', 'master_notices.id', '=', 'notices.master_notice_id')
                ->whereIn('work_orders.status', ['6', '7'])
                ->where(['work_orders.invoice_generated' => 'No'])
                ->where(['work_orders.is_billable' => 1])
                ->orderBy('id', 'desc')
                ->get(['rush_hour_amount', 'rush_hour_charges', 'work_orders.id', 'work_orders.status as work_order_status', 'user_id', 'type as notice_type', 'notice_id', 'master_notice_id', 'master_notices.qbo_product_id'])
                ->toArray();
                

            if (!empty($all_work_order)) {
                $this->info('Completed work order records found');
                foreach ($all_work_order as $work_order) {
                    $additional_addr_charges = 0;
                    $basic_address_charges = 0;
                    $work_order_id = $work_order['id'];
                    $qboProductId = $work_order['qbo_product_id'] ?? config('constants.QBO.NoticeBaseAmount');
                    $user = \App\Models\User::find($work_order['user_id']);
                    
                    //Check if the user is registered in quickbooks, if no then we cannot generate invoice for that user
                    if ($user->qbo_customer_id != '') {
                        $this->info('QBO customer id found for user with id ' . $work_order['user_id']);
                        $notice = \App\Models\Notice::join('work_orders', 'work_orders.notice_id', '=', 'notices.id')
                            ->join('notice_fields', 'notice_fields.notice_id', '=', 'notices.id')
                            ->where(['work_orders.id' => $work_order_id])
                            ->get([\DB::raw('notice_fields.id as notice_field_id'), \DB::raw('work_orders.id as workorder_id'), \DB::raw('notice_fields.name as notice_field_name'), \DB::raw('notices.name as notice_name'), \DB::raw('notices.id as notice_id1'), \DB::raw('work_orders.order_no as order_no'), \DB::raw('notices.master_notice_id as master_notice_id'), \DB::raw('work_orders.created_at as work_order_date')])
                            ->toArray();

                        $recipients = \App\Models\Recipient::join('work_orders', 'work_orders.id', '=', 'recipients.work_order_id')
                            ->where(['work_orders.id' => $work_order_id])
                            ->pluck('recipients.id');

                        /* get customer Work order count for => same month , same notice id  */
                        if ($work_order['work_order_status'] == "6") {
                            /* check date with completed_at */
                            $workorder_count = \App\Models\WorkOrder::where('notice_id', $work_order['notice_id'])
                                ->where('user_id', $work_order['user_id'])
                                ->where('status', $work_order['work_order_status'])
                                ->where('work_orders.completed_at', '>=', $first_date_of_month)
                                ->where('work_orders.completed_at', '<=', $last_date_of_month)->count();
                        } else {
                            /* check date with cancelled at */
                            $workorder_count = \App\Models\WorkOrder::where('notice_id', $work_order['notice_id'])
                                ->where('user_id', $work_order['user_id'])
                                ->where('status', $work_order['work_order_status'])
                                ->where('work_orders.cancelled_at', '>=', $first_date_of_month)
                                            ->where('work_orders.cancelled_at', '<=', $last_date_of_month)->count();
                        }



                        if ($work_order['notice_type'] == "2") {
                            /* soft Notice */
                            /* get customer notice id to find charges form customer pacakge */
                            /* $notice_id_info = \App\Models\Pricing::join('pricings as pricings_self', 'pricings.id', '=', 'pricings_self.pricing_copied_id')
                                            join('notices','notices.id','pricings.notice_id')->where('pricings.lower_limit', '<=', $workorder_count)
                                            ->where('pricings.upper_limit', '>=', $workorder_count)
                                            ->where('pricings.notice_id', $work_order['notice_id'])
                                            ->where('pricings.pricing_copied_id', '!=', '0')
                                            ->select('pricings_self.notice_id as customer_notice_id')->first();*/
                            $notice_id_info = \App\Models\Pricing::join('notices', 'notices.id', 'pricings.notice_id')->where('pricings.lower_limit', '<=', $workorder_count)
                                ->where('pricings.upper_limit', '>=', $workorder_count)->where('notices.master_notice_id', $work_order['master_notice_id'])->select('pricings.notice_id as customer_notice_id')->first();

                            $customer_package_notice_id = $work_order['notice_id'];
                            if (!empty($notice_id_info)) {
                                $customer_package_notice_id = $notice_id_info['customer_notice_id'];
                            }
                            /* Soft Notice Price upper limit and lower limit     */
                            $customer_package = \App\Models\Customer_package::where(['customer_id' => $user->customer->id, 'notice_id' => $customer_package_notice_id, 'package_id' => '1'])
                                ->where('lower_limit', '<=', $workorder_count)
                                ->where('upper_limit', '>=', $workorder_count)
                                ->first();
                            /*if customer dont have package take charges from pricing */

                            if (empty($customer_package)) {
                                $customer_package = \App\Models\Pricing::where(['package_id' => '1', 'notice_id' => $customer_package_notice_id])
                                    ->where('lower_limit', '<=', $workorder_count)
                                    ->where('upper_limit', '>=', $workorder_count)
                                    ->first();
                            }

                            /* check workorder status for charges => Cancelled or completed */
                            if ($work_order['work_order_status'] == "6") {
                                $charges = $customer_package['charge'];
                            } else {
                                $charges = $customer_package['cancelation_charge'];
                            }
                            /*if customer enter more than 1 address then chanrges calculation*/

                            /* $customer_package = \App\Models\Customer_package::where(['customer_id' => $user->customer->id, 'notice_id' => $customer_package_notice_id])
                            ->where('lower_limit', '=', 0)
                            ->where('upper_limit', '=', 0)
                            ->first();
                            if (empty($customer_package)) {
                                $customer_package = \App\Models\Pricing::where(['package_id' => '1', 'notice_id' => $work_order['notice_id']])
                                        ->where('lower_limit', '=', 0)
                                        ->where('upper_limit', '=', 0)
                                        ->first();
                            }*/
                            /*if customer enter more than 1 address then chanrges calculation*/
                            $no_of_addr = '';

                            foreach ($notice as $key => $val) {
                                $field_value = \App\Models\WorkOrderFields::where(['notice_field_id' => $val['notice_field_id'], 'workorder_id' => $val['workorder_id']])->pluck('value')->toArray();
                                $notice_fields[$val['notice_field_name']] = (isset($field_value[0])) ? $field_value[0] : '';

                                if (isset($notice_fields['project_address'])) {
                                    $project_address = explode('**', $notice_fields['project_address']);
                                    $no_of_addr = count($project_address) - 1;
                                    /*$additional_addr_charges = ($no_of_addr-1)* $basic_address_charges;*/
                                }
                            }

                            $customer_package = \App\Models\Customer_package::where(['customer_id' => $user->customer->id, 'notice_id' => $customer_package_notice_id, 'package_id' => '1'])
                                ->where('lower_limit', '<=', $no_of_addr)
                                ->where('upper_limit', '>=', $no_of_addr)
                                ->where('additional_address', 'additional_address')->first();
                            /* if customer dont have package take charges from pricing */

                            if (empty($customer_package)) {
                                $customer_package = \App\Models\Pricing::where(['package_id' => '1', 'notice_id' => $customer_package_notice_id])
                                    ->where('lower_limit', '<=', $no_of_addr)
                                    ->where('upper_limit', '>=', $no_of_addr)
                                    ->where('additional_address', 'additional_address')->first();
                            }

                            $basic_address_charges  = $customer_package['charge'];
                            if (!empty($customer_package)) {
                                $additional_addr_charges =  $customer_package['charge'] * $no_of_addr;
                            }
                        } else {
                            /* Hard Notice Price */
                            /* get customer notice id to find charges form customer pacakge */

                            $master_notice_id = \App\Models\Notice::find($work_order['notice_id']);
                            $master_notice_id = $master_notice_id->master_notice_id;

                            if ($master_notice_id == config('constants.MASTER_NOTICE')['COL']['ID'] || $master_notice_id == config('constants.MASTER_NOTICE')['BCOL']['ID']) {
                                $unpaid_amount = 00;

                                foreach ($notice as $key => $val) {
                                    $field_value = \App\Models\WorkOrderFields::where(['notice_field_id' => $val['notice_field_id'], 'workorder_id' => $val['workorder_id']])->pluck('value')->toArray();
                                    $notice_fields[$val['notice_field_name']] = (isset($field_value[0])) ? $field_value[0] : '';

                                    if (isset($notice_fields['unpaid_amount'])) {
                                        $unpaid_amount = $notice_fields['unpaid_amount'];
                                    }
                                }

                                $notice_id_info = \App\Models\Pricing::join('notices', 'notices.id', 'pricings.notice_id')->where('pricings.lower_limit', '<=', $unpaid_amount)
                                    ->where('pricings.upper_limit', '>=', $unpaid_amount)->where('notices.master_notice_id', $work_order['master_notice_id'])->select('pricings.notice_id as customer_notice_id')->first();

                                /* $notice_id_info = \App\Models\Pricing::
                                                where('pricings.notice_id', $work_order['notice_id'])->get();*/
                                $customer_package_notice_id = $work_order['notice_id'];
                                if (!empty($notice_id_info)) {
                                    $customer_package_notice_id = $notice_id_info['customer_notice_id'];
                                }

                                /* Hard Notice Price upper limit and lower limit     */
                                $customer_package = \App\Models\Customer_package::where(['customer_id' => $user->customer->id, 'notice_id' => $customer_package_notice_id, 'package_id' => '1'])
                                    ->where('lower_limit', '<=', $unpaid_amount)
                                    ->where('upper_limit', '>=', $unpaid_amount)
                                    ->first();
                                /* if customer dont have package take charges from pricing */
                                if (empty($customer_package)) {
                                    $customer_package = \App\Models\Pricing::where(['package_id' => '1', 'notice_id' => $customer_package_notice_id])
                                        ->where('lower_limit', '<=', $unpaid_amount)
                                        ->where('upper_limit', '>=', $unpaid_amount)
                                        ->first();
                                }
                                /* check workorder status for charges => Cancelled or completed */
                                if ($work_order['work_order_status'] == "6") {
                                    $charges = $customer_package['charge'];
                                } else {
                                    $charges = $customer_package['cancelation_charge'];
                                }
                            } else {

                                $notice_id_info = \App\Models\Pricing::join('notices', 'notices.id', 'pricings.notice_id')->where('notices.master_notice_id', $work_order['master_notice_id'])->select('pricings.notice_id as customer_notice_id')->first();
                                $customer_package_notice_id = $work_order['notice_id'];
                                if (!empty($notice_id_info)) {
                                    $customer_package_notice_id = $notice_id_info['customer_notice_id'];
                                }

                                /* Soft Notice Price upper limit and lower limit     */
                                $customer_package = \App\Models\Customer_package::where(['customer_id' => $user->customer->id, 'notice_id' => $customer_package_notice_id, 'package_id' => '1'])
                                    ->first();
                                /* if customer dont have package take charges from pricing */
                                if (empty($customer_package)) {
                                    $customer_package = \App\Models\Pricing::where(['package_id' => '1', 'notice_id' => $customer_package_notice_id])
                                        ->first();
                                }
                                /* check workorder status for charges => Cancelled or completed */
                                if ($work_order['work_order_status'] == "6") {
                                    $charges = $customer_package['charge'];
                                } else {
                                    $charges = $customer_package['cancelation_charge'];
                                }
                            }
                        }

                        if (!empty($notice) && !empty($recipients) && !empty($work_order)) {
                            $this->info('===========' . $work_order_id . '============');
                            $this->info('notice, receipients & wo data found');
                            $generated_labels = \App\Models\StampsLabel::whereIn('recipient_id', $recipients)
                                ->where(['generated_label' => 'yes'])
                                ->where('type_of_label', '!=', 'manual')
                                ->groupBy(['type_of_label', 'type_of_mailing'])
                                ->get([\DB::raw('count(id) as label_count'), 'tracking_number', 'id', 'ShipDate', 'DeliveryDate', \DB::raw('SUM(Amount) as total_amount'), 'type_of_label', 'type_of_mailing', 'Amount as unit_price'])
                                ->toArray();

                            //generate 'manual' labels (Manual in invoice should show as a separate entry)
                            $manual_labels = \App\Models\StampsLabel::whereIn('recipient_id', $recipients)
                                ->where(['generated_label' => 'yes'])
                                ->where('type_of_label', '=', 'manual')
                                ->get([\DB::raw('1 as label_count'), 'tracking_number', 'id', 'ShipDate', 'DeliveryDate', \DB::raw('Amount as total_amount'), 'type_of_label', 'type_of_mailing', 'Amount as unit_price'])
                                ->toArray();

                            //merge stamp labels & manual labels
                            $generated_labels = array_merge($generated_labels, $manual_labels);

                            $this->info('labels found');
                            $label_count = count($generated_labels);
                            $base_amount = ($charges) ? $charges : 30;

                            foreach ($notice as $key => $val) {
                                $field_value = \App\Models\WorkOrderFields::where(['notice_field_id' => $val['notice_field_id'], 'workorder_id' => $val['workorder_id']])->pluck('value')->toArray();
                                $notice_fields[$val['notice_field_name']] = (isset($field_value[0])) ? $field_value[0] : '';
                                $description = [];
                                // $description[] = (isset($val['notice_name']))?$val['notice_name']:'';
                                // if (isset($notice_fields['project_address'])) {
                                //     $description[] = 'Project Address - '; $description[] = $notice_fields['project_address'];
                                // }
                                if (isset($notice_fields['contracted_by'])) {
                                    $description[] = 'Contracted By - ' . $notice_fields['contracted_by'];
                                }
                                if (isset($notice_fields['parent_work_order']) && $notice_fields['parent_work_order'] != null) {
                                    $description[] = 'Parent Work Order Number - ' . $notice_fields['parent_work_order'];
                                }
                                // if (isset($notice_fields['work_order_id'])) {
                                //     $description[] = 'Current Work Order Number - ' . $notice_fields['work_order_id'];
                                // }
                                if (isset($notice_fields['your_job_reference_no'])) {
                                    //$description[] = 'Job Reference Number - ' . $notice_fields['your_job_reference_no'];
                                }

                                /*  if (isset($notice_fields['project_address']))
                                    {
                                        if(isset($basic_address_charges) && !empty($basic_address_charges)){
                                        $project_address = explode('**', $notice_fields['project_address']);
                                       
                                        $no_of_addr = count($project_address);
                                        $additional_addr_charges = ($no_of_addr-1)* $basic_address_charges;  
                                        }
                                    }*/

                                //required to remove empty values from description array

                                $description = array_filter($description);

                                $quickbooks_labels = [
                                    "Line" => [
                                        [
                                            "Amount" => $base_amount,
                                            "Description" => implode("\n", $description),
                                            "DetailType" => "SalesItemLineDetail",
                                            "SalesItemLineDetail" => [
                                                "ItemRef" => [
                                                    "value" => $qboProductId,
                                                    "name" => "Notice"
                                                ],
                                                "UnitPrice" => $base_amount,
                                                "Qty" => 1
                                            ]
                                        ],
                                    ]
                                ];

                                $label_description = ['manual' => 'Manual', 'next day delivery' => 'these are high priority delivery mails', 'priority mail' => 'these are priority mails', 'rush hour charges' => 'Rush Fee', 'Cert. USPS' => 'these are certified mails', 'Cert. ER' => 'these are electronic return receipt', 'Cert. RR' => 'these are return receipt requested', 'firm mail' => 'Firm Mails', 'Additional Address' => 'Additional address charges', 'Certified Mail,Return Receipt Requested' => 'these are certified mail and return receipt requested'];
                                $item_ref = [
                                    'Manual' => [
                                        "value" => config('constants.QBO.ManualCharges'),
                                        "name" => "Manual"
                                    ],
                                    'Cert. USPS' => [
                                        "value" => config('constants.QBO.CertifiedMail'),
                                        "name" => "Certified Mail"
                                    ],
                                    'Cert. RR' => [
                                        "value" => config('constants.QBO.ReturnReceiptRequested'),
                                        "name" => "Return Receipt Requested"
                                    ],
                                    'Cert. ER' => [
                                        "value" => config('constants.QBO.ElectronicReturnReceipt'),
                                        "name" => "Electronic Return Receipt"
                                    ],
                                    'next day delivery' => [
                                        "value" => config('constants.QBO.NextDayDelivery'),
                                        "name" => "Next Day Delivery"
                                    ],
                                    'Priority Mail' => [
                                        "value" => config('constants.QBO.PriorityMail'),
                                        "name" => "Priority Mail"
                                    ],
                                    'rush hour charges' => [
                                        "value" => config('constants.QBO.RushHourCharges'),
                                        "name" => "Rush Hour Charges"
                                    ],
                                    'firm mail' => [
                                        "value" => config('constants.QBO.FirmMail'),
                                        "name" => "Firm Mail"
                                    ],
                                    'Additional Address' => [
                                        "value" => config('constants.QBO.AdditionalAddress'),
                                        "name" => "Additional Address"
                                    ]
                                ];


                                //calculate charges with address
                                if (isset($additional_addr_charges) && !empty($additional_addr_charges)) {
                                    $quickbooks_labels["Line"][] = [
                                        'Amount' => $additional_addr_charges,
                                        'Description' => $label_description['Additional Address'],
                                        "DetailType" => "SalesItemLineDetail",
                                        "SalesItemLineDetail" => [
                                            "ItemRef" => $item_ref['Additional Address'],
                                            "Qty" => $no_of_addr,
                                            'UnitPrice' => $basic_address_charges,
                                            //"UnitPrice" => $additional_addr_charges,
                                            //"UnitPrice" => 10,
                                        ]
                                    ];
                                }


                                //check if we need to add rush hour charges
                                if (isset($work_order['rush_hour_charges']) && $work_order['rush_hour_charges'] == 'yes' && $work_order['rush_hour_amount'] != 0) {
                                    $quickbooks_labels["Line"][] = [
                                        'Amount' => $work_order['rush_hour_amount'],
                                        'Description' => $label_description['rush hour charges'],
                                        "DetailType" => "SalesItemLineDetail",
                                        "SalesItemLineDetail" => [
                                            "ItemRef" => $item_ref['rush hour charges'],
                                            "Qty" => 1,
                                            "UnitPrice" => $work_order['rush_hour_amount'],
                                        ]
                                    ];
                                }
                                //calculate charges with address
                                /* $quickbooks_labels =[
                                    "Line" =>[
                                    [
                                            'Amount' => 10,
                                            'Description' => $label_description['additional address charges'],
                                            "DetailType" => "SalesItemLineDetail",
                                            "SalesItemLineDetail" => [
                                                "ItemRef" => $item_ref['additional address charges'],
                                                "Qty" => 1,
                                                "UnitPrice" => 10,
                                            ]
                                        ]
                                    ]
                                ];*/

                                if (!empty($generated_labels)) {
                                    foreach ($generated_labels as $labels) {
                                        if (isset($labels['type_of_mailing'])) {
                                            if ($labels['type_of_mailing'] == 'Certified Mail,Electronic Return Receipt') {
                                                $label_name = 'Cert. ER';
                                                $itemValue = config('constants.QBO.ElectronicReturnReceipt');
                                            } else if ($labels['type_of_mailing'] == 'Certified Mail,Return Receipt Requested') {
                                                $label_name = 'Cert. RR';
                                                $itemValue = config('constants.QBO.ReturnReceiptRequested');
                                            } else if ($labels['type_of_mailing'] == 'Certified Mail') {
                                                $label_name = 'Cert. USPS';
                                                $itemValue = config('constants.QBO.CertifiedMail');
                                            } else {
                                                $label_name = 'Cert. USPS';
                                                $itemValue = config('constants.QBO.CertifiedMail');
                                            }

                                            $description = str_replace(',', ' with ', $labels['type_of_mailing']);
                                            $quickbooks_labels["Line"][] = [
                                                'Amount' => $labels['total_amount'],
                                                'Description' => ($description) ? $description : '',
                                                "DetailType" => "SalesItemLineDetail",
                                                "SalesItemLineDetail" => [
                                                    "ItemRef" => $item_ref[$label_name],
                                                    //"UnitPrice" =>$labels['total_amount'],
                                                    "UnitPrice" => $labels['unit_price'],
                                                    "Qty" => $labels['label_count'],
                                                ]
                                            ];
                                        }
                                        //add manual charges
                                        //add manual charges
                                        if (isset($labels['type_of_label']) && $labels['type_of_label'] == 'manual') {
                                            $description = str_replace(',', ' with ', $labels['type_of_mailing']);
                                            $quickbooks_labels["Line"][] = [
                                                'Amount' => $labels['total_amount'] ?? 0,
                                                'Description' => isset($labels['tracking_number']) ? $labels['tracking_number'] : 'Manual',
                                                "DetailType" => "SalesItemLineDetail",
                                                "SalesItemLineDetail" => [
                                                    "ItemRef" => $item_ref['Manual'],
                                                    "UnitPrice" => $labels['total_amount'],
                                                    "Qty" => 1,

                                                ]
                                            ];
                                        }

                                        //Check if firm mail or next day delivery
                                        if (isset($labels['type_of_label']) && $labels['type_of_label'] != 'stamps'  && $labels['type_of_label'] != 'manual') {
                                            $quickbooks_labels["Line"][] = [
                                                'Amount' => $labels['total_amount'],
                                                'Description' => isset($labels['type_of_label']) ? ucwords($labels['type_of_label']) : '',
                                                "DetailType" => "SalesItemLineDetail",
                                                "SalesItemLineDetail" => [
                                                    "ItemRef" => isset($item_ref[$labels['type_of_label']]) ? $item_ref[$labels['type_of_label']] : "",
                                                    "Qty" => $labels['label_count'],
                                                    "UnitPrice" => $labels['unit_price'],
                                                ]
                                            ];
                                        }
                                        /* update base ammount */
                                        \App\Models\StampsLabel::where(['id' => $labels['id']])->update(['base_amount_charges' => $base_amount]);
                                    }
                                } /* else {
                                    \Log::error('no stamp labels to add');
                                } */

                                $quickbooks_labels["CustomerRef"] = [
                                    "value" => $user->qbo_customer_id
                                ];
                                if (isset($notice_fields['project_address'])) {
                                    $project_addr = explode('**', $notice_fields['project_address']);
                                    //  dd(preg_replace('#<[^>]+>#', ' ', $project_addr[0]));
                                    $project_addr = preg_replace('#<[^>]+>#', ' ', $project_addr[0]);
                                }

                                $quickbooks_labels["CustomField"] = [
                                    [
                                        'DefinitionId' => '1',
                                        'Name' => 'Job Ref No',
                                        'Type' => 'StringType',
                                        'StringValue' => (isset($notice_fields['your_job_reference_no']) ? $notice_fields['your_job_reference_no'] : "") //$val['workorder_id']
                                    ],
                                    [
                                        'DefinitionId' => '2',
                                        'Name' => 'Notice Type',
                                        'Type' => 'StringType',
                                        'StringValue' => (isset($val['notice_name'])) ? substr($val['notice_name'], 0, 30) : ''
                                    ],
                                    [
                                        'DefinitionId' => '3',
                                        'Name' => 'Job Location',
                                        'Type' => 'StringType',
                                        'StringValue' => (isset($notice_fields['project_address'])) ? substr($project_addr, 0, 30) : ''
                                    ],
                                    /*[
                                            'DefinitionId' => '4',
                                            'Name' => 'Work Order Number',
                                            'Type' => 'StringType',
                                            'StringValue' => (isset($val['workorder_id']) ? $val['workorder_id'] : "")//$val['workorder_id']
                                        ],*/
                                ];

                                /* $quickbooks_labels["TxnDate"] = $val['work_order_date'];
                                     $quickbooks_labels["MetaData"] = [
                                        "CreateTime" => $val['work_order_date'],
                                    ];*/

                                $quickbooks_labels["DocNumber"] =  $val['workorder_id'] . '_' . time();


                                $quickbooks_labels["SalesTermRef"] = [
                                    "value" => 2
                                ];

                                /*if(isset($notice_fields['job_start_date'])){
                                        $duedateDays = 40 . 'days';
                                        $due_date = date('Y-m-d', strtotime('+ ' . $duedateDays, strtotime($notice_fields['job_start_date'])));
                                        $quickbooks_labels["DueDate"] = $due_date;
                                    }*/
                            }

                            if (!empty($quickbooks_labels)) {
                                $this->info('invoice generation data set for work order id ' . $work_order_id);
                                try {
                                    $qbo = new \App\Http\Controllers\QuickBooksController();
                                    $qbo_response = $qbo->generateTokens(config('constants.QBO.SCOPE'), 'createInvoice', ['customer_id' => $user->id, 'invoice_details' => $quickbooks_labels]);

                                    $this->info($qbo_response['message']);
                                    if ($qbo_response['response'] == 'success') {
                                        \App\Models\WorkOrder::where(['id' => $work_order_id])->update(['invoice_generated' => 'Yes']);
                                        $this->info('Process completed for work order id ' . $work_order_id);
                                    } else {
                                        \Log::error($qbo_response['message'] . ' for work order id ' . $work_order_id);
                                    }
                                } catch (\Exception $e) {
                                    $this->info('Exception caught from QB for WO ' . $work_order_id);
                                    \Log::error('Exception caught from QB for WO ' . $work_order_id . ' >> ' . $e->getMessage() . ' at line ' . $e->getLine() . ' in file ' . $e->getFile());
                                    \Log::error($e->getTraceAsString());
                                }
                            } else {
                                \Log::error('Could not generate invoice as >> No Quickbook labels data set');
                                //return redirect('customer/statement-invoices')->with('error', 'Could not generate invoice');
                            }
                        }
                    } else {
                        $this->info('Quickbooks customer id not found for user with id ' . $user->id);
                        \Log::error('Quickbooks customer id not found for user with id ' . $user->id);
                    }
                }
            } else {
                $this->info('No completed work orders found');
                \Log::error('No completed work orders found');
            }
        } catch (\Exception $e) {
            \Log::error('last cacth >> ' . $e->getTraceAsString());
        }
    }
}