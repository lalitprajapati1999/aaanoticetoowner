<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\Backpack\PermissionManager::class,
        '\App\Console\Commands\GetQuickbooksTokens',
        '\App\Console\Commands\SendWorkOrderDueDateNotification',
        '\App\Console\Commands\SendCreateYourOwnDueDateNotification',
        '\App\Console\Commands\SendLastDateOnJobNotification',
        '\App\Console\Commands\SendCyoLastDateOnJobNotification',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        
        $schedule->command('GetQuickbooksTokens:getTokens')->cron('0 */1 * * *'); // every 1 hour
        $schedule->command('SendWorkOrderDueDateNotification:sendWorkOrderNotification')->daily();
        $schedule->command('SendCreateYourOwnDueDateNotification:sendCreateYourOwnWorkOrderNotification')->daily();
        $schedule->command('GeneratingQuickbooksInvoice:generatingInvoice')->twiceDaily(1, 13);;
        $schedule->command('GeneratingCyoQuickbooksInvoice:generatingCyoInvoice')->twiceDaily(1, 13);
        $schedule->command('CheckCustomerSubscription:changedSubscriptionStatus')->daily();
        $schedule->command('SendlastDateOnJobNotification:sendWorkOrderNotification')->daily();
        $schedule->command('SendCyoLastDateOnJobNotification:sendWorkOrderNotification')->daily();
    }


    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
