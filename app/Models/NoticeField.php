<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;
class NoticeField extends Model
{
	use SoftDeletes;
	protected $dates = ['deleted_at'];
	protected $fillable = ['state_id', 'notice_id', 'name', 'type', 'validation', 'is_required', 'sort_order','section','status'];

	/**
	 * Get the state that owns the contact.
	 */
	public function state()
	{
		return $this->belongsTo(State::class, 'state_id', 'id');
	}

	/**
	 * Get the customer that owns the contact.
	 */
	public function notice()
	{
		return $this->belongsTo(Notice::class, 'notice_id', 'id');
	}

    /**
     * Get the Notice Type.
     *
     * @return string
     */
    public function getDisplayTypeAttribute()
    {
        return config('constants.field_types')[$this->type];
    }
   	public function noticeField()
	{
		return $this->belongsTo(WorkOrderFields::class, 'id', 'notice_field_id')->withDefault();
	}
	public function newQuery()
	{
		//return parent::newQuery()->where('status','=','1');
		$user_id = Auth::user()->id;
		$user = \App\Models\RoleUsers::where('user_id',$user_id)->select('role_id')->get();
		if($user[0]->role_id != 1){
	    	return parent::newQuery()->whereStatus('1');
		}else{
			return parent::newQuery();
		}
	}
}
