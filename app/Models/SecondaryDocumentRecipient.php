<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SecondaryDocumentRecipient extends Model
{
	protected $fillable = ['secondary_document_id', 'category_id', 'name', 'contact', 'mobile', 'address', 'city_id', 'state_id', 'zip', 'fax','email','country_id'];

	/**
	 * Get the secondaryDocument that owns the Recipient.
	 */
	public function secondaryDocument()
	{
		return $this->belongsTo(SecondaryDocument::class, 'secondary_document_id', 'id');
	}

	/**
	 * Get the Category that owns the Recipient.
	 */
	public function category()
	{
		return $this->belongsTo(Category::class, 'category_id', 'id')->withDefault();
	}

	/**
	 * Get the city that owns the Recipient.
	 */
	public function city()
	{
		return $this->belongsTo(City::class, 'city_id', 'id')->withDefault();
	}

	/**
	 * Get the state that owns the Recipient.
	 */
	public function state()
	{
		return $this->belongsTo(State::class, 'state_id', 'id')->withDefault();
	}

	/**
	 * Get usps details
	 */
	public function secondaryDocUspsAddress()
	{
		return $this->belongsTo(SecondaryDocumentUspsAddress::class, 'id', 'recipient_id')->withDefault();
	}
}

