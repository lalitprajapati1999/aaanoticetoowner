<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NoticeTemplate extends Model
{
	protected $fillable = ['state_id', 'notice_id', 'name', 'no_of_days','content','is_parent_work_order'];

	/**
	 * Get the state that owns the contact.
	 */
	public function state()
	{
		return $this->belongsTo(State::class, 'state_id', 'id');
	}

	/**
	 * Get the customer that owns the contact.
	 */
	public function notice()
	{
		return $this->belongsTo(Notice::class, 'notice_id', 'id');
	}
}
