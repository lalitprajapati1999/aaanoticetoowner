<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cyo_WorkOrders extends Model
{
    protected $fillable = ['parent_id', 'customer_id', 'user_id', 'notice_id','order_no', 'status','document_type','account_manager_id','is_rescind', 'cancelled_at', 'is_billable'];

	/**
	 * Get the work order that owns the other work order.
	 */
	public function parent()
	{
		return $this->belongsTo(self::class, 'parent_id', 'id')->withDefault();
	}

	/**
	 * Get the customer that owns the work order.
	 */
	public function customer()
	{
		return $this->belongsTo(Customer::class, 'customer_id', 'id')->withDefault();
	}

	/**
	 * Get the account manager that owns the work order.
	 */
	public function user()
	{
		return $this->belongsTo(User::class, 'user_id', 'id')->withDefault();
	}

	/**
	 * Get the notice that owns the work order.
	 */
	public function notice()
	{
		return $this->belongsTo(Notice::class, 'notice_id', 'id')->withDefault();
	}

	/**
	 * Get the city that owns the work order.
	 */
	public function city()
	{
		return $this->belongsTo(City::class, 'city_id', 'id')->withDefault();
	}

	/**
	 * Get the state that owns the work order.
	 */
	public function state()
	{
		return $this->belongsTo(State::class, 'state_id', 'id')->withDefault();
	}
    public function woFields()
	{
		return $this->hasMany(Cyo_WorkOrder_fields::class, 'workorder_id', 'id');
	}
		/*public function noticeField()
	{
		return $this->belongsTo(WorkOrderFields::class, 'workorder_id', 'id')->withDefault();
	}
*/
}
