<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SecondaryDocumentUspsAddress extends Model
{
    //
    protected $table = 'secondary_document_usps_addresses';
}
