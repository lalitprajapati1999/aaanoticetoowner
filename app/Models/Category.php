<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	protected $fillable = ['parent_id', 'name'];

	/**
	 * Get the work order that owns the other work order.
	 */
	public function parent()
	{
		return $this->belongsTo(self::class, 'parent_id', 'id')->withDefault();
	}
}
