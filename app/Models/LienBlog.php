<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LienBlog extends Model
{


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'question', 'answer', 'status'
    ];
     protected $table = 'notice_lienblog';
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    

}
