<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cyo_WorkOrder_fields extends Model
{
    //
    //
    protected $fillable = ['workorder_id', 'notice_id','notice_field_id','value'];

    public function workOrder()
	{
		return $this->belongsTo(WorkOrder::class, 'workorder_id', 'id')->withDefault();
	}

	/**
	 * Get the notice field that owns the field data.
	 */
	public function noticeField()
	{
		return $this->belongsTo(NoticeField::class, 'notice_field_id', 'id')->where('notice_fields.name','project_address')->withDefault();
	}
}
