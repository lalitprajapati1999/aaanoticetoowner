<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OAuth2InvoiceResponse extends Model
{
	protected $table = 'oauth2_invoice_response';
	protected $fiilable = ['invoice_id',
							'bill_addr',
							'sales_term_ref',
							'tranx_date',
							'due_date',
							'invoice_status',
							'job_ref_no',
							'notice_type',
							'job_location',
							'total_amt',
							'balance',
							'doc_number',
							'other_details'							
						  ];

}
