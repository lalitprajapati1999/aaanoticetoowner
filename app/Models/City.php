<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model {

    //
    public $timestamps = false;
    // public $table ='zipcodecity';
    protected $fillable = ['id', 'name', 'county', 'state_id', 'zip_code','company_address','city_id'];

    /**
     * Get the State .
     */
    public function states() {
        return $this->belongsTo(State::class, 'state_id', 'id');
    }

}
