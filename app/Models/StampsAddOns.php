<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StampsAddOns extends Model
{
    protected $table = 'stamps_add_ons';
    public $timestamps = false;
}
