<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WorkOrderData extends Model
{
	protected $fillable = ['work_order_id', 'notice_field_id', 'value'];

	/**
	 * Get the work order that owns the field data.
	 */
	public function workOrder()
	{
		return $this->belongsTo(WorkOrder::class, 'work_order_id', 'id')->withDefault();
	}

	/**
	 * Get the notice field that owns the field data.
	 */
	public function noticeField()
	{
		return $this->belongsTo(NoticeField::class, 'notice_field_id', 'id')->withDefault();
	}
}
