<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserCard extends Model
{
    protected $table = 'user_card';
    protected $fillable = ['id','user_id','card_id'];
}
