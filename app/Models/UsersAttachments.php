<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsersAttachments extends Model
{
	protected $fillable = ['user_id','file_name','original_file_name'];

	
}
