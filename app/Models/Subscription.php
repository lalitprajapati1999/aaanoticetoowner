<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
	protected $fillable = ['name', 'type', 'duration_type', 'charge', 'description', 'status'];

	/**
	 * Get the customer that owns the contact.
	 */
	public function notices()
	{
		return $this->belongsToMany(Notice::class, 'subscription_notices', 'subscription_id', 'notice_id')->withPivot('limit', 'charge', 'post_subscription_charge');
	}
}
