<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactUs extends Model
{
	protected $fillable = ['name', 'email','phone', 'query','customer_id','company_name','phone'];
        
        protected  $table = "contact_us";

}
