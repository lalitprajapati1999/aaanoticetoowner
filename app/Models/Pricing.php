<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pricing extends Model
{
    //protected $table = 'pricing';
    
    
    public function notice() {
        return $this->hasOne(Notice::class, 'id', 'notice_id');
    }
    
    public function package() {
        return $this->hasOne(Package::class, 'id', 'package_id');
    }
}
