<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StampsAddOnsRequired extends Model
{
    protected $table = 'stamps_add_ons_required';
    public $timestamps = false;
}
