<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SecondaryDocumentFields extends Model
{
     protected $fillable = ['secondary_document_id', 'notice_id','notice_field_id','value'];


}
