<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notice extends Model {

    protected $fillable = ['name', 'type', 'allow_cyo', 'status', 'allow_deadline_calculator', 'state_id', 'notice_sequence', 'is_claim_of_lien', 'owners_html_in_pdf', 'is_bond_of_claim','is_rescind','master_notice_id'];

    /**
     * Get the customer that owns the contact.
     */
    public function noticeFields() {
        return $this->hasMany(NoticeField::class, 'notice_id', 'id');
    }

    /**
     * Get the Notice Type.
     *
     * @return string
     */
    public function getDisplayTypeAttribute() {
        return config('constants.notice_type')[$this->type];
    }

    /**
     * Get the Notice Status.
     *
     * @return string
     */
    public function getDisplayStatusAttribute() {
        return config('constants.notice_status')[$this->status];
    }

    public function pricing() {
        return $this->hasMany(Pricing::class, 'notice_id', 'id');
    }

    public function customerpackage() {
        return $this->hasMany(Customer_package::class, 'notice_id', 'id');
    }

    /**
     * Get the state that owns the contact.
     */
    public function state() {
        return $this->belongsTo(State::class, 'state_id', 'id')->withDefault();
    }

}
