<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerAgent extends Model
{
	protected $fillable = ['customer_id', 'title', 'first_name', 'last_name','updated_at', 'created_at'];

	/**
	 * Get the customer that owns the Agent.
	 */
	public function customer()
	{
		return $this->belongsTo(Customer::class, 'customer_id', 'id');
	}
}
