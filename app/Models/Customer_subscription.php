<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer_subscription extends Model
{
    use SoftDeletes;
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    protected $fillable = ['customer_id', 'package_id', 'charges', 'type', 'start_date', 'end_date', 'cheque_no', 'bank_details', 'status', 'created_at', 'updated_at', 'deleted_at'];
    
    
    public function package() {
        return $this->hasOne(Package::class, 'id', 'package_id');
    }
    
    public function customer() {
        return $this->hasOne(Customer::class, 'id', 'customer_id');
    }
}
