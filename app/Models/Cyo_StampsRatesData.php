<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cyo_StampsRatesData extends Model
{
    protected $table='cyo_stamps_rates_data';
    public $timestamps = false;
    protected $fillable = ['rates_data','label_error'];
}
