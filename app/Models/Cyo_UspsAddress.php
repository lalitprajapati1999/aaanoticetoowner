<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cyo_UspsAddress extends Model
{
    //
      /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cyo__usps_addresses';
}
