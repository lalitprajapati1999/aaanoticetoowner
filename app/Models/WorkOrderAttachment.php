<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WorkOrderAttachment extends Model
{
	protected $fillable = ['work_order_id', 'user_id', 'type', 'title', 'file_name','original_file_name','visibility'];

	/**
	 * Get the work order that owns the work order attachment.
	 */
	public function workOrder()
	{
		return $this->belongsTo(WorkOrder::class, 'work_order_id', 'id')->withDefault();
	}

	/**
	 * Get the user that uploaded work order attachment.
	 */
	public function user()
	{
		return $this->belongsTo(User::class, 'user_id', 'id')->withDefault();
	}
}
