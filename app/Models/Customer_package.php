<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer_package extends Model {

    public function notice() {
        return $this->hasOne(Notice::class, 'id', 'notice_id');
    }
    
    public function package() {
        return $this->hasOne(Package::class, 'id', 'package_id');
    }

}
