<?php

namespace App\Models;

class Role extends \Backpack\PermissionManager\app\Models\Role
{
	public function scopeAdmin($query)
    {
        return $query->whereIn('name', ['admin', 'account-manager']);
    }

	public function scopeAccountManager($query)
    {
        return $query->whereIn('name', ['account-manager']);
    }

	public function scopeAdminAccountManager($query)
    {
        return $query->whereIn('name', ['admin', 'account-manager']);
    }
}
