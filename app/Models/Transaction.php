<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
	protected $table = 'transactions';
	public $timestamps = false;
	protected $fillable = ['transaction_id','auth_code','amount','card_id','transaction_status','user_id'];
}
