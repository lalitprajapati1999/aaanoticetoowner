<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactDetail extends Model
{
	protected $fillable = ['contact_id', 'type', 'name', 'email', 'mobile', 'fax','designation'];

	/**
	 * Get the contact that owns the details.
	 */
	public function contact()
	{
		return $this->belongsTo(Contact::class, 'contact_id', 'id');
	}

}
