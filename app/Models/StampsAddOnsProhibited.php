<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StampsAddOnsProhibited extends Model
{
    protected $table = 'stamps_add_ons_prohibited';
    public $timestamps = false;
}
