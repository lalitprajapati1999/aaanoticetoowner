<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
	protected $fillable = ['qbo_customer_id','company_name', 'contact_person', 'mailing_address', 'mailing_city_id', 'mailing_state_id', 'mailing_zip', 'physical_address', 'physical_city_id', 'physical_state_id', 'physical_zip', 'hear_about', 'office_number', 'fax_number', 'mobile_number', 'company_email','user_id','no_of_offices','account_manager_id'];

	/**
	 * Get the user that owns the customer.
	 */
	public function user()
	{
		return $this->belongsTo(User::class, 'user_id', 'id');
	}

	/**
	 * Get the mailing city that owns the customer.
	 */
	public function mailingCity()
	{
		return $this->belongsTo(City::class, 'mailing_city_id', 'id')->withDefault();
	}

	/**
	 * Get the mailing state that owns the customer.
	 */
	public function mailingState()
	{
		return $this->belongsTo(State::class, 'mailing_state_id', 'id')->withDefault();
	}

	/**
	 * Get the physical city that owns the customer.
	 */
	public function physicalCity()
	{
		return $this->belongsTo(City::class, 'physical_city_id', 'id')->withDefault();
	}

	/**
	 * Get the physical state that owns the customer.
	 */
	public function physicalState()
	{
		return $this->belongsTo(State::class, 'physical_state_id', 'id')->withDefault();
	}

	/**
	 * Get the customer has many branches.
	 */
	public function branches()
	{
		return $this->hasMany(Branch::class, 'customer_id', 'id');
	}

	/**
	 * Get the customer has many agents.
	 */
	public function agents()
	{
		return $this->hasMany(CustomerAgent::class, 'customer_id', 'id');
	}

	/**
	 * Get the customer has many representatives.
	 */
	public function representatives()
	{
		return $this->hasMany(CustomerRepresentative::class, 'customer_id', 'id');
	}
        
        public function customersubscription() {
            return $this->hasMany(Customer_subscription::class, 'customer_id', 'id');
        }

}
