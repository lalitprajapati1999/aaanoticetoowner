<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WorkOrderCorrections extends Model
{
	protected $fillable = [ 'work_order_id', 'customer_id', 'correction', 'email', 'visibility','user_id'];

	/**
	 * Get the work order that owns the work order note.
	 */
	public function workorder()
	{
		return $this->belongsTo(WorkOrder::class, 'work_order_id', 'id')->withDefault();
	}

	/**
	 * Get the user that added work order note.
	 */
	public function customer()
	{
		return $this->belongsTo(Customer::class, 'customer_id', 'id')->withDefault();
	}
}
