<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SecondaryDocument extends Model
{
	protected $fillable = ['date', 'customer', 'work_order_no', 'address', 'amount', 'due_date', 'first_name', 'signature', 'comment','customer_id','type','file_name','country_id','project_name','amount_in_text',
                            'legal_description','service_labour_furnished','status'];

	/**
	 * Get the secondaryDocument that owns the Recipients.
	 */
	public function recepients()
	{
		return $this->hasMany(SecondaryDocumentRecipient::class, 'secondary_document_id', 'id');
	}
        
        public static function getAdminEmail(){
             $admin_email = \App\User::leftjoin('role_users', 'role_users.user_id', '=', 'users.id')
                ->where('role_users.role_id', '=', 2)
                ->select('users.email')
                ->first();
             return $admin_email;
        }
        public static function getCustomerEmail($customer_id){
               $customer_email = \App\Models\Customer::leftjoin('users', 'users.id', '=', 'customers.user_id')
                                                              ->select('users.email')
                                                              ->find($customer_id);
             return $customer_email;
        }
        public static function getAccountManagerEmail($customer_id){
             $account_manager_email = \App\Models\Customer::leftjoin('users', 'users.id', '=', 'customers.account_manager_id')
                                                              ->select('users.email')
                                                              ->find($customer_id);
             return $account_manager_email;
        }
        public static function getAllAccountManagerEmail(){
             $account_manager_email = \App\Models\User::leftjoin('role_users', 'role_users.user_id', '=', 'users.id')
                                                              ->select('users.email')
                                                              ->where('role_users.role_id','=',3)
                                                              ->get();
             return $account_manager_email;
        }
        public static function getOtherAccManagerEmailExcludingLoggedIn($user_id){
            $account_manager_email = \App\Models\User::leftjoin('role_users', 'role_users.user_id', '=', 'users.id')
                                                              ->select('users.email')
                                                              ->where('users.id','!=',$user_id)
                                                              ->where('role_users.role_id','=',3)
                                                              ->get();
             return $account_manager_email;
        }
}
