<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cyo_StampsLabel extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cyo__stamps_labels';
    
        protected $fillable = ['add_ons','service_type','package_type','type_of_mailing','type_of_label','generated_label','rates_data_id','tracking_number','StampsTxID','URL','ShipDate','DeliveryDate','Amount','recipient_id'];
}
