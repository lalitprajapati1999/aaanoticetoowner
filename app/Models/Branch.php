<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
	protected $fillable = ['customer_id', 'name', 'contact_person', 'phone', 'email', 'title', 'first_name', 'last_name', 'address', 'city_id', 'state_id', 'zip','country'];

	/**
	 * Get the customer that owns the contact.
	 */
	public function customer()
	{
		return $this->belongsTo(Customer::class, 'customer_id', 'id');
	}

	/**
	 * Get the city that owns the contact.
	 */
	public function city()
	{
		return $this->belongsTo(City::class, 'city_id', 'id')->withDefault();
	}

	/**
	 * Get the state that owns the contact.
	 */
	public function state()
	{
		return $this->belongsTo(State::class, 'state_id', 'id')->withDefault();
	}
}
