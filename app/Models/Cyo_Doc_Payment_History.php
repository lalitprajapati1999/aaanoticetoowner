<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

class Cyo_Doc_Payment_History extends Model
{    
    public $table = 'cyo_doc_payment_historys';
    protected $fillable = ['work_order_id', 'customer_id', 'package_id', 'charges', 'type', 'status', 'start_date', 'end_date'];
}