<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerRepresentative extends Model
{
	protected $fillable = ['customer_id', 'contact_person', 'branch_name', 'email','mobile_number'];

	/**
	 * Get the customer that owns the Representative.
	 */
	public function customer()
	{
		return $this->belongsTo(Customer::class, 'customer_id', 'id');
	}
}
