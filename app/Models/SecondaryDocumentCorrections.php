<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SecondaryDocumentCorrections extends Model
{
	protected $fillable = [ 'secondary_document_id', 'customer_id', 'correction', 'email', 'visibility','user_id'];

	/**
	 * Get the work order that owns the work order note.
	 */
	public function SecondaryDocument()
	{
		return $this->belongsTo(SecondaryDocument::class, 'secondary_document_id', 'id')->withDefault();
	}

	/**
	 * Get the user that added work order note.
	 */
	public function customer()
	{
		return $this->belongsTo(Customer::class, 'customer_id', 'id')->withDefault();
	}
}
