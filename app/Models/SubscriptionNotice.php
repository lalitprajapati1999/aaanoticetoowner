<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubscriptionNotice extends Model
{
	protected $fillable = ['subscription_id', 'notice_id', 'limit', 'charge', 'post_subscription_charge'];
}
