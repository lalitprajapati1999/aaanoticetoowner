<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StampsServicePackageAddon extends Model
{
    protected $table = 'stamps_service_package_addon';
    public $timestamps = false;
}
