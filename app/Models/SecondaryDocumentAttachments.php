<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SecondaryDocumentAttachments extends Model
{
	protected $fillable = ['secondary_document_id', 'type', 'title', 'file_name','original_file_name','visibility'];

	/**
	 * Get the work order that owns the work order attachment.
	 */
//	public function secondaryDocument()
//	{
//		return $this->belongsTo(SecondaryDocument::class, 'secondary_document_id', 'id')->withDefault();
//	}

	
}
