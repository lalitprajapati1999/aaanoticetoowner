<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuickbooksToken extends Model
{
    protected $table = 'qbo_oauth';

    protected $fillable = ['scope','access_token', 'refresh_token', 'company_id', 'customer_id','token_expires_at','refresh_token_expires_at'];
}
