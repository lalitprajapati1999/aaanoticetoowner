<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StampsServices extends Model
{
    protected $table = 'stamps_service_type';
    public $timestamps = false;
}
