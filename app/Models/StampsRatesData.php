<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StampsRatesData extends Model
{
    protected $table='stamps_rates_data';
    public $timestamps = false;
    protected $fillable = ['rates_data','label_error'];
}
