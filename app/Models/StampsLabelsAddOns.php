<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StampsLabelsAddOns extends Model
{
    protected $table = 'stamps_labels_add_ons';
    public $timestamps = false;
}
