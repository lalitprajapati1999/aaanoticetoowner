<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cyo_WorkOrder_Recipient extends Model
{
    //
     protected $fillable = ['work_order_id', 'category_id', 'name', 'contact', 'address', 'city_id', 'state_id', 'zip', 'fax','email','mobile','attn','parent_work_order'];

	/**
	 * Get the workOrder that owns the Recipient.
	 */
	public function workOrder()
	{
		return $this->belongsTo(WorkOrder::class, 'work_order_id', 'id');
	}

	/**
	 * Get the Category that owns the Recipient.
	 */
	public function category()
	{
		return $this->belongsTo(Category::class, 'category_id', 'id');
	}

	/**
	 * Get the city that owns the Recipient.
	 */
	public function city()
	{
		return $this->belongsTo(City::class, 'city_id', 'id')->withDefault();
	}

	/**
	 * Get the state that owns the Recipient.
	 */
	public function state()
	{
		return $this->belongsTo(State::class, 'state_id', 'id')->withDefault();
	}

	/**
	 * Get usps details
	 */
	public function CyoUspsAddress()
	{
		return $this->belongsTo(Cyo_UspsAddress::class, 'id', 'recipient_id')->withDefault();
	}
}
