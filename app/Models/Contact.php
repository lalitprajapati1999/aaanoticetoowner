<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = ['customer_id','company_name', 'company_address', 'mailing_address', 'city_id', 'state_id', 'zip', 'phone','email','country_id','attn','added_form'];

	/**
	 * Get the customer that owns the contact.
	 */
	public function customer()
	{
		return $this->belongsTo(Customer::class, 'customer_id', 'id');
	}

	/**
	 * Get the city that owns the contact.
	 */
	public function city()
	{
		return $this->belongsTo(City::class, 'city_id', 'id')->withDefault();
	}

	/**
	 * Get the state that owns the contact.
	 */
	public function state()
	{
		return $this->belongsTo(State::class, 'state_id', 'id')->withDefault();
	}

	/**
	 * Get the contact have many details.
	 */
	public function contactDetails()
	{
		return $this->hasMany(ContactDetail::class, 'contact_id', 'id');
	}

    /**
     * Get the Contact Persons.
     *
     * @return string
     */
    public function contactPersons()
    {
        return $this->contactDetails()->where('type', array_flip(config('constants.contact_detail_type'))['contact_person']);
    }

    /**
     * Get the Sales Persons.
     *
     * @return array collection
     */
    public function salesPersons()
    {
        return $this->contactDetails()->where('type', array_flip(config('constants.contact_detail_type'))['sales_person']);
    }
    
}
