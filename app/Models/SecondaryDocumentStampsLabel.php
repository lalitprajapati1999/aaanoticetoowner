<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SecondaryDocumentStampsLabel extends Model
{
    //
     protected $table = 'secondary_document_stamps_labels';
}
