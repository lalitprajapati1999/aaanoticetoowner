<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','content', 'client_name', 'status','image'
    ];
     protected $table = 'notice_testimonial';
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    

}
