<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    //protected $table = 'package';
    
    public function pricing() {
        return $this->hasMany(Pricing::class, 'package_id', 'id');
    }
    
    public function customerpackage() {
        return $this->hasMany(Customer_package::class, 'package_id', 'id');
    }
    
    public function customersubscription() {
        return $this->hasMany(Customer_subscription::class, 'package_id', 'id');
    }
}
