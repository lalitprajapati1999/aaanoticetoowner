<?php

namespace App\Traits;

trait UserRelationShip
{
	public static function updatePermissions()
	{
		// Insert Roles and Permissions
        $role_permissions = config('backpack.permissionmanager.data.permissions');
        $default_user = config('backpack.permissionmanager.data.default_user');
        \DB::beginTransaction();
            try {

                foreach ($role_permissions as $role => $permissions) {
                // Find Or create Role
                $roleModel = config('laravel-permission.models.role')::firstOrNew(['name' => $role]);
                $roleModel->save();

                // Create default user
                if (isset($default_user[$role])) {
                    $userInfo = $default_user[$role];
                    $user = self::firstOrNew(['email' => $userInfo['email']]);
                    $user->name = empty($user->name)?$userInfo['name']:$user->name;
                    $user->password = empty($user->password)?bcrypt($userInfo['password']):$user->password;
                    $user->save();

                    if (!$user->hasRole($role))
                        $user->assignRole($role);
                }

                $rolePermission = [];
                foreach ($permissions as $permission) {
                    $permissionModel = config('laravel-permission.models.permission')::firstOrNew(['name' => $permission]);
                    $permissionModel->save();

                    $rolePermission[] = $permissionModel->id;
                }
                // Sync Role Permission
                $roleModel->permissions()->sync($rolePermission);
            }

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollBack();
            logger($e->getMessage());
            throw new \Exception("ACL tables could not be created.", 1);
        }
	}

    /**
     * Get the user that have one customer.
     */
    public function customer()
    {
        return $this->hasOne(\App\Models\Customer::class, 'user_id', 'id')->withDefault();
    }
}