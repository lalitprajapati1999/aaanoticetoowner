<?php

namespace App\Http\Middleware\Backpack;

use Closure;

class PermissionManagerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (
            ($request->is(config('backpack.base.route_prefix', 'admin').'/role*') && !$request->user()->can('acl-management-roles')) || 
            ($request->is(config('backpack.base.route_prefix', 'admin').'/user*') && !$request->user()->can('acl-management-users')) || 
            ($request->is(config('backpack.base.route_prefix', 'admin').'/permission*') && !$request->user()->can('acl-management-permissions'))
        ) {
            abort(403);
        }

        return $next($request);
    }
}
