<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            $user = \Auth::user();

            if($user->can('admin-dashboard'))
                return redirect(config('backpack.base.route_prefix', 'admin') . '/dashboard');
            else
                return redirect('/home');
        }

        return $next($request);
    }
}
