<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Requests\Validators;

use Illuminate\Contracts\Validation\Factory;

/**
 * Description of CustomValidator
 *
 * @author neosoft
 */
class CustomValidator {

    //put your code here

    private $validationFactory;
    public $errorMessage;

    public function __construct(Factory $factory) {
        $this->validationFactory = $factory;
        $this->errorMessage = "Something went wrong, Please try again";
    }

    public function checkDuplicateEntry($key) {
        $this->validationFactory->extend('validate_duplicate_entry', function ($attribute, $value, $parameters, $validator) use($key) {
            $noticeIds = $validator->getData()['noticechange'];
            $resultcount = array_keys($noticeIds, $value);
            if(count($resultcount) > 1) {
                return false;
            } else {
                return true;
            }
        }, "There found a duplicate entry"
        );
        return $this;
    }

}
