<?php

namespace App\Http\Requests;

use App\Http\Requests\Validators\CustomValidator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class CustomerStorePricingPost extends FormRequest {

    public function __construct(array $query = array(),
            array $request = array(),
            array $attributes = array(),
            array $cookies = array(),
            array $files = array(),
            array $server = array(),
            $content = null,
            CustomValidator $customValidator) {
        parent::__construct($query, $request, $attributes, $cookies, $files, $server, $content);
        $customValidator->checkDuplicateEntry('noticechange');
    }

        /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $rules = [
            'package_type' => 'required|integer',
            //'state' => 'required',
            'noticechange.*' => 'required|integer|min:1|validate_duplicate_entry',
            'noticecharges.*' => 'required|integer|min:1',
            'post_sub_description.*' => 'required',
            'id' => 'required|integer|min:1',
        ];

        if ($this->request->get('package_type') == 1) {
            foreach ($this->request->get('notice') as $key => $val) {
                $rules['notice.' . $key] = 'required';
                $rules['low_limit.' . $key] = 'required|integer|min:1';
                $rules['high_limit.' . $key] = 'required|integer|min:1';
                $rules['charges.' . $key] = 'required|integer|min:1';
                $rules['nto_cancellation_charge.' . $key] = 'integer|min:0';
            }
        }
        if ($this->request->get('package_type') == 2 || $this->request->get('package_type') == 3) {
            $rules['create_own_charge'] = 'required|integer|min:1';
            $rules['description'] = 'required';
            $rules['cancellation_charge'] = 'integer|min:0';
        }

        return $rules;
    }

    public function validate() {
        $instance = $this->getValidatorInstance();
        if ($instance->fails()) {
            throw new HttpResponseException(response()->json(['status' => FALSE, 'error' => $instance->errors()], 404));
        }
    }

    public function messages() {
        $messages = [];
        if (count($this->request->get('low_limit')) > 0) {
            foreach ($this->request->get('low_limit') as $key => $val) {
                $messages['low_limit.' . $key . ".required"] = 'The field is required.';
            }
        }
        if (count($this->request->get('high_limit')) > 0) {
            foreach ($this->request->get('high_limit') as $key => $val) {
                $messages['high_limit.' . $key . ".required"] = 'The field is required.';
            }
        }
        if (count($this->request->get('charges')) > 0) {
            foreach ($this->request->get('charges') as $key => $val) {
                $messages['charges.' . $key . ".required"] = 'The field is required.';
            }
        }
        if (count($this->request->get('post_sub_description')) > 0) {
            foreach ($this->request->get('post_sub_description') as $key => $val) {
                $messages['post_sub_description.' . $key . ".required"] = 'The field is required.';
            }
        }
        if (count($this->request->get('noticechange')) > 0) {
            foreach ($this->request->get('noticechange') as $key => $val) {
                $messages['noticechange.' . $key . ".validate_duplicate_entry"] = 'Found a duplicate entry';
            }
        }
        return $messages;
    }

}
