<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveCreditcardPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'pname'=>'required',
            'cctype'=>'required',
            'cnumber'=>'required|integer',
            'expirymonth'=>'required|integer',
            'expiryyear'=>'required|integer',
            'securitycode'=>'required|integer',
            'zipcode'=>'required|integer',
        ];
    }
    
    public function messages() {
        $messages = [];
        
        return $messages;
    }
}
