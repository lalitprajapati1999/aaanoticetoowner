<?php

namespace App\Http\Controllers\AccountManager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Auth;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;
use App\Models\WorkOrder;
use PDF;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;
use LynX39\LaraPdfMerger\Facades\PdfMerger;
use Carbon\Carbon;
class MailingController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($flag) {
        //dd(WorkOrderFields'index');
        if (Session::has('is_mailing')) {
            Session::forget('is_mailing');
            setcookie("select_all_mail", "");
            setcookie("mailing_checkbox", "");
            

            return view('account_manager.mailing.list')->with('flag',$flag);
        } else {
          // $flag = 0;
           // return redirect('admin/dashboard');
            return view('account_manager.mailing.list')->with('flag',$flag);

        }
    }

    /**
     * datatables list workorders
     */
    public function get_workorders(Request $request) {
        $not_mailed_out_master_ids= [config('constants.MASTER_NOTICE')['SBC']['ID'],config('constants.MASTER_NOTICE')['WRL']['ID']];
        if(Auth::user()->hasRole('admin')){
            $WorkOrderFields = \App\Models\WorkOrderFields::select('notices.master_notice_id as master_notice_id','notices.name as notice_name', 'work_orders.parent_id','work_orders.file_name','work_orders.is_rescind as rescind_work_order', 'work_orders.created_at', 'work_orders.user_id', 'work_orders.notice_id as notice_id', 'work_orders.status', 'work_orders.print_firm_mail', 'work_orders.user_id', 'work_orders.account_manager_id', 'work_order_fields.workorder_id', 'work_order_fields.notice_id', DB::raw("(GROUP_CONCAT(notice_fields.name SEPARATOR '||')) as `field_names`"), DB::raw("(GROUP_CONCAT(value SEPARATOR '||')) as `field_values`"))
                ->rightjoin('notice_fields', 'notice_fields.id', '=', 'work_order_fields.notice_field_id')
                ->join('work_orders', 'work_orders.id', '=', 'work_order_fields.workorder_id')
                ->leftjoin('notices', 'notices.id', '=', 'work_orders.notice_id')
                ->groupBy('work_order_fields.workorder_id', 'work_order_fields.notice_id') ;
                /* $WorkOrderFields = \App\Models\WorkOrderFields::select('notices.master_notice_id as master_notice_id','notices.name as notice_name', 'notices.type', 'notices.is_rescind as rescind_notice', 'work_orders.parent_id', 'work_orders.created_at', 'work_orders.is_rescind as rescind_work_order', 'work_orders.file_name', 'customers.company_name','users.name as customer_name', 'work_orders.user_id', 'work_orders.notice_id as notice_id', 'work_orders.status', 'usr.name as account_manager_name', 'work_orders.user_id', 'work_orders.account_manager_id', 'work_order_fields.workorder_id', 'work_order_fields.notice_id','users.name as user_name', DB::raw("(GROUP_CONCAT(notice_fields.name SEPARATOR '||')) as `field_names`"), DB::raw("(GROUP_CONCAT(value SEPARATOR '||')) as `field_values`"))
                    ->rightjoin('notice_fields', 'notice_fields.id', '=', 'work_order_fields.notice_field_id')
                    ->join('work_orders', 'work_orders.id', '=', 'work_order_fields.workorder_id')
                    ->leftjoin('notices', 'notices.id', '=', 'work_orders.notice_id')
                    ->leftjoin('users as usr', 'usr.id', '=', 'work_orders.account_manager_id')
                    ->leftjoin('users', 'users.id', '=', 'work_orders.user_id')
                    ->leftjoin('customers', 'customers.id', '=', 'work_orders.customer_id')
                    ->groupBy('work_order_fields.workorder_id', 'work_order_fields.notice_id');*/
            }else{
                $WorkOrderFields = \App\Models\WorkOrderFields::select('notices.master_notice_id as master_notice_id','notices.name as notice_name', 'work_orders.parent_id','work_orders.file_name','work_orders.is_rescind as rescind_work_order', 'work_orders.created_at', 'work_orders.user_id', 'work_orders.notice_id as notice_id', 'work_orders.status', 'work_orders.print_firm_mail', 'work_orders.user_id', 'work_orders.account_manager_id', 'work_order_fields.workorder_id', 'work_order_fields.notice_id', DB::raw("(GROUP_CONCAT(notice_fields.name SEPARATOR '||')) as `field_names`"), DB::raw("(GROUP_CONCAT(value SEPARATOR '||')) as `field_values`"))
                        ->rightjoin('notice_fields', 'notice_fields.id', '=', 'work_order_fields.notice_field_id')
                        ->join('work_orders', 'work_orders.id', '=', 'work_order_fields.workorder_id')
                        ->leftjoin('notices', 'notices.id', '=', 'work_orders.notice_id')
                        ->groupBy('work_order_fields.workorder_id', 'work_order_fields.notice_id')
                        ->where('work_orders.account_manager_id', Auth::user()->id);
            }
        // ->where('account_manager_id', Auth::user()->id)



        if (($request->has('from_date')) && ($request->get('from_date') != NULL)) {
            $WorkOrderFields->whereDate('work_orders.created_at', '>=', date('Y-m-d', strtotime($request->get('from_date'))));
        }
        if (($request->has('to_date')) && ($request->get('to_date') != NULL)) {
            $WorkOrderFields->whereDate('work_orders.created_at', '<=', date('Y-m-d', strtotime($request->get('to_date'))));
        }
        if (($request->has('work_order_no')) && ($request->get('work_order_no') != NULL)) {
            
            $WorkOrderFields->where('work_order_fields.workorder_id', 'like', '%' . $request->get("work_order_no") . '%');
                /* search for workorder */
           // $WorkOrderFields->whereIN('work_orders.status', [5, 6]);
        }
        
        if(($request->has('from_date') && $request->get('from_date') != NULL) || ($request->has('to_date') && $request->get('to_date') != NULL) || ($request->has('work_order_no')&&$request->get('work_order_no') != NULL)){
            $WorkOrderFields->whereIN('work_orders.status', [5, 6]);
        }else{
           if ($request->Completedwolist == 1) {
                $WorkOrderFields->whereIN('work_orders.status', [6]);
                $date = Carbon::now()->subDays(5);
                $WorkOrderFields->where('work_orders.updated_at', '>=', $date);
            }else{
                $WorkOrderFields->whereIN('work_orders.status', [5]);
            }
        }
        $WorkOrderFields->whereNotIn('notices.master_notice_id',$not_mailed_out_master_ids);
//dump($WorkOrderFields->get());
       
       /* if (!empty($request->get('search')['value'])) {*/
            /* search for workorder */
            
        //} else {
            /* show records which is not printed  || not frim mail */
            /*$WorkOrderFields->where('work_orders.status', '5')->orWhere('work_orders.status', '6')->where('work_orders.print_firm_mail', 'No'); dd($WorkOrderFields->get());*/
      //  }
        $WorkOrderFields = $WorkOrderFields->get()->toArray();

        $data_table = [];
        foreach ($WorkOrderFields as $fields_data) {
            $field_names = explode('||', $fields_data['field_names']);
            $field_values = explode('||', $fields_data['field_values']);
            $field_names_values = (count($field_names) == count($field_values)) ? array_combine($field_names, $field_values) : [];
            $field_names_values['default'] = '';
            $data_table[] = array_merge($fields_data, $field_names_values);
        }
        foreach ($data_table as $k => $each_record) {
            if (isset($each_record['rescind_work_order']) && $each_record['rescind_work_order'] == 1) {
                $data_table[$k]['notice_name'] = $data_table[$k]['notice_name'].' Amendment';
            }
        }
        if (isset($data_table) && !empty($data_table)) {
            foreach ($data_table As $key => $value) {
                $master_notice_id = $value['master_notice_id'];
                $data_table[$key]['firm_mail'] = 'No';
                $data_table[$key]['next_day_Delivery'] = 'No';
                foreach ($value AS $k1 => $v1) {
                    if ($k1 == 'job_start_date' && $v1!="") {
                        if (isset($v1) && $v1 != "") {
                        if($master_notice_id==config('constants.MASTER_NOTICE')['NTO']['ID']){
                                $duedateDays = 40 . 'days';
                                $highlightDays = 30 . 'days';
                                $data_table[$key]['due_date'] = date('m/d/Y', strtotime('+ ' . $duedateDays, strtotime($v1)));
                                $data_table[$key]['highligted_date'] = date('m/d/Y', strtotime('+ ' . $highlightDays, strtotime($v1)));
                        }
                    }
                    }else if ($k1 == 'clerk_of_court_recorded_date' && $v1!=""){
                        if($master_notice_id==config('constants.MASTER_NOTICE')['COL']['ID']){
                          $duedateDays = 270 . 'days';
                          $highlightDays = 270 . 'days';
                          $data_table[$key]['due_date'] = date('m/d/Y', strtotime('+ ' . $duedateDays, strtotime($v1)));
                          $data_table[$key]['highligted_date'] = date('m/d/Y', strtotime('+ ' . $highlightDays, strtotime($v1)));
                        }
                    } else if ($k1 == 'workorder_id') {
                        $recipients = \App\Models\Recipient::leftjoin('stamps_label', 'stamps_label.recipient_id', '=', 'recipients.id')
                                ->where('work_order_id', '=', $v1)
                                ->select('stamps_label.type_of_label', 'stamps_label.recipient_id')
                                ->get();
                        if (isset($recipients) && count($recipients) > 0) {
                            foreach ($recipients AS $k => $v) {
                                if ($v->type_of_label == 'firm mail') {
                                    $data_table[$key]['firm_mail'] = 'Yes';
                                } elseif ($v->type_of_label == 'next day delivery') {
                                    $data_table[$key]['next_day_Delivery'] = 'Yes';
                                }
                            }
                        }
                    }
                }
            }
        }

        $datatables = Datatables::of($data_table)->addColumn('actions', function ($workorder) {
                    return view('account_manager.mailing.action', compact('workorder'))->render();
                })->rawColumns(['actions', 'is_other']);
        return $datatables->make(true);
    }

    public function generateShippinglabels(Request $request) {
        $flash = ['', ''];
        $debugLog = [];
        $rules = ['select_work_orders' => 'required'];
        $error_flash_messages = 'Something went wrong while generating labels from Stamp service. ';
        $success_flash_messages = 'Shipping labels generated for recipient  ';
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        // $work_order_ids = $request->select_work_orders;
        $work_order_ids = [];
        if (isset($_COOKIE['select_all_mail']) && $_COOKIE['select_all_mail'] == 1) {
            $work_order_ids = $this->getselectAllWorkorders($request);
        } elseif (isset($_COOKIE['mailing_checkbox']) && $_COOKIE['mailing_checkbox'] != "") {
            $work_order_ids = explode(",", $_COOKIE['mailing_checkbox']);
        } else {
            $work_order_ids = $request->select_work_orders;
        }

        session(['is_mailing' => 'yes']);
        //Get recipient addresses for the selected work orders
        $recipients = \App\Models\Recipient::join('work_orders', 'work_orders.id', '=', 'recipients.work_order_id')
                ->join('usps_address', 'usps_address.recipient_id', '=', 'recipients.id')
                ->join('stamps_label', 'stamps_label.recipient_id', '=', 'recipients.id')
                ->join('stamps_rates_data', 'stamps_label.rates_data_id', '=', 'stamps_rates_data.id')
                ->whereIn('work_orders.id', $work_order_ids)
                ->where(['stamps_label.type_of_label' => 'stamps', 'stamps_label.generated_label' => 'no'])
                ->whereNotNull('stamps_label.rates_data_id')
                ->get(['usps_address.*', 'recipients.name', 'rates_data', \DB::raw('stamps_rates_data.id as stamps_rates_data_id'), \DB::raw('stamps_label.id as stamps_label_id'), 'type_of_mailing', 'recipients.work_order_id'])
                ->toArray();
                // dump($recipients);
        if (!empty($recipients)) {
            //Stamps.com get token
            $wsdl           = config('constants.STAMPS.WSDL');
            $integrationID  = config('constants.STAMPS.INTEGRATION_ID');
            $username       = config('constants.STAMPS.USERNAME');
            $password       = config('constants.STAMPS.PASSWORD');
            
            $stamps = new \App\Helpers\StampsHelper($wsdl, $integrationID, $username, $password);

            //Setting from address for creating label
            $from_addr = [
                'FullName' => env('FROM_NAME'),
                'Address1' => env('FROM_ADDRESS'),
                'City' => env('FROM_CITY'),
                'State' => env('FROM_STATE'),
                'ZIPCode' => env('FROM_ZIPCODE'),
                'PhoneNumber' => env('FROM_PHONE_NUMBER'),
            ];
            
            foreach ($recipients as $recipient) {
                $shipping_date = date('Y-m-d', strtotime('+1 day'));
                $recipient['company'] = strip_tags($recipient['company']);
                $recipient['company'] = html_entity_decode($recipient['company']);
                $recipient['company'] =str_replace("&NBSP;","",$recipient['company']);
                
                $to_addr = [
                    'FullName' => strtoupper($recipient['name']),
                    'Address1' => strtoupper($recipient['address']),
                    'Address2' => strtoupper($recipient['address2']),
                    'Address3' => strtoupper($recipient['address3']),
                    'City' => $recipient['city'],
                    'State' => $recipient['state'],
                    'Company' => strtoupper($recipient['company']),
                    'ZIPCode' => $recipient['zipcode'],
                    // 'ZIPCodeAddOn' => $recipient['zipcode_add_on'],
                    'DPB' => $recipient['dpb'],
                    'CheckDigit' => $recipient['check_digit'],
                    'OverrideHash' => $recipient['override_hash'],
                ];
                
                $rates1 = json_decode($recipient['rates_data'], true);
                $label = $stamps->CreateIndicium($rates1, $to_addr, $from_addr);

                //log response for debugging
                \Log::info("\n----------\nStamp CreateIndicium Request\n-----------\n".
                        "\n>>To Address<<\n".
                        print_r($to_addr, TRUE).
                        "\n>>From Address<<\n".
                        print_r($from_addr, TRUE).
                        "\n>>Rates<<\n".
                        print_r($rates1, TRUE).
                        "----------\nReponse\n-----------\n".
                        print_r($label, TRUE));
                //log response for debugging
                
                if ($label['status'] == 'success') {
                    $label = $label['result'];
                    $amount = $label->Rate->Amount;
                    $rates = $label->Rate;
                    $rates = json_decode(json_encode($rates), true);

                    if (isset($rates['AddOns'])) {
                        if (isset($rates['AddOns']['AddOnV10']['Amount']) && $rates['AddOns']['AddOnV10']['Amount'] != '') {
                            $amount = $amount + (float) round($rates['AddOns']['AddOnV10']['Amount'], 2);
                        } else {
                            foreach ($rates['AddOns']['AddOnV10'] as $val) {
                                if(isset($val['Amount'])){
                                $amount = $amount + (float) round($val['Amount'], 2);
                                }
                            }
                        }
                    }

                    $url = $label->URL;
                    $contents = file_get_contents($url);
                    $name = substr($url, strrpos($url, '/') + 1);
                    $name = $label->TrackingNumber . '.png';
                    $path = public_path() . '/pdf/stamps_labels';
                    if (!file_exists($path)) {
                        mkdir($path, 0777, true);
                    }
                    Storage::disk('public_uploads')->put('pdf/stamps_labels/' . $name, $contents);
                    $label_url = public_path(). '/pdf/stamps_labels/' . $name;
                    
                    //add extra margin to amount
                    $extraAmount = 0;
                    if(strpos($recipient['type_of_mailing'], 'Electronic Return Receipt') > 0) {
                        $extraAmount = config('constants.QBO.MARGIN_CERTIFIED_ERR');
                    } else if(strpos($recipient['type_of_mailing'], 'Return Receipt') > 0) {
                        $extraAmount = config('constants.QBO.MARGIN_CERTIFIED_RR');
                    } else if(strpos($recipient['type_of_mailing'], 'Certified Mail') >= 0) {
                        $extraAmount = config('constants.QBO.MARGIN_CERTIFIED');
                    }
                    $amount += $extraAmount;

                     //Update status in stamps_label table for generated label
                    \App\Models\StampsLabel::where(['id' => $recipient['stamps_label_id']])->update(['tracking_number' => $label->TrackingNumber, 'StampsTxID' => $label->StampsTxID, 'URL' => $label_url, 'ShipDate' => $label->Rate->ShipDate, 'DeliveryDate' => isset($label->Rate->DeliveryDate) ? $label->Rate->DeliveryDate : '', 'Amount' => $amount, 'recipient_id' => $recipient['recipient_id'], 'generated_label' => 'yes', 'rates_data_id' => NULL]);
                    \App\Models\StampsRatesData::where(['id' => $recipient['stamps_rates_data_id']])->delete();
                   
                    $success_ids[] = $recipient['id'];
                    $debugLog[] = '#'.$recipient['work_order_id'].' - '.$recipient['id'].' "'.$recipient['name'].'" || Success';
                } else { 
                    \App\Models\StampsRatesData::where(['id' => $recipient['stamps_rates_data_id']])->update(['label_error' => $label['message']]);
                    $error_ids[] = $recipient['id'];
                    $debugLog[] = '#'.$recipient['work_order_id'].' - '.$recipient['id'].' "'.$recipient['name'].'" || Error: '.$label['message'];
                    \Log::error('Encountered Problem While Generating Label for recipient with id ' . $recipient['id']);
                }
            }//for
       
            if (!empty($error_ids)) {
                $error_ids = implode(', ', $error_ids);
                //$error_flash_messages .= $error_ids;
                $flash = ['error', $error_flash_messages];
            }

            if (!empty($success_ids)) {
                $success_ids = implode(', ', $success_ids);
                //$success_flash_messages .= $success_ids;
                $flash = ['success', $success_flash_messages];
            }
        } else {
            $flash = ['error', 'Labels already generated for this work order'];
        }

        return redirect('account-manager/mailing/0')->with($flash[0], $flash[1])->with('debugLog', $debugLog);
    }

    public function generateCyoShippinglabels(Request $request) {
        $flash = ['', ''];
        $debugLog = [];
        $error_flash_messages = 'Something went wrong while generating labels from Stamp service. ';
        $success_flash_messages = 'Shipping labels generated for recipient  ';
        $rules = ['select_work_orders' => 'required'];

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        //$work_order_ids = $request->select_work_orders;

        /**
         * If you want to print multiple pdfs
         */
        $work_order_ids = [];
        if (isset($_COOKIE['select_all_mail']) && $_COOKIE['select_all_mail'] == 1) {
            $work_order_ids = $this->getselectAllWorkorders($request);
        } elseif (isset($_COOKIE['mailing_checkbox']) && $_COOKIE['mailing_checkbox'] != "") {
            $work_order_ids = explode(",", $_COOKIE['mailing_checkbox']);
        } else {
            $work_order_ids = $request->select_work_orders;
        }



        session(['is_mailing' => 'yes']);
        //Get recipient addresses for the selected work orders
        $recipients = \App\Models\Cyo_WorkOrder_Recipient::join('cyo__work_orders', 'cyo__work_orders.id', '=', 'cyo__work_order__recipients.work_order_id')
                ->join('cyo__usps_addresses', 'cyo__usps_addresses.recipient_id', '=', 'cyo__work_order__recipients.id')
                ->join('cyo__stamps_labels', 'cyo__stamps_labels.recipient_id', '=', 'cyo__work_order__recipients.id')
                ->join('cyo_stamps_rates_data', 'cyo__stamps_labels.rates_data_id', '=', 'cyo_stamps_rates_data.id')
                ->whereIn('cyo__work_orders.id', $work_order_ids)
                ->where(['cyo__stamps_labels.type_of_label' => 'stamps', 'cyo__stamps_labels.generated_label' => 'no'])
                ->whereNotNull('cyo__stamps_labels.rates_data_id')
                ->get(['cyo__usps_addresses.*', 'cyo__work_order__recipients.name', 'rates_data', \DB::raw('cyo_stamps_rates_data.id as stamps_rates_data_id'), \DB::raw('cyo__stamps_labels.id as stamps_label_id'), 'type_of_mailing','cyo__work_order__recipients.work_order_id'])
                ->toArray();
                
        if (!empty($recipients)) {
            //Stamps.com get token
            $wsdl           = config('constants.STAMPS.WSDL');
            $integrationID  = config('constants.STAMPS.INTEGRATION_ID');
            $username       = config('constants.STAMPS.USERNAME');
            $password       = config('constants.STAMPS.PASSWORD');

            $stamps = new \App\Helpers\StampsHelper($wsdl, $integrationID, $username, $password);

            //Setting from address for creating label
            $from_addr = [
                'FullName' => env('FROM_NAME'),
                'Address1' => env('FROM_ADDRESS'),
                'City' => env('FROM_CITY'),
                'State' => env('FROM_STATE'),
                'ZIPCode' => env('FROM_ZIPCODE'),
                'PhoneNumber' => env('FROM_PHONE_NUMBER'),
            ];

            foreach ($recipients as $recipient) {
                $shipping_date = date('Y-m-d', strtotime('+1 day'));
                $recipient['company'] = strip_tags($recipient['company']);
                $recipient['company'] = html_entity_decode($recipient['company']);
                $recipient['company'] =str_replace("&NBSP;","",$recipient['company']);
                
                $to_addr = [
                    'FullName' => strtoupper($recipient['name']),
                    'Address1' => strtoupper($recipient['address']),
                    'Address2' => strtoupper($recipient['address2']),
                    'Address3' => strtoupper($recipient['address3']),
                    'City' => $recipient['city'],
                    'State' => $recipient['state'],
                    'Company' => strtoupper($recipient['company']),
                    'ZIPCode' => $recipient['zipcode'],
                    // 'ZIPCodeAddOn' => $recipient['zipcode_add_on'],
                    'DPB' => $recipient['dpb'],
                    'CheckDigit' => $recipient['check_digit'],
                    'OverrideHash' => $recipient['override_hash'],
                ];

                $rates1 = json_decode($recipient['rates_data'], true);
                $label = $stamps->CreateIndicium($rates1, $to_addr, $from_addr);

                //log response for debugging
                \Log::info("\n----------\nCYO Stamp CreateIndicium Request\n-----------\n".
                        "\n>>To Address<<\n".
                        print_r($to_addr, TRUE).
                        "\n>>From Address<<\n".
                        print_r($from_addr, TRUE).
                        "\n>>Rates<<\n".
                        print_r($rates1, TRUE).
                        "----------\nReponse\n-----------\n".
                        print_r($label, TRUE));
                //log response for debugging

                if ($label['status'] == 'success') {
                    $label = $label['result'];
                    $amount = $label->Rate->Amount;
                    $rates = $label->Rate;
                    $rates = json_decode(json_encode($rates), true);

                    if (isset($rates['AddOns'])) {
                        if (isset($rates['AddOns']['AddOnV10']['Amount']) && $rates['AddOns']['AddOnV10']['Amount'] != '') {
                            $amount = $amount + (float) round($rates['AddOns']['AddOnV10']['Amount'], 2);
                        } else {
                            foreach ($rates['AddOns']['AddOnV10'] as $val) {
                                if(isset($val['Amount'])){
                                $amount = $amount + (float) round($val['Amount'], 2);
                                }
                            }
                        }
                    }
                    
                    $extraAmount = 0;
                    if(strpos($recipient['type_of_mailing'], 'Electronic Return Receipt') > 0) {
                        $extraAmount = config('constants.QBO.MARGIN_CERTIFIED_ERR');
                    } else if(strpos($recipient['type_of_mailing'], 'Return Receipt') > 0) {
                        $extraAmount = config('constants.QBO.MARGIN_CERTIFIED_RR');
                    } else if(strpos($recipient['type_of_mailing'], 'Certified Mail') >= 0) {
                        $extraAmount = config('constants.QBO.MARGIN_CERTIFIED');
                    }
                    $amount += $extraAmount;
                    
                    $url = $label->URL;
                    $contents = file_get_contents($url);
                    $name = substr($url, strrpos($url, '/') + 1);
                    $name = $label->TrackingNumber . '.png';
                    $path = public_path() . '/pdf/cyo_stamps_labels';
                    if (!file_exists($path)) {
                        mkdir($path, 0777, true);
                    }
                    Storage::disk('public_uploads')->put('pdf/cyo_stamps_labels/' . $name, $contents);
                    $label_url = public_path(). '/pdf/cyo_stamps_labels/' . $name;
                    /*Storage::put('/public/cyo_stamps_labels/' . $name, $contents);
                    $label_url = url('/storage/cyo_stamps_labels') . '/' . $name;*/
                    //Remove record from stamps_rates_data table
                     //Update status in stamps_label table for generated label
                    \App\Models\Cyo_StampsLabel::where(['id' => $recipient['stamps_label_id']])->update(['tracking_number' => $label->TrackingNumber, 'StampsTxID' => $label->StampsTxID, 'URL' => $label_url, 'ShipDate' => $label->Rate->ShipDate, 'DeliveryDate' => isset($label->Rate->DeliveryDate) ? $label->Rate->DeliveryDate : '', 'Amount' => $amount, 'recipient_id' => $recipient['recipient_id'], 'generated_label' => 'yes', 'rates_data_id' => NULL]);

                    \App\Models\Cyo_StampsRatesData::where(['id' => $recipient['stamps_rates_data_id']])->delete();
                   
                    $success_ids[] = $recipient['id'];
                    $debugLog[] = '#'.$recipient['work_order_id'].' - '.$recipient['id'].' "'.$recipient['name'].'" || Success';
                } else {
                    \App\Models\Cyo_StampsRatesData::where(['id' => $recipient['stamps_rates_data_id']])->update(['label_error' => $label['message']]);
                    $error_ids[] = $recipient['id'];
                    $debugLog[] = '#'.$recipient['work_order_id'].' - '.$recipient['id'].' "'.$recipient['name'].'" || Error: '.$label['message'];
                    \Log::error('Encountered Problem While Generating Label for recipient with id ' . $recipient['id'] . ' for CYO');
                }
            }//for
            if (!empty($error_ids)) {
                $error_ids = implode(', ', $error_ids);
                //$error_flash_messages .= $error_ids;
                $flash = ['error', $error_flash_messages];
            }

            if (!empty($success_ids)) {
                $success_ids = implode(', ', $success_ids);
              //  $success_flash_messages .= $success_ids;
                $flash = ['success', $success_flash_messages];
            }
        } else {
            $flash = ['error', 'Labels already generated for this work order'];
        }
        return redirect('account-manager/cyo-mailing/0')->with($flash[0], $flash[1])->with('debugLog', $debugLog);;
    }

    /**
     * print work order pdf
     */
    public function printWO(Request $request) {

        $rules = ['select_work_orders' => 'required',
        ];

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $work_order_numbers = $request->select_work_orders;
        foreach ($work_order_numbers as $key => $work_order_id) {

            $file_name = \App\Models\WorkOrder::find($work_order_id);
            if (file_exists(public_path() . '/pdf/work_order_document/' . $file_name->file_name)) {

                $mypdf = asset('/') . 'pdf/work_order_document/' . $file_name->file_name;
                echo "<script>window.open('$mypdf', '_blank');</script>";
                $status_change = \App\Models\WorkOrder::find($work_order_id);
                $status_change->status = 6;
                $status = $status_change->save();
                if ($status) {
                    Session::flash('success', 'Your work order # ' . $work_order_id . ' Work Order Completed Successfully.<a href="' . url('customer/work-order/view/' . $work_order_id) . '">Click here to print.</a>');
                } else {
                    Session::flash('success', 'Your work order # ' . $work_order_id . ' Problem in Record saving');
                }
            }

            return view('account_manager.mailing.list');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function cyoMailingList($flag) {
        if (Session::has('is_mailing')) {

            Session::forget('is_mailing');
            setcookie("select_all_mail", "");
            setcookie("mailing_checkbox", "");
            return view('account_manager.cyo_mailing.list')->with('flag',$flag);
        } else {
            return view('account_manager.cyo_mailing.list')->with('flag',$flag);

           // return redirect('admin/dashboard');
        }
    }

    /**
     *  datatables list workorders
     */
    public function getCyoMailing(Request $request) {
        $not_mailed_out_master_ids= [config('constants.MASTER_NOTICE')['SBC']['ID'],config('constants.MASTER_NOTICE')['WRL']['ID']];
        if(Auth::user()->hasRole('admin')){
        $WorkOrderFields = \App\Models\Cyo_WorkOrder_fields::select('notices.master_notice_id as master_notice_id','notices.name as notice_name', 'cyo__work_orders.parent_id','cyo__work_orders.file_name', 'cyo__work_orders.is_rescind as rescind_work_order', 'cyo__work_orders.created_at', 'cyo__work_orders.user_id', 'cyo__work_orders.notice_id as notice_id', 'cyo__work_orders.status', 'cyo__work_orders.print_firm_mail', 'cyo__work_orders.user_id', 'cyo__work_orders.account_manager_id', 'cyo__work_order_fields.workorder_id', 'cyo__work_order_fields.notice_id', DB::raw("(GROUP_CONCAT(notice_fields.name SEPARATOR '||')) as `field_names`"), DB::raw("(GROUP_CONCAT(value SEPARATOR '||')) as `field_values`"))
                ->leftjoin('notice_fields', 'notice_fields.id', '=', 'cyo__work_order_fields.notice_field_id')
                ->leftjoin('cyo__work_orders', 'cyo__work_orders.id', '=', 'cyo__work_order_fields.workorder_id')
                ->leftjoin('notices', 'notices.id', '=', 'cyo__work_orders.notice_id')
                ->groupBy('cyo__work_order_fields.workorder_id', 'cyo__work_order_fields.notice_id');
        }else{
            $WorkOrderFields = \App\Models\Cyo_WorkOrder_fields::select('notices.master_notice_id as master_notice_id','notices.name as notice_name', 'cyo__work_orders.parent_id','cyo__work_orders.file_name', 'cyo__work_orders.is_rescind as rescind_work_order', 'cyo__work_orders.created_at', 'cyo__work_orders.user_id', 'cyo__work_orders.notice_id as notice_id', 'cyo__work_orders.status', 'cyo__work_orders.print_firm_mail', 'cyo__work_orders.user_id', 'cyo__work_orders.account_manager_id', 'cyo__work_order_fields.workorder_id', 'cyo__work_order_fields.notice_id', DB::raw("(GROUP_CONCAT(notice_fields.name SEPARATOR '||')) as `field_names`"), DB::raw("(GROUP_CONCAT(value SEPARATOR '||')) as `field_values`"))
                ->rightjoin('notice_fields', 'notice_fields.id', '=', 'cyo__work_order_fields.notice_field_id')
                ->join('cyo__work_orders', 'cyo__work_orders.id', '=', 'cyo__work_order_fields.workorder_id')
                ->leftjoin('notices', 'notices.id', '=', 'cyo__work_orders.notice_id')
                ->groupBy('cyo__work_order_fields.workorder_id', 'cyo__work_order_fields.notice_id')
                ->where('cyo__work_orders.account_manager_id', Auth::user()->id); 
        }
//                        ->where('account_manager_id', Auth::user()->id)
        //->where('cyo__work_orders.status', [5, 6]);



        if (($request->has('from_date')) && ($request->get('from_date') != NULL)) {
            $WorkOrderFields->whereDate('cyo__work_orders.created_at', '>=', date('Y-m-d', strtotime($request->get('from_date'))));
        }
        if (($request->has('to_date')) && ($request->get('to_date') != NULL)) {
            $WorkOrderFields->whereDate('cyo__work_orders.created_at', '<=', date('Y-m-d', strtotime($request->get('to_date'))));
        }
        if (($request->has('work_order_no')) && ($request->get('work_order_no') != NULL)) {
            
            $WorkOrderFields->where('cyo__work_order_fields.workorder_id', 'like', '%' . $request->get("work_order_no") . '%');
                /* search for workorder */
        //$WorkOrderFields->whereIN('cyo__work_orders.status', [5, 6]);
        }

        if(($request->has('from_date') && $request->get('from_date') != NULL) || ($request->has('to_date') && $request->get('to_date') != NULL) || ($request->has('work_order_no')&&$request->get('work_order_no') != NULL)){
             $WorkOrderFields->whereIN('cyo__work_orders.status', [5, 6]);
        }else{
            if ($request->Completedwolist == 1) {
                $WorkOrderFields->whereIN('cyo__work_orders.status', [6]);
                $date = Carbon::now()->subDays(5);
                $WorkOrderFields->where('cyo__work_orders.updated_at', '>=', $date);
            }else{
                $WorkOrderFields->whereIN('cyo__work_orders.status', [5]);
            }
        }

      //  if ($request->get('search')['value'] != "") {
            /* search for workorder */
          //  $WorkOrderFields->whereIN('cyo__work_orders.status', [5, 6]);
       // } else {
            /* show records which is not printed  || not frim mail */
          /*  $WorkOrderFields->where('cyo__work_orders.status', '5')->orWhere('cyo__work_orders.status', '6')->where('cyo__work_orders.print_firm_mail', 'No');
        }*/
        $WorkOrderFields->whereNotIn('notices.master_notice_id',$not_mailed_out_master_ids);
        $WorkOrderFields = $WorkOrderFields->get()->toArray();
        $data_table = [];
        foreach ($WorkOrderFields as $fields_data) {
            $field_names = explode('||', $fields_data['field_names']);
            $field_values = explode('||', $fields_data['field_values']);
            $field_names_values = (count($field_names) == count($field_values)) ? array_combine($field_names, $field_values) : [];
            $field_names_values['default'] = '';
            $data_table[] = array_merge($fields_data, $field_names_values);
        }
        foreach ($data_table as $k => $each_record) {
            if (isset($each_record['rescind_work_order']) && $each_record['rescind_work_order'] == 1) {
                $data_table[$k]['notice_name'] = $data_table[$k]['notice_name'].' Amendment';
            }
        }
        if (isset($data_table) && !empty($data_table)) {
            foreach ($data_table As $key => $value) {
                $master_notice_id = $value['master_notice_id'];
                $data_table[$key]['firm_mail'] = 'No';
                $data_table[$key]['next_day_Delivery'] = 'No';
                foreach ($value AS $k1 => $v1) {
                    if ($k1 == 'job_start_date' && $v1!=""){
                      if($master_notice_id==config('constants.MASTER_NOTICE')['NTO']['ID']){
                        $duedateDays = 40 . 'days';
                        $highlightDays = 30 . 'days';
                        $data_table[$key]['due_date'] = date('m/d/Y', strtotime('+ ' . $duedateDays, strtotime($v1)));
                        $data_table[$key]['highligted_date'] = date('m/d/Y', strtotime('+ ' . $highlightDays, strtotime($v1)));
                      }
                    }else if ($k1 == 'clerk_of_court_recorded_date' && $v1!=""){
                      if($master_notice_id==config('constants.MASTER_NOTICE')['COL']['ID']){
                        $duedateDays = 270 . 'days';
                        $highlightDays = 270 . 'days';
                        $data_table[$key]['due_date'] = date('m/d/Y', strtotime('+ ' . $duedateDays, strtotime($v1)));
                        $data_table[$key]['highligted_date'] = date('m/d/Y', strtotime('+ ' . $highlightDays, strtotime($v1)));
                      }
                    } else if ($k1 == 'workorder_id') {
                        $recipients = \App\Models\Cyo_WorkOrder_Recipient::leftjoin('cyo__stamps_labels', 'cyo__stamps_labels.recipient_id', '=', 'cyo__work_order__recipients.id')
                                ->where('work_order_id', '=', $v1)
                                ->select('cyo__stamps_labels.type_of_label', 'cyo__stamps_labels.recipient_id')
                                ->get();
                        if (isset($recipients) && count($recipients) > 0) {
                            foreach ($recipients AS $k => $v) {
                                if ($v->type_of_label == 'firm mail') {
                                    $data_table[$key]['firm_mail'] = 'Yes';
                                } elseif ($v->type_of_label == 'next day delivery') {
                                    $data_table[$key]['next_day_Delivery'] = 'Yes';
                                }
                            }
                        }
                    }
                }
            }
        }
        $datatables = Datatables::of($data_table)->addColumn('actions', function ($workorder) {
                    return view('account_manager.cyo_mailing.action', compact('workorder'))->render();
                })->rawColumns(['actions', 'is_other']);
        return $datatables->make(true);
    }

    public function checkMailingPin(Request $request) {
        $validator = Validator::make(Input::all(), [
                    'mailing_pin' => 'required|numeric'
        ]);
        if ($validator->fails()) {
            return json_encode(['status' => 'error', 'message' => $validator->errors()->all()]);
        }
        $user_details = Auth::user();
        if (isset($user_details->pin) && $user_details->pin != NULL) {
            if ($user_details->pin == $request->mailing_pin) {
                session(['is_mailing' => 'yes']);
                \Session::flash('success', 'You can access mailing module.');
                return json_encode(['status' => 'success', 'message' => 'You can access mailing module.']);
            } else {
                return json_encode(['status' => 'error', 'message' => 'Please enter valid pin']);
            }
        } else {
            return json_encode(['status' => 'error', 'message' => 'You cannot access mailing module.']);
        }
    }

    public function getselectAllWorkorders($request) {

        if (isset($request->type_work_order) && $request->type_work_order == 'cyo') {
            $WorkOrderFields = \App\Models\Cyo_WorkOrder_fields::
                    leftjoin('notice_fields', 'notice_fields.id', '=', 'cyo__work_order_fields.notice_field_id')
                    ->leftjoin('cyo__work_orders', 'cyo__work_orders.id', '=', 'cyo__work_order_fields.workorder_id')
                    ->leftjoin('notices', 'notices.id', '=', 'cyo__work_orders.notice_id')
                    ->groupBy('cyo__work_order_fields.workorder_id', 'cyo__work_order_fields.notice_id');
//                        ->where('account_manager_id', Auth::user()->id)
            //->where('cyo__work_orders.status', [5, 6]);



            if (($request->has('from_date')) && ($request->get('from_date') != NULL)) {
                $WorkOrderFields->whereDate('cyo__work_orders.created_at', '>=', date('Y-m-d', strtotime($request->get('from_date'))));
            }
            if (($request->has('to_date')) && ($request->get('to_date') != NULL)) {
                $WorkOrderFields->whereDate('cyo__work_orders.created_at', '<=', date('Y-m-d', strtotime($request->get('to_date'))));
            }

            if (($request->has('work_order_no')) && ($request->get('work_order_no') != NULL)) {
                $WorkOrderFields->where('cyo__work_order_fields.workorder_id', 'like', '%' . $request->get("work_order_no") . '%');
                /* search for workorder */
                $WorkOrderFields->whereIN('cyo__work_orders.status', [5, 6]);
            } else {
                /* show records which is not printed  || not frim mail */
                $WorkOrderFields->where('cyo__work_orders.status', '5')->where('cyo__work_orders.print_firm_mail', 'No');
            }
            $WorkOrderFields = $WorkOrderFields->pluck('cyo__work_orders.id', 'cyo__work_orders.id')->toArray();
        } else {

            $WorkOrderFields = \App\Models\WorkOrderFields::
                    leftjoin('notice_fields', 'notice_fields.id', '=', 'work_order_fields.notice_field_id')
                    ->leftjoin('work_orders', 'work_orders.id', '=', 'work_order_fields.workorder_id')
                    ->leftjoin('notices', 'notices.id', '=', 'work_orders.notice_id')
                    ->groupBy('work_order_fields.workorder_id', 'work_order_fields.notice_id');
            // ->where('account_manager_id', Auth::user()->id)

            if (($request->has('from-date')) && ($request->get('from-date') != NULL)) {

                $WorkOrderFields->whereDate('work_orders.created_at', '>=', date('Y-m-d', strtotime($request->get('from-date'))));
            }
            if (($request->has('to-date')) && ($request->get('to-date') != NULL)) {
                $WorkOrderFields->whereDate('work_orders.created_at', '<=', date('Y-m-d', strtotime($request->get('to-date'))));
            }
            if (($request->has('work_order_no')) && ($request->get('work_order_no') != NULL)) {
                $WorkOrderFields->where('work_order_fields.workorder_id', 'like', '%' . $request->get("work_order_no") . '%');

                /* search for workorder */
                $WorkOrderFields->whereIN('work_orders.status', [5, 6]);
            } else {
                /* show records which is not printed  || not frim mail */
                $WorkOrderFields->where('work_orders.status', '5')->where('work_orders.print_firm_mail', 'No');
            }
            $WorkOrderFields = $WorkOrderFields->pluck('work_orders.id', 'work_orders.id')->toArray();
        }
        return $WorkOrderFields;
    }

    /**
     * Print Work Order
     */
    public function printWorkOrder(Request $request) {
        
        set_time_limit(0); 
        //if (isset($request->select_work_orders) && count($request->select_work_orders) == 1) {
        //    $work_order_id = $request->select_work_orders[0];
        //} else {
        //    return \Redirect::back()->withErrors(['Please select only one work order.']);
        //}

        /**
         * If you want to print multiple pdfs
         */
        $type_of_mailing = explode(',',$request->type_of_mailing);
        foreach ($type_of_mailing as $key => $value) {
             if($value== 'firm mail' || $value== 'next day delivery' || $value== 'Cert. RR' || $value== 'Cert. ER' || $value== 'Cert. USPS' ){
                    $type_of_mailing[$key] = $value;
                    }else if($value=='Certified Mail Return Receipt Requested'){
                        $replaceValue = str_replace("Mail Return", "Mail,Return",$value);
                        $type_of_mailing[$key] = $replaceValue;
                       // $value = $replaceValue;
                    }else if($value=='Certified Mail Electronic Return Receipt'){
                        $replaceValue = str_replace("Mail Electronic", "Mail,Electronic",$value);
                        $type_of_mailing[$key] = $replaceValue;
                        //$value = $replaceValue;
                    }else{
                        $type_of_mailing[$key] = $value;
                    }
            # code...
        }
        $print_workorder_id = [];
        $myprint_pdf = [];
        if (isset($_COOKIE['select_all_mail']) && $_COOKIE['select_all_mail'] == 1) {

            $print_workorder_id = $this->getselectAllWorkorders($request);
          
        } elseif (isset($_COOKIE['mailing_checkbox']) && $_COOKIE['mailing_checkbox'] != "") {
            $print_workorder_id = explode(",", $_COOKIE['mailing_checkbox']);
        } else {
            $print_workorder_id = $request->select_work_orders;
        }
        if (!empty($print_workorder_id)) {
            if (($key = array_search(null, $print_workorder_id)) !== false) {
                unset($print_workorder_id[$key]);
            }
        }

        // if ($request->select_work_orders == null) {
        if (empty($print_workorder_id)) {
            return response()->json(['status' => 'error', 'message' => 'Please select at least one work order.']);
        }

        //  foreach ($request->select_work_orders AS $value) {
        foreach ($print_workorder_id AS $value) {
            if(isset($value) && $value!=null){
            $work_order_id = $value;
            /**
             * If pdf type is cyo
             */
            if (isset($request->type_work_order) && $request->type_work_order == 'cyo') {
                $work_order = \App\Models\Cyo_WorkOrders::find($work_order_id);
                $notice_id = $work_order->notice_id;


                //dd($request->all());

                $WorkOrderFields = \App\Models\Cyo_WorkOrder_fields::select('cyo__work_orders.parent_id', 'cyo__work_orders.created_at', 'cyo__work_orders.user_id', 'cyo__work_orders.customer_id','cyo__work_orders.is_rescind as rescind_work_order', 'cyo__work_orders.notice_id as notice_id', 'cyo__work_orders.status', 'cyo__work_orders.user_id', 'cyo__work_orders.account_manager_id', 'cyo__work_order_fields.workorder_id', 'cyo__work_order_fields.notice_id', 'cyo__work_orders.completed_at as completed_date',DB::raw("(GROUP_CONCAT(name SEPARATOR '||')) as `field_names`"), DB::raw("(GROUP_CONCAT(value SEPARATOR '||')) as `field_values`"))->join('notice_fields', 'notice_fields.id', '=', 'cyo__work_order_fields.notice_field_id')
                                ->join('cyo__work_orders', 'cyo__work_orders.id', '=', 'cyo__work_order_fields.workorder_id')
                                ->groupBy('cyo__work_order_fields.workorder_id', 'cyo__work_order_fields.notice_id')
                                ->where('cyo__work_orders.id', $work_order_id)
                                ->get()->toArray();
            } else {
                /**
                 * If pdf type is not cyo means it is work order
                 */
                $work_order = WorkOrder::find($work_order_id);
                $notice_id = $work_order->notice_id;


                //dd($request->all());

                $WorkOrderFields = \App\Models\WorkOrderFields::select('work_orders.parent_id', 'work_orders.created_at', 'work_orders.user_id', 'work_orders.customer_id','work_orders.is_rescind as rescind_work_order', 'work_orders.notice_id as notice_id', 'work_orders.status', 'work_orders.user_id', 'work_orders.account_manager_id','work_orders.completed_at as completed_date', 'work_order_fields.workorder_id', 'work_order_fields.notice_id', DB::raw("(GROUP_CONCAT(name SEPARATOR '||')) as `field_names`"), DB::raw("(GROUP_CONCAT(value SEPARATOR '||')) as `field_values`"))->join('notice_fields', 'notice_fields.id', '=', 'work_order_fields.notice_field_id')
                                ->join('work_orders', 'work_orders.id', '=', 'work_order_fields.workorder_id')->groupBy('work_order_fields.workorder_id', 'work_order_fields.notice_id')
                                ->where('work_orders.id', $work_order_id)
                                ->get()->toArray();
            }
        }
            $rescind_work_order = '';
            if($WorkOrderFields[0]['rescind_work_order']==1){
                $WorkOrderFields[0]['rescind_work_order'] = 'AMENDMENT';
            }else{
                $WorkOrderFields[0]['rescind_work_order'] = '';
            }
            $notice_type = \App\Models\Notice::find($notice_id);

            $result = [];
            if (isset($WorkOrderFields) && !empty($WorkOrderFields)) {
                foreach ($WorkOrderFields as $fields_data) {
                    $field_names = explode('||', $fields_data['field_names']);
                    $field_values = explode('||', $fields_data['field_values']);
                    $field_names_values = (count($field_names) == count($field_values)) ? array_combine($field_names, $field_values) : [];
                   // $field_names_values = array_combine($field_names, $field_values);
                    $field_names_values['default'] = '';
                    $result[] = array_merge($fields_data, $field_names_values);
                }
            }
            /*
             * Fetch all work order recipients
             */
            if (isset($request->type_work_order) && $request->type_work_order == 'cyo') {
                    $recipients = \App\Models\Cyo_WorkOrder_Recipient::leftjoin('cyo__stamps_labels', 'cyo__stamps_labels.recipient_id', '=', 'cyo__work_order__recipients.id')
                        ->leftjoin('cyo__usps_addresses', 'cyo__usps_addresses.recipient_id', '=', 'cyo__work_order__recipients.id')
                        ->where('work_order_id', '=', $work_order_id)
                        ->select('cyo__work_order__recipients.address as ad','cyo__work_order__recipients.category_id','cyo__work_order__recipients.city_id','cyo__work_order__recipients.state_id','cyo__work_order__recipients.zip','cyo__work_order__recipients.attn','cyo__work_order__recipients.name', 'cyo__usps_addresses.*','cyo__stamps_labels.*')
                        ->get();
                    if ($notice_type->is_bond_of_claim) {
                        $recipients_is_bond_of_claim = \App\Models\Cyo_WorkOrder_Recipient::leftjoin('cyo__stamps_labels', 'cyo__stamps_labels.recipient_id', '=', 'cyo__work_order__recipients.id')
                            ->leftjoin('cyo__usps_addresses', 'cyo__usps_addresses.recipient_id', '=', 'cyo__work_order__recipients.id')
                            ->where('work_order_id', '=', $work_order_id)->where('category_id', '9')
                            ->select('cyo__work_order__recipients.address as ad','cyo__work_order__recipients.category_id','cyo__work_order__recipients.city_id','cyo__work_order__recipients.state_id','cyo__work_order__recipients.zip','cyo__work_order__recipients.attn','cyo__work_order__recipients.name', 'cyo__usps_addresses.*','cyo__stamps_labels.*')
                            ->get();
                    }
            } else {
                    $recipients = \App\Models\Recipient::leftjoin('stamps_label', 'stamps_label.recipient_id', '=', 'recipients.id')
                        ->leftjoin('usps_address', 'usps_address.recipient_id', '=', 'recipients.id')
                        ->where('work_order_id', '=', $work_order_id)
                        ->select('recipients.address as ad','recipients.city_id','recipients.category_id','recipients.state_id','recipients.zip','recipients.attn','recipients.name', 'usps_address.*','stamps_label.*')
                        ->get();
                    if ($notice_type->is_bond_of_claim) {
                        $recipients_is_bond_of_claim = \App\Models\Recipient::leftjoin('stamps_label', 'stamps_label.recipient_id', '=', 'recipients.id')
                            ->leftjoin('usps_address', 'usps_address.recipient_id', '=', 'recipients.id')
                            ->where('work_order_id', '=', $work_order_id)->where('category_id', '9')
                            ->select('recipients.address as ad','recipients.city_id','recipients.category_id','recipients.state_id','recipients.zip','recipients.attn','recipients.name', 'usps_address.*','stamps_label.*')
                            ->get();
                    }
                }

            $data = $result[0];
            /*
             * Get the notice template as per notice id
             */
            $template_content = \App\Models\NoticeTemplate::where('notice_id', '=', $notice_id)->first();
            /*
             * Template is not available for bond claim of notice
             */
            if ($template_content == null) {
                $content = 'No pdf is availble for bond claim of lien notice';
                // $pdf = PDF::loadHTML($content);
                // $file_name =  $work_order_id . '_' . uniqid() . '.pdf';
                // $pdf->stream($file_name);
                return response()->json(['status' => 'error', 'message' => 'No pdf is availble for bond claim of lien notice.']);
                //\Session::flash('success', 'No pdf is availble for bond claim of lien notice');
            } else {
                $content = $template_content->content;
                $notice_state = \App\Models\State::find($template_content->state_id);
                $noticestate = $notice_state->name;
                $arr_variable = [];
                $arr_replace = [];
                $contracted_by = '';
                $legal_description = '';
                $bond_claim_number = '';
                $your_job_reference_no = '';
                $title = '';
                $recorded_day = '';
                $recorded_month = '';
                $project_name = '';
                $sub_subcontractor_received_type_mailing = "";
                $subcontractor_received_type_mailing = "";
                $general_contractor_received_type_mailing = "";
                $owner_received_type_mailing = "";
                $owner_received_nto_date = "";
                $general_contractor_received_nto_date = "";
                $sub_subcontractor_received_nto_date = "";
                $subcontractor_received_nto_date = "";
                $parent_work_order = "";
                $project_owner = "";
                $parent_county = "";
                //$amendment = '';
                if (isset($content) && $content != NULL) {
                    /*                     * *********Dynamic Data**************** */
                    if (isset($data) && !empty($data)) {
                        foreach ($data AS $key => $value) {
                            /*
                             * All dynamic data push into array key is like {{project_address}}
                             */
                            array_push($arr_variable, '{{' . $key . '}}');
                            /*
                             * If dynamic data want to modify
                             */
                            if ($key == 'date_request' || $key == 'enter_into_agreement' ||
                                    $key == 'last_date_of_labor_service_furnished') {
                                if (isset($value) && $value != NULL && $value != '') {
                                    $value = date('M d,Y', strtotime($value));
                                //$date = \DateTime::createFromFormat("m-d-Y", $value->value);
                                //$date = $date->format('m-d-Y'); 
                                //$value->value = $date;
                                }
                            } else if ($key == 'workorder_id') {
                                if (isset($request->type_work_order) && $request->type_work_order == 'cyo') {
                                    $value = 'CYO W/O #' . $value . ' CUST #' . $work_order->customer_id;
                                      }else{
                                      $value = 'W/O #' . $value . ' CUST #' . $work_order->customer_id;
                                    }
                            } else if ($key == 'parent_work_order') {
                                if (isset($value) && !empty($value)) {
                                    $parent_work_order = $value;
                                 if (isset($work_order_type) && $work_order_type == 'cyo') {
                                    $work_order_parent = \App\Models\Cyo_WorkOrders::find($value);
                                  }else{
                                    $work_order_parent = WorkOrder::find($value);
                                  }
                                $notice_id = $work_order_parent->notice_id;
                                $notice_field_id = \App\Models\NoticeField::where('notice_id',$notice_id)->where('name','county')->get();
                                if(!empty($notice_field_id) && $notice_field_id->count()){
                                $notice_field_id = $notice_field_id[0]->id;
                                $WorkOrderFields = \App\Models\WorkOrderFields::select('value')->where('workorder_id',$value )->where('notice_field_id',$notice_field_id)->get()->toArray();
                                if(!empty($WorkOrderFields)){
                                  $county_id = $WorkOrderFields[0]['value'];
                                  $county_name = \App\Models\City::find($county_id);
                                  $parent_county = !empty($county_name) ? $county_name->county: "";            
                                }
                                }  
                                $value = 'Parent Work Order:' . $value;
                                }
                            } else if ($key == 'project_name') {
                                if (isset($value) && !empty($value)) {
                                    $project_name = 'Project Name : Project:' . $value;
                                    $value = 'Project Name : ' . $value;
                                } else {
                                    $project_name = '';
                                    $value = "";
                                }
                            }else if ($key == 'project_address') {
                                if (isset($value) && !empty($value)) {
                                    $value = str_lreplace($value);
                                    $value = strtoupper($value);
                                    $project_address = 'Project:' . $value;
                                } else {
                                    $project_address = '';
                                }
                            }else if($key == 'project_owner'){
                              if (isset($value) && !empty($value)) {
                                $value = $value;
                                $project_owner = $value;
                              }
                            }else if ($key == 'additional_comment') {
                                if (isset($value) && !empty($value)) {
                                    $value =  $value;
                                } else {
                                    $value = '';
                                }
                            }else if ($key == 'total_amount_satisfied') {
                                if (isset($value) && !empty($value)) {
                                     if(is_numeric($value)){
                                        $value = number_format($value,2);
                                      }
                                } else {
                                    $value = '00.00';
                                }
                            }else if ($key == 'completed_date') {
                                if (isset($value) && !empty($value)) {
                                $value = date('d M Y', strtotime($value));
                                $value = explode(' ', $value);
                                $value = $value[0].' day of '.$value[1].', '.$value[2]; 
                                }else{
                                    $value = '___ day of _________, 20__';
                                }
                            }else if ($key == 'total_value') {
                                  if (isset($value) && !empty($value)) {
                                     if(is_numeric($value)){
                                        $value = '($'.number_format($value,2).')';
                                      }
                                } else {
                                    $value = '($00.00)';
                                }
                            }else if ($key == 'unpaid_amount') {
                                 if (isset($value) && !empty($value)) {
                                     if(is_numeric($value)){
                                        $value = '($'.number_format($value,2).')';
                                      }
                                } else {
                                    $value = '($00.00)';
                                }
                            }else if ($key == 'contract_price_or_estimated_value_of_labor') {
                                if (isset($value) && !empty($value)) {
                                     if(is_numeric($value)){
                                        $value = number_format($value,2);
                                      }
                                } else {
                                    $value = '00.00';
                                }
                            }else if ($key == 'estimate_of_amount_owed') {
                                if (isset($value) && !empty($value)) {
                                     if(is_numeric($value)){
                                        $value = number_format($value,2);
                                      }
                                } else {
                                    $value = '00.00';
                                }
                            }else if ($key == 'final_payment') {
                                 if (isset($value) && !empty($value)) {
                                     if(is_numeric($value)){
                                        $value = number_format($value,2);
                                      }
                                } else {
                                    $value = '00.00';
                                }
                            }else if ($key == 'amount_due') {
                                 if (isset($value) && !empty($value)) {
                                     if(is_numeric($value)){
                                        $value = number_format($value,2);
                                      }
                                } else {
                                    $value = '00.00';
                                }
                            }else if ($key == 'estimate_value_of_labor') {
                                 if (isset($value) && !empty($value)) {
                                     if(is_numeric($value)){
                                        $value = number_format($value,2);
                                      }
                                } else {
                                    $value = '00.00';
                                }
                            } else if ($key == 'county' || $key == 'notary_county' || $key == 'recorded_county') {
                                if (isset($value) && !empty($value)) {
                                    $county = \App\Models\City::select('county')->find($value);

                                    $value = $county->county;
                                }
                            } else if ($key == 'notary_state' || $key == 'recorded_state') {
                                if (isset($value) && !empty($value)) {
                                    $notary_state_name = \App\Models\State::find($value);
                                    $value = $notary_state_name->name;
                                }
                            } else if ($key == 'owner_received_type_mailing') {
                                    if (isset($value) && !empty($value)) {
                                    $value = $value;
                                    $owner_received_type_mailing = $value;
                                        }
                            }else if($key == 'general_contractor_received_type_mailing'){
                               if (isset($value) && !empty($value)) {
                                    $value =  $value;
                                    $general_contractor_received_type_mailing = $value;
                                }
                            }else if($key == 'subcontractor_received_type_mailing'){
                                if (isset($value) && !empty($value)) {
                                    $value = $value;
                                    $subcontractor_received_type_mailing = $value;
                                }
                            }else if($key == 'sub_subcontractor_received_type_mailing'){
                                if (isset($value) && !empty($value)) {
                                    $value = $value;
                                    $sub_subcontractor_received_type_mailing = $value;
                                }
                            } else if ($key == 'authorise_agent') {
                                if (isset($value) && !empty($value)) {
                                    //$signature = \App\Models\CustomerAgent::find($value);
                                    //$value = $signature->first_name . ' ' . $signature->last_name;
                                    //$title = $signature->title;
                                    $value = $value;
                                }else{
                                    $value = "";
                                }
                            } else if ($key == 'recorded_date') {
                                if (isset($value) && !empty($value)) {
                                    $recorded_day = date('d', strtotime($value)); //October 2018
                                    //Recorded Month
                                    $recorded_month = date('F Y', strtotime($value));
                                }
                            } else if ($key == 'contracted_by') {
                                if (isset($value) && !empty($value)) {
                                    $contracted_by = $value;
                                } else {
                                    $contracted_by = '';
                                }
                            } else if ($key == 'legal_description') {
                                if (isset($value) && !empty($value)) {
                                    $legal_description = $value;
                                } else {
                                    $legal_description = '';
                                }
                            } else if ($key == 'bond_claim_number') {
                                if (isset($value) && !empty($value)) {
                                    $bond_claim_number = $value;
                                } else {
                                    $bond_claim_number = '';
                                }
                            } else if ($key == 'your_job_reference_no') {
                                if (isset($value) && !empty($value)) {
                                    $your_job_reference_no = $value;
                                } else {
                                    $your_job_reference_no = '';
                                }
                                // array_push($arr_replace, $value);
                            } else if ($key == 'collection_for_attorneys_fees') {
                                if (isset($value) && !empty($value)) {
                                    $value = 'these charges along with interest accruing at the rate of ' . $value . ' monthly and attorneys fees, ';
                                } else {
                                    $value = '';
                                }
                            }else if($key == 'owner_received_nto_date'){
                            if (isset($value) && !empty($value)) {
                              $owner_received_nto_date = $value;
                            }
                            }else if($key == 'general_contractor_received_nto_date'){
                            if (isset($value) && !empty($value)) {
                              $general_contractor_received_nto_date = $value;
                            }
                            }else if($key == 'sub_subcontractor_received_nto_date'){
                              if (isset($value) && !empty($value)) {
                              $sub_subcontractor_received_nto_date = $value;
                            }
                            }else if($key == 'subcontractor_received_nto_date'){
                              if (isset($value) && !empty($value)) {
                                $subcontractor_received_nto_date = $value;
                              }
                            } /* else if ($key == 'job_start_date' || $key == 'last_date_on_the_job' || $key == 'owner_received_nto_date' ||
                                    $key == 'general_contractor_received_nto_date' || $key == 'subcontractor_received_nto_date' ||
                                    $key == 'sub_subcontractor_received_nto_date') {
                                if (isset($value) && !empty($value)) {
                                    $value = date('M d,Y', strtotime($value));
                                }
                            } */else if ($key == 'job_start_date' || $key == 'last_date_on_the_job') {
                                if (isset($value) && !empty($value)) {
                                    $value = date('M d,Y', strtotime($value));
                                }
                            }else if ($key == "check_clear") {

                                if ($value) {
                                    $value = "This form of release is expressly conditioned upon the undersigned’s receiving the actual amount referenced above and payment has been properly endorsed and funds paid/cleared by bank. ";
                                } else {
                                    $value = "";
                                }
                            } else if ($key == "notary_seal") {
                                if ($value) {
                                    $value = "<table style='width: 100%;'>
                                                <tbody>
                                                    <tr>
                                                        <td colspan='2'>
                                                            <table style='width: 50%;'>
                                                                <tbody>
                                                                        <tr>
                                                                                <td style='vertical-align: top;width: 35%;padding: 1px 0; font-size:10px; line-height: 10px;'>State:</td>
                                                                                <td style='width: 50%;padding: 1px 0; font-size:10px; line-height: 10px;'>_________________________________________________________</td>
                                                                        </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                     </tr>
                                                     <tr>
                                                        <td colspan='2'>
                                                            <table style='width: 50%;'>
                                                                <tbody>
                                                                        <tr>
                                                                                <td style='vertical-align: top;width: 35%;padding: 1px 0; font-size:10px; line-height: 10px;'>County:</td>
                                                                                <td style='width: 50%;padding: 1px 0; font-size:10px; line-height: 10px;'>_________________________________________________________</td>
                                                                        </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                     </tr>
                                                     
                                                   <tr>
                                                        <td colspan='2'>
                                                            <table style='width: 100%;'>
                                                                <tbody>
                                                                        <tr>
                                                                                <td style='vertical-align: top;width: 35%;padding: 1px 0; font-size:10px; line-height: 10px;'> Sworn to (or affirmed) and subscribed before me this _______________(Month) ______(Date) _______________(Year)
                                                           BY_______________personally know_______________Produce Identification __________________________________(Type of Identification)      
                                                      </td>
                                                                              
                                                                        </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                     </tr>
                                                     <tr>
                                                        <td colspan='2'>
                                                            <table style='width: 50%;'>
                                                                <tbody>
                                                                        <tr>
                                                                                <td style='vertical-align: top;width: 35%;padding: 1px 0; font-size:10px; line-height: 10px;'>Notary Signature:</td>
                                                                                <td style='width: 50%;padding: 2px 0;'>_________________________________________________________</td>
                                                                        </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                     </tr>
                                                      <tr>
                                                        <td colspan='2'>
                                                            <table style='width: 50%;'>
                                                                <tbody>
                                                                        <tr>
                                                                                <td style='vertical-align: top;width: 35%;padding: 1px 0; font-size:10px; line-height: 10px;'>Notary Stamp</td>
                                                                                <td style='width: 50%;padding: 1px 0; font-size:10px; line-height: 10px;'></td>
                                                                        </tr>
                                                                                <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                                <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                                <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                                <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                                <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                                <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                                <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                                <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                                <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                                <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                                <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                                <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                     </tr>
                                               </tbody></table>";
                                    // dd($value);
                                } else {
                                    $value = "";
                                }
                            }
                            /*
                             * All dynamic data push into array value is like project address
                             */
                            array_push($arr_replace, $value);
                        }
                    }
                }
                $amendment = '';
                if(array_key_exists('amendment',$arr_replace)){
                 // $amendment = '';
                }else{
                 $amendment = '';
                }
                /*                 * *********Dynamic Data**************** */

                /*                 * *********Customer Registration Data**************** */

                if (!in_array("{{parent_work_order}}", $arr_variable)) {
                    array_push($arr_variable, "{{parent_work_order}}");
                    array_push($arr_replace, $parent_work_order);
                }

                if(!in_array("{{county}}", $arr_variable)){
                  array_push($arr_variable,"{{county}}");
                  array_push($arr_replace,$parent_county);
                }else{
                  $index = array_search('{{county}}',$arr_variable);
                  if($arr_replace[$index]==""){
                   $arr_replace[$index] = $parent_county;
                  }
                }
                $customer_company_name = \App\Models\Customer::find($work_order->customer_id);
                $company_name = $customer_company_name->company_name;
                $customer_telephone = $customer_company_name->office_number;
                $company_address = $customer_company_name->mailing_address;
                $company_state_name = \App\Models\State::find($customer_company_name->mailing_state_id);
                $company_state = $company_state_name->name;
                $company_city_name = \App\Models\City::find($customer_company_name->mailing_city_id);
                
                $company_city = !empty($company_city_name) ? $company_city_name->name : '';
                $company_city_state_zip = $company_city . ' ' . $company_state . ' ' . $customer_company_name->mailing_zip;
                /*
                 * Get all work order attachment data
                 */
                if (isset($request->type_work_order) && $request->type_work_order == 'cyo') {
                    $document = \App\Models\Cyo_WorkOrder_Attachment::where('work_order_id', '=', $work_order_id)->get();
                } else {
                    $document = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $work_order_id)->get();
                }
                /*                 * *********Customer Registration Data**************** */
                /*                 * *********Work Order Document Data**************** */
                $bond = '';
                $noc = '';
                $folio = '';
                $permit = '';
                foreach ($document As $key => $each_document) {
                    if ($each_document->type == 1 || $each_document->type == 2 || $each_document->type == 10 || $each_document->type == 3) {
                        /*
                         * If document type is bond
                         */
                        if ($each_document->type == 1 && $each_document->title != "") {
                            $bond = $bond . " " . ' Bond # ' . $each_document->title;
                        }
                        /*
                         * If document type is NOC
                         */
                        if ($each_document->type == 2 && $each_document->title != "") {
                            $noc = $noc . " " . ' BK # ' . $each_document->title;
                        }


                        /*
                         * If document type is Foilo
                         */
                        if ($each_document->type == 10 && $each_document->title != "") {
                            $folio = $folio . " " . ' Folio No # ' . $each_document->title;
                        }

                        /*
                         * If document type is Foilo
                         */
                        if ($each_document->type == 3 && $each_document->title != "") {
                            $permit = $permit . " " . ' Permit # ' . $each_document->title;
                        }
                    }
                }
                /*                 * *********Work Order Document Data**************** */
                /*                 * *********Recipients Owner Data**************** */
                $owner_firm_mailing = '';
                $owner_name = '';
                $owner_address = '';
                $owner_city_state_zip = '';

                $contractor_name = '';
                $sub_contractor_name = '';
                $sub_sub_contractor_name = '';
                $sub_contractor = '';
                /*
                 * Pdf first page display two owners only for notice to owner 
                 */
                $owners = '<table><tbody>';
                $o = 1;
                if (isset($recipients) && count($recipients) > 0) {
                    foreach ($recipients AS $k => $each_recipient) {
                        //Owner 1 & Owner 2 printing
                        if ($each_recipient->category_id == 1 || $each_recipient->category_id ==22) {
                            $owner_firm_mailing = $each_recipient->tracking_number;

                            $owner_name = $each_recipient->name;
                           $each_recipient->attn = str_lreplace($each_recipient->attn);
                            $owner_attn = strtoupper($each_recipient->attn) != "" ? " " . strtoupper($each_recipient->attn) . "<br/>" : "";
                            
                            //$owner_attn = $each_recipient->attn != "" ? " " . $each_recipient->attn . "<br/>" : "";
                            //$owner_address = $each_recipient->address;
                            $owner_address = (!empty($each_recipient->address)) ? $each_recipient->address : $each_recipient->ad;
                            $owner_address .= ((!empty($each_recipient->address2)) ? '<br />' : '').$each_recipient->address2;
                            $owner_address .= ((!empty($each_recipient->address3)) ? '<br />' : '').$each_recipient->address3;

                            $state = "";
                            if (isset($each_recipient->state_id) && $each_recipient->state_id != null) {
                                $state_name = \App\Models\State::find($each_recipient->state_id);
                                $state = $state_name->name;
                            }
                            $city = "";
                            if (isset($each_recipient->city_id) && $each_recipient->city_id != null) {
                                $city_name = \App\Models\City::find($each_recipient->city_id);
                                $city = $city_name->name;
                            }
                            if ($each_recipient->city_id != NULL || $each_recipient->state_id != NULL) {
                                $owner_city_state_zip = $city . ' ' . $state . ' ' . $each_recipient->zip;
                            } else {
                                $owner_city_state_zip = '';
                            }
                            //Start first owner 
                            if ($o == 1) {
                                $owners = $owners . '<tr>';
                            }
                            $owners = $owners . '<td>
                                <table scope="col">
                                    <tbody>
                                        <tr>
                                            <td style="vertical-align: top;padding: 0 2px;font-size:10px; line-height: 10px;">'.(strpos( $content,'{{owners_with_label_property}}') ?  'PROPERTY OWNER' : (strpos( $content,'{{owners_with_label_to_property_owner}}')? 'TO PROPERTY OWNER' : 'TO')).':</td>
                                            <td style="padding: 0 2px;word-wrap:break-word; word-break:break-all;font-size:10px; line-height: 10px;">' . $owner_firm_mailing . '<br/>' . $owner_name 
                                            . '<br/>' . nl2br($owner_attn) . nl2br($owner_address) 
                                            . '<br/>' . $owner_city_state_zip . '<br/></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>';
                            if ($o == 2) {
                                $owners = $owners . '<tr>';
                                //After second onwer break the loop
                                $o = 1;
                                break;
                            }
                            $o++;
                        } else if ($each_recipient->category_id == 2) {
                            //Display the General Contractor name
                            $contractor_name = $each_recipient->name;
                        } else if ($each_recipient->category_id == 3) {
                            //Sub Contractor
                           $state = "";
                            if (isset($each_recipient->state_id) && $each_recipient->state_id != null) {
                                $state_name = \App\Models\State::find($each_recipient->state_id);
                                $state = $state_name->name;
                            }
                            $city = "";
                            if (isset($each_recipient->city_id) && $each_recipient->city_id != null) {
                                $city_name = \App\Models\City::find($each_recipient->city_id);
                                $city = $city_name->name;
                            }
                            if ($each_recipient->city_id != NULL || $each_recipient->state_id != NULL) {
                                $owner_city_state_zip = $city . ' ' . $state . ' ' . $each_recipient->zip;
                            } else {
                                $owner_city_state_zip = '';
                            }
                            $sub_contractor_name = $each_recipient->name;
                            $each_recipient->attn = str_lreplace($each_recipient->attn);
                            $sub_contractor = $each_recipient->name .' '. nl2br($each_recipient->attn).' '. nl2br($each_recipient->address) .' '. $owner_city_state_zip ;

                        } else if ($each_recipient->category_id == 4) {
                            //Display the Sub-Sub Contractor name
                            $sub_sub_contractor_name = $each_recipient->name;
                        }
                    }
                }
                $owners = $owners . '</tbody></table>';


                /*
                 * Receipients Contracted By
                 */
                $contracted_by_recipient = '<table><tbody>';
                $o = 1;
                if (isset($recipients) && count($recipients) > 0) {
                    foreach ($recipients AS $k => $each_recipient) {
                        if ($each_recipient->category_id ==21) {
                            //Contracted By
                            $owner_firm_mailing = $each_recipient->tracking_number;

                            $owner_name = $each_recipient->name;
                            $each_recipient->attn = str_lreplace($each_recipient->attn);
                            $owner_attn = strtoupper($each_recipient->attn) != "" ? " " . strtoupper($each_recipient->attn) . "<br/>" : "";
                           
                            // $owner_attn = $each_recipient->attn != "" ? " " . $each_recipient->attn . "<br/>" : "";
                           // $owner_address = $each_recipient->address;
                            $owner_address = (!empty($each_recipient->address)) ? $each_recipient->address : $each_recipient->ad;
                            $owner_address .= ((!empty($each_recipient->address2)) ? '<br />' : '').$each_recipient->address2;
                            $owner_address .= ((!empty($each_recipient->address3)) ? '<br />' : '').$each_recipient->address3;

                            $state = "";
                            if (isset($each_recipient->state_id) && $each_recipient->state_id != null) {
                                $state_name = \App\Models\State::find($each_recipient->state_id);
                                $state = $state_name->name;
                            }
                            $city = "";
                            if (isset($each_recipient->city_id) && $each_recipient->city_id != null) {
                                $city_name = \App\Models\City::find($each_recipient->city_id);
                                $city = $city_name->name;
                            }
                            if ($each_recipient->city_id != NULL || $each_recipient->state_id != NULL) {
                                $owner_city_state_zip = $city . ' ' . $state . ' ' . $each_recipient->zip;
                            } else {
                                $owner_city_state_zip = '';
                            }
                            //Start first owner 
                            if ($o == 1) {
                                $contracted_by_recipient = $contracted_by_recipient . '<tr>';
                            }
                            $contracted_by_recipient = $contracted_by_recipient . '<td>
                                <table scope="col">
                                    <tbody>
                                        <tr>
                                            <td style="vertical-align: top;padding: 0 2px;font-size:10px; line-height: 10px;">'.(strpos( $content,'{{owners_with_label_property}}') ?  'PROPERTY OWNER' : (strpos( $content,'{{owners_with_label_to_property_owner}}')? 'TO PROPERTY OWNER' : 'TO')).':</td>
                                            <td style="padding: 0 2px;word-wrap:break-word; word-break:break-all;font-size:10px; line-height: 10px;">' . $owner_firm_mailing . '<br/>
                                                                                    <p style="width: 200px; margin-bottom: 0;margin-top: 0;font-size:10px; line-height: 10px;">' . $owner_name . '</p>
                                                                                        ' . nl2br($owner_attn) . nl2br($owner_address) . '<br/>
                                                                                            ' . $owner_city_state_zip . '<br/></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>';
                            if ($o == 1) {
                                $contracted_by_recipient = $contracted_by_recipient . '<tr>';
                                //After second onwer break the loop
                                break;
                                $o = 1;
                            }
                            $o++;
                        }
                    }
                }
                $contracted_by_recipient = $contracted_by_recipient . '</tbody></table>';

                // Receipient Surety(Bond Company)
                $surety_recipient = '<table><tbody>';
                $o = 1;
                if (isset($recipients) && count($recipients) > 0) {
                    foreach ($recipients AS $k => $each_recipient) {
                        if ($each_recipient->category_id ==9) {
                            //Surety(Bond Company
                            $owner_firm_mailing = $each_recipient->tracking_number;

                            $owner_name = $each_recipient->name;
                            $each_recipient->attn = str_lreplace($each_recipient->attn);
                            $owner_attn = strtoupper($each_recipient->attn) != "" ? " " . strtoupper($each_recipient->attn) . "<br/>" : "";
                           
                            // $owner_attn = $each_recipient->attn != "" ? " " . $each_recipient->attn . "<br/>" : "";
                           // $owner_address = $each_recipient->address;
                            $owner_address = (!empty($each_recipient->address)) ? $each_recipient->address : $each_recipient->ad;
                            $owner_address .= ((!empty($each_recipient->address2)) ? '<br />' : '').$each_recipient->address2;
                            $owner_address .= ((!empty($each_recipient->address3)) ? '<br />' : '').$each_recipient->address3;

                            $state = "";
                            if (isset($each_recipient->state_id) && $each_recipient->state_id != null) {
                                $state_name = \App\Models\State::find($each_recipient->state_id);
                                $state = $state_name->name;
                            }
                            $city = "";
                            if (isset($each_recipient->city_id) && $each_recipient->city_id != null) {
                                $city_name = \App\Models\City::find($each_recipient->city_id);
                                $city = $city_name->name;
                            }
                            if ($each_recipient->city_id != NULL || $each_recipient->state_id != NULL) {
                                $owner_city_state_zip = $city . ' ' . $state . ' ' . $each_recipient->zip;
                            } else {
                                $owner_city_state_zip = '';
                            }
                            //Start first owner 
                            if ($o == 1) {
                                $surety_recipient = $surety_recipient . '<tr>';
                            }
                            $surety_recipient = $surety_recipient . '
                                <td>
                                <table scope="col">
                                    <tbody>
                                        <tr>
                                            <td style="vertical-align: top;padding: 0 2px;font-size:10px; line-height: 10px;">'.(strpos( $content,'{{owners_with_label_property}}') ?  'PROPERTY OWNER' : (strpos( $content,'{{owners_with_label_to_property_owner}}')? 'TO PROPERTY OWNER' : 'TO')).':</td>
                                            <td style="padding: 0 2px;word-wrap:break-word; word-break:break-all;font-size:10px; line-height: 10px;">' . $owner_firm_mailing . '<br/>
                                                                                    <p style="width: 200px; margin-bottom: 0;margin-top: 0;font-size:10px; line-height: 10px;">' . $owner_name . '</p>
                                                                                        ' . nl2br($owner_attn) . nl2br($owner_address) . '<br/>
                                                                                            ' . $owner_city_state_zip . '<br/></td>
                                        </tr>
                                    </tbody>
                                </table>
                                </td>
                                ';
                            if ($o == 3) {
                                $surety_recipient = $surety_recipient . '</tr>';
                                //After second onwer break the loop
                                //break;
                                $o = 0;
                            }
                            $o++;
                        }
                    }

                    if ($o == 3) {
                        $surety_recipient = $surety_recipient . '</tr>';
                    }
                }
                $surety_recipient = $surety_recipient . '</tbody></table>';

                //print surety_rec & GC in one line
        $surety_recipient_and_general_contractor = '<table><tbody>';
        $o = 1;
        $recSuretyAndGC = $recipients->whereIn('category_id', [2, 9]);
        if (isset($recSuretyAndGC) && count($recSuretyAndGC) > 0) {
            foreach ($recSuretyAndGC as $k => $each_recipient) {
                $owner_firm_mailing = $each_recipient->tracking_number;

                $owner_name = $each_recipient->name;
                $each_recipient->attn = str_lreplace($each_recipient->attn);
                $owner_attn = strtoupper($each_recipient->attn) != "" ? " " . strtoupper($each_recipient->attn) . "<br/>" : "";
                
                // $owner_attn = $each_recipient->attn != "" ? " " . $each_recipient->attn . "<br/>" : "";
                //$owner_address = $each_recipient->address;
                $owner_address = (!empty($each_recipient->address)) ? $each_recipient->address : $each_recipient->ad;
                $owner_address .= ((!empty($each_recipient->address2)) ? '<br />' : '').$each_recipient->address2;
                $owner_address .= ((!empty($each_recipient->address3)) ? '<br />' : '').$each_recipient->address3;

                $state = "";
                if (isset($each_recipient->state_id) && $each_recipient->state_id != null) {
                    $state_name = \App\Models\State::find($each_recipient->state_id);
                    $state = $state_name->name;
                }
                $city = "";
                if (isset($each_recipient->city_id) && $each_recipient->city_id != null) {
                    $city_name = \App\Models\City::find($each_recipient->city_id);
                    $city = $city_name->name;
                }
                if ($each_recipient->city_id != NULL || $each_recipient->state_id != NULL) {
                    $owner_city_state_zip = $city . ' ' . $state . ' ' . $each_recipient->zip;
                } else {
                    $owner_city_state_zip = '';
                }
                //Start first owner 
                if ($o == 1) {
                    $surety_recipient_and_general_contractor = $surety_recipient_and_general_contractor . '<tr>';
                }
                $surety_recipient_and_general_contractor = $surety_recipient_and_general_contractor . '
                    <td>
                    <table scope="col">
                        <tbody>
                            <tr>
                                <td style="vertical-align: top;padding: 0 2px;font-size:10px; line-height: 10px;">' . (strpos($content, '{{owners_with_label_property}}') ?  'PROPERTY OWNER' : (strpos($content, '{{owners_with_label_to_property_owner}}') ? 'TO PROPERTY OWNER' : 'TO')) . ':</td>
                                <td style="padding: 0 2px;word-wrap:break-word; word-break:break-all;font-size:10px; line-height: 10px;">' . $owner_firm_mailing . '<br/>
                                                                        <p style="width: 200px; margin-bottom: 0;margin-top: 0;font-size:10px; line-height: 10px;">' .$owner_name . '</p>
                                                                            ' . nl2br($owner_attn) . nl2br($owner_address) . '<br/>
                                                                                ' . $owner_city_state_zip . '<br/></td>
                            </tr>
                        </tbody>
                    </table>
                    </td>
                    ';
                if ($o == 3) {
                    $surety_recipient_and_general_contractor = $surety_recipient_and_general_contractor .'</tr>';
                    //After second onwer break the loop
                    $o = 0;
                    //break;
                }
                $o++;
            }
            if ($o == 3) {
                $surety_recipient_and_general_contractor = $surety_recipient_and_general_contractor . '</tr>';
            }
        }
        $surety_recipient_and_general_contractor = $surety_recipient_and_general_contractor . '</tbody></table>';
        //END: print surety_rec & GC in one line



                $is_bond_of_claim = '<table><tbody>';
                $o = 1;
                if (isset($recipients_is_bond_of_claim) && count($recipients_is_bond_of_claim) > 0) {//dd($recipients_is_bond_of_claim);
                    foreach ($recipients_is_bond_of_claim AS $k => $each_recipient) {
                        //Surety(Bond Company)
                        if ($each_recipient->category_id == 9) {

                            $bond_of_claim_firm_mailing = $each_recipient->tracking_number;

                            $bond_of_claim_name = $each_recipient->name;
                            $each_recipient->attn = str_lreplace($each_recipient->attn);
                            $bond_of_claim_attn = strtoupper($each_recipient->attn) != "" ? " " . strtoupper($each_recipient->attn) . "<br/>" : "";
                           
                            // $bond_of_claim_attn = $each_recipient->attn != "" ? " " . $each_recipient->attn . "<br/>" : "";
                            //$bond_of_claim_address = $each_recipient->address;
                            $bond_of_claim_address = (!empty($each_recipient->address)) ? $each_recipient->address : $each_recipient->ad;
                            $bond_of_claim_address .= ((!empty($each_recipient->address2)) ? '<br />' : '').$each_recipient->address2;
                            $bond_of_claim_address .= ((!empty($each_recipient->address3)) ? '<br />' : '').$each_recipient->address3;

                            $state = "";
                            if (isset($each_recipient->state_id) && $each_recipient->state_id != null) {
                                $state_name = \App\Models\State::find($each_recipient->state_id);
                                $state = $state_name->name;
                            }
                            $city = "";
                            if (isset($each_recipient->city_id) && $each_recipient->city_id != null) {
                                $city_name = \App\Models\City::find($each_recipient->city_id);
                                $city = $city_name->name;
                            }
                            if ($each_recipient->city_id != NULL || $each_recipient->state_id != NULL) {
                                $bond_of_claim_city_state_zip = $city . ' ' . $state . ' ' . $each_recipient->zip;
                            } else {
                                $bond_of_claim_city_state_zip = '';
                            }
                            //Start first bond_of_claim 
                            if ($o == 1) {
                                $is_bond_of_claim = $is_bond_of_claim . '<tr>';
                            }
                            $is_bond_of_claim = $is_bond_of_claim . '<td>
                                <table scope="col">
                                    <tbody>
                                        <tr>
                                            <td style="padding: 0 2px;word-wrap:break-word; word-break:break-all;font-size:10px; line-height: 10px;">' . $bond_of_claim_firm_mailing . '<br/>
                                                                                    <p style="width: 200px; margin-bottom: 0;margin-top: 0;font-size:10px; line-height: 10px;">' . $bond_of_claim_name . '</p>
                                                                                        ' . $bond_of_claim_attn . $bond_of_claim_address . '<br/>
                                                                                            ' . $bond_of_claim_city_state_zip . '<br/></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>';
                            if ($o == 2) {
                                $is_bond_of_claim = $is_bond_of_claim . '<tr>';
                                //After second onwer break the loop
                                break;
                                $o = 1;
                            }
                            $o++;
                        }
                    }
                }
                $is_bond_of_claim = $is_bond_of_claim . '</tbody></table>';
                          $general_contractor = '<table><tbody>';
                $o = 1;
                if (isset($recipients) && count($recipients) > 0) {
                    foreach ($recipients AS $k => $each_recipient) {
                        //General Contractor
                        if ($each_recipient->category_id == 2) {

                            $bond_of_claim_firm_mailing = $each_recipient->tracking_number;

                            $bond_of_claim_name = $each_recipient->name;
                            $each_recipient->attn = str_lreplace($each_recipient->attn);
                            $bond_of_claim_attn = strtoupper($each_recipient->attn) != "" ? " " . strtoupper($each_recipient->attn) . "<br/>" : "";
                           
                            // $bond_of_claim_attn = $each_recipient->attn != "" ? " " . $each_recipient->attn . "<br/>" : "";
                           // $bond_of_claim_address = $each_recipient->address;
                            $bond_of_claim_address = (!empty($each_recipient->address)) ? $each_recipient->address : $each_recipient->ad;
                            $bond_of_claim_address .= ((!empty($each_recipient->address2)) ? '<br />' : '').$each_recipient->address2;
                            $bond_of_claim_address .= ((!empty($each_recipient->address3)) ? '<br />' : '').$each_recipient->address3;
                            

                            $state = "";
                            if (isset($each_recipient->state_id) && $each_recipient->state_id != null) {
                                $state_name = \App\Models\State::find($each_recipient->state_id);
                                $state = $state_name->name;
                            }
                            $city = "";
                            if (isset($each_recipient->city_id) && $each_recipient->city_id != null) {
                                $city_name = \App\Models\City::find($each_recipient->city_id);
                                $city = $city_name->name;
                            }
                            if ($each_recipient->city_id != NULL || $each_recipient->state_id != NULL) {
                                $bond_of_claim_city_state_zip = $city . ' ' . $state . ' ' . $each_recipient->zip;
                            } else {
                                $bond_of_claim_city_state_zip = '';
                            }
                            //Start first bond_of_claim 
                            if ($o == 1) {
                                $general_contractor = $general_contractor . '<tr>';
                            }
                            $general_contractor = $general_contractor . '<td>
                                    <table scope="col">
                                        <tbody>
                                            <tr>
                                            <td style="padding: 0 2px;word-wrap:break-word; word-break:break-all;font-size:10px; line-height: 10px;">' . $bond_of_claim_firm_mailing . '<br/>
                                            <p style="width: 200px; margin-bottom: 0;margin-top: 0;font-size:10px; line-height: 10px;">' . $bond_of_claim_name . '</p>' 
                                            . $bond_of_claim_attn . $bond_of_claim_address . '<br/>' 
                                            . $bond_of_claim_city_state_zip . '<br/></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>';
                            if ($o == 3) {
                                $general_contractor = $general_contractor . '<tr>';
                                //After second onwer break the loop
                                // break;
                                $o = 1;
                            }
                            $o++;
                        }//end

                    }
                }
                $general_contractor = $general_contractor . '</tbody></table>';

                if (isset($contractor_name) && !empty($contractor_name)) {
                    $contractor_name = ',that the lienor served copies of the notice on the contractor,' . $contractor_name;
                }


                if (isset($sub_contractor_name) && !empty($sub_contractor_name)) {
                    $sub_contractor_name = 'and on the Sub Contractor';
                }
                if (isset($sub_sub_contractor_name) && !empty($sub_sub_contractor_name)) {
                    $sub_sub_contractor_name = 'and on the Sub Sub-Contractor';
                }
                if (isset($work_order_type) && $work_order_type == 'cyo') {

                    $recipients_owners = \App\Models\Cyo_WorkOrder_Recipient::leftjoin('cyo__stamps_labels', 'cyo__stamps_labels.recipient_id', '=', 'cyo__work_order__recipients.id')
                    ->where('work_order_id', '=', $work_order_id)
                    ->whereIn('category_id',['1','22'])->select('name','category_id')->get()->toArray();
                } else {
                    $recipients_owners = \App\Models\Recipient::leftjoin('stamps_label', 'stamps_label.recipient_id', '=', 'recipients.id')
                            ->where('work_order_id', '=', $work_order_id)
                            ->whereIn('category_id',['1','22'])->select('name','category_id')->get()->toArray();
                }
                $recipient_owners = "";
                if (isset($recipients_owners) && count($recipients_owners) > 0) {
                    foreach ($recipients_owners AS $k => $each_recipient) {
                        if(count($recipients_owners)==1){
                            $recipient_owners = $each_recipient['name'];
                        }else if((count($recipients_owners)-1)==$k){
                            $recipient_owners = $recipient_owners . ' and ' .$each_recipient['name']; 
                        }else if($k == (count($recipients_owners)-2)){
                        $recipient_owners = $recipient_owners .' '.$each_recipient['name']; 
                        }else{
                            $recipient_owners = $recipient_owners .$each_recipient['name'].' , '; 
                        }
                        }                    
                }

                
                $recipient_general_contractor = "";
                if (isset($recipient_contractors) && count($recipient_contractors) > 0) {
                    foreach ($recipient_contractors AS $k => $each_recipient) {
                        if(count($recipient_contractors)==1){
                            $recipient_general_contractor = $each_recipient['name'];
                        }else if((count($recipient_contractors)-1)==$k){
                            $recipient_general_contractor = $recipient_general_contractor . ' and ' .$each_recipient['name']; 
                        }else if($k == (count($recipient_contractors)-2)){
                        $recipient_general_contractor = $recipient_general_contractor .' '.$each_recipient['name']; 
                        }else{
                            $recipient_general_contractor = $recipient_general_contractor .$each_recipient['name'].' , '; 
                        }                  
                    }
                }
                if (isset($work_order_type) && $work_order_type == 'cyo') {

                    $recipient_sub_contractors = \App\Models\Cyo_WorkOrder_Recipient::leftjoin('cyo__stamps_labels', 'cyo__stamps_labels.recipient_id', '=', 'cyo__work_order__recipients.id')
                            ->where('work_order_id', '=', $work_order_id)
                            ->where('category_id','3')->select('name','category_id')->get()->toArray();
                } else {
                    $recipient_sub_contractors = \App\Models\Recipient::leftjoin('stamps_label', 'stamps_label.recipient_id', '=', 'recipients.id')
                            ->where('work_order_id', '=', $work_order_id)
                            ->where('category_id','3')->select('name','category_id')->get()->toArray();
                }
                $recipient_sub_contractor = "";
                if (isset($recipient_sub_contractors) && count($recipient_sub_contractors) > 0) {
                    foreach ($recipient_sub_contractors AS $k => $each_recipient) {
                        if(count($recipient_sub_contractors)==1){
                            $recipient_sub_contractor = $each_recipient['name'];
                        }else if((count($recipient_sub_contractors)-1)==$k){
                            $recipient_sub_contractor = $recipient_sub_contractor . ' and ' .$each_recipient['name']; 
                        }else if($k == (count($recipient_sub_contractors)-2)){
                        $recipient_sub_contractor = $recipient_sub_contractor .' '.$each_recipient['name']; 
                        }else{
                            $recipient_sub_contractor = $recipient_sub_contractor .$each_recipient['name'].' , '; 
                        }                  
                    }
                }
                if (isset($work_order_type) && $work_order_type == 'cyo') {

                    $recipient_sub_sub_contractors = \App\Models\Cyo_WorkOrder_Recipient::leftjoin('cyo__stamps_labels', 'cyo__stamps_labels.recipient_id', '=', 'cyo__work_order__recipients.id')
                            ->where('work_order_id', '=', $work_order_id)
                            ->where('category_id','4')->select('name','category_id')->get()->toArray();
                } else {
                    $recipient_sub_sub_contractors = \App\Models\Recipient::leftjoin('stamps_label', 'stamps_label.recipient_id', '=', 'recipients.id')
                            ->where('work_order_id', '=', $work_order_id)
                            ->where('category_id','4')->select('name','category_id')->get()->toArray();
                }
                $recipient_sub_sub_contractor = "";
                if (isset($recipient_sub_sub_contractors) && count($recipient_sub_sub_contractors) > 0) {
                    foreach ($recipient_sub_sub_contractors AS $k => $each_recipient) {
                        if(count($recipient_sub_sub_contractors)==1){
                            $recipient_sub_sub_contractor = $each_recipient['name'];
                        }else if((count($recipient_sub_sub_contractors)-1)==$k){
                            $recipient_sub_sub_contractor = $recipient_sub_sub_contractor . ' and ' .$each_recipient['name']; 
                        }else if($k == (count($recipient_sub_sub_contractors)-2)){
                        $recipient_sub_sub_contractor = $recipient_sub_sub_contractor .' '.$each_recipient['name']; 
                        }else{
                            $recipient_sub_sub_contractor = $recipient_sub_sub_contractor .$each_recipient['name'].' , '; 
                        }                  
                    }
                }
                if (isset($work_order_type) && $work_order_type == 'cyo') {

                    $recipient_contractors = \App\Models\Cyo_WorkOrder_Recipient::leftjoin('cyo__stamps_labels', 'cyo__stamps_labels.recipient_id', '=', 'cyo__work_order__recipients.id')
                            ->where('work_order_id', '=', $parent_work_order)
                            ->where('category_id','21')->select('name','category_id')->get()->toArray();
                } else {
                    $recipient_contractors = \App\Models\Recipient::leftjoin('stamps_label', 'stamps_label.recipient_id', '=', 'recipients.id')
                            ->where('work_order_id', '=', $parent_work_order)
                            ->where('category_id','21')->select('name','category_id')->get()->toArray();
                }
        $recipient_contracted_by_name = "";
        if (isset($recipient_contractors) && count($recipient_contractors) > 0) {
            foreach ($recipient_contractors AS $k => $each_recipient) {
                if(count($recipient_contractors)==1){
                    $recipient_contracted_by_name = $each_recipient['name'];
                }else if((count($recipient_contractors)-1)==$k){
                    $recipient_contracted_by_name = $recipient_contracted_by_name . ' and ' .$each_recipient['name']; 
                }else if($k == (count($recipient_contractors)-2)){
                $recipient_contracted_by_name = $recipient_contracted_by_name .' '.$each_recipient['name']; 
                }else{
                    $recipient_contracted_by_name = $recipient_contracted_by_name .$each_recipient['name'].' , '; 
                }                  
            }
        }
        $final_col_owners = "";
        if(!empty($owner_received_nto_date) && !empty($owner_received_type_mailing)){
          $final_col_owners = $recipient_owners;
        }else{
          $final_col_owners = $project_owner;
        }
        $final_col_content = "";
        if(empty($owner_received_nto_date) && empty($owner_received_type_mailing) && empty($general_contractor_received_nto_date) && empty($general_contractor_received_type_mailing) &&  empty($sub_subcontractor_received_nto_date) && empty($sub_subcontractor_received_type_mailing) && empty($subcontractor_received_nto_date) && empty($subcontractor_received_type_mailing) && empty($contracted_by_received_nto_date) && empty($contracted_by_received_type_mailing)){
          $final_col_content = "";
        }else if(!empty($owner_received_nto_date) || !empty($owner_received_type_mailing) || !empty($general_contractor_received_nto_date) || !empty($general_contractor_received_type_mailing) ||  !empty($sub_subcontractor_received_nto_date) || !empty($sub_subcontractor_received_type_mailing) || !empty($subcontractor_received_nto_date) || !empty($sub_subcontractor_received_type_mailing) || !empty($contracted_by_received_nto_date) || !empty($contracted_by_received_type_mailing)){

            if(!empty($owner_received_nto_date) && !empty($owner_received_type_mailing)){
              $final_col_content .= 'and (if the lien is claimed by one not in privity with the owner) that the lienor served his notice to owner  on ' .$owner_received_nto_date.' BY '.$owner_received_type_mailing;
              
            } 

            if(!empty($final_col_content) && !empty($general_contractor_received_nto_date) && !empty($general_contractor_received_type_mailing) ){
              $final_col_content .= ', ';
            }else if(!empty($final_col_content) && !empty($contracted_by_received_nto_date) && !empty($contracted_by_received_type_mailing) && empty($general_contractor_received_nto_date) && empty($general_contractor_received_type_mailing) && !empty($owner_received_nto_date) && !empty($owner_received_type_mailing)){
              $final_col_content .= ', ';
            }
           
            if(!empty($general_contractor_received_nto_date) && !empty($general_contractor_received_type_mailing) ){
            $final_col_content .= (empty($final_col_content) ? ', ' : '').'(if required) that the lienor served copies of the notice on the Contractor '.$recipient_general_contractor.' on '.$general_contractor_received_nto_date.' BY '.$general_contractor_received_type_mailing;
                
            }else if(empty($general_contractor_received_nto_date) && empty($general_contractor_received_type_mailing) && !empty($contracted_by_received_nto_date) && !empty($contracted_by_received_type_mailing) && !empty($owner_received_nto_date) && !empty($owner_received_type_mailing)){
               $final_col_content .= (empty($final_col_content) ? ', ' : '').'(if required) that the lienor served copies of the notice on the Contractor '.$recipient_contracted_by_name.' on '.$contracted_by_received_nto_date.' BY '.$contracted_by_received_type_mailing;
            }
           
           /* if(!empty($final_col_content) && !empty($contracted_by_received_nto_date) && !empty($contracted_by_received_type_mailing)){
              $final_col_content .= ', ';
            }
          
            if(!empty($contracted_by_received_nto_date) && !empty($contracted_by_received_type_mailing) ){
                $final_col_content .= (empty($final_col_content) ? ', ' : '').' the Contracted By '.$recipient_contracted_by_name.' on '.$contracted_by_received_nto_date.' BY '.$contracted_by_received_type_mailing;
               
            } */
           
            if(!empty($final_col_content) && !empty($subcontractor_received_nto_date) && !empty($subcontractor_received_type_mailing)){
              $final_col_content .= ' and ';
            }else if(!empty($final_col_content) && empty($subcontractor_received_nto_date) && empty($subcontractor_received_type_mailing) && !empty($contracted_by_received_nto_date) && !empty($contracted_by_received_type_mailing) && !empty($general_contractor_received_nto_date) && !empty($general_contractor_received_type_mailing) && !empty($owner_received_nto_date) && !empty($owner_received_type_mailing)){
              $final_col_content .= ' and ';
            }

            if(!empty($subcontractor_received_nto_date) && !empty($subcontractor_received_type_mailing)){
              $final_col_content .= (empty($final_col_content) ? ', ' : '').'the Sub Contractor '.$recipient_sub_contractor.' on '.$subcontractor_received_nto_date.' BY '.$subcontractor_received_type_mailing;
            }else if(empty($subcontractor_received_nto_date) && empty($subcontractor_received_type_mailing)&& !empty($contracted_by_received_nto_date) && !empty($contracted_by_received_type_mailing) && !empty($general_contractor_received_nto_date) && !empty($general_contractor_received_type_mailing) && !empty($owner_received_nto_date) && !empty($owner_received_type_mailing)){
              $final_col_content .= (empty($final_col_content) ? ', ' : '').'the Sub Contractor '.$recipient_contracted_by_name.' on '.$contracted_by_received_nto_date.' BY '.$contracted_by_received_type_mailing;
            }
            
            if(!empty($final_col_content) && !empty($sub_subcontractor_received_nto_date) && !empty($sub_subcontractor_received_type_mailing)){
              $final_col_content .= ', also';
            }else if(!empty($final_col_content) && empty($sub_subcontractor_received_nto_date) && empty($sub_subcontractor_received_type_mailing) && !empty($contracted_by_received_nto_date) && !empty($contracted_by_received_type_mailing) && !empty($general_contractor_received_nto_date) && !empty($general_contractor_received_type_mailing) && !empty($subcontractor_received_nto_date) && !empty($subcontractor_received_type_mailing) && !empty($owner_received_nto_date) && !empty($owner_received_type_mailing)){
               $final_col_content .= ', also';
            }
            
             if(!empty($sub_subcontractor_received_nto_date) && !empty($sub_subcontractor_received_type_mailing)){
                    
                   $final_col_content .=  (empty($final_col_content) ? ', ' : '').' a copy to Sub-Sub Contractor, '.$recipient_sub_sub_contractor.' on '.$sub_subcontractor_received_nto_date.', BY '.$sub_subcontractor_received_type_mailing;
              }else if(empty($sub_subcontractor_received_nto_date) && empty($sub_subcontractor_received_type_mailing) && !empty($contracted_by_received_nto_date) && !empty($contracted_by_received_type_mailing) && !empty($subcontractor_received_nto_date) && !empty($subcontractor_received_type_mailing) && !empty($general_contractor_received_nto_date) && !empty($general_contractor_received_type_mailing) && !empty($owner_received_nto_date) && !empty($owner_received_type_mailing)){
                   $final_col_content .=  (empty($final_col_content) ? ', ' : '').' a copy to Sub-Sub Contractor, '.$recipient_contracted_by_name.' on '.$contracted_by_received_nto_date.', BY '.$contracted_by_received_type_mailing;
              }
        }
       
       
        /** *********Recipients Owner Data**************** */
        //$printing_date = date('F d,Y');
        if(isset($work_order->completed_at)){
            $printing_date = date('F d, Y', strtotime($work_order->completed_at));
        }else{
            $printing_date = date('F d, Y');
        }
        //Printing Day
        $printing_day = date('d'); //October 2018
        //Printing Month
        $printing_month = date('F Y');
        $logo = public_path() . '/img/logo.png';
        //   $logo = 'http://noticetoowner.php-dev.in/public/img/logo.png';
        // dd($data);
        $date_request = $data['date_request'];
        $year = explode('-', $date_request);
        $year = $year[2];
        $customer_agent ='CIRA RANGEL';

        $static_array_var = ['{{printing_date}}', '{{owners}}','{{owners_with_label_property_owner}}','{{owners_with_label_to_property_owner}}', '{{owner_name}}', '{{your_job_reference_no}}', '{{folio}}', '{{bond}}', '{{noc}}', '{{permit}}', '{{company_name}}', '{{company_address}}', '{{company_city_state_zip}}', '{{logo}}', '{{printing_day}}', '{{printing_month}}', '{{contractor_name}}', '{{sub_contractor_name}}', '{{sub_sub_contractor_name}}', '{{title}}', '{{year}}', '{{recorded_day}}', '{{recorded_month}}', '{{state}}', '{{contracted_by}}', '{{legal_description}}', '{{bond_claim_number}}', '{{project_name}}', '{{company_city}}', '{{company_state}}','{{rescind_work_order}}','{{amendment}}', '{{is_bond_of_claim}}', '{{general_contractor}}','{{project_address}}','{{recipient_owners}}','{{sub_contractor}}','{{contracted_by_recipient}}','{{surety_recipient}}','{{recipient_sub_sub_contractor}}','{{recipient_sub_contractor}}','{{recipient_general_contractor}}','{{final_col_content}}','{{recipient_contracted_by_name}}','{{final_col_owners}}','{{customer_agent}}', '{{surety_recipient_and_general_contractor}}'];
        
        
        $static_array_replace = [$printing_date, $owners,$owners,$owners, $owner_name, $your_job_reference_no,
                $folio, $bond, $noc, $permit, $company_name, $company_address, $company_city_state_zip,
                $logo, $printing_day, $printing_month, $contractor_name,
                $sub_contractor_name, $sub_sub_contractor_name, $title, $year, $recorded_day, $recorded_month,
                $noticestate, $contracted_by, $legal_description, $bond_claim_number, $project_name, $company_city, $company_state,$rescind_work_order,$amendment,$is_bond_of_claim,$general_contractor,$project_address,$recipient_owners,$sub_contractor,$contracted_by_recipient,$surety_recipient,$recipient_sub_sub_contractor,$recipient_sub_contractor,$recipient_general_contractor,$final_col_content,$recipient_contracted_by_name,$final_col_owners,$customer_agent, $surety_recipient_and_general_contractor];
        
        //Merge static and dynamci array
        $variable_arr = array_merge($arr_variable, $static_array_var);
        $replace_arr = array_merge($arr_replace, $static_array_replace);
        //This is the first page content
        $content = str_replace($variable_arr, $replace_arr, $content);

        //template content
        //echo($content);exit;
        $final_content = '';
        /*
            * Fectch all the recipients
            */
            /*->whereOr('work_order_id', '=', $work_order_id)->where('stamps_label.type_of_label','=',$request->type_of_mailing)*/
        
        if (isset($request->type_work_order) && $request->type_work_order == 'cyo') {
            if(isset($request->type_of_mailing) && !empty($request->type_of_mailing)){
                if ($notice_type->is_bond_of_claim) {
                    $all_recipients1 = \App\Models\Cyo_WorkOrder_Recipient::leftjoin('cyo__stamps_labels', 'cyo__stamps_labels.recipient_id', '=', 'cyo__work_order__recipients.id')
                            ->leftjoin('cyo__usps_addresses', 'cyo__usps_addresses.recipient_id', '=', 'cyo__work_order__recipients.id')
                            ->where('work_order_id', '=', $work_order_id)->where('category_id', '9')
                            ->whereIn('cyo__stamps_labels.type_of_label',$type_of_mailing )
                            ->select('cyo__work_order__recipients.address as ad','cyo__work_order__recipients.city_id','cyo__work_order__recipients.state_id','cyo__work_order__recipients.zip','cyo__work_order__recipients.attn','cyo__work_order__recipients.name', 'cyo__usps_addresses.*','cyo__stamps_labels.*')
                            ->get()->toArray();
                    $all_recipients2 = \App\Models\Cyo_WorkOrder_Recipient::leftjoin('cyo__stamps_labels', 'cyo__stamps_labels.recipient_id', '=', 'cyo__work_order__recipients.id')
                            ->leftjoin('cyo__usps_addresses', 'cyo__usps_addresses.recipient_id', '=', 'cyo__work_order__recipients.id')
                            ->where('work_order_id', '=', $work_order_id)->where('category_id', '9')
                            ->whereIn('cyo__stamps_labels.type_of_mailing',$type_of_mailing )
                            ->select('cyo__work_order__recipients.address as ad','cyo__work_order__recipients.city_id','cyo__work_order__recipients.state_id','cyo__work_order__recipients.zip','cyo__work_order__recipients.attn','cyo__work_order__recipients.name', 'cyo__usps_addresses.*','cyo__stamps_labels.*')
                            ->get()->toArray();
                    $all_recipients = array_merge($all_recipients1,$all_recipients2);
                }else{
                    $all_recipients1 = \App\Models\Cyo_WorkOrder_Recipient::leftjoin('cyo__stamps_labels', 'cyo__stamps_labels.recipient_id', '=', 'cyo__work_order__recipients.id')
                            ->leftjoin('cyo__usps_addresses', 'cyo__usps_addresses.recipient_id', '=', 'cyo__work_order__recipients.id')
                            ->where('work_order_id', '=', $work_order_id)
                            ->whereIn('cyo__stamps_labels.type_of_label',$type_of_mailing )
                            ->select('cyo__work_order__recipients.address as ad','cyo__work_order__recipients.city_id','cyo__work_order__recipients.state_id','cyo__work_order__recipients.zip','cyo__work_order__recipients.attn','cyo__work_order__recipients.name', 'cyo__usps_addresses.*','cyo__stamps_labels.*')
                            ->get()->toArray();
                    $all_recipients2 = \App\Models\Cyo_WorkOrder_Recipient::leftjoin('cyo__stamps_labels', 'cyo__stamps_labels.recipient_id', '=', 'cyo__work_order__recipients.id')
                            ->leftjoin('cyo__usps_addresses', 'cyo__usps_addresses.recipient_id', '=', 'cyo__work_order__recipients.id')
                            ->where('work_order_id', '=', $work_order_id)
                            ->whereIn('cyo__stamps_labels.type_of_mailing',$type_of_mailing )
                            ->select('cyo__work_order__recipients.address as ad','cyo__work_order__recipients.city_id','cyo__work_order__recipients.state_id','cyo__work_order__recipients.zip','cyo__work_order__recipients.attn','cyo__work_order__recipients.name', 'cyo__usps_addresses.*','cyo__stamps_labels.*')
                            ->get()->toArray();
                $all_recipients = array_merge($all_recipients1,$all_recipients2);
                }

            }else{
                if ($notice_type->is_bond_of_claim) {
                    $all_recipients = \App\Models\Cyo_WorkOrder_Recipient::leftjoin('cyo__stamps_labels', 'cyo__stamps_labels.recipient_id', '=', 'cyo__work_order__recipients.id')
                            ->leftjoin('cyo__usps_addresses', 'cyo__usps_addresses.recipient_id', '=', 'cyo__work_order__recipients.id')
                            ->where('work_order_id', '=', $work_order_id)->where('category_id', '9')
                            ->select('cyo__work_order__recipients.address as ad','cyo__work_order__recipients.city_id','cyo__work_order__recipients.state_id','cyo__work_order__recipients.zip','cyo__work_order__recipients.attn','cyo__work_order__recipients.name', 'cyo__usps_addresses.*','cyo__stamps_labels.*')
                            ->get()->toArray();
                } else {
                    $all_recipients = \App\Models\Cyo_WorkOrder_Recipient::leftjoin('cyo__stamps_labels', 'cyo__stamps_labels.recipient_id', '=', 'cyo__work_order__recipients.id')
                            ->leftjoin('cyo__usps_addresses', 'cyo__usps_addresses.recipient_id', '=', 'cyo__work_order__recipients.id')
                            ->where('work_order_id', '=', $work_order_id)
                            ->select('cyo__work_order__recipients.address as ad','cyo__work_order__recipients.city_id','cyo__work_order__recipients.state_id','cyo__work_order__recipients.zip','cyo__work_order__recipients.attn','cyo__work_order__recipients.name', 'cyo__usps_addresses.*','cyo__stamps_labels.*')
                            ->get()->toArray();
                }
            }   
        } else {
            if(isset($request->type_of_mailing) && !empty($request->type_of_mailing)){
                if ($notice_type->is_bond_of_claim) {
                    $all_recipients1 = \App\Models\Recipient::leftjoin('stamps_label', 'stamps_label.recipient_id', '=', 'recipients.id')
                            ->leftjoin('usps_address', 'usps_address.recipient_id', '=', 'recipients.id')
                            ->where('work_order_id', '=', $work_order_id)
                            ->whereIn('stamps_label.type_of_label',$type_of_mailing)
                            ->where('category_id', '9')
                            ->select('recipients.address as ad','recipients.city_id','recipients.state_id','recipients.zip','recipients.attn','recipients.name', 'usps_address.*','stamps_label.*')
                            ->get()->toArray();
                    $all_recipients2 = \App\Models\Recipient::leftjoin('stamps_label', 'stamps_label.recipient_id', '=', 'recipients.id')
                            ->leftjoin('usps_address', 'usps_address.recipient_id', '=', 'recipients.id')
                            ->where('work_order_id', '=', $work_order_id)
                            ->whereIn('stamps_label.type_of_mailing',$type_of_mailing)
                            ->where('category_id', '9')
                            ->select('recipients.address as ad','recipients.city_id','recipients.state_id','recipients.zip','recipients.attn','recipients.name', 'usps_address.*','stamps_label.*')
                            ->get()->toArray();
                    $all_recipients = array_merge($all_recipients1,$all_recipients2);

                } else {
                    $all_recipients1 = \App\Models\Recipient::leftjoin('stamps_label', 'stamps_label.recipient_id', '=', 'recipients.id')
                            ->leftjoin('usps_address', 'usps_address.recipient_id', '=', 'recipients.id')
                            ->where('work_order_id', '=', $work_order_id)
                            ->whereIn('stamps_label.type_of_label',$type_of_mailing )
                            ->select('recipients.address as ad','recipients.city_id','recipients.state_id','recipients.zip','recipients.attn','recipients.name', 'usps_address.*','stamps_label.*')
                            ->get()->toArray();
                    $all_recipients2 = \App\Models\Recipient::leftjoin('stamps_label', 'stamps_label.recipient_id', '=', 'recipients.id')
                            ->leftjoin('usps_address', 'usps_address.recipient_id', '=', 'recipients.id')
                            ->where('work_order_id', '=', $work_order_id)
                            ->whereIn('stamps_label.type_of_mailing',$type_of_mailing)
                            ->select('recipients.address as ad','recipients.city_id','recipients.state_id','recipients.zip','recipients.attn','recipients.name', 'usps_address.*','stamps_label.*')
                            ->get()->toArray();
                    $all_recipients = array_merge($all_recipients1,$all_recipients2);
                }
            }else{
                if ($notice_type->is_bond_of_claim) {
                    $all_recipients = \App\Models\Recipient::leftjoin('stamps_label', 'stamps_label.recipient_id', '=', 'recipients.id')
                            ->leftjoin('usps_address', 'usps_address.recipient_id', '=', 'recipients.id')
                            ->where('work_order_id', '=', $work_order_id)->where('category_id', '9')
                            ->select('recipients.address as ad','recipients.city_id','recipients.state_id','recipients.zip','recipients.attn','recipients.name', 'usps_address.*','stamps_label.*')
                            ->get()->toArray();
                } else {
                    $all_recipients = \App\Models\Recipient::leftjoin('stamps_label', 'stamps_label.recipient_id', '=', 'recipients.id')
                            ->leftjoin('usps_address', 'usps_address.recipient_id', '=', 'recipients.id')
                            ->where('work_order_id', '=', $work_order_id)
                            //->whereNotIn('category_id', ['9', '2']) //skip GC & Surety bond as they are already printed on top
                            ->select('recipients.address as ad','recipients.city_id','recipients.state_id','recipients.zip','recipients.attn','recipients.name', 'usps_address.*','stamps_label.*')
                            ->get()->toArray();
                }
            }
        }
        
        $allrecipientfinalcontent = '<table style="width: 700px;font-size: 11px;padding:0 7px;table-layout: fixed;page-break-after:always;margin: 0 auto;" cellpadding="0">';
        $master_notice_id = [config('constants.MASTER_NOTICE')['BCOL']['ID'],config('constants.MASTER_NOTICE')['SBC']['ID'],config('constants.MASTER_NOTICE')['WRL']['ID'],config('constants.MASTER_NOTICE')['NPN']['ID']];

        if(strpos($content,'{{firm_with_label_noticing_party}}' )){
            $firm_label = 'NOTICING PARTY:';
            $content = str_replace('{{firm_with_label_noticing_party}}', "", $content);
        }else if(strpos($content,'{{firm_with_label_notifying_party}}' )){
            $firm_label = 'Notifying Party:';
            $content = str_replace('{{firm_with_label_notifying_party}}', "", $content);
        }else if(strpos($content,'{{firm_with_label_claimant}}' )){
            $firm_label = 'CLAIMANT:';
            $content = str_replace('{{firm_with_label_claimant}}', "", $content);
        }else{
            $firm_label = 'Firm:';
            $content = str_replace('{{firm_with_label_noticing_party}}', "", $content);
        }

        if (isset($request->type_work_order) && $request->type_work_order == 'cyo') {
            $c_name = "";
        }else{

            if(strpos($content,'CIRA RANGEL, Agent')){
                $c_name = '';
            }else{
                $c_name = 'CIRA RANGEL, Agent';
            }
        }
        
        $master_notice_auth_sign = [config('constants.MASTER_NOTICE')['BCOL']['ID'],config('constants.MASTER_NOTICE')['WRL']['ID'],config('constants.MASTER_NOTICE')['SBC']['ID']];
        if(in_array($notice_type->master_notice_id, $master_notice_auth_sign)){
            $c_name = "";
        }
        if($notice_type->type==2){
            if(isset($your_job_reference_no) && !empty($your_job_reference_no)){
            $your_job_reference_no = "W/O #".$your_job_reference_no;
            }
        }
        if(strpos($content,'{{customer_telephone}}')){
            $customer_telephone = $customer_telephone.'<br/>';
            $content = str_replace('{{customer_telephone}}', "", $content);
        }else{
            $customer_telephone = '';
        }

        if (isset($recipients) && count($recipients) > 0) {
                
            //All Recipient on main page
            /*
                * $allrecipientshtml = '' initialise to blank
                */
            $allrecipientshtml = '';
            /*
            * Then display the company details at time of registration details entered
            */

            if ($notice_type->is_claim_of_lien != 1) {
                $allrecipientshtml = $allrecipientshtml . '<tr>
                    <td colspan="3">
                        <table>
                            <tr>
                                <td style="vertical-align: top;font-size:10px; line-height: 10px;">'.$firm_label.'</td>
                                <td style="padding:0px 0;">' . $company_name . '<br />
                                    ' . $company_address . '<br />
                                    '.$customer_telephone. $company_city_state_zip . '<br/>' . $your_job_reference_no . 
                                    '<p style="font-weight: bold;font-size:10px; line-height: 10px;">'.$c_name.'</p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>';
            }

            /*
            * Initialise i=1 to display 3 recipients on each row
            */
            $i = 1;
        
            $check_close_cnt=1;

            if(!in_array($notice_type->master_notice_id, $master_notice_id)){
                foreach ($recipients AS $k => $each) {

                    if (isset($each->state_id) && $each->state_id != NULL) {
                        $each_sub_state_name = \App\Models\State::find($each->state_id);
                        $each_sub_state = $each_sub_state_name->name;
                    }
                    if (isset($each->city_id) && $each->city_id != NULL) {
                        $each_sub_city_name = \App\Models\City::find($each->city_id);
                        $each_sub_city = $each_sub_city_name->name;
                    }
                    if ($each->city_id != NULL || $each->state_id != NULL) {
                        $each_city_state_zip = "";
                        if($each->city_id!=NULL){
                        $each_city_state_zip .= $each_sub_city . ' ' ;
                        }
                        if($each->state_id!=NULL){
                        $each_city_state_zip .= $each_sub_state . ' ';
                        }
                        $each_city_state_zip .= $each->zip;
                    } else {
                        $each_city_state_zip = '';
                    }
                    /*
                    * ($each->category_id == 1 && $notice_type->type == 2) 
                    * This condition is used only for soft doument owner is not to be displayed at the bottom of first page
                    */
                    if ((!($each->category_id == 1 && $notice_type->owners_html_in_pdf == 1) && !($each->category_id == 22  && $notice_type->owners_html_in_pdf == 1))) {
                        /*
                        * Bottom of recipient not to displayed for claim of lien notice so make is_claim_of_lien = 1
                        * in notices table
                        */
                        if ($notice_type->is_claim_of_lien != 1) {

                            if($notice_type->master_notice_id==config('constants.MASTER_NOTICE')['NPN']['ID']){
                                if(!($each->category_id == 9  || $each->category_id == 21))
                                {
                                    /*
                                    * On first page at the bottom of pdf display all recipients each row contain 3 recipients
                                    */
                                    /*
                                    * If i==1 start the new row
                                    */
                                    if ($i == 1) {
                                        $check_close_cnt=1;
                                        $allrecipientshtml = $allrecipientshtml . '<tr><td ><table><tr>';
                                    }

                                    /*
                                    * each column contain recipients with tracking number
                                    */
                                    $each->attn = str_lreplace($each->attn);
                                    $all_attn = strtoupper($each->attn) != "" ? '<div style="width:200.33px;"> ' . strtoupper($each->attn) . '</div>' : "";


                                   // $address = $each->address;
                                    $address = (!empty($each_recipient->address)) ? $each_recipient->address : $each_recipient->ad;
                                    $address .= ((!empty($each->address2)) ? '<br />' : '').$each->address2;
                                    $address .= ((!empty($each->address3)) ? '<br />' : '').$each->address3;
                                        
                                    $allrecipientshtml = $allrecipientshtml . '<td style="width: 30%;padding: 1px 10px;text-align:left;font-size:10px; line-height: 10px;" scope="col">
                                    <div style="width:200.33px;">' . $each->tracking_number . '</div>
                                    <div style="width:200.33px;">' . $each->name . '</div>' . nl2br($all_attn) . '
                                    <div style="width:200.33px;">' . nl2br($address) . '</div>
                                    <div style="width:200.33px;">' . $each_city_state_zip . '</div>
                                                                                
                                    </td>
                                    ';

                                    /*
                                    * check i==3 because each row display 3 recipients only
                                    * if yes i becomes 0 and then increment i and start the new row
                                    */
                                    if ($i == 3) {
                                        $allrecipientshtml = $allrecipientshtml . '</tr></table></td></tr>';
                                        $i = 0;
                                        $check_close_cnt=0;
                                    }
                                    /*
                                    * Increment i value for displayin recipients
                                    */
                                        $i++;

                                }
                            }else{
                                /*
                                 * On first page at the bottom of pdf display all recipients each row contain 3 recipients
                                 */
                                /*
                                 * If i==1 start the new row
                                 */
                                if ($i == 1) {
                                    $check_close_cnt=1;
                                    $allrecipientshtml = $allrecipientshtml . '<tr><td ><table><tr>';
                                }

                                /*
                                 * each column contain recipients with tracking number
                                 */
                                    $each->attn = str_lreplace($each->attn);
                                    $all_attn = strtoupper($each->attn) != "" ? '<div style="width:200.33px;"> ' . strtoupper($each->attn) . '</div>' : "";
                                
                                //$address = $each->address;
                                $address = (!empty($each->address)) ? $each->address : $each->ad;
                                   
                                $address .= ((!empty($each->address2)) ? '<br />' : '').$each->address2;
                                $address .= ((!empty($each->address3)) ? '<br />' : '').$each->address3;

                                $allrecipientshtml = $allrecipientshtml . '<td style="width: 30%;padding: 1px 10px;text-align:left;font-size:10px; line-height: 10px;" scope="col">
                                    <div style="width:200.33px;">' . $each->tracking_number . '</div>
                                    <div style="width:200.33px;">' . $each->name . '</div>' . nl2br($all_attn) . '
                                    <div style="width:200.33px;">' .nl2br($address) . '</div>
                                    <div style="width:200.33px;">' . $each_city_state_zip . '</div>
                                </td>';

                                /*
                                 * check i==3 because each row display 3 recipients only
                                 * if yes i becomes 0 and then increment i and start the new row
                                 */
                                if ($i == 3) {
                                    $allrecipientshtml = $allrecipientshtml . '</tr></table></td></tr>';
                                    $i = 0;
                                    $check_close_cnt=0;
                                }
                                /*
                                 * Increment i value for displayin recipients
                                 */
                                $i++;
                            }   
                                
                        }
                    }
                }
            }
            /*
                * This condition is used if recipients count is only 2 or 5 so tr loop not closing
                * check i==3 and end thr tr loop
                */
            // if ($i == 3) {
            if( $check_close_cnt){
                $allrecipientshtml .= '</tr>
                    
                            </table>
                        </td>
                                    </tr>';
            
            }
            /*
                * Concat all recipients with table data
                */
            $allrecipientfinalcontent = $allrecipientfinalcontent . $allrecipientshtml.'</table>';
            /*
            * first page contain main content and all recipients at the bottom
            *  total page of pdf = Count of recipients * 2;
            */
            // $content = $content . $allrecipientfinalcontent . '</table>';
            if( strpos($content, '{{recipients_html}}') !== false) {
                $content = str_replace('{{recipients_html}}', $allrecipientfinalcontent, $content);
                $content = $content;
            }else{
                $content = $content . $allrecipientfinalcontent;
            }
        }
        //   dd($content);
        /**
         * Below code is for to display second page if it is label or firm mail
         */
        $pdf_arr = [];
        //dd($notice_type->master_notice_id, $master_notice_id);
        $sign_document = "";
        
        //if(!in_array($notice_type->master_notice_id, $master_notice_id)){
            if (isset($all_recipients) && count($all_recipients) > 0) {
                
                //Cover page of each recipient
                $recipients_html = '';$i=0;
                foreach ($all_recipients AS $each) {
                    if($each['type_of_label'] != null){
                    if (isset($each['state_id']) && $each['state_id'] != NULL) {
                        $each_sub_state_name = \App\Models\State::find($each['state_id']);
                        $each_sub_state = $each_sub_state_name->name;
                    }
                    if (isset($each['city_id']) && $each['city_id'] != NULL) {
                        $each_sub_city_name = \App\Models\City::find($each['city_id']);
                        $each_sub_city = $each_sub_city_name->name;
                    }
                        if ($each['city_id'] != NULL || $each['state_id'] != NULL) {
                        $each_city_state_zip = "";
                        if($each['city_id']!=NULL){
                        $each_city_state_zip .= $each_sub_city . ' ' ;
                        }
                        if($each['state_id']!=NULL){
                        $each_city_state_zip .= $each_sub_state . ' ';
                        }
                        $each_city_state_zip .= $each['zip'];
                    } else {
                        $each_city_state_zip = '';
                    }

                    /*
                        * If recipient mailing type select as stamps label then url must exist
                        */
                        
                    if (!empty($each['URL'])) {
                        // $stamps_label_path = url('public/storage/stamps_labels/'.$each->tracking_number);
                        //$stamps_label_path = public_path().('/storage/stamps_labels/'.$each->tracking_number);
                    
                        if (isset($each['URL']) && $each['URL'] != NULL) {
                            $stamps_label_path = '<img src="' . $each['URL'] . '" height="350" width="570"/>';
                        } else {
                            $stamps_label_path = '';
                        }

                        $paddingTop = '540px';
                        //for RR label should print on first page partially
                        if(strpos($each['type_of_mailing'], 'Return Receipt Requested')) {
                            $paddingTop = '360px';
                        }

                        /* if (isset($each->URL) && $each->URL != NULL) {
                            $stamps_label_path = '<img src="' . $each->URL . '" height="250" width="600"/>';
                        } else {

                            $stamps_label_path = '';
                        }*/
                        /*
                            * First display logo
                            * then label
                            */

                        $recipients_html = '<table style="width: 680px;font-size: 11px;margin: 0 auto;table-layout: fixed;page-break-after:always;height:750px;position:relative;">
                            <tr>
                                <td style="vertical-align:top;padding-top: '.$paddingTop.';">
                                    <table style="width: 100%;">                        
                                        <tr>
                                            <td style="text-align: left;padding: 5px;font-size: 10px; line-height:10px;">
                                                <table>
                                                    <tr>
                                                        <td style="text-align: center; width:10%; vertical-align:top;">
                                                            <img  src="' . $logo . '" style="height: 80px;width: 80px;">
                                                        </td>
                                                        <td style="">'.$stamps_label_path.'</td>
                                                    </tr>
                                                    
                                                </table>
                                            </td>
                                        </tr>                  
                                    </table> 
                                    <table style="height:50px;position:absolute;top:86%;left:0;">
                                        <tr> 
                                            <td>' . $work_order->customer_id . "/" . $work_order_id . '</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>';
                    } else {
                        /*
                        * First display logo
                        * then tracking number and recipient all info
                        */
                         $each['attn'] = str_lreplace($each['attn']);
                        $all_res = $each['attn'] != "" ? " " . $each['attn'] . "<br/>" : "";
                        $address = (!empty($each['address'])) ? $each['address'] : $each['ad'];
                        $address .= ((!empty($each['address2'])) ? '<br />' : '').$each['address2'];
                        $address .= ((!empty($each['address3'])) ? '<br />' : '').$each['address3'];
                        $address =strtoupper($address);
                        $recipients_html = '	
                        <table style="width: 680px;font-size: 11px;margin: 0 auto;table-layout: fixed;page-break-after:always;height:750px;position:relative;">
                        <tr>
                            <td style="vertical-align:middle; padding-top: 370px;">
                                <table style="width: 100%;">
                                        <tr>
                                        <td style="text-align: left;padding: 5px;font-size: 10px; line-height: 10px;">
                                            <table style="padding-top: 20px;">
                                                <tr>
                                                    <td style="text-align: center;">
                                                        <img  src="' . $logo . '" style="height: 120px;width: 120px; padding-top: 20px;">
                                                    </td>
                                                </tr>
                                                <tr style="font-size: 12px;">
                                                                                        <td>AAA BUSINESS ASSOC.<br />
                                                                                        P.O. Box 22821<br />
                                                                                        Hialeah Florida 33002</td>
                                                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="right" style="padding: 2px 0;margin:0 20px;font-size: 17px; line-height: 17px;">
                                            <table align="right" style="width:40%;">
                                                <tr>
                                                <td style="font-size: 17px; vertical-align: bottom;padding-top: 80px;">
                                                            
                                                                                                 ' . $each['tracking_number'] . '<br/>
                                                                                                  ' . strtoupper($each['name']) . '<br/>' . nl2br($all_res) . '
                                                                                                  ' . nl2br($address) . '<br/>
                                                                                                 ' . strtoupper($each_city_state_zip) . '<br/>
                                                                                            </td>

                                                </tr>
                                            </table>
                                        </td>
                                    </tr>                     
                                </table>
                                <table style="height:50px;position:absolute;top:86%;left:0;">
                                    <tr> 
                                        <td style="vertical-align:bottom;">' . $work_order->customer_id . "/" . $work_order_id . '</td>
                                    </tr>
                                </table>
                            </td>
                        </tr></table>';
                    }
                    /**
                     * Depending on recipient count content will be displayed
                     * so content is concat with recipient
                     */
                    if(in_array($notice_type->master_notice_id,[config('constants.MASTER_NOTICE')['SOL']['ID'], config('constants.MASTER_NOTICE')['COL']['ID'],config('constants.MASTER_NOTICE')['NPN']['ID']] )){
                        if(in_array($notice_type->master_notice_id,[config('constants.MASTER_NOTICE')['SOL']['ID'], config('constants.MASTER_NOTICE')['COL']['ID']] )){
                            $type = 12;
                        }else{
                            $type = 9;
                        } 

                        if (isset($request->type_work_order) && $request->type_work_order == 'cyo') {
                            $clerk_document = \App\Models\Cyo_WorkOrder_Attachment::select('file_name')->where('work_order_id',$work_order_id)->orderBy('created_at', 'desc')->where('type',$type)->get();
                            if(!($clerk_document->isEmpty())){
                                $sign_document = $clerk_document[0]['file_name'];
                                $sign_document = public_path().'/attachment/cyo_work_order_document/'.$sign_document ;
                            }
                        }else{
                            $clerk_document = \App\Models\WorkOrderAttachment::select('file_name')->where('work_order_id',$work_order_id)->orderBy('created_at', 'desc')->where('type',$type)->get();
                            if(!($clerk_document->isEmpty())){
                                $sign_document = $clerk_document[0]['file_name'];
                                $sign_document = public_path().'/attachment/work_order_document/'.$sign_document ;
                            }
                        }
                        if(isset($sign_document) && !($sign_document =="")){
                            //if(in_array($notice_type->master_notice_id,[config('constants.MASTER_NOTICE')['SOL']['ID'], config('constants.MASTER_NOTICE')['COL']['ID']] )){
                                $recipients_html = $recipients_html;
                            // }else{
                            //     $recipients_html = $content . $recipients_html;
                            // }
                            
                        }else{
                            $recipients_html = $content . $recipients_html;
                        }
                        
                        $final_content =  $recipients_html . "</table>"; 
                        
                        $pdf = PDF::loadHTML($final_content); 

                        $file_name = $each['id'] . '_' . uniqid() . '.pdf';
                            
                        if (isset($request->type_work_order) && $request->type_work_order == 'cyo') {
                            if (!file_exists(public_path() . '/pdf/cyo_col_work_order_document')) {
                                mkdir(public_path() . '/pdf/cyo_col_work_order_document', 0777, true);
                            }
                            if (!file_exists(public_path() . '/pdf/cyo_col_work_order_document/' . $file_name)) {
                                $pdf->save(public_path() . '/pdf/cyo_col_work_order_document/' . $file_name);
                            } else {
                                unlink(public_path() . '/pdf/cyo_col_work_order_document/' . $file_name);
                                $pdf->save(public_path() . '/pdf/cyo_col_work_order_document/' . $file_name);
                            }
                            $re = chmod(public_path() . '/pdf/cyo_col_work_order_document/' . $file_name, 0777); 
                            //update file name
                            $pdf_arr[$i] = public_path()  . '/pdf/cyo_col_work_order_document/' . $file_name;
                                $i++;
                            //echo "<script>window.open('$mypdf');</script>";
                        } else {
                            if (!file_exists(public_path() . '/pdf/col_work_order_document')) {
                                mkdir(public_path() . '/pdf/col_work_order_document', 0777, true);
                            }
                            

                            if (!file_exists(public_path() . '/pdf/col_work_order_document/' . $file_name)) {
                                //$pdfMerger->duplexMerge();
                                $pdf->save(public_path() . '/pdf/col_work_order_document/' . $file_name);
                            } else {
                                unlink(public_path() . '/pdf/col_work_order_document/' . $file_name);
                                //$pdfMerger->duplexMerge();
                                $pdf->save(public_path() . '/pdf/col_work_order_document/' . $file_name);
                            }
                            $re = chmod(public_path() . '/pdf/col_work_order_document/' . $file_name, 0777); 
                            //update file name
                            if(isset($request->type_of_label) && $request->type_of_label!=''){

                            }
                            /* $work_order->file_name = $file_name;
                            $work_order->status = 6;
                            $work_order->completed_at = date("Y-m-d H:i:s");
                            $work_order->save();*/

                            $pdf_arr[$i] = public_path() . '/pdf/col_work_order_document/' . $file_name; 
                            $i++;
                            //  echo "<script>window.open('$mypdf[]');</script>";
                        }
                        
                }else{
                        $final_content .= $content . $recipients_html . "</table>";
                }
            }
        }

            //Cover page of each recipient
        //}
    }
    
            //dd($content);
            if(in_array($notice_type->master_notice_id,[config('constants.MASTER_NOTICE')['SOL']['ID'], config('constants.MASTER_NOTICE')['COL']['ID'],config('constants.MASTER_NOTICE')['NPN']['ID']] ) 
                && isset($pdf_arr) && !empty($pdf_arr)){
                $pdfMerger = PDFMerger::init(); 
                foreach ($pdf_arr as $key => $value) {
                    if(isset($sign_document) && !($sign_document=="")){
                        if (isset($request->type_work_order) && $request->type_work_order == 'cyo') {
                                $path = $sign_document ;
                        }else{
                                $path = $sign_document ;
                        }
                        if (file_exists($path)) {
                            $pdfMerger->addPDF($sign_document, 'all');
                        }
                    }
                    $pdfMerger->addPDF($value, 'all');

                }
                $pdf = $pdfMerger->merge();
            
                  //  $re = chmod(public_path() . '/pdf/combine.pdf', 0777);

                if(isset($request->type_of_label) && $request->type_of_label!='')
                {
                    $file_name = $work_order_id . '_' . uniqid() . '.pdf';
                }else if(isset($request->type_of_mailing) && $request->type_of_mailing!=''){
                    $file_name = $work_order_id . '_' . uniqid() . '.pdf';
                }
                else{
                    if ($work_order->file_name == NULL) {
                        $file_name = $work_order_id . '_' . uniqid() . '.pdf';
                    } else {
                        $file_name = $work_order->file_name;
                    }
                }

                if (isset($request->type_work_order) && $request->type_work_order == 'cyo') {
                    if (!file_exists(public_path() . '/pdf/cyo_work_order_document')) {
                        mkdir(public_path() . '/pdf/cyo_work_order_document', 0777, true);
                    }
                    if (!file_exists(public_path() . '/pdf/cyo_work_order_document/' . $file_name)) {
                       // $pdfMerger->save(public_path() .'/pdf/combine.pdf');
                        $pdfMerger->save(public_path() . '/pdf/cyo_work_order_document/' . $file_name);
                    } else {
                       // $pdfMerger->save(public_path() .'/pdf/combine.pdf');
                        unlink(public_path() . '/pdf/cyo_work_order_document/' . $file_name);
                        $pdfMerger->save(public_path() . '/pdf/cyo_work_order_document/' . $file_name);
                    }
                    //update file name
                    $work_order->file_name = $file_name;
                   /* $work_order->status = 6;
                    $work_order->completed_at = date("Y-m-d H:i:s");*/
                    $work_order->save();

                    $mypdf[] = asset('/') . 'pdf/cyo_work_order_document/' . $file_name;
                    $myprint_pdf[] = public_path() . '/pdf/cyo_work_order_document/' . $file_name;
                    //echo "<script>window.open('$mypdf');</script>";
                } else {
                    if (!file_exists(public_path() . '/pdf/work_order_document')) {
                        mkdir(public_path() . '/pdf/work_order_document', 0777, true);
                    }

                    if (!file_exists(public_path() . '/pdf/work_order_document/' . $file_name)) {
                        //$pdfMerger->duplexMerge();
                        $pdfMerger->save(public_path() . '/pdf/work_order_document/' . $file_name);
                    } else {
                        unlink(public_path() . '/pdf/work_order_document/' . $file_name);
                        //$pdfMerger->duplexMerge();
                        $pdfMerger->save(public_path() . '/pdf/work_order_document/' . $file_name);
                    }
                   $re = chmod(public_path() . '/pdf/work_order_document/' . $file_name, 0777); 
                    //dd(public_path() . '/pdf/work_order_document/' . $file_name,$re);
                    //update file name
                   if(isset($request->type_of_label) && $request->type_of_label!=''){

                   }
                    $work_order->file_name = $file_name;
                   /* $work_order->status = 6;
                    $work_order->completed_at = date("Y-m-d H:i:s");*/
                    $work_order->save();
                    $mypdf[] = asset('/') . 'pdf/work_order_document/' . $file_name;
                    $myprint_pdf[] = public_path() . '/pdf/work_order_document/' . $file_name;

                   //  echo "<script>window.open(<?php $mypdf ?);</script>";
                }
            }else{
           
                /*  $pdfMerger->addPDF("/var/www/html/notice-to-owner/public/pdf/work_order_document/2_5d316101cd6f4.pdf", '1-2', 'P');*/
                # code...
          
                $content_pdf = '';
                /**
                 * If recipient data is available
                 */
                if ($final_content != '') {
                    $content_pdf = $final_content;
                } else {
                    /*
                     * if recipient is not exist display only first page
                     * UPDATE: If no recipient then dont even show first page
                     */
                    //$content_pdf = $content;
                }
                $surety_bond = '<table><tbody>';
                $o = 1;
                if(config('constants.MASTER_NOTICE')['BCOL']['ID'] == $notice_type->master_notice_id){
                    if(strpos($content_pdf,'{{surety_bond}}')){
                        $new_content_pdf = '';
                        if (isset($recipients) && count($recipients) > 0) {//dd($recipients_is_bond_of_claim);
                            foreach ($recipients AS $k => $each_recipient) {
                                if ($each_recipient->category_id == 9) {
                                    $bond_of_claim_firm_mailing = $each_recipient->tracking_number;

                                    $bond_of_claim_name = $each_recipient->name;
                                    $each_recipient->attn = str_lreplace($each_recipient->attn);
                                    $bond_of_claim_attn = $each_recipient->attn != "" ? " " . $each_recipient->attn . "<br/>" : "";
                                    //$bond_of_claim_address = $each_recipient->address;
                                    $bond_of_claim_address = (!empty($each_recipient->address)) ? $each_recipient->address : $each_recipient->ad;
                                   
                                    $bond_of_claim_address .= ((!empty($each_recipient->address2)) ? '<br />' : '').$each_recipient->address2;
                                    $bond_of_claim_address .= ((!empty($each_recipient->address3)) ? '<br />' : '').$each_recipient->address3;

                                    $state = "";
                                    if (isset($each_recipient->state_id) && $each_recipient->state_id != null) {
                                        $state_name = \App\Models\State::find($each_recipient->state_id);
                                        $state = $state_name->name;
                                    }
                                    $city = "";
                                    if (isset($each_recipient->city_id) && $each_recipient->city_id != null) {
                                        $city_name = \App\Models\City::find($each_recipient->city_id);
                                        $city = $city_name->name;
                                    }
                                    if ($each_recipient->city_id != NULL || $each_recipient->state_id != NULL) {
                                        $bond_of_claim_city_state_zip = $city . ' ' . $state . ' ' . $each_recipient->zip;
                                    } else {
                                        $bond_of_claim_city_state_zip = '';
                                    }
                                    if ($o == 1) {
                                        $surety_bond = $surety_bond . '<tr>';
                                    }
                                    $surety_bond = $surety_bond .  '<td>
                                    <table scope="col">
                                        <tbody>
                                            <tr>
                                            <td style="padding: 0 2px;word-wrap:break-word; word-break:break-all;font-size:10px; line-height: 10px;">' . $bond_of_claim_firm_mailing . '<br/>
                                            <p style="width: 200px; margin-bottom: 0;margin-top: 0;font-size:10px; line-height: 10px;">' . $bond_of_claim_name . '</p>' 
                                            . $bond_of_claim_attn . $bond_of_claim_address . '<br/>' 
                                            . $bond_of_claim_city_state_zip . '<br/></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                        ';
                                        if ($o == 3) {
                                            $surety_recipient = $surety_recipient . '</tr>';
                                            //After second onwer break the loop
                                            //break;
                                            $o = 0;
                                        }
                                        $o++;
                                }
                            }
                            if ($o == 3) {
                                $surety_bond = $surety_bond . '</tr>';
                            }
                            $surety_bond = $surety_bond . '</tbody></table>';
                            $content =  str_replace('{{surety_bond}}',$surety_bond, $content_pdf);
                            $new_content_pdf = $new_content_pdf.$content;
                        }else{
                            $new_content_pdf = $content_pdf;
                        }
                        if($new_content_pdf==''){
                            $new_content_pdf = $content_pdf;
                        }
                        $content_pdf = $new_content_pdf;
                    }
                }
                $pdf = null;
                if(!empty($content_pdf)) {
                    $content_pdf = str_replace('<table style="width: 700px;', '<table style="width: 730px;', $content_pdf);
                    $pdf = PDF::loadHTML($content_pdf);
                }
                /*
                * If file name not null create new one and storr
                */
                if(isset($request->type_of_label) && $request->type_of_label!='')
                {
                    $file_name = $work_order_id . '_' . uniqid() . '.pdf';
                }else if(isset($request->type_of_mailing) && $request->type_of_mailing!=''){
                    $file_name = $work_order_id . '_' . uniqid() . '.pdf';
                }
                else{
                    if ($work_order->file_name == NULL) {
                        $file_name = $work_order_id . '_' . uniqid() . '.pdf';
                    } else {
                        $file_name = $work_order->file_name;
                    }
                }
                /*
                * Create new folder if not exist
                * Check file name if available delete it and create new one
                * Make status complated then whole pdf will display in customer and account manager login
                */
                if(!empty($pdf)) {
                    if (isset($request->type_work_order) && $request->type_work_order == 'cyo') {
                        if (!file_exists(public_path() . '/pdf/cyo_work_order_document')) {
                            mkdir(public_path() . '/pdf/cyo_work_order_document', 0777, true);
                        }
                        if (!file_exists(public_path() . '/pdf/cyo_work_order_document/' . $file_name)) {
                            $pdf->save(public_path() . '/pdf/cyo_work_order_document/' . $file_name);
                        } else {
                            unlink(public_path() . '/pdf/cyo_work_order_document/' . $file_name);
                            $pdf->save(public_path() . '/pdf/cyo_work_order_document/' . $file_name);
                        }
                        //update file name
                        $work_order->file_name = $file_name;
                        /*$work_order->status = 6;
                        $work_order->completed_at = date("Y-m-d H:i:s");*/
                        $work_order->save();

                        $chmod_cyo = chmod(public_path() . '/pdf/cyo_work_order_document/' . $file_name, 0777); 
                        $mypdf[] = asset('/') . 'pdf/cyo_work_order_document/' . $file_name;
                        $myprint_pdf[] = public_path() . '/pdf/cyo_work_order_document/' . $file_name;

                        //echo "<script>window.open('$mypdf');</script>";
                    } else {
                        if (!file_exists(public_path() . '/pdf/work_order_document')) {
                            mkdir(public_path() . '/pdf/work_order_document', 0777, true);
                        }

                        if (!file_exists(public_path() . '/pdf/work_order_document/' . $file_name)) {
                            //$pdfMerger->duplexMerge();
                            $pdf->save(public_path() . '/pdf/work_order_document/' . $file_name);
                        } else {
                            unlink(public_path() . '/pdf/work_order_document/' . $file_name);
                            //$pdfMerger->duplexMerge();
                            $pdf->save(public_path() . '/pdf/work_order_document/' . $file_name);
                        }
                    $re = chmod(public_path() . '/pdf/work_order_document/' . $file_name, 0777); 
                        //dd(public_path() . '/pdf/work_order_document/' . $file_name,$re);
                        //update file name
                    if(isset($request->type_of_label) && $request->type_of_label!=''){

                    }
                        $work_order->file_name = $file_name;
                        //$work_order->status = 6;
                        //$work_order->completed_at = date("Y-m-d H:i:s");
                        $work_order->save();
                        $mypdf[] = asset('/') . 'pdf/work_order_document/' . $file_name;
                        $myprint_pdf[] = public_path() . '/pdf/work_order_document/' . $file_name;

                    //  echo "<script>window.open(<?php $mypdf ?);</script>";
                    }
                }
            }
        }
    }
    
    if(empty($myprint_pdf)) {
        return response()->json(['status' => 'error', 'message' => 'No data found to generate for selected work order']);
    }
    
        $pdfMerger = PDFMerger::init();
        foreach ($myprint_pdf as $key => $value) {
            $pdfMerger->addPDF($value, 'all');

        }
        $pdfMerger->merge();
        $pdfMerger->save(public_path() .'/pdf/combine.pdf');
        $combine_pdf = asset('/').'pdf/combine.pdf';
     
        return response()->json(['status' => 'success', 'message' => $combine_pdf]);
        //return response()->json(['status' => 'success', 'message' => $mypdf]);
        //return view('account_manager.mailing.list');
    }

    public function completeWo(Request $request) {
        $print_workorder_id = [];
        if (isset($_COOKIE['select_all_mail']) && $_COOKIE['select_all_mail'] == 1) {
            $print_workorder_id = $this->getselectAllWorkorders($request);
        } elseif (isset($_COOKIE['mailing_checkbox']) && $_COOKIE['mailing_checkbox'] != "") { 
            $print_workorder_id = explode(",", $_COOKIE['mailing_checkbox']);
        } else {
            $print_workorder_id = $request->select_work_orders;
        }
        if (empty($print_workorder_id)) {
            return response()->json(['status' => 'error', 'message' => 'Please select at least one work order.']);
        }
        foreach ($print_workorder_id AS $value) {
            $work_order_id = $value;
            if($value!="" && $value!=NULL)
            {
                if (isset($request->type_work_order) && $request->type_work_order == 'cyo') 
                {
                    $work_order = \App\Models\Cyo_WorkOrders::find($work_order_id);
                    $work_order->status = 6;
                    $work_order->completed_at = date("Y-m-d H:i:s");
                    $work_order->save();
                } else 
                {
                    $work_order = WorkOrder::find($work_order_id);
                    $work_order->status = 6;
                    $work_order->completed_at = date("Y-m-d H:i:s");
                    $work_order->save();
                }
            }
        }
        
        if (isset($request->type_work_order) && $request->type_work_order == 'cyo') 
        {
            \Artisan::call("GeneratingCyoQuickbooksInvoice:generatingCyoInvoice");
        } else 
        {
            \Artisan::call("GeneratingQuickbooksInvoice:generatingInvoice");
        }
        return response()->json(['status' => 'success']);
    }

    public function printFirmMails(Request $request, $print_type) {
        $arrPrintType = ['Firm Mail', 'Manual'];
        $print_workorder_id = [];
        if (isset($_COOKIE['select_all_mail']) && $_COOKIE['select_all_mail'] == 1) {
            $print_workorder_id = $this->getselectAllWorkorders($request);
        } elseif (isset($_COOKIE['mailing_checkbox']) && $_COOKIE['mailing_checkbox'] != "") {
            $print_workorder_id = explode(",", $_COOKIE['mailing_checkbox']);
        } else {
            $print_workorder_id = $request->select_work_orders;
        }
        if (empty($print_workorder_id)) {
            return response()->json(['status' => 'error', 'message' => 'Please select at least one work order.']);
        }
        $content = "<style>html { margin: 40px 20px; }</style>";
        $first_count = 1;
        $i = 1;
        $j = 1;
        foreach ($print_workorder_id AS $value) {

            $work_order_id = $value;
            /*
             * Get all the recipient data if mailing type is firm mail
             */
            if (isset($request->type_work_order) && $request->type_work_order == 'cyo') {
                $work_order = \App\Models\Cyo_WorkOrders::find($work_order_id);
                $notice_id = $work_order->notice_id;
                //Work Order Recipients
                $recipients = \App\Models\Cyo_WorkOrder_Recipient::leftjoin('cyo__stamps_labels', 'cyo__stamps_labels.recipient_id', '=', 'cyo__work_order__recipients.id')
                        ->leftjoin('cyo__usps_addresses', 'cyo__usps_addresses.recipient_id', '=', 'cyo__work_order__recipients.id')
                        ->where('work_order_id', '=', $work_order_id)
                        ->whereIn('cyo__stamps_labels.type_of_label', $arrPrintType)
                        ->select('cyo__work_order__recipients.address as ad','cyo__work_order__recipients.city_id','cyo__work_order__recipients.state_id','cyo__work_order__recipients.zip','cyo__work_order__recipients.attn','cyo__work_order__recipients.name', 'cyo__usps_addresses.*','cyo__stamps_labels.*')
                        ->get(); 
            } else {
                $work_order = WorkOrder::find($work_order_id);
                $notice_id = $work_order->notice_id;

                //Work Order Recipients
                $recipients = \App\Models\Recipient::leftjoin('stamps_label', 'stamps_label.recipient_id', '=', 'recipients.id')
                        ->leftjoin('usps_address', 'usps_address.recipient_id', '=', 'recipients.id')
                        ->where('work_order_id', '=', $work_order_id)
                        ->whereIn('stamps_label.type_of_label', $arrPrintType)
                        ->select('recipients.address as ad','recipients.city_id','recipients.state_id','recipients.zip','recipients.attn','recipients.name', 'usps_address.*','stamps_label.*')
                        ->get();
            }
            $recipients_html = '';
            if (isset($recipients) && count($recipients) > 0 && $first_count == 1) {
                $final_content = '<table style="width: 750px;font-size: 12px;table-layout:fixed;">';
            } else {
                $final_content = '';
            }

            if (isset($recipients) && count($recipients) > 0) {
                $res_cnt = 1;
                foreach ($recipients AS $k => $each) {
                    if ($i == 1) {
                        $recipients_html .= '<tr>';
                        $add_padding = "padding:0 40px 0 0;";
                    } else if ($i == 3){
                        $add_padding = "padding:0 0 0 30px;";
                    }else {
                        $add_padding = "padding-left:15px;";
                    }



                    if (isset($each->state_id) && $each->state_id != NULL) {
                        $each_sub_state_name = \App\Models\State::find($each->state_id);
                        $each_sub_state = $each_sub_state_name->name;
                    }
                    if (isset($each->city_id) && $each->city_id != NULL) {
                        $each_sub_city_name = \App\Models\City::find($each->city_id);
                        $each_sub_city = $each_sub_city_name->name;
                    }
                    if ($each->city_id != NULL || $each->state_id != NULL) {
                            $each_city_state_zip = "";
                          if($each->city_id!=NULL){
                            $each_city_state_zip .= $each_sub_city . ' ' ;
                          }
                          if($each->state_id!=NULL){
                            $each_city_state_zip .= $each_sub_state . ' ';
                          }
                          $each_city_state_zip .= $each->zip;
                        } else {
                            $each_city_state_zip = '';
                        }
                    //$each_city_state_zip .= 'Extra text will go here for testing xtra text will go here for testing xtra text will go here for testing xtra text will go here for testing xtra text will go here for testing testing xtra text will go here for testing testing xtra text will go here for testing  testing xtra text will go here for testing testing xtra text will go here for testing ';
                    /*
                     * Display recipient info with tracking number
                     */
                    /* <!-- <tr><td style="padding:0px 0;margin:0;font-weight: normal;word-break: break-all;">' . $each->tracking_number . '</td></tr>
                      <tr><td style="padding: 0px 0;margin:0;font-weight: normal;word-break: break-all;">' . $each->name . '</td></tr>
                      <tr> <td style="padding: 0px 0;margin:0;font-weight: normal;word-break: break-all;">' . $each->address . '</td></tr>
                      <tr><td style="padding: 0px 0;margin:0;font-weight: normal;word-break: break-all;">' . $each_city_state_zip . '<td></tr>--> */

                    /*$recipients_html .= ' <td style="padding:0px 10px;vertical-align: top;word-break: break-all; height: 100px;" width="30%"><table style="width:100%;"><tr style="width:100%; " ><td style="margin:0;font-weight: normal;word-break: break-all; font-size: 10px;' . $add_padding . '">' . $each->tracking_number . '  <br/>' . $each->name . '<br/>' . $each->address . '<br/>' . $each_city_state_zip . '
                                                </td></tr></table></td>';*/

                    $recipients_html .= ' <td style="vertical-align: top;" width="31%"><div style="height: 100px;overflow: hidden;word-break: break-all; font-size: 10px; width: 100%; '.$add_padding .'">' . $each->tracking_number . '  <br/>' . $each->name . '<br/>';
                    $recipients_html .= (!empty($each->address)) ? $each->address : $each->ad;
                    $recipients_html .= (!empty($each->address2) ? '<br />' : '').$each->address2;
                    $recipients_html .= (!empty($each->address3) ? '<br />' : '').$each->address3;
                    $recipients_html .= '<br />'.$each_city_state_zip . '</div></td>';

                    //   echo (count($recipients) % 2 ) . "--J=" . $j . "--" . count($request->select_work_orders) . "<br/>";
                    if (count($recipients) % 2 == 0 && $res_cnt == count($recipients) && $j == count($request->select_work_orders)) {
                        $recipients_html .= '<td>&nbsp;</td>';
                    }

                    if ($i == 3) {
                        $recipients_html .= '</tr>';
                        $i = 0;
                    }
                    $i++;
                    $res_cnt++;
                }
                $final_content = $final_content . $recipients_html;
                $last_html = "</table>";
                $final_content = $final_content;
                $first_count++;
            }
            $print_path = 'manual_firm_mailing'; 
            $print_file = 'MM';
        
            $j++;
            if (!file_exists(public_path() . '/'.$print_path)) {
                $response = mkdir(public_path() . '/'. $print_path, 0777, true);
            }



            if ($final_content != '') {

                $content = $content . $final_content;


                /* $pdf = PDF::loadHTML($content);



                  $file_name = 'FM' . $work_order_id . '_' . uniqid() . '.pdf';
                  $pdf->save(public_path() . '/firm_mailing/' . $file_name);
                  $mypdf[] = asset('/') . 'firm_mailing/' . $file_name; */
                if(in_array($print_type, $arrPrintType)){
                    $work_order->print_firm_mail = 'Yes';
                    $work_order->save();
                }
                // echo "<script>window.open('$mypdf');</script>";
                //return $pdf->stream($file_name);
            } else {
                // $content = $content . 'No firm mails records found.';

                /* $pdf = PDF::loadHTML($content);


                  $file_name = 'FM' . $work_order_id . '_' . uniqid() . '.pdf';

                  $pdf->save(public_path() . '/firm_mailing/' . $file_name);
                  $mypdf[] = asset('/') . 'firm_mailing/' . $file_name; */

                //  echo "<script>window.open('$mypdf');</script>";
                //  return $pdf->stream($file_name);
            }
        }

        // dd($content);
        if ($content != '') {

            $content = $content;

//echo $content; die;
            $pdf = PDF::loadHTML($content)->setPaper('a4'); //->setPaper('letter', 'portrait');



            $file_name = $print_file . date("ymdhis") . '.pdf';
            $pdf->save(public_path() .'/'. $print_path.'/' . $file_name);
            $mypdf[] = asset('/') .$print_path.'/' . $file_name;

            // echo "<script>window.open('$mypdf');</script>";
            //return $pdf->stream($file_name);
        } else {
            $content = 'No firm mails records found.';

            $pdf = PDF::loadHTML($content);


            $file_name = $print_file . date("ymdhis") . '.pdf';

            $pdf->save(public_path()  .'/'.$print_path.'/' . $file_name);
            $mypdf[] = asset('/') . $print_path.'/' . $file_name;

            //  echo "<script>window.open('$mypdf');</script>";
            //  return $pdf->stream($file_name);
        }
        return response()->json(['status' => 'success', 'message' => $mypdf]);
    }

    public function testing() {
        try{
            $wsdl           = config('constants.STAMPS.WSDL');
            $integrationID  = config('constants.STAMPS.INTEGRATION_ID');
            $username       = config('constants.STAMPS.USERNAME');
            $password       = config('constants.STAMPS.PASSWORD');
            
            $stamps = new \App\Helpers\StampsHelper($wsdl, $integrationID, $username, $password);


            //verify address
            $address = ['FirstName' => 'Harsh', 'LastName'=> 'K', 'Company' =>'ONE PNC PLAZA', 'Department' => '', 'Address1' => '249 FIFTH AVENUE','City' => 'ETTRICK', 'State' => 'MD', 'ZIPCode' => '15222'];
            $clean_address = $stamps->CleanseAddress($address);
            dd($clean_address);

            //get address
            $packages = 'Letter';
            $service_type = 'US-FC';
            $get_rates_values = [
                "FromZIPCode" => env('FROM_ZIPCODE'),
                "ToZIPCode" => '33602',
                // "WeightLb" => 1,
                // "InsuredValue" => 100,
                "PackageType" => $packages,
                "ShipDate" => '2019-12-30',
                "ServiceType" => $service_type,

            ];

            /* $get_rates = $stamps->GetRates($get_rates_values);
            dd($get_rates);
            array:2 [▼
                "status" => "success"
                "result" => array:12 [▼
                    "FromZIPCode" => "33166"
                    "ToZIPCode" => "33602"
                    "Amount" => "0.5"
                    "ServiceType" => "US-FC"
                    "DeliverDays" => "2"
                    "PackageType" => "Letter"
                    "ShipDate" => "2019-12-30"
                    "DeliveryDate" => "2020-01-02"
                    "Surcharges" => []
                    "Zone" => 3
                    "RateCategory" => 1000
                    "ToState" => "FL"
                ]
                ] */


            //Setting from address for creating label
            $from_addr = [
                'FullName' => env('FROM_NAME'),
                'Address1' => env('FROM_ADDRESS'),
                'City' => env('FROM_CITY'),
                'State' => env('FROM_STATE'),
                'ZIPCode' => env('FROM_ZIPCODE')
            ];

                $to_addr = [
                    'FullName' => 'Harshad K',
                    'Address1' => 'MAIL COLD',
                    'City' => 'TAMPA',
                    'State' => 'FL',
                    'ZIPCode' => '33602',
                    'ZIPCodeAddOn' => '5854',
                    'DPB' => '',
                    'CheckDigit' => '',
                    'OverrideHash' => 'C9DOqh4m9eea4BugpyNbZ/+YjzpkZWFkYmVlZg==20200412',
                ];

               

                
                  $rate = '{"FromZIPCode":"33166","ToZIPCode":"33602","Amount":"0.5","ServiceType":"US-FC","DeliverDays":"2","PackageType":"Letter","ShipDate":"2019-12-28","DeliveryDate":"2019-12-30","Surcharges":[],"Zone":1,"RateCategory":1000,"ToState":"FL","AddOns":[{"AddOnType":"US-A-CM"},{"AddOnType":"US-A-RR"}]}';
                $rates1 = json_decode($rate, true);
                $label = $stamps->CreateIndicium($rates1, $to_addr, $from_addr);
                dd($label);
    } catch(\Exception $e) {
        dd($e->getMessage());
    }
} 

public function completewoprintpdf(Request $request)
    {
        $work_order_type=$request->work_order_type;
        $myprint_pdf=[];
        foreach($request->work_order_id as $work_order_id) {       
                if (isset($work_order_type) && $work_order_type == 'cyo') {
                    $work_order = \App\Models\Cyo_WorkOrders::find($work_order_id);
                    $notice_id = $work_order->notice_id;

                    $WorkOrderFields = \App\Models\Cyo_WorkOrder_fields::select('cyo__work_orders.parent_id', 'cyo__work_orders.created_at', 'cyo__work_orders.user_id', 'cyo__work_orders.customer_id', 'cyo__work_orders.is_rescind as rescind_work_order', 'cyo__work_orders.notice_id as notice_id', 'cyo__work_orders.status', 'cyo__work_orders.completed_at as completed_date', 'cyo__work_orders.user_id', 'cyo__work_orders.account_manager_id', 'cyo__work_order_fields.workorder_id', 'cyo__work_orders.notice_id', DB::raw("(GROUP_CONCAT(name SEPARATOR '||')) as `field_names`"), DB::raw("(GROUP_CONCAT(value SEPARATOR '||')) as `field_values`"))->join('notice_fields', 'notice_fields.id', '=', 'cyo__work_order_fields.notice_field_id')
                        ->join('cyo__work_orders', 'cyo__work_orders.id', '=', 'cyo__work_order_fields.workorder_id')
                        ->groupBy('cyo__work_order_fields.workorder_id', 'cyo__work_order_fields.notice_id')
                        ->where('cyo__work_orders.id', $work_order_id)
                        ->get()->toArray();
                } else {
                    $work_order = WorkOrder::find($work_order_id);
                    $notice_id = $work_order->notice_id;


                    //dd($request->all());

                    $WorkOrderFields = \App\Models\WorkOrderFields::select('work_orders.parent_id', 'work_orders.created_at', 'work_orders.user_id', 'work_orders.customer_id', 'work_orders.is_rescind as rescind_work_order',  'work_orders.notice_id as notice_id', 'work_orders.status', 'work_orders.user_id', 'work_orders.account_manager_id', 'work_orders.completed_at as completed_date', 'work_order_fields.workorder_id', 'work_order_fields.notice_id', DB::raw("(GROUP_CONCAT(name SEPARATOR '||')) as `field_names`"), DB::raw("(GROUP_CONCAT(value SEPARATOR '||')) as `field_values`"))->join('notice_fields', 'notice_fields.id', '=', 'work_order_fields.notice_field_id')
                        ->join('work_orders', 'work_orders.id', '=', 'work_order_fields.workorder_id')->groupBy('work_order_fields.workorder_id', 'work_order_fields.notice_id')
                        ->where('work_orders.id', $work_order_id)
                        ->get()->toArray();
                } //dd($WorkOrderFields);
                $rescind_work_order = '';
                if ($WorkOrderFields[0]['rescind_work_order'] == 1) {
                    $WorkOrderFields[0]['rescind_work_order'] = 'AMENDMENT';
                } else {
                    $WorkOrderFields[0]['rescind_work_order'] = '';
                }
                $notice_type = \App\Models\Notice::find($notice_id);
                $result = [];
                if (isset($WorkOrderFields) && !empty($WorkOrderFields)) {
                    foreach ($WorkOrderFields as $fields_data) {
                        $field_names = explode('||', $fields_data['field_names']);
                        $field_values = explode('||', $fields_data['field_values']);
                        $field_names_values = array_combine($field_names, $field_values);
                        $field_names_values['default'] = '';
                        $result[] = array_merge($fields_data, $field_names_values);
                    }
                }
                //Work Order Recipients
                if (isset($work_order_type) && $work_order_type == 'cyo') {

                    $recipients = \App\Models\Cyo_WorkOrder_Recipient::leftjoin('cyo__stamps_labels', 'cyo__stamps_labels.recipient_id', '=', 'cyo__work_order__recipients.id')
                        ->leftjoin('cyo__usps_addresses', 'cyo__usps_addresses.recipient_id', '=', 'cyo__work_order__recipients.id')
                        ->where('work_order_id', '=', $work_order_id)
                        ->select('cyo__work_order__recipients.address as ad','cyo__work_order__recipients.category_id','cyo__work_order__recipients.city_id','cyo__work_order__recipients.state_id','cyo__work_order__recipients.zip','cyo__work_order__recipients.attn','cyo__work_order__recipients.name', 'cyo__usps_addresses.*','cyo__stamps_labels.*')
                        ->get();
                    if ($notice_type->is_bond_of_claim) {
                        $recipients_is_bond_of_claim = \App\Models\Cyo_WorkOrder_Recipient::leftjoin('cyo__stamps_labels', 'cyo__stamps_labels.recipient_id', '=', 'cyo__work_order__recipients.id')
                            ->leftjoin('cyo__usps_addresses', 'cyo__usps_addresses.recipient_id', '=', 'cyo__work_order__recipients.id')
                            ->where('work_order_id', '=', $work_order_id)->where('category_id', '9')
                            ->select('cyo__work_order__recipients.address as ad','cyo__work_order__recipients.category_id','cyo__work_order__recipients.city_id','cyo__work_order__recipients.state_id','cyo__work_order__recipients.zip','cyo__work_order__recipients.attn','cyo__work_order__recipients.name', 'cyo__usps_addresses.*','cyo__stamps_labels.*')
                            ->get();
                    }
                } else {
                    $recipients = \App\Models\Recipient::leftjoin('stamps_label', 'stamps_label.recipient_id', '=', 'recipients.id')
                        ->leftjoin('usps_address', 'usps_address.recipient_id', '=', 'recipients.id')
                        ->where('work_order_id', '=', $work_order_id)
                        ->select('recipients.address as ad','recipients.city_id','recipients.category_id','recipients.state_id','recipients.zip','recipients.attn','recipients.name', 'usps_address.*','stamps_label.*')
                            ->get();
                    if ($notice_type->is_bond_of_claim) {
                        $recipients_is_bond_of_claim = \App\Models\Recipient::leftjoin('stamps_label', 'stamps_label.recipient_id', '=', 'recipients.id')
                            ->leftjoin('usps_address', 'usps_address.recipient_id', '=', 'recipients.id')
                            ->select('recipients.address as ad','recipients.city_id','recipients.category_id','recipients.state_id','recipients.zip','recipients.attn','recipients.name', 'usps_address.*','stamps_label.*')
                            ->where('work_order_id', '=', $work_order_id)->where('category_id', '9')
                            ->get();
                    }
                }
                $data = $result[0];

                $template_content = \App\Models\NoticeTemplate::where('notice_id', '=', $notice_id)->first();

                if ($template_content == null) {
                    \Session::flash('success', 'Pdf will not generate for this notice');
                    return redirect::back();
                }

                $content = $template_content->content;

                $notice_state = \App\Models\State::find($template_content->state_id);

                $noticestate = $notice_state->name;
                $arr_variable = [];
                $arr_replace = [];
                $contracted_by = '';
                $legal_description = '';
                $bond_claim_number = '';
                $your_job_reference_no = '';
                $title = '';
                $recorded_day = '';
                $recorded_month = '';
                $project_name = '';
                $sub_subcontractor_received_type_mailing = "";
                $subcontractor_received_type_mailing = "";
                $general_contractor_received_type_mailing = "";
                $owner_received_type_mailing = "";
                $owner_received_nto_date = "";
                $general_contractor_received_nto_date = "";
                $sub_subcontractor_received_nto_date = "";
                $subcontractor_received_nto_date = "";
                $contracted_by_received_type_mailing = "";
                $contracted_by_received_nto_date = "";
                $parent_work_order = "";
                $project_owner = "";
                $parent_county = "";

                if (isset($content) && $content != NULL) {
                    /*             * *********Dynamic Data**************** */
                    if (isset($data) && !empty($data)) {
                        foreach ($data as $key => $value) {
                            array_push($arr_variable, '{{' . $key . '}}');
                            if (
                                $key == 'date_request' || $key == 'enter_into_agreement' ||
                                $key == 'last_date_of_labor_service_furnished'
                            ) {
                                if (isset($value) && $value != NULL && $value != '') {
                                    $value = date('M d,Y', strtotime($value));
                                    //                          $date = \DateTime::createFromFormat("m-d-Y", $value->value);
                                    //                           
                                    //                            $date = $date->format('m-d-Y'); 
                                    //                            $value->value = $date;
                                }
                            } else if ($key == 'workorder_id') {
                                if (isset($work_order_type) && $work_order_type == 'cyo') {
                                    $value = 'CYO Work Order No:' . $value;
                                } else {
                                    $value = 'Work Order No:' . $value;
                                }
                            } else if ($key == 'parent_work_order') {
                                $parent_county = "";
                                if (isset($value) && !empty($value)) {
                                    $parent_work_order  = $value;
                                    if (isset($work_order_type) && $work_order_type == 'cyo') {
                                        $work_order_parent = \App\Models\Cyo_WorkOrders::find($value);
                                    } else {
                                        $work_order_parent = WorkOrder::find($value);
                                    }
                                    $notice_id = $work_order_parent->notice_id;
                                    $notice_field_id = \App\Models\NoticeField::where('notice_id', $notice_id)->where('name', 'county')->get();
                                    if (!empty($notice_field_id) && $notice_field_id->count()) {
                                        $notice_field_id = $notice_field_id[0]->id;
                                        $WorkOrderFields = \App\Models\WorkOrderFields::select('value')->where('workorder_id', $value)->where('notice_field_id', $notice_field_id)->get()->toArray();
                                        if (!empty($WorkOrderFields)) {
                                            $county_id = $WorkOrderFields[0]['value'];
                                            $county_name = \App\Models\City::find($county_id);
                                            $parent_county = !empty($county_name) ? $county_name->county : "";
                                        }
                                    }
                                    $value = 'Parent Work Order:' . $value;
                                }
                            } else if ($key == 'project_name') {

                                if (isset($value) && !empty($value)) {
                                    $project_name = 'Project Name : ' . $value;
                                    $value = 'Project Name : ' . $value;
                                } else {
                                    $project_name = '';
                                    $value = "";
                                }
                            } else if ($key == 'total_amount_satisfied') {
                                if (isset($value) && !empty($value)) {
                                    if (is_numeric($value)) {
                                        $value = number_format($value, 2);
                                    }
                                } else {
                                    $value = '00.00';
                                }
                            } else if ($key == 'completed_date') {
                                if (isset($value) && !empty($value)) {
                                    $value = date('d M Y', strtotime($value));
                                    $value = explode(' ', $value);
                                    $value = $value[0] . ' day of ' . $value[1] . ', ' . $value[2];
                                } else {
                                    $value = '___ day of _________, 20__';
                                }
                            } else if ($key == 'estimate_value_of_labor') {
                                if (isset($value) && !empty($value)) {
                                    if (is_numeric($value)) {
                                        $value = number_format($value, 2);
                                    }
                                } else {
                                    $value = '00.00';
                                }
                            } else if ($key == 'total_value') {
                                if (isset($value) && !empty($value)) {
                                    if (is_numeric($value)) {
                                        $value = '($' . number_format($value, 2) . ')';
                                    }
                                } else {
                                    $value = '($00.00)';
                                }
                            } else if ($key == 'unpaid_amount') {
                                if (isset($value) && !empty($value)) {
                                    if (is_numeric($value)) {
                                        $value = '($' . number_format($value, 2) . ')';
                                    }
                                } else {
                                    $value = '($00.00)';
                                }
                            } else if ($key == 'contract_price_or_estimated_value_of_labor') {
                                if (isset($value) && !empty($value)) {
                                    if (is_numeric($value)) {
                                        $value = number_format($value, 2);
                                    }
                                } else {
                                    $value = '00.00';
                                }
                            } else if ($key == 'estimate_of_amount_owed') {
                                if (isset($value) && !empty($value)) {
                                    if (is_numeric($value)) {
                                        $value = number_format($value, 2);
                                    }
                                } else {
                                    $value = '00.00';
                                }
                            } else if ($key == 'final_payment') {
                                if (isset($value) && !empty($value)) {
                                    if (is_numeric($value)) {
                                        $value = number_format($value, 2);
                                    }
                                } else {
                                    $value = '00.00';
                                }
                            } else if ($key == 'amount_due') {
                                if (isset($value) && !empty($value)) {
                                    if (is_numeric($value)) {
                                        $value = number_format($value, 2);
                                    }
                                } else {
                                    $value = '00.00';
                                }
                            } else if ($key == 'project_address') {
                                if (isset($value) && !empty($value)) {
                                    $value=str_lreplace($value);
                                    $value = strtoupper($value);
                                    $project_address = 'Project:' . $value;
                                } else {
                                    $project_address = '';
                                }
                            } else if ($key == 'additional_comment') {
                                if (isset($value) && !empty($value)) {
                                    $value =  $value;
                                } else {
                                    $value = '';
                                }
                            } else if ($key == 'county' || $key == 'notary_county' || $key == 'recorded_county') {

                                if (isset($value) && !empty($value)) {
                                    $county = \App\Models\City::select('county')->find($value);

                                    $value = $county->county;
                                }
                            } else if ($key == 'notary_state' || $key == 'recorded_state') {
                                if (isset($value) && !empty($value)) {
                                    $notary_state_name = \App\Models\State::find($value);
                                    $value = $notary_state_name->name;
                                }
                            } else if ($key == 'owner_received_type_mailing') {
                                if (isset($value) && !empty($value)) {
                                    $value = $value;
                                    $owner_received_type_mailing = $value;
                                }
                            } else if ($key == 'general_contractor_received_type_mailing') {
                                if (isset($value) && !empty($value)) {
                                    $value =  $value;
                                    $general_contractor_received_type_mailing = $value;
                                }
                            } else if ($key == 'subcontractor_received_type_mailing') {
                                if (isset($value) && !empty($value)) {
                                    $value = $value;
                                    $subcontractor_received_type_mailing = $value;
                                }
                            } else if ($key == 'sub_subcontractor_received_type_mailing') {
                                if (isset($value) && !empty($value)) {
                                    $value = $value;
                                    $sub_subcontractor_received_type_mailing = $value;
                                }
                            } else if ($key == 'contracted_by_received_type_mailing') {
                                if (isset($value) && !empty($value)) {
                                    $value = $value;
                                    $contracted_by_received_type_mailing = $value;
                                }
                            } else if ($key == 'authorise_agent') {
                                if (isset($value) && !empty($value)) {
                                    //                            $signature = \App\Models\CustomerAgent::find($value);
                                    //                            $value = $signature->first_name . ' ' . $signature->last_name;
                                    //                            $title = $signature->title;
                                    $value = $value;
                                } else {
                                    $value = "";
                                }
                            } else if ($key == 'recorded_date') {
                                if (isset($value) && !empty($value)) {
                                    $recorded_day = date('d', strtotime($value)); //October 2018
                                    //Recorded Month
                                    $recorded_month = date('F Y', strtotime($value));
                                }
                            } else if ($key == 'contracted_by') {
                                if (isset($value) && !empty($value)) {
                                    $contracted_by = $value;
                                } else {
                                    $contracted_by = '';
                                }
                            } else if ($key == 'legal_description') {
                                if (isset($value) && !empty($value)) {
                                    $legal_description = $value;
                                } else {
                                    $legal_description = '';
                                }
                            } else if ($key == 'bond_claim_number') {
                                if (isset($value) && !empty($value)) {
                                    $bond_claim_number = $value;
                                } else {
                                    $bond_claim_number = '';
                                }
                                // array_push($arr_replace, $value);
                            } else if ($key == 'your_job_reference_no') {
                                if (isset($value) && !empty($value)) {
                                    $your_job_reference_no = $value;
                                } else {
                                    $your_job_reference_no = '';
                                }
                                // array_push($arr_replace, $value);
                            } else if ($key == 'collection_for_attorneys_fees') {
                                if (isset($value) && !empty($value)) {
                                    $value = 'these charges along with interest accruing at the rate of ' . $value . ' monthly and attorneys fees, ';
                                } else {
                                    $value = '';
                                }
                            } else if ($key == 'owner_received_nto_date') {
                                if (isset($value) && !empty($value)) {
                                    $owner_received_nto_date = $value;
                                }
                            } else if ($key == 'general_contractor_received_nto_date') {
                                if (isset($value) && !empty($value)) {
                                    $general_contractor_received_nto_date = $value;
                                }
                            } else if ($key == 'sub_subcontractor_received_nto_date') {
                                if (isset($value) && !empty($value)) {
                                    $sub_subcontractor_received_nto_date = $value;
                                }
                            } else if ($key == 'subcontractor_received_nto_date') {
                                if (isset($value) && !empty($value)) {
                                    $subcontractor_received_nto_date = $value;
                                }
                            } else if ($key == 'contracted_by_received_nto_date') {
                                if (isset($value) && !empty($value)) {
                                    $contracted_by_received_nto_date = $value;
                                }
                            } else if ($key == 'project_owner') {
                                if (isset($value) && !empty($value)) {
                                    $value = $value;
                                    $project_owner = $value;
                                }
                            }   /*else if ($key == 'job_start_date' || $key == 'last_date_on_the_job' || $key == 'owner_received_nto_date' ||
                                    $key == 'general_contractor_received_nto_date' || $key == 'subcontractor_received_nto_date' ||
                                    $key == 'sub_subcontractor_received_nto_date') {
                                if (isset($value) && !empty($value)) {
                                    $value = date('F d,Y', strtotime($value));
                                }
                            } */ else if ($key == 'job_start_date' || $key == 'last_date_on_the_job') {
                                if (isset($value) && !empty($value)) {
                                    $value = date('F d,Y', strtotime($value));
                                }
                            } else if ($key == "check_clear") {

                                if ($value) {
                                    $value = "This form of release is expressly conditioned upon the undersigned’s receiving the actual amount referenced above and payment has been properly endorsed and funds paid/cleared by bank. ";
                                } else {
                                    $value = "";
                                }
                            } else if ($key == "notary_seal") {
                                if ($value) {
                                    $value = "<table style='width: 100%;'>
                                                <tbody>
                                                    <tr>
                                                        <td colspan='2'>
                                                            <table style='width: 50%;'>
                                                                <tbody>
                                                                        <tr>
                                                                                <td style='vertical-align: top;width: 35%;padding: 1px 0; font-size:10px; line-height: 10px;'>State:</td>
                                                                                <td style='width: 50%;padding: 1px 0; font-size:10px; line-height: 10px;'>_________________________________________________________</td>
                                                                        </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan='2'>
                                                            <table style='width: 50%;'>
                                                                <tbody>
                                                                        <tr>
                                                                                <td style='vertical-align: top;width: 35%;padding: 1px 0; font-size:10px; line-height: 10px;'>County:</td>
                                                                                <td style='width: 50%;padding: 1px 0; font-size:10px; line-height: 10px;'>_________________________________________________________</td>
                                                                        </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    
                                                <tr>
                                                        <td colspan='2'>
                                                            <table style='width: 100%;'>
                                                                <tbody>
                                                                        <tr>
                                                                                <td style='vertical-align: top;width: 35%;padding: 1px 0; font-size:10px; line-height: 10px;'> Sworn to (or affirmed) and subscribed before me this _______________(Month) ______(Date) _______________(Year)
                                                        BY_______________personally know_______________ Produce Identification __________________________________(Type of Identification)      
                                                    </td>
                                                                            
                                                                        </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan='2'>
                                                            <table style='width: 50%;'>
                                                                <tbody>
                                                                        <tr>
                                                                                <td style='vertical-align: top;width: 35%;padding: 1px 0; font-size:10px; line-height: 10px;'>Notary Signature:</td>
                                                                                <td style='width: 50%;padding: 2px 0;'>_________________________________________________________</td>
                                                                        </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan='2'>
                                                            <table style='width: 50%;'>
                                                                <tbody>
                                                                        <tr>
                                                                                <td style='vertical-align: top;width: 35%;padding: 1px 0; font-size:10px; line-height: 10px;'>Notary Stamp</td>
                                                                                <td style='width: 50%;padding: 1px 0; font-size:10px; line-height: 10px;'></td>
                                                                        </tr>
                                                                        <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                                        <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                                        <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                                        <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                                        <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                                        <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                                        <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                                        <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                                        <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                                        <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                                        <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                                        <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                            </tbody></table>";
                                    // dd($value);
                                } else {
                                    $value = "";
                                }
                            }
                            array_push($arr_replace, $value);
                        }
                    }
                } //dd($legal_description);
                $amendment = '';
                if (array_key_exists('amendment', $arr_replace)) {
                    // $amendment = '';
                } else {
                    $amendment = '';
                }
                /*         * *********Dynamic Data**************** */

                /*         * *********Customer Registration Data**************** */

                if (!in_array("{{parent_work_order}}", $arr_variable)) {
                    array_push($arr_variable, "{{parent_work_order}}");
                    array_push($arr_replace, $parent_work_order);
                }

                if (!in_array("{{county}}", $arr_variable)) {
                    array_push($arr_variable, "{{county}}");
                    array_push($arr_replace, $parent_county);
                } else {
                    $index = array_search('{{county}}', $arr_variable);
                    if ($arr_replace[$index] == "") {
                        $arr_replace[$index] = $parent_county;
                    }
                }

                $customer_company_name = \App\Models\Customer::find($work_order->customer_id);
                $company_name = $customer_company_name->company_name;
                $customer_telephone = $customer_company_name->office_number;
                $company_address = $customer_company_name->mailing_address;
                $company_state_name = \App\Models\State::find($customer_company_name->mailing_state_id);
                $company_state = $company_state_name->name;
                $company_city_name = \App\Models\City::find($customer_company_name->mailing_city_id);
                $company_city = !empty($company_city_name) ? $company_city_name->name : '';
                $company_city_state_zip = $company_city . ' ' . $company_state . ' ' . $customer_company_name->mailing_zip;
                //Noc ,folio no and bond no
                if ($work_order_type && $work_order_type == 'cyo') {
                    $document = \App\Models\Cyo_WorkOrder_Attachment::where('work_order_id', '=', $work_order_id)->get();
                } else {
                    $document = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $work_order_id)->get();
                }
                /*         * *********Customer Registration Data**************** */
                /*         * *********Work Order Document Data**************** */
                $bond = '';
                $noc = '';
                $folio = '';
                $permit = '';
                foreach ($document as $key => $each_document) {
                    //dd($each_document->type, $each_document->title);
                    if ($each_document->type == 1 || $each_document->type == 2 || $each_document->type == 10 || $each_document->type == 3) {

                        if ($each_document->type == 1 && $each_document->title != "") {
                            $bond = $bond . ' Bond:' . $each_document->title;
                        }
                        //Noc
                        //if ($each_document->type == 2 && $each_document->title != "") {
                        if ($each_document->type == 2) {
                            $noc = $noc . ' BK:' . $each_document->title;
                            //dd($noc);
                        }

                        //Folio
                        if ($each_document->type == 10 && $each_document->title != "") {
                            $folio = $folio . ' Folio No: ' . $each_document->title;
                        }
                        /*
                        * If document type is Foilo
                        */
                        if ($each_document->type == 3 && $each_document->title != "") {
                            $permit = $permit . " " . ' Permit # ' . $each_document->title;
                        }
                    }
                }
                /*         * *********Work Order Document Data**************** */
                /*         * *********Recipients Owner Data**************** */
                $owner_firm_mailing = '';
                $owner_name = '';
                $owner_address = '';
                $owner_city_state_zip = '';

                $contractor_name = '';
                $sub_contractor_name = '';
                $sub_sub_contractor_name = '';
                $sub_contractor = '';
                $recipient_owners = "";
                $owners = '<table><tbody>';
                $o = $count = 1; //dd($recipients);


                //BOTOTM:: if cat_id in (21, 2) then contract_by(21) show bottom
                $bothContractedByAndGeneralContractor =  $recipients->whereIn('category_id', [21, 2])
                    ->groupBy('category_id')->toArray();
                //TOP:: if cat_id != 2 then 21 will show
                $showContractedByByGcToTop = array_key_exists(2, $bothContractedByAndGeneralContractor) ? 'GC' : (array_key_exists(21, $bothContractedByAndGeneralContractor) ? 'CB' : null);


                if (isset($recipients) && count($recipients) > 0) {
                    foreach ($recipients as $k => $each_recipient) {
                        if ($each_recipient->category_id == 22 || $each_recipient->category_id == 1) {

                            $owner_firm_mailing = $each_recipient->tracking_number;

                            $owner_name = $each_recipient->name;
                            //$owner_address = $each_recipient->address;
                            $owner_address = (!empty($each_recipient->address)) ? $each_recipient->address : $each_recipient->ad;
                                
                            $owner_address .= ((!empty($each_recipient->address2)) ? '<br />' : '').$each_recipient->address2;
                            $owner_address .= ((!empty($each_recipient->address3)) ? '<br />' : '').$each_recipient->address3;

                            $each_recipient->attn = str_lreplace($each_recipient->attn);
                            $owner_attn = strtoupper($each_recipient->attn) != "" ? " " . strtoupper($each_recipient->attn) . "<br/>" : "";
                            $state = "";
                            if (isset($each_recipient->state_id) && $each_recipient->state_id != null) {
                                $state_name = \App\Models\State::find($each_recipient->state_id);
                                $state = $state_name->name;
                            }
                            $city = "";
                            if (isset($each_recipient->city_id) && $each_recipient->city_id != null) {
                                $city_name = \App\Models\City::find($each_recipient->city_id);
                                $city = $city_name->name;
                            }
                            if ($each_recipient->city_id != NULL || $each_recipient->state_id != NULL) {
                                $owner_city_state_zip = $city . ' ' . $state . ' ' . $each_recipient->zip;
                            } else {
                                $owner_city_state_zip = '';
                            }
                            if ($o == 1) {
                                $owners = $owners . '<tr>';
                            }
                            $owners = $owners . '
                            
                                <td  width="340px;">
                                <table scope="col">
                                    <tbody>
                                        <tr>
                                            <td style="vertical-align: top;padding: 0 2px; font-size:10px; line-height: 10px;">' . (($count > 1) ? '' : (strpos($content, '{{owners_with_label_property_owner}}') ?  'PROPERTY OWNER' : (strpos($content, '{{owners_with_label_to_property_owner}}') ? 'TO PROPERTY OWNER:' : 'TO:'))) . '</td>
                                            <td style="padding: 0 2px; font-size:10px; line-height: 10px;">' . $owner_firm_mailing . '<br/>
                                                                                    ' . $owner_name . '<br/>' . nl2br($owner_attn) . nl2br($owner_address) . '<br/>
                                                                                            ' . $owner_city_state_zip . '<br/></td>

                                        </tr>
                                    </tbody>
                                </table>
                                </td>
                                ';
                            if ($o == 2) {
                                $owners = $owners . '<tr>';
                                $o = 1;
                                break;
                            }
                            $o++;
                            $count++;
                            //                        $owner_firm_mailing2 = $each_recipient->tracking_number;
                            //
                            //                        $owner_name2 = $each_recipient->name;
                            //                        $owner_address2 = $each_recipient->address;
                            //
                            //                        $state = "";
                            //                        if (isset($each_recipient->state_id) && $each_recipient->state_id != null) {
                            //                            $state_name = \App\Models\State::find($each_recipient->state_id);
                            //                            $state = $state_name->name;
                            //                        }
                            //                        $city = "";
                            //                        if (isset($each_recipient->city_id) && $each_recipient->city_id != null) {
                            //                            $city_name = \App\Models\City::find($each_recipient->city_id);
                            //                            $city = $city_name->name;
                            //                        }
                            //
                            //                        $owner_city_state_zip2 = $city . ' ' . $state . ' ' . $each_recipient->zip;
                            //
                            //                        $owner_html2 = '<td style="vertical-align: top;padding: 10px;">TO:</td>
                            //						<td style="padding: 10px;">' . $owner_firm_mailing2 . '
                            //						' . $owner_name2 . '
                            //						' . $owner_address2 . '
                            //						' . $owner_city_state_zip2 . '</td>
                            //					';
                        } else if ($each_recipient->category_id == 2) {
                            $contractor_name = $each_recipient->name;
                        } else if ($each_recipient->category_id == 3) {
                            $state = "";
                            if (isset($each_recipient->state_id) && $each_recipient->state_id != null) {
                                $state_name = \App\Models\State::find($each_recipient->state_id);
                                $state = $state_name->name;
                            }
                            $city = "";
                            if (isset($each_recipient->city_id) && $each_recipient->city_id != null) {
                                $city_name = \App\Models\City::find($each_recipient->city_id);
                                $city = $city_name->name;
                            }
                            if ($each_recipient->city_id != NULL || $each_recipient->state_id != NULL) {
                                $owner_city_state_zip = $city . ' ' . $state . ' ' . $each_recipient->zip;
                            } else {
                                $owner_city_state_zip = '';
                            }
                            $sub_contractor_name = $each_recipient->name;
                            $each_recipient->attn = str_lreplace($each_recipient->attn);
                            $sub_contractor = $each_recipient->name . ' ' . nl2br($each_recipient->attn) . ' ' . nl2br($each_recipient->address) . ' ' . $owner_city_state_zip;
                        } else if ($each_recipient->category_id == 4) {

                            $sub_sub_contractor_name = $each_recipient->name;
                        }
                    }
                }
                $owners = $owners . '</tbody></table>';

                $contracted_by_recipient = '<table><tbody>';
                $o = 1;
                if (isset($recipients) && count($recipients) > 0) {
                    foreach ($recipients as $k => $each_recipient) {
                        if (($each_recipient->category_id == 21 && $showContractedByByGcToTop == 'CB') || ($each_recipient->category_id == 2 && $showContractedByByGcToTop == 'GC')) {

                            $owner_firm_mailing = $each_recipient->tracking_number;

                            $owner_name = $each_recipient->name;
                            $each_recipient->attn = str_lreplace($each_recipient->attn);
                            $owner_attn = strtoupper($each_recipient->attn) != "" ? " " . strtoupper($each_recipient->attn) . "<br/>" : "";
                            //$owner_address = $each_recipient->address;
                            $owner_address = (!empty($each_recipient->address)) ? $each_recipient->address : $each_recipient->ad;
                                
                            $owner_address .= ((!empty($each_recipient->address2)) ? '<br />' : '').$each_recipient->address2;
                            $owner_address .= ((!empty($each_recipient->address3)) ? '<br />' : '').$each_recipient->address3;

                            $state = "";
                            if (isset($each_recipient->state_id) && $each_recipient->state_id != null) {
                                $state_name = \App\Models\State::find($each_recipient->state_id);
                                $state = $state_name->name;
                            }
                            $city = "";
                            if (isset($each_recipient->city_id) && $each_recipient->city_id != null) {
                                $city_name = \App\Models\City::find($each_recipient->city_id);
                                $city = $city_name->name;
                            }
                            if ($each_recipient->city_id != NULL || $each_recipient->state_id != NULL) {
                                $owner_city_state_zip = $city . ' ' . $state . ' ' . $each_recipient->zip;
                            } else {
                                $owner_city_state_zip = '';
                            }
                            //Start first owner 
                            if ($o == 1) {
                                $contracted_by_recipient = $contracted_by_recipient . '<tr>';
                            }
                            $contracted_by_recipient = $contracted_by_recipient . '
                                <td>
                                <table scope="col">
                                    <tbody>
                                        <tr>
                                            <td style="vertical-align: top;padding: 0 2px;font-size:10px; line-height: 10px;">' . (strpos($content, '{{owners_with_label_property}}') ?  'PROPERTY OWNER' : (strpos($content, '{{owners_with_label_to_property_owner}}') ? 'TO PROPERTY OWNER' : 'TO')) . ':</td>
                                            <td style="padding: 0 2px;word-wrap:break-word; word-break:break-all;font-size:10px; line-height: 10px;">' . $owner_firm_mailing . '<br/>
                                                                                    <p style="width: 200px; margin-bottom: 0;margin-top: 0;font-size:10px; line-height: 10px;">' . $owner_name . '</p>
                                                                                        ' . nl2br($owner_attn) . nl2br($owner_address) . '<br/>
                                                                                            ' . $owner_city_state_zip . '<br/></td>
                                        </tr>
                                    </tbody>
                                </table>
                                </td>
                                ';
                            if ($o == 1) {
                                $contracted_by_recipient = $contracted_by_recipient . '<tr>';
                                //After second onwer break the loop
                                break;
                                $o = 1;
                            }
                            $o++;
                            //                        $owner_firm_mailing2 = $each_recipient->tracking_number;
                            //
                            //                        $owner_name2 = $each_recipient->name;
                            //                        $owner_address2 = $each_recipient->address;
                            //
                            //                        $state = "";
                            //                        if (isset($each_recipient->state_id) && $each_recipient->state_id != null) {
                            //                            $state_name = \App\Models\State::find($each_recipient->state_id);
                            //                            $state = $state_name->name;
                            //                        }
                            //                        $city = "";
                            //                        if (isset($each_recipient->city_id) && $each_recipient->city_id != null) {
                            //                            $city_name = \App\Models\City::find($each_recipient->city_id);
                            //                            $city = $city_name->name;
                            //                        }
                            //
                            //                        $owner_city_state_zip2 = $city . ' ' . $state . ' ' . $each_recipient->zip;
                            //
                            //                        $owner_html2 = '<td style="vertical-align: top;padding: 10px;">TO:</td>
                            //                      <td style="padding: 10px;">' . $owner_firm_mailing2 . '
                            //                      ' . $owner_name2 . '
                            //                      ' . $owner_address2 . '
                            //                      ' . $owner_city_state_zip2 . '</td>
                            //                  ';
                        }
                    }
                }

                $contracted_by_recipient = $contracted_by_recipient . '</tbody></table>';
                $surety_recipient = '<table><tbody>';
                $o = 1;
                if (isset($recipients) && count($recipients) > 0) {
                    foreach ($recipients as $k => $each_recipient) {
                        if ($each_recipient->category_id == 9) {

                            $owner_firm_mailing = $each_recipient->tracking_number;

                            $owner_name = $each_recipient->name;
                            $each_recipient->attn = str_lreplace($each_recipient->attn);
                            $owner_attn = strtoupper($each_recipient->attn) != "" ? " " . strtoupper($each_recipient->attn) . "<br/>" : "";
                            // $owner_attn = $each_recipient->attn != "" ? " " . $each_recipient->attn . "<br/>" : "";
                            //$owner_address = $each_recipient->address;
                            $owner_address = (!empty($each_recipient->address)) ? $each_recipient->address : $each_recipient->ad;
                                
                            $owner_address .= ((!empty($each_recipient->address2)) ? '<br />' : '').$each_recipient->address2;
                            $owner_address .= ((!empty($each_recipient->address3)) ? '<br />' : '').$each_recipient->address3;

                            $state = "";
                            if (isset($each_recipient->state_id) && $each_recipient->state_id != null) {
                                $state_name = \App\Models\State::find($each_recipient->state_id);
                                $state = $state_name->name;
                            }
                            $city = "";
                            if (isset($each_recipient->city_id) && $each_recipient->city_id != null) {
                                $city_name = \App\Models\City::find($each_recipient->city_id);
                                $city = $city_name->name;
                            }
                            if ($each_recipient->city_id != NULL || $each_recipient->state_id != NULL) {
                                $owner_city_state_zip = $city . ' ' . $state . ' ' . $each_recipient->zip;
                            } else {
                                $owner_city_state_zip = '';
                            }
                            //Start first owner 
                            if ($o == 1) {
                                $surety_recipient = $surety_recipient . '<tr>';
                            }
                            $surety_recipient = $surety_recipient . '
                                <td>
                                <table scope="col">
                                    <tbody>
                                        <tr>
                                            <td style="vertical-align: top;padding: 0 2px;font-size:10px; line-height: 10px;">' . (strpos($content, '{{owners_with_label_property}}') ?  'PROPERTY OWNER' : (strpos($content, '{{owners_with_label_to_property_owner}}') ? 'TO PROPERTY OWNER' : 'TO')) . ':</td>
                                            <td style="padding: 0 2px;word-wrap:break-word; word-break:break-all;font-size:10px; line-height: 10px;">' . $owner_firm_mailing . '<br/>
                                                                                    <p style="width: 200px; margin-bottom: 0;margin-top: 0;font-size:10px; line-height: 10px;">' .$owner_name . '</p>
                                                                                        ' . nl2br($owner_attn) . nl2br($owner_address) . '<br/>
                                                                                            ' . $owner_city_state_zip . '<br/></td>
                                        </tr>
                                    </tbody>
                                </table>
                                </td>
                                ';
                            if ($o == 3) {
                                $surety_recipient = $surety_recipient .'</tr>';
                                //After second onwer break the loop
                                $o = 0;
                                //break;
                            }
                            $o++;
                            //                        $owner_firm_mailing2 = $each_recipient->tracking_number;
                            //
                            //                        $owner_name2 = $each_recipient->name;
                            //                        $owner_address2 = $each_recipient->address;
                            //
                            //                        $state = "";
                            //                        if (isset($each_recipient->state_id) && $each_recipient->state_id != null) {
                            //                            $state_name = \App\Models\State::find($each_recipient->state_id);
                            //                            $state = $state_name->name;
                            //                        }
                            //                        $city = "";
                            //                        if (isset($each_recipient->city_id) && $each_recipient->city_id != null) {
                            //                            $city_name = \App\Models\City::find($each_recipient->city_id);
                            //                            $city = $city_name->name;
                            //                        }
                            //
                            //                        $owner_city_state_zip2 = $city . ' ' . $state . ' ' . $each_recipient->zip;
                            //
                            //                        $owner_html2 = '<td style="vertical-align: top;padding: 10px;">TO:</td>
                            //                      <td style="padding: 10px;">' . $owner_firm_mailing2 . '
                            //                      ' . $owner_name2 . '
                            //                      ' . $owner_address2 . '
                            //                      ' . $owner_city_state_zip2 . '</td>
                            //                  ';
                        }
                    }
                    if ($o == 3) {
                        $surety_recipient = $surety_recipient . '</tr>';
                    }
                }
                $surety_recipient = $surety_recipient . '</tbody></table>';

                //print surety_rec & GC in one line
                $surety_recipient_and_general_contractor = '<table><tbody>';
                $o = 1;
                $recSuretyAndGC = $recipients->whereIn('category_id', [2, 9]);
                if (isset($recSuretyAndGC) && count($recSuretyAndGC) > 0) {
                    foreach ($recSuretyAndGC as $k => $each_recipient) {
                        $owner_firm_mailing = $each_recipient->tracking_number;

                        $owner_name = $each_recipient->name;
                        $each_recipient->attn = str_lreplace($each_recipient->attn);
                        $owner_attn = strtoupper($each_recipient->attn) != "" ? " " . strtoupper($each_recipient->attn) . "<br/>" : "";
                        // $owner_attn = $each_recipient->attn != "" ? " " . $each_recipient->attn . "<br/>" : "";
                        //$owner_address = $each_recipient->address;
                        $owner_address = (!empty($each_recipient->address)) ? $each_recipient->address : $each_recipient->ad;
                                
                        $owner_address .= ((!empty($each_recipient->address2)) ? '<br />' : '').$each_recipient->address2;
                        $owner_address .= ((!empty($each_recipient->address3)) ? '<br />' : '').$each_recipient->address3;

                        $state = "";
                        if (isset($each_recipient->state_id) && $each_recipient->state_id != null) {
                            $state_name = \App\Models\State::find($each_recipient->state_id);
                            $state = $state_name->name;
                        }
                        $city = "";
                        if (isset($each_recipient->city_id) && $each_recipient->city_id != null) {
                            $city_name = \App\Models\City::find($each_recipient->city_id);
                            $city = $city_name->name;
                        }
                        if ($each_recipient->city_id != NULL || $each_recipient->state_id != NULL) {
                            $owner_city_state_zip = $city . ' ' . $state . ' ' . $each_recipient->zip;
                        } else {
                            $owner_city_state_zip = '';
                        }
                        //Start first owner 
                        if ($o == 1) {
                            $surety_recipient_and_general_contractor = $surety_recipient_and_general_contractor . '<tr>';
                        }
                        $surety_recipient_and_general_contractor = $surety_recipient_and_general_contractor . '
                            <td>
                            <table scope="col">
                                <tbody>
                                    <tr>
                                        <td style="vertical-align: top;padding: 0 2px;font-size:10px; line-height: 10px;">' . (strpos($content, '{{owners_with_label_property}}') ?  'PROPERTY OWNER' : (strpos($content, '{{owners_with_label_to_property_owner}}') ? 'TO PROPERTY OWNER' : 'TO')) . ':</td>
                                        <td style="padding: 0 2px;word-wrap:break-word; word-break:break-all;font-size:10px; line-height: 10px;">' . $owner_firm_mailing . '<br/>
                                                                                <p style="width: 200px; margin-bottom: 0;margin-top: 0;font-size:10px; line-height: 10px;">' .$owner_name . '</p>
                                                                                    ' . nl2br($owner_attn) . nl2br($owner_address) . '<br/>
                                                                                        ' . $owner_city_state_zip . '<br/></td>
                                    </tr>
                                </tbody>
                            </table>
                            </td>
                            ';
                        if ($o == 3) {
                            $surety_recipient_and_general_contractor = $surety_recipient_and_general_contractor .'</tr>';
                            //After second onwer break the loop
                            $o = 0;
                            //break;
                        }
                        $o++;
                    }
                    if ($o == 3) {
                        $surety_recipient_and_general_contractor = $surety_recipient_and_general_contractor . '</tr>';
                    }
                }
                $surety_recipient_and_general_contractor = $surety_recipient_and_general_contractor . '</tbody></table>';
                //END: print surety_rec & GC in one line

                $is_bond_of_claim = '<table><tbody>';
                $o = 1;
                if (isset($recipients_is_bond_of_claim) && count($recipients_is_bond_of_claim) > 0) { //dd($recipients_is_bond_of_claim);
                    foreach ($recipients_is_bond_of_claim as $k => $each_recipient) {
                        if ($each_recipient->category_id == 9) {

                            $bond_of_claim_firm_mailing = $each_recipient->tracking_number;

                            $bond_of_claim_name = $each_recipient->name;
                            $each_recipient->attn = str_lreplace($each_recipient->attn);
                            $bond_of_claim_attn = strtoupper($each_recipient->attn) != "" ? " " . strtoupper($each_recipient->attn) . "<br/>" : "";
                            // $bond_of_claim_attn = $each_recipient->attn != "" ? " " . $each_recipient->attn . "<br/>" : "";
                        // $bond_of_claim_address = $each_recipient->address;
                            $bond_of_claim_address = (!empty($each_recipient->address)) ? $each_recipient->address : $each_recipient->ad;
                                
                            $bond_of_claim_address .= ((!empty($each_recipient->address2)) ? '<br />' : '').$each_recipient->address2;
                            $bond_of_claim_address .= ((!empty($each_recipient->address3)) ? '<br />' : '').$each_recipient->address3;

                            $state = "";
                            if (isset($each_recipient->state_id) && $each_recipient->state_id != null) {
                                $state_name = \App\Models\State::find($each_recipient->state_id);
                                $state = $state_name->name;
                            }
                            $city = "";
                            if (isset($each_recipient->city_id) && $each_recipient->city_id != null) {
                                $city_name = \App\Models\City::find($each_recipient->city_id);
                                $city = $city_name->name;
                            }
                            if ($each_recipient->city_id != NULL || $each_recipient->state_id != NULL) {
                                $bond_of_claim_city_state_zip = $city . ' ' . $state . ' ' . $each_recipient->zip;
                            } else {
                                $bond_of_claim_city_state_zip = '';
                            }
                            //Start first bond_of_claim 
                            if ($o == 1) {
                                $is_bond_of_claim = $is_bond_of_claim . '<tr>';
                            }
                            $is_bond_of_claim = $is_bond_of_claim . '<td>
                                <table scope="col">
                                <tbody>
                                    <tr>
                                    
                                    <td style="padding: 0 2px;word-wrap:break-word; word-break:break-all;font-size:10px; line-height: 10px;">' . $bond_of_claim_firm_mailing . '<br/>
                                                                                                <p style="width: 200px; margin-bottom: 0;margin-top: 0;font-size:10px; line-height: 10px;">' . $bond_of_claim_name . '</p>
                                                                                                    ' . $bond_of_claim_attn . $bond_of_claim_address . '<br/>
                                                                                                        ' . $bond_of_claim_city_state_zip . '<br/></td>
                                    </tr>
                                </tbody>
                                </table>
                            </td>';
                            if ($o == 3) {
                                $is_bond_of_claim = $is_bond_of_claim . '<tr>';
                                //After second onwer break the loop
                                break;
                                $o = 1;
                            }
                            $o++;
                            //                       
                            //          ';
                        }
                    }
                }
                $is_bond_of_claim = $is_bond_of_claim . '</tbody></table>';

                $bothContractedByAndGeneralContractor =  $recipients->whereIn('category_id', [21, 2])
                    ->groupBy('category_id')->toArray();
                //TOP:: if cat_id != 2 then 21 will show
                $showContractedByByGcToTop = array_key_exists(2, $bothContractedByAndGeneralContractor) ? 2 : (array_key_exists(21, $bothContractedByAndGeneralContractor) ? 21 : null);

                $general_contractor = '<table><tbody>';
                $o = 1;
                if (isset($recipients) && count($recipients) > 0) { //dd($recipients_is_bond_of_claim);
                    foreach ($recipients as $k => $each_recipient) {
                        if ($each_recipient->category_id == $showContractedByByGcToTop) {

                            $bond_of_claim_firm_mailing = $each_recipient->tracking_number;

                            $bond_of_claim_name = $each_recipient->name;
                            $each_recipient->attn = str_lreplace($each_recipient->attn);
                            $bond_of_claim_attn = strtoupper($each_recipient->attn) != "" ? " " . strtoupper($each_recipient->attn) . "<br/>" : "";
                            // $bond_of_claim_attn = $each_recipient->attn != "" ? " " . $each_recipient->attn . "<br/>" : "";
                        // $bond_of_claim_address = $each_recipient->address;
                            $bond_of_claim_address = (!empty($each_recipient->address)) ? $each_recipient->address : $each_recipient->ad;
                            
                            $bond_of_claim_address .= ((!empty($each_recipient->address2)) ? '<br />' : '').$each_recipient->address2;
                            $bond_of_claim_address .= ((!empty($each_recipient->address3)) ? '<br />' : '').$each_recipient->address3;

                            $state = "";
                            if (isset($each_recipient->state_id) && $each_recipient->state_id != null) {
                                $state_name = \App\Models\State::find($each_recipient->state_id);
                                $state = $state_name->name;
                            }
                            $city = "";
                            if (isset($each_recipient->city_id) && $each_recipient->city_id != null) {
                                $city_name = \App\Models\City::find($each_recipient->city_id);
                                $city = $city_name->name;
                            }
                            if ($each_recipient->city_id != NULL || $each_recipient->state_id != NULL) {
                                $bond_of_claim_city_state_zip = $city . ' ' . $state . ' ' . $each_recipient->zip;
                            } else {
                                $bond_of_claim_city_state_zip = '';
                            }
                            //Start first bond_of_claim 
                            if ($o == 1) {
                                $general_contractor = $general_contractor . '<tr>';
                            }
                            $general_contractor = $general_contractor . '<td>
                                <table scope="col">
                                <tbody>
                                    <tr>
                                    
                                    <td style="padding: 0 2px;word-wrap:break-word; word-break:break-all;font-size:10px; line-height: 10px;">' . $bond_of_claim_firm_mailing . '<br/>
                                        <p style="width: 200px; margin-bottom: 0;margin-top: 0;font-size:10px; line-height: 10px;">' . $bond_of_claim_name . '</p>
                                            ' . $bond_of_claim_attn . $bond_of_claim_address . '<br/>
                                                ' . $bond_of_claim_city_state_zip . '<br/></td>
                                    </tr>
                                </tbody>
                                </table>
                                </td>';
                            if ($o == 3) {
                                $general_contractor = $general_contractor . '<tr>';
                                //After second onwer break the loop
                                //break;
                                $o = 0;
                            }
                            $o++;
                        }
                    }
                }
                $general_contractor = $general_contractor . '</tbody></table>';
                // $surety_bond = $surety_bond . '</tbody></table>';


                if (isset($contractor_name) && !empty($contractor_name)) {
                    $contractor_name = ', that the lienor served copies of the notice on the contractor, ' . $contractor_name;
                }


                if (isset($sub_contractor_name) && !empty($sub_contractor_name)) {
                    $sub_contractor_name = 'and on the Sub Contractor';
                }
                if (isset($sub_sub_contractor_name) && !empty($sub_sub_contractor_name)) {
                    $sub_sub_contractor_name = 'and on the Sub Sub Contractor';
                }

                if (isset($work_order_type) && $work_order_type == 'cyo') {

                    $recipients_owners = \App\Models\Cyo_WorkOrder_Recipient::leftjoin('cyo__stamps_labels', 'cyo__stamps_labels.recipient_id', '=', 'cyo__work_order__recipients.id')
                        ->where('work_order_id', '=', $work_order_id)
                        ->whereIn('category_id', ['1', '22'])->select('name', 'category_id')->get()->toArray();
                } else {
                    $recipients_owners = \App\Models\Recipient::leftjoin('stamps_label', 'stamps_label.recipient_id', '=', 'recipients.id')
                        ->where('work_order_id', '=', $work_order_id)
                        ->whereIn('category_id', ['1', '22'])->select('name', 'category_id')->get()->toArray();
                }
                $recipient_owners = "";
                if (isset($recipients_owners) && count($recipients_owners) > 0) {
                    foreach ($recipients_owners as $k => $each_recipient) {
                        if (count($recipients_owners) == 1) {
                            $recipient_owners = $each_recipient['name'];
                        } else if ((count($recipients_owners) - 1) == $k) {
                            $recipient_owners = $recipient_owners . ' and ' . $each_recipient['name'];
                        } else if ($k == (count($recipients_owners) - 2)) {
                            $recipient_owners = $recipient_owners . ' ' . $each_recipient['name'];
                        } else {
                            $recipient_owners = $recipient_owners . $each_recipient['name'] . ' , ';
                        }
                    }
                }
                if (isset($work_order_type) && $work_order_type == 'cyo') {

                    $recipient_contractors = \App\Models\Cyo_WorkOrder_Recipient::leftjoin('cyo__stamps_labels', 'cyo__stamps_labels.recipient_id', '=', 'cyo__work_order__recipients.id')
                        ->where('work_order_id', '=', $work_order_id)
                        ->where('category_id', '2')->select('name', 'category_id')->get()->toArray();
                } else {
                    $recipient_contractors = \App\Models\Recipient::leftjoin('stamps_label', 'stamps_label.recipient_id', '=', 'recipients.id')
                        ->where('work_order_id', '=', $work_order_id)
                        ->where('category_id', '2')->select('name', 'category_id')->get()->toArray();
                }
                $recipient_general_contractor = "";
                if (isset($recipient_contractors) && count($recipient_contractors) > 0) {
                    foreach ($recipient_contractors as $k => $each_recipient) {
                        if (count($recipient_contractors) == 1) {
                            $recipient_general_contractor = $each_recipient['name'];
                        } else if ((count($recipient_contractors) - 1) == $k) {
                            $recipient_general_contractor = $recipient_general_contractor . ' and ' . $each_recipient['name'];
                        } else if ($k == (count($recipient_contractors) - 2)) {
                            $recipient_general_contractor = $recipient_general_contractor . ' ' . $each_recipient['name'];
                        } else {
                            $recipient_general_contractor = $recipient_general_contractor . $each_recipient['name'] . ' , ';
                        }
                    }
                }

                if (isset($work_order_type) && $work_order_type == 'cyo') {

                    $recipient_sub_contractors = \App\Models\Cyo_WorkOrder_Recipient::leftjoin('cyo__stamps_labels', 'cyo__stamps_labels.recipient_id', '=', 'cyo__work_order__recipients.id')
                        ->where('work_order_id', '=', $work_order_id)
                        ->where('category_id', '3')->select('name', 'category_id')->get()->toArray();
                } else {
                    $recipient_sub_contractors = \App\Models\Recipient::leftjoin('stamps_label', 'stamps_label.recipient_id', '=', 'recipients.id')
                        ->where('work_order_id', '=', $work_order_id)
                        ->where('category_id', '3')->select('name', 'category_id')->get()->toArray();
                }
                $recipient_sub_contractor = "";
                if (isset($recipient_sub_contractors) && count($recipient_sub_contractors) > 0) {
                    foreach ($recipient_sub_contractors as $k => $each_recipient) {
                        if (count($recipient_sub_contractors) == 1) {
                            $recipient_sub_contractor = $each_recipient['name'];
                        } else if ((count($recipient_sub_contractors) - 1) == $k) {
                            $recipient_sub_contractor = $recipient_sub_contractor . ' and ' . $each_recipient['name'];
                        } else if ($k == (count($recipient_sub_contractors) - 2)) {
                            $recipient_sub_contractor = $recipient_sub_contractor . ' ' . $each_recipient['name'];
                        } else {
                            $recipient_sub_contractor = $recipient_sub_contractor . $each_recipient['name'] . ' , ';
                        }
                    }
                }
                if (isset($work_order_type) && $work_order_type == 'cyo') {

                    $recipient_sub_sub_contractors = \App\Models\Cyo_WorkOrder_Recipient::leftjoin('cyo__stamps_labels', 'cyo__stamps_labels.recipient_id', '=', 'cyo__work_order__recipients.id')
                        ->where('work_order_id', '=', $work_order_id)
                        ->where('category_id', '4')->select('name', 'category_id')->get()->toArray();
                } else {
                    $recipient_sub_sub_contractors = \App\Models\Recipient::leftjoin('stamps_label', 'stamps_label.recipient_id', '=', 'recipients.id')
                        ->where('work_order_id', '=', $work_order_id)
                        ->where('category_id', '4')->select('name', 'category_id')->get()->toArray();
                }
                $recipient_sub_sub_contractor = "";
                if (isset($recipient_sub_sub_contractors) && count($recipient_sub_sub_contractors) > 0) {
                    foreach ($recipient_sub_sub_contractors as $k => $each_recipient) {
                        if (count($recipient_sub_sub_contractors) == 1) {
                            $recipient_sub_sub_contractor = $each_recipient['name'];
                        } else if ((count($recipient_sub_sub_contractors) - 1) == $k) {
                            $recipient_sub_sub_contractor = $recipient_sub_sub_contractor . ' and ' . $each_recipient['name'];
                        } else if ($k == (count($recipient_sub_sub_contractors) - 2)) {
                            $recipient_sub_sub_contractor = $recipient_sub_sub_contractor . ' ' . $each_recipient['name'];
                        } else {
                            $recipient_sub_sub_contractor = $recipient_sub_sub_contractor . $each_recipient['name'] . ' , ';
                        }
                    }
                }
                if (isset($work_order_type) && $work_order_type == 'cyo') {

                    $recipient_contractors = \App\Models\Cyo_WorkOrder_Recipient::leftjoin('cyo__stamps_labels', 'cyo__stamps_labels.recipient_id', '=', 'cyo__work_order__recipients.id')
                        ->where('work_order_id', '=', $parent_work_order)
                        ->where('category_id', '21')->select('name', 'category_id')->get()->toArray();
                } else {
                    $recipient_contractors = \App\Models\Recipient::leftjoin('stamps_label', 'stamps_label.recipient_id', '=', 'recipients.id')
                        ->where('work_order_id', '=', $parent_work_order)
                        ->where('category_id', '21')->select('name', 'category_id')->get()->toArray();
                }
                $recipient_contracted_by_name = "";
                if (isset($recipient_contractors) && count($recipient_contractors) > 0) {
                    foreach ($recipient_contractors as $k => $each_recipient) {
                        if (count($recipient_contractors) == 1) {
                            $recipient_contracted_by_name = $each_recipient['name'];
                        } else if ((count($recipient_contractors) - 1) == $k) {
                            $recipient_contracted_by_name = $recipient_contracted_by_name . ' and ' . $each_recipient['name'];
                        } else if ($k == (count($recipient_contractors) - 2)) {
                            $recipient_contracted_by_name = $recipient_contracted_by_name . ' ' . $each_recipient['name'];
                        } else {
                            $recipient_contracted_by_name = $recipient_contracted_by_name . $each_recipient['name'] . ' , ';
                        }
                    }
                }
                $final_col_owners = "";
                if (!empty($owner_received_nto_date) && !empty($owner_received_type_mailing)) {
                    $final_col_owners = $recipient_owners;
                } else {
                    $final_col_owners = $project_owner;
                }
                $final_col_content = "";
                if (empty($owner_received_nto_date) && empty($owner_received_type_mailing) && empty($general_contractor_received_nto_date) && empty($general_contractor_received_type_mailing) &&  empty($sub_subcontractor_received_nto_date) && empty($sub_subcontractor_received_type_mailing) && empty($subcontractor_received_nto_date) && empty($subcontractor_received_type_mailing) && empty($contracted_by_received_nto_date) && empty($contracted_by_received_type_mailing)) {
                    $final_col_content = "";
                } else if (!empty($owner_received_nto_date) || !empty($owner_received_type_mailing) || !empty($general_contractor_received_nto_date) || !empty($general_contractor_received_type_mailing) ||  !empty($sub_subcontractor_received_nto_date) || !empty($sub_subcontractor_received_type_mailing) || !empty($subcontractor_received_nto_date) || !empty($sub_subcontractor_received_type_mailing) || !empty($contracted_by_received_nto_date) || !empty($contracted_by_received_type_mailing)) {

                    if (!empty($owner_received_nto_date) && !empty($owner_received_type_mailing)) {
                        $final_col_content .= 'and (if the lien is claimed by one not in privity with the owner) that the lienor served his notice to owner  on ' . $owner_received_nto_date . ' BY ' . $owner_received_type_mailing;
                    }

                    if (!empty($final_col_content) && !empty($general_contractor_received_nto_date) && !empty($general_contractor_received_type_mailing)) {
                        $final_col_content .= ', ';
                    } else if (!empty($final_col_content) && !empty($contracted_by_received_nto_date) && !empty($contracted_by_received_type_mailing) && empty($general_contractor_received_nto_date) && empty($general_contractor_received_type_mailing) && !empty($owner_received_nto_date) && !empty($owner_received_type_mailing)) {
                        $final_col_content .= ', ';
                    }

                    if (!empty($general_contractor_received_nto_date) && !empty($general_contractor_received_type_mailing)) {
                        $final_col_content .= (empty($final_col_content) ? ', ' : '') . '(if required) that the lienor served copies of the notice on the Contractor ' . $recipient_general_contractor . ' on ' . $general_contractor_received_nto_date . ' BY ' . $general_contractor_received_type_mailing;
                    } else if (empty($general_contractor_received_nto_date) && empty($general_contractor_received_type_mailing) && !empty($contracted_by_received_nto_date) && !empty($contracted_by_received_type_mailing) && !empty($owner_received_nto_date) && !empty($owner_received_type_mailing)) {
                        $final_col_content .= (empty($final_col_content) ? ', ' : '') . '(if required) that the lienor served copies of the notice on the Contractor ' . $recipient_contracted_by_name . ' on ' . $contracted_by_received_nto_date . ' BY ' . $contracted_by_received_type_mailing;
                    }

                    /* if(!empty($final_col_content) && !empty($contracted_by_received_nto_date) && !empty($contracted_by_received_type_mailing)){
                    $final_col_content .= ', ';
                    }
                
                    if(!empty($contracted_by_received_nto_date) && !empty($contracted_by_received_type_mailing) ){
                        $final_col_content .= (empty($final_col_content) ? ', ' : '').' the Contracted By '.$recipient_contracted_by_name.' on '.$contracted_by_received_nto_date.' BY '.$contracted_by_received_type_mailing;
                    
                    } */

                    if (!empty($final_col_content) && !empty($subcontractor_received_nto_date) && !empty($subcontractor_received_type_mailing)) {
                        $final_col_content .= ' and ';
                    } else if (!empty($final_col_content) && empty($subcontractor_received_nto_date) && empty($subcontractor_received_type_mailing) && !empty($contracted_by_received_nto_date) && !empty($contracted_by_received_type_mailing) && !empty($general_contractor_received_nto_date) && !empty($general_contractor_received_type_mailing) && !empty($owner_received_nto_date) && !empty($owner_received_type_mailing)) {
                        $final_col_content .= ' and ';
                    }

                    if (!empty($subcontractor_received_nto_date) && !empty($subcontractor_received_type_mailing)) {
                        $final_col_content .= (empty($final_col_content) ? ', ' : '') . 'the Sub Contractor ' . $recipient_sub_contractor . ' on ' . $subcontractor_received_nto_date . ' BY ' . $subcontractor_received_type_mailing;
                    } else if (empty($subcontractor_received_nto_date) && empty($subcontractor_received_type_mailing) && !empty($contracted_by_received_nto_date) && !empty($contracted_by_received_type_mailing) && !empty($general_contractor_received_nto_date) && !empty($general_contractor_received_type_mailing) && !empty($owner_received_nto_date) && !empty($owner_received_type_mailing)) {
                        $final_col_content .= (empty($final_col_content) ? ', ' : '') . 'the Sub Contractor ' . $recipient_contracted_by_name . ' on ' . $contracted_by_received_nto_date . ' BY ' . $contracted_by_received_type_mailing;
                    }

                    if (!empty($final_col_content) && !empty($sub_subcontractor_received_nto_date) && !empty($sub_subcontractor_received_type_mailing)) {
                        $final_col_content .= ', also';
                    } else if (!empty($final_col_content) && empty($sub_subcontractor_received_nto_date) && empty($sub_subcontractor_received_type_mailing) && !empty($contracted_by_received_nto_date) && !empty($contracted_by_received_type_mailing) && !empty($general_contractor_received_nto_date) && !empty($general_contractor_received_type_mailing) && !empty($subcontractor_received_nto_date) && !empty($subcontractor_received_type_mailing) && !empty($owner_received_nto_date) && !empty($owner_received_type_mailing)) {
                        $final_col_content .= ', also';
                    }

                    if (!empty($sub_subcontractor_received_nto_date) && !empty($sub_subcontractor_received_type_mailing)) {

                        $final_col_content .=  (empty($final_col_content) ? ', ' : '') . ' a copy to Sub-Sub Contractor, ' . $recipient_sub_sub_contractor . ' on ' . $sub_subcontractor_received_nto_date . ', BY ' . $sub_subcontractor_received_type_mailing;
                    } else if (empty($sub_subcontractor_received_nto_date) && empty($sub_subcontractor_received_type_mailing) && !empty($contracted_by_received_nto_date) && !empty($contracted_by_received_type_mailing) && !empty($subcontractor_received_nto_date) && !empty($subcontractor_received_type_mailing) && !empty($general_contractor_received_nto_date) && !empty($general_contractor_received_type_mailing) && !empty($owner_received_nto_date) && !empty($owner_received_type_mailing)) {
                        $final_col_content .=  (empty($final_col_content) ? ', ' : '') . ' a copy to Sub-Sub Contractor, ' . $recipient_contracted_by_name . ' on ' . $contracted_by_received_nto_date . ', BY ' . $contracted_by_received_type_mailing;
                    }
                }

                /*         * *********Recipients Owner Data**************** */
                //$printing_date = date('F d, Y');
                if(isset($work_order->completed_at)){
                    $printing_date = date('F d, Y', strtotime($work_order->completed_at));
                }else{
                    $printing_date = date('F d, Y');
                }
                //Printing Day
                $printing_day = date('d'); //October 2018
                //Printing Month
                $printing_month = date('F Y');
                $logo = public_path() . '/img/logo.png';
                $date_request = $data['date_request'];
                $year = explode('-', $date_request);

                $year = $year[2];
                $customer_agent = 'CIRA RANGEL';
                //$is_bond_of_claim = "";
                //dd($surety_recipient);
                $static_array_var = [
                    '{{printing_date}}', '{{owners}}', '{{owners_with_label_property_owner}}', '{{owners_with_label_to_property_owner}}', '{{owner_name}}', '{{your_job_reference_no}}', '{{folio}}', '{{bond}}', '{{noc}}', '{{permit}}', '{{company_name}}', '{{company_address}}', '{{company_city_state_zip}}', '{{logo}}', '{{printing_day}}', '{{printing_month}}', '{{contractor_name}}', '{{sub_contractor_name}}', '{{sub_sub_contractor_name}}', '{{title}}', '{{year}}', '{{recorded_day}}', '{{recorded_month}}', '{{state}}', '{{contracted_by}}', '{{legal_description}}', '{{bond_claim_number}}', '{{project_name}}', '{{company_city}}', '{{company_state}}', '{{rescind_work_order}}', '{{amendment}}', '{{is_bond_of_claim}}', '{{general_contractor}}', '{{project_address}}', '{{recipient_owners}}', '{{sub_contractor}}', '{{contracted_by_recipient}}', '{{surety_recipient}}', '{{recipient_sub_sub_contractor}}', '{{recipient_sub_contractor}}', '{{recipient_general_contractor}}', '{{final_col_content}}', '{{recipient_contracted_by_name}}', '{{final_col_owners}}', '{{customer_agent}}', '{{surety_recipient_and_general_contractor}}'
                ];
                $static_array_replace = [
                    $printing_date, $owners, $owners, $owners, $owner_name, $your_job_reference_no,
                    $folio, $bond, $noc, $permit, $company_name, $company_address, $company_city_state_zip,
                    $logo, $printing_day, $printing_month, $contractor_name,
                    $sub_contractor_name, $sub_sub_contractor_name, $title, $year, $recorded_day, $recorded_month,
                    $noticestate, $contracted_by, $legal_description, $bond_claim_number, $project_name, $company_city, $company_state, $rescind_work_order, $amendment, $is_bond_of_claim, $general_contractor, $project_address, $recipient_owners, $sub_contractor, $contracted_by_recipient, $surety_recipient, $recipient_sub_sub_contractor, $recipient_sub_contractor, $recipient_general_contractor, $final_col_content, $recipient_contracted_by_name, $final_col_owners, $customer_agent, $surety_recipient_and_general_contractor
                ];
                //dd($static_array_replace);
                //Merge static and dynamci array
                $variable_arr = array_merge($arr_variable, $static_array_var);
                $replace_arr = array_merge($arr_replace, $static_array_replace);

                $content = str_replace($variable_arr, $replace_arr, $content);
                //template content

                $final_content = '';

                if ($work_order_type && $work_order_type == 'cyo') {

                    $all_recipients = \App\Models\Cyo_WorkOrder_Recipient::leftjoin('cyo__stamps_labels', 'cyo__stamps_labels.recipient_id', '=', 'cyo__work_order__recipients.id')
                        ->leftjoin('cyo__usps_addresses', 'cyo__usps_addresses.recipient_id', '=', 'cyo__work_order__recipients.id')
                        ->where('work_order_id', '=', $work_order_id)
                        ->select('cyo__work_order__recipients.address as ad','cyo__work_order__recipients.category_id','cyo__work_order__recipients.city_id','cyo__work_order__recipients.state_id','cyo__work_order__recipients.zip','cyo__work_order__recipients.attn','cyo__work_order__recipients.name', 'cyo__usps_addresses.*','cyo__stamps_labels.*')
                        ->get();
                } else {
                    $all_recipients = \App\Models\Recipient::leftjoin('stamps_label', 'stamps_label.recipient_id', '=', 'recipients.id')
                        ->leftjoin('usps_address', 'usps_address.recipient_id', '=', 'recipients.id')
                        ->where('work_order_id', '=', $work_order_id)
                        ->select('recipients.address as ad','recipients.city_id','recipients.category_id','recipients.state_id','recipients.zip','recipients.attn','recipients.name', 'usps_address.*','stamps_label.*')
                        ->get();
                }

                //BOTOTM:: if cat_id in (21, 2) then contract_by(21) show bottom
                $bothContractedByAndGeneralContractor =  $all_recipients->whereIn('category_id', [21, 2])
                    ->groupBy('category_id')->toArray();
                //$showContractedByToBottom = ($bothContractedByAndGeneralContractor->count() == 2) ? true: false;

                //TOP:: if cat_id != 2 then 21 will show
                //$showContractedByToTop = !array_key_exists(2, $bothContractedByAndGeneralContractor);
                //$showContractedByByGcToTop = array_key_exists(2, $bothContractedByAndGeneralContractor) ? 2 : (array_key_exists(21, $bothContractedByAndGeneralContractor) ? 21 : null);

                $showContractedByByGcToTop = array_key_exists(2, $bothContractedByAndGeneralContractor) ? 'GC' : (array_key_exists(21, $bothContractedByAndGeneralContractor) ? 'CB' : null);

                $allrecipientfinalcontent = '<table style = "width: 680px;font-size: 10px;margin: 0 auto;table-layout: fixed;page-break-after:always;">';
                $master_notice_id = [config('constants.MASTER_NOTICE')['BCOL']['ID'], config('constants.MASTER_NOTICE')['SBC']['ID'], config('constants.MASTER_NOTICE')['WRL']['ID']];


                if (strpos($content, '{{firm_with_label_noticing_party}}')) {
                    $firm_label = 'NOTICING PARTY:';
                    $content = str_replace('{{firm_with_label_noticing_party}}', "", $content);
                } else if (strpos($content, '{{firm_with_label_notifying_party}}')) {
                    $firm_label = 'Notifying Party:';
                    $content = str_replace('{{firm_with_label_notifying_party}}', "", $content);
                } else if (strpos($content, '{{firm_with_label_claimant}}')) {
                    $firm_label = 'CLAIMANT:';
                    $content = str_replace('{{firm_with_label_claimant}}', "", $content);
                } else {
                    $firm_label = 'Firm:';
                    $content = str_replace('{{firm_with_label_noticing_party}}', "", $content);
                }
                if ($notice_type->type == 2) {
                    if (isset($your_job_reference_no) && !empty($your_job_reference_no)) {
                        $your_job_reference_no = "W/O #" . $your_job_reference_no;
                    }
                }

                if ($work_order_type && $work_order_type == 'cyo') {
                    $c_name = "";
                }else{
                    if (strpos($content, 'CIRA RANGEL, Agent')) {
                        $c_name = '';
                    } else {
                        $c_name = 'CIRA RANGEL, Agent';
                    }
                }

                $master_notice_auth_sign = [config('constants.MASTER_NOTICE')['BCOL']['ID'], config('constants.MASTER_NOTICE')['WRL']['ID'], config('constants.MASTER_NOTICE')['SBC']['ID'], config('constants.MASTER_NOTICE')['NPN']['ID']];
                if (in_array($notice_type->master_notice_id, $master_notice_auth_sign)) {
                    $c_name = "";
                }
                if (strpos($content, '{{customer_telephone}}')) {
                    $customer_telephone = $customer_telephone . '<br/>';
                    $content = str_replace('{{customer_telephone}}', "", $content);
                } else {
                    $customer_telephone = '';
                }

                if (isset($all_recipients) && count($all_recipients) > 0) {
                    //All Recipient on main page
                    $allrecipientshtml = '';
                    if ($notice_type->is_claim_of_lien != 1) {
                        $allrecipientshtml = $allrecipientshtml . '<tr>
                    <td  colspan="3">
                        <table>
                <tr>
                                <td style="vertical-align: top; font-size: 10px; line-height: 10px;">' . $firm_label . '</td>
                                                <td style="padding:2px; font-size: 10px; line-height: 10px;">' . $company_name . '<br />
                                ' . $company_address . '<br />
                    ' . $customer_telephone
                            . $company_city_state_zip . '<br/>' . $your_job_reference_no . '<p style="font-weight: bold; font-size: 10px; line-height: 10px;">' . $c_name . '</p></td>
                            </tr>
                                            
                        
                    </table>
            </td>
            </tr>';
                    }
                    //dd($all_recipients);
                    $surety = 1;
                    $con = 1;
                    $i = 1;

                    if (!in_array($notice_type->master_notice_id, $master_notice_id)) {
                        foreach ($all_recipients as $k => $each) {
                            if (isset($each->state_id) && $each->state_id != NULL) {
                                $each_sub_state_name = \App\Models\State::find($each->state_id);
                                $each_sub_state = $each_sub_state_name->name;
                            }
                            if (isset($each->city_id) && $each->city_id != NULL) {
                                $each_sub_city_name = \App\Models\City::find($each->city_id);
                                $each_sub_city = $each_sub_city_name->name;
                            }
                            if ($each->city_id != NULL || $each->state_id != NULL) {
                                $each_city_state_zip = "";
                                if ($each->city_id != NULL) {
                                    $each_city_state_zip .= $each_sub_city . ' ';
                                }
                                if ($each->state_id != NULL) {
                                    $each_city_state_zip .= $each_sub_state . ' ';
                                }
                                $each_city_state_zip .= $each->zip;
                            } else {
                                $each_city_state_zip = '';
                            }
                            if ((!($each->category_id == 22  && $notice_type->owners_html_in_pdf == 1) && !($each->category_id == 1  && $notice_type->owners_html_in_pdf == 1))) {
                                if ($notice_type->is_claim_of_lien != 1) {
                                    if ($notice_type->master_notice_id == config('constants.MASTER_NOTICE')['NPN']['ID']) {
                        

                                        if ($each->category_id == 9) {
                                            /* if ($surety != 1) {
                                                $all_attn = $each->attn != "" ? '<div style="width:200.33px;"> ' . $each->attn . '</div>' : "";

                                                $address = $each->address;
                                                $address .= ((!empty($each->address2)) ? '<br />' : '').$each->address2;
                                                $address .= ((!empty($each->address3)) ? '<br />' : '').$each->address3;
                                                
                                                $allrecipientshtml = $allrecipientshtml . '
                                    <td style="width: 30%;padding: 1px 10px;text-align:left;font-size: 10px; line-height: 10px;" scope="col" >
                                        <div style="width:200.33px;">' . $each->tracking_number . '</div>
                                        <div style="width:200.33px;">' . $each->name . '</div>' . $all_attn . '
                                        <div style="width:200.33px;">' . nl2br($address) . '</div>
                                        <div>' . $each_city_state_zip . '</div>
                                                                                    
                                            </td>';
                                            }
                                            $surety++;
                                        
                                            if ($i == 3) {
                                                $allrecipientshtml = $allrecipientshtml . '</tr>
                                                    </table>
                                                    </td>
                                                    </tr>';
                                                $i = 0;
                                            }
                                            $i++; */
                                        } else if ($each->category_id == 21 && $showContractedByByGcToTop == 'CB') {

                                        }else if ($each->category_id == 2 && $showContractedByByGcToTop == 'GC') {
                                        }
                                        else {

                                            if ($i == 1) {
                                                $allrecipientshtml = $allrecipientshtml . '<tr>
                                                <td colspan = "3">
                                                <table>
            
                                                <tr>';
                                            }
                                        $each->attn = str_lreplace($each->attn);
                                        $all_attn = strtoupper($each->attn) != "" ? '<div style="width:200.33px;"> ' . strtoupper($each->attn) . '</div>' : "";

                                            //$address = $each->address;
                                            $address = (!empty($each->address)) ? $each->address : $each->ad;
                                    
                                            $address .= ((!empty($each->address2)) ? '<br />' : '').$each->address2;
                                            $address .= ((!empty($each->address3)) ? '<br />' : '').$each->address3;
                                            
                                            $allrecipientshtml = $allrecipientshtml . '
                                    <td style="width: 30%;padding: 1px 10px;text-align:left;font-size: 10px; line-height: 10px;" scope="col" >
                                        <div style="width:200.33px;">' . $each->tracking_number . '</div>
                                        <div style="width:200.33px;">' . $each->name . '</div>' . $all_attn . '
                                        <div style="width:200.33px;">' . nl2br($address) . '</div>
                                        <div>' . $each_city_state_zip . '</div>
                                                                                    
                                            </td>';
                                        //  dump($allrecipientshtml);
                                            if ($i == 3) {
                                                $allrecipientshtml = $allrecipientshtml . '</tr>
                                                    </table>
                                                    </td>
                                                    </tr>';
                                                $i = 0;
                                            }
                                            $i++;
                                        } 

                                        
                                    } else {
                                        if ($i == 1) {
                                            $allrecipientshtml = $allrecipientshtml . '<tr>
                                                <td colspan = "3">
                                                <table>
                                            <tr>';
                                        }
                                        $each->attn = str_lreplace($each->attn);
                                        $all_attn = strtoupper($each->attn) != "" ? '<div style="width:200.33px;"> ' . strtoupper($each->attn) . '</div>' : "";


                                    //  $address = $each->address;
                                    $address = (!empty($each->address)) ? $each->address : $each->ad;
                                    
                                        $address .= ((!empty($each->address2)) ? '<br />' : '').$each->address2;
                                        $address .= ((!empty($each->address3)) ? '<br />' : '').$each->address3;

                                        $allrecipientshtml = $allrecipientshtml . '
                                <td style="width: 30%;padding: 1px 10px;text-align:left;font-size: 10px; line-height: 10px;" scope="col" >
                                    <div style="width:200.33px;">' . $each->tracking_number . '</div>
                                    <div style="width:200.33px;">' . $each->name . '</div>' . $all_attn . '
                                    <div style="width:200.33px;">' . nl2br($address) . '</div>
                                    <div>' . $each_city_state_zip . '</div>
                                                                                
                                            </td>';
                                        //dump($allrecipientshtml);
                                        if ($i == 3) {
                                            $allrecipientshtml = $allrecipientshtml . '</tr>
                                                </table>
                                                </td>
                                            </tr>';
                                            $i = 0;
                                        }
                                        $i++;
                                    }
                                }
                            }
                        }
                    }
                    if ($i == 3) {
                        $allrecipientshtml .= '</tr>
                                            </table> 
                                            </td>
                                        </tr>';
                    }
                    
                    $allrecipientfinalcontent = $allrecipientfinalcontent . $allrecipientshtml;
                    $last_html = "</table>";
                    $allrecipientfinalcontent = $last_html . $allrecipientfinalcontent;

                    if (strpos($content, '{{recipients_html}}') !== false) {
                        $final_content = str_replace('{{recipients_html}}', $allrecipientfinalcontent, $content);
                    } else {
                        $final_content = $content . $allrecipientfinalcontent;
                    }
                    //$final_content = $content . $allrecipientfinalcontent;
                    //$content = $content . $allrecipientfinalcontent . '</table><table style="width: 680px;font-size: 11px;margin: 0 auto;table-layout: fixed;page-break-after:always;page-break-before:always;height:750px;position:relative;">';
                }

                /**
                 * Below code is for to display second page if it is label or firm mail
                 */
                if ($final_content != '') {
                    $content_pdf = $final_content;
                } else {
                    $content_pdf = $content;
                }
                $surety_bond = '';
                if (config('constants.MASTER_NOTICE')['BCOL']['ID'] == $notice_type->master_notice_id) {
                    if (strpos($content_pdf, '{{surety_bond}}')) {
                        $new_content_pdf = '';
                        if (isset($recipients) && count($recipients) > 0) { //dd($recipients_is_bond_of_claim);
                            $o=1;
                            foreach ($recipients as $k => $each_recipient) {
                                if ($each_recipient->category_id == 9) {
                                    $bond_of_claim_firm_mailing = $each_recipient->tracking_number;

                                    $bond_of_claim_name = $each_recipient->name;
                                    $each_recipient->attn = str_lreplace($each_recipient->attn);
                                    $bond_of_claim_attn = strtoupper($each_recipient->attn) != "" ? " " . strtoupper($each_recipient->attn) . "<br/>" : "";
                                    // $bond_of_claim_attn = $each_recipient->attn != "" ? " " . $each_recipient->attn . "<br/>" : "";
                                    //$bond_of_claim_address = $each_recipient->address;
                                    $bond_of_claim_address = (!empty($each_recipient->address)) ? $each_recipient->address : $each_recipient->ad;
                                    
                                    $state = "";
                                    if (isset($each_recipient->state_id) && $each_recipient->state_id != null) {
                                        $state_name = \App\Models\State::find($each_recipient->state_id);
                                        $state = $state_name->name;
                                    }
                                    $city = "";
                                    if (isset($each_recipient->city_id) && $each_recipient->city_id != null) {
                                        $city_name = \App\Models\City::find($each_recipient->city_id);
                                        $city = $city_name->name;
                                    }
                                    if ($each_recipient->city_id != NULL || $each_recipient->state_id != NULL) {
                                        $bond_of_claim_city_state_zip = $city . ' ' . $state . ' ' . $each_recipient->zip;
                                    } else {
                                        $bond_of_claim_city_state_zip = '';
                                    }

                                    if ($o == 1) {
                                        $surety_bond .= '<tr>';
                                    }

                                    $surety_bond .= '<td style="padding: 0 2px;word-wrap:break-word; word-break:break-all;font-size:10px; line-height: 10px;">' . $bond_of_claim_firm_mailing . '<br/>
                                                                                    <p style="width: 200px; margin-bottom: 0;margin-top: 0;font-size:10px; line-height: 10px;">' . $bond_of_claim_name . '</p>
                                                                                        ' . $bond_of_claim_attn . $bond_of_claim_address . '<br/>
                                                                                            ' . $bond_of_claim_city_state_zip . '<br/></td>';
                                    if ($o == 3) {
                                        $surety_bond .= '</tr>';
                                        $o = 0;
                                    }
                                    $o++;
                                }
                            }

                            if ($o == 3) {
                                $surety_bond = $surety_bond . '</tr>';
                            }
                            if (!empty($surety_bond)) {
                                $surety_bond = '<table><tbody><tr>
                        <td style="vertical-align: top;padding: 0 2px;font-size:10px; line-height: 10px;">TO:</td><td><table>' . $surety_bond . '</table></td></tr></tbody></table>';
                            }
                            $content =  str_replace('{{surety_bond}}', $surety_bond, $content_pdf);
                            $new_content_pdf = $new_content_pdf . $content;
                        } else {
                            $new_content_pdf = $content_pdf;
                        }
                        if ($new_content_pdf == '') {
                            $new_content_pdf = $content_pdf;
                        }
                        $content_pdf = $new_content_pdf;
                    }
                }
                $watermark_Notices = [config('constants.MASTER_NOTICE')['NTO']['ID'], config('constants.MASTER_NOTICE')['ITL']['ID'], config('constants.MASTER_NOTICE')['NPN']['ID'], config('constants.MASTER_NOTICE')['COL']['ID'], config('constants.MASTER_NOTICE')['SOL']['ID'], config('constants.MASTER_NOTICE')['BCOL']['ID'], config('constants.MASTER_NOTICE')['SBC']['ID'], config('constants.MASTER_NOTICE')['WRL']['ID']];
                $CYO_watermark_Notices = [config('constants.MASTER_NOTICE')['NTO']['ID'], config('constants.MASTER_NOTICE')['ITL']['ID'], config('constants.MASTER_NOTICE')['NPN']['ID'], config('constants.MASTER_NOTICE')['COL']['ID'], config('constants.MASTER_NOTICE')['SOL']['ID'], config('constants.MASTER_NOTICE')['WRL']['ID']];

                if (Auth::user()->hasRole('customer')) {
                    if (isset($request->type_work_order) && $request->type_work_order == 'cyo') {;
                        if (in_array($notice_type->master_notice_id, $CYO_watermark_Notices)) {
                            $content_pdf = '<html>
                    <head><style>' . config('constants.PDF_WATERMARK_CSS') . '</style>
                    </head>
                    <body>
                        ' . config('constants.PDF_WATERMARK_HTML') . '<main>' . $content_pdf . ' </main> </body>
                    </html>';
                        }
                    } else {
                        if (in_array($notice_type->master_notice_id, $watermark_Notices)) {
                            $content_pdf = '<html>
                    <head><style>' . config('constants.PDF_WATERMARK_CSS') . '</style>
                    </head>
                    <body>
                        ' . config('constants.PDF_WATERMARK_HTML') . '<main>' . $content_pdf . ' </main> </body>
                    </html>';
                        }
                    }
                }

                $pdf = PDF::loadHTML($content_pdf); //setOptions(['debugLayout'=> true])->
                //dd($pdf);
                $not_mailed_out_master_ids = [config('constants.MASTER_NOTICE')['SBC']['ID'], config('constants.MASTER_NOTICE')['WRL']['ID']];


               
                if ($work_order->file_name == NULL) {
                    $file_name = $work_order_id . '_' . uniqid() . '.pdf';
                } else {
                    $file_name = $work_order->file_name;
                }
                                if (isset($request->type_work_order) && $request->type_work_order == 'cyo') {
                                if (!file_exists(public_path() . '/pdf/cyo_work_order_document')) {
                                    mkdir(public_path() . '/pdf/cyo_work_order_document', 0777, true);
                                }
                                if (!file_exists(public_path() . '/pdf/cyo_work_order_document/' . $file_name)) {
                                    $pdf->save(public_path() . '/pdf/cyo_work_order_document/' . $file_name);
                                } else {
                                    unlink(public_path() . '/pdf/cyo_work_order_document/' . $file_name);
                                    $pdf->save(public_path() . '/pdf/cyo_work_order_document/' . $file_name);
                                }
                                //update file name
                                $work_order->file_name = $file_name;
                                /*$work_order->status = 6;
                                $work_order->completed_at = date("Y-m-d H:i:s");*/
                                $work_order->save();

                                $chmod_cyo = chmod(public_path() . '/pdf/cyo_work_order_document/' . $file_name, 0777); 
                                $mypdf[] = asset('/') . 'pdf/cyo_work_order_document/' . $file_name;
                                $myprint_pdf[] = public_path() . '/pdf/cyo_work_order_document/' . $file_name;

                                //echo "<script>window.open('$mypdf');</script>";
                            } else {
                                if (!file_exists(public_path() . '/pdf/work_order_document')) {
                                    mkdir(public_path() . '/pdf/work_order_document', 0777, true);
                                }

                                if (!file_exists(public_path() . '/pdf/work_order_document/' . $file_name)) {
                                    //$pdfMerger->duplexMerge();
                                    $pdf->save(public_path() . '/pdf/work_order_document/' . $file_name);
                                } else {
                                    unlink(public_path() . '/pdf/work_order_document/' . $file_name);
                                    //$pdfMerger->duplexMerge();
                                    $pdf->save(public_path() . '/pdf/work_order_document/' . $file_name);
                                }
                            $re = chmod(public_path() . '/pdf/work_order_document/' . $file_name, 0777); 
                                //dd(public_path() . '/pdf/work_order_document/' . $file_name,$re);
                                //update file name
                            if(isset($request->type_of_label) && $request->type_of_label!=''){

                            }
                                $work_order->file_name = $file_name;
                                //$work_order->status = 6;
                                //$work_order->completed_at = date("Y-m-d H:i:s");
                                $work_order->save();
                                $mypdf[] = asset('/') . 'pdf/work_order_document/' . $file_name;
                                $myprint_pdf[] = public_path() . '/pdf/work_order_document/' . $file_name;

                            //  echo "<script>window.open(<?php $mypdf ?);</script>";
                            }
                        
            // return $pdf->stream();
    }
    $pdfMerger = PDFMerger::init();
    foreach ($myprint_pdf as $key => $value) {
        $pdfMerger->addPDF($value, 'all');

    }
    $pdfMerger->merge();
    $pdfMerger->save(public_path() .'/pdf/CompletedWO.pdf');
    $combine_pdf = asset('/').'public/pdf/CompletedWO.pdf';
    return response()->json(['status' => 'success', 'message' => $combine_pdf]);
  }

}
