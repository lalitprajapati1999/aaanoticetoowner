<?php

namespace App\Http\Controllers\AccountManager;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Mail\AddNote;
use Illuminate\Support\Facades\Mail;
use Yajra\Datatables\Datatables;
use App\Models\WorkOrder;
use DB;
use App\Mail\WorkOrderNote;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Alert;
use PDF;
use Illuminate\Support\Facades\Validator;

class ResearchController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $customer_id = Auth::User()->customer->id;
        $data['categories'] = \App\Models\Category::select()->orderBy('name', 'ASC')->get();
        $data['project_roles'] = \App\Models\ProjectRoles::select()->orderBy('name', 'ASC')->get();
        $data['account_managers'] = \App\Models\User::select('id', 'name')->join('role_users', 'role_users.user_id', 'users.id')->where('role_users.role_id', 3)->orderBy('name', 'asc')->get();
        // dd($data['account_managers']);
        if (Auth::user()->hasRole('account-manager')) {
            $data['customers'] = \App\Models\Customer::where('account_manager_id', '=', Auth::user()->id)->orderBy('company_name', 'ASC')->where('status',1)->get();
            $data['customers_order'] = \App\Models\Customer::where('account_manager_id', '=', Auth::user()->id)->join('users', 'users.id', '=', 'customers.user_id')->orderBy('name', 'ASC')->get();

            $accntMgrAssignCustomerIds = $data['customers']->pluck('id')->toArray();
            $data['contracted_by'] = \App\Models\Contact::select()->whereIn('customer_id', $accntMgrAssignCustomerIds)->groupBy('company_name')->orderBy('company_name', 'ASC')->get();
        } else {
            $data['customers'] = \App\Models\Customer::orderBy('company_name', 'ASC')->where('status',1)->get();
            $data['customers_order'] = \App\Models\Customer::orderBy('name', 'ASC')->join('users', 'users.id', '=', 'customers.user_id')->get();

            $data['contracted_by'] = \App\Models\Contact::select()->groupBy('company_name')->orderBy('company_name', 'ASC')->get();
        }
        $data['states'] = \App\Models\State::select()->orderBy('name')->get();
        $data['WorkOrders'] = \App\Models\WorkOrder::select()->where('customer_id', $customer_id)->paginate(10);

        // dd($data['WorkOrders']);

        return view('account_manager.research.list', $data);
    }

       public function get_note_email($id){
            $work_order_status = \App\Models\WorkOrder::find($id);
            $results = get_user_notice_email($work_order_status->customer_id);
            $html='';
            $html .= '<option value="">Select</option>';
            foreach($results as $result) {
            $html .= "<option value=$result>$result</option>";
            }
            return $html;
        }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $notice_id, $is_rescind)
    {
        $data['is_rescind'] = $is_rescind;
        $notice = \App\Models\Notice::select('name', 'id', 'type', 'is_claim_of_lien', 'master_notice_id')->where('id', $notice_id)->get()->toArray();
        $data['rush_hour'] = \App\Models\WorkOrder::where(['id' => $id])->pluck('rush_hour_charges');
        $data['notice'] = $notice[0];
        $data['label_generated_count'] = 0;
        $data['countries'] = \App\Models\Country::select()->get();
        $data['states'] = \App\Models\State::select()->orderBy('name', 'asc')->get();
        $data['categories'] = \App\Models\Category::select()->orderBy('name', 'ASC')->get();
        $data['project_roles'] = \App\Models\ProjectRoles::select()->orderBy('name', 'ASC')->get();
        $data['recipients'] = \App\Models\Recipient::select('recipients.id', 'work_order_id', 'category_id', 'name', 'contact', 'mobile', 'recipients.address', 'city_id', 'state_id', 'zip', 'fax', 'recipients.created_at', 'recipients.updated_at', \DB::raw('usps_address.address as usps_address'), \DB::raw('usps_address.city as usps_city'), \DB::raw('usps_address.state as usps_state'), 'zipcode', 'zipcode_add_on', 'dpb', 'check_digit', 'comment')->leftJoin('usps_address', 'usps_address.recipient_id', '=', 'recipients.id')->where('work_order_id', $id)->get();
        $data['rush_hour'] = \App\Models\WorkOrder::select('rush_hour_charges', 'rush_hour_amount')->where('id', $id)->get();
        $verified_address_count = \App\Models\Recipient::select(\DB::raw('COUNT(usps_address.recipient_id) as verified_address_count'))->leftJoin('usps_address', 'usps_address.recipient_id', '=', 'recipients.id')->where('work_order_id', $id)->get();
        $data['verified_address_count'] = ($verified_address_count) ? $verified_address_count[0]['verified_address_count'] : 0;

        $data['projectTypes'] = \App\Models\ProjectType::select()->get();
        $data['count'] = \App\Models\Recipient::select(DB::raw('COUNT(*) AS total'), 'categories.name')->groupBy('recipients.category_id')->join('categories', 'categories.id', '=', 'recipients.category_id')->where('work_order_id', $id)->get();
        $data['id'] = $id;
        $data['notice_id'] = $notice_id;
        $result = \App\Models\WorkOrder::select('project_address_count', 'state_id', 'customer_id')->join('notice_fields', 'notice_fields.notice_id', '=', 'work_orders.notice_id')->where('work_orders.id', $id)->limit(1)->get()->toArray();
        $data['project_address_count'] = $result[0]['project_address_count'];
        $data['customer_id'] = $result[0]['customer_id'];
        $data['customer_details'] = \App\Models\Customer::find($data['customer_id']);
        $data['officers_directors'] = \App\Models\CustomerAgent::where('customer_id', '=', $data['customer_id'])
            ->get();

        $data['names'] = \App\Models\Contact::select('contacts.id', 'contacts.company_name')
            // ->where('contact_details.type', '=', 0)
            ->where('contacts.customer_id', '=', $data['customer_id'])
            // ->Join('contact_details', 'contact_details.contact_id', '=', 'contacts.id')
            ->orderBy('contacts.company_name', 'ASC')
            ->get();

        $data['attachment'] = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $id)
            ->get();

        $data['service_types'] = \App\Models\StampsServices::where(['status' => '1'])->get(['description', 'abbr', 'id'])->toArray();
        $data['package_types'] = \App\Models\StampsAddOns::join('stamps_service_package_addon', 'stamps_service_package_addon.add_on', '=', 'stamps_add_ons.id')
            ->join('stamps_service_type', 'stamps_service_type.id', '=', 'stamps_service_package_addon.service_type')
            ->join('stamps_packages', 'stamps_packages.id', '=', 'stamps_service_package_addon.package')
            ->where(['stamps_packages.status' => '1'])
            ->get([\DB::raw('DISTINCT(stamps_packages.name)'), 'stamps_packages.id'])->toArray();

        if ($data['recipients']) {
            for ($cnt = 0; $cnt < count($data['recipients']); $cnt++) {
                $labels = \App\Models\StampsLabel::leftJoin('stamps_service_type', 'stamps_service_type.id', 'stamps_label.service_type')
                    ->leftJoin('stamps_packages', 'stamps_packages.id', 'stamps_label.package_type')
                    ->where(['recipient_id' => $data['recipients'][$cnt]['id']])
                    ->first(['stamps_label.*']); // dd($labels);
                if (!empty($labels) && $labels['generated_label'] == 'yes') {
                    $data['label_generated_count']++;
                    $data['recipients'][$cnt]['label_generated'] = 'yes';
                } else {
                    $data['recipients'][$cnt]['label_generated'] = 'no';
                }
                $data['recipients'][$cnt]['recipient_shipping_labels'] = $labels;
                $data['recipients'][$cnt]['add_ons'] = \App\Models\StampsAddOns::join('stamps_service_package_addon', 'stamps_service_package_addon.add_on', '=', 'stamps_add_ons.id')
                    ->join('stamps_service_type', 'stamps_service_type.id', '=', 'stamps_service_package_addon.service_type')
                    ->join('stamps_packages', 'stamps_packages.id', '=', 'stamps_service_package_addon.package')
                    ->where(['stamps_add_ons.status' => '1'])
                    ->where('stamps_service_type.id', '=', 1)
                    ->where('stamps_packages.id', '=', 2)
                    // ->where(function($query) use ($labels) {  
                    //     if(isset($labels) && !empty($labels))
                    //     {
                    //         $query->where('stamps_service_type.id','=',$labels['service_type']);
                    //         $query->where('stamps_packages.id','=',$labels['package_type']);
                    //     }	
                    // })
                    ->get([\DB::raw('DISTINCT(stamps_add_ons.id)'), 'stamps_add_ons.description'])->toArray();

                if (!empty($data['recipients'][$cnt]['add_ons'])) {
                    $cnt1 = 1;
                    $temp_add_ons = $data['recipients'][$cnt]['add_ons'];
                    for ($counter = 0; $counter < count($temp_add_ons); $counter++) {
                        $prohibited = \App\Models\StampsAddOnsProhibited::join('stamps_add_ons', 'stamps_add_ons_prohibited.prohibited', '=', 'stamps_add_ons.id')
                            ->where(['stamps_add_ons_prohibited.add_on' => $temp_add_ons[$counter]['id']])
                            ->pluck('prohibited')->toArray();

                        $required = \App\Models\StampsAddOnsRequired::join('stamps_add_ons', 'stamps_add_ons_required.required', '=', 'stamps_add_ons.id')
                            ->where(['stamps_add_ons_required.add_on' => $data['recipients'][$cnt]['add_ons'][$counter]['id']])
                            ->pluck('required')->toArray();
                        $temp_add_ons[$counter]['prohibited'] = json_encode($prohibited);
                        $temp_add_ons[$counter]['required'] = json_encode($required);
                    }
                    unset($data['recipients'][$cnt]['add_ons']);
                    $data['recipients'][$cnt]['add_ons'] = $temp_add_ons;
                }
            }
        }

        //dd($data['recipients']);
        $work_order_status = \App\Models\WorkOrder::find($id);
        $data['is_rescind'] = $work_order_status->is_rescind;
        if ($work_order_status->status == '0') {
            $data['status'] = 'draft';
        } else {
            $data['status'] = 'request';
        }
        $data['work_order_status'] = $work_order_status->status;
        $data['cities'] = [];

        /* Notes and correction * */
        $admin_email = \App\Models\SecondaryDocument::getAdminEmail();

        $data['note_emails'] = get_user_notice_email($work_order_status->customer_id);
        $data['notes'] = \App\Models\WorkOrderNotes::leftjoin('users', 'users.id', '=', 'work_order_notes.user_id')
            ->select('work_order_notes.*', 'users.name')
            ->where('customer_id', '=', $work_order_status->customer_id)
            ->where('work_order_notes.work_order_id', '=', $id)
            ->orderBy('work_order_notes.id', 'DESC')
            ->get();
        $data['corrections'] = \App\Models\WorkOrderCorrections::leftjoin('users', 'users.id', '=', 'work_order_corrections.user_id')
            ->select('work_order_corrections.*', 'users.name')
            ->where('customer_id', '=', $work_order_status->customer_id)
            ->where('work_order_corrections.work_order_id', '=', $id)
            ->orderBy('work_order_corrections.id', 'DESC')
            ->get();

        $data['parent_wo_list'] = \App\Models\WorkOrder::select('id', DB::raw('CONCAT("#",id) AS `parent_id`'))
            ->where('id', '!=', $id)
            ->pluck('parent_id', 'id')
            ->toArray();

        $data['customer_id'] = $work_order_status->customer_id;

        /* Notes and correction * */
        if ($notice[0]['type'] == 2) {
            $data['notice_fields_section1'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.workorder_id', $id)->where('work_order_fields.notice_id', $notice_id)->where('notice_fields.section', 1)->orderBy('sort_order', 'asc')->get();

            $data['notice_fields_section2'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.notice_id', $notice_id)->where('work_order_fields.workorder_id', $id)->where('notice_fields.section', 2)->orderBy('sort_order', 'asc')->get();
            $data['tab'] = 'project';
            //To display parent work order
            foreach ($data['notice_fields_section1'] as $key => $value) {
                $key = explode("_", $key);

                if ($value) {

                    if ($value->name == 'parent_work_order') {
                        $parent_work_order = $value->value;
                        $data['parent_work_order'] = $parent_work_order;
                    }
                    if ($value->name == 'project_address') {
                        $project_address = $value->value;
                        $project_address = explode('**', $project_address);
                        $data['project_address_count'] = count($project_address);
                    }
                }
            }
            return view('account_manager.research.edit_soft_notices', $data);
        } else {
            $data['tab'] = 'project';
            $data['notice_fields'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.notice_id', $notice_id)->where('work_order_fields.workorder_id', $id)->orderBy('sort_order', 'asc')->get();

            //To display parent work order
            foreach ($data['notice_fields'] as $key => $value) {
                $key = explode("_", $key);

                if ($value) {

                    if ($value->name == 'parent_work_order') {
                        $parent_work_order = $value->value;
                        $data['parent_work_order'] = $parent_work_order;
                    }
                }
            }
            return view('account_manager.research.edit', $data);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $notice_id, $customer_id, $is_rescind)
    {
        $data['is_rescind'] = $is_rescind;
        $data['service_types'] = \App\Models\StampsServices::where(['status' => '1'])->get(['description', 'abbr', 'id'])->toArray();
        $data['package_types'] = \App\Models\StampsAddOns::join('stamps_service_package_addon', 'stamps_service_package_addon.add_on', '=', 'stamps_add_ons.id')
            ->join('stamps_service_type', 'stamps_service_type.id', '=', 'stamps_service_package_addon.service_type')
            ->join('stamps_packages', 'stamps_packages.id', '=', 'stamps_service_package_addon.package')
            ->where(['stamps_packages.status' => '1'])
            ->get([\DB::raw('DISTINCT(stamps_packages.name)'), 'stamps_packages.id'])->toArray();
        $data['add_ons'] = \App\Models\StampsAddOns::join('stamps_service_package_addon', 'stamps_service_package_addon.add_on', '=', 'stamps_add_ons.id')
            ->join('stamps_service_type', 'stamps_service_type.id', '=', 'stamps_service_package_addon.service_type')
            ->join('stamps_packages', 'stamps_packages.id', '=', 'stamps_service_package_addon.package')
            ->where(['stamps_add_ons.status' => '1'])
            ->where(['stamps_service_type.id' => 1])
            ->where(['stamps_packages.id' => 2])
            ->get([\DB::raw('DISTINCT(stamps_add_ons.id)'), 'stamps_add_ons.description'])->toArray();

        if (!empty($data['add_ons'])) {
            for ($counter = 0; $counter < count($data['add_ons']); $counter++) {
                $prohibited = \App\Models\StampsAddOnsProhibited::join('stamps_add_ons', 'stamps_add_ons_prohibited.prohibited', '=', 'stamps_add_ons.id')
                    ->where(['stamps_add_ons_prohibited.add_on' => $data['add_ons'][$counter]['id']])
                    ->pluck('prohibited')->toArray();

                $required = \App\Models\StampsAddOnsRequired::join('stamps_add_ons', 'stamps_add_ons_required.required', '=', 'stamps_add_ons.id')
                    ->where(['stamps_add_ons_required.add_on' => $data['add_ons'][$counter]['id']])
                    ->pluck('required')->toArray();
                $data['add_ons'][$counter]['prohibited'] = json_encode($prohibited);
                $data['add_ons'][$counter]['required'] = json_encode($required);
            }
        }
        $data['label_generated_count'] = 0;
        $data['customer_details'] = \App\Models\Customer::find($customer_id);
        // $fields = array_merge($request->except(['_token', 'continue', 'type', 'notice_id', 'edit_soft_notices_attachment', 'file_name', 'doc_input', 'hidden_city_name', 'customer_id', 'duplicate', 'remove_doc', 'recipient_county_id', 'visibility', 'recipient_notary_county_id', 'recipient_project_county_id', 'recipient_recorded_county_id', 'project_address_count']));
        $fields = array_merge($request->except(['_token', 'continue', 'type', 'notice_id', 'edit_soft_notices_attachment', 'file_name', 'doc_input', 'hidden_city_val', 'hidden_city_name', 'customer_id', 'duplicate', 'remove_doc', 'parent_work_order_previous', 'recipient_county_id', 'contracted_by_exist', 'document_type', 'work_order_attachment', 'visibility', 'recipient_notary_county_id', 'recipient_project_county_id', 'recipient_recorded_county_id', 'project_address_count']));

        //Update count of project addresses
        \App\Models\WorkOrder::where(['id' => $id])->update(['project_address_count' => $request->project_address_count]);
        $fieldsData = [];
        $values = [];
        $i = 0;
        foreach ($fields as $key => $value) {
            $field = explode("_", $key);
            $fieldsData[$i] = $field['0'];
            $values[$i] = $value;
            $i++;
        }
        //dd($fields);

        $workorderiddetails = \App\Models\WorkOrder::find($id);
        $userId = $workorderiddetails->user_id;
        $data['work_order_status'] = $workorderiddetails->status;
        if ($request->duplicate == 'duplicate') {
            //$customerId = Auth::user()->customer->id;
            $account_manager_id = Auth::user()->customer->account_manager_id;
            $WorkOrders = \App\Models\WorkOrder::find($id);

            $userId = $WorkOrders->user_id;
            if ($request->continue == 'Continue') {
                $result1 = WorkOrder::create([
                    'notice_id' => $notice_id,
                    'customer_id' => $customer_id,
                    'order_no' => str_random(5),
                    'user_id' => $userId,
                    'status' => '1',
                    'account_manager_id' => $WorkOrders->account_manager_id,
                ]);
                $data['status'] = 'request';
            } else {
                $result1 = WorkOrder::create([
                    'notice_id' => $notice_id,
                    'customer_id' => $customer_id,
                    'order_no' => str_random(5),
                    'user_id' => $userId,
                    'status' => '0',
                    'account_manager_id' => $WorkOrders->account_manager_id,
                ]);
                $data['status'] = 'draft';
            }



            $work_order_id = $result1->id;
            /* $result2 = \App\Models\WorkOrderFields::select()->where('workorder_id', $id)->delete(); */
            foreach ($fieldsData as $key => $value) {

                if ($values[$key] == NULL) {
                    $result2 = \App\Models\WorkOrderFields::create([
                        'workorder_id' => $work_order_id,
                        'notice_id' => $request->notice_id,
                        'notice_field_id' => $value,
                        'value' => '',
                    ]);
                } else
                    $result2 = \App\Models\WorkOrderFields::create([
                        'workorder_id' => $work_order_id,
                        'notice_id' => $request->notice_id,
                        'notice_field_id' => $value,
                        'value' => nl2br($values[$key]),
                    ]);
            }

            //  $arr=array_filter($fields, function($k) {
            //       return strpos($k, 'last_date_on_the_job')>0;
            // }, ARRAY_FILTER_USE_KEY);
            //  $last_date_on_the_job_val=array_pop($arr);
            //  //dd($last_date_on_the_job_val);
            // $arr_parent_work_order=array_filter($fields, function($k) {
            //    return strpos($k, 'parent_work_order')>0;
            // }, ARRAY_FILTER_USE_KEY); 

            // $parent_work_order_id=array_pop($arr_parent_work_order);

            //  $parent_notice_id = WorkOrder::select('notice_id')->where('id', $parent_work_order_id)->first();

            //  if(isset($parent_notice_id)){
            //      $notice_field_id = \App\Models\NoticeField::select('id')->where('notice_id', $parent_notice_id->notice_id)
            //      ->where('name','last_date_on_the_job')->first();

            //      $wrk_order_fields=\App\Models\WorkOrderFields::where('workorder_id',$parent_work_order)
            //      ->where('notice_id',$parent_notice_id->notice_id)
            //      ->where('notice_field_id',$notice_field_id->id)
            //      ->update(['value'=>$last_date_on_the_job_val]);
            //  }
            $recipients = \App\Models\Recipient::select('id')->where('work_order_id', $id)->get()->toArray();
            foreach ($recipients as $key => $value) {

                $recipient = \App\Models\Recipient::find($value['id']);
                \App\Models\Recipient::create([
                    'work_order_id' => $result1->id,
                    'category_id' => $recipient->category_id,
                    'name' => $recipient->name,
                    'contact' => $recipient->contact,
                    'mobile' => $recipient->mobile,
                    'address' => $recipient->address,
                    'city_id' => $recipient->city_id,
                    'state_id' => $recipient->state_id,
                    'zip' => $recipient->zip,
                    'fax' => $recipient->fax,
                    'attn' => $recipient->attn
                ]);
            }
            $attachment = explode(',|,', trim($request->edit_soft_notices_attachment[0], '|,'));

            foreach ($attachment as $k => $v) {
                $v = trim($v, '|,');
                if (isset($request->remove_doc) && !empty($request->remove_doc) && $request->remove_doc[0] != '') {
                    $remove_doc = explode(',', $request->remove_doc[0]);

                    $each_attachment = explode(',', $v);
                    if ($each_attachment[0] != ''  && isset($each_attachment[5])) {
                        $true = !in_array($each_attachment[5], $remove_doc);
                    } else {
                        $true = '';
                    }
                    if (isset($true) && $true != '') {
                        if ($v != '') {

                            $work_order_attachment_obj = new \App\Models\WorkOrderAttachment();
                            $work_order_attachment_obj->work_order_id = $result1->id;
                            $work_order_attachment_obj->type = $each_attachment[0];
                            $work_order_attachment_obj->title = $each_attachment[1];
                            $work_order_attachment_obj->file_name = $each_attachment[2];
                            $work_order_attachment_obj->original_file_name = $each_attachment[3];
                            if (Auth::user()->hasRole('account-manager')) {
                                $work_order_attachment_obj->visibility = $each_attachment[4];
                            } else {
                                $work_order_attachment_obj->visibility = 0;
                            }
                            $work_order_attachment_obj->save();
                        }
                    }
                    //  else{
                    $previous_attachments = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $id)->get();

                    if (isset($previous_attachments) && count($previous_attachments) > 0) {
                        foreach ($previous_attachments as $k1 => $a1) {

                            /* $each_remove_doc = explode(',', $request->remove_doc[0]);
                            foreach ($each_remove_doc As $k => $each) {
                                $explode_v = explode('_', $each);
                                $count = count($explode_v);
                                if($count>1){
                                $each_remove_doc[$k] = $explode_v[$count - 3] . '_' . $explode_v[$count - 2] . '_' . $explode_v[$count - 1];
                              }
                            }

                            $true = !in_array($a1->file_name, $each_remove_doc);*/
                            $true = "true";

                            if (isset($true) && $true != '') {

                                $work_order_attachment_obj = new \App\Models\WorkOrderAttachment();
                                $work_order_attachment_obj->work_order_id = $result1->id;
                                $work_order_attachment_obj->type = $a1->type;
                                $work_order_attachment_obj->title = $a1->title;
                                $work_order_attachment_obj->file_name = date('H') . '_' . $a1->file_name;
                                $work_order_attachment_obj->original_file_name = $a1->original_file_name;
                                if (Auth::user()->hasRole('account-manager')) {
                                    $work_order_attachment_obj->visibility = $a1->visibility;
                                } else {
                                    $work_order_attachment_obj->visibility = 0;
                                }
                                $work_order_attachment_obj->save();
                            }
                        }
                    }
                    //  }
                } else {

                    if ($v != '') {
                        $each_attachment = explode(',', $v);
                        $work_order_attachment_obj = new \App\Models\WorkOrderAttachment();
                        $work_order_attachment_obj->work_order_id = $result1->id;
                        $work_order_attachment_obj->type = $each_attachment[0];
                        $work_order_attachment_obj->title = $each_attachment[1];
                        $work_order_attachment_obj->file_name = $each_attachment[2];
                        $work_order_attachment_obj->original_file_name = $each_attachment[3];
                        if (Auth::user()->hasRole('account-manager')) {
                            $work_order_attachment_obj->visibility = $each_attachment[4];
                        } else {
                            $work_order_attachment_obj->visibility = 0;
                        }
                        $work_order_attachment_obj->save();
                    }

                    $previous_attachments = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $id)->get();

                    if (isset($previous_attachments) && count($previous_attachments) > 0) {
                        foreach ($previous_attachments as $k1 => $a1) {
                            $work_order_attachment_obj = new \App\Models\WorkOrderAttachment();
                            $work_order_attachment_obj->work_order_id = $result1->id;
                            $work_order_attachment_obj->type = $a1->type;
                            $work_order_attachment_obj->title = $a1->title;
                            $work_order_attachment_obj->file_name = date('H') . '_' . $a1->file_name;
                            $work_order_attachment_obj->original_file_name = $a1->original_file_name;
                            if (Auth::user()->hasRole('account-manager')) {
                                $work_order_attachment_obj->visibility = $a1->visibility;
                            } else {
                                $work_order_attachment_obj->visibility = 0;
                            }
                            $work_order_attachment_obj->save();
                        }
                    }
                }
            }
            $attachment = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $result1->id)
                ->get();
        } else {
            if ($request->continue == 'Continue') {
                $result1 = \App\Models\WorkOrder::find($id);
                $result1->notice_id = $notice_id;
                $result1->customer_id = $customer_id;
                $result1->order_no = str_random(5);
                $result1->user_id = $userId;
                //  $result1->status = '1';
                $result1->save();
                $data['status'] = 'request';
                /*Session::flash('success', 'Your work order # ' . $result1->id . ' has been successfully submitted.<a href="' . url('customer/work-order/view/' . $result1->id) . '">Click here to print.</a>');*/
            } else {
                $result1 = \App\Models\WorkOrder::find($id);
                $result1->notice_id = $notice_id;
                $result1->customer_id = $customer_id;
                $result1->order_no = str_random(5);
                $result1->user_id = $userId;
                //$result1->status = '0';
                $result1->save();
                $data['status'] = 'draft';
                Session::flash('success', 'Your work order # ' . $result1->id . ' has been saved as a processing you must complete Work Order and submit at a later time.');
                /*<a href="' . url('customer/work-order/view/' . $result1->id) . '">Click here to print.</a>'*/
            }
            $work_order_id = $result1->id;
            $result2 = \App\Models\WorkOrderFields::select()->where('workorder_id', $id)->delete();
            foreach ($fieldsData as $key => $value) {

                if ($values[$key] == NULL && $value > 0) {
                    $result2 = \App\Models\WorkOrderFields::create([
                        'workorder_id' => $work_order_id,
                        'notice_id' => $request->notice_id,
                        'notice_field_id' => $value,
                        'value' => '',
                    ]);
                } else if ($value > 0) {
                    $result2 = \App\Models\WorkOrderFields::create([
                        'workorder_id' => $work_order_id,
                        'notice_id' => $request->notice_id,
                        'notice_field_id' => $value,
                        'value' => nl2br($values[$key]),
                    ]);
                }
            }
        }
        //dd('test');
        $arr = array_filter($fields, function ($k) {
            return strpos($k, 'last_date_on_the_job') > 0;
        }, ARRAY_FILTER_USE_KEY);
        $last_date_on_the_job_val = array_pop($arr);
        //dd($last_date_on_the_job_val);
        $arr_parent_work_order = array_filter($fields, function ($k) {
            return strpos($k, 'parent_work_order') > 0;
        }, ARRAY_FILTER_USE_KEY);

        $parent_work_order_id = array_pop($arr_parent_work_order);

        $parent_notice_id = WorkOrder::select('notice_id')->where('id', $parent_work_order_id)->first();

        if (!empty($parent_notice_id) && !empty($last_date_on_the_job_val)) {
            $notice_field_id = \App\Models\NoticeField::select('id')->where('notice_id', $parent_notice_id->notice_id)
                ->where('name', 'last_date_on_the_job')->first();
            if (!empty($notice_field_id)) {
                if ($last_date_on_the_job_val == NULL) {
                    $wrk_order_fields = \App\Models\WorkOrderFields::where('workorder_id', $parent_work_order_id)
                        ->where('notice_id', $parent_notice_id->notice_id)
                        ->where('notice_field_id', $notice_field_id->id)
                        ->update(['value' => '']);
                } else {
                    $wrk_order_fields = \App\Models\WorkOrderFields::where('workorder_id', $parent_work_order_id)
                        ->where('notice_id', $parent_notice_id->notice_id)
                        ->where('notice_field_id', $notice_field_id->id)
                        ->update(['value' => $last_date_on_the_job_val]);
                }
            }
        }

        if ($request->duplicate != 'duplicate') {
            if ($result1) {
                $attachment = explode(',|,', trim($request->edit_soft_notices_attachment[0], '|,'));

                foreach ($attachment as $k => $v) {
                    $v = trim($v, '|,');
                    if ($v != '|') {
                        if (isset($request->remove_doc) && !empty($request->remove_doc) && $request->remove_doc[0] != '') {
                            $remove_doc = explode(',', $request->remove_doc[0]);

                            $each_attachment = explode(',', $v);

                            if ($each_attachment[0] != '' && isset($each_attachment[5])) {
                                $true = !in_array($each_attachment[5], $remove_doc);
                            } else {
                                $true = '';
                            }
                            if (isset($true) && $true != '') {
                                if ($v != '') {

                                    $work_order_attachment_obj = new \App\Models\WorkOrderAttachment();
                                    $work_order_attachment_obj->work_order_id = $result1->id;
                                    $work_order_attachment_obj->type = $each_attachment[0];
                                    $work_order_attachment_obj->title = $each_attachment[1];
                                    $work_order_attachment_obj->file_name = $each_attachment[2];
                                    $work_order_attachment_obj->original_file_name = $each_attachment[3];
                                    if (Auth::user()->hasRole('account-manager')) {
                                        $work_order_attachment_obj->visibility = $each_attachment[4];
                                    } else {
                                        $work_order_attachment_obj->visibility = 0;
                                    }
                                    $work_order_attachment_obj->save();
                                }
                            }
                        } else {
                            if ($v != '') {
                                $each_attachment = explode(',', $v);
                                $work_order_attachment_obj = new \App\Models\WorkOrderAttachment();
                                $work_order_attachment_obj->work_order_id = $result1->id;
                                $work_order_attachment_obj->type = $each_attachment[0];
                                $work_order_attachment_obj->title = $each_attachment[1];
                                $work_order_attachment_obj->file_name = $each_attachment[2];
                                $work_order_attachment_obj->original_file_name = $each_attachment[3];
                                if (Auth::user()->hasRole('account-manager')) {
                                    $work_order_attachment_obj->visibility = $each_attachment[4];
                                } else {
                                    $work_order_attachment_obj->visibility = 0;
                                }
                                $work_order_attachment_obj->save();
                            }
                        }
                    }
                }
                foreach ($fields as $key => $value) {
                    $field = explode("_", $key);

                    if (isset($field[3])) {
                        $check_field = $field[1] . '_' . $field[2] . '_' . $field[3];

                        if ($check_field == 'parent_work_order') {
                            $parent_work_order = $value;
                            $data['parent_work_order'] = $parent_work_order;
                            $parent_work_order_previous = $request->parent_work_order_previous;
                            if ($parent_work_order_previous != $parent_work_order) {
                                //If parent work order is selected document of parent work order will inserted

                                if ($parent_work_order != '') {
                                    if (isset($request->remove_doc) && !empty($request->remove_doc) && $request->remove_doc[0] != '') {
                                        $previous_attachments = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $parent_work_order)->get();

                                        if (isset($previous_attachments) && count($previous_attachments) > 0) {
                                            foreach ($previous_attachments as $k1 => $a1) {

                                                /* $each_remove_doc = explode(',', $request->remove_doc[0]);
                                                foreach ($each_remove_doc As $k => $each) {
                                                    $explode_v = explode('_', $each);
                                                    $count = count($explode_v);
                                                    if($count>1){
                                                    $each_remove_doc[$k] = $explode_v[$count - 3] . '_' . $explode_v[$count - 2] . '_' . $explode_v[$count - 1];
                                                    }
                                                }*/

                                                // $true = !in_array($a1->file_name, $each_remove_doc);
                                                $true = "true";

                                                if (isset($true) && $true != '') {

                                                    $work_order_attachment_obj = new \App\Models\WorkOrderAttachment();
                                                    $work_order_attachment_obj->work_order_id = $result1->id;
                                                    $work_order_attachment_obj->type = $a1->type;
                                                    $work_order_attachment_obj->title = $a1->title;
                                                    if (!empty($a1->file_name)) {
                                                        $work_order_attachment_obj->file_name = $result1->id . '_' . $a1->file_name;
                                                    } else {
                                                        $work_order_attachment_obj->file_name = "";
                                                    }
                                                    if (!empty($a1->original_file_name)) {
                                                        $work_order_attachment_obj->original_file_name = $a1->original_file_name;
                                                    } else {
                                                        $work_order_attachment_obj->original_file_name = "";
                                                    }
                                                    if (Auth::user()->hasRole('account-manager')) {
                                                        $work_order_attachment_obj->visibility = $a1->visibility;
                                                    } else {
                                                        $work_order_attachment_obj->visibility = 0;
                                                    }
                                                    $work_order_attachment_obj->save();
                                                    if ($a1->file_name != "" && file_exists(public_path() . '/attachment/work_order_document/parent_document/' . $a1->file_name) && !empty($a1->file_name)) {
                                                        rename(public_path() . '/attachment/work_order_document/parent_document/' . $a1->file_name, public_path() . '/attachment/work_order_document/parent_document/' . $work_order_attachment_obj->file_name);
                                                        \File::copy(public_path() . '/attachment/work_order_document/parent_document/' . $work_order_attachment_obj->file_name, public_path() . '/attachment/work_order_document/' . $work_order_attachment_obj->file_name);
                                                        unlink(public_path() . '/attachment/work_order_document/parent_document/' . $work_order_attachment_obj->file_name);
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        $remove_doc = explode(',', $request->remove_doc[0]);
                                        $previous_attachments = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $parent_work_order)->get();

                                        if (isset($previous_attachments) && count($previous_attachments) > 0) {
                                            foreach ($previous_attachments as $k1 => $a1) {
                                                $work_order_attachment_obj = new \App\Models\WorkOrderAttachment();
                                                $work_order_attachment_obj->work_order_id = $result1->id;
                                                $work_order_attachment_obj->type = $a1->type;
                                                $work_order_attachment_obj->title = $a1->title;
                                                if (!empty($a1->file_name)) {
                                                    $work_order_attachment_obj->file_name = $result1->id . '_' . $a1->file_name;
                                                } else {
                                                    $work_order_attachment_obj->file_name = "";
                                                }
                                                if (!empty($a1->original_file_name)) {
                                                    $work_order_attachment_obj->original_file_name = $a1->original_file_name;
                                                } else {
                                                    $work_order_attachment_obj->original_file_name = "";
                                                }
                                                if (Auth::user()->hasRole('account-manager')) {
                                                    $work_order_attachment_obj->visibility = $a1->visibility;
                                                } else {
                                                    $work_order_attachment_obj->visibility = 0;
                                                }
                                                $work_order_attachment_obj->save();

                                                if ($a1->file_name != "" && file_exists(public_path() . '/attachment/work_order_document/parent_document/' . $a1->file_name) && !empty($a1->file_name)) {
                                                    rename(public_path() . '/attachment/work_order_document/parent_document/' . $a1->file_name, public_path() . '/attachment/work_order_document/parent_document/' . $work_order_attachment_obj->file_name);
                                                    \File::copy(public_path() . '/attachment/work_order_document/parent_document/' . $work_order_attachment_obj->file_name, public_path() . '/attachment/work_order_document/' . $work_order_attachment_obj->file_name);
                                                    unlink(public_path() . '/attachment/work_order_document/parent_document/' . $work_order_attachment_obj->file_name);
                                                }
                                            }
                                        }
                                    }
                                    $data['parent_work_order'] = $parent_work_order;

                                    /* save parent work order recipient  */
                                    $first_category = \App\Models\Category::where('name', 'Contracted By')->first();
                                    /* save parent work order recipient  */
                                    if (isset($request->contracted_by_exist)) {
                                        $parent_work_order_recipients = \App\Models\Recipient::select()->where('work_order_id', $parent_work_order)->where('category_id', '!=', $first_category->id)->get();
                                    } else {
                                        $parent_work_order_recipients = \App\Models\Recipient::select()->where('work_order_id', $parent_work_order)->get();
                                    }
                                    \App\Models\Recipient::where('work_order_id', $result1->id)->where('parent_work_order', '!=', NULL)->delete();
                                    if (isset($parent_work_order_recipients) && count($parent_work_order_recipients) > 0) {
                                        foreach ($parent_work_order_recipients as $k => $each_parent_recipient) {
                                            $save_recipients = \App\Models\Recipient::create([
                                                'work_order_id' => $result1->id,
                                                'category_id' => $each_parent_recipient->category_id,
                                                'name' => $each_parent_recipient->name,
                                                'contact' => $each_parent_recipient->contact,
                                                'address' => $each_parent_recipient->address,
                                                'city_id' => $each_parent_recipient->city_id,
                                                'state_id' => $each_parent_recipient->state_id,
                                                'zip' => $each_parent_recipient->zip,
                                                'email' => $each_parent_recipient->email,
                                                'attn' => $each_parent_recipient->attn,
                                                'parent_work_order' => $parent_work_order,
                                            ]);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    //Insert contracted by is first recipient
                    if (isset($field[2])) {
                        $check_contracted_by_field = $field[1] . '_' . $field[2];

                        if ($check_contracted_by_field == 'contracted_by') {
                            $first_category = \App\Models\Category::where('name', 'Contracted By')->first();
                            $contracted_by = $value;

                            $customerId = WorkOrder::find($work_order_id);
                            if ($contracted_by != "") {
                                /* check contracted by is exist or not */
                                $contracted_by_check = \App\Models\Contact::where('company_name', $contracted_by)->first();
                                if (empty($contracted_by_check)) {
                                    $contact = \App\Models\Contact::create([
                                        'customer_id' => $customerId->customer_id,
                                        'company_name' => $contracted_by,
                                    ]);
                                }
                            }

                            $contactdetails = \App\Models\Contact::where('company_name', '=', $contracted_by)
                                ->where('customer_id', '=', $customerId->customer_id)
                                ->first();

                            if (isset($contactdetails) && $contactdetails != null) {
                                /* check contract details exit or not */
                                $check_contract_recipient_exist = \App\Models\Recipient::where(['category_id' => $first_category->id, 'work_order_id' => $result1->id])->first();
                                /* if not empty delete previouse record */

                                if (!empty($check_contract_recipient_exist)) {
                                    if ($check_contract_recipient_exist->name != $contracted_by) {
                                        $contract_recipient_delete = \App\Models\Recipient::where(['category_id' => $first_category->id, 'work_order_id' => $result1->id])->delete();
                                        $check_contract_recipient_exist = [];
                                    }
                                }
                                if (empty($check_contract_recipient_exist)) {
                                    $Recipient = \App\Models\Recipient::create([
                                        'work_order_id' => $result1->id,
                                        'category_id' => $first_category->id,
                                        'name' => $contactdetails->company_name,
                                        'contact' => $contactdetails->phone,
                                        'address' => $contactdetails->mailing_address,
                                        'city_id' => $contactdetails->city_id,
                                        'state_id' => $contactdetails->state_id,
                                        'zip' => $contactdetails->zip,
                                        'email' => $contactdetails->email,
                                        'attn' => $contactdetails->attn
                                    ]);
                                }
                            }
                        }
                    }
                }
            }
        }


        $work_order_id = $result1->id;
        $result2 = \App\Models\WorkOrderFields::select()->where('workorder_id', $work_order_id)->delete();
        foreach ($fieldsData as $key => $value) {

            if ($values[$key] == NULL && $value > 0) {
                $result2 = \App\Models\WorkOrderFields::create([
                    'workorder_id' => $work_order_id,
                    'notice_id' => $request->notice_id,
                    'notice_field_id' => $value,
                    'value' => '',
                ]);
            } else if ($value > 0) {
                $result2 = \App\Models\WorkOrderFields::create([
                    'workorder_id' => $work_order_id,
                    'notice_id' => $request->notice_id,
                    'notice_field_id' => $value,
                    'value' => nl2br($values[$key]),
                ]);
            }
        }


        $notice_id = $result1->notice_id;
        $id = $result1->id;
        $notice = \App\Models\Notice::select('name', 'id', 'type', 'is_claim_of_lien', 'master_notice_id')->where('id', $notice_id)->get()->toArray();
        $data['notice'] = $notice[0];
        $data['cities'] = \App\Models\City::select()->get();
        $data['countries'] = \App\Models\Country::select()->get();
        $data['states'] = \App\Models\State::select()->orderBy('name', 'asc')->get();
        $data['duplicate'] = $request->duplicate;
        $data['recipients'] = \App\Models\Recipient::select('recipients.id', 'work_order_id', 'category_id', 'name', 'contact', 'mobile', 'recipients.address', 'city_id', 'state_id', 'zip', 'fax', 'recipients.created_at', 'recipients.updated_at', \DB::raw('usps_address.address as usps_address'), \DB::raw('usps_address.city as usps_city'), \DB::raw('usps_address.state as usps_state'), 'zipcode', 'zipcode_add_on', 'dpb', 'check_digit', 'comment')->leftJoin('usps_address', 'usps_address.recipient_id', '=', 'recipients.id')->where('work_order_id', $work_order_id)->get();

        // dd($data['recipients']);
        $data['rush_hour'] = \App\Models\WorkOrder::select('rush_hour_charges', 'rush_hour_amount')->where('id', $work_order_id)->get();
        $verified_address_count = \App\Models\Recipient::select(\DB::raw('COUNT(usps_address.recipient_id) as verified_address_count'))->leftJoin('usps_address', 'usps_address.recipient_id', '=', 'recipients.id')->where('work_order_id', $work_order_id)->get();
        $data['verified_address_count'] = ($verified_address_count) ? $verified_address_count[0]['verified_address_count'] : 0;

        $data['customer_id'] = $customer_id;
        $data['officers_directors'] = \App\Models\CustomerAgent::where('customer_id', '=', $data['customer_id'])
            ->get();
        $data['names'] = \App\Models\Contact::select('contacts.id', 'contacts.company_name')
            //    ->where('contact_details.type', '=', 0)
            ->where('contacts.customer_id', '=', $data['customer_id'])
            //   ->Join('contact_details', 'contact_details.contact_id', '=', 'contacts.id')
            ->orderBy('contacts.company_name', 'ASC')
            ->get();

        $data['customer_id'] = $customer_id;
        $data['categories'] = \App\Models\Category::select()->orderBy('name', 'ASC')->get();
        $data['project_roles'] = \App\Models\ProjectRoles::select()->orderBy('name', 'ASC')->get();
        $data['count'] = \App\Models\Recipient::select(DB::raw('COUNT(*) AS total'), 'categories.name')->groupBy('recipients.category_id')->join('categories', 'categories.id', '=', 'recipients.category_id')->where('work_order_id', $id)->get();
        $data['id'] = $id;
        $data['notice_id'] = $notice_id;
        //        $result = \App\Models\WorkOrder::select('state_id')->join('notice_fields', 'notice_fields.id', '=', 'work_orders.notice_id')->where('work_orders.id', $id)->get()->toArray();
        $result = \App\Models\WorkOrder::select('project_address_count', 'state_id', 'customer_id', 'is_rescind')->join('notice_fields', 'notice_fields.notice_id', '=', 'work_orders.notice_id')->where('work_orders.id', $id)->limit(1)->get()->toArray();
        $data['is_rescind'] = $result[0]['is_rescind'];
        $data['project_address_count'] = $result[0]['project_address_count'];
        $data['projectTypes'] = \App\Models\ProjectType::select()->get();
        $data['attachment'] = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $id)
            ->get();
        $data['service_types'] = \App\Models\StampsServices::where(['status' => '1'])->get(['description', 'abbr'])->toArray();
        /* Notes and correction * */

        $admin_email = \App\Models\SecondaryDocument::getAdminEmail();
        if ($data['recipients']) {
            for ($cnt = 0; $cnt < count($data['recipients']); $cnt++) {
                $labels = \App\Models\StampsLabel::leftJoin('stamps_service_type', 'stamps_service_type.id', 'stamps_label.service_type')
                    ->leftJoin('stamps_packages', 'stamps_packages.id', 'stamps_label.package_type')
                    ->where(['recipient_id' => $data['recipients'][$cnt]['id']])
                    ->first(['stamps_label.*']);

                if (!empty($labels) && $labels['generated_label'] == 'yes') {
                    $data['label_generated_count']++;
                    $data['recipients'][$cnt]['label_generated'] = 'yes';
                } else {
                    $data['recipients'][$cnt]['label_generated'] = 'no';
                }
                $data['recipients'][$cnt]['recipient_shipping_labels'] = $labels;
                $data['recipients'][$cnt]['add_ons'] = \App\Models\StampsAddOns::join('stamps_service_package_addon', 'stamps_service_package_addon.add_on', '=', 'stamps_add_ons.id')
                    ->join('stamps_service_type', 'stamps_service_type.id', '=', 'stamps_service_package_addon.service_type')
                    ->join('stamps_packages', 'stamps_packages.id', '=', 'stamps_service_package_addon.package')
                    ->where(['stamps_add_ons.status' => '1'])
                    ->where('stamps_service_type.id', '=', 1)
                    ->where('stamps_packages.id', '=', 2)
                    // ->where(function($query) use ($labels) {  
                    //     if(isset($labels) && !empty($labels))
                    //     {
                    //         $query->where('stamps_service_type.id','=',$labels['service_type']);
                    //         $query->where('stamps_packages.id','=',$labels['package_type']);
                    //     }    
                    // })
                    ->get([\DB::raw('DISTINCT(stamps_add_ons.id)'), 'stamps_add_ons.description'])->toArray();

                if (!empty($data['recipients'][$cnt]['add_ons'])) {
                    $cnt1 = 1;
                    $temp_add_ons = $data['recipients'][$cnt]['add_ons'];
                    for ($counter = 0; $counter < count($temp_add_ons); $counter++) {
                        $prohibited = \App\Models\StampsAddOnsProhibited::join('stamps_add_ons', 'stamps_add_ons_prohibited.prohibited', '=', 'stamps_add_ons.id')
                            ->where(['stamps_add_ons_prohibited.add_on' => $temp_add_ons[$counter]['id']])
                            ->pluck('prohibited')->toArray();

                        $required = \App\Models\StampsAddOnsRequired::join('stamps_add_ons', 'stamps_add_ons_required.required', '=', 'stamps_add_ons.id')
                            ->where(['stamps_add_ons_required.add_on' => $data['recipients'][$cnt]['add_ons'][$counter]['id']])
                            ->pluck('required')->toArray();
                        $temp_add_ons[$counter]['prohibited'] = json_encode($prohibited);
                        $temp_add_ons[$counter]['required'] = json_encode($required);
                    }
                    unset($data['recipients'][$cnt]['add_ons']);
                    $data['recipients'][$cnt]['add_ons'] = $temp_add_ons;
                }
            }
        }
        /* if (Auth::user()->hasRole('account-manager')) {
          $account_manager_email = \App\Models\SecondaryDocument::getOtherAccManagerEmailExcludingLoggedIn(Auth::user()->id);
          $customer_email = \App\Models\SecondaryDocument::getCustomerEmail($result1->customer_id);
          if (isset($account_manager_email) && !empty($account_manager_email)) {
          $data['note_emails'] = [
          $customer_email->email,
          $admin_email->email,
          ];
          foreach ($account_manager_email AS $each_emails) {
          array_push($data['note_emails'], $each_emails->email);
          }
          } else {
          $data['note_emails'] = [
          $customer_email->email,
          $admin_email->email,
          ];
          }
          } else {
          $account_manager_email = \App\Models\SecondaryDocument::getAllAccountManagerEmail();
          $customer_email = \App\Models\SecondaryDocument::getCustomerEmail($result1->customer_id);
          if (isset($account_manager_email) && !empty($account_manager_email)) {
          $data['note_emails'] = [
          $customer_email->email
          ];
          foreach ($account_manager_email AS $each_emails) {
          array_push($data['note_emails'], $each_emails->email);
          }
          } else {
          $data['note_emails'] = [
          $customer_email->email
          ];
          }
          } */
        // dd($data['recipients']);
        $data['note_emails'] = get_user_notice_email($result1->customer_id);
        $data['notes'] = \App\Models\WorkOrderNotes::leftjoin('users', 'users.id', '=', 'work_order_notes.user_id')
            ->select('work_order_notes.*', 'users.name')
            ->where('customer_id', '=', $result1->customer_id)
            ->where('work_order_notes.work_order_id', '=', $result1->id)
            ->orderBy('work_order_notes.id', 'DESC')
            ->get();

        $data['corrections'] = \App\Models\WorkOrderCorrections::leftjoin('users', 'users.id', '=', 'work_order_corrections.user_id')
            ->select('work_order_corrections.*', 'users.name')
            ->where('customer_id', '=', $result1->customer_id)
            ->where('work_order_corrections.work_order_id', '=', $result1->id)
            ->orderBy('work_order_corrections.id', 'DESC')
            ->get();
        /* Notes and correction * */
        //        if ($request->duplicate == 'duplicate') {
        if ($request->continue == 'Continue') {
            return redirect()->to('account-manager/research/edit/' . $id . '/' . $notice_id . '/' . $customer_id . '/' . $is_rescind);
        } else {
            return redirect('account-manager/research');
        }
        /*if ($request->continue == 'Continue') {
            if ($notice[0]['type'] == 2) {
                $data['notice_fields_section1'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.workorder_id', $id)->where('work_order_fields.notice_id', $notice_id)->where('notice_fields.section', 1)->orderBy('sort_order', 'asc')->get();

                $data['notice_fields_section2'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.notice_id', $notice_id)->where('work_order_fields.workorder_id', $id)->where('notice_fields.section', 2)->orderBy('sort_order', 'asc')->get();
                $data['tab'] = 'recipients';
                return view('account_manager.research.edit_soft_notices', $data);
            } else {
                $data['notice_fields'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.notice_id', $notice_id)->where('work_order_fields.workorder_id', $id)->orderBy('sort_order', 'asc')->get();
                $data['tab'] = 'recipients';
                return view('account_manager.research.edit', $data);
            }
        } else {
            return redirect('account-manager/research');
        }*/
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function researchEditNextStep(Request $request, $id, $notice_id, $customer_id, $is_rescind)
    {
        $data['is_rescind'] = $is_rescind;
        $data['service_types'] = \App\Models\StampsServices::where(['status' => '1'])->get(['description', 'abbr', 'id'])->toArray();
        $data['package_types'] = \App\Models\StampsAddOns::join('stamps_service_package_addon', 'stamps_service_package_addon.add_on', '=', 'stamps_add_ons.id')
            ->join('stamps_service_type', 'stamps_service_type.id', '=', 'stamps_service_package_addon.service_type')
            ->join('stamps_packages', 'stamps_packages.id', '=', 'stamps_service_package_addon.package')
            ->where(['stamps_packages.status' => '1'])
            ->get([\DB::raw('DISTINCT(stamps_packages.name)'), 'stamps_packages.id'])->toArray();

        $data['parent_wo_list'] = \App\Models\WorkOrder::select('id', DB::raw('CONCAT("#",id) AS `parent_id`'))
            ->where('id', '!=', $id)
            ->pluck('parent_id', 'id')
            ->toArray();
        $data['add_ons'] = \App\Models\StampsAddOns::join('stamps_service_package_addon', 'stamps_service_package_addon.add_on', '=', 'stamps_add_ons.id')
            ->join('stamps_service_type', 'stamps_service_type.id', '=', 'stamps_service_package_addon.service_type')
            ->join('stamps_packages', 'stamps_packages.id', '=', 'stamps_service_package_addon.package')
            ->where(['stamps_add_ons.status' => '1'])
            ->where(['stamps_service_type.id' => 1])
            ->where(['stamps_packages.id' => 2])
            ->get([\DB::raw('DISTINCT(stamps_add_ons.id)'), 'stamps_add_ons.description'])->toArray();

        if (!empty($data['add_ons'])) {
            for ($counter = 0; $counter < count($data['add_ons']); $counter++) {
                $prohibited = \App\Models\StampsAddOnsProhibited::join('stamps_add_ons', 'stamps_add_ons_prohibited.prohibited', '=', 'stamps_add_ons.id')
                    ->where(['stamps_add_ons_prohibited.add_on' => $data['add_ons'][$counter]['id']])
                    ->pluck('prohibited')->toArray();

                $required = \App\Models\StampsAddOnsRequired::join('stamps_add_ons', 'stamps_add_ons_required.required', '=', 'stamps_add_ons.id')
                    ->where(['stamps_add_ons_required.add_on' => $data['add_ons'][$counter]['id']])
                    ->pluck('required')->toArray();
                $data['add_ons'][$counter]['prohibited'] = json_encode($prohibited);
                $data['add_ons'][$counter]['required'] = json_encode($required);
            }
        }
        $data['label_generated_count'] = 0;
        $data['customer_details'] = \App\Models\Customer::find($customer_id);

        //Update count of project addresses
        \App\Models\WorkOrder::where(['id' => $id])->update(['project_address_count' => $request->project_address_count]);


        $workorderiddetails = \App\Models\WorkOrder::find($id);
        $userId = $workorderiddetails->user_id;
        $data['work_order_status'] = $workorderiddetails->status;

        $work_order_id = $id;

        $notice_id = $workorderiddetails->notice_id;

        $notice = \App\Models\Notice::select('name', 'id', 'type', 'is_claim_of_lien', 'master_notice_id')->where('id', $notice_id)->get()->toArray();
        $data['notice'] = $notice[0];
        $data['cities'] = \App\Models\City::select()->get();
        $data['countries'] = \App\Models\Country::select()->get();
        $data['states'] = \App\Models\State::select()->orderBy('name', 'asc')->get();
        $data['duplicate'] = 0;
        $data['recipients'] = \App\Models\Recipient::select('recipients.id', 'work_order_id', 'category_id', 'name', 'contact', 'mobile', 'recipients.address', 'city_id', 'state_id', 'zip', 'fax', 'recipients.created_at', 'recipients.updated_at', \DB::raw('usps_address.address as usps_address'), \DB::raw('usps_address.city as usps_city'), \DB::raw('usps_address.state as usps_state'), 'zipcode', 'zipcode_add_on', 'dpb', 'check_digit', 'comment')->leftJoin('usps_address', 'usps_address.recipient_id', '=', 'recipients.id')->where('work_order_id', $work_order_id)->get();
        $data['rush_hour'] = \App\Models\WorkOrder::select('rush_hour_charges', 'rush_hour_amount')->where('id', $work_order_id)->get();
        $verified_address_count = \App\Models\Recipient::select(\DB::raw('COUNT(usps_address.recipient_id) as verified_address_count'))->leftJoin('usps_address', 'usps_address.recipient_id', '=', 'recipients.id')->where('work_order_id', $work_order_id)->get();
        $data['verified_address_count'] = ($verified_address_count) ? $verified_address_count[0]['verified_address_count'] : 0;

        $data['customer_id'] = $customer_id;
        $data['officers_directors'] = \App\Models\CustomerAgent::where('customer_id', '=', $data['customer_id'])
            ->get();
        $data['names'] = \App\Models\Contact::select('contacts.id', 'contacts.company_name')
            //    ->where('contact_details.type', '=', 0)
            ->where('contacts.customer_id', '=', $data['customer_id'])
            //   ->Join('contact_details', 'contact_details.contact_id', '=', 'contacts.id')
            ->orderBy('contacts.company_name', 'ASC')
            ->get();

        $data['customer_id'] = $customer_id;
        $data['categories'] = \App\Models\Category::select()->orderBy('name', 'ASC')->get();
        $data['project_roles'] = \App\Models\ProjectRoles::select()->orderBy('name', 'ASC')->get();
        $data['count'] = \App\Models\Recipient::select(DB::raw('COUNT(*) AS total'), 'categories.name')->groupBy('recipients.category_id')->join('categories', 'categories.id', '=', 'recipients.category_id')->where('work_order_id', $id)->get();
        $data['id'] = $id;
        $data['notice_id'] = $notice_id;
        //        $result = \App\Models\WorkOrder::select('state_id')->join('notice_fields', 'notice_fields.id', '=', 'work_orders.notice_id')->where('work_orders.id', $id)->get()->toArray();
        $result = \App\Models\WorkOrder::select('project_address_count', 'state_id', 'customer_id', 'is_rescind')->join('notice_fields', 'notice_fields.notice_id', '=', 'work_orders.notice_id')->where('work_orders.id', $id)->limit(1)->get()->toArray();
        $data['is_rescind'] = $result[0]['is_rescind'];
        $data['project_address_count'] = $result[0]['project_address_count'];
        $data['projectTypes'] = \App\Models\ProjectType::select()->get();
        $data['attachment'] = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $id)
            ->get();
        $data['service_types'] = \App\Models\StampsServices::where(['status' => '1'])->get(['description', 'abbr'])->toArray();
        /* Notes and correction * */

        $admin_email = \App\Models\SecondaryDocument::getAdminEmail();

        $data['note_emails'] = get_user_notice_email($workorderiddetails->customer_id);
        $data['notes'] = \App\Models\WorkOrderNotes::leftjoin('users', 'users.id', '=', 'work_order_notes.user_id')
            ->where('customer_id', '=', $workorderiddetails->customer_id)
            ->where('work_order_notes.work_order_id', '=', $workorderiddetails->id)
            ->orderBy('work_order_notes.id', 'DESC')
            ->get();

        $data['corrections'] = \App\Models\WorkOrderCorrections::leftjoin('users', 'users.id', '=', 'work_order_corrections.user_id')->where('customer_id', '=', $workorderiddetails->customer_id)
            ->where('work_order_corrections.work_order_id', '=', $workorderiddetails->id)
            ->orderBy('work_order_corrections.id', 'DESC')
            ->get();
        /* Notes and correction * */
        //        if ($request->duplicate == 'duplicate') {


        if ($data['recipients']) {
            for ($cnt = 0; $cnt < count($data['recipients']); $cnt++) {
                $labels = \App\Models\StampsLabel::leftJoin('stamps_service_type', 'stamps_service_type.id', 'stamps_label.service_type')
                    ->leftJoin('stamps_packages', 'stamps_packages.id', 'stamps_label.package_type')
                    ->where(['recipient_id' => $data['recipients'][$cnt]['id']])
                    ->first(['stamps_label.*']);

                if (!empty($labels) && $labels['generated_label'] == 'yes') {
                    $data['label_generated_count']++;
                    $data['recipients'][$cnt]['label_generated'] = 'yes';
                } else {
                    $data['recipients'][$cnt]['label_generated'] = 'no';
                }
                $data['recipients'][$cnt]['recipient_shipping_labels'] = $labels;
                $data['recipients'][$cnt]['add_ons'] = \App\Models\StampsAddOns::join('stamps_service_package_addon', 'stamps_service_package_addon.add_on', '=', 'stamps_add_ons.id')
                    ->join('stamps_service_type', 'stamps_service_type.id', '=', 'stamps_service_package_addon.service_type')
                    ->join('stamps_packages', 'stamps_packages.id', '=', 'stamps_service_package_addon.package')
                    ->where(['stamps_add_ons.status' => '1'])
                    ->where('stamps_service_type.id', '=', 1)
                    ->where('stamps_packages.id', '=', 2)
                    // ->where(function($query) use ($labels) {  
                    //     if(isset($labels) && !empty($labels))
                    //     {
                    //         $query->where('stamps_service_type.id','=',$labels['service_type']);
                    //         $query->where('stamps_packages.id','=',$labels['package_type']);
                    //     }    
                    // })
                    ->get([\DB::raw('DISTINCT(stamps_add_ons.id)'), 'stamps_add_ons.description'])->toArray();

                if (!empty($data['recipients'][$cnt]['add_ons'])) {
                    $cnt1 = 1;
                    $temp_add_ons = $data['recipients'][$cnt]['add_ons'];
                    for ($counter = 0; $counter < count($temp_add_ons); $counter++) {
                        $prohibited = \App\Models\StampsAddOnsProhibited::join('stamps_add_ons', 'stamps_add_ons_prohibited.prohibited', '=', 'stamps_add_ons.id')
                            ->where(['stamps_add_ons_prohibited.add_on' => $temp_add_ons[$counter]['id']])
                            ->pluck('prohibited')->toArray();

                        $required = \App\Models\StampsAddOnsRequired::join('stamps_add_ons', 'stamps_add_ons_required.required', '=', 'stamps_add_ons.id')
                            ->where(['stamps_add_ons_required.add_on' => $data['recipients'][$cnt]['add_ons'][$counter]['id']])
                            ->pluck('required')->toArray();
                        $temp_add_ons[$counter]['prohibited'] = json_encode($prohibited);
                        $temp_add_ons[$counter]['required'] = json_encode($required);
                    }
                    unset($data['recipients'][$cnt]['add_ons']);
                    $data['recipients'][$cnt]['add_ons'] = $temp_add_ons;
                }
            }
        }

        if ($notice[0]['type'] == 2) {
            $data['notice_fields_section1'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.workorder_id', $id)->where('work_order_fields.notice_id', $notice_id)->where('notice_fields.section', 1)->orderBy('sort_order', 'asc')->get();

            $data['notice_fields_section2'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.notice_id', $notice_id)->where('work_order_fields.workorder_id', $id)->where('notice_fields.section', 2)->orderBy('sort_order', 'asc')->get();
            $data['tab'] = 'recipients';

            //To display parent work order
            foreach ($data['notice_fields_section1'] as $key => $value) {
                $key = explode("_", $key);

                if ($value) {
                    if ($value->name == 'parent_work_order') {
                        $parent_work_order = $value->value;
                        $data['parent_work_order'] = $parent_work_order;
                    }
                    if ($value->name == 'project_address') {
                        $project_address = $value->value;
                        $project_address = explode('**', $project_address);
                        $data['project_address_count'] = count($project_address);
                    }
                }
            }

            return view('account_manager.research.edit_soft_notices', $data);
        } else {
            $data['notice_fields'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.notice_id', $notice_id)->where('work_order_fields.workorder_id', $id)->orderBy('sort_order', 'asc')->get();
            $data['tab'] = 'recipients';
            return view('account_manager.research.edit', $data);
        }
    }

    public function cancel($id)
    {
        //
        $result = \App\Models\WorkOrder::find($id);
        $updateData = ['status' => 7, 'cancelled_at' => date("Y-m-d H:i:s")];
        //if status is draft/request & updated by AM then will consider as no-cancellation-charge on invoice
        if(!empty($result) && ( $result->status == 0 ||  $result->status == 1) ) {
            $updateData += ['is_billable' => 0];
        }
        $result->update($updateData);

        if ($result) {
             \Artisan::call('cache:clear');
            \Artisan::call("GeneratingQuickbooksInvoice:generatingInvoice");
            Session::flash('success', 'Your work order # ' . $id . ' Record Cancel Successfully.');
            /*<a href="' . url('customer/work-order/view/' . $id) . '">Click here to print.</a>*/
        } else {
            Session::flash('success', 'Your work order # ' . $id . ' Problem in Record saving');
        }
        return redirect()->back();
    }

    public function proceed($id, $notice_id)
    {
        // dd('ffff');
        $result = \App\Models\WorkOrder::find($id);
        $result->update(['status' => 2]);
        $result->save();
        $is_rescind = $result->is_rescind;
        /* if ($result) {
          Session::flash('success', 'Record updated Successfully');
          } else {
          Session::flash('success', 'Problem in Record saving');
          } */

        return redirect('account-manager/research/edit/' . $id . '/' . $notice_id . '/' . $is_rescind);
    }

    public function duplicate($id, $notice_id, $is_rescind)
    {
        $workorder_data = WorkOrder::find($id);

        //$is_rescind = $workorder_data->is_rescind;
        // dd(Auth::user()->hasRole('account-manager'));
        if (Auth::user()->hasRole('account-manager') || Auth::user()->hasRole('admin')) {
            $customerId = $workorder_data->customer_id;
        } else {
            $customerId = Auth::user()->customer->id;
        }
        $data['customer_details'] = \App\Models\Customer::find($customerId);

        $userId = Auth::user()->id;
        $customerUserId = $data['customer_details']->user_id;
        $data['officers_directors'] = \App\Models\CustomerAgent::where('customer_id', '=', $customerId)
            ->get();
        $account_manager_id = Auth::user()->customer->account_manager_id;
        $result1 = WorkOrder::create([
            'notice_id' => $notice_id,
            'customer_id' => $customerId,
            'order_no' => str_random(5),
            'user_id' => $customerUserId,
            'status' => '2',
            'account_manager_id' => $userId,
            'is_rescind' => $is_rescind,
        ]);
        $data['status'] = 'processing';
        Session::flash('success', 'your work order # ' . $id . ' has been duplicated. ');
        /*<a href="' . url('customer/work-order/view/' . $result1->id) . '">Click here to print.</a>*/


        $recipients = \App\Models\Recipient::select('id')->where('work_order_id', $id)->get()->toArray();
        foreach ($recipients as $key => $value) {

            $recipient = \App\Models\Recipient::find($value['id']);
            $new_recipient_data = \App\Models\Recipient::create([
                'work_order_id' => $result1->id,
                'category_id' => $recipient->category_id,
                'name' => $recipient->name,
                'contact' => $recipient->contact,
                'mobile' => $recipient->mobile,
                'email' => $recipient->email,
                'address' => $recipient->address,
                'city_id' => $recipient->city_id,
                'state_id' => $recipient->state_id,
                'zip' => $recipient->zip,
                'country_id' => $recipient->country_id,
                'fax' => $recipient->fax,
                'attn' => $recipient->attn,
                'contact_id' => $recipient->contact_id,
            ]);

            /* save UPS address verified */
            $recipients_uspsAddress = \App\Models\UspsAddress::where('recipient_id', $value['id'])->get();

            foreach ($recipients_uspsAddress as $recipients_uspsAddress_val) {
                $usps_address = [
                    'fullname' => $recipients_uspsAddress_val->fullname,
                    'company' => $recipients_uspsAddress_val->company,
                    'department' => $recipients_uspsAddress_val->department,
                    'address' => $recipients_uspsAddress_val->address,
                    'address2' => $recipients_uspsAddress_val->address2,
                    'address3' => $recipients_uspsAddress_val->address3,
                    'city' => $recipients_uspsAddress_val->city,
                    'state' => $recipients_uspsAddress_val->state,
                    'zipcode' => $recipients_uspsAddress_val->zipcode,
                    'zipcode_add_on' => $recipients_uspsAddress_val->zipcode_add_on,
                    'phone_number' => $recipients_uspsAddress_val->phone_number,
                    'email' => $recipients_uspsAddress_val->email,
                    'country' => $recipients_uspsAddress_val->country,
                    'dpb' => $recipients_uspsAddress_val->dpb,
                    'check_digit' => $recipients_uspsAddress_val->check_digit,
                    'comment' => $recipients_uspsAddress_val->comment,
                    'override_hash' => $recipients_uspsAddress_val->override_hash,
                    'recipient_id' => $new_recipient_data->id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ];

                \App\Models\UspsAddress::insert($usps_address);
            }
        }

        $previous_attachments = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $id)->get();



        if ($previous_attachments && count($previous_attachments) > 0) {
            foreach ($previous_attachments as $k => $val) {
                if ($val->original_file_name != "") {
                    $original_file_name = explode('.', $val->original_file_name);
                    $val->original_file_name = !empty($original_file_name[1]) ? '2_' . $original_file_name[0] . '.' . $original_file_name[1] : "";
                }
                if ($val->file_name != "") {
                    $file_name = explode('.', $val->file_name);
                    if (!empty($file_name[1])) {
                        $new_file_name = date('H') . '_' . $file_name[0] . '.' . $file_name[1];

                        if (!file_exists(public_path() . '/attachment/work_order_document/temp_duplicate')) {
                            $dir = mkdir(public_path() . '/attachment/work_order_document/temp_duplicate', 0777, true);
                            \File::copy(public_path() . '/attachment/work_order_document/' . $val->file_name, public_path() . '/attachment/work_order_document/temp_duplicate/' . $val->file_name);
                        } else {
                            if (file_exists(public_path() . '/attachment/work_order_document/' . $val->file_name)) {
                                \File::copy(public_path() . '/attachment/work_order_document/' . $val->file_name, public_path() . '/attachment/work_order_document/temp_duplicate/' . $val->file_name);
                            }
                        }
                        if (file_exists(public_path() . '/attachment/work_order_document/temp_duplicate/' . $val->file_name)) {
                            rename(public_path() . '/attachment/work_order_document/temp_duplicate/' . $val->file_name, public_path() . '/attachment/work_order_document/temp_duplicate/' . $new_file_name);
                            //\Storage::move(public_path().'/attachment/work_order_document/temp_duplicate/'.$new_file_name, public_path().'/attachment/work_order_document/'.$new_file_name);
                            \File::copy(public_path() . '/attachment/work_order_document/temp_duplicate/' . $new_file_name, public_path() . '/attachment/work_order_document/' . $new_file_name);
                            unlink(public_path() . '/attachment/work_order_document/temp_duplicate/' . $new_file_name);
                        }
                        $val->file_name = $new_file_name;
                        $val->file_name = date('H') . '_' . $val->file_name;
                    } else {
                        $val->file_name = "";
                    }
                } else {
                    $val->file_name = "";
                }
                $work_order_attachment_obj = new \App\Models\WorkOrderAttachment();
                $work_order_attachment_obj->work_order_id = $result1->id;
                $work_order_attachment_obj->type = $val->type;
                $work_order_attachment_obj->title = $val->title;
                $work_order_attachment_obj->file_name = $val->file_name;
                $work_order_attachment_obj->original_file_name = $val->original_file_name;
                if (Auth::user()->hasRole('account-manager')) {
                    $work_order_attachment_obj->visibility = $val->visibility;
                } else {
                    $work_order_attachment_obj->visibility = 0;
                }
                $work_order_attachment_obj->save();
            }
        }


        $notice = \App\Models\Notice::select('name', 'id', 'type', 'is_claim_of_lien', 'state_id')->where('id', $notice_id)->get()->toArray();

        /* parent work order notice field id */
        $parent_workorder_notice_field = \App\Models\NoticeField::where(['notice_id' => $notice_id, 'name' => 'parent_work_order'])->first();

        $fieldsData = \App\Models\WorkOrderFields::select()->where('workorder_id', $id)->get();

        foreach ($fieldsData as $value) {
            $work_val = nl2br($value->value);
            if ($parent_workorder_notice_field->id == $value->notice_field_id && $is_rescind != 1) {
                $work_val = "";
            }

            $result2 = \App\Models\WorkOrderFields::create([
                'workorder_id' => $result1->id,
                'notice_id' => $value->notice_id,
                'notice_field_id' => $value->notice_field_id,
                'value' => nl2br($value->value),
            ]);
        }
        if ($is_rescind == 1) {
            $amendment_fields = \App\Models\NoticeField::select()->where('state_id', $notice[0]['state_id'])->where('notice_id', $notice_id)->where('name', 'amendment')->orderBy('sort_order', 'asc')->get()->toArray();
            $amendmentValue = \App\Models\WorkOrderFields::select('value')->where('notice_field_id', $amendment_fields[0]['id'])->where('workorder_id', $result1->id)->get();
            if ($amendment_fields && $amendmentValue->isEmpty()) {
                $result2 = \App\Models\WorkOrderFields::create([
                    'workorder_id' => $result1->id,
                    'notice_id' => $notice_id,
                    'notice_field_id' => $amendment_fields[0]['id'],
                    'value' => ' ',
                ]);
            }
        }

        return redirect("account-manager/research/edit/" . $result1->id . "/" . $notice_id . '/' . $is_rescind);
    }

    /* public function duplicate($id, $notice_id) {
      $notice = \App\Models\Notice::select('name', 'id', 'type')->where('id', $notice_id)->get()->toArray();
      $data['notice'] = $notice[0];
      $data['cities'] = \App\Models\City::select()->get();
      $data['countries'] = \App\Models\Country::select()->get();
      $data['states'] = \App\Models\State::select()->get();
      $data['projectTypes'] = \App\Models\ProjectType::select()->get();
      $data['recipients'] = \App\Models\Recipient::select('recipients.id', 'work_order_id', 'category_id', 'name', 'contact', 'mobile', 'recipients.address', 'city_id', 'state_id', 'zip', 'fax', 'recipients.created_at', 'recipients.updated_at', \DB::raw('usps_address.address as usps_address'), \DB::raw('usps_address.city as usps_city'), \DB::raw('usps_address.state as usps_state'), 'zipcode', 'zipcode_add_on', 'dpb', 'check_digit', 'comment')->leftJoin('usps_address', 'usps_address.recipient_id', '=', 'recipients.id')->where('work_order_id', $id)->get();

      $data['categories'] = \App\Models\Category::select()->get();
      $data['count'] = \App\Models\Recipient::select(DB::raw('COUNT(*) AS total'), 'categories.name')->groupBy('recipients.category_id')->join('categories', 'categories.id', '=', 'recipients.category_id')->where('work_order_id', $id)->get();
      $data['id'] = $id;
      $data['notice_id'] = $notice_id;
      $result = \App\Models\WorkOrder::select('project_address_count', 'state_id', 'customer_id')->join('notice_fields', 'notice_fields.notice_id', '=', 'work_orders.notice_id')->where('work_orders.id', $id)->limit(1)->get()->toArray();
      $data['project_address_count'] = $result[0]['project_address_count'];
      //                $result = \App\Models\WorkOrder::select('state_id','customer_id')->join('notice_fields','notice_fields.id','=','work_orders.notice_id')->where('work_orders.id',$id)->get()->toArray();
      $result = \App\Models\WorkOrder::select('state_id', 'customer_id')->join('notice_fields', 'notice_fields.notice_id', '=', 'work_orders.notice_id')->where('work_orders.id', $id)->limit(1)->get()->toArray();

      $data['customer_id'] = $result[0]['customer_id'];

      $data['names'] = \App\Models\Contact::select('contacts.id', 'contacts.company_name')
      ->where('contacts.customer_id', '=', $data['customer_id'])
      ->orderBy('contacts.id', 'DESC')
      ->get();
      $data['duplicate'] = 'duplicate';
      $data['status'] = 'draft';
      $data['attachment'] = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $id)
      ->get();
      $data['service_types'] = \App\Models\StampsServices::where(['status' => '1'])->get(['description', 'abbr'])->toArray();
      if ($data['attachment'] && count($data['attachment']) > 0) {
      foreach ($data['attachment'] AS $k => $val) {

      $original_file_name = explode('.', $val->original_file_name);
      $val->original_file_name = '2_' . $original_file_name[0] . '.' . $original_file_name[1];

      $file_name = explode('.', $val->file_name);
      $new_file_name = date('H') . '_' . $file_name[0] . '.' . $file_name[1];

      if (!file_exists(public_path() . '/attachment/work_order_document/temp_duplicate')) {
      $dir = mkdir(public_path() . '/attachment/work_order_document/temp_duplicate', 0777, true);
      \File::copy(public_path() . '/attachment/work_order_document/' . $val->file_name, public_path() . '/attachment/work_order_document/temp_duplicate/' . $val->file_name);
      } else {
      if (file_exists(public_path() . '/attachment/work_order_document/' . $val->file_name)) {
      \File::copy(public_path() . '/attachment/work_order_document/' . $val->file_name, public_path() . '/attachment/work_order_document/temp_duplicate/' . $val->file_name);
      }
      }
      if (file_exists(public_path() . '/attachment/work_order_document/temp_duplicate/' . $val->file_name)) {
      rename(public_path() . '/attachment/work_order_document/temp_duplicate/' . $val->file_name, public_path() . '/attachment/work_order_document/temp_duplicate/' . $new_file_name);
      //\Storage::move(public_path().'/attachment/work_order_document/temp_duplicate/'.$new_file_name, public_path().'/attachment/work_order_document/'.$new_file_name);
      \File::copy(public_path() . '/attachment/work_order_document/temp_duplicate/' . $new_file_name, public_path() . '/attachment/work_order_document/' . $new_file_name);
      unlink(public_path() . '/attachment/work_order_document/temp_duplicate/' . $new_file_name);
      }
      $val->file_name = $new_file_name;
      }
      }
      // Notes and correction
      $admin_email = \App\Models\SecondaryDocument::getAdminEmail();
      if (Auth::user()->hasRole('account-manager')) {
      $account_manager_email = \App\Models\SecondaryDocument::getOtherAccManagerEmailExcludingLoggedIn(Auth::user()->id);
      $customer_email = \App\Models\SecondaryDocument::getCustomerEmail($data['customer_id']);
      if (isset($account_manager_email) && !empty($account_manager_email)) {
      $data['note_emails'] = [
      $customer_email->email,
      $admin_email->email,
      ];
      foreach ($account_manager_email AS $each_emails) {
      array_push($data['note_emails'], $each_emails->email);
      }
      } else {
      $data['note_emails'] = [
      $customer_email->email,
      $admin_email->email,
      ];
      }
      } else {
      $account_manager_email = \App\Models\SecondaryDocument::getAllAccountManagerEmail();
      $customer_email = \App\Models\SecondaryDocument::getCustomerEmail($data['customer_id']);
      if (isset($account_manager_email) && !empty($account_manager_email)) {
      $data['note_emails'] = [
      $customer_email->email
      ];
      foreach ($account_manager_email AS $each_emails) {
      array_push($data['note_emails'], $each_emails->email);
      }
      } else {
      $data['note_emails'] = [
      $customer_email->email
      ];
      }
      }
      $data['notes'] = \App\Models\WorkOrderNotes::where('customer_id', '=', $data['customer_id'])
      ->where('work_order_id', '=', $id)
      ->orderBy('id', 'DESC')
      ->get();
      $data['corrections'] = \App\Models\WorkOrderCorrections::where('customer_id', '=', $data['customer_id'])
      ->where('work_order_id', '=', $id)
      ->orderBy('id', 'DESC')
      ->get();
      // Notes and correction
      //dd($notice[0]['type']);
      if ($notice[0]['type'] == 2) {
      $data['notice_fields_section1'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.workorder_id', $id)->where('work_order_fields.notice_id', $notice_id)->where('notice_fields.section', 1)->orderBy('sort_order', 'asc')->get();

      $data['notice_fields_section2'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.notice_id', $notice_id)->where('work_order_fields.workorder_id', $id)->where('notice_fields.section', 2)->orderBy('sort_order', 'asc')->get();
      $data['tab'] = 'project';
      return view('account_manager.research.edit_soft_notices', $data);
      } else {
      $data['tab'] = 'project';
      $data['notice_fields'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.notice_id', $notice_id)->where('work_order_fields.workorder_id', $id)->orderBy('sort_order', 'asc')->get();
      //dd($data);
      return view('account_manager.research.edit', $data);
      }
      } */

    public function get_workorders(Request $request)
    {
        if (Auth::user()->hasRole('account-manager')) {

            $WorkOrderFields = \App\Models\WorkOrderFields::select('notices.master_notice_id as master_notice_id', 'notices.name as notice_name', 'notices.type', 'notices.is_rescind as rescind_notice', 'work_orders.parent_id', 'work_orders.created_at', 'work_orders.file_name', 'work_orders.is_rescind as rescind_work_order', 'customers.company_name', 'users.name as customer_name', 'work_orders.user_id', 'work_orders.notice_id as notice_id', 'work_orders.status', 'usr.name as account_manager_name', 'work_orders.user_id', 'work_orders.account_manager_id', 'work_order_fields.workorder_id', 'work_order_fields.notice_id', 'users.name as user_name', DB::raw("(GROUP_CONCAT(notice_fields.name SEPARATOR '||')) as `field_names`"), DB::raw("(GROUP_CONCAT(value SEPARATOR '||')) as `field_values`"))
                ->rightjoin('notice_fields', 'notice_fields.id', '=', 'work_order_fields.notice_field_id')
                ->join('work_orders', 'work_orders.id', '=', 'work_order_fields.workorder_id')
                ->leftjoin('notices', 'notices.id', '=', 'work_orders.notice_id')
                ->leftjoin('users as usr', 'usr.id', '=', 'work_orders.account_manager_id')
                ->leftjoin('users', 'users.id', '=', 'work_orders.user_id')
                ->leftjoin('customers', 'customers.id', '=', 'work_orders.customer_id')
                ->groupBy('work_order_fields.workorder_id', 'work_order_fields.notice_id')
                ->where('work_orders.account_manager_id', Auth::user()->id);
        } else {
            $WorkOrderFields = \App\Models\WorkOrderFields::select('notices.master_notice_id as master_notice_id', 'notices.name as notice_name', 'notices.type', 'notices.is_rescind as rescind_notice', 'work_orders.parent_id', 'work_orders.created_at', 'work_orders.is_rescind as rescind_work_order', 'work_orders.file_name', 'customers.company_name', 'users.name as customer_name', 'work_orders.user_id', 'work_orders.notice_id as notice_id', 'work_orders.status', 'usr.name as account_manager_name', 'work_orders.user_id', 'work_orders.account_manager_id', 'work_order_fields.workorder_id', 'work_order_fields.notice_id', 'users.name as user_name', DB::raw("(GROUP_CONCAT(notice_fields.name SEPARATOR '||')) as `field_names`"), DB::raw("(GROUP_CONCAT(value SEPARATOR '||')) as `field_values`"))
                ->rightjoin('notice_fields', 'notice_fields.id', '=', 'work_order_fields.notice_field_id')
                ->join('work_orders', 'work_orders.id', '=', 'work_order_fields.workorder_id')
                ->leftjoin('notices', 'notices.id', '=', 'work_orders.notice_id')
                ->leftjoin('users as usr', 'usr.id', '=', 'work_orders.account_manager_id')
                ->leftjoin('users', 'users.id', '=', 'work_orders.user_id')
                ->leftjoin('customers', 'customers.id', '=', 'work_orders.customer_id')
                ->groupBy('work_order_fields.workorder_id', 'work_order_fields.notice_id');
        }

        if (($request->has('from_date')) && ($request->get('from_date') != NULL)) {
            $WorkOrderFields->whereDate('work_orders.created_at', '>=', date('Y-m-d', strtotime($request->get('from_date'))));
        }
        if (($request->has('to_date')) && ($request->get('to_date') != NULL)) {
            $WorkOrderFields->whereDate('work_orders.created_at', '<=', date('Y-m-d', strtotime($request->get('to_date'))));
        }
        $WorkOrderFields = $WorkOrderFields->get()->toArray();
        $data_table = [];

        foreach ($WorkOrderFields as $fields_data) {
            $field_names = explode('||', $fields_data['field_names']);
            $field_values = explode('||', $fields_data['field_values']);
            $recipients = \App\Models\Recipient::where('work_order_id', '=', $fields_data['workorder_id'])
                ->whereIn('category_id', [1, 2])
                ->select('category_id', 'name')
                ->orderBy('category_id', 'asc')
                ->get();
            $owner = 0;
            $general_contracted = 0;
            if (isset($recipients) && !empty($recipients)) {


                foreach ($recipients as $k => $v) {

                    if (isset($v->category_id) && $v->category_id == 1) {
                        if ($owner == 0) {
                            array_push($field_names, 'project_owner');
                            array_push($field_values, $v->name);
                            $owner = 1;
                        }
                    }
                    if (isset($v->category_id) && $v->category_id == 2) {
                        if ($general_contracted == 0) {
                            array_push($field_names, 'general_contracted');
                            array_push($field_values, $v->name);
                            $general_contracted = 1;
                        }
                    }
                }
                if ($owner == 0) {
                    array_push($field_names, 'project_owner');
                    array_push($field_values, "");
                }
                if ($general_contracted == 0) {
                    array_push($field_names, 'general_contracted');
                    array_push($field_values, "");
                }
            }

            $field_names_values = (count($field_names) == count($field_values)) ? array_combine($field_names, $field_values) : [];

            $field_names_values['default'] = '';
            $data_table[] = array_merge($fields_data, $field_names_values);
        }
        foreach ($data_table as $k => $each_record) {
            if (isset($each_record['city']) && $each_record['city'] != null) {
                $city_id = \App\Models\City::find($each_record['city']);
                $data_table[$k]['city'] = $city_id->name;
            }
        }
        foreach ($data_table as $k => $each_record) {
            if (isset($each_record['state']) && $each_record['state'] != null) {
                $state_id = \App\Models\State::find($each_record['state']);
                $data_table[$k]['state'] = $state_id->name;
            }
        }
        foreach ($data_table as $k => $each_record) {
            if (isset($each_record['rescind_work_order']) && $each_record['rescind_work_order'] == 1) {
                $data_table[$k]['notice_name'] = $data_table[$k]['notice_name'] . ' Amendment';
            }
        }
        foreach ($data_table as $k => $each_record) {
            if (isset($each_record['notice_id']) && $each_record['notice_id'] != null) {
                $notice_state_id = \App\Models\Notice::where('id', '=', $each_record['notice_id'])->first();

                $state_id = \App\Models\State::find($notice_state_id->state_id);
                $data_table[$k]['project_state'] = $state_id->name;
            }
        }
        //Calculationo fof due date 40 days from job start date
        //and row highlighted at 30 days
        //  dd($data_table);
        $due_date_master_ids = [config('constants.MASTER_NOTICE')['NTO']['ID'], config('constants.MASTER_NOTICE')['COL']['ID']];

        if (isset($data_table) && !empty($data_table)) {
            foreach ($data_table as $key => $value) {
                $master_notice_id = $value['master_notice_id'];
                foreach ($value as $k1 => $v1) {

                    if ($k1 == 'job_start_date' && $v1 != "") {
                        if ($master_notice_id == config('constants.MASTER_NOTICE')['NTO']['ID']) {
                            $duedateDays = 40 . 'days';
                            $highlightDays = 30 . 'days';
                            $data_table[$key]['due_date'] = date('m/d/Y', strtotime('+ ' . $duedateDays, strtotime($v1)));
                            $data_table[$key]['highligted_date'] = date('m/d/Y', strtotime('+ ' . $highlightDays, strtotime($v1)));
                        }
                    }
                    if ($k1 == 'clerk_of_court_recorded_date' && $v1 != "") {
                        if ($master_notice_id == config('constants.MASTER_NOTICE')['COL']['ID']) {
                            $duedateDays = 270 . 'days';
                            $highlightDays = 270 . 'days';
                            $data_table[$key]['due_date'] = date('m/d/Y', strtotime('+ ' . $duedateDays, strtotime($v1)));
                            $data_table[$key]['highligted_date'] = date('m/d/Y', strtotime('+ ' . $highlightDays, strtotime($v1)));
                        }
                    } else if ($k1 == 'clerk_of_court_recorded_date' && $v1 == "") {
                        $data_table[$key]['clerk_of_court_recorded_date'] = 'NA';
                    }
                    if (!isset($data_table[$key]['amount_due'])) {
                        $data_table[$key]['amount_due'] = 'NA';
                    }
                    if (!isset($data_table[$key]['clerk_of_court_recorded_date'])) {
                        $data_table[$key]['clerk_of_court_recorded_date'] = 'NA';
                    }
                    /*if ($k1 == 'last_date_on_the_job'){
                        if($master_notice_id==config('constants.MASTER_NOTICE')['COL']['ID'] || $master_notice_id==config('constants.MASTER_NOTICE')['NPN']['ID']){
                         // $duedateDays = 270 . 'days';
                          $highlightDays = 70 . 'days';
                          //$data_table[$key]['due_date'] = date('m/d/Y', strtotime('+ ' . $duedateDays, strtotime($v1)));
                          $data_table[$key]['highligted_date'] = date('m/d/Y', strtotime('+ ' . $highlightDays, strtotime($v1)));
                        }
                    }*/
                }
            }
        }  //dd($data_table);
        return Datatables::of($data_table)
            ->addColumn('actions', function ($workorder) {
                //return view('account_manager.research.actionworkorder', compact('workorder'))->render();
                if ($workorder['status'] == 0) {
                    return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                                <option value=''>Select</option>
                                <option value='add_note'>Add Note</option>  
                                </select>";
                } elseif ($workorder['status'] == 1) {
                    if ($workorder['rescind_notice'] == 1 && $workorder['rescind_work_order'] == 1) {
                        return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                                <option value=''>Select</option>
                                <option value='cancel_work_order'>Cancel This Work Order</option>
                                <option value='change_work_order'>Change Account Manager For Work Order</option>
                                <option value='add_note'>Add Note</option>
                                <option value='proceed_work_order'>Proceed With Work Order</option>
                                <option value='generate_work_order_pdf'>Generate PDF</option>
                                <option value='duplicate_work_order'>Duplicate This Work Order</option>

                                </select>";
                    } else {
                        return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                                <option value=''>Select</option>
                                <option value='cancel_work_order'>Cancel This Work Order</option>
                                <option value='change_work_order'>Change Account Manager For Work Order</option>
                                <option value='add_note'>Add Note</option>
                                <option value='proceed_work_order'>Proceed With Work Order</option>
                                <option value='generate_work_order_pdf'>Generate PDF</option>
                                <option value='duplicate_work_order'>Duplicate This Work Order</option>

                                </select>";
                    }
                } elseif ($workorder['status'] == 2 || $workorder['status'] == 3 || $workorder['status'] == 4) {
                    if ($workorder['rescind_notice'] == 1 && $workorder['rescind_work_order'] == 1) {
                        return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                                <option value=''>Select</option>
                                <option value='cancel_work_order'>Cancel This Work Order</option>
                                <option value='change_work_order'>Change Account Manager For Work Order</option>
                                <option value='continue_working'>Continue Working On This Work Order</option>
                                <option value='do_not_mail'>Do Not Mail Work Order</option>
                                <option value='add_note'>Add Note</option>
                                <option value='generate_work_order_pdf'>Generate PDF</option>
                                <option value='duplicate_work_order'>Duplicate This Work Order</option>

                                </select>";
                    } else {
                        return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                                  <option value=''>Select</option>
                                  <option value='cancel_work_order'>Cancel This Work Order</option>
                                  <option value='change_work_order'>Change Account Manager For Work Order</option>
                                  <option value='continue_working'>Continue Working On This Work Order</option>
                                  <option value='do_not_mail'>Do Not Mail Work Order</option>
                                  <option value='add_note'>Add Note</option>
                                  <option value='generate_work_order_pdf'>Generate PDF</option>
                                <option value='duplicate_work_order'>Duplicate This Work Order</option>

                                  </select>";
                    }
                } elseif ($workorder['status'] == 5) {
                    if ($workorder['rescind_notice'] == 1 && $workorder['rescind_work_order'] == 1) {
                        return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                                <option value=''>Select</option>
                                <option value='cancel_work_order'>Cancel This Work Order</option>
                                <option value='send_back'>Send Back To Proccessing</option>
                                <option value='change_work_order'>Change Account Manager For Work Order</option>
                                <option value='add_note'>Add Note</option>
                                <option value='generate_work_order_pdf'>Generate PDF</option>
                                <option value='duplicate_work_order'>Duplicate This Work Order</option>

                             </select>";
                    } else {
                        return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                                <option value=''>Select</option>
                                <option value='cancel_work_order'>Cancel This Work Order</option>
                                <option value='send_back'>Send Back To Proccessing</option>
                               
                                <option value='change_work_order'>Change Account Manager For Work Order</option>
                                <option value='add_note'>Add Note</option>
                                <option value='generate_work_order_pdf'>Generate PDF</option>
                                <option value='duplicate_work_order'>Duplicate This Work Order</option>

                             </select>";
                    }
                } elseif ($workorder['status'] == 6) {
                    if ($workorder['rescind_notice'] == 1 && $workorder['rescind_work_order'] == 0) {


                        $selectDropdown = "<option value=''>Select</option>
                                  <option value='add_note'>Add Note</option>
                                 
                                  <option value='rescind_work_order'>Rescind This Work Order</option>
                                  <option value='view_work_order'>View PDF</option>
                                  <option value='view_completed_work_order'>View Completed Work Order</option>
                                  <option value='generate_work_order_pdf'>Generate PDF</option>
                                <option value='duplicate_work_order'>Duplicate This Work Order</option>";
                    }
                    if ($workorder['rescind_notice'] == 1 && $workorder['rescind_work_order'] == 1) {

                        $selectDropdown = " <option value=''>Select</option>
                                  <option value='add_note'>Add Note</option>
                                  <option value='view_work_order'>View PDF</option>
                                  <option value='view_completed_work_order'>View Completed Work Order</option>
                                  <option value='generate_work_order_pdf'>Generate PDF</option>
                                <option value='duplicate_work_order'>Duplicate This Work Order</option>";
                    } else {


                        $selectDropdown = "<option value='add_note'>Add Note</option>
                                  <option value='view_work_order'>View PDF</option>
                                    <option value='view_completed_work_order'>View Completed Work Order</option>
                                    <option value='generate_work_order_pdf'>Generate PDF</option>
                                <option value='duplicate_work_order'>Duplicate This Work Order</option>";
                    }

                    if (
                        $workorder['master_notice_id'] == config('constants.MASTER_NOTICE')['COL']['ID']
                        || $workorder['master_notice_id'] == config('constants.MASTER_NOTICE')['NTO']['ID']
                    ) {
                        $selectDropdown .= "<option value='add_ldonj'>Add Last Date On The Job</option>";
                    }

                    if (
                        $workorder['master_notice_id'] == config('constants.MASTER_NOTICE')['COL']['ID']
                    ) {
                        $selectDropdown .= "<option value='add_ccrd'>Add Clerk of Court Recorded date</option>";
                    }

                    
                    
                    return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                                    <option value=''>Select</option>"
                        . $selectDropdown
                        . "</select>";
                } elseif ($workorder['status'] == 7) {
                    if ($workorder['rescind_notice'] == 1 && $workorder['rescind_work_order'] == 1) {
                        return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                                <option value=''>Select</option>
                                <option value='add_note'>Add Note</option>
                                <option value='generate_work_order_pdf'>Generate PDF</option>
                                <option value='duplicate_work_order'>Duplicate This Work Order</option>

                             </select>";
                    } else {
                        return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                                <option value=''>Select</option>
                                
                                <option value='add_note'>Add Note</option>
                                <option value='generate_work_order_pdf'>Generate PDF</option>
                                <option value='duplicate_work_order'>Duplicate This Work Order</option>

                             </select>";
                    }
                } elseif ($workorder['status'] == 8) {
                    if ($workorder['rescind_notice'] == 1 && $workorder['rescind_work_order'] == 1) {
                        return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                                <option value=''>Select</option>
                                <option value='continue_working'>Continue Working On This Work Order</option>
                                <option value='change_work_order'>Change Account Manager For Work Order</option>
                                <option value='add_note'>Add Note</option>
                                <option value='generate_work_order_pdf'>Generate PDF</option>
                                <option value='duplicate_work_order'>Duplicate This Work Order</option>

                             </select>";
                    } else {
                        return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                                <option value=''>Select</option>
                                <option value='continue_working'>Continue Working On This Work Order</option>
                                <option value='change_work_order'>Change Account Manager For Work Order</option>
                                <option value='add_note'>Add Note</option>
                                <option value='generate_work_order_pdf'>Generate PDF</option>
                                <option value='duplicate_work_order'>Duplicate This Work Order</option>

                             </select>";
                    }
                } else {
                    if ($workorder['rescind_notice'] == 1 && $workorder['rescind_work_order'] == 1) {
                        return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                                <option value=''>Select</option>
                                <option value='send_back'>Send Back To Proccessing</option>
                                <option value='do_not_mail'>Do Not Mail Work Order</option>
                                <option value='continue_working'>Continue Working On This Work Order</option>
                                <option value='proceed_work_order'>Proceed With Work Order</option>
                                <option value='add_note'>Add Note</option>
                                <option value='generate_work_order_pdf'>Generate PDF</option>
                                <option value='duplicate_work_order'>Duplicate This Work Order</option>

                              </select>";
                    } else {
                        return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                                <option value=''>Select</option>
                                <option value='send_back'>Send Back To Proccessing</option>
                                <option value='do_not_mail'>Do Not Mail Work Order</option>
                                
                                <option value='continue_working'>Continue Working On This Work Order</option>
                                <option value='proceed_work_order'>Proceed With Work Order</option>
                                <option value='add_note'>Add Note</option>
                                <option value='generate_work_order_pdf'>Generate PDF</option>
                                <option value='duplicate_work_order'>Duplicate This Work Order</option>
                                
                              </select>";
                    }
                }
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    public function add_note(Request $request)
    {
        $rules = [
            'note' => 'required',
            'email' => 'required|email',
        ];
        $validator = \Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            //return response()->json(['errors' => $validator->errors()->all()]);
            return response()->json(['result' => 'errors', 'message' => $validator->errors()->all()]);
        }
        $customer_id = \App\Models\WorkOrder::select('customer_id')->where('id', $request->work_order_id)->get()->toArray();
        if ($customer_id != null) {
            $customer_id = $customer_id[0]['customer_id'];
        }
        //dd($request->all());
        $request->request->set('customer_id', $customer_id);

        $result = \App\Models\WorkOrderNotes::create($request->all());
        Mail::to($request->email)->send(new \App\Mail\WorkOrderNote($result));

        return response()->json(['result' => 'success', 'message' => 'Note submitted successfully.']);
        //return redirect::to('account-manager/research')->with('success', 'Note submitted successfully.');
        //return redirect('account-manager/research');
    }

    public function store_recipients(Request $request, $id, $notice_id, $customer_id)
    {
        DB::beginTransaction();
        try {
            if (!is_numeric($request->city_id)) {
                $city = \App\Models\City::firstOrNew(array('name' => $request->city_id));
                //$city = new \App\Models\City();
                $city->name = $request->city_id;
                $city->state_id = $request->state_id;
                $city->zip_code = $request->zip;
                $city->save();
                $request->request->set('city_id', $city->id);
            }

            //  $rules = [
            //     'note' => 'required',
            //     'email' => 'required|email',
            // ];
            // $validator = \Validator::make(Input::all(), $rules);
            // if ($validator->fails()) {
            //     return response()->json(['errors'=>$validator->errors()->all()]);
            // }
            /* $category = \App\Models\Category::create([
          'name' => $request->category_id]);
          $data['customer_id'] = $customer_id;
          $request->request->set('category_id',$category->id); */

            $data['service_types'] = \App\Models\StampsServices::where(['status' => '1'])->get(['description', 'abbr', 'id'])->toArray();
            $data['package_types'] = \App\Models\StampsAddOns::join('stamps_service_package_addon', 'stamps_service_package_addon.add_on', '=', 'stamps_add_ons.id')
                ->join('stamps_service_type', 'stamps_service_type.id', '=', 'stamps_service_package_addon.service_type')
                ->join('stamps_packages', 'stamps_packages.id', '=', 'stamps_service_package_addon.package')
                ->where(['stamps_packages.status' => '1'])
                ->get([\DB::raw('DISTINCT(stamps_packages.name)'), 'stamps_packages.id'])->toArray();
            $data['add_ons'] = \App\Models\StampsAddOns::join('stamps_service_package_addon', 'stamps_service_package_addon.add_on', '=', 'stamps_add_ons.id')
                ->join('stamps_service_type', 'stamps_service_type.id', '=', 'stamps_service_package_addon.service_type')
                ->join('stamps_packages', 'stamps_packages.id', '=', 'stamps_service_package_addon.package')
                ->where(['stamps_add_ons.status' => '1'])
                ->where(['stamps_service_type.id' => 1])
                ->where(['stamps_packages.id' => 2])
                ->get([\DB::raw('DISTINCT(stamps_add_ons.id)'), 'stamps_add_ons.description'])->toArray();
            //dd( $data['service_types'],$data['package_types'],$data['add_ons']);
            if (!empty($data['add_ons'])) {
                for ($counter = 0; $counter < count($data['add_ons']); $counter++) {
                    $prohibited = \App\Models\StampsAddOnsProhibited::join('stamps_add_ons', 'stamps_add_ons_prohibited.prohibited', '=', 'stamps_add_ons.id')
                        ->where(['stamps_add_ons_prohibited.add_on' => $data['add_ons'][$counter]['id']])
                        ->pluck('prohibited')->toArray();

                    $required = \App\Models\StampsAddOnsRequired::join('stamps_add_ons', 'stamps_add_ons_required.required', '=', 'stamps_add_ons.id')
                        ->where(['stamps_add_ons_required.add_on' => $data['add_ons'][$counter]['id']])
                        ->pluck('required')->toArray();
                    $data['add_ons'][$counter]['prohibited'] = json_encode($prohibited);
                    $data['add_ons'][$counter]['required'] = json_encode($required);
                }
            }
            $data['label_generated_count'] = 0;

            $request->request->set('work_order_id', $id);

            //  $request->request->set('city_id', $request->recipient_city_id);
            $request->request->set('city_id', $request->city_id);
            $contracted_by = $request->name;
            $contact_id = $request->recipt_id;
            $workorder_data = WorkOrder::find($id);

            if (Auth::user()->hasRole('account-manager') || Auth::user()->hasRole('admin')) {
                $customerId = $workorder_data->customer_id;
            } else {
                $customerId = Auth::user()->customer->id;
            }
            $data['officers_directors'] = \App\Models\CustomerAgent::where('customer_id', '=', $customerId)
                ->get();
            if ($contracted_by != "") {
                /* check contracted by is exist or not */
                $contracted_by_check = \App\Models\Contact::where('company_name', $contracted_by)->where('customer_id', $customerId)->first(); //dd($contracted_by_check);
                if (empty($contracted_by_check)) {
                    $contact = \App\Models\Contact::create([
                        'customer_id' => $customerId,
                        'company_name' => $contracted_by,
                        'added_form'   => 'work_order',
                        'company_address' => $request->address,
                        'mailing_address' => $request->address,
                        'city_id' => $request->city_id,
                        'state_id' => $request->state_id,
                        'zip' => $request->zip,
                        'phone' => $request->contact,
                        'email' => $request->email,
                        'attn' => $request->attn,

                    ]);
                    $contact_id = $contact->id;
                }
            }
            $request->request->set('contact_id', $contact_id);
            $first_category = \App\Models\Category::where('name', 'Contracted By')->first();
            if ($first_category->id == $request->category_id) {
                /* check contract details exit or not */
                $check_contract_recipient_exist = \App\Models\Recipient::where(['category_id' => $first_category->id, 'work_order_id' => $request->work_order_id])->where('id', '!=', $request->recipient_id)->first();

                /* if not empty delete previouse record */
                if (!empty($check_contract_recipient_exist)) {
                    $contract_recipient_delete = \App\Models\Recipient::where(['category_id' => $request->category_id, 'work_order_id' => $request->work_order_id])->where('id', '!=', $request->recipient_id)->delete();
                }
            }

            if (isset($request->verified_usps_address)) {
                $clean_address = json_decode($request->verified_usps_address, true);

                $usps_address = [
                    'fullname' => $clean_address['Address']['FullName'] ?? '',
                    'company' => $clean_address['Address']['Company'] ?? '',
                    'department' => $clean_address['Address']['Department'] ?? '',
                    'address' => $clean_address['Address']['Address1'],
                    'address2' => $clean_address['Address']['Address2'] ?? '',
                    'address3' => $clean_address['Address']['Address3'] ?? '',
                    'city' => $clean_address['Address']['City'],
                    'state' => $clean_address['Address']['State'],
                    'zipcode' => $clean_address['Address']['ZIPCode'],
                    'zipcode_add_on' => $clean_address['Address']['ZIPCodeAddOn'],
                    'phone_number' => $clean_address['Address']['PhoneNumber'] ?? '',
                    'email' => $clean_address['Address']['EmailAddress'] ?? '',
                    'dpb' => $clean_address['Address']['DPB'],
                    'check_digit' => $clean_address['Address']['CheckDigit'],
                    'country' => 'US',
                    'comment' => $clean_address['AddressCleansingResult'],
                    'override_hash' => $clean_address['Address']['OverrideHash'],
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s"),
                ];
            } else {
                $usps_address = [];
            }


            if ($request->continue_recipient == 'add') {

                $Recipient = \App\Models\Recipient::create(
                    $request->except(['_token', 'country_id'])
                );
                if ($Recipient) {
                    if (!empty($usps_address)) {
                        $usps_address['recipient_id'] = $Recipient->id;
                        \App\Models\UspsAddress::insert($usps_address);
                    }
                    Session::flash('success', 'Your work order # ' . $id . ' recipient successfully updated.');
                } else {
                    Session::flash('success', 'Your work order # ' . $id . ' problem in record submitting.');
                }
            } else {
                /* $WorkOrderFields = \App\Models\WorkOrderFields::select('work_orders.parent_id', 'work_orders.created_at', 'work_orders.user_id', 'work_orders.customer_id','work_orders.is_rescind as rescind_work_order', 'work_orders.notice_id as notice_id', 'work_orders.status', 'work_orders.user_id', 'work_orders.account_manager_id','work_orders.completed_at as completed_date', 'work_order_fields.workorder_id', 'work_order_fields.notice_id', DB::raw("(GROUP_CONCAT(name SEPARATOR '||')) as `field_names`"), DB::raw("(GROUP_CONCAT(value SEPARATOR '||')) as `field_values`"))->join('notice_fields', 'notice_fields.id', '=', 'work_order_fields.notice_field_id')
                                ->join('work_orders', 'work_orders.id', '=', 'work_order_fields.workorder_id')->groupBy('work_order_fields.workorder_id', 'work_order_fields.notice_id')
                                ->where('work_orders.id', $id)
                                ->get()->toArray();
dd($WorkOrderFields);
            if(isset($WorkOrderFields) && !empty($WorkOrderFields)) {
                foreach ($WorkOrderFields as $fields_data) {
                    $field_names = explode('||', $fields_data['field_names']);
                    $field_values = explode('||', $fields_data['field_values']);
                    $field_names_values = array_combine($field_names, $field_values);
                    $field_names_values['default'] = '';
                    $result[] = array_merge($fields_data, $field_names_values);
                }
            }
            $data = $result[0];
            $parent_id = "";
            if (isset($data) && !empty($data)) {
              foreach ($data AS $key => $value) {
                if ($key == 'parent_work_order') {
                  $parent_id = $value;
                }
              }
            }*/


                // dd($request->name);

                $Recipient = \App\Models\Recipient::find($request->recipient_id);
                $Recipient->category_id = $request->category_id;
                $Recipient->name = $request->name;
                $Recipient->contact = $request->contact;
                $Recipient->address = $request->address;
                $Recipient->city_id = $request->city_id;
                $Recipient->state_id = $request->state_id;
                $Recipient->zip = $request->zip;
                $Recipient->email = $request->email;
                $Recipient->attn = $request->attn;
                $Recipient->contact_id = $request->contact_id;
                $Recipient->save();
                if ($Recipient) {
                    //Remove stamps label entry if generated for this recipient since the address has been changed
                    $chk_label_record = \App\Models\StampsLabel::where(['recipient_id' => $request->recipient_id])->orderBy('id', 'desc')->limit(1)->get()->toArray();
                    if (count($chk_label_record) > 0) {
                        \App\Models\StampsLabel::where(['recipient_id' => $request->recipient_id])->delete();
                    }
                    if (!empty($usps_address)) {
                        //Check if there is usps_address entry for this recipient then update else insert
                        $check_usps_record = \App\Models\UspsAddress::where(['recipient_id' => $request->recipient_id])->count();
                        if ($check_usps_record > 0) {
                            $usps_address['recipient_id'] = $request->recipient_id;
                            \App\Models\UspsAddress::where(['recipient_id' => $request->recipient_id])->update($usps_address);
                        } else {
                            $usps_address['recipient_id'] = $Recipient->id;
                            \App\Models\UspsAddress::insert($usps_address);
                        }
                    }
                    Session::flash('success', 'Your work order # ' . $id . ' recipient successfully updated. ');
                    $work_orders = \App\Models\WorkOrderFields::select('workorder_id')->join('notice_fields', 'work_order_fields.notice_field_id', 'notice_fields.id')->join('work_orders', 'work_orders.id', 'work_order_fields.workorder_id')->where('notice_fields.name', 'parent_work_order')->whereNotIn('work_orders.status', [5, 6])->where('work_order_fields.value', $id)->get()->toArray();

                    $parent_work_order_recipients = \App\Models\Recipient::select()->where('work_order_id', $id)->where('category_id', '!=', 21)->get()->toArray();

                    if (isset($work_orders) && count($work_orders) > 0) {

                        foreach ($work_orders as $k => $work_order) {

                            $delete_previous_recipients = \App\Models\Recipient::where('work_order_id', $work_order['workorder_id'])->where('parent_work_order', $id)->delete();
                            if (isset($parent_work_order_recipients) && count($parent_work_order_recipients) > 0) {
                                foreach ($parent_work_order_recipients as $k => $each_parent_recipient) {
                                    $usps_recipient = \App\Models\UspsAddress::select()->where('recipient_id', $each_parent_recipient['id'])->get()->toArray();

                                    $save_recipients = \App\Models\Recipient::create([
                                        'work_order_id' => $work_order['workorder_id'],
                                        'category_id' => $each_parent_recipient['category_id'],
                                        'name' => $each_parent_recipient['name'],
                                        'contact' => $each_parent_recipient['contact'],
                                        'address' => $each_parent_recipient['address'],
                                        'city_id' => $each_parent_recipient['city_id'],
                                        'attn' => $each_parent_recipient['attn'],
                                        'state_id' => $each_parent_recipient['state_id'],
                                        'zip' => $each_parent_recipient['zip'],
                                        'email' => $each_parent_recipient['email'],
                                        'attn' => $each_parent_recipient['attn'],
                                        'parent_work_order' => $id,

                                    ]);
                                    // dd($usps_recipient);
                                    if (!empty($save_recipients) && !empty($usps_recipient)) {
                                        $usps_address = [
                                            'fullname' => $usps_recipient[0]['fullname'],
                                            'address' => $usps_recipient[0]['address'],
                                            'address2' => $usps_recipient[0]['address2'],
                                            'address3' => $usps_recipient[0]['address3'],
                                            'company' => $usps_recipient[0]['company'],
                                            'city' => $usps_recipient[0]['city'],
                                            'state' => $usps_recipient[0]['state'],
                                            'zipcode' => $usps_recipient[0]['zipcode'],
                                            'zipcode_add_on' => $usps_recipient[0]['zipcode_add_on'],
                                            'phone_number' => $usps_recipient[0]['phone_number'],
                                            'email' => $usps_recipient[0]['email'],
                                            'dpb' => $usps_recipient[0]['dpb'],
                                            'check_digit' => $usps_recipient[0]['check_digit'],
                                            'country' => $usps_recipient[0]['country'],
                                            'comment' => $usps_recipient[0]['comment'],
                                            'override_hash' => $usps_recipient[0]['override_hash']
                                        ];
                                        $usps_address['recipient_id'] = $save_recipients->id;
                                        \App\Models\UspsAddress::insert($usps_address);
                                    }
                                }
                            }
                        }
                    }
                } else {
                    Session::flash('success', 'Your work order # ' . $id . ' problem in record submitting.');
                }
            }


            $notice = \App\Models\Notice::select('name', 'id', 'type', 'is_claim_of_lien', 'master_notice_id')->where('id', $notice_id)->get()->toArray();
            $data['notice'] = $notice[0];
            $data['cities'] = \App\Models\City::select()->get();
            $data['countries'] = \App\Models\Country::select()->get();
            $data['states'] = \App\Models\State::select()->orderBy('name', 'asc')->get();
            $data['projectTypes'] = \App\Models\ProjectType::select()->get();
            $data['customer_id'] = $customer_id;
            $data['customer_details'] = \App\Models\Customer::find($data['customer_id']);
            $data['categories'] = \App\Models\Category::select()->orderBy('name', 'ASC')->get();
            $data['project_roles'] = \App\Models\ProjectRoles::select()->orderBy('name', 'ASC')->get();
            $data['count'] = \App\Models\Recipient::select(DB::raw('COUNT(*) AS total'), 'categories.name')->groupBy('recipients.category_id')->join('categories', 'categories.id', '=', 'recipients.category_id')->where('work_order_id', $id)->get();
            $data['id'] = $id;
            $data['recipients'] = \App\Models\Recipient::select('recipients.id', 'work_order_id', 'category_id', 'name', 'contact', 'mobile', 'recipients.address', 'city_id', 'state_id', 'zip', 'fax', 'recipients.created_at', 'recipients.updated_at', \DB::raw('usps_address.address as usps_address'), \DB::raw('usps_address.city as usps_city'), \DB::raw('usps_address.state as usps_state'), 'zipcode', 'zipcode_add_on', 'dpb', 'check_digit', 'comment')
                ->leftJoin('usps_address', 'usps_address.recipient_id', '=', 'recipients.id')
                ->where('work_order_id', $id)
                ->get();
            $data['rush_hour'] = \App\Models\WorkOrder::select('rush_hour_charges', 'rush_hour_amount')->where('id', $id)->get();
            $verified_address_count = \App\Models\Recipient::select(\DB::raw('COUNT(usps_address.recipient_id) as verified_address_count'))->leftJoin('usps_address', 'usps_address.recipient_id', '=', 'recipients.id')->where('work_order_id', $id)->get();
            $data['verified_address_count'] = ($verified_address_count) ? $verified_address_count[0]['verified_address_count'] : 0;

            $data['notice_id'] = $notice_id;
            //                $result = \App\Models\WorkOrder::select('state_id')->join('notice_fields','notice_fields.id','=','work_orders.notice_id')->where('work_orders.id',$id)->get()->toArray();
            $result = \App\Models\WorkOrder::select('state_id', 'project_address_count')->join('notice_fields', 'notice_fields.notice_id', '=', 'work_orders.notice_id')->where('work_orders.id', $id)->limit(1)->get()->toArray();
            $data['project_address_count'] = $result[0]['project_address_count'];
            $data['names'] = \App\Models\Contact::select('contacts.id', 'contacts.company_name')
                //  ->where('contact_details.type', '=', 0)
                ->where('contacts.customer_id', '=', $data['customer_id'])
                // ->Join('contact_details', 'contact_details.contact_id', '=', 'contacts.id')
                ->orderBy('contacts.id', 'DESC')
                ->get();
            $work_order_status = \App\Models\WorkOrder::find($id);
            if ($work_order_status->status == '0') {
                $data['status'] = 'draft';
            } else {
                $data['status'] = 'request';
            }
            $data['work_order_status'] = $work_order_status->status;
            if ($data['recipients']) {
                for ($cnt = 0; $cnt < count($data['recipients']); $cnt++) {
                    $labels = \App\Models\StampsLabel::leftJoin('stamps_service_type', 'stamps_service_type.id', 'stamps_label.service_type')
                        ->leftJoin('stamps_packages', 'stamps_packages.id', 'stamps_label.package_type')
                        ->where(['recipient_id' => $data['recipients'][$cnt]['id']])
                        ->first(['stamps_label.*']);
                    if (!empty($labels) && $labels['generated_label'] == 'yes') {
                        $data['label_generated_count']++;
                        $data['recipients'][$cnt]['label_generated'] = 'yes';
                    } else {
                        $data['recipients'][$cnt]['label_generated'] = 'no';
                    }
                    $data['recipients'][$cnt]['recipient_shipping_labels'] = $labels;
                    $data['recipients'][$cnt]['add_ons'] = \App\Models\StampsAddOns::join('stamps_service_package_addon', 'stamps_service_package_addon.add_on', '=', 'stamps_add_ons.id')
                        ->join('stamps_service_type', 'stamps_service_type.id', '=', 'stamps_service_package_addon.service_type')
                        ->join('stamps_packages', 'stamps_packages.id', '=', 'stamps_service_package_addon.package')
                        ->where(['stamps_add_ons.status' => '1'])
                        ->where('stamps_service_type.id', '=', 1)
                        ->where('stamps_packages.id', '=', 2)
                        // ->where(function($query) use ($labels) {  
                        //     if(isset($labels) && !empty($labels))
                        //     {
                        //         $query->where('stamps_service_type.id','=',$labels['service_type']);
                        //         $query->where('stamps_packages.id','=',$labels['package_type']);
                        //     }	
                        // })
                        ->get([\DB::raw('DISTINCT(stamps_add_ons.id)'), 'stamps_add_ons.description'])->toArray();

                    if (!empty($data['recipients'][$cnt]['add_ons'])) {
                        $cnt1 = 1;
                        $temp_add_ons = $data['recipients'][$cnt]['add_ons'];
                        for ($counter = 0; $counter < count($temp_add_ons); $counter++) {
                            $prohibited = \App\Models\StampsAddOnsProhibited::join('stamps_add_ons', 'stamps_add_ons_prohibited.prohibited', '=', 'stamps_add_ons.id')
                                ->where(['stamps_add_ons_prohibited.add_on' => $temp_add_ons[$counter]['id']])
                                ->pluck('prohibited')->toArray();

                            $required = \App\Models\StampsAddOnsRequired::join('stamps_add_ons', 'stamps_add_ons_required.required', '=', 'stamps_add_ons.id')
                                ->where(['stamps_add_ons_required.add_on' => $data['recipients'][$cnt]['add_ons'][$counter]['id']])
                                ->pluck('required')->toArray();
                            $temp_add_ons[$counter]['prohibited'] = json_encode($prohibited);
                            $temp_add_ons[$counter]['required'] = json_encode($required);
                        }
                        unset($data['recipients'][$cnt]['add_ons']);
                        $data['recipients'][$cnt]['add_ons'] = $temp_add_ons;
                    }
                }
            }

            /* Notes and correction * */
            $admin_email = \App\Models\SecondaryDocument::getAdminEmail();
            /* if (Auth::user()->hasRole('account-manager')) {
          $account_manager_email = \App\Models\SecondaryDocument::getOtherAccManagerEmailExcludingLoggedIn(Auth::user()->id);
          $customer_email = \App\Models\SecondaryDocument::getCustomerEmail($work_order_status->customer_id);
          if (isset($account_manager_email) && !empty($account_manager_email)) {
          $data['note_emails'] = [
          $customer_email->email,
          $admin_email->email,
          ];
          foreach ($account_manager_email AS $each_emails) {
          array_push($data['note_emails'], $each_emails->email);
          }
          } else {
          $data['note_emails'] = [
          $customer_email->email,
          $admin_email->email,
          ];
          }
          } else {
          $account_manager_email = \App\Models\SecondaryDocument::getAllAccountManagerEmail();
          $customer_email = \App\Models\SecondaryDocument::getCustomerEmail($work_order_status->customer_id);
          if (isset($account_manager_email) && !empty($account_manager_email)) {
          $data['note_emails'] = [
          $customer_email->email
          ];
          foreach ($account_manager_email AS $each_emails) {
          array_push($data['note_emails'], $each_emails->email);
          }
          } else {
          $data['note_emails'] = [
          $customer_email->email
          ];
          }
          } */
            $data['note_emails'] = get_user_notice_email($work_order_status->customer_id);
            $data['notes'] = \App\Models\WorkOrderNotes::leftjoin('users', 'users.id', '=', 'work_order_notes.user_id')->where('customer_id', '=', $work_order_status->customer_id)
                ->where('work_order_notes.work_order_id', '=', $work_order_status->id)
                ->orderBy('work_order_notes.id', 'DESC')
                ->get();
            $data['corrections'] = \App\Models\WorkOrderCorrections::leftjoin('users', 'users.id', '=', 'work_order_corrections.user_id')->where('customer_id', '=', $work_order_status->customer_id)
                ->where('work_order_corrections.work_order_id', '=', $work_order_status->id)
                ->orderBy('work_order_corrections.id', 'DESC')
                ->get();
            /* Notes and correction * */
            if ($notice[0]['type'] == 2) {
                $data['notice_fields_section1'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.workorder_id', $id)->where('work_order_fields.notice_id', $notice_id)->where('notice_fields.section', 1)->orderBy('sort_order', 'asc')->get();

                $data['notice_fields_section2'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.notice_id', $notice_id)->where('work_order_fields.workorder_id', $id)->where('notice_fields.section', 2)->orderBy('sort_order', 'asc')->get();
                $data['tab'] = 'recipients';
                //To display parent work order
                foreach ($data['notice_fields_section1'] as $key => $value) {
                    $key = explode("_", $key);

                    if ($value) {

                        if ($value->name == 'parent_work_order') {
                            $parent_work_order = $value->value;
                            $data['parent_work_order'] = $parent_work_order;
                        }
                    }
                }
                //return view('account_manager.research.edit_soft_notices', $data);
            } else {
                $data['notice_fields'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.notice_id', $notice_id)->where('work_order_fields.workorder_id', $id)->orderBy('sort_order', 'asc')->get();
                $data['tab'] = 'recipients';
                //To display parent work order
                foreach ($data['notice_fields'] as $key => $value) {
                    $key = explode("_", $key);

                    if ($value) {

                        if ($value->name == 'parent_work_order') {
                            $parent_work_order = $value->value;
                            $data['parent_work_order'] = $parent_work_order;
                        }
                    }
                }
                //return view('account_manager.research.edit', $data);
            }
            DB::commit();

            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();

            return redirect()->back();
        }
    }

    function changeAccountManager(Request $request)
    {
        $workorder = \App\Models\WorkOrder::find($request->work_order_id);
        $workorder->account_manager_id = $request->account_manager_name;
        $workorder->save();
        if ($workorder) {
            Session::flash('success', 'Your work order # ' . $request->work_order_id . ' Account manager get changed Successfully.');
        } else {
            Session::flash('success', 'Your work order # ' . $request->work_order_id . ' Problem in Record saving');
        }

        return redirect()->back();
    }

    function getAccountManagerList()
    {
        $account_managers = \App\Models\User::select('id', 'name')->join('role_users', 'role_users.user_id', 'users.id')->where('role_users.role_id', 3)->get();
        return $account_managers;
    }

    public function store_attachments(Request $request)
    {
        $file = $request->file('file');
        $extension = $request->file('file')->getClientOriginalExtension(); // getting excel extension
        $original_file_name = $request->file('file')->getClientOriginalName();
        $dir = public_path() . '/attachment/work_order_document/';
        $filename = uniqid() . '_' . time() . '_' . date('Ymd') . '.' . $extension;
        $request->file('file')->move($dir, $filename);
        $data = [];
        $data['db_arr'] = [
            $request->document_type,
            $request->doc_value,
            $filename,
            $original_file_name
        ];
        if ($request->document_type == 1) {
            $request->document_type = 'Bond';
        } elseif ($request->document_type == 2) {
            $request->document_type = 'Notice to Commencement';
        } elseif ($request->document_type == 3) {
            $request->document_type = 'Permit';
        } elseif ($request->document_type == 4) {
            $request->document_type = 'Contract';
        } elseif ($request->document_type == 5) {
            $request->document_type = 'Invoice';
        } elseif ($request->document_type == 6) {
            $request->document_type = 'Final Release';
        } elseif ($request->document_type == 7) {
            $request->document_type = 'Partial Release';
        } elseif ($request->document_type == 8) {
            $request->document_type = 'Misc Document';
        } elseif ($request->document_type == 9) {
            $request->document_type = 'Signed Document';
        } elseif ($request->document_type == 10) {
            $request->document_type = 'Folio';
        } elseif ($request->document_type == 11) {
            $request->document_type = 'COL Signed';
        } elseif ($request->document_type == 12) {
            $request->document_type = 'Recorded COL/SCOL';
        } elseif ($request->document_type == 13) {
            $request->document_type = 'Pending Signature';
        }

        $data['ajax_arr'] = [
            $request->document_type,
            // $request->doc_value,
            $original_file_name,
            date('d M y h:i A'),
            $extension
        ];


        return json_encode($data);
    }

    public function getWorkOrdeNumber(Request $request)
    {
        $data = \App\Models\WorkOrder::where('id', '!=', $request->work_order_id)
            ->select('id')->get();
        return response()->json($data);
    }

    public function getWorkOrdeNumberV1(Request $request)
    {
        $query = $request->get('term', '');

        $datas = \App\Models\WorkOrder::where('id', '!=', $request->work_order_id)->where('id', 'LIKE', '%' . $query . '%')->orderBy('id')
            ->pluck('id')->toArray();
        return $datas;
    }

    public function destroy($id, $notice_id, $work_order_id, $customer_id)
    {

        $usps_address = \App\Models\UspsAddress::where('recipient_id', $id)->delete();
        $data['label_generated_count'] = 0;
        $result = \App\Models\Recipient::where('id', $id)->delete();
        if ($result) {
            Session::flash('success', 'Your work order # ' . $work_order_id . ' Record delete Successfully.');
        } else {
            Session::flash('success', 'Your work order # ' . $work_order_id . ' Problem in Record saving');
        }

        $notice = \App\Models\Notice::select('name', 'id', 'type', 'is_claim_of_lien', 'master_notice_id')->where('id', $notice_id)->get()->toArray();
        $data['notice'] = $notice[0];
        $data['cities'] = \App\Models\City::select()->get();
        $data['countries'] = \App\Models\Country::select()->get();
        $data['states'] = \App\Models\State::select()->orderBy('name', 'asc')->get();
        $data['categories'] = \App\Models\Category::select()->orderBy('name', 'ASC')->get();
        $data['project_roles'] = \App\Models\ProjectRoles::select()->orderBy('name', 'ASC')->get();
        $data['recipients'] = \App\Models\Recipient::select('recipients.id', 'work_order_id', 'category_id', 'name', 'contact', 'mobile', 'recipients.address', 'city_id', 'state_id', 'zip', 'fax', 'recipients.created_at', 'recipients.updated_at', \DB::raw('usps_address.address as usps_address'), \DB::raw('usps_address.city as usps_city'), \DB::raw('usps_address.state as usps_state'), 'zipcode', 'zipcode_add_on', 'dpb', 'check_digit', 'comment')->leftJoin('usps_address', 'usps_address.recipient_id', '=', 'recipients.id')->where('work_order_id', $work_order_id)->get();
        $data['rush_hour'] = \App\Models\WorkOrder::select('rush_hour_charges', 'rush_hour_amount')->where('id', $work_order_id)->get();
        $verified_address_count = \App\Models\Recipient::select(\DB::raw('COUNT(usps_address.recipient_id) as verified_address_count'))->leftJoin('usps_address', 'usps_address.recipient_id', '=', 'recipients.id')->where('work_order_id', $work_order_id)->get();
        $data['verified_address_count'] = ($verified_address_count) ? $verified_address_count[0]['verified_address_count'] : 0;

        $data['count'] = \App\Models\Recipient::select(DB::raw('COUNT(*) AS total'), 'categories.name')->groupBy('recipients.category_id')->join('categories', 'categories.id', '=', 'recipients.category_id')->where('work_order_id', $work_order_id)->get();
        $data['id'] = $work_order_id;
        $data['notice_id'] = $notice_id;
        $data['projectTypes'] = \App\Models\ProjectType::select()->get();
        $data['customer_id'] = $customer_id;
        $data['customer_details'] = \App\Models\Customer::find($data['customer_id']);
        $data['officers_directors'] = \App\Models\CustomerAgent::where('customer_id', '=', $data['customer_id'])
            ->get();
        //        $result = \App\Models\WorkOrder::select('state_id')->join('notice_fields', 'notice_fields.id', '=', 'work_orders.notice_id')->where('work_orders.id', $work_order_id)->get()->toArray();
        $result = \App\Models\WorkOrder::select('state_id', 'project_address_count')->join('notice_fields', 'notice_fields.notice_id', '=', 'work_orders.notice_id')->where('work_orders.id', $work_order_id)->limit(1)->get()->toArray();
        $data['project_address_count'] = $result[0]['project_address_count'];
        $data['names'] = \App\Models\Contact::select('contacts.id', 'contacts.company_name')
            //  ->where('contact_details.type', '=', 0)
            ->where('contacts.customer_id', '=', $data['customer_id'])
            // ->Join('contact_details', 'contact_details.contact_id', '=', 'contacts.id')
            ->orderBy('contacts.company_name', 'ASC')
            ->get();
        $work_order_status = \App\Models\WorkOrder::find($work_order_id);
        if ($work_order_status->status == '0') {
            $data['status'] = 'draft';
        } else {
            $data['status'] = 'request';
        }
        $data['work_order_status'] = $work_order_status->status;

        $data['service_types'] = \App\Models\StampsServices::where(['status' => '1'])->get(['description', 'abbr', 'id'])->toArray();
        $data['package_types'] = \App\Models\StampsAddOns::join('stamps_service_package_addon', 'stamps_service_package_addon.add_on', '=', 'stamps_add_ons.id')
            ->join('stamps_service_type', 'stamps_service_type.id', '=', 'stamps_service_package_addon.service_type')
            ->join('stamps_packages', 'stamps_packages.id', '=', 'stamps_service_package_addon.package')
            ->where(['stamps_packages.status' => '1'])
            ->get([\DB::raw('DISTINCT(stamps_packages.name)'), 'stamps_packages.id'])->toArray();

        if ($data['recipients']) {
            for ($cnt = 0; $cnt < count($data['recipients']); $cnt++) {
                $labels = \App\Models\StampsLabel::leftJoin('stamps_service_type', 'stamps_service_type.id', 'stamps_label.service_type')
                    ->leftJoin('stamps_packages', 'stamps_packages.id', 'stamps_label.package_type')
                    ->where(['recipient_id' => $data['recipients'][$cnt]['id']])
                    ->first(['stamps_label.*']);
                if (!empty($labels) && $labels['generated_label'] == 'yes') {
                    $data['label_generated_count']++;
                    $data['recipients'][$cnt]['label_generated'] = 'yes';
                } else {
                    $data['recipients'][$cnt]['label_generated'] = 'no';
                }
                $data['recipients'][$cnt]['recipient_shipping_labels'] = $labels;
                $data['recipients'][$cnt]['add_ons'] = \App\Models\StampsAddOns::join('stamps_service_package_addon', 'stamps_service_package_addon.add_on', '=', 'stamps_add_ons.id')
                    ->join('stamps_service_type', 'stamps_service_type.id', '=', 'stamps_service_package_addon.service_type')
                    ->join('stamps_packages', 'stamps_packages.id', '=', 'stamps_service_package_addon.package')
                    ->where('stamps_service_type.id', '=', 1)
                    ->where('stamps_packages.id', '=', 2)
                    ->where('stamps_add_ons.status', '=', '1')
                    // ->where(function($query) use ($labels) {  
                    //     if(isset($labels) && !empty($labels))
                    //     {
                    //         $query->where('stamps_service_type.id','=',$labels['service_type']);
                    //         $query->where('stamps_packages.id','=',$labels['package_type']);
                    //     }	
                    // })
                    ->get([\DB::raw('DISTINCT(stamps_add_ons.id)'), 'stamps_add_ons.description'])->toArray();


                if (!empty($data['recipients'][$cnt]['add_ons'])) {
                    $cnt1 = 1;
                    $temp_add_ons = $data['recipients'][$cnt]['add_ons'];
                    for ($counter = 0; $counter < count($temp_add_ons); $counter++) {
                        $prohibited = \App\Models\StampsAddOnsProhibited::join('stamps_add_ons', 'stamps_add_ons_prohibited.prohibited', '=', 'stamps_add_ons.id')
                            ->where(['stamps_add_ons_prohibited.add_on' => $temp_add_ons[$counter]['id']])
                            ->pluck('prohibited')->toArray();

                        $required = \App\Models\StampsAddOnsRequired::join('stamps_add_ons', 'stamps_add_ons_required.required', '=', 'stamps_add_ons.id')
                            ->where(['stamps_add_ons_required.add_on' => $data['recipients'][$cnt]['add_ons'][$counter]['id']])
                            ->pluck('required')->toArray();
                        $temp_add_ons[$counter]['prohibited'] = json_encode($prohibited);
                        $temp_add_ons[$counter]['required'] = json_encode($required);
                    }
                    unset($data['recipients'][$cnt]['add_ons']);
                    $data['recipients'][$cnt]['add_ons'] = $temp_add_ons;
                }
            }
        }

        /* Notes and correction * */
        $admin_email = \App\Models\SecondaryDocument::getAdminEmail();
        /* if (Auth::user()->hasRole('account-manager')) {
          $account_manager_email = \App\Models\SecondaryDocument::getOtherAccManagerEmailExcludingLoggedIn(Auth::user()->id);
          $customer_email = \App\Models\SecondaryDocument::getCustomerEmail($work_order_status->customer_id);
          if (isset($account_manager_email) && !empty($account_manager_email)) {
          $data['note_emails'] = [
          $customer_email->email,
          $admin_email->email,
          ];
          foreach ($account_manager_email AS $each_emails) {
          array_push($data['note_emails'], $each_emails->email);
          }
          } else {
          $data['note_emails'] = [
          $customer_email->email,
          $admin_email->email,
          ];
          }
          } else {
          $account_manager_email = \App\Models\SecondaryDocument::getAllAccountManagerEmail();
          $customer_email = \App\Models\SecondaryDocument::getCustomerEmail($work_order_status->customer_id);
          if (isset($account_manager_email) && !empty($account_manager_email)) {
          $data['note_emails'] = [
          $customer_email->email
          ];
          foreach ($account_manager_email AS $each_emails) {
          array_push($data['note_emails'], $each_emails->email);
          }
          } else {
          $data['note_emails'] = [
          $customer_email->email
          ];
          }
          } */
        $data['note_emails'] = get_user_notice_email($work_order_status->customer_id);
        $data['notes'] = \App\Models\WorkOrderNotes::leftjoin('users', 'users.id', '=', 'work_order_notes.user_id')->where('customer_id', '=', $work_order_status->customer_id)
            ->where('work_order_notes.work_order_id', '=', $work_order_status->id)
            ->orderBy('work_order_notes.id', 'DESC')
            ->get();
        $data['corrections'] = \App\Models\WorkOrderCorrections::leftjoin('users', 'users.id', '=', 'work_order_corrections.user_id')->where('customer_id', '=', $work_order_status->customer_id)
            ->where('work_order_corrections.work_order_id', '=', $work_order_status->id)
            ->orderBy('work_order_corrections.id', 'DESC')
            ->get();
        /* Notes and correction * */

        if ($notice[0]['type'] == 2) {
            $data['notice_fields_section1'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.workorder_id', $work_order_id)->where('work_order_fields.notice_id', $notice_id)->where('notice_fields.section', 1)->orderBy('sort_order', 'asc')->get();

            $data['notice_fields_section2'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.notice_id', $notice_id)->where('work_order_fields.workorder_id', $work_order_id)->where('notice_fields.section', 2)->orderBy('sort_order', 'asc')->get();
            $data['tab'] = 'recipients';
            //To display parent work order
            foreach ($data['notice_fields_section1'] as $key => $value) {
                $key = explode("_", $key);

                if ($value) {

                    if ($value->name == 'parent_work_order') {
                        $parent_work_order = $value->value;
                        $data['parent_work_order'] = $parent_work_order;
                    }
                }
            }
            //return view('account_manager.research.edit_soft_notices', $data);
        } else {
            $data['notice_fields'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.notice_id', $notice_id)->where('work_order_fields.workorder_id', $work_order_id)->orderBy('sort_order', 'asc')->get();
            $data['tab'] = 'recipients';
            //To display parent work order
            foreach ($data['notice_fields'] as $key => $value) {
                $key = explode("_", $key);

                if ($value) {

                    if ($value->name == 'parent_work_order') {
                        $parent_work_order = $value->value;
                        $data['parent_work_order'] = $parent_work_order;
                    }
                }
            }
            //return view('account_manager.research.edit', $data);
        }
        return redirect()->back();
    }

    public function restrictWorkOrder($work_order_id)
    {
        $result = \App\Models\WorkOrder::find($work_order_id);
        $result->update(['status' => 8]);
        $result->save();
        if ($result) {
            Session::flash('success', 'Your work order # ' . $work_order_id . ' Record Updated Successfully.');
        } else {
            Session::flash('success', 'Your work order # ' . $work_order_id . ' Problem in Record saving');
        }
        return redirect()->back();
    }

    public function BackToProccessing($work_order_id)
    {
        $result = \App\Models\WorkOrder::find($work_order_id);
        $result->update(['status' => 2]);
        $result->save();
        if ($result) {
            Session::flash('success', 'Your work order # ' . $work_order_id . ' Record Send Back To Proccessing Successfully.');
        } else {
            Session::flash('success', 'Your work order # ' . $work_order_id . ' Problem in Record saving');
        }
        return redirect()->back();
    }

    public function CyoBackToProccessing($work_order_id)
    {
        $result = \App\Models\Cyo_WorkOrders::find($work_order_id);
        $result->update(['status' => 2]);
        $result->save();
        if ($result) {
            Session::flash('success', 'Your work order # ' . $work_order_id . ' Record Send Back To Proccessing Successfully.');
        } else {
            Session::flash('success', 'Your work order # ' . $work_order_id . ' Problem in Record saving');
        }
        return redirect()->back();
    }

    /*
      firm mail for stamps
      recipient return receipt
      priority mail
      from address is project address added for work order
     */

    public function createLabel(Request $request)
    {
        $flash = ['', ''];
        $error_flash_messages = 'Could not save data for recipient ids ';
        $success_flash_messages = 'Saved data for recipient ids ';
        $user = \Auth::user();
        $now = date('Y-m-d H:i:s');
        $error_ids = [];
        $recipient_ids = $request->recipient_ids;
        //set manual charges

        //Get firm mail price from quickbooks
        $qbo = new \App\Http\Controllers\QuickBooksController();
        $qbo_response = $qbo->generateTokens(config('constants.QBO.SCOPE'), 'getProductDetails', ['customer_id' => $user->id, 'product_id' => env('FirmMail')]);

        if ($qbo_response['response'] == 'error') {
            $firm_mail = 1;
        } else {
            $firm_mail = $qbo_response['item_details']['Item']['UnitPrice'];
        }

        $qbo_response = $qbo->generateTokens(config('constants.QBO.SCOPE'), 'getProductDetails', ['customer_id' => $user->id, 'product_id' => env('ReturnReceiptRequested')]);

        if ($qbo_response['response'] == 'error') {
            $cert_rr = 1;
        } else {
            $cert_rr = $qbo_response['item_details']['Item']['UnitPrice'];
        }
        $qbo_response = $qbo->generateTokens(config('constants.QBO.SCOPE'), 'getProductDetails', ['customer_id' => $user->id, 'product_id' => env('ElectronicReturnReceipt')]);

        if ($qbo_response['response'] == 'error') {
            $cert_er = 1;
        } else {
            $cert_er = $qbo_response['item_details']['Item']['UnitPrice'];
        }

        $qbo_response = $qbo->generateTokens(config('constants.QBO.SCOPE'), 'getProductDetails', ['customer_id' => $user->id, 'product_id' => env('CertifiedMail')]);

        if ($qbo_response['response'] == 'error') {
            $cert_usps = 1;
        } else {
            $cert_usps = $qbo_response['item_details']['Item']['UnitPrice'];
        }

        //Get next day delivery price from quickbooks
        $qbo_response = $qbo->generateTokens(config('constants.QBO.SCOPE'), 'getProductDetails', ['customer_id' => $user->id, 'product_id' => env('NextDay')]);
        if ($qbo_response['response'] == 'error') {
            $next_day_delivery = 2;
        } else {
            $next_day_delivery = $qbo_response['item_details']['Item']['UnitPrice'];
        }

        /* if (isset($request->rush_hour)) {
            $rush_hour = 'yes';
            //Get rush hour charge from quickbooks
            $qbo_response = $qbo->generateTokens(config('constants.QBO.SCOPE'), 'getProductDetails', ['customer_id' => $user->id, 'product_id' => env('RushHourCharges')]);
            if ($qbo_response['response'] == 'error') {
                $rush_hour_amount = 10;
            } else {
                $rush_hour_amount = $qbo_response['item_details']['Item']['UnitPrice'];
            }
        } else {
            $rush_hour = 'no';
            $rush_hour_amount = 0;
        }*/
        if (isset($request->rush_hour)) {
            $rush_hour = 'yes';
            $rush_hour_amount = $request->rush_hour_charges;
        } else {
            $rush_hour = 'no';
            $rush_hour_amount = 0;
        }


        //Add the rush hour condition for work order
        \App\Models\WorkOrder::where(['id' => $request->workorder_id])->update(['rush_hour_charges' => $rush_hour, 'rush_hour_amount' => $rush_hour_amount]);

        for ($cnt = 1; $cnt <= count($recipient_ids); $cnt++) {
            $create_flag = 0;
            if (isset($request['type_of_mailing_' . $cnt]) && $request['type_of_mailing_' . $cnt] != '') {
                $type_of_label = isset($request['type_of_mailing_' . $cnt]) ? $request['type_of_mailing_' . $cnt] : 'stamps';
                if ($type_of_label == 'firm mail' || $type_of_label == 'next day delivery' || $type_of_label == 'Cert. USPS' || $type_of_label == 'Cert. RR' || $type_of_label == 'Cert. ER') {
                    if (isset($request['manual_inputs' . $cnt]) && $request['manual_inputs' . $cnt] != '') {
                        $tracking_number = $request['manual_inputs' . $cnt];
                        $generated_label = 'yes';
                        $create_flag = 1;
                    }
                } else if ($type_of_label == 'manual') {
                    if (isset($request['manual_inputs' . $cnt]) && $request['manual_inputs' . $cnt] != '') {
                        $manual_charges = $request['manual_amount' . $cnt];
                        $tracking_number = $request['manual_inputs' . $cnt];
                        $generated_label = 'yes';
                        $create_flag = 1;
                    }
                } else {
                    $tracking_number = '';
                    $generated_label = 'no';
                    $create_flag = 1;
                }

                if ($create_flag == 1) {
                    $rate = ($type_of_label == 'firm mail') ? $firm_mail : $next_day_delivery;
                    if ($type_of_label == "firm mail") {
                        $rate = $firm_mail;
                    } else if ($type_of_label == "next day delivery") {
                        $rate = $next_day_delivery;
                    } else if ($type_of_label == "Cert. USPS") {
                        $rate = $cert_usps;
                    } else if ($type_of_label == "Cert. RR") {
                        $rate = $cert_rr;
                    } else if ($type_of_label == "Cert. ER") {
                        $rate = $cert_er;
                    } else if ($type_of_label == "manual") {
                        $rate = $manual_charges;
                    }
                    $label_insert_array = [
                        'Amount' => $rate,
                        'tracking_number' => $tracking_number,
                        'recipient_id' => $recipient_ids[$cnt - 1],
                        'created_at' => $now,
                        'updated_at' => $now,
                        'type_of_label' => $type_of_label,
                        'generated_label' => $generated_label,
                        'add_ons' => "",
                        'ShipDate' => "",
                        'rates_data_id' => null, 
                        'type_of_mailing' => null
                    ];
                    //Check if label record present for the recipient
                    $chk_label_record = \App\Models\StampsLabel::where(['recipient_id' => $recipient_ids[$cnt - 1]])->orderBy('id', 'desc')->limit(1)->get()->toArray();

                    if (count($chk_label_record) > 0) {
                        \App\Models\StampsLabel::where(['recipient_id' => $recipient_ids[$cnt - 1]])->update($label_insert_array);
                        //Delete previous record if present
                        if ($chk_label_record[0]['rates_data_id'] != NULL) {
                            /* $rates_data_id = \App\Models\StampsRatesData::where(['id' => $chk_label_record[0]['rates_data_id']])->delete();*/
                        }

                        $label_id = $chk_label_record[0]['id'];
                    } else {
                        $label_id = \App\Models\StampsLabel::create($label_insert_array)->id;
                    }
                }

                //Storing data for stamps shipping labels
                if ($type_of_label == 'stamps') {
                    $today = date('Y-m-d');
                    $shipping_date = (isset($request['shipping_date' . ($cnt)]) && $request['shipping_date' . ($cnt)] != '') ? date('Y-m-d', strtotime($request['shipping_date' . ($cnt)])) : $today;

                    //Stamps.com get token
                    $wsdl           = config('constants.STAMPS.WSDL');
                    $integrationID  = config('constants.STAMPS.INTEGRATION_ID');
                    $username       = config('constants.STAMPS.USERNAME');
                    $password       = config('constants.STAMPS.PASSWORD');
                    $stamps = new \App\Helpers\StampsHelper($wsdl, $integrationID, $username, $password);

                    //Setting from address for creating label
                    $from_addr = [
                        'FullName' => env('FROM_NAME'),
                        'Address1' => env('FROM_ADDRESS'),
                        'City' => env('FROM_CITY'),
                        'State' => env('FROM_STATE'),
                        'ZIPCode' => env('FROM_ZIPCODE')
                    ];

                    $wsdl           = config('constants.STAMPS.WSDL');
                    $integrationID  = config('constants.STAMPS.INTEGRATION_ID');
                    $username       = config('constants.STAMPS.USERNAME');
                    $password       = config('constants.STAMPS.PASSWORD');

                    $stamps = new \App\Helpers\StampsHelper($wsdl, $integrationID, $username, $password);

                    // $packages = $request['package_type' . ($cnt)];
                    // $service_type = $request['service_types' . ($cnt)];
                    $packages = 'Letter';
                    $service_type = 'US-FC';
                    $add_on_ids = $request['add_ons_' . ($cnt)] ?? [];
                    $package_type_id = \App\Models\StampsPackages::where('name', $packages)->pluck('id')->toArray();
                    $service_type_id = \App\Models\StampsServices::where('abbr', $service_type)->pluck('id')->toArray();

                    if (count($add_on_ids) > 0) {
                        $addons = \App\Models\StampsAddOns::whereIn('id', $add_on_ids)->pluck('abbr')->toArray();
                        $addons_description = \App\Models\StampsAddOns::whereIn('id', $add_on_ids)->pluck('description')->toArray();
                    } else {
                        $addons = $addons_description = [];
                    }

                    $addons_description = !empty($addons_description) ? implode(',', $addons_description) : '';

                    $add_on_ids = !empty($add_on_ids) ? implode(',', $add_on_ids) : '';
                    $usps_addr = json_decode($request['usps_addr_' . $cnt], true);
                    $get_rates_values = [
                        "FromZIPCode" => $from_addr['ZIPCode'],
                        "ToZIPCode" => $usps_addr['zipcode'],
                        // "WeightLb" => 1,
                        // "InsuredValue" => 100,
                        "PackageType" => $packages,
                        "ShipDate" => $shipping_date,
                        "ServiceType" => $service_type,

                    ];


                    if (!empty($addons)) {
                        $get_rates_values["AddOns"] = $addons;

                        //add print layout for label
                        $get_rates_values["PrintLayout"] = 'SDC3820';
                        if (in_array('US-A-RRE', $addons)) {
                            $get_rates_values["PrintLayout"] = 'SDC3820';
                        } else if (in_array('US-A-RR', $addons)) {
                            $get_rates_values["PrintLayout"] = 'SDC3810';
                        }
                        //for future: //Certified - SDC3810
                    }
                    // dd($addons_decsription);
                    try {
                        $get_rates = $stamps->GetRates($get_rates_values);
                        \Log::info("");
                        \Log::info("\n----------\nStamp Get Rates on research wo: " . $request->workorder_id . "\n-----------\n" .
                            "\n>>USPS receipeint address<<\n");
                        \Log::info(print_r($usps_addr, TRUE));
                        \Log::info("\n>>Request<<\n" . print_r($get_rates_values, TRUE));
                        \Log::info("\n >>Rates response<<\n" . print_r($get_rates, TRUE));
                        \Log::info("");
                        if ($get_rates['status'] == 'success') {
                            $get_rates = $get_rates['result'];
                            //Store the rates data in stamps_rates_data table, and delete the record once label is generated successfully
                            $rates_data_id = \App\Models\StampsRatesData::create(['rates_data' => json_encode($get_rates)])->id;
                            //Store rates_data_id in stamps_label table for generating label later

                            $update_array = ['ShipDate' => $get_rates_values['ShipDate'], 'rates_data_id' => $rates_data_id, 'add_ons' => $add_on_ids, 'package_type' => $package_type_id[0], 'service_type' => $service_type_id[0], 'type_of_mailing' => $addons_description];
                            // dd($update_array,$label_id);
                            \App\Models\StampsLabel::where(['id' => $label_id])->update($update_array);
                            $success_ids[] = $recipient_ids[$cnt - 1];
                        } else {
                            \Log::error(json_encode($get_rates) . ' for recipient id' . $recipient_ids[$cnt - 1]);
                            $error_ids[] = $recipient_ids[$cnt - 1];
                        }
                    } catch (Exception $e) {
                        $error_ids[] = $recipient_ids[$cnt - 1];
                        \Log::error('Issue generating rates using stamps.com for recipient id ' . $recipient_ids[$cnt - 1]);
                    }
                } //inner if for type of mailing stamps
            } //if
        } //for


        if (!empty($error_ids)) {
            $error_ids = implode(', ', $error_ids);
            $error_flash_messages .= $error_ids;
            $flash = ['error', $error_flash_messages];
        }

        if (!empty($success_ids)) {
            $success_ids = implode(', ', $success_ids);
            $success_flash_messages .= $success_ids;
            $flash = ['success', $success_flash_messages];
        }

        if ($request->saveLabelData == 'no') {
            $mailing_status = WorkOrder::find($request->workorder_id);
            $mailing_status->status = 5;
            $mailing_status->save();
            $flash = ['success', 'Work order has been successfully proceed to mailing.'];
        }
        if ($request->recording_pending_signature != 0) {
            $mailing_status = WorkOrder::find($request->workorder_id);
            $mailing_status->status = $request->recording_pending_signature;
            $mailing_status->save();
            return json_encode(['status' => 'success', 'message' => 'Status successfully updated']);
        }
        if ($request->generate_pdf_flag != 0) {
            return "true";
        }
        if (empty($error_ids)) {
            if ($flash[0] == "" && $flash['1'] == "") {
                return redirect('account-manager/research')->with('success', 'Your Work order is in proccesing.');
            } else {
                return redirect('account-manager/research')->with($flash[0], $flash[1]);
            }
        } else {
            return redirect()->back()->with($get_rates['status'], 'W/O#' . $request->workorder_id . ' ' . $get_rates['message']['message']);
        }
    }

    public function get_contacts(Request $request)
    {


        $contacts = \App\Models\Contact::select()
            ->where('contacts.customer_id', '=', $request->customer_id)
            ->where('contacts.company_name', '=', $request->name)
            ->get();

        /* if(isEmpty($contacts)){

          } */
        return $contacts;
    }

    /**
     * Print Work Order
     */
    public function printWorkOrder($work_order_id, $work_order_type)
    {
        if (isset($work_order_type) && $work_order_type == 'cyo') {
            $work_order = \App\Models\Cyo_WorkOrders::find($work_order_id);
            $notice_id = $work_order->notice_id;

            $WorkOrderFields = \App\Models\Cyo_WorkOrder_fields::select('cyo__work_orders.parent_id', 'cyo__work_orders.created_at', 'cyo__work_orders.user_id', 'cyo__work_orders.customer_id', 'cyo__work_orders.is_rescind as rescind_work_order', 'cyo__work_orders.notice_id as notice_id', 'cyo__work_orders.status', 'cyo__work_orders.completed_at as completed_date', 'cyo__work_orders.user_id', 'cyo__work_orders.account_manager_id', 'cyo__work_order_fields.workorder_id', 'cyo__work_orders.notice_id', DB::raw("(GROUP_CONCAT(name SEPARATOR '||')) as `field_names`"), DB::raw("(GROUP_CONCAT(value SEPARATOR '||')) as `field_values`"))->join('notice_fields', 'notice_fields.id', '=', 'cyo__work_order_fields.notice_field_id')
                ->join('cyo__work_orders', 'cyo__work_orders.id', '=', 'cyo__work_order_fields.workorder_id')
                ->groupBy('cyo__work_order_fields.workorder_id', 'cyo__work_order_fields.notice_id')
                ->where('cyo__work_orders.id', $work_order_id)
                ->get()->toArray();
        } else {
            $work_order = WorkOrder::find($work_order_id);
            $notice_id = $work_order->notice_id;


            //dd($request->all());

            $WorkOrderFields = \App\Models\WorkOrderFields::select('work_orders.parent_id', 'work_orders.created_at', 'work_orders.user_id', 'work_orders.customer_id', 'work_orders.is_rescind as rescind_work_order',  'work_orders.notice_id as notice_id', 'work_orders.status', 'work_orders.user_id', 'work_orders.account_manager_id', 'work_orders.completed_at as completed_date', 'work_order_fields.workorder_id', 'work_order_fields.notice_id', DB::raw("(GROUP_CONCAT(name SEPARATOR '||')) as `field_names`"), DB::raw("(GROUP_CONCAT(value SEPARATOR '||')) as `field_values`"))->join('notice_fields', 'notice_fields.id', '=', 'work_order_fields.notice_field_id')
                ->join('work_orders', 'work_orders.id', '=', 'work_order_fields.workorder_id')->groupBy('work_order_fields.workorder_id', 'work_order_fields.notice_id')
                ->where('work_orders.id', $work_order_id)
                ->get()->toArray();
        } //dd($WorkOrderFields);
        $rescind_work_order = '';
        if ($WorkOrderFields[0]['rescind_work_order'] == 1) {
            $WorkOrderFields[0]['rescind_work_order'] = 'AMENDMENT';
        } else {
            $WorkOrderFields[0]['rescind_work_order'] = '';
        }
        $notice_type = \App\Models\Notice::find($notice_id);
        $result = [];
        if (isset($WorkOrderFields) && !empty($WorkOrderFields)) {
            foreach ($WorkOrderFields as $fields_data) {
                $field_names = explode('||', $fields_data['field_names']);
                $field_values = explode('||', $fields_data['field_values']);
                $field_names_values = array_combine($field_names, $field_values);
                $field_names_values['default'] = '';
                $result[] = array_merge($fields_data, $field_names_values);
            }
        }
        //Work Order Recipients
        if (isset($work_order_type) && $work_order_type == 'cyo') {

            $recipients = \App\Models\Cyo_WorkOrder_Recipient::leftjoin('cyo__stamps_labels', 'cyo__stamps_labels.recipient_id', '=', 'cyo__work_order__recipients.id')
                ->leftjoin('cyo__usps_addresses', 'cyo__usps_addresses.recipient_id', '=', 'cyo__work_order__recipients.id')
                ->where('work_order_id', '=', $work_order_id)
                ->select('cyo__work_order__recipients.address as ad','cyo__work_order__recipients.category_id','cyo__work_order__recipients.city_id','cyo__work_order__recipients.state_id','cyo__work_order__recipients.zip','cyo__work_order__recipients.attn','cyo__work_order__recipients.name', 'cyo__usps_addresses.*','cyo__stamps_labels.*')
                ->get();
            if ($notice_type->is_bond_of_claim) {
                $recipients_is_bond_of_claim = \App\Models\Cyo_WorkOrder_Recipient::leftjoin('cyo__stamps_labels', 'cyo__stamps_labels.recipient_id', '=', 'cyo__work_order__recipients.id')
                    ->leftjoin('cyo__usps_addresses', 'cyo__usps_addresses.recipient_id', '=', 'cyo__work_order__recipients.id')
                    ->where('work_order_id', '=', $work_order_id)->where('category_id', '9')
                    ->select('cyo__work_order__recipients.address as ad','cyo__work_order__recipients.category_id','cyo__work_order__recipients.city_id','cyo__work_order__recipients.state_id','cyo__work_order__recipients.zip','cyo__work_order__recipients.attn','cyo__work_order__recipients.name', 'cyo__usps_addresses.*','cyo__stamps_labels.*')
                    ->get();
            }
        } else {
            $recipients = \App\Models\Recipient::leftjoin('stamps_label', 'stamps_label.recipient_id', '=', 'recipients.id')
                ->leftjoin('usps_address', 'usps_address.recipient_id', '=', 'recipients.id')
                ->where('work_order_id', '=', $work_order_id)
                ->select('recipients.address as ad','recipients.city_id','recipients.category_id','recipients.state_id','recipients.zip','recipients.attn','recipients.name', 'usps_address.*','stamps_label.*')
                       ->get();
            if ($notice_type->is_bond_of_claim) {
                $recipients_is_bond_of_claim = \App\Models\Recipient::leftjoin('stamps_label', 'stamps_label.recipient_id', '=', 'recipients.id')
                    ->leftjoin('usps_address', 'usps_address.recipient_id', '=', 'recipients.id')
                    ->select('recipients.address as ad','recipients.city_id','recipients.category_id','recipients.state_id','recipients.zip','recipients.attn','recipients.name', 'usps_address.*','stamps_label.*')
                    ->where('work_order_id', '=', $work_order_id)->where('category_id', '9')
                    ->get();
            }
        }
        $data = $result[0];

        $template_content = \App\Models\NoticeTemplate::where('notice_id', '=', $notice_id)->first();

        if ($template_content == null) {
            \Session::flash('success', 'Pdf will not generate for this notice');
            return redirect::back();
        }

        $content = $template_content->content;

        $notice_state = \App\Models\State::find($template_content->state_id);

        $noticestate = $notice_state->name;
        $arr_variable = [];
        $arr_replace = [];
        $contracted_by = '';
        $legal_description = '';
        $bond_claim_number = '';
        $your_job_reference_no = '';
        $title = '';
        $recorded_day = '';
        $recorded_month = '';
        $project_name = '';
        $sub_subcontractor_received_type_mailing = "";
        $subcontractor_received_type_mailing = "";
        $general_contractor_received_type_mailing = "";
        $owner_received_type_mailing = "";
        $owner_received_nto_date = "";
        $general_contractor_received_nto_date = "";
        $sub_subcontractor_received_nto_date = "";
        $subcontractor_received_nto_date = "";
        $contracted_by_received_type_mailing = "";
        $contracted_by_received_nto_date = "";
        $parent_work_order = "";
        $project_owner = "";
        $parent_county = "";

        if (isset($content) && $content != NULL) {
            /*             * *********Dynamic Data**************** */
            if (isset($data) && !empty($data)) {
                foreach ($data as $key => $value) {
                    array_push($arr_variable, '{{' . $key . '}}');
                    if (
                        $key == 'date_request' || $key == 'enter_into_agreement' ||
                        $key == 'last_date_of_labor_service_furnished'
                    ) {
                        if (isset($value) && $value != NULL && $value != '') {
                            $value = date('M d,Y', strtotime($value));
                            //                          $date = \DateTime::createFromFormat("m-d-Y", $value->value);
                            //                           
                            //                            $date = $date->format('m-d-Y'); 
                            //                            $value->value = $date;
                        }
                    } else if ($key == 'workorder_id') {
                        if (isset($work_order_type) && $work_order_type == 'cyo') {
                            $value = 'CYO Work Order No:' . $value;
                        } else {
                            $value = 'Work Order No:' . $value;
                        }
                    } else if ($key == 'parent_work_order') {
                        $parent_county = "";
                        if (isset($value) && !empty($value)) {
                            $parent_work_order  = $value;
                            if (isset($work_order_type) && $work_order_type == 'cyo') {
                                $work_order_parent = \App\Models\Cyo_WorkOrders::find($value);
                            } else {
                                $work_order_parent = WorkOrder::find($value);
                            }
                            $notice_id = $work_order_parent->notice_id;
                            $notice_field_id = \App\Models\NoticeField::where('notice_id', $notice_id)->where('name', 'county')->get();
                            if (!empty($notice_field_id) && $notice_field_id->count()) {
                                $notice_field_id = $notice_field_id[0]->id;
                                $WorkOrderFields = \App\Models\WorkOrderFields::select('value')->where('workorder_id', $value)->where('notice_field_id', $notice_field_id)->get()->toArray();
                                if (!empty($WorkOrderFields)) {
                                    $county_id = $WorkOrderFields[0]['value'];
                                    $county_name = \App\Models\City::find($county_id);
                                    $parent_county = !empty($county_name) ? $county_name->county : "";
                                }
                            }
                            $value = 'Parent Work Order:' . $value;
                        }
                    } else if ($key == 'project_name') {

                        if (isset($value) && !empty($value)) {
                            $project_name = 'Project Name : ' . $value;
                            $value = 'Project Name : ' . $value;
                        } else {
                            $project_name = '';
                            $value = "";
                        }
                    } else if ($key == 'total_amount_satisfied') {
                        if (isset($value) && !empty($value)) {
                            if (is_numeric($value)) {
                                $value = number_format($value, 2);
                            }
                        } else {
                            $value = '00.00';
                        }
                    } else if ($key == 'completed_date') {
                        if (isset($value) && !empty($value)) {
                            $value = date('d M Y', strtotime($value));
                            $value = explode(' ', $value);
                            $value = $value[0] . ' day of ' . $value[1] . ', ' . $value[2];
                        } else {
                            $value = '___ day of _________, 20__';
                        }
                    } else if ($key == 'estimate_value_of_labor') {
                        if (isset($value) && !empty($value)) {
                            if (is_numeric($value)) {
                                $value = number_format($value, 2);
                            }
                        } else {
                            $value = '00.00';
                        }
                    } else if ($key == 'total_value') {
                        if (isset($value) && !empty($value)) {
                            if (is_numeric($value)) {
                                $value = '($' . number_format($value, 2) . ')';
                            }
                        } else {
                            $value = '($00.00)';
                        }
                    } else if ($key == 'unpaid_amount') {
                        if (isset($value) && !empty($value)) {
                            if (is_numeric($value)) {
                                $value = '($' . number_format($value, 2) . ')';
                            }
                        } else {
                            $value = '($00.00)';
                        }
                    } else if ($key == 'contract_price_or_estimated_value_of_labor') {
                        if (isset($value) && !empty($value)) {
                            if (is_numeric($value)) {
                                $value = number_format($value, 2);
                            }
                        } else {
                            $value = '00.00';
                        }
                    } else if ($key == 'estimate_of_amount_owed') {
                        if (isset($value) && !empty($value)) {
                            if (is_numeric($value)) {
                                $value = number_format($value, 2);
                            }
                        } else {
                            $value = '00.00';
                        }
                    } else if ($key == 'final_payment') {
                        if (isset($value) && !empty($value)) {
                            if (is_numeric($value)) {
                                $value = number_format($value, 2);
                            }
                        } else {
                            $value = '00.00';
                        }
                    } else if ($key == 'amount_due') {
                        if (isset($value) && !empty($value)) {
                            if (is_numeric($value)) {
                                $value = number_format($value, 2);
                            }
                        } else {
                            $value = '00.00';
                        }
                    } else if ($key == 'project_address') {
                        if (isset($value) && !empty($value)) {
                            $value=str_lreplace($value);
                            $value = strtoupper($value);
                            $project_address = 'Project:' . $value;
                        } else {
                            $project_address = '';
                        }
                    } else if ($key == 'additional_comment') {
                        if (isset($value) && !empty($value)) {
                            $value =  $value;
                        } else {
                            $value = '';
                        }
                    } else if ($key == 'county' || $key == 'notary_county' || $key == 'recorded_county') {

                        if (isset($value) && !empty($value)) {
                            $county = \App\Models\City::select('county')->find($value);

                            $value = $county->county;
                        }
                    } else if ($key == 'notary_state' || $key == 'recorded_state') {
                        if (isset($value) && !empty($value)) {
                            $notary_state_name = \App\Models\State::find($value);
                            $value = $notary_state_name->name;
                        }
                    } else if ($key == 'owner_received_type_mailing') {
                        if (isset($value) && !empty($value)) {
                            $value = $value;
                            $owner_received_type_mailing = $value;
                        }
                    } else if ($key == 'general_contractor_received_type_mailing') {
                        if (isset($value) && !empty($value)) {
                            $value =  $value;
                            $general_contractor_received_type_mailing = $value;
                        }
                    } else if ($key == 'subcontractor_received_type_mailing') {
                        if (isset($value) && !empty($value)) {
                            $value = $value;
                            $subcontractor_received_type_mailing = $value;
                        }
                    } else if ($key == 'sub_subcontractor_received_type_mailing') {
                        if (isset($value) && !empty($value)) {
                            $value = $value;
                            $sub_subcontractor_received_type_mailing = $value;
                        }
                    } else if ($key == 'contracted_by_received_type_mailing') {
                        if (isset($value) && !empty($value)) {
                            $value = $value;
                            $contracted_by_received_type_mailing = $value;
                        }
                    } else if ($key == 'authorise_agent') {
                        if (isset($value) && !empty($value)) {
                            //                            $signature = \App\Models\CustomerAgent::find($value);
                            //                            $value = $signature->first_name . ' ' . $signature->last_name;
                            //                            $title = $signature->title;
                            $value = $value;
                        } else {
                            $value = "";
                        }
                    } else if ($key == 'recorded_date') {
                        if (isset($value) && !empty($value)) {
                            $recorded_day = date('d', strtotime($value)); //October 2018
                            //Recorded Month
                            $recorded_month = date('F Y', strtotime($value));
                        }
                    } else if ($key == 'contracted_by') {
                        if (isset($value) && !empty($value)) {
                            $contracted_by = $value;
                        } else {
                            $contracted_by = '';
                        }
                    } else if ($key == 'legal_description') {
                        if (isset($value) && !empty($value)) {
                            $legal_description = $value;
                        } else {
                            $legal_description = '';
                        }
                    } else if ($key == 'bond_claim_number') {
                        if (isset($value) && !empty($value)) {
                            $bond_claim_number = $value;
                        } else {
                            $bond_claim_number = '';
                        }
                        // array_push($arr_replace, $value);
                    } else if ($key == 'your_job_reference_no') {
                        if (isset($value) && !empty($value)) {
                            $your_job_reference_no = $value;
                        } else {
                            $your_job_reference_no = '';
                        }
                        // array_push($arr_replace, $value);
                    } else if ($key == 'collection_for_attorneys_fees') {
                        if (isset($value) && !empty($value)) {
                            $value = 'these charges along with interest accruing at the rate of ' . $value . ' monthly and attorneys fees, ';
                        } else {
                            $value = '';
                        }
                    } else if ($key == 'owner_received_nto_date') {
                        if (isset($value) && !empty($value)) {
                            $owner_received_nto_date = $value;
                        }
                    } else if ($key == 'general_contractor_received_nto_date') {
                        if (isset($value) && !empty($value)) {
                            $general_contractor_received_nto_date = $value;
                        }
                    } else if ($key == 'sub_subcontractor_received_nto_date') {
                        if (isset($value) && !empty($value)) {
                            $sub_subcontractor_received_nto_date = $value;
                        }
                    } else if ($key == 'subcontractor_received_nto_date') {
                        if (isset($value) && !empty($value)) {
                            $subcontractor_received_nto_date = $value;
                        }
                    } else if ($key == 'contracted_by_received_nto_date') {
                        if (isset($value) && !empty($value)) {
                            $contracted_by_received_nto_date = $value;
                        }
                    } else if ($key == 'project_owner') {
                        if (isset($value) && !empty($value)) {
                            $value = $value;
                            $project_owner = $value;
                        }
                    }   /*else if ($key == 'job_start_date' || $key == 'last_date_on_the_job' || $key == 'owner_received_nto_date' ||
                            $key == 'general_contractor_received_nto_date' || $key == 'subcontractor_received_nto_date' ||
                            $key == 'sub_subcontractor_received_nto_date') {
                        if (isset($value) && !empty($value)) {
                            $value = date('F d,Y', strtotime($value));
                        }
                    } */ else if ($key == 'job_start_date' || $key == 'last_date_on_the_job') {
                        if (isset($value) && !empty($value)) {
                            $value = date('F d,Y', strtotime($value));
                        }
                    } else if ($key == "check_clear") {

                        if ($value) {
                            $value = "This form of release is expressly conditioned upon the undersigned’s receiving the actual amount referenced above and payment has been properly endorsed and funds paid/cleared by bank. ";
                        } else {
                            $value = "";
                        }
                    } else if ($key == "notary_seal") {
                        if ($value) {
                            $value = "<table style='width: 100%;'>
                                        <tbody>
                                            <tr>
                                                <td colspan='2'>
                                                    <table style='width: 50%;'>
                                                        <tbody>
                                                                <tr>
                                                                        <td style='vertical-align: top;width: 35%;padding: 1px 0; font-size:10px; line-height: 10px;'>State:</td>
                                                                        <td style='width: 50%;padding: 1px 0; font-size:10px; line-height: 10px;'>_________________________________________________________</td>
                                                                </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td colspan='2'>
                                                    <table style='width: 50%;'>
                                                        <tbody>
                                                                <tr>
                                                                        <td style='vertical-align: top;width: 35%;padding: 1px 0; font-size:10px; line-height: 10px;'>County:</td>
                                                                        <td style='width: 50%;padding: 1px 0; font-size:10px; line-height: 10px;'>_________________________________________________________</td>
                                                                </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                             </tr>
                                             
                                           <tr>
                                                <td colspan='2'>
                                                    <table style='width: 100%;'>
                                                        <tbody>
                                                                <tr>
                                                                        <td style='vertical-align: top;width: 35%;padding: 1px 0; font-size:10px; line-height: 10px;'> Sworn to (or affirmed) and subscribed before me this _______________(Month) ______(Date) _______________(Year)
                                                   BY_______________personally know_______________ Produce Identification __________________________________(Type of Identification)      
                                              </td>
                                                                      
                                                                </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td colspan='2'>
                                                    <table style='width: 50%;'>
                                                        <tbody>
                                                                <tr>
                                                                        <td style='vertical-align: top;width: 35%;padding: 1px 0; font-size:10px; line-height: 10px;'>Notary Signature:</td>
                                                                        <td style='width: 50%;padding: 2px 0;'>_________________________________________________________</td>
                                                                </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                             </tr>
                                              <tr>
                                                <td colspan='2'>
                                                    <table style='width: 50%;'>
                                                        <tbody>
                                                                <tr>
                                                                        <td style='vertical-align: top;width: 35%;padding: 1px 0; font-size:10px; line-height: 10px;'>Notary Stamp</td>
                                                                        <td style='width: 50%;padding: 1px 0; font-size:10px; line-height: 10px;'></td>
                                                                </tr>
                                                                <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                                <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                                <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                                <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                                <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                                <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                                <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                                <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                                <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                                <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                                <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                                <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                             </tr>
                                       </tbody></table>";
                            // dd($value);
                        } else {
                            $value = "";
                        }
                    }
                    array_push($arr_replace, $value);
                }
            }
        } //dd($legal_description);
        $amendment = '';
        if (array_key_exists('amendment', $arr_replace)) {
            // $amendment = '';
        } else {
            $amendment = '';
        }
        /*         * *********Dynamic Data**************** */

        /*         * *********Customer Registration Data**************** */

        if (!in_array("{{parent_work_order}}", $arr_variable)) {
            array_push($arr_variable, "{{parent_work_order}}");
            array_push($arr_replace, $parent_work_order);
        }

        if (!in_array("{{county}}", $arr_variable)) {
            array_push($arr_variable, "{{county}}");
            array_push($arr_replace, $parent_county);
        } else {
            $index = array_search('{{county}}', $arr_variable);
            if ($arr_replace[$index] == "") {
                $arr_replace[$index] = $parent_county;
            }
        }

        $customer_company_name = \App\Models\Customer::find($work_order->customer_id);
        $company_name = $customer_company_name->company_name;
        $customer_telephone = $customer_company_name->office_number;
        $company_address = $customer_company_name->mailing_address;
        $company_state_name = \App\Models\State::find($customer_company_name->mailing_state_id);
        $company_state = $company_state_name->name;
        $company_city_name = \App\Models\City::find($customer_company_name->mailing_city_id);
        $company_city = !empty($company_city_name) ? $company_city_name->name : '';
        $company_city_state_zip = $company_city . ' ' . $company_state . ' ' . $customer_company_name->mailing_zip;
        //Noc ,folio no and bond no
        if ($work_order_type && $work_order_type == 'cyo') {
            $document = \App\Models\Cyo_WorkOrder_Attachment::where('work_order_id', '=', $work_order_id)->get();
        } else {
            $document = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $work_order_id)->get();
        }
        /*         * *********Customer Registration Data**************** */
        /*         * *********Work Order Document Data**************** */
        $bond = '';
        $noc = '';
        $folio = '';
        $permit = '';
        foreach ($document as $key => $each_document) {
            //dd($each_document->type, $each_document->title);
            if ($each_document->type == 1 || $each_document->type == 2 || $each_document->type == 10 || $each_document->type == 3) {

                if ($each_document->type == 1 && $each_document->title != "") {
                    $bond = $bond . ' Bond:' . $each_document->title;
                }
                //Noc
                //if ($each_document->type == 2 && $each_document->title != "") {
                if ($each_document->type == 2) {
                    $noc = $noc . ' BK:' . $each_document->title;
                    //dd($noc);
                }

                //Folio
                if ($each_document->type == 10 && $each_document->title != "") {
                    $folio = $folio . ' Folio No: ' . $each_document->title;
                }
                /*
                 * If document type is Foilo
                 */
                if ($each_document->type == 3 && $each_document->title != "") {
                    $permit = $permit . " " . ' Permit # ' . $each_document->title;
                }
            }
        }
        /*         * *********Work Order Document Data**************** */
        /*         * *********Recipients Owner Data**************** */
        $owner_firm_mailing = '';
        $owner_name = '';
        $owner_address = '';
        $owner_city_state_zip = '';

        $contractor_name = '';
        $sub_contractor_name = '';
        $sub_sub_contractor_name = '';
        $sub_contractor = '';
        $recipient_owners = "";
        $owners = '<table><tbody>';
        $o = $count = 1; //dd($recipients);


        //BOTOTM:: if cat_id in (21, 2) then contract_by(21) show bottom
        $bothContractedByAndGeneralContractor =  $recipients->whereIn('category_id', [21, 2])
            ->groupBy('category_id')->toArray();
        //TOP:: if cat_id != 2 then 21 will show
        $showContractedByByGcToTop = array_key_exists(2, $bothContractedByAndGeneralContractor) ? 'GC' : (array_key_exists(21, $bothContractedByAndGeneralContractor) ? 'CB' : null);


        if (isset($recipients) && count($recipients) > 0) {
            foreach ($recipients as $k => $each_recipient) {
                if ($each_recipient->category_id == 22 || $each_recipient->category_id == 1) {

                    $owner_firm_mailing = $each_recipient->tracking_number;

                    $owner_name = $each_recipient->name;
                    //$owner_address = $each_recipient->address;
                    $owner_address = (!empty($each_recipient->address)) ? $each_recipient->address : $each_recipient->ad;
                           
                    $owner_address .= ((!empty($each_recipient->address2)) ? '<br />' : '').$each_recipient->address2;
                    $owner_address .= ((!empty($each_recipient->address3)) ? '<br />' : '').$each_recipient->address3;

                    $each_recipient->attn = str_lreplace($each_recipient->attn);
                    $owner_attn = strtoupper($each_recipient->attn) != "" ? " " . strtoupper($each_recipient->attn) . "<br/>" : "";
                    $state = "";
                    if (isset($each_recipient->state_id) && $each_recipient->state_id != null) {
                        $state_name = \App\Models\State::find($each_recipient->state_id);
                        $state = $state_name->name;
                    }
                    $city = "";
                    if (isset($each_recipient->city_id) && $each_recipient->city_id != null) {
                        $city_name = \App\Models\City::find($each_recipient->city_id);
                        $city = $city_name->name;
                    }
                    if ($each_recipient->city_id != NULL || $each_recipient->state_id != NULL) {
                        $owner_city_state_zip = $city . ' ' . $state . ' ' . $each_recipient->zip;
                    } else {
                        $owner_city_state_zip = '';
                    }
                    if ($o == 1) {
                        $owners = $owners . '<tr>';
                    }
                    $owners = $owners . '
					
						<td  width="340px;">
						<table scope="col">
							<tbody>
								<tr>
									<td style="vertical-align: top;padding: 0 2px; font-size:10px; line-height: 10px;">' . (($count > 1) ? '' : (strpos($content, '{{owners_with_label_property_owner}}') ?  'PROPERTY OWNER' : (strpos($content, '{{owners_with_label_to_property_owner}}') ? 'TO PROPERTY OWNER:' : 'TO:'))) . '</td>
									<td style="padding: 0 2px; font-size:10px; line-height: 10px;">' . $owner_firm_mailing . '<br/>
                                                                            ' . $owner_name . '<br/>' . nl2br($owner_attn) . nl2br($owner_address) . '<br/>
                                                                                    ' . $owner_city_state_zip . '<br/></td>

								</tr>
							</tbody>
						</table>
						</td>
						';
                    if ($o == 2) {
                        $owners = $owners . '<tr>';
                        $o = 1;
                        break;
                    }
                    $o++;
                    $count++;
                    //                        $owner_firm_mailing2 = $each_recipient->tracking_number;
                    //
                    //                        $owner_name2 = $each_recipient->name;
                    //                        $owner_address2 = $each_recipient->address;
                    //
                    //                        $state = "";
                    //                        if (isset($each_recipient->state_id) && $each_recipient->state_id != null) {
                    //                            $state_name = \App\Models\State::find($each_recipient->state_id);
                    //                            $state = $state_name->name;
                    //                        }
                    //                        $city = "";
                    //                        if (isset($each_recipient->city_id) && $each_recipient->city_id != null) {
                    //                            $city_name = \App\Models\City::find($each_recipient->city_id);
                    //                            $city = $city_name->name;
                    //                        }
                    //
                    //                        $owner_city_state_zip2 = $city . ' ' . $state . ' ' . $each_recipient->zip;
                    //
                    //                        $owner_html2 = '<td style="vertical-align: top;padding: 10px;">TO:</td>
                    //						<td style="padding: 10px;">' . $owner_firm_mailing2 . '
                    //						' . $owner_name2 . '
                    //						' . $owner_address2 . '
                    //						' . $owner_city_state_zip2 . '</td>
                    //					';
                } else if ($each_recipient->category_id == 2) {
                    $contractor_name = $each_recipient->name;
                } else if ($each_recipient->category_id == 3) {
                    $state = "";
                    if (isset($each_recipient->state_id) && $each_recipient->state_id != null) {
                        $state_name = \App\Models\State::find($each_recipient->state_id);
                        $state = $state_name->name;
                    }
                    $city = "";
                    if (isset($each_recipient->city_id) && $each_recipient->city_id != null) {
                        $city_name = \App\Models\City::find($each_recipient->city_id);
                        $city = $city_name->name;
                    }
                    if ($each_recipient->city_id != NULL || $each_recipient->state_id != NULL) {
                        $owner_city_state_zip = $city . ' ' . $state . ' ' . $each_recipient->zip;
                    } else {
                        $owner_city_state_zip = '';
                    }
                    $sub_contractor_name = $each_recipient->name;
                    $each_recipient->attn = str_lreplace($each_recipient->attn);
                    $sub_contractor = $each_recipient->name . ' ' . nl2br($each_recipient->attn) . ' ' . nl2br($each_recipient->address) . ' ' . $owner_city_state_zip;
                } else if ($each_recipient->category_id == 4) {

                    $sub_sub_contractor_name = $each_recipient->name;
                }
            }
        }
        $owners = $owners . '</tbody></table>';

        $contracted_by_recipient = '<table><tbody>';
        $o = 1;
        if (isset($recipients) && count($recipients) > 0) {
            foreach ($recipients as $k => $each_recipient) {
                if (($each_recipient->category_id == 21 && $showContractedByByGcToTop == 'CB') || ($each_recipient->category_id == 2 && $showContractedByByGcToTop == 'GC')) {

                    $owner_firm_mailing = $each_recipient->tracking_number;

                    $owner_name = $each_recipient->name;
                    $each_recipient->attn = str_lreplace($each_recipient->attn);
                    $owner_attn = strtoupper($each_recipient->attn) != "" ? " " . strtoupper($each_recipient->attn) . "<br/>" : "";
                    //$owner_address = $each_recipient->address;
                    $owner_address = (!empty($each_recipient->address)) ? $each_recipient->address : $each_recipient->ad;
                           
                    $owner_address .= ((!empty($each_recipient->address2)) ? '<br />' : '').$each_recipient->address2;
                    $owner_address .= ((!empty($each_recipient->address3)) ? '<br />' : '').$each_recipient->address3;

                    $state = "";
                    if (isset($each_recipient->state_id) && $each_recipient->state_id != null) {
                        $state_name = \App\Models\State::find($each_recipient->state_id);
                        $state = $state_name->name;
                    }
                    $city = "";
                    if (isset($each_recipient->city_id) && $each_recipient->city_id != null) {
                        $city_name = \App\Models\City::find($each_recipient->city_id);
                        $city = $city_name->name;
                    }
                    if ($each_recipient->city_id != NULL || $each_recipient->state_id != NULL) {
                        $owner_city_state_zip = $city . ' ' . $state . ' ' . $each_recipient->zip;
                    } else {
                        $owner_city_state_zip = '';
                    }
                    //Start first owner 
                    if ($o == 1) {
                        $contracted_by_recipient = $contracted_by_recipient . '<tr>';
                    }
                    $contracted_by_recipient = $contracted_by_recipient . '
                        <td>
                        <table scope="col">
                            <tbody>
                                <tr>
                                    <td style="vertical-align: top;padding: 0 2px;font-size:10px; line-height: 10px;">' . (strpos($content, '{{owners_with_label_property}}') ?  'PROPERTY OWNER' : (strpos($content, '{{owners_with_label_to_property_owner}}') ? 'TO PROPERTY OWNER' : 'TO')) . ':</td>
                                    <td style="padding: 0 2px;word-wrap:break-word; word-break:break-all;font-size:10px; line-height: 10px;">' . $owner_firm_mailing . '<br/>
                                                                            <p style="width: 200px; margin-bottom: 0;margin-top: 0;font-size:10px; line-height: 10px;">' . $owner_name . '</p>
                                                                                ' . nl2br($owner_attn) . nl2br($owner_address) . '<br/>
                                                                                    ' . $owner_city_state_zip . '<br/></td>
                                </tr>
                            </tbody>
                        </table>
                        </td>
                        ';
                    if ($o == 1) {
                        $contracted_by_recipient = $contracted_by_recipient . '<tr>';
                        //After second onwer break the loop
                        break;
                        $o = 1;
                    }
                    $o++;
                    //                        $owner_firm_mailing2 = $each_recipient->tracking_number;
                    //
                    //                        $owner_name2 = $each_recipient->name;
                    //                        $owner_address2 = $each_recipient->address;
                    //
                    //                        $state = "";
                    //                        if (isset($each_recipient->state_id) && $each_recipient->state_id != null) {
                    //                            $state_name = \App\Models\State::find($each_recipient->state_id);
                    //                            $state = $state_name->name;
                    //                        }
                    //                        $city = "";
                    //                        if (isset($each_recipient->city_id) && $each_recipient->city_id != null) {
                    //                            $city_name = \App\Models\City::find($each_recipient->city_id);
                    //                            $city = $city_name->name;
                    //                        }
                    //
                    //                        $owner_city_state_zip2 = $city . ' ' . $state . ' ' . $each_recipient->zip;
                    //
                    //                        $owner_html2 = '<td style="vertical-align: top;padding: 10px;">TO:</td>
                    //                      <td style="padding: 10px;">' . $owner_firm_mailing2 . '
                    //                      ' . $owner_name2 . '
                    //                      ' . $owner_address2 . '
                    //                      ' . $owner_city_state_zip2 . '</td>
                    //                  ';
                }
            }
        }

        $contracted_by_recipient = $contracted_by_recipient . '</tbody></table>';
        $surety_recipient = '<table><tbody>';
        $o = 1;
        if (isset($recipients) && count($recipients) > 0) {
            foreach ($recipients as $k => $each_recipient) {
                if ($each_recipient->category_id == 9) {

                    $owner_firm_mailing = $each_recipient->tracking_number;

                    $owner_name = $each_recipient->name;
                    $each_recipient->attn = str_lreplace($each_recipient->attn);
                    $owner_attn = strtoupper($each_recipient->attn) != "" ? " " . strtoupper($each_recipient->attn) . "<br/>" : "";
                    // $owner_attn = $each_recipient->attn != "" ? " " . $each_recipient->attn . "<br/>" : "";
                    //$owner_address = $each_recipient->address;
                    $owner_address = (!empty($each_recipient->address)) ? $each_recipient->address : $each_recipient->ad;
                           
                    $owner_address .= ((!empty($each_recipient->address2)) ? '<br />' : '').$each_recipient->address2;
                    $owner_address .= ((!empty($each_recipient->address3)) ? '<br />' : '').$each_recipient->address3;

                    $state = "";
                    if (isset($each_recipient->state_id) && $each_recipient->state_id != null) {
                        $state_name = \App\Models\State::find($each_recipient->state_id);
                        $state = $state_name->name;
                    }
                    $city = "";
                    if (isset($each_recipient->city_id) && $each_recipient->city_id != null) {
                        $city_name = \App\Models\City::find($each_recipient->city_id);
                        $city = $city_name->name;
                    }
                    if ($each_recipient->city_id != NULL || $each_recipient->state_id != NULL) {
                        $owner_city_state_zip = $city . ' ' . $state . ' ' . $each_recipient->zip;
                    } else {
                        $owner_city_state_zip = '';
                    }
                    //Start first owner 
                    if ($o == 1) {
                        $surety_recipient = $surety_recipient . '<tr>';
                    }
                    $surety_recipient = $surety_recipient . '
                        <td>
                        <table scope="col">
                            <tbody>
                                <tr>
                                    <td style="vertical-align: top;padding: 0 2px;font-size:10px; line-height: 10px;">' . (strpos($content, '{{owners_with_label_property}}') ?  'PROPERTY OWNER' : (strpos($content, '{{owners_with_label_to_property_owner}}') ? 'TO PROPERTY OWNER' : 'TO')) . ':</td>
                                    <td style="padding: 0 2px;word-wrap:break-word; word-break:break-all;font-size:10px; line-height: 10px;">' . $owner_firm_mailing . '<br/>
                                                                            <p style="width: 200px; margin-bottom: 0;margin-top: 0;font-size:10px; line-height: 10px;">' .$owner_name . '</p>
                                                                                ' . nl2br($owner_attn) . nl2br($owner_address) . '<br/>
                                                                                    ' . $owner_city_state_zip . '<br/></td>
                                </tr>
                            </tbody>
                        </table>
                        </td>
                        ';
                    if ($o == 3) {
                        $surety_recipient = $surety_recipient .'</tr>';
                        //After second onwer break the loop
                        $o = 0;
                        //break;
                    }
                    $o++;
                    //                        $owner_firm_mailing2 = $each_recipient->tracking_number;
                    //
                    //                        $owner_name2 = $each_recipient->name;
                    //                        $owner_address2 = $each_recipient->address;
                    //
                    //                        $state = "";
                    //                        if (isset($each_recipient->state_id) && $each_recipient->state_id != null) {
                    //                            $state_name = \App\Models\State::find($each_recipient->state_id);
                    //                            $state = $state_name->name;
                    //                        }
                    //                        $city = "";
                    //                        if (isset($each_recipient->city_id) && $each_recipient->city_id != null) {
                    //                            $city_name = \App\Models\City::find($each_recipient->city_id);
                    //                            $city = $city_name->name;
                    //                        }
                    //
                    //                        $owner_city_state_zip2 = $city . ' ' . $state . ' ' . $each_recipient->zip;
                    //
                    //                        $owner_html2 = '<td style="vertical-align: top;padding: 10px;">TO:</td>
                    //                      <td style="padding: 10px;">' . $owner_firm_mailing2 . '
                    //                      ' . $owner_name2 . '
                    //                      ' . $owner_address2 . '
                    //                      ' . $owner_city_state_zip2 . '</td>
                    //                  ';
                }
            }
            if ($o == 3) {
                $surety_recipient = $surety_recipient . '</tr>';
            }
        }
        $surety_recipient = $surety_recipient . '</tbody></table>';

        //print surety_rec & GC in one line
        $surety_recipient_and_general_contractor = '<table><tbody>';
        $o = 1;
        $recSuretyAndGC = $recipients->whereIn('category_id', [2, 9]);
        if (isset($recSuretyAndGC) && count($recSuretyAndGC) > 0) {
            foreach ($recSuretyAndGC as $k => $each_recipient) {
                $owner_firm_mailing = $each_recipient->tracking_number;

                $owner_name = $each_recipient->name;
                $each_recipient->attn = str_lreplace($each_recipient->attn);
                $owner_attn = strtoupper($each_recipient->attn) != "" ? " " . strtoupper($each_recipient->attn) . "<br/>" : "";
                // $owner_attn = $each_recipient->attn != "" ? " " . $each_recipient->attn . "<br/>" : "";
                //$owner_address = $each_recipient->address;
                $owner_address = (!empty($each_recipient->address)) ? $each_recipient->address : $each_recipient->ad;
                           
                $owner_address .= ((!empty($each_recipient->address2)) ? '<br />' : '').$each_recipient->address2;
                $owner_address .= ((!empty($each_recipient->address3)) ? '<br />' : '').$each_recipient->address3;

                $state = "";
                if (isset($each_recipient->state_id) && $each_recipient->state_id != null) {
                    $state_name = \App\Models\State::find($each_recipient->state_id);
                    $state = $state_name->name;
                }
                $city = "";
                if (isset($each_recipient->city_id) && $each_recipient->city_id != null) {
                    $city_name = \App\Models\City::find($each_recipient->city_id);
                    $city = $city_name->name;
                }
                if ($each_recipient->city_id != NULL || $each_recipient->state_id != NULL) {
                    $owner_city_state_zip = $city . ' ' . $state . ' ' . $each_recipient->zip;
                } else {
                    $owner_city_state_zip = '';
                }
                //Start first owner 
                if ($o == 1) {
                    $surety_recipient_and_general_contractor = $surety_recipient_and_general_contractor . '<tr>';
                }
                $surety_recipient_and_general_contractor = $surety_recipient_and_general_contractor . '
                    <td>
                    <table scope="col">
                        <tbody>
                            <tr>
                                <td style="vertical-align: top;padding: 0 2px;font-size:10px; line-height: 10px;">' . (strpos($content, '{{owners_with_label_property}}') ?  'PROPERTY OWNER' : (strpos($content, '{{owners_with_label_to_property_owner}}') ? 'TO PROPERTY OWNER' : 'TO')) . ':</td>
                                <td style="padding: 0 2px;word-wrap:break-word; word-break:break-all;font-size:10px; line-height: 10px;">' . $owner_firm_mailing . '<br/>
                                                                        <p style="width: 200px; margin-bottom: 0;margin-top: 0;font-size:10px; line-height: 10px;">' .$owner_name . '</p>
                                                                            ' . nl2br($owner_attn) . nl2br($owner_address) . '<br/>
                                                                                ' . $owner_city_state_zip . '<br/></td>
                            </tr>
                        </tbody>
                    </table>
                    </td>
                    ';
                if ($o == 3) {
                    $surety_recipient_and_general_contractor = $surety_recipient_and_general_contractor .'</tr>';
                    //After second onwer break the loop
                    $o = 0;
                    //break;
                }
                $o++;
            }
            if ($o == 3) {
                $surety_recipient_and_general_contractor = $surety_recipient_and_general_contractor . '</tr>';
            }
        }
        $surety_recipient_and_general_contractor = $surety_recipient_and_general_contractor . '</tbody></table>';
        //END: print surety_rec & GC in one line

        $is_bond_of_claim = '<table><tbody>';
        $o = 1;
        if (isset($recipients_is_bond_of_claim) && count($recipients_is_bond_of_claim) > 0) { //dd($recipients_is_bond_of_claim);
            foreach ($recipients_is_bond_of_claim as $k => $each_recipient) {
                if ($each_recipient->category_id == 9) {

                    $bond_of_claim_firm_mailing = $each_recipient->tracking_number;

                    $bond_of_claim_name = $each_recipient->name;
                    $each_recipient->attn = str_lreplace($each_recipient->attn);
                    $bond_of_claim_attn = strtoupper($each_recipient->attn) != "" ? " " . strtoupper($each_recipient->attn) . "<br/>" : "";
                    // $bond_of_claim_attn = $each_recipient->attn != "" ? " " . $each_recipient->attn . "<br/>" : "";
                   // $bond_of_claim_address = $each_recipient->address;
                    $bond_of_claim_address = (!empty($each_recipient->address)) ? $each_recipient->address : $each_recipient->ad;
                           
                    $bond_of_claim_address .= ((!empty($each_recipient->address2)) ? '<br />' : '').$each_recipient->address2;
                    $bond_of_claim_address .= ((!empty($each_recipient->address3)) ? '<br />' : '').$each_recipient->address3;

                    $state = "";
                    if (isset($each_recipient->state_id) && $each_recipient->state_id != null) {
                        $state_name = \App\Models\State::find($each_recipient->state_id);
                        $state = $state_name->name;
                    }
                    $city = "";
                    if (isset($each_recipient->city_id) && $each_recipient->city_id != null) {
                        $city_name = \App\Models\City::find($each_recipient->city_id);
                        $city = $city_name->name;
                    }
                    if ($each_recipient->city_id != NULL || $each_recipient->state_id != NULL) {
                        $bond_of_claim_city_state_zip = $city . ' ' . $state . ' ' . $each_recipient->zip;
                    } else {
                        $bond_of_claim_city_state_zip = '';
                    }
                    //Start first bond_of_claim 
                    if ($o == 1) {
                        $is_bond_of_claim = $is_bond_of_claim . '<tr>';
                    }
                    $is_bond_of_claim = $is_bond_of_claim . '<td>
                        <table scope="col">
                        <tbody>
                            <tr>
                            
                            <td style="padding: 0 2px;word-wrap:break-word; word-break:break-all;font-size:10px; line-height: 10px;">' . $bond_of_claim_firm_mailing . '<br/>
                                                                                        <p style="width: 200px; margin-bottom: 0;margin-top: 0;font-size:10px; line-height: 10px;">' . $bond_of_claim_name . '</p>
                                                                                            ' . $bond_of_claim_attn . $bond_of_claim_address . '<br/>
                                                                                                ' . $bond_of_claim_city_state_zip . '<br/></td>
                            </tr>
                        </tbody>
                        </table>
                    </td>';
                    if ($o == 3) {
                        $is_bond_of_claim = $is_bond_of_claim . '<tr>';
                        //After second onwer break the loop
                        break;
                        $o = 1;
                    }
                    $o++;
                    //                       
                    //          ';
                }
            }
        }
        $is_bond_of_claim = $is_bond_of_claim . '</tbody></table>';

        $bothContractedByAndGeneralContractor =  $recipients->whereIn('category_id', [21, 2])
            ->groupBy('category_id')->toArray();
        //TOP:: if cat_id != 2 then 21 will show
        $showContractedByByGcToTop = array_key_exists(2, $bothContractedByAndGeneralContractor) ? 2 : (array_key_exists(21, $bothContractedByAndGeneralContractor) ? 21 : null);

        $general_contractor = '<table><tbody>';
        $o = 1;
        if (isset($recipients) && count($recipients) > 0) { //dd($recipients_is_bond_of_claim);
            foreach ($recipients as $k => $each_recipient) {
                if ($each_recipient->category_id == $showContractedByByGcToTop) {

                    $bond_of_claim_firm_mailing = $each_recipient->tracking_number;

                    $bond_of_claim_name = $each_recipient->name;
                    $each_recipient->attn = str_lreplace($each_recipient->attn);
                    $bond_of_claim_attn = strtoupper($each_recipient->attn) != "" ? " " . strtoupper($each_recipient->attn) . "<br/>" : "";
                    // $bond_of_claim_attn = $each_recipient->attn != "" ? " " . $each_recipient->attn . "<br/>" : "";
                   // $bond_of_claim_address = $each_recipient->address;
                    $bond_of_claim_address = (!empty($each_recipient->address)) ? $each_recipient->address : $each_recipient->ad;
                    
                    $bond_of_claim_address .= ((!empty($each_recipient->address2)) ? '<br />' : '').$each_recipient->address2;
                    $bond_of_claim_address .= ((!empty($each_recipient->address3)) ? '<br />' : '').$each_recipient->address3;

                    $state = "";
                    if (isset($each_recipient->state_id) && $each_recipient->state_id != null) {
                        $state_name = \App\Models\State::find($each_recipient->state_id);
                        $state = $state_name->name;
                    }
                    $city = "";
                    if (isset($each_recipient->city_id) && $each_recipient->city_id != null) {
                        $city_name = \App\Models\City::find($each_recipient->city_id);
                        $city = $city_name->name;
                    }
                    if ($each_recipient->city_id != NULL || $each_recipient->state_id != NULL) {
                        $bond_of_claim_city_state_zip = $city . ' ' . $state . ' ' . $each_recipient->zip;
                    } else {
                        $bond_of_claim_city_state_zip = '';
                    }
                    //Start first bond_of_claim 
                    if ($o == 1) {
                        $general_contractor = $general_contractor . '<tr>';
                    }
                    $general_contractor = $general_contractor . '<td>
                        <table scope="col">
                        <tbody>
                            <tr>
                            
                            <td style="padding: 0 2px;word-wrap:break-word; word-break:break-all;font-size:10px; line-height: 10px;">' . $bond_of_claim_firm_mailing . '<br/>
                                <p style="width: 200px; margin-bottom: 0;margin-top: 0;font-size:10px; line-height: 10px;">' . $bond_of_claim_name . '</p>
                                    ' . $bond_of_claim_attn . $bond_of_claim_address . '<br/>
                                        ' . $bond_of_claim_city_state_zip . '<br/></td>
                            </tr>
                        </tbody>
                        </table>
                        </td>';
                    if ($o == 3) {
                        $general_contractor = $general_contractor . '<tr>';
                        //After second onwer break the loop
                        //break;
                        $o = 0;
                    }
                    $o++;
                }
            }
        }
        $general_contractor = $general_contractor . '</tbody></table>';
        // $surety_bond = $surety_bond . '</tbody></table>';


        if (isset($contractor_name) && !empty($contractor_name)) {
            $contractor_name = ', that the lienor served copies of the notice on the contractor, ' . $contractor_name;
        }


        if (isset($sub_contractor_name) && !empty($sub_contractor_name)) {
            $sub_contractor_name = 'and on the Sub Contractor';
        }
        if (isset($sub_sub_contractor_name) && !empty($sub_sub_contractor_name)) {
            $sub_sub_contractor_name = 'and on the Sub Sub Contractor';
        }

        if (isset($work_order_type) && $work_order_type == 'cyo') {

            $recipients_owners = \App\Models\Cyo_WorkOrder_Recipient::leftjoin('cyo__stamps_labels', 'cyo__stamps_labels.recipient_id', '=', 'cyo__work_order__recipients.id')
                ->where('work_order_id', '=', $work_order_id)
                ->whereIn('category_id', ['1', '22'])->select('name', 'category_id')->get()->toArray();
        } else {
            $recipients_owners = \App\Models\Recipient::leftjoin('stamps_label', 'stamps_label.recipient_id', '=', 'recipients.id')
                ->where('work_order_id', '=', $work_order_id)
                ->whereIn('category_id', ['1', '22'])->select('name', 'category_id')->get()->toArray();
        }
        $recipient_owners = "";
        if (isset($recipients_owners) && count($recipients_owners) > 0) {
            foreach ($recipients_owners as $k => $each_recipient) {
                if (count($recipients_owners) == 1) {
                    $recipient_owners = $each_recipient['name'];
                } else if ((count($recipients_owners) - 1) == $k) {
                    $recipient_owners = $recipient_owners . ' and ' . $each_recipient['name'];
                } else if ($k == (count($recipients_owners) - 2)) {
                    $recipient_owners = $recipient_owners . ' ' . $each_recipient['name'];
                } else {
                    $recipient_owners = $recipient_owners . $each_recipient['name'] . ' , ';
                }
            }
        }
        if (isset($work_order_type) && $work_order_type == 'cyo') {

            $recipient_contractors = \App\Models\Cyo_WorkOrder_Recipient::leftjoin('cyo__stamps_labels', 'cyo__stamps_labels.recipient_id', '=', 'cyo__work_order__recipients.id')
                ->where('work_order_id', '=', $work_order_id)
                ->where('category_id', '2')->select('name', 'category_id')->get()->toArray();
        } else {
            $recipient_contractors = \App\Models\Recipient::leftjoin('stamps_label', 'stamps_label.recipient_id', '=', 'recipients.id')
                ->where('work_order_id', '=', $work_order_id)
                ->where('category_id', '2')->select('name', 'category_id')->get()->toArray();
        }
        $recipient_general_contractor = "";
        if (isset($recipient_contractors) && count($recipient_contractors) > 0) {
            foreach ($recipient_contractors as $k => $each_recipient) {
                if (count($recipient_contractors) == 1) {
                    $recipient_general_contractor = $each_recipient['name'];
                } else if ((count($recipient_contractors) - 1) == $k) {
                    $recipient_general_contractor = $recipient_general_contractor . ' and ' . $each_recipient['name'];
                } else if ($k == (count($recipient_contractors) - 2)) {
                    $recipient_general_contractor = $recipient_general_contractor . ' ' . $each_recipient['name'];
                } else {
                    $recipient_general_contractor = $recipient_general_contractor . $each_recipient['name'] . ' , ';
                }
            }
        }

        if (isset($work_order_type) && $work_order_type == 'cyo') {

            $recipient_sub_contractors = \App\Models\Cyo_WorkOrder_Recipient::leftjoin('cyo__stamps_labels', 'cyo__stamps_labels.recipient_id', '=', 'cyo__work_order__recipients.id')
                ->where('work_order_id', '=', $work_order_id)
                ->where('category_id', '3')->select('name', 'category_id')->get()->toArray();
        } else {
            $recipient_sub_contractors = \App\Models\Recipient::leftjoin('stamps_label', 'stamps_label.recipient_id', '=', 'recipients.id')
                ->where('work_order_id', '=', $work_order_id)
                ->where('category_id', '3')->select('name', 'category_id')->get()->toArray();
        }
        $recipient_sub_contractor = "";
        if (isset($recipient_sub_contractors) && count($recipient_sub_contractors) > 0) {
            foreach ($recipient_sub_contractors as $k => $each_recipient) {
                if (count($recipient_sub_contractors) == 1) {
                    $recipient_sub_contractor = $each_recipient['name'];
                } else if ((count($recipient_sub_contractors) - 1) == $k) {
                    $recipient_sub_contractor = $recipient_sub_contractor . ' and ' . $each_recipient['name'];
                } else if ($k == (count($recipient_sub_contractors) - 2)) {
                    $recipient_sub_contractor = $recipient_sub_contractor . ' ' . $each_recipient['name'];
                } else {
                    $recipient_sub_contractor = $recipient_sub_contractor . $each_recipient['name'] . ' , ';
                }
            }
        }
        if (isset($work_order_type) && $work_order_type == 'cyo') {

            $recipient_sub_sub_contractors = \App\Models\Cyo_WorkOrder_Recipient::leftjoin('cyo__stamps_labels', 'cyo__stamps_labels.recipient_id', '=', 'cyo__work_order__recipients.id')
                ->where('work_order_id', '=', $work_order_id)
                ->where('category_id', '4')->select('name', 'category_id')->get()->toArray();
        } else {
            $recipient_sub_sub_contractors = \App\Models\Recipient::leftjoin('stamps_label', 'stamps_label.recipient_id', '=', 'recipients.id')
                ->where('work_order_id', '=', $work_order_id)
                ->where('category_id', '4')->select('name', 'category_id')->get()->toArray();
        }
        $recipient_sub_sub_contractor = "";
        if (isset($recipient_sub_sub_contractors) && count($recipient_sub_sub_contractors) > 0) {
            foreach ($recipient_sub_sub_contractors as $k => $each_recipient) {
                if (count($recipient_sub_sub_contractors) == 1) {
                    $recipient_sub_sub_contractor = $each_recipient['name'];
                } else if ((count($recipient_sub_sub_contractors) - 1) == $k) {
                    $recipient_sub_sub_contractor = $recipient_sub_sub_contractor . ' and ' . $each_recipient['name'];
                } else if ($k == (count($recipient_sub_sub_contractors) - 2)) {
                    $recipient_sub_sub_contractor = $recipient_sub_sub_contractor . ' ' . $each_recipient['name'];
                } else {
                    $recipient_sub_sub_contractor = $recipient_sub_sub_contractor . $each_recipient['name'] . ' , ';
                }
            }
        }
        if (isset($work_order_type) && $work_order_type == 'cyo') {

            $recipient_contractors = \App\Models\Cyo_WorkOrder_Recipient::leftjoin('cyo__stamps_labels', 'cyo__stamps_labels.recipient_id', '=', 'cyo__work_order__recipients.id')
                ->where('work_order_id', '=', $parent_work_order)
                ->where('category_id', '21')->select('name', 'category_id')->get()->toArray();
        } else {
            $recipient_contractors = \App\Models\Recipient::leftjoin('stamps_label', 'stamps_label.recipient_id', '=', 'recipients.id')
                ->where('work_order_id', '=', $parent_work_order)
                ->where('category_id', '21')->select('name', 'category_id')->get()->toArray();
        }
        $recipient_contracted_by_name = "";
        if (isset($recipient_contractors) && count($recipient_contractors) > 0) {
            foreach ($recipient_contractors as $k => $each_recipient) {
                if (count($recipient_contractors) == 1) {
                    $recipient_contracted_by_name = $each_recipient['name'];
                } else if ((count($recipient_contractors) - 1) == $k) {
                    $recipient_contracted_by_name = $recipient_contracted_by_name . ' and ' . $each_recipient['name'];
                } else if ($k == (count($recipient_contractors) - 2)) {
                    $recipient_contracted_by_name = $recipient_contracted_by_name . ' ' . $each_recipient['name'];
                } else {
                    $recipient_contracted_by_name = $recipient_contracted_by_name . $each_recipient['name'] . ' , ';
                }
            }
        }
        $final_col_owners = "";
        if (!empty($owner_received_nto_date) && !empty($owner_received_type_mailing)) {
            $final_col_owners = $recipient_owners;
        } else {
            $final_col_owners = $project_owner;
        }
        $final_col_content = "";
        if (empty($owner_received_nto_date) && empty($owner_received_type_mailing) && empty($general_contractor_received_nto_date) && empty($general_contractor_received_type_mailing) &&  empty($sub_subcontractor_received_nto_date) && empty($sub_subcontractor_received_type_mailing) && empty($subcontractor_received_nto_date) && empty($subcontractor_received_type_mailing) && empty($contracted_by_received_nto_date) && empty($contracted_by_received_type_mailing)) {
            $final_col_content = "";
        } else if (!empty($owner_received_nto_date) || !empty($owner_received_type_mailing) || !empty($general_contractor_received_nto_date) || !empty($general_contractor_received_type_mailing) ||  !empty($sub_subcontractor_received_nto_date) || !empty($sub_subcontractor_received_type_mailing) || !empty($subcontractor_received_nto_date) || !empty($sub_subcontractor_received_type_mailing) || !empty($contracted_by_received_nto_date) || !empty($contracted_by_received_type_mailing)) {

            if (!empty($owner_received_nto_date) && !empty($owner_received_type_mailing)) {
                $final_col_content .= 'and (if the lien is claimed by one not in privity with the owner) that the lienor served his notice to owner  on ' . $owner_received_nto_date . ' BY ' . $owner_received_type_mailing;
            }

            if (!empty($final_col_content) && !empty($general_contractor_received_nto_date) && !empty($general_contractor_received_type_mailing)) {
                $final_col_content .= ', ';
            } else if (!empty($final_col_content) && !empty($contracted_by_received_nto_date) && !empty($contracted_by_received_type_mailing) && empty($general_contractor_received_nto_date) && empty($general_contractor_received_type_mailing) && !empty($owner_received_nto_date) && !empty($owner_received_type_mailing)) {
                $final_col_content .= ', ';
            }

            if (!empty($general_contractor_received_nto_date) && !empty($general_contractor_received_type_mailing)) {
                $final_col_content .= (empty($final_col_content) ? ', ' : '') . '(if required) that the lienor served copies of the notice on the Contractor ' . $recipient_general_contractor . ' on ' . $general_contractor_received_nto_date . ' BY ' . $general_contractor_received_type_mailing;
            } else if (empty($general_contractor_received_nto_date) && empty($general_contractor_received_type_mailing) && !empty($contracted_by_received_nto_date) && !empty($contracted_by_received_type_mailing) && !empty($owner_received_nto_date) && !empty($owner_received_type_mailing)) {
                $final_col_content .= (empty($final_col_content) ? ', ' : '') . '(if required) that the lienor served copies of the notice on the Contractor ' . $recipient_contracted_by_name . ' on ' . $contracted_by_received_nto_date . ' BY ' . $contracted_by_received_type_mailing;
            }

            /* if(!empty($final_col_content) && !empty($contracted_by_received_nto_date) && !empty($contracted_by_received_type_mailing)){
              $final_col_content .= ', ';
            }
          
            if(!empty($contracted_by_received_nto_date) && !empty($contracted_by_received_type_mailing) ){
                $final_col_content .= (empty($final_col_content) ? ', ' : '').' the Contracted By '.$recipient_contracted_by_name.' on '.$contracted_by_received_nto_date.' BY '.$contracted_by_received_type_mailing;
               
            } */

            if (!empty($final_col_content) && !empty($subcontractor_received_nto_date) && !empty($subcontractor_received_type_mailing)) {
                $final_col_content .= ' and ';
            } else if (!empty($final_col_content) && empty($subcontractor_received_nto_date) && empty($subcontractor_received_type_mailing) && !empty($contracted_by_received_nto_date) && !empty($contracted_by_received_type_mailing) && !empty($general_contractor_received_nto_date) && !empty($general_contractor_received_type_mailing) && !empty($owner_received_nto_date) && !empty($owner_received_type_mailing)) {
                $final_col_content .= ' and ';
            }

            if (!empty($subcontractor_received_nto_date) && !empty($subcontractor_received_type_mailing)) {
                $final_col_content .= (empty($final_col_content) ? ', ' : '') . 'the Sub Contractor ' . $recipient_sub_contractor . ' on ' . $subcontractor_received_nto_date . ' BY ' . $subcontractor_received_type_mailing;
            } else if (empty($subcontractor_received_nto_date) && empty($subcontractor_received_type_mailing) && !empty($contracted_by_received_nto_date) && !empty($contracted_by_received_type_mailing) && !empty($general_contractor_received_nto_date) && !empty($general_contractor_received_type_mailing) && !empty($owner_received_nto_date) && !empty($owner_received_type_mailing)) {
                $final_col_content .= (empty($final_col_content) ? ', ' : '') . 'the Sub Contractor ' . $recipient_contracted_by_name . ' on ' . $contracted_by_received_nto_date . ' BY ' . $contracted_by_received_type_mailing;
            }

            if (!empty($final_col_content) && !empty($sub_subcontractor_received_nto_date) && !empty($sub_subcontractor_received_type_mailing)) {
                $final_col_content .= ', also';
            } else if (!empty($final_col_content) && empty($sub_subcontractor_received_nto_date) && empty($sub_subcontractor_received_type_mailing) && !empty($contracted_by_received_nto_date) && !empty($contracted_by_received_type_mailing) && !empty($general_contractor_received_nto_date) && !empty($general_contractor_received_type_mailing) && !empty($subcontractor_received_nto_date) && !empty($subcontractor_received_type_mailing) && !empty($owner_received_nto_date) && !empty($owner_received_type_mailing)) {
                $final_col_content .= ', also';
            }

            if (!empty($sub_subcontractor_received_nto_date) && !empty($sub_subcontractor_received_type_mailing)) {

                $final_col_content .=  (empty($final_col_content) ? ', ' : '') . ' a copy to Sub-Sub Contractor, ' . $recipient_sub_sub_contractor . ' on ' . $sub_subcontractor_received_nto_date . ', BY ' . $sub_subcontractor_received_type_mailing;
            } else if (empty($sub_subcontractor_received_nto_date) && empty($sub_subcontractor_received_type_mailing) && !empty($contracted_by_received_nto_date) && !empty($contracted_by_received_type_mailing) && !empty($subcontractor_received_nto_date) && !empty($subcontractor_received_type_mailing) && !empty($general_contractor_received_nto_date) && !empty($general_contractor_received_type_mailing) && !empty($owner_received_nto_date) && !empty($owner_received_type_mailing)) {
                $final_col_content .=  (empty($final_col_content) ? ', ' : '') . ' a copy to Sub-Sub Contractor, ' . $recipient_contracted_by_name . ' on ' . $contracted_by_received_nto_date . ', BY ' . $contracted_by_received_type_mailing;
            }
        }

        /*         * *********Recipients Owner Data**************** */
        //$printing_date = date('F d, Y');
        if(isset($work_order->completed_at)){
            $printing_date = date('F d, Y', strtotime($work_order->completed_at));
        }else{
            $printing_date = date('F d, Y');
        }
        //Printing Day
        $printing_day = date('d'); //October 2018
        //Printing Month
        $printing_month = date('F Y');
        $logo = public_path() . '/img/logo.png';
        $date_request = $data['date_request'];
        $year = explode('-', $date_request);

        $year = $year[2];
        $customer_agent = 'CIRA RANGEL';
        //$is_bond_of_claim = "";
        //dd($surety_recipient);
        $static_array_var = [
            '{{printing_date}}', '{{owners}}', '{{owners_with_label_property_owner}}', '{{owners_with_label_to_property_owner}}', '{{owner_name}}', '{{your_job_reference_no}}', '{{folio}}', '{{bond}}', '{{noc}}', '{{permit}}', '{{company_name}}', '{{company_address}}', '{{company_city_state_zip}}', '{{logo}}', '{{printing_day}}', '{{printing_month}}', '{{contractor_name}}', '{{sub_contractor_name}}', '{{sub_sub_contractor_name}}', '{{title}}', '{{year}}', '{{recorded_day}}', '{{recorded_month}}', '{{state}}', '{{contracted_by}}', '{{legal_description}}', '{{bond_claim_number}}', '{{project_name}}', '{{company_city}}', '{{company_state}}', '{{rescind_work_order}}', '{{amendment}}', '{{is_bond_of_claim}}', '{{general_contractor}}', '{{project_address}}', '{{recipient_owners}}', '{{sub_contractor}}', '{{contracted_by_recipient}}', '{{surety_recipient}}', '{{recipient_sub_sub_contractor}}', '{{recipient_sub_contractor}}', '{{recipient_general_contractor}}', '{{final_col_content}}', '{{recipient_contracted_by_name}}', '{{final_col_owners}}', '{{customer_agent}}', '{{surety_recipient_and_general_contractor}}'
        ];
        $static_array_replace = [
            $printing_date, $owners, $owners, $owners, $owner_name, $your_job_reference_no,
            $folio, $bond, $noc, $permit, $company_name, $company_address, $company_city_state_zip,
            $logo, $printing_day, $printing_month, $contractor_name,
            $sub_contractor_name, $sub_sub_contractor_name, $title, $year, $recorded_day, $recorded_month,
            $noticestate, $contracted_by, $legal_description, $bond_claim_number, $project_name, $company_city, $company_state, $rescind_work_order, $amendment, $is_bond_of_claim, $general_contractor, $project_address, $recipient_owners, $sub_contractor, $contracted_by_recipient, $surety_recipient, $recipient_sub_sub_contractor, $recipient_sub_contractor, $recipient_general_contractor, $final_col_content, $recipient_contracted_by_name, $final_col_owners, $customer_agent, $surety_recipient_and_general_contractor
        ];
        //dd($static_array_replace);
        //Merge static and dynamci array
        $variable_arr = array_merge($arr_variable, $static_array_var);
        $replace_arr = array_merge($arr_replace, $static_array_replace);

        $content = str_replace($variable_arr, $replace_arr, $content);
        //template content

        $final_content = '';

        if ($work_order_type && $work_order_type == 'cyo') {

            $all_recipients = \App\Models\Cyo_WorkOrder_Recipient::leftjoin('cyo__stamps_labels', 'cyo__stamps_labels.recipient_id', '=', 'cyo__work_order__recipients.id')
                ->leftjoin('cyo__usps_addresses', 'cyo__usps_addresses.recipient_id', '=', 'cyo__work_order__recipients.id')
                ->where('work_order_id', '=', $work_order_id)
                ->select('cyo__work_order__recipients.address as ad','cyo__work_order__recipients.category_id','cyo__work_order__recipients.city_id','cyo__work_order__recipients.state_id','cyo__work_order__recipients.zip','cyo__work_order__recipients.attn','cyo__work_order__recipients.name', 'cyo__usps_addresses.*','cyo__stamps_labels.*')
                ->get();
        } else {
            $all_recipients = \App\Models\Recipient::leftjoin('stamps_label', 'stamps_label.recipient_id', '=', 'recipients.id')
                ->leftjoin('usps_address', 'usps_address.recipient_id', '=', 'recipients.id')
                ->where('work_order_id', '=', $work_order_id)
                ->select('recipients.address as ad','recipients.city_id','recipients.category_id','recipients.state_id','recipients.zip','recipients.attn','recipients.name', 'usps_address.*','stamps_label.*')
                ->get();
        }

        //BOTOTM:: if cat_id in (21, 2) then contract_by(21) show bottom
        $bothContractedByAndGeneralContractor =  $all_recipients->whereIn('category_id', [21, 2])
            ->groupBy('category_id')->toArray();
        //$showContractedByToBottom = ($bothContractedByAndGeneralContractor->count() == 2) ? true: false;

        //TOP:: if cat_id != 2 then 21 will show
        //$showContractedByToTop = !array_key_exists(2, $bothContractedByAndGeneralContractor);
        //$showContractedByByGcToTop = array_key_exists(2, $bothContractedByAndGeneralContractor) ? 2 : (array_key_exists(21, $bothContractedByAndGeneralContractor) ? 21 : null);

        $showContractedByByGcToTop = array_key_exists(2, $bothContractedByAndGeneralContractor) ? 'GC' : (array_key_exists(21, $bothContractedByAndGeneralContractor) ? 'CB' : null);

        $allrecipientfinalcontent = '<table style = "width: 680px;font-size: 10px;margin: 0 auto;table-layout: fixed;page-break-after:always;">';
        $master_notice_id = [config('constants.MASTER_NOTICE')['BCOL']['ID'], config('constants.MASTER_NOTICE')['SBC']['ID'], config('constants.MASTER_NOTICE')['WRL']['ID']];


        if (strpos($content, '{{firm_with_label_noticing_party}}')) {
            $firm_label = 'NOTICING PARTY:';
            $content = str_replace('{{firm_with_label_noticing_party}}', "", $content);
        } else if (strpos($content, '{{firm_with_label_notifying_party}}')) {
            $firm_label = 'Notifying Party:';
            $content = str_replace('{{firm_with_label_notifying_party}}', "", $content);
        } else if (strpos($content, '{{firm_with_label_claimant}}')) {
            $firm_label = 'CLAIMANT:';
            $content = str_replace('{{firm_with_label_claimant}}', "", $content);
        } else {
            $firm_label = 'Firm:';
            $content = str_replace('{{firm_with_label_noticing_party}}', "", $content);
        }
        if ($notice_type->type == 2) {
            if (isset($your_job_reference_no) && !empty($your_job_reference_no)) {
                $your_job_reference_no = "W/O #" . $your_job_reference_no;
            }
        }

        if ($work_order_type && $work_order_type == 'cyo') {
            $c_name = "";
        }else{
            if (strpos($content, 'CIRA RANGEL, Agent')) {
                $c_name = '';
            } else {
                $c_name = 'CIRA RANGEL, Agent';
            }
        }

        $master_notice_auth_sign = [config('constants.MASTER_NOTICE')['BCOL']['ID'], config('constants.MASTER_NOTICE')['WRL']['ID'], config('constants.MASTER_NOTICE')['SBC']['ID'], config('constants.MASTER_NOTICE')['NPN']['ID']];
        if (in_array($notice_type->master_notice_id, $master_notice_auth_sign)) {
            $c_name = "";
        }
        if (strpos($content, '{{customer_telephone}}')) {
            $customer_telephone = $customer_telephone . '<br/>';
            $content = str_replace('{{customer_telephone}}', "", $content);
        } else {
            $customer_telephone = '';
        }

        if (isset($all_recipients) && count($all_recipients) > 0) {
            //All Recipient on main page
            $allrecipientshtml = '';
            if ($notice_type->is_claim_of_lien != 1) {
                $allrecipientshtml = $allrecipientshtml . '<tr>
			<td  colspan="3">
				<table>
          <tr>
						<td style="vertical-align: top; font-size: 10px; line-height: 10px;">' . $firm_label . '</td>
										<td style="padding:2px; font-size: 10px; line-height: 10px;">' . $company_name . '<br />
						' . $company_address . '<br />
            ' . $customer_telephone
                    . $company_city_state_zip . '<br/>' . $your_job_reference_no . '<p style="font-weight: bold; font-size: 10px; line-height: 10px;">' . $c_name . '</p></td>
					</tr>
                                    
				
			</table>
      </td>
    </tr>';
            }
             //dd($all_recipients);
            $surety = 1;
            $con = 1;
            $i = 1;

            if (!in_array($notice_type->master_notice_id, $master_notice_id)) {
                foreach ($all_recipients as $k => $each) {
                    if (isset($each->state_id) && $each->state_id != NULL) {
                        $each_sub_state_name = \App\Models\State::find($each->state_id);
                        $each_sub_state = $each_sub_state_name->name;
                    }
                    if (isset($each->city_id) && $each->city_id != NULL) {
                        $each_sub_city_name = \App\Models\City::find($each->city_id);
                        $each_sub_city = $each_sub_city_name->name;
                    }
                    if ($each->city_id != NULL || $each->state_id != NULL) {
                        $each_city_state_zip = "";
                        if ($each->city_id != NULL) {
                            $each_city_state_zip .= $each_sub_city . ' ';
                        }
                        if ($each->state_id != NULL) {
                            $each_city_state_zip .= $each_sub_state . ' ';
                        }
                        $each_city_state_zip .= $each->zip;
                    } else {
                        $each_city_state_zip = '';
                    }
                    if ((!($each->category_id == 22  && $notice_type->owners_html_in_pdf == 1) && !($each->category_id == 1  && $notice_type->owners_html_in_pdf == 1))) {
                        if ($notice_type->is_claim_of_lien != 1) {
                            if ($notice_type->master_notice_id == config('constants.MASTER_NOTICE')['NPN']['ID']) {
                  

                                if ($each->category_id == 9) {
                                    /* if ($surety != 1) {
                                        $all_attn = $each->attn != "" ? '<div style="width:200.33px;"> ' . $each->attn . '</div>' : "";

                                        $address = $each->address;
                                        $address .= ((!empty($each->address2)) ? '<br />' : '').$each->address2;
                                        $address .= ((!empty($each->address3)) ? '<br />' : '').$each->address3;
                                        
                                        $allrecipientshtml = $allrecipientshtml . '
                            <td style="width: 30%;padding: 1px 10px;text-align:left;font-size: 10px; line-height: 10px;" scope="col" >
                                <div style="width:200.33px;">' . $each->tracking_number . '</div>
                                <div style="width:200.33px;">' . $each->name . '</div>' . $all_attn . '
                                <div style="width:200.33px;">' . nl2br($address) . '</div>
                                <div>' . $each_city_state_zip . '</div>
                                                                             
                                    </td>';
                                    }
                                    $surety++;
                                   
                                    if ($i == 3) {
                                        $allrecipientshtml = $allrecipientshtml . '</tr>
                                            </table>
                                            </td>
                                            </tr>';
                                        $i = 0;
                                    }
                                    $i++; */
                                } else if ($each->category_id == 21 && $showContractedByByGcToTop == 'CB') {

                                }else if ($each->category_id == 2 && $showContractedByByGcToTop == 'GC') {
                                }
                                else {

                                    if ($i == 1) {
                                        $allrecipientshtml = $allrecipientshtml . '<tr>
                                        <td colspan = "3">
                                        <table>
    
                                        <tr>';
                                    }

                                $each->attn = str_lreplace($each->attn);
                                $all_attn = strtoupper($each->attn) != "" ? '<div style="width:200.33px;"> ' . strtoupper($each->attn) . '</div>' : "";

                                    //$address = $each->address;
                                    $address = (!empty($each->address)) ? $each->address : $each->ad;
                            
                                    $address .= ((!empty($each->address2)) ? '<br />' : '').$each->address2;
                                    $address .= ((!empty($each->address3)) ? '<br />' : '').$each->address3;
                                    
                                    $allrecipientshtml = $allrecipientshtml . '
                            <td style="width: 30%;padding: 1px 10px;text-align:left;font-size: 10px; line-height: 10px;" scope="col" >
                                <div style="width:200.33px;">' . $each->tracking_number . '</div>
                                <div style="width:200.33px;">' . $each->name . '</div>' . $all_attn . '
                                <div style="width:200.33px;">' . nl2br($address) . '</div>
                                <div>' . $each_city_state_zip . '</div>
                                                                             
                                    </td>';
                                
                                    if ($i == 3) {
                                        $allrecipientshtml = $allrecipientshtml . '</tr>
                                            </table>
                                            </td>
                                            </tr>';
                                        $i = 0;
                                    }
                                    $i++;
                                } 

                                
                            } else {
                                if ($i == 1) {
                                    $allrecipientshtml = $allrecipientshtml . '<tr>
                                        <td colspan = "3">
                                        <table>
                                    <tr>';
                                }
                                $each->attn = str_lreplace($each->attn);
                                $all_attn = strtoupper($each->attn) != "" ? '<div style="width:200.33px;"> ' . strtoupper($each->attn) . '</div>' : "";


                              //  $address = $each->address;
                              $address = (!empty($each->address)) ? $each->address : $each->ad;
                            
                                $address .= ((!empty($each->address2)) ? '<br />' : '').$each->address2;
                                $address .= ((!empty($each->address3)) ? '<br />' : '').$each->address3;

                                $allrecipientshtml = $allrecipientshtml . '
                        <td style="width: 30%;padding: 1px 10px;text-align:left;font-size: 10px; line-height: 10px;" scope="col" >
                            <div style="width:200.33px;">' . $each->tracking_number . '</div>
                            <div style="width:200.33px;">' . $each->name . '</div>' . $all_attn . '
                            <div style="width:200.33px;">' . nl2br($address) . '</div>
                            <div>' . $each_city_state_zip . '</div>
                                                                         
                                    </td>';

                                if ($i == 3) {
                                    $allrecipientshtml = $allrecipientshtml . '</tr>
                                        </table>
                                        </td>
                                    </tr>';
                                    $i = 0;
                                }
                                $i++;
                            }
                        }
                    }
                }
            }
            if ($i == 3) {
                $allrecipientshtml .= '</tr>
                                       </table> 
                                       </td>
                                  </tr>';
            }
            
            $allrecipientfinalcontent = $allrecipientfinalcontent . $allrecipientshtml;
            $last_html = "</table>";
            $allrecipientfinalcontent = $last_html . $allrecipientfinalcontent;

            if (strpos($content, '{{recipients_html}}') !== false) {
                $final_content = str_replace('{{recipients_html}}', $allrecipientfinalcontent, $content);
            } else {
                $final_content = $content . $allrecipientfinalcontent;
            }
            //$final_content = $content . $allrecipientfinalcontent;
            //$content = $content . $allrecipientfinalcontent . '</table><table style="width: 680px;font-size: 11px;margin: 0 auto;table-layout: fixed;page-break-after:always;page-break-before:always;height:750px;position:relative;">';
        }

        /**
         * Below code is for to display second page if it is label or firm mail
         */
        if ($final_content != '') {
            $content_pdf = $final_content;
        } else {
            $content_pdf = $content;
        }
        $surety_bond = '';
        if (config('constants.MASTER_NOTICE')['BCOL']['ID'] == $notice_type->master_notice_id) {
            if (strpos($content_pdf, '{{surety_bond}}')) {
                $new_content_pdf = '';
                if (isset($recipients) && count($recipients) > 0) { //dd($recipients_is_bond_of_claim);
                    $o=1;
                    foreach ($recipients as $k => $each_recipient) {
                        if ($each_recipient->category_id == 9) {
                            $bond_of_claim_firm_mailing = $each_recipient->tracking_number;

                            $bond_of_claim_name = $each_recipient->name;
                            $each_recipient->attn = str_lreplace($each_recipient->attn);
                            $bond_of_claim_attn = strtoupper($each_recipient->attn) != "" ? " " . strtoupper($each_recipient->attn) . "<br/>" : "";
                            // $bond_of_claim_attn = $each_recipient->attn != "" ? " " . $each_recipient->attn . "<br/>" : "";
                            //$bond_of_claim_address = $each_recipient->address;
                            $bond_of_claim_address = (!empty($each_recipient->address)) ? $each_recipient->address : $each_recipient->ad;
                            
                            $state = "";
                            if (isset($each_recipient->state_id) && $each_recipient->state_id != null) {
                                $state_name = \App\Models\State::find($each_recipient->state_id);
                                $state = $state_name->name;
                            }
                            $city = "";
                            if (isset($each_recipient->city_id) && $each_recipient->city_id != null) {
                                $city_name = \App\Models\City::find($each_recipient->city_id);
                                $city = $city_name->name;
                            }
                            if ($each_recipient->city_id != NULL || $each_recipient->state_id != NULL) {
                                $bond_of_claim_city_state_zip = $city . ' ' . $state . ' ' . $each_recipient->zip;
                            } else {
                                $bond_of_claim_city_state_zip = '';
                            }

                            if ($o == 1) {
                                $surety_bond .= '<tr>';
                            }

                            $surety_bond .= '<td style="padding: 0 2px;word-wrap:break-word; word-break:break-all;font-size:10px; line-height: 10px;">' . $bond_of_claim_firm_mailing . '<br/>
                                                                            <p style="width: 200px; margin-bottom: 0;margin-top: 0;font-size:10px; line-height: 10px;">' . $bond_of_claim_name . '</p>
                                                                                ' . $bond_of_claim_attn . $bond_of_claim_address . '<br/>
                                                                                    ' . $bond_of_claim_city_state_zip . '<br/></td>';
                            if ($o == 3) {
                                $surety_bond .= '</tr>';
                                $o = 0;
                            }
                            $o++;
                        }
                    }

                    if ($o == 3) {
                        $surety_bond = $surety_bond . '</tr>';
                    }
                    if (!empty($surety_bond)) {
                        $surety_bond = '<table><tbody><tr>
                  <td style="vertical-align: top;padding: 0 2px;font-size:10px; line-height: 10px;">TO:</td><td><table>' . $surety_bond . '</table></td></tr></tbody></table>';
                    }
                    $content =  str_replace('{{surety_bond}}', $surety_bond, $content_pdf);
                    $new_content_pdf = $new_content_pdf . $content;
                } else {
                    $new_content_pdf = $content_pdf;
                }
                if ($new_content_pdf == '') {
                    $new_content_pdf = $content_pdf;
                }
                $content_pdf = $new_content_pdf;
            }
        }
        $watermark_Notices = [config('constants.MASTER_NOTICE')['NTO']['ID'], config('constants.MASTER_NOTICE')['ITL']['ID'], config('constants.MASTER_NOTICE')['NPN']['ID'], config('constants.MASTER_NOTICE')['COL']['ID'], config('constants.MASTER_NOTICE')['SOL']['ID'], config('constants.MASTER_NOTICE')['BCOL']['ID'], config('constants.MASTER_NOTICE')['SBC']['ID'], config('constants.MASTER_NOTICE')['WRL']['ID']];
        $CYO_watermark_Notices = [config('constants.MASTER_NOTICE')['NTO']['ID'], config('constants.MASTER_NOTICE')['ITL']['ID'], config('constants.MASTER_NOTICE')['NPN']['ID'], config('constants.MASTER_NOTICE')['COL']['ID'], config('constants.MASTER_NOTICE')['SOL']['ID'], config('constants.MASTER_NOTICE')['WRL']['ID']];

        if (Auth::user()->hasRole('customer')) {
            if (isset($request->type_work_order) && $request->type_work_order == 'cyo') {;
                if (in_array($notice_type->master_notice_id, $CYO_watermark_Notices)) {
                    $content_pdf = '<html>
              <head><style>' . config('constants.PDF_WATERMARK_CSS') . '</style>
              </head>
              <body>
                  ' . config('constants.PDF_WATERMARK_HTML') . '<main>' . $content_pdf . ' </main> </body>
              </html>';
                }
            } else {
                if (in_array($notice_type->master_notice_id, $watermark_Notices)) {
                    $content_pdf = '<html>
              <head><style>' . config('constants.PDF_WATERMARK_CSS') . '</style>
              </head>
              <body>
                  ' . config('constants.PDF_WATERMARK_HTML') . '<main>' . $content_pdf . ' </main> </body>
              </html>';
                }
            }
        }

        $pdf = PDF::loadHTML($content_pdf); //setOptions(['debugLayout'=> true])->

        $not_mailed_out_master_ids = [config('constants.MASTER_NOTICE')['SBC']['ID'], config('constants.MASTER_NOTICE')['WRL']['ID']];



        if ($work_order->file_name == NULL) {
            $file_name = $work_order_id . '_' . uniqid() . '.pdf';
        } else {
            $file_name = $work_order->file_name;
        }
        if (in_array($notice_type->master_notice_id, $not_mailed_out_master_ids)) {
            $work_order->file_name = $file_name;
            $work_order->status = 6;
            $work_order->completed_at = date("Y-m-d H:i:s");
            $work_order->save();
        }
        //
        //        if (isset($request->type_work_order) && $request->type_work_order == 'cyo') {
        //            if (!file_exists(public_path() . '/pdf/cyo_work_order_document')) {
        //                mkdir(public_path() . '/pdf/cyo_work_order_document', 0777, true);
        //            }
        //            if (!file_exists(public_path() . '/pdf/cyo_work_order_document/' . $file_name)) {
        //                $pdf->save(public_path() . '/pdf/cyo_work_order_document/' . $file_name);
        //            } else {
        //                unlink(public_path() . '/pdf/cyo_work_order_document/' . $file_name);
        //                $pdf->save(public_path() . '/pdf/cyo_work_order_document/' . $file_name);
        //            }
        //            //update file name
        //            $work_order->file_name = $file_name;
        //            $work_order->status = 5;
        //            $work_order->save();
        //
        //
        //            $mypdf = asset('/') . 'pdf/cyo_work_order_document/' . $file_name;
        //            echo "<script>window.open('$mypdf', '_blank');</script>";
        //        } else {
        //            if (!file_exists(public_path() . '/pdf/work_order_document')) {
        //                mkdir(public_path() . '/pdf/work_order_document', 0777, true);
        //            }
        //
        //            if (!file_exists(public_path() . '/pdf/work_order_document/' . $file_name)) {
        //
        //                $pdf->save(public_path() . '/pdf/work_order_document/' . $file_name);
        //            } else {
        //                unlink(public_path() . '/pdf/work_order_document/' . $file_name);
        //                $pdf->save(public_path() . '/pdf/work_order_document/' . $file_name);
        //            }
        //            //update file name
        //            $work_order->file_name = $file_name;
        //            $work_order->status = 5;
        //            $work_order->save();
        //
        //            $mypdf = asset('/') . 'pdf/work_order_document/' . $file_name;
        //
        //            echo "<script>window.open('$mypdf', '_blank');</script>";
        //        }



        return $pdf->stream();
    }

    /**
     * print work order pdf
     */
    public function printWO($work_order_id)
    {

        $file_name = \App\Models\WorkOrder::find($work_order_id);
        if (file_exists(public_path() . '/pdf/work_order_document/' . $file_name->file_name)) {

            $mypdf = asset('/') . 'pdf/work_order_document/' . $file_name->file_name;
            echo "<script>window.open('$mypdf', '_blank');</script>";
        }

        $customer_id = Auth::User()->customer->id;
        $data['categories'] = \App\Models\Category::select()->orderBy('name', 'ASC')->get();
        $data['project_roles'] = \App\Models\ProjectRoles::select()->orderBy('name', 'ASC')->get();
        $data['account_managers'] = \App\Models\User::select('id', 'name')->join('role_users', 'role_users.user_id', 'users.id')->where('role_users.role_id', 3)->get();
        // dd($data['account_managers']);

        if (Auth::user()->hasRole('account-manager')) {
            $data['customers'] = \App\Models\Customer::where('account_manager_id', '=', Auth::user()->id)->orderBy('company_name', 'ASC')->get();
            $data['customers_order'] = \App\Models\Customer::where('account_manager_id', '=', Auth::user()->id)->join('users', 'users.id', '=', 'customers.user_id')->orderBy('name', 'ASC')->get();
        } else {
            $data['customers'] = \App\Models\Customer::orderBy('company_name', 'ASC')->get();
            $data['customers_order'] = \App\Models\Customer::orderBy('name', 'ASC')->join('users', 'users.id', '=', 'customers.user_id')->get();
        }
        $data['customers'] = \App\Models\Customer::paginate();
        $data['WorkOrders'] = \App\Models\WorkOrder::select()->where('customer_id', $customer_id)->paginate(10);
        $data['states'] = \App\Models\State::select()->orderBy('name')->get();
        return view('account_manager.research.list', $data);
    }

    public function getVerifiedAddress(Request $request)
    {
        try {
            //Verify recipient address using stamps.com API
            $wsdl           = config('constants.STAMPS.WSDL');
            $integrationID  = config('constants.STAMPS.INTEGRATION_ID');
            $username       = config('constants.STAMPS.USERNAME');
            $password       = config('constants.STAMPS.PASSWORD');

            $stamps = new \App\Helpers\StampsHelper($wsdl, $integrationID, $username, $password);
            //dd($stamps);
            if (is_numeric($request->city)) {
                $city = \App\Models\City::where(['id' => $request->city])->first(['name'])->toArray();
            } else {
                $city['name'] = $request->city;
            }

            $state = \App\Models\State::where(['id' => $request->state])->first(['name'])->toArray();

            $address = [
                'FullName' => $request->name,
                'EmailAddress' => $request->recipient_email,
                'PhoneNumber' => $request->recipient_contact,
                'Department' => $request->recipient_department,
                'Company' => $request->attn,
                'Address1' => $request->address,
                'Address2' => $request->address2,
                'Address3' => $request->address3,
                'City' => $city['name'],
                'State' => $state['name'],
                'ZIPCode' => $request->zip
            ];

            $clean_address = $tempCleansAddress = $stamps->CleanseAddress($address);
            //dd($clean_address);
            //log response for debugging
            unset($tempCleansAddress['result']->Rates);
            unset($tempCleansAddress['result']->StatusCodes);
            \Log::info("\n----------\nStamp CleansAddress Request\n-----------\n" .
                print_r($address, TRUE) .
                "----------\nReponse\n-----------\n" .
                print_r($tempCleansAddress, TRUE));
            //log response for debugging

            $SelectedAddress = implode('**', [$request->address, $city['name'], $state['name'], $request->zip, $request->name ?? '', $request->company ?? '', $request->recipient_email ?? '',  $request->recipient_contact ?? '']);

            //$entered_html = '<div class="row">';
            $entered_html = '<div class="col-md-6">
                                <div class="radio-group"><div class="readonly full-width">
                                <ul class="button-holder full-width list-inline">
                                 <li><input type="radio" id="rad_select_addr" name="select_addr" required="true" class="regular-radio" value = "' . $SelectedAddress . '" checked="checked" ><label for="rad_select_addr">Entered Address</label></li>
                                 </ul></div><p style="margin:10px 0 10px 50px">';

            $entered_html .= 'Address : ' . $request->address . '<br/>';
            $entered_html .= 'City : ' . $city['name'] . '<br/>';
            $entered_html .= 'State : ' . $state['name'] . '<br/>';
            $entered_html .= 'Zip Code : ' . $request->zip . '<br/></p></div></div>';

            if ($clean_address['status'] == 'success') {
                $clean_address = $clean_address['result'];

                /* if ($clean_address->Address->OverrideHash != '') {
                    return json_encode(['response' => 'success', 'data' => json_encode($clean_address)]);
                } else {
                    return json_encode(['response' => 'error', 'message' => $clean_address->AddressCleansingResult, 'overridehashpresent' => false]);
                } */

                if (isset($clean_address->CandidateAddresses->Address) && $clean_address->VerificationLevel == 'Partial') {
                    $html = $entered_html;
                    foreach ($clean_address->CandidateAddresses->Address as $k => $addr) {
                        if($request->attn){
                           $addr->Company=strip_tags($addr->Company);
                        }
                        $addr_val = implode('**', [$addr->Address1, $addr->City, $addr->State, $addr->ZIPCode, $addr->FullName ?? '', $addr->Company ?? '', $addr->EmailAddress ?? '', $addr->PhoneNumber ?? '']);
                        $html .= '<div class="col-md-6">
                            <div class="radio-group"><div class="readonly full-width">
                            <ul class="button-holder full-width list-inline">
                             <li><input data-usps-addr="' . json_encode($addr) . '" type = "radio" name = "select_addr" required = "true" value = "' . $addr_val . '" class="regular-radio" id="select_addr' . $k . '"><label for="select_addr' . $k . '">USPS Address</label></li>
                             </ul></div><p style="margin:10px 0 10px 50px">';
                        $html .= 'Address1 : ' . ucwords(strtolower($addr->Address1)) . '<br/>';
                        $html .= 'City : ' . ucwords(strtolower($addr->City)) . '<br/>';
                        $html .= 'State : ' . $addr->State . '<br/>';
                        $html .= 'Zip Code : ' . $addr->ZIPCode . '<br/></p></div></div>';
                        // alternate_addresses = alternate_addresses.concat('Zip Code Add On: '.$addr->ZIPCodeAddOn+'<br/>');
                        // alternate_addresses = alternate_addresses.concat('OverrideHash: '.$addr->OverrideHash+'<br/>');
                    }

                    return json_encode(['response' => 'error', 'message' => $clean_address->AddressCleansingResult, 'alternate_addresses' => $html]);
                } else if (
                    isset($clean_address->Address) && !empty($clean_address->Address)
                    && (strtolower($clean_address->Address->Address1) != strtolower($request->address)
                        || strtolower($clean_address->Address->City) != strtolower($city['name']))
                ) {
                    $addr = $clean_address->Address;
                    if($request->attn){
                      $addr->Company=strip_tags($addr->Company);
                    }
                    $html = $entered_html;
                    $addr_val = implode('**', [$addr->Address1, $addr->City, $addr->State, $addr->ZIPCode, $addr->FullName ?? '', $addr->Company ?? '', $addr->EmailAddress ?? '', $addr->PhoneNumber ?? '']);
                    $html .= '<div class="col-md-6">
                            <div class="radio-group"><div class="readonly full-width">
                            <ul class="button-holder full-width list-inline">
                             <li><input data-usps-addr="' . json_encode($addr) . '" type = "radio" name = "select_addr" required = "true" value = "' . $addr_val . '" class="regular-radio" id="select_addr"><label for="select_addr">USPS Address</label></li>
                             </ul></div><p style="margin:10px 0 10px 50px">';
                    $html .= 'Address : ' . ucwords(strtolower($addr->Address1)) . '<br/>';
                    $html .= 'City : ' . ucwords(strtolower($addr->City)) . '<br/>';
                    $html .= 'State : ' . $addr->State . '<br/>';
                    $html .= 'Zip Code : ' . $addr->ZIPCode . '<br/></p></div></div>';
                    return json_encode(['response' => 'error', 'message' => $clean_address->AddressCleansingResult, 'alternate_addresses' => $html]);
                } else if ($clean_address->AddressMatch === true && $clean_address->CityStateZipOK === true) {
                    return json_encode(['response' => 'success', 'data' => json_encode($clean_address)]);
                } else {
                    if ($clean_address->VerificationLevel == 'Partial' && $clean_address->Address->OverrideHash != '') {
                        return json_encode(['response' => 'success', 'data' => json_encode($clean_address)]);
                    } else {
                        return json_encode(['response' => 'error', 'message' => $clean_address->AddressCleansingResult, 'overridehashpresent' => false]);
                    }
                }
            } else if (isset($clean_address['status']) && isset($clean_address['message']) && $clean_address['status'] == 'error') {
                return json_encode(['response' => 'error', 'message' => $clean_address['message'], 'overridehashpresent' => false]);
            } else {
                return json_encode(['response' => 'error', 'message' => 'Oops! something went wrong. Could not verify address, please try again after sometime.', 'overridehashpresent' => false]);
            }
        } catch (\Exception $e) {
            return json_encode(['response' => 'error', 'message' => $e->getMessage() . ' on file ' . $e->getFile() . ' at line ' . $e->getLine(), 'overridehashpresent' => false]);
        }
    }

    function CheckFields(Request $request)
    {

        $Fields = \App\Models\WorkOrder::find($request->id);
        $Fields_Data = \App\Models\WorkOrderFields::select()->where('workorder_id', $request->id)->get()->toArray();
        $notice_Field_ids = \App\Models\NoticeField::select()->where('notice_id', $Fields->notice_id)->where('is_required', 1)->get()->toArray();
        $count = 0;
        foreach ($notice_Field_ids as $notice_field_id) {
            // return $notice_field_id;
            $result = \App\Models\WorkOrderFields::select('value')->where('workorder_id', $request->id)->where('notice_field_id', $notice_field_id['id'])->get()->toArray();
            if (isset($result[0]) && $result[0]['value'] == '') {
                $count = $count + 1;
            }
        }
        if ($count != 0) {
            return 0;
        } else
            return 1;
    }

    function updateStatus($id)
    {
        /* $result = \App\Models\Cyo_WorkOrders::find($id);

          $result->status = 0;
          $result->save(); */
        Session::flash('success', 'Your work order # ' . $id . ' has been saved Successfully.<a href="' . url('customer/work-order/view/' . $id) . '">Click here to print.</a>');
        return redirect('account-manager/research');
    }

    function getPackageTypes(Request $request)
    {
        $packages = \App\Models\StampsAddOns::join('stamps_service_package_addon', 'stamps_service_package_addon.add_on', '=', 'stamps_add_ons.id')
            ->join('stamps_service_type', 'stamps_service_type.id', '=', 'stamps_service_package_addon.service_type')
            ->join('stamps_packages', 'stamps_packages.id', '=', 'stamps_service_package_addon.package')
            ->where(['stamps_service_type.abbr' => $request->service_type, 'stamps_packages.status' => '1'])
            ->get([\DB::raw('DISTINCT(stamps_packages.name)')])->toArray();

        $html = '';
        if (!empty($packages)) {
            $cnt = $request->counter;
            $html .= '<label for = "">Select Package</label><select required = "required" class = "selectpicker" id = "package_type_' . $cnt . '" name = "package_type' . $cnt . '" onChange = "getAddOns(this);"><option value = "" disabled selected>Choose your package</option>';
            foreach ($packages as $val) {
                $html .= '<option value = "' . $val['name'] . '">' . $val['name'] . '</option>';
            }
            $html .= '</select>';
            $response = 'success';
        } else {
            $response = 'error';
            $html .= '<span>No Records Found</span>';
        }
        return $html;
    }

    function getAddons(Request $request)
    {
        $cnt = $request->counter;
        $service_package_add_ons = \App\Models\StampsAddOns::join('stamps_service_package_addon', 'stamps_service_package_addon.add_on', '=', 'stamps_add_ons.id')
            ->join('stamps_service_type', 'stamps_service_type.id', '=', 'stamps_service_package_addon.service_type')
            ->join('stamps_packages', 'stamps_packages.id', '=', 'stamps_service_package_addon.package')
            ->where(['stamps_packages.name' => $request->package_type, 'stamps_service_type.abbr' => $request->service_type, 'stamps_add_ons.status' => '1'])
            ->get(['stamps_add_ons.status', 'stamps_add_ons.abbr', 'stamps_add_ons.description', 'stamps_add_ons.id'])->toArray();

        $html = '<label>Add Ons <i class = "fa fa-info-circle" title = "Please note that clashing add ons will be automatically removed"></i></label><br/>';
        if (!empty($service_package_add_ons)) {
            $cnt1 = 1;
            foreach ($service_package_add_ons as $val) {
                $prohibited = \App\Models\StampsAddOnsProhibited::join('stamps_add_ons', 'stamps_add_ons_prohibited.prohibited', '=', 'stamps_add_ons.id')
                    ->where(['stamps_add_ons_prohibited.add_on' => $val['id']])
                    ->pluck('prohibited')->toArray();

                $required = \App\Models\StampsAddOnsRequired::join('stamps_add_ons', 'stamps_add_ons_required.required', '=', 'stamps_add_ons.id')
                    ->where(['stamps_add_ons_required.add_on' => $val['id']])
                    ->pluck('required')->toArray();

                $params = [$cnt, $val['id']];
                $input_name = 'add_ons_' . $cnt . '[]';
                $html .= "<input class='add_ons_input' name='" . $input_name . "' id='add_on_id_" . $cnt . "_" . $cnt1++ . "' onChange='getProhibited(this)'  data-required='" . json_encode($required) . "' data-prohibited='" . json_encode($prohibited) . "' type='checkbox' value='" . $val["id"] . "'> " . $val["description"] . '<br/>';
            }
            $response = 'success';
        } else {
            $response = 'error';
            $html .= '<span>No Records Found</span>';
        }
        // return json_encode(['result'=>$html,'response'=>$response]);
        return $html;
    }

    function getProhibitedRequiredAddOns(Request $request)
    {
        $prohibited = \App\Models\StampsAddOnsProhibited::join('stamps_add_ons', 'stamps_add_ons_prohibited.prohibited', '=', 'stamps_add_ons.id')
            ->where(['stamps_add_ons_prohibited.add_on' => $request->add_on, 'stamps_add_ons.status' => 1])
            ->pluck('prohibited')->toArray();

        $required = \App\Models\StampsAddOnsRequired::join('stamps_add_ons', 'stamps_add_ons_required.required', '=', 'stamps_add_ons.id')
            ->where(['stamps_add_ons_required.add_on' => $request->add_on, 'stamps_add_ons.status' => 1])
            ->pluck('required')->toArray();

        return json_encode(['prohibited' => $prohibited, 'required' => $required]);
    }

    function proceedtoMailing($work_order_id)
    {
        //Change staus for mailing 
        $mailing_status = WorkOrder::find($work_order_id);
        $mailing_status->status = 5;
        $mailing_status->save();

        return redirect::to('account-manager/research')->with('success', 'Work order has been successfully proceed to mailing.');
    }

    function updateRecordingPendingSignStatus($status, $work_order_id)
    {
        try {
            $update_status = WorkOrder::find($work_order_id);
            $update_status->status = $status;
            $update_status->save();
            return json_encode(['status' => 'success', 'message' => 'Status successfully updated']);
        } catch (Exception $e) {
            return json_encode(['status' => 'error', 'message' => 'Status not updated']);
        }
    }
    /** Filter with ajax **/
    function filterSelect(Request $request)
    {
        if ($request->ajax()) {
            $page_type = $request->page_type;

            $request->session()->put($page_type, $request->all());
            //dd($request->session()->all());
        }
    }
    /** Reset Filter with ajax **/
    function resetFilter(Request $request)
    {
        if ($request->ajax()) {
            //dd('j');
            $page_type = $request->page_type;
            $request->session()->forget($page_type);
        }
    }
    function createYourOwn()
    {
        return view('account_manager.create_your_own.dashboard');
    }
    public function WorkOrderHitory()
    {

        $customer_id = Auth::User()->customer->id;
        $accountManagerId = Auth::user()->id;
        $data['account_managers'] = \App\Models\User::select('id', 'name')->join('role_users', 'role_users.user_id', 'users.id')->where('role_users.role_id', 3)->get();
        $data['categories'] = \App\Models\Category::select()->orderBy('name', 'ASC')->get();
        $data['customers'] = \App\Models\Customer::paginate();
        $data['states'] = \App\Models\State::select()->orderBy('name')->get();
        $data['contracted_by'] = \App\Models\Contact::select()->whereIn('customer_id', $accntMgrAssignCustomerIds)->orderBy('company_name', 'ASC')->get();
        return view('account_manager.create_your_own.work_order_history', $data);
    }

    function notesEmail(Request $request)
    {
        //$data = [];
        if ($request['visibility'] == 0) {
            $data[] = get_user_notice_email($request['customer_id']);
        } else {
            $data[] = get_user_private_notice_email($request['customer_id']);
        }

        return $data;
    }




    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function editWo($id, $notice_id = 0, $is_rescind = 0)
    {
        $startTime = microtime(true);

        $data['is_rescind'] = $is_rescind;
        $notice = \App\Models\Notice::select('name', 'id', 'type', 'is_claim_of_lien', 'master_notice_id')->where('id', $notice_id)->get()->toArray();
        $data['rush_hour'] = \App\Models\WorkOrder::where(['id' => $id])->pluck('rush_hour_charges');
        $data['notice'] = $notice[0];
        $data['label_generated_count'] = 0;
        $data['countries'] = \App\Models\Country::select()->get();
        $data['states'] = \App\Models\State::select()->orderBy('name', 'asc')->get();
        $data['categories'] = \App\Models\Category::select()->orderBy('name', 'ASC')->get();
        $data['project_roles'] = \App\Models\ProjectRoles::select()->orderBy('name', 'ASC')->get();

        $data['recipients'] = \App\Models\Recipient::select('recipients.id', 'work_order_id', 'category_id', 'name', 'contact', 'mobile', 'recipients.address', 'city_id', 'state_id', 'zip', 'fax', 'recipients.created_at', 'recipients.updated_at', \DB::raw('usps_address.address as usps_address'), \DB::raw('usps_address.city as usps_city'), \DB::raw('usps_address.state as usps_state'), 'zipcode', 'zipcode_add_on', 'dpb', 'check_digit', 'comment')->leftJoin('usps_address', 'usps_address.recipient_id', '=', 'recipients.id')->where('work_order_id', $id)->get();
        $data['rush_hour'] = \App\Models\WorkOrder::select('rush_hour_charges', 'rush_hour_amount')->where('id', $id)->get();
        $verified_address_count = \App\Models\Recipient::select(\DB::raw('COUNT(usps_address.recipient_id) as verified_address_count'))->leftJoin('usps_address', 'usps_address.recipient_id', '=', 'recipients.id')->where('work_order_id', $id)->get();
        $data['verified_address_count'] = ($verified_address_count) ? $verified_address_count[0]['verified_address_count'] : 0;

        $data['projectTypes'] = \App\Models\ProjectType::select()->get();
        $data['count'] = \App\Models\Recipient::select(DB::raw('COUNT(*) AS total'), 'categories.name')->groupBy('recipients.category_id')->join('categories', 'categories.id', '=', 'recipients.category_id')->where('work_order_id', $id)->get();
        $data['id'] = $id;
        $data['notice_id'] = $notice_id;
        $result = \App\Models\WorkOrder::select('project_address_count', 'state_id', 'customer_id')->join('notice_fields', 'notice_fields.notice_id', '=', 'work_orders.notice_id')->where('work_orders.id', $id)->limit(1)->get()->toArray();
        $data['project_address_count'] = $result[0]['project_address_count'];
        $data['customer_id'] = $result[0]['customer_id'];
        $data['customer_details'] = \App\Models\Customer::find($data['customer_id']);
        $data['officers_directors'] = \App\Models\CustomerAgent::where('customer_id', '=', $data['customer_id'])
            ->get();

        $data['names'] = \App\Models\Contact::select('contacts.id', 'contacts.company_name')
            // ->where('contact_details.type', '=', 0)
            ->where('contacts.customer_id', '=', $data['customer_id'])
            // ->Join('contact_details', 'contact_details.contact_id', '=', 'contacts.id')
            ->orderBy('contacts.company_name', 'ASC')
            ->get();

        $data['attachment'] = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $id)
            ->get();

        $data['service_types'] = \App\Models\StampsServices::where(['status' => '1'])->get(['description', 'abbr', 'id'])->toArray();
        $data['package_types'] = \App\Models\StampsAddOns::join('stamps_service_package_addon', 'stamps_service_package_addon.add_on', '=', 'stamps_add_ons.id')
            ->join('stamps_service_type', 'stamps_service_type.id', '=', 'stamps_service_package_addon.service_type')
            ->join('stamps_packages', 'stamps_packages.id', '=', 'stamps_service_package_addon.package')
            ->where(['stamps_packages.status' => '1'])
            ->get([\DB::raw('DISTINCT(stamps_packages.name)'), 'stamps_packages.id'])->toArray();

        if ($data['recipients']) {
            for ($cnt = 0; $cnt < count($data['recipients']); $cnt++) {
                $labels = \App\Models\StampsLabel::leftJoin('stamps_service_type', 'stamps_service_type.id', 'stamps_label.service_type')
                    ->leftJoin('stamps_packages', 'stamps_packages.id', 'stamps_label.package_type')
                    ->where(['recipient_id' => $data['recipients'][$cnt]['id']])
                    ->first(['stamps_label.*']); // dd($labels);
                if (!empty($labels) && $labels['generated_label'] == 'yes') {
                    $data['label_generated_count']++;
                    $data['recipients'][$cnt]['label_generated'] = 'yes';
                } else {
                    $data['recipients'][$cnt]['label_generated'] = 'no';
                }
                $data['recipients'][$cnt]['recipient_shipping_labels'] = $labels;
                $data['recipients'][$cnt]['add_ons'] = \App\Models\StampsAddOns::join('stamps_service_package_addon', 'stamps_service_package_addon.add_on', '=', 'stamps_add_ons.id')
                    ->join('stamps_service_type', 'stamps_service_type.id', '=', 'stamps_service_package_addon.service_type')
                    ->join('stamps_packages', 'stamps_packages.id', '=', 'stamps_service_package_addon.package')
                    ->where(['stamps_add_ons.status' => '1'])
                    ->where('stamps_service_type.id', '=', 1)
                    ->where('stamps_packages.id', '=', 2)
                    // ->where(function($query) use ($labels) {  
                    //     if(isset($labels) && !empty($labels))
                    //     {
                    //         $query->where('stamps_service_type.id','=',$labels['service_type']);
                    //         $query->where('stamps_packages.id','=',$labels['package_type']);
                    //     }	
                    // })
                    ->get([\DB::raw('DISTINCT(stamps_add_ons.id)'), 'stamps_add_ons.description'])->toArray();

                if (!empty($data['recipients'][$cnt]['add_ons'])) {
                    $cnt1 = 1;
                    $temp_add_ons = $data['recipients'][$cnt]['add_ons'];
                    for ($counter = 0; $counter < count($temp_add_ons); $counter++) {
                        $prohibited = \App\Models\StampsAddOnsProhibited::join('stamps_add_ons', 'stamps_add_ons_prohibited.prohibited', '=', 'stamps_add_ons.id')
                            ->where(['stamps_add_ons_prohibited.add_on' => $temp_add_ons[$counter]['id']])
                            ->pluck('prohibited')->toArray();

                        $required = \App\Models\StampsAddOnsRequired::join('stamps_add_ons', 'stamps_add_ons_required.required', '=', 'stamps_add_ons.id')
                            ->where(['stamps_add_ons_required.add_on' => $data['recipients'][$cnt]['add_ons'][$counter]['id']])
                            ->pluck('required')->toArray();
                        $temp_add_ons[$counter]['prohibited'] = json_encode($prohibited);
                        $temp_add_ons[$counter]['required'] = json_encode($required);
                    }
                    unset($data['recipients'][$cnt]['add_ons']);
                    $data['recipients'][$cnt]['add_ons'] = $temp_add_ons;
                }
            }
        }
        $data['parent_wo_list'] = \App\Models\WorkOrder::select('id')->where('id', '!=', $id)
            ->select('id', DB::raw('CONCAT("#",id) AS `parent_id`'))->pluck('parent_id', 'id')->toArray();
        //dd($data['parent_wo_list']);

        $work_order_status = \App\Models\WorkOrder::find($id);
        $data['is_rescind'] = $work_order_status->is_rescind;
        if ($work_order_status->status == '0') {
            $data['status'] = 'draft';
        } else {
            $data['status'] = 'request';
        }
        $data['work_order_status'] = $work_order_status->status;
        $data['cities'] = [];

        /* Notes and correction * */
        $admin_email = \App\Models\SecondaryDocument::getAdminEmail();

        $data['note_emails'] = get_user_notice_email($work_order_status->customer_id);
        $data['notes'] = \App\Models\WorkOrderNotes::leftjoin('users', 'users.id', '=', 'work_order_notes.user_id')->where('customer_id', '=', $work_order_status->customer_id)
            ->where('work_order_notes.work_order_id', '=', $id)
            ->orderBy('work_order_notes.id', 'DESC')
            ->get();

        $data['corrections'] = \App\Models\WorkOrderCorrections::leftjoin('users', 'users.id', '=', 'work_order_corrections.user_id')->where('customer_id', '=', $work_order_status->customer_id)
            ->where('work_order_corrections.work_order_id', '=', $id)
            ->orderBy('work_order_corrections.id', 'DESC')
            ->get();


        /* Notes and correction * */
        if ($notice[0]['type'] == 2) {
            $data['notice_fields_section1'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.workorder_id', $id)->where('work_order_fields.notice_id', $notice_id)->where('notice_fields.section', 1)->orderBy('sort_order', 'asc')->get();

            $data['notice_fields_section2'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.notice_id', $notice_id)->where('work_order_fields.workorder_id', $id)->where('notice_fields.section', 2)->orderBy('sort_order', 'asc')->get();
            $data['tab'] = 'project';
            //To display parent work order

            foreach ($data['notice_fields_section1'] as $key => $value) {
                $key = explode("_", $key);

                if ($value) {

                    if ($value->name == 'parent_work_order') {
                        $parent_work_order = $value->value;
                        $data['parent_work_order'] = $parent_work_order;
                    }
                    if ($value->name == 'project_address') {
                        $project_address = $value->value;
                        $project_address = explode('**', $project_address);
                        $data['project_address_count'] = count($project_address);
                    }
                }
            }
            //dd((microtime(true)-$startTime));
            return view('account_manager.research.edit_soft_notices', $data);
        } else {
            $data['tab'] = 'project';
            $data['notice_fields'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.notice_id', $notice_id)->where('work_order_fields.workorder_id', $id)->orderBy('sort_order', 'asc')->get();

            //To display parent work order
            foreach ($data['notice_fields'] as $key => $value) {
                $key = explode("_", $key);

                if ($value) {

                    if ($value->name == 'parent_work_order') {
                        $parent_work_order = $value->value;
                        $data['parent_work_order'] = $parent_work_order;
                    }
                }
            }
            return view('account_manager.research.edit', $data);
        }
    }
}
