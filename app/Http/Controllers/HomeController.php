<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Function is used to get cities
     */
    public function getCities(Request $request){
        $cities = \App\Models\City::select('id','name')
                                        ->where('state_id','=',$request->state)
                                        ->get();
        
        return $cities;
    }
    // public function getStates(Request $request){
    //     $options = "";
       
    //     $states = DB::table("states")->select('id','name')->get();
    //     if(isset($states) && (!empty($states))) {
    //         foreach($states as $s) {
    //             $options = $options . "<option value='".$s->id."'>".$s->name."</option>"
    //         }
    //     }
     
    //     return $states;
    // }
     /**
     * Show the customer dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function customerDashboard()
    {
        return view('customer.dashboard');
    }

}
