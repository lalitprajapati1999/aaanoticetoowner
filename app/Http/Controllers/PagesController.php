<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\State;
use App\Models\Notice;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Models\NoticeTemplate;
use Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use App\Mail\Contactus;

class PagesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data['states'] = State::select('id', 'name')->orderBy('name')->get();
        // $data['notices'] = Notice::select('id','name')->where('status',1)->get();
        return view('dashboard_deadline_calculator', $data);
    }

    public function getNotices(Request $request) {
        $notices = NoticeTemplate::leftjoin('notices', 'notices.id', '=', 'notice_templates.notice_id')
                ->select('notices.id', 'notices.name')
                ->where('notice_templates.state_id', '=', $request->state)
                ->where('notices.status', 1)
                ->where('allow_deadline_calculator', 1)
                ->get();

        return $notices;
    }

    public function lien_laws() {
        $data['lien_blogs'] = \App\Models\LienBlog::where('status', '=', 1)
                ->orderBy('id', 'ASC')
                ->get();

        return view('lien_laws', $data);
    }

    public function pricing() {
        $data = array();
        $data['packages'] = \App\Models\Package::where('id', '!=', 1)->get();
        $data['pricings'] = \App\Models\Pricing::join('packages', 'packages.id', '=', 'pricings.package_id')
                ->join('notices', 'notices.id', '=', 'pricings.notice_id')
                ->where(['pricings.status' => 1, 'pricings.notice_id' => 9, 'pricings.package_id' => 1])->orderBy('pricings.id')
                ->get(['notices.name', 'package_id', 'notice_id', 'upper_limit', 'lower_limit', 'pricings.charge', 'pricings.cancelation_charge', 'pricings.description','pricings.additional_address']);
        if(Auth::user()){
        $data['subscription_package'] = \App\Models\Customer_subscription::select('package_id')->where('status','1')->where('start_date', '<=', date('Y-m-d'))->where('end_date', '>=', date('Y-m-d'))->where('customer_id',Auth::user()->customer->id)->get()->toArray();
        $data['subscription'] = [];
        foreach ($data['subscription_package'] as $key => $value) {
            # code...
            array_push($data['subscription'],$value['package_id']);
        }
        } 

        if (!Auth::user()) {
            \Session::flash('error', 'Please login or register for subscription');
        }

        return view('pricing', $data);
    }

    public function calculate_date(Request $request) {
        //Validate Data
        $validation = Validator::make(Input::all(), [
                    'state' => 'required| exists:states,id',
                    'document_type' => 'required | exists:notices,id',
                    'month' => 'required',
                    'day' => 'required',
                    'year' => 'required'
        ]);

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation)->withInput();
        }

        $input = $request->all();
        //dd($input);

        $day = $input['day'];
        $month = $input['month'];
        $year = $input['year'];
        $state_id = $input['state'];
        $document_id = $input['document_type'];

        //Days Required to calculate end date
        $result = NoticeTemplate::where('state_id', '=', $state_id)
                ->where('notice_id', '=', $document_id)
                ->select('no_of_days')
                ->first();

        if ($result) {
            $days = $result->no_of_days;
        }
        //Get State Name
        $state = State::where('id', $state_id)->select('name')->first();
        $data['state'] = $state;

        //Get Document Type
        $document_type = Notice::where('id', $document_id)->select('name')->first();
        $data['document_type'] = $document_type;
        $data['days'] = $days;

        //Start Date
        $data['job_start_date'] = $input['month'] . '/' . $input['day'] . '/' . $input['year'];
        //  dd($data['job_start_date']);
        //Calculate end date
        $start_date = $input['year'] . '-' . $input['month'] . '-' . $input['day'];
        $dayStr = $days == 1 ? 'day' : 'days';
        $end_date = date('m/d/Y', strtotime('+ ' . $days . ' ' . $dayStr, strtotime($start_date)));

        $data['last_day_on_job'] = $end_date;
        $data['due_date'] = $end_date;
        $data['document_id'] = $document_id;
        return view('deadline', $data);
    }

    /**
     * Function is used to get cities
     */
    public function getCities() {
        $cities = \App\Models\City::select('id', 'name')
                ->get();

        return $cities;
    }

    /*
     * Function is used to get states
     */

    public function getStates() {
        $states = \App\Models\State::select('id', 'name')
                ->get();

        return $states;
    }

    /**
     * 
     * @param type $id
     * @return type
     * @Note: checking for the authentication & redirecting to subscription form.
     * @todo If redirected to login set seesion so after login wii redirect to subsc
     */
    public function subscription_redirection($id) {
        \Session::put('subscription_package_selected', $id);
        \Session::save();
        try {
            if ((isset($id)) && (!empty($id))) {
                if (Auth::check()) {
                   /* $data['subscription_package'] = \App\Models\Customer_subscription::select('package_id')->where('status','1')->where('start_date', '<=', date('Y-m-d'))->where('end_date', '>=', date('Y-m-d'))->where('customer_id',Auth::user()->customer->id)->where('package_id',$id)->get();dd($data['subscription_package']);*/   
                       return redirect::to('customer/subscription/' . $id . '/make-payment');   
                } else {
                    return redirect::to('register')->with('error', 'Please login or register for subscription');
                }
            }
        } catch (\Exception $ex) {

            return redirect::to('login')->with('error', 'Please login or register for subscription');
        }
    }

    public function contactus() {
        return view('contactus');
    }

    public function storeContactus(Request $request) {
        //dd($request->all());
        $validation = Validator::make(Input::all(), [
                    'name' => 'required',
                    'email' => 'required|email',
                    'contact' => 'required',
                    'query' => 'required',
                    'g-recaptcha-response' => 'required|captcha'
                        ], [
                    'contact.required' => 'Telephone is required',
                    'query.required' => 'Query is required',
                    'g-recaptcha-response.required' => 'Captcha is required',
                    'g-recaptcha-response.captcha' => 'Captcha Invalid'
        ]);
        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation)->withInput();
        }
        $input = $request->all();

        $contactusObj = new \App\Models\ContactUs();
        $contactusObj->name = $input['name'];
        $contactusObj->email = $input['email'];
        $contactusObj->phone = $input['contact'];
        $contactusObj->query = $input['query'];
        $contactusObj->save();

        $contactusdetails = \App\Models\ContactUs::find($contactusObj->id);
        Mail::to(config('constants.TRANSACTION_EMAILS.SALES_EMAIL'))->send(new Contactus($contactusdetails));

        return redirect::to('/contact-us')->with('success', 'Thanks for contacting us! We will be in touch with you shortly.');
    }

    public function storeDeadlineContactus(Request $request) {
        $validation = Validator::make(Input::all(), [
                    'company_name' => 'required',
                    'name' => 'required',
                    'email' => 'required|email',
                    'phone' => 'required',
                    'g-recaptcha-response' => 'required|captcha'
                        ], [
                    'query.required' => 'Query is required',
                    'g-recaptcha-response.required' => 'Captcha is required',
                    'g-recaptcha-response.captcha' => 'Captcha Invalid'
        ]);

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation)->withInput();
        }
        $input = $request->all();

        $contactusObj = new \App\Models\ContactUs();
        $contactusObj->company_name = $input['company_name'];
        $contactusObj->name = $input['name'];
        $contactusObj->email = $input['email'];
        $contactusObj->phone = $input['phone'];
        $contactusObj->save();

        $contactusdetails = \App\Models\ContactUs::find($contactusObj->id);
        Mail::to($request->email)->send(new \App\Mail\ContactusDeadline($contactusdetails));

        return redirect::to('/deadline-calculator')->with('success', 'Thanks for contacting us! We will be in touch with you shortly.');
    }

}
