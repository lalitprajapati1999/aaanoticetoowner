<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NoticeFieldsController extends Controller
{
    /**
     * Display a listing of the states.
     *
     * @return \Illuminate\Http\Response
     */
    public function states(Request $request, \App\Models\Notice $notice)
    {
        $states = \App\Models\State::select()->where('status',1)->paginate();

        return view('admin.notice_fields.states', compact('states', 'notice'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, \App\Models\Notice $notice,$state_id)

    {   $state = \App\Models\State::select()->where('id',$state_id)->get()->toArray();
        $state_id = $state[0]['id'];
        $state_name = $state[0]['name'];
        $fields = $notice->noticeFields()->orderBy('sort_order', 'asc')->get();
        $notice_type = $notice->type;
        return view('admin.notice_fields.index', compact('notice', 'state_id','state_name', 'fields','notice_type'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, \App\Models\Notice $notice, \App\Models\State $state)
    {//dd($request->all());
        $request->validate([

            'name' => 'required|regex:/^[a-z0-9_]+$/',
            'type' => 'required|in:'.implode(',', array_keys(config('constants.field_types'))),
            'validation' => 'nullable',
            'is_required' => 'in:0,1',
            'sort_order' => 'required',
        ]);
        if($request->input('section')){
            try {
                $noticeField = \App\Models\NoticeField::firstOrNew(['id' => $request->id, 'notice_id' => $notice->id, 'state_id' => $state->id]);
                $noticeField->fill($request->only('sort_order','section','name', 'type', 'validation', 'is_required','status'));
                $noticeField->save();
            }catch (\Exception $e) {
                logger()->error($e->getMessage());
                \Alert::error(trans('backpack::base.error_saving'))->flash();

                return redirect()->back();
            }

        } else {
            
             try {
                $noticeField = \App\Models\NoticeField::firstOrNew(['id' => $request->id, 'notice_id' => $notice->id, 'state_id' => $state->id]);
                $prevFieldName = $noticeField->name;
                $noticeField->fill($request->only('sort_order','name', 'type', 'validation', 'is_required','status'));
                if($noticeField->save() && $prevFieldName != $request->name) {
                    //if save successfully, then update {name} used in notice content 
                    
                    \App\Models\NoticeTemplate::where(['notice_id' => $notice->id,
                    'state_id' => $state->id])
                            ->update(['content' => \DB::raw("REPLACE(content, '{".$prevFieldName."}', '{".$request->name."}')")]);
                }
            }catch (\Exception $e) {
                logger()->error($e->getMessage());
                \Alert::error(trans('backpack::base.error_saving'))->flash();

                return redirect()->back();
            }
        }
       
        // show a success message
        \Alert::success(trans('backpack::crud.update_success'))->flash();

        return redirect()->route('admin.notices.states.fields', ['notice' => $notice->id, 'state' => $state->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function changeOrder(Request $request, \App\Models\Notice $notice, \App\Models\State $state)
    {
        collect(collect(json_decode($request->data))->first())->filter(function($field, $key) {
            return isset($field->id);
        })->values()->transform(function($field, $key) {
            return ['id' => $field->id, 'sort_order' => ($key+1)];
        })->each(function($field) {
            \App\Models\NoticeField::where('id', $field['id'])->update(['sort_order' => $field['sort_order']]);
        });
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(\App\Models\Notice $notice, \App\Models\State $state, \App\Models\NoticeField $noticeField)
    {
        /*$state = \App\Models\State::select()->where('id',$state->id)->get()->toArray();
        $state_id = $state[0]['id'];
        $state_name = $state[0]['name'];*/
        $fields = $notice->noticeFields()->where('state_id', $state->id)->get();
        $state_id = $fields[0]['state_id'];
        $state = \App\Models\State::find($state_id);
        $state_name = $state->name;
        $notice_type = $notice->type;
        return view('admin.notice_fields.index', compact('notice', 'state', 'fields', 'noticeField','state_id','state_name','notice_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $fields = \App\Models\NoticeField::find($request->notice_field_id);
        $fields->delete();
        return redirect()->back()->with('success','Record Deleted Successfully.');
    }
}
