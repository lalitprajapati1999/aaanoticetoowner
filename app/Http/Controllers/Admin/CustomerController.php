<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Auth;
use Alert;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Str;
use App\Models\CustomerAgent;
use App\Models\CustomerRepresentative;
use App\Models\Branch;
use App\Models\Notice;
use App\Models\User;
use App\Models\Customer;
use App\Models\Customer_subscription;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use App\Mail\CustomerApprovalMail;
use App\Mail\CustomerActivationMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\QuickBooksController;

class CustomerController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data = [];

//        $data['customers']=\App\Models\Customer::paginate();
//        if ($data['customers']->isEmpty()) {
//                //Alert::success('No record found')->flash();
//            Session::flash('No record found');
//        } 

        return view('admin.customer.index', $data);
    }

    /*
     * Function is used for listing and filter by employee name
     */

    public function getCustomerFilter(Request $request) {


        $customers = \App\Models\Customer::whereIn('status', ['1', '0']);
        if (Auth::user()->hasRole('account-manager')) {
            $customers->where('account_manager_id', '=', Auth::user()->id);
        }

        if (($request->has('customer_name')) && ($request->get('customer_name') != NULL)) {
            $customers->where('company_name', 'like', '%' . $request->get('customer_name') . "%");
        }
        $customers = $customers->get();
        foreach ($customers AS $customer) {
            $customer->name = $customer->user()->first()->name;
            $customer->date_applied=date('M d,Y', strtotime($customer->created_at));
            //$cyo = \App\Models\Transaction::where('user_id',$customer->user_id)->doesntExist();
             /* check  customer_subscriptions */
            $cutomer_subscription_info = \App\Models\Customer_subscription::where('customer_id', $customer->id)->where('end_date', '>=', date('Y-m-d'))->where('status','1')->doesntExist();
            if($cutomer_subscription_info){
                $customer->cyo='No';
            }
            else{
                    $customer->cyo='Yes';
                }

        }

        return Datatables::of($customers)
                        ->addColumn('action', function($customer) {
                            return view('admin.customer.actions', compact('customer'))->render();
                        })
                        ->addColumn('customer_name', function ($customer) {
                            return view('admin.customer.customer_name', compact('customer'))->render();
                            //'<a href="'.url('admin/customer/view/'.$customer->id).'">'.$customer->name.'</a>'; 
                        })
                        ->addColumn('company_name', function ($customer) {
                           // return view('admin.customer.customer_name', compact('customer'))->render();
                            return '<a href="'.url('admin/customer/view/'.$customer->id).'">'.$customer->company_name.'</a>'; 

                        })
                    
                        /* ->filter(function ($instance) use ($request) {
                          if (($request->has('customer_name')) && ($request->get('customer_name') != NULL)) {
                          $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                          return Str::contains($row['company_name'], $request->get('customer_name')) ? true : false;
                          });
                          }
                          }) */
                        ->rawColumns(['action', 'company_name'])
                        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data['customer'] = \App\Models\Customer::where('id', $id)->first();
        $mailing_city_name = \App\Models\City::where('id', '=', $data['customer']->mailing_city_id)->select('name')->first();

        $data['customer']->mailing_city_name = !empty($mailing_city_name) ? $mailing_city_name->name : '';
        $physical_city_name = \App\Models\City::where('id', '=', $data['customer']->physical_city_id)->select('name')->first();
        $data['customer']->physical_city_name = !empty($physical_city_name) ? $physical_city_name->name : '';

        $data['states'] = \App\Models\State::orderBy('name')->get();
        $data['cities'] = \App\Models\City::get();
        $data['countries'] = \App\Models\Country::get();
        $data['id'] = $id;
        $data['customer_agents'] = CustomerAgent::where('customer_id', '=', $id)->get();
        $data['customer_repres'] = CustomerRepresentative::where('customer_id', '=', $id)->get();
        $data['user_info'] = User::find($data['customer']->user_id);
        $data['account_managers'] = User::leftjoin('role_users', 'role_users.user_id', '=', 'users.id')
                ->where('role_users.role_id', '=', '3')
                ->get();
        $branches = Branch::where('customer_id', '=', $id)->get();
        if (isset($branches) && count($branches) > 0) {
            foreach ($branches AS $each_branch) {
                $city_name = \App\Models\City::where('id', '=', $each_branch->city_id)->select('name')->first();
                $each_branch->branch_city_name = ($city_name) ? $city_name->name : NULL;
                $county_name = \App\Models\City::where('id', '=', $each_branch->country)->select('county')->first();

                $each_branch->branch_county_name = ($county_name) ? $county_name->county : NULL;
            }
        }
        $data['branches'] = $branches;

        return view('admin.customer.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        $validation = Validator::make($request->all(), [
                    'name' => 'required',
                    'email' => 'required|unique:users,email,' . $request->user_id,
                    'password' => 'required',
                    'company_name' => 'required',
                    'contact_person' => 'required',
                    'mailing_address' => 'required',
                    'mailing_city' => 'required',
                    'mailing_state' => 'required|exists:states,id',
                    'mailing_zip' => 'required|numeric',
                    'company_physical_address' => 'required',
                    'physical_city' => 'required',
                    'physical_state' => 'required|exists:states,id',
                    'physical_zip' => 'required|numeric',
                    'office_number' => 'required',
                    'fax' => '',
//                    'mobile_number' => 'numeric',
                    'no_of_offices' => 'nullable|numeric',
                    'company_email.*' => 'required|email',
                    'agent_first_name.*' => 'required',
                    'agent_last_name.*' => 'required',
                    'agent_title.*' => 'required',
                    'repres_contact_person.*' => 'nullable',
                    'repres_company_branch_name.*' => 'nullable',
                    'repres_email.*' => 'nullable|email'
                        ], [
                    'company_email.*' => 'Company email is invalid',
                    'agent_first_name.*' => 'Officer first name format is invalid',
                    'agent_last_name.*' => 'Officer last name format is invalid',
                    'agent_title.*' => 'Officer title format is invalid',
                    'repres_contact_person.*' => 'Representative contact person format is invalid',
//                    'repres_company_branch_name.*' => 'Representative branch name format is invalid',
                    'repres_email.*' => 'Representative email format is invalid',
        ]);

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation)->withInput($request->all());
        }
        // dd($request->all());


        $input = $request->all();
        $emailArray = $request->company_email;
        $email = implode(",", $emailArray);
        $request->request->set('company_email', $email);
        $request->request->set('mailing_city', $request->mailing_city_value_id);
        $request->request->set('physical_city', $request->physical_city_value_id);

        $request->request->set('branch_city', isset($request->branch_city_ids[0])?$request->branch_city_ids[0]:null);
        $request->request->set('branch_country', isset($request->branch_county_ids[0])?$request->branch_county_ids[0]:null);
        // dd($email);

        $user = User::find($request->user_id)->update([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'password_text' => $request->password
        ]);

        $result2 = \App\Models\Customer::find($id)->update([
            'company_name' => $request->company_name,
            'contact_person' => $request->contact_person,
            'mailing_address' => $request->mailing_address,
            'mailing_city_id' => $request->mailing_city,
            'mailing_state_id' => $request->mailing_state,
            'mailing_zip' => $request->mailing_zip,
            'physical_address' => $request->company_physical_address,
            'physical_city_id' => $request->physical_city,
            'physical_state_id' => $request->physical_state,
            'physical_zip' => $request->physical_zip,
            'hear_about' => $request->hear_about,
            'office_number' => $request->office_number,
            'fax_number' => $request->fax,
            'mobile_number' => $request->mobile_number,
            'company_email' => $request->company_email,
            'no_of_offices' => $request->no_of_offices,
            'account_manager_id' => $request->account_manager_id,
        ]);
         if($result2 && isset($request->account_manager_id)){
            $work_orders = \App\Models\WorkOrder::where('customer_id',$id)->get();
            $orders = $work_orders->toarray();
            foreach ($work_orders as $key => $value) {
                $value->account_manager_id = $request->account_manager_id;
                $value->save();  
            }

            $cyo_work_orders = \App\Models\Cyo_WorkOrders::where('customer_id',$id)->get();
            $cyo_orders = $cyo_work_orders->toarray();
              foreach ($cyo_work_orders as $key => $value) {
                $value->account_manager_id = $request->account_manager_id;
                $value->save();  
            }
    }

        //Customer Agents
        $customerAgents = CustomerAgent::where('customer_id', '=', $id)->delete();

        if ((isset($input['agent_first_name'])) && $input['agent_first_name'][0] != NULL) {
            foreach ($input['agent_first_name'] as $key => $val) {
                CustomerAgent::create([
                    'customer_id' => $id,
                    'first_name' => $input['agent_first_name'][$key],
                    'last_name' => $input['agent_last_name'][$key],
                    'title' => $input['agent_title'][$key]
                ]);
            }
        }

            
        /* if ((isset($input['agent_first_name'])) && $input['agent_first_name'][0] != NULL) {
            foreach ($input['agent_first_name'] as $key => $val) {
                echo $input['agent_first_name'][$key].'<br>';
                CustomerAgent::where('customer_id', '=', $id)
                ->update([
                    'first_name' => $input['agent_first_name'][$key],
                    'last_name' => $input['agent_last_name'][$key],
                    'title' => $input['agent_title'][$key]
                    ]);
                }
            } */
           

        //Customer repreentative
        $customerRepresentatives = CustomerRepresentative::where('customer_id', '=', $id)->delete();

        //Representative Details
        if ((isset($input['repres_contact_person'])) && $input['repres_contact_person'][0] != NULL) {
            foreach ($input['repres_contact_person'] as $key => $val) {
                if(!empty($input['repres_contact_person'][$key]) || !empty($input['repres_company_branch_name'][$key]) || !empty($input['repres_email'][$key]) || !empty($input['repres_mobile_number'][$key])) {
                    CustomerRepresentative::create([
                        'customer_id' => $id,
                        'contact_person' => $input['repres_contact_person'][$key],
                        'branch_name' => $input['repres_company_branch_name'][$key],
                        'email' => $input['repres_email'][$key],
                        'mobile_number' => $input['repres_mobile_number'][$key]
                    ]);
                }
            }
        }
            

        /* if ((isset($input['repres_contact_person'])) && $input['repres_contact_person'][0] != NULL) {
            foreach ($input['repres_contact_person'] as $key => $val) {
                CustomerRepresentative::where('customer_id', '=', $id)
                        ->update([
                            'contact_person' => $input['repres_contact_person'][$key],
                            'branch_name' => $input['repres_company_branch_name'][$key],
                            'email' => $input['repres_email'][$key],
                            'mobile_number' => $input['repres_mobile_number'][$key]
                ]);
            }
        } */
        //Customer branches
        if (!empty($request->branch_city_ids) && count($request->branch_city_ids) > 1) {
            $branch_city = explode(',', $request->branch_city);
        }
        if (!empty($request->branch_county_ids) &&  count($request->branch_county_ids) > 1) {
            if ($request->branch_country != NULL) {
                $branch_country = explode(',', $request->branch_country);
            } else {
                $branch_country = '';
            }
        }


        if ((isset($input['branch_name'])) && $input['branch_name'][0] != NULL) {
            foreach ($input['branch_name'] as $key => $val) {
                Branch::where('customer_id', '=', $id)
                        ->update([
                            'name' => $input['branch_name'][$key],
                            'contact_person' => $input['branch_contact_person'][$key],
                            'phone' => $input['branch_phone'][$key],
                            'email' => NULL,
                            'title' => $input['branch_title'][$key],
                            'first_name' => $input['branch_first_name'][$key],
                            'last_name' => $input['branch_last_name'][$key],
                            'address' => $input['branch_address'][$key],
                            'city_id' => (isset($branch_city)) ? $branch_city[$key] : $request->branch_city,
                            'state_id' => $input['branch_state'][$key],
                            'country' => (isset($branch_country)) ? (($branch_country != '') ? $branch_country[$key] : '') : $request->branch_country,
                            'zip' => $input['branch_zip'][$key]
                ]);
            }
        }


        if ($result2) {
            //Session::set('success',trans('backpack::base.account_updated'));
            return redirect()->back()->with('success', 'Customer info updated successfully.');
        } else {

            return redirect()->back()->with('error', trans('backpack::base.error_saving'));
        }
//        $emailArray = $request->company_email;
//        $email = implode(",", $emailArray);
//        $request->company_email = $email;
//        $result1 = \App\Models\Customer::find($id)->update($request->except(['_token', 'company_email']));
//        $result2 = \App\Models\Customer::find($id)->update([
//            'company_email' => $email,
//        ]);
//        if ($result1 && $result2) {
//            Alert::success('Record updated uccessfully')->flash();
//        } else {
//            Alert::error('Problem in Record saving')->flash();
//        }
//        return redirect('admin/customer');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    /*
     * Function is used to change customer status
     */

    public function changestatus(Request $request) {

        $id = $request->input('customer_id');
        $status = $request->input('status');

        $result = \App\Models\Customer::find($id);

        $result->status = $status;
        $result->save();
        $email_id = \App\Models\Customer::select('users.email')->join('users', 'users.id', 'customers.user_id')->where('customers.id', $id)->get();
        $email_id = $email_id[0]['email'];
        if ($status == 1) {
            Mail::to($email_id)->send(new CustomerApprovalMail($result));
        }


//        return response()->json($result);
        return redirect::back()->with('success', 'Customer status successfully changed');
    }

    /*
     * Function is used to view customer details
     */

    public function viewCustomer($customer_id) {


        $customer = \App\Models\Customer::find($customer_id);

        $user_details = \App\Models\User::find($customer->user_id);

        $mailing_state = \App\Models\State::find($customer->mailing_state_id);
        $mailing_city = \App\Models\City::find($customer->mailing_city_id);

//        $customer->mailing_address = $customer->mailing_address . ',' . $mailing_city->name . ',' . $mailing_state->name . ',' . $customer->mailing_zip;
        $customer->mailing_state_name = !empty($mailing_state) ? $mailing_state->name : '';
        $customer->mailing_city_name = !empty($mailing_city) ? $mailing_city->name : '';
        $physical_state = \App\Models\State::find($customer->physical_state_id);
        $physical_city = \App\Models\City::find($customer->physical_city_id);

//        $customer->physical_address = $customer->physical_address . ',' . $physical_city->name . ',' . $physical_state->name . ',' . $customer->physical_zip;
        $customer->physical_state_name = !empty($physical_state) ? $physical_state->name : '';
        $customer->physical_city_name = !empty($physical_city) ? $physical_city->name : '';
        if ($customer->account_manager_id != NULL) {
            $account_manager = \App\Models\User::find($customer->account_manager_id);
            $customer->account_manager_name = $account_manager->name;
        }


        //Customer Agent
        $customer_agent = \App\Models\CustomerAgent::where('customer_id', '=', $customer_id)->get();

        //Customer Representative
        $customer_repres = \App\Models\CustomerRepresentative::where('customer_id', '=', $customer_id)->get();

        //Customer Branches
        $customer_branches = \App\Models\Branch::where('customer_id', '=', $customer_id)->get();
        if (isset($customer_branches) && count($customer_branches) > 0) {
            foreach ($customer_branches AS $each_branch) {
                if (isset($each_branch->state_id) && $each_branch->state_id != NULL) {
                    $state = \App\Models\State::find($each_branch->state_id);
                    $each_branch->state = $state->name;
                }
                if (isset($each_branch->city_id) && $each_branch->city_id != NULL) {

                    $city = \App\Models\City::find($each_branch->city_id);
                    $each_branch->city = $city->name;
                }
            }
        }
        $data['user_details'] = $user_details;
        $data['agents'] = $customer_agent;
        $data['customer'] = $customer;
        $data['representatives'] = $customer_repres;
        $data['branches'] = $customer_branches;
        //Check if valid subscription for customer is present
        $data['customer_subscription'] = \App\Models\Customer_subscription::join('packages', 'packages.id', '=', 'customer_subscriptions.package_id')->where(['customer_id' => $data['customer']['id']])->orderBy('customer_subscriptions.id', 'desc')->limit(1)->first();

        /*****Card details*******/
      
            $data['user_cards'] = [];
            $cards = \App\Models\UserCard::where(['user_id'=>$user_details->id])->get()->toArray();
            
            if($cards)
            {
                $qbo = new QuickBooksController();       
                $user_cards = $qbo->generateTokens(config('constants.QBO.PAYMENT_SCOPE'),'getSavedCards',['customer_id'=>$user_details->id,'qbo_customer_id'=>$user_details->qbo_customer_id,'card_id'=>$cards[0]['card_id']]);   
                if($user_cards['response'] == 'success' && !empty($user_cards['cards']))
                {
                    $data['user_cards'] = $user_cards['cards'];
                }
            }
        return view('admin.customer.view', $data);
    }

    /**
     * Showing the manage package for the specific customer
     * @param  int  $id
     */
    public function managePackagePricing($id) {
        $data['id'] = $id;
        /*
         * Remove where clause when we want to add for all packages 
         * Now taking for the base package.
         */
        $customer = \App\Models\Customer_subscription::where('customer_id','=',$id)->select('package_id')->get()->toArray();
        //dd($customer[0]);
        $data['package'] = \App\Models\Package::get();
        $data['state'] = \App\Models\State::where('status', '=', 1)->get();
        if(isset($customer[0]['package_id'])){
            $data['selected_pkg']=\App\Models\Package::where('id','=',$customer[0]['package_id'])->get();
            //dd($data['selected_pkg'],$customer);
        }
        
        //dd($data['selected_pkg'][0]['id']);
        return view('admin.customer.manage_package_pricing', $data);
    }

    //php artisan make:model Models/Customer_package
    public function loadPackagePricing(Request $request) {
        try {
            //if (isset($request['state_id']) && (!empty($request['state_id'])) && (isset($request['package_id']) && (!empty($request['package_id']))) && (isset($request['id']) && (!empty($request['id'])))) {
            if ((isset($request['package_id']) && (!empty($request['package_id']))) && (isset($request['id']) && (!empty($request['id'])))) {
                $data = array();
                $package_id = $request['package_id'];
                // $state_id = $request['state_id'];

                $ntoId = 9; /* Need to take from the env */
                $data['nto_id'] = $ntoId;
                $data['package'] = \App\Models\Package::find($package_id);
                $data['package_id'] = $package_id;
                // $data['notice'] = Notice::where('status', '=', 1)->get();
                $data['nto'] = Notice::where(array('status' => 1, 'id' => $ntoId))->get();
                //$data['result'] = \App\Models\Customer_package::where(array('package_id' => $package_id, 'state_id' => $state_id, 'status' => 1, 'customer_id' => $request['id']))->get();
                $data['result'] = \App\Models\Customer_package::where(array('package_id' => $package_id, 'status' => 1, 'customer_id' => $request['id']))->get();
                $data['customer_id'] = $request['id'];
                $data['notices'] = \App\Models\Notice::where('notices.status', 1)->where('state_id', '10')->where('notices.notice_sequence','0')->whereIn('type', ['1', '2'])
                        ->orderBy('notices.type', 'DESC')->orderBy('notices.notice_sequence', 'ASC')
                        ->get();

               /* $data['price_range'] = [['1', '30'], ['31', '50'], ['51', '100'], ['101', '150'], ['151', '200'], ['200', '10000'],['0','0']];
                $data['col_price_range'] = [['1','5000'],['5001','10000'],['10001','15000'],['15001','20000'],['20001','30000'],['30001','40000'],['40001','50000'],['50001','99999999']];*/
                $data['price_range'] = \App\Models\Customer_package::join('notices','notices.id','=','customer_packages.notice_id')->select('customer_packages.id','lower_limit','upper_limit','master_notice_id','charge','cancelation_charge','additional_address','description')->where('notices.master_notice_id',config('constants.MASTER_NOTICE')['NTO']['ID'])->where('customer_packages.package_id',$package_id)->where('customer_id',$data['customer_id'])->where('additional_address','=',NULL)->get()->toArray();
               $data['price_range_id'] = 'customer';
                if(empty($data['price_range'])){
                    $data['price_range'] = \App\Models\Pricing::join('notices','notices.id','=','pricings.notice_id')->select('pricings.id','lower_limit','upper_limit','master_notice_id','charge','cancelation_charge','additional_address','description')->where('notices.master_notice_id',config('constants.MASTER_NOTICE')['NTO']['ID'])->where('pricings.package_id',$package_id)->where('pricings.pricing_copied_id',0)->whereNull('additional_address')->get()->toArray();
                    $data['price_range_id'] = 'default';
                }
 
                $data['col_price_range'] = \App\Models\Customer_package::join('notices','notices.id','=','customer_packages.notice_id')->select('customer_packages.id','lower_limit','upper_limit','master_notice_id','charge','cancelation_charge','additional_address','description')->where('notices.master_notice_id',config('constants.MASTER_NOTICE')['COL']['ID'])->where('customer_packages.package_id',$package_id)->where('customer_id',$data['customer_id'])->get()->toArray();
                if(empty($data['col_price_range'])){
                    $data['col_price_range'] = \App\Models\Pricing::join('notices','notices.id','=','pricings.notice_id')->select('pricings.id','lower_limit','upper_limit','master_notice_id','charge','cancelation_charge','additional_address','description')->where('notices.master_notice_id',config('constants.MASTER_NOTICE')['COL']['ID'])->where('pricings.pricing_copied_id',0)->where('pricings.package_id',$package_id)->get()->toArray();
                }

                $data['bcol_price_range'] = \App\Models\Customer_package::join('notices','notices.id','=','customer_packages.notice_id')->select('customer_packages.id','lower_limit','upper_limit','master_notice_id','charge','cancelation_charge','additional_address','charge','cancelation_charge','additional_address','description')->where('notices.master_notice_id',config('constants.MASTER_NOTICE')['BCOL']['ID'])->where('customer_packages.package_id',$package_id)->where('customer_id',$data['customer_id'])->get()->toArray();
                if(empty($data['bcol_price_range'])){
                   $data['bcol_price_range'] = \App\Models\Pricing::join('notices','notices.id','=','pricings.notice_id')->select('pricings.id','lower_limit','upper_limit','master_notice_id','charge','cancelation_charge','additional_address','description')->where('notices.master_notice_id',config('constants.MASTER_NOTICE')['BCOL']['ID'])->where('pricings.pricing_copied_id',0)
                        ->where('pricings.package_id',$package_id)->get()->toArray();
                }

                $data['additional_address'] = \App\Models\Customer_package::join('notices','notices.id','=','customer_packages.notice_id')->select('customer_packages.id','lower_limit','upper_limit','master_notice_id','charge','cancelation_charge','additional_address','description')->where('notices.master_notice_id',config('constants.MASTER_NOTICE')['NTO']['ID'])->where('customer_packages.package_id',$package_id)->where('additional_address','=','additional_address')->where('customer_id',$data['customer_id'])->get()->toArray();

                if(empty($data['additional_address'])){
                    $data['additional_address'] = \App\Models\Pricing::join('notices','notices.id','=','pricings.notice_id')->select('pricings.id','lower_limit','upper_limit','master_notice_id','charge','cancelation_charge','additional_address','description')->where('notices.master_notice_id',config('constants.MASTER_NOTICE')['NTO']['ID'])->where('pricings.package_id','=',$request['package_id'])
                        ->where('pricings.pricing_copied_id',0)
                        ->where('additional_address','=','additional_address')->get()->toArray();
                }




                $view = view('admin.customer.partial_package', $data)->render();
                return Response::json(array('html' => $view, 'status' => TRUE));
            } else {
                throw new Exception("Package Id is missing");
            }
        } catch (\Exception $ex) {
            return Response::json(array('message' => $ex->getMessage(), 'status' => FALSE));
        }
    }

    public function savePackagePricing(Request $request) {
        try {
            $postdata = $request->toArray();  
            
            if (isset($postdata['package_type']) && (!empty($postdata['package_type']))) {
                //if (isset($postdata['state']) && (!empty($postdata['state'])) && (isset($postdata['id']) && (!empty($postdata['id'])))) {

                /* Making status 0 for all pricing against package_id / notice_id */

                //\App\Models\Customer_package::where(array('package_id' => $postdata['package_type'], 'state_id' => $postdata['state'], 'customer_id'=>$postdata['id']))->delete();
                /*  \App\Models\Customer_package::where(array('package_id' => $postdata['package_type'], 'customer_id' => $postdata['id']))->delete();

                  if (isset($postdata['nto_pricing_id']) && (!empty($postdata['nto_pricing_id'])) && ($postdata['package_type'] == 1)) {
                  for ($x = 0; $x < count($postdata['nto_pricing_id']); $x++) {
                  $lowerLimit = $postdata['low_limit'][$x];
                  $uppperLimit = $postdata['high_limit'][$x];
                  $charges = $postdata['charges'][$x];
                  $noticeId = $postdata['notice'][$x];
                  $pricingId = $postdata['nto_pricing_id'][$x];
                  $cancellationChage = $postdata['nto_cancellation_charge'][$x];
                  $pricing = new \App\Models\Customer_package();
                  // if ($pricingId > 0) {
                  //  $pricing = \App\Models\Pricing::find($pricingId);
                  // }
                  $pricing->package_id = $postdata['package_type'];
                  $pricing->notice_id = $noticeId;
                  //$pricing->state_id = $postdata['state'];
                  $pricing->lower_limit = $lowerLimit;
                  $pricing->upper_limit = $uppperLimit;
                  $pricing->charge = $charges;
                  $pricing->created_at = date("Y-m-d H:i:s");
                  $pricing->status = 1;
                  $pricing->cancelation_charge = $cancellationChage;
                  $pricing->customer_id = $postdata['id'];
                  $pricing->save();
                  }
                  }

                  if (isset($postdata['per_notice_pricing_id']) && (!empty($postdata['per_notice_pricing_id']))) {
                  for ($x = 0; $x < count($postdata['per_notice_pricing_id']); $x++) {
                  $lowerLimit = NULL;
                  $uppperLimit = NULL;
                  $charges = $postdata['noticecharges'][$x];
                  $noticeId = $postdata['noticechange'][$x];
                  $pricingId = $postdata['per_notice_pricing_id'][$x];
                  $pricing = new \App\Models\Customer_package();

                  $pricing->package_id = $postdata['package_type'];
                  $pricing->notice_id = $noticeId;
                  //$pricing->state_id = $postdata['state'];
                  $pricing->lower_limit = $lowerLimit;
                  $pricing->upper_limit = $uppperLimit;
                  $pricing->charge = $charges;
                  $pricing->created_at = date("Y-m-d H:i:s");
                  $pricing->description = $postdata['post_sub_description'][$x];
                  $pricing->status = 1;
                  $pricing->customer_id = $postdata['id'];
                  $pricing->save();
                  }
                  } */
                //}

                if ($postdata['package_type'] != 1) {
                    $package = \App\Models\Package::find($postdata['package_type']);
                    $package->charge = $postdata['create_own_charge'];
                    $package->description = $postdata['description'];
                    $package->cancellation_charge = NULL;
                    $package->save();
                }
                // dd($postdata['notice']);
                if (isset($postdata['notice']) && (!empty($postdata['notice']))) {
                    foreach ($postdata['notice'] as $notice_val) {
                        $keyAdjust = 0;
                        foreach ($notice_val['description'] as $key => $description_val) {
                            $pricing_id = isset($notice_val['id']) ? $notice_val['id'][$key] : NULL;
                            
                            $limtKey = $key;
                            $additional_address = $lowerLimit = $uppperLimit = NULL;
                            
                            if(isset($notice_val['additional_address'][$key]) && !empty($notice_val['additional_address'][$key])) {
                                $additional_address = $notice_val['additional_address'][$key];
                                // $limtKey = $key-1;
                            }

                            if(isset($notice_val['low_limit'][$limtKey]) && !empty($notice_val['low_limit'][$limtKey])) {
                                $lowerLimit = $notice_val['low_limit'][$limtKey];
                            } 
                            if(isset($notice_val['high_limit'][$limtKey]) && !empty($notice_val['high_limit'][$limtKey])) {
                                $uppperLimit = $notice_val['high_limit'][$limtKey];
                            } 
                            
                            //$additional_address = isset($notice_val['additional_address'][$key]) && !empty($notice_val['additional_address'][$key]) ? $notice_val['additional_address'][$key] : NULL;
                            $package_id = $postdata['package_type'];
                            $noticeId = $notice_val['notice_id'];
                            $charges = $notice_val['charges'][$key];
                            $package_range = config('constants.PACKAGE_RANGE');
                            if(array_key_exists($uppperLimit, $package_range)){
                               $uppperLimit = $package_range[$uppperLimit];
                            }
                            $nto_cancellation_charge = $notice_val['nto_cancellation_charge'][$key];
                            $description = $description_val;

                           /* $data_is_exist = \App\Models\Customer_package::where(['customer_id' => $postdata['id'], 'notice_id' => $noticeId, 'package_id' => $package_id, 'lower_limit' => $lowerLimit, 'upper_limit' => $uppperLimit])->first();*/
                            // $data_is_exist = \App\Models\Customer_package::find($pricing_id);
                           $data_is_exist = \App\Models\Customer_package::where(['id' => $pricing_id,'customer_id' => $postdata['id'], 'notice_id' => $noticeId, 'package_id' => $package_id])->first();
                           if(!empty($charges) || $charges >= 0){
                            if (!empty($data_is_exist)) {
                                /* if not empty update pricing */
                                $updateData = [
                                    'package_id' => $package_id, 'notice_id' => $noticeId,
                                    'lower_limit' => $lowerLimit, 'upper_limit' => $uppperLimit,
                                    'charge' => $charges, 'description' => $description,
                                    'cancelation_charge' => $nto_cancellation_charge,
                                    'additional_address' => $additional_address,
                                    'updated_at' => date("Y-m-d H:i:s"), 'status' => 1,
                                ];
                                \App\Models\Customer_package::where('id', $pricing_id)
                                        ->update($updateData);
                            } else {
                                /* add pricing */
                                $pricing = new \App\Models\Customer_package();
                                $pricing->package_id = $package_id;
                                $pricing->customer_id = $postdata['id'];
                                $pricing->notice_id = $noticeId;
                                $pricing->lower_limit = $lowerLimit;
                                $pricing->upper_limit = $uppperLimit;
                                $pricing->charge = $charges;
                                $pricing->created_at = date("Y-m-d H:i:s");
                                $pricing->description = $description;
                                $pricing->additional_address = $additional_address;
                                $pricing->cancelation_charge = $nto_cancellation_charge;
                                $pricing->status = 1;
                                $pricing->save();
                            }
                        } else{
                            if(!empty($pricing_id)){
                                $result = \App\Models\Customer_package::find($pricing_id);
                                if(!empty($result)) {
                                    $result->delete();
                                }
                            }
                        }
                        }
                    }
                }
                
                Session::flash('success', 'Pricing details saved successfully');
                return redirect('admin/customer/');
                //  return Response::json(array('message' => 'Pricing details saved successfully', 'status' => TRUE));
                //return redirect::to('admin/manage-subscriptions/add-package')->with('success', 'Pricing details saved successfully');
            }
        } catch (\Exception $ex) {
            dd($ex->getMessage());
            Session::flash('error', 'Pricing details not save successfully,Please try again');
            return redirect('admin/customer/');
        }
    }

    public function postChangePasswordForm(Request $request) {
        $validation = Validator::make($request->all(), [
                    'old_password' => 'required',
                    'new_password' => 'required|min:6',
                    'confirm_password' => 'required|same:new_password|min:6',
        ]);
        $user_password = \Auth::user()->password;
//         echo $user_password;
//        dd(Hash::check($request['old_password'],$user_password));

        if ($validation->fails()) {

            return redirect()->back()->withErrors($validation)->withInput($request->all());
        }
        if (!Hash::check($request['old_password'], \Auth::user()->password)) {
            return redirect()->back()->with('error', trans('backpack::base.old_password_incorrect'));
        }

        $user = Auth::user();
        $user->password = Hash::make($request->new_password);
        $user->password_text = $request->new_password;
        if ($user->save()) {
            return \Redirect::to('admin/edit-account-info')->with('success', 'Password successfully changed');
        } else {
            return redirect()->back()->with('error', trans('backpack::base.error_saving'));
        }
    }

    public function postCustomerChangePasswordForm(Request $request) {
        $validation = Validator::make($request->all(), [
                    'old_password' => 'required',
                    'new_password' => 'required|min:6',
                    'confirm_password' => 'required|same:new_password|min:6',
        ]);
        $user_password = \Auth::user()->password;
//         echo $user_password;
//        dd(Hash::check($request['old_password'],$user_password));

        if ($validation->fails()) {

            return response()->json(['error' => $validation->errors()->all()]);
        }
        if (!Hash::check($request['old_password'], \Auth::user()->password)) {
            return response()->json(['error' => [trans('backpack::base.old_password_incorrect')]]);
        }

        $user = Auth::user();
        $user->password = Hash::make($request->new_password);
        $user->password_text = $request->new_password;
        if ($user->save()) {

            return response()->json(['success' => 'Password successfully changed']);
        } else {
            return response()->json(['error' => [trans('backpack::base.error_saving')]]);
        }
    }

    public function autoCompleteCustomer(Request $request) {
        $query = $request->get('term', '');

//        $names= \App\User::leftjoin('role_users','role_users.user_id','=','users.id')
//                            ->where('name','LIKE',$query.'%')
//                            ->where('role_users.role_id','=',4)
//                            ->get();
//        
//        $data=array();
//        foreach ($names as $name) {
//                $data[]=array('value'=>$name->name,'id'=>$name->id);
//        }
        $names = \App\Models\Customer::where('company_name', 'LIKE', '%' . $query . '%')
                ->get();

        $data = array();
        foreach ($names as $name) {
            $data[] = array('value' => $name->company_name, 'id' => $name->company_name);
        }
        if (count($data))
            return $data;
        else
            return ['value' => 'No Result Found', 'id' => ''];
    }

    public function postChangeAccountSetting(Request $request) {
        $user_id = Auth::user()->id;

        $validation = Validator::make($request->all(), [
                    'name' => 'required|string|max:255',
                    'email' => 'required|unique:users,email,' . $user_id,
                    'company_name' => 'required',
                    'contact_person' => 'required|regex:/^[a-zA-Z ]+$/u',
                    'mailing_address' => 'required',
                    'mailing_city' => 'required',
                    'mailing_state' => 'required|exists:states,id',
                    'mailing_zip' => 'required|numeric',
                    'physical_address' => 'required',
                    'physical_city' => 'required',
                    'physical_state' => 'required|exists:states,id',
                    'physical_zip' => 'required|numeric',
                    'office_number' => 'required',
                    'fax' => '',
                    // 'mobile_number' => 'numeric',
                    'no_of_offices' => 'nullable|numeric',
                    'company_email.*' => 'required|email',
                    'agent_first_name.*' => 'required|regex:/^[a-zA-Z ]+$/u',
                    'agent_last_name.*' => 'required|regex:/^[a-zA-Z ]+$/u',
                    'agent_title.*' => 'required|regex:/^[a-zA-Z ]+$/u',
                    'repres_contact_person.*' => 'nullable|regex:/^[a-zA-Z ]+$/u',
                    'repres_company_branch_name.*' => 'nullable',
                    'repres_email.*' => 'nullable|email'
                        ], [
                    'company_email.*' => 'Company email is invalid',
                    'agent_first_name.*' => 'Officer first name format is invalid',
                    'agent_last_name.*' => 'Officer last name format is invalid',
                    'agent_title.*' => 'Officer title format is invalid',
                    'repres_contact_person.*' => 'Representative contact person format is invalid',
//            'repres_company_branch_name.*' => 'Representative branch name format is invalid',  
                    'repres_email.*' => 'Representative email format is invalid',
        ]);

        if ($validation->fails()) {
            return response()->json(['error' => $validation->errors()->all()]);
        }
        $user = Auth::user();
        $user->name = $request->name;
        $user->email = $request->email;
        $result1 = $user->save();
//        $result1 = $this->guard()->user()->update($request->except(['_token']));
        if (Auth::user()->hasRole('customer')) {
            $request->request->set('mailing_city', $request->mailing_city_value_id);
            $request->request->set('physical_city', $request->physical_city_value_id);
            $request->request->set('branch_city', $request->branch_city_ids);
            $request->request->set('branch_country', $request->branch_county_ids);

            $input = $request->all();
            $emailArray = $request->company_email;
            $email = implode(",", $emailArray);
            $request->request->set('company_email', $email);
            $result2 = Auth::user()->customer()->update([
                'company_name' => $request->company_name,
                'contact_person' => $request->contact_person,
                'mailing_address' => $request->mailing_address,
                'mailing_city_id' => $request->mailing_city,
                'mailing_state_id' => $request->mailing_state,
                'mailing_zip' => $request->mailing_zip,
                'physical_address' => $request->physical_address,
                'physical_city_id' => $request->physical_city,
                'physical_state_id' => $request->physical_state,
                'physical_zip' => $request->physical_zip,
                'hear_about' => $request->hear_about,
                'office_number' => $request->office_number,
                'fax_number' => $request->fax,
                'mobile_number' => $request->mobile_number,
                'company_email' => $request->company_email,
                'no_of_offices' => $request->no_of_offices,
            ]);

            //Customer Agents
            $customerAgents = CustomerAgent::where('customer_id', '=', Auth::user()->customer->id)->delete();

            if ((isset($input['agent_first_name'])) && $input['agent_first_name'][0] != NULL) {
                foreach ($input['agent_first_name'] as $key => $val) {
                    CustomerAgent::create([
                        'customer_id' => Auth::user()->customer->id,
                        'first_name' => $input['agent_first_name'][$key],
                        'last_name' => $input['agent_last_name'][$key],
                        'title' => $input['agent_title'][$key]
                    ]);
                }
            }

            //Customer repreentative
            $customerRepresentatives = CustomerRepresentative::where('customer_id', '=', Auth::user()->customer->id)->delete();

            //Representative Details
            if ((isset($input['repres_contact_person'])) && $input['repres_contact_person'][0] != NULL) {
                foreach ($input['repres_contact_person'] as $key => $val) {
                    if(!empty($input['repres_contact_person'][$key]) || !empty($input['repres_company_branch_name'][$key]) || !empty($input['repres_email'][$key]) || !empty($input['repres_mobile_number'][$key])) {
                        CustomerRepresentative::create([
                            'customer_id' => Auth::user()->customer->id,
                            'contact_person' => $input['repres_contact_person'][$key],
                            'branch_name' => $input['repres_company_branch_name'][$key],
                            'email' => $input['repres_email'][$key],
                            'mobile_number' => $input['repres_mobile_number'][$key]
                        ]);
                    }
                }
            }
            //Customer branches

            $customerBranches = Branch::where('customer_id', '=', Auth::user()->customer->id)->delete();

            //Branch Details
            if ((isset($input['branch_name'])) && $input['branch_name'][0] != NULL) {
                foreach ($input['branch_name'] as $key => $val) {
                    if ($input['branch_city'][$key]) {
                        $branch_city = $input['branch_city'][$key];
                    } else {
                        $branch_city = 'NULL';
                    }
                    if ($input['branch_country'][$key]) {
                        $branch_country = $input['branch_country'][$key];
                    } else {
                        $branch_country = NULL;
                    }
                    Branch::create([
                        'customer_id' => Auth::user()->customer->id,
                        'name' => $input['branch_name'][$key],
                        'contact_person' => ($input['branch_contact_person'][$key]) ? $input['branch_contact_person'][$key] : NULL,
                        'phone' => ($input['branch_phone'][$key]) ? $input['branch_phone'][$key] : 'NULL',
                        'email' => 'NULL',
                        'title' => ($input['branch_title'][$key]) ? $input['branch_title'][$key] : 'NULL',
                        'first_name' => ($input['branch_first_name'][$key]) ? $input['branch_first_name'][$key] : 'NULL',
                        'last_name' => ($input['branch_last_name'][$key]) ? $input['branch_last_name'][$key] : 'NULL',
                        'address' => ($input['branch_address'][$key]) ? $input['branch_address'][$key] : 'NULL',
                        'city_id' => $branch_city,
                        'state_id' => ($input['branch_state'][$key]) ? $input['branch_state'][$key] : 'NULL',
                        'country' => $branch_country,
                        'zip' => ($input['branch_zip'][$key]) ? $input['branch_zip'][$key] : 'NULL'
                    ]);
                }
            }
        }

        if ($result1 || $result2) {
            // Session::set('success',trans('backpack::base.account_updated'));
            return response()->json(['success' => 'Account settings successfully updated']);
        } else {

            return response()->json(['error' => [trans('backpack::base.error_saving')]]);
        }
    }

    public function activateSubscription(Request $request) {
        $customer_id = $request->customer_id;
        $customer_details = Customer::join('users', 'users.id', '=', 'customers.user_id')->where(['customers.id' => $customer_id])->first();

        if (isset($customer_id) && $customer_id != '') {
            try {
                $result = Customer_subscription::where(['customer_id' => $customer_id])->update(['status' => '1']);
                $customer_email = Customer::join('users', 'users.id', '=', 'customers.user_id')->where(['customers.id' => $customer_id])->get(['email'])->toArray();
                if (!empty($customer_email) && $customer_email[0]['email'] != '') {

                    \Mail::to($customer_email[0]['email'])->send(new CustomerActivationMail($customer_details));
                }
                \Session::flash('success', 'Subscription has been activated');
            } catch (\Exception $e) {
                // \Session::flash('error','Subscription could not be activated');
                \Session::flash('error', 'Error while sending mail');
            }
        } else {
            \Session::flash('error', 'Subscription could not be activated');
        }
        return redirect('admin/customer/view/' . $customer_id);
    }

    //delete customer agent
    public function removeAgent($id=0) {
        $result = false;
        if(!empty($id)) {
            $result = CustomerAgent::find($id)->delete();
        }
        return response()->json(['result' => $result]);

    }

    //delete customer representative
    public function removeRepresentative($id=0) {
        $result = false;
        if(!empty($id)) {
            $result = CustomerRepresentative::find($id)->delete();
        }
        return response()->json(['result' => $result]);

    }
   

}