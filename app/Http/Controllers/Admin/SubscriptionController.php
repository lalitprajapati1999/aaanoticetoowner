<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Notice;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Mail;
use App\Mail\ApproveChequeSubscription;

class SubscriptionController extends Controller {

    /**
     * Function is used to package listing
     * 
     */
    public function index() {
        $data = [];
        $data['package'] = \App\Models\Package::get();
        // dd($data);
        return view('admin.subscriptions.index', $data);
    }

    /**
     * 
     */
    public function datatableindex() {

        return Datatables::of()
                        ->addColumn('option', function() {
                            return view('admin.subscriptions.actions')->render();
                        })
                        ->rawColumns(['option'])
                        ->make(true);
    }

    public function addPackage($id) {
        $data = [];
        $data['id'] = $id;
        $data['package'] = \App\Models\Package::where('id', $id)->first();
        // dd($data['package']);
        $data['state'] = \App\Models\State::where('status', '=', 1)->get();

        $data['notices'] = \App\Models\Notice::where('notices.status', 1)->where('state_id', '10')->where('notices.notice_sequence','0')->whereIn('type', ['1', '2'])
                ->orderBy('notices.type', 'DESC')->orderBy('notices.notice_sequence', 'ASC')
                ->get();

       /* $data['price_range'] = [['1', '30'], ['31', '50'], ['51', '100'], ['101', '150'], ['151', '200'], ['200', '10000'],['1','10']];
        $data['col_price_range'] = [['1','5000'],['5001','10000'],['10001','15000'],['15001','20000'],['20001','30000'],['30001','40000'],['40001','50000'],['50001','99999999']];*/

        $data['price_range'] = \App\Models\Pricing::join('notices','notices.id','=','pricings.notice_id')->select('pricings.id','lower_limit','upper_limit','master_notice_id')->where('notices.master_notice_id',config('constants.MASTER_NOTICE')['NTO']['ID'])
          ->where('pricings.pricing_copied_id',0)->where('pricings.package_id',$id)->where('additional_address','=',NULL)->get()->toArray();
        $data['col_price_range'] = \App\Models\Pricing::join('notices','notices.id','=','pricings.notice_id')->select('pricings.id','lower_limit','upper_limit','master_notice_id')->where('pricings.pricing_copied_id',0)
          ->where('notices.master_notice_id',config('constants.MASTER_NOTICE')['COL']['ID'])->where('pricings.package_id',$id)->get()->toArray();
        $data['bcol_price_range'] = \App\Models\Pricing::join('notices','notices.id','=','pricings.notice_id')->select('pricings.id','lower_limit','upper_limit','master_notice_id')->where('notices.master_notice_id',config('constants.MASTER_NOTICE')['BCOL']['ID'])->where('pricings.pricing_copied_id',0)
          ->where('pricings.package_id',$id)->get()->toArray();
        $data['additional_address'] = \App\Models\Pricing::join('notices','notices.id','=','pricings.notice_id')->select('pricings.id','lower_limit','upper_limit','master_notice_id')->where('notices.master_notice_id',config('constants.MASTER_NOTICE')['NTO']['ID'])->where('pricings.package_id',$id)->where('pricings.pricing_copied_id',0)->where('additional_address','=','additional_address')->get()->toArray();
        $data['cyo_additional_address'] = \App\Models\Pricing::join('notices','notices.id','=','pricings.notice_id')->select('pricings.id','lower_limit','upper_limit','master_notice_id')->where('notices.master_notice_id',config('constants.MASTER_NOTICE')['NTO']['ID'])->where('pricings.package_id',$id)->where('pricings.pricing_copied_id',0)->where('additional_address','=','additional_address')->get()->toArray();
      //  dd($data);

      
         /*$data['price_range'] = [];
         $data['col_price_range'] = [];
*/
        $masterNoticeId = config('constants.MASTER_NOTICE');
        /*dd(config('constants.MASTER_NOTICE')['COL']['ID']);
        dd($masterNoticeId['NTO']['ID']);*/
        return view('admin.subscriptions.add_package', $data);
    }

    public function savePackage(Request $request) {

        try {


            $postdata = $request->all();
            if (isset($postdata['package_type']) && (!empty($postdata['package_type']))) {
                //if (isset($postdata['state']) && (!empty($postdata['state']))) {

                /* Making status 0 for all pricing against package_id / notice_id */
                //\App\Models\Pricing::where(array('package_id' => $postdata['package_type'], 'state_id' => $postdata['state']))->delete();
                // \App\Models\Pricing::where(array('package_id' => $postdata['package_type']))->delete();

                /* if (isset($postdata['nto_pricing_id']) && (!empty($postdata['nto_pricing_id'])) && ($postdata['package_type'] == 1)) {
                  for ($x = 0; $x < count($postdata['nto_pricing_id']); $x ++) {
                  $lowerLimit = $postdata['low_limit'][$x];
                  $uppperLimit = $postdata['high_limit'][$x];
                  $charges = $postdata['charges'][$x];
                  $noticeId = $postdata['notice'][$x];
                  $pricingId = $postdata['nto_pricing_id'][$x];
                  $description = $postdata['description'][$x];
                  $cancellationChage = $postdata['nto_cancellation_charge'][$x];
                  $pricing = new \App\Models\Pricing();
                  // if ($pricingId > 0) {
                  // $pricing = \App\Models\Pricing::find($pricingId);
                  //  }
                  $pricing->package_id = $postdata['package_type'];
                  $pricing->notice_id = $noticeId;
                  //$pricing->state_id = $postdata['state'];
                  $pricing->description = $description;
                  $pricing->lower_limit = $lowerLimit;
                  $pricing->upper_limit = $uppperLimit;
                  $pricing->charge = $charges;
                  $pricing->created_at = date("Y-m-d H:i:s");
                  $pricing->status = 1;
                  $pricing->cancelation_charge = $cancellationChage;
                  $pricing->save();
                  }
                  } else */
                if ($postdata['package_type'] != 1) {
                    $package = \App\Models\Package::find($postdata['package_type']);
                    $package->charge = $postdata['create_own_charge'];
                    $package->description = $postdata['description'];
                    $package->cancellation_charge = NULL;
                    $package->save();
                }

                if (isset($postdata['notice']) && (!empty($postdata['notice']))) {

                    foreach ($postdata['notice'] as $notice_val) {
                        foreach ($notice_val['description'] as $key => $description_val) {
                            $pricing_id = isset($notice_val['id']) ? $notice_val['id'][$key] : NULL;
                            $additional_address = isset($notice_val['additional_address']) ? $notice_val['additional_address'][$key] : NULL;
                            $package_id = $postdata['package_type'];
                            $noticeId = $notice_val['notice_id'];
                            $charges = $notice_val['charges'][$key];
                            $lowerLimit = isset($notice_val['low_limit']) ? $notice_val['low_limit'][$key] : NULL;
                            $uppperLimit = isset($notice_val['high_limit']) ? $notice_val['high_limit'][$key] : NULL;
                            $package_range = config('constants.PACKAGE_RANGE');
                            if(array_key_exists($uppperLimit, $package_range)){
                               $uppperLimit = $package_range[$uppperLimit];
                            }
                            $nto_cancellation_charge =  isset($notice_val['nto_cancellation_charge']) ? $notice_val['nto_cancellation_charge'][$key] : NULL;
                            $description = $description_val;


                            /*$data_is_exist = \App\Models\Pricing::where(['notice_id' => $noticeId, 'package_id' => $package_id, 'lower_limit' => $lowerLimit, 'upper_limit' => $uppperLimit])->first();*/
                        
                        if(!empty($charges)){
                            $data_is_exist = \App\Models\Pricing::find($pricing_id);
                            if(!empty($data_is_exist)) {
                                /* if not empty update pricing */
                                \App\Models\Pricing::where('id', $data_is_exist['id'])
                                        ->update([
                                            'package_id' => $package_id, 'notice_id' => $noticeId,
                                            'lower_limit' => $lowerLimit, 'upper_limit' => $uppperLimit,
                                            'charge' => $charges, 'description' => $description,
                                            'cancelation_charge' => $nto_cancellation_charge,
                                            'additional_address' => $additional_address,
                                            'updated_at' => date("Y-m-d H:i:s"), 'status' => 1,
                                ]);


                                /* update copied by values with updated value */
                                /* if not empty update pricing */
                                \App\Models\Pricing::where('pricing_copied_id', $data_is_exist['id'])
                                        ->update([
                                            'package_id' => $package_id,
                                            'lower_limit' => $lowerLimit, 'upper_limit' => $uppperLimit,
                                            'charge' => $charges, 'description' => $description,
                                            'cancelation_charge' => $nto_cancellation_charge,
                                            'additional_address' => $additional_address,
                                            'updated_at' => date("Y-m-d H:i:s"), 'status' => 1,
                                ]);

                            } else {
                                /* add pricing */
                                $pricing = new \App\Models\Pricing();
                                $pricing->package_id = $package_id;
                                $pricing->notice_id = $noticeId;
                                $pricing->lower_limit = $lowerLimit;
                                $pricing->upper_limit = $uppperLimit;
                                $pricing->charge = $charges;
                                $pricing->created_at = date("Y-m-d H:i:s");
                                $pricing->description = $description;
                                $pricing->cancelation_charge = $nto_cancellation_charge;
                                $pricing->additional_address = $additional_address;
                                $pricing->status = 1;
                                $pricing->save();
                            }
                          }else{
                            if(!empty($pricing_id)){
                            $result = \App\Models\Pricing::find($pricing_id);
                            $result->delete();
                          }
                          }
                        }
                    }
                   
                }
              
                /*    if (isset($postdata['per_notice_pricing_id']) && (!empty($postdata['per_notice_pricing_id']))) {
                  for ($x = 0; $x < count($postdata['per_notice_pricing_id']); $x ++) {
                  $lowerLimit = NULL;
                  $uppperLimit = NULL;
                  $charges = $postdata['noticecharges'][$x];
                  $noticeId = $postdata['noticechange'][$x];
                  $pricingId = $postdata['per_notice_pricing_id'][$x];
                  $pricing = new \App\Models\Pricing();
                  //                            if ($pricingId > 0) {
                  //                                $pricing = \App\Models\Pricing::find($pricingId);
                  //                            }
                  $pricing->package_id = $postdata['package_type'];
                  $pricing->notice_id = $noticeId;
                  //$pricing->state_id = $postdata['state'];
                  $pricing->lower_limit = $lowerLimit;
                  $pricing->upper_limit = $uppperLimit;
                  $pricing->charge = $charges;
                  $pricing->created_at = date("Y-m-d H:i:s");
                  $pricing->description = $postdata['post_sub_description'][$x];
                  $pricing->status = 1;
                  $pricing->save();
                  }
                  } */
                //    return Response::json(array('message' => 'Pricing details saved successfully', 'status' => TRUE));
                //}
                Session::flash('success', 'Pricing details saved successfully');
                return redirect('admin/manage-subscriptions/');
            }
        } catch (\Exception $ex) {
            Session::flash('success', 'Pricing details not save successfully,Please try again');
            return redirect('admin/manage-subscriptions/');
        }
    }

    public function loadPackage(Request $request) {
        try {
            //if (isset($request['state_id']) && (!empty($request['state_id'])) && (isset($request['package_id']) && (!empty($request['package_id'])))) {
            if ((isset($request['package_id']) && (!empty($request['package_id'])))) {
                $data = array();
                $package_id = $request['package_id'];
                //$state_id = $request['state_id'];

                $ntoId = 9; /* Need to take from env */
                $data['nto_id'] = $ntoId;
                $data['package'] = \App\Models\Package::find($package_id);
                $data['package_id'] = $package_id;
                $data['notice'] = Notice::where('status', ' = ', 1)->get();
                $data['nto'] = Notice::where(array('status' => 1, 'id' => $ntoId))->get();
                //$data['result'] = \App\Models\Pricing::where(array('package_id' => $package_id, 'state_id' => $state_id, 'status' => 1))->get();
                $data['result'] = \App\Models\Pricing::where(array('package_id' => $package_id, 'status' => 1))->get();

                $view = view('admin.subscriptions.partial_package', $data)->render();
                return Response::json(array('html' => $view, 'status' => TRUE));
            } else {
                throw new Exception("State or Package Id is missing");
            }
        } catch (\Exception $ex) {
            return Response::json(array('message' => $ex->getMessage(), 'status' => FALSE));
        }
    }

    public function getChequeSubscriptionList() {
        $data = array();

        $data['cheque_subscription'] = \App\Models\Customer_subscription::whereHas('package', function($query) {
                    
                })->whereHas('customer', function() {
                    
                })->with(['customer.user' => function() {
                        
                    }])->where(['type' => 2, 'status' => 0])->get();

        return view('admin.subscriptions.cheque_subscription_list', $data);
    }

    public function approveChequeSubscriptionList(Request $request) {
        try {
            $id = $request['id'];
            //dd($id);
            if (isset($id) && (!empty($id))) {

                $cheque_subscription = \App\Models\Customer_subscription::whereHas('package', function($query) {
                            
                        })->whereHas('customer', function() {
                            
                        })->with(['customer.user' => function() {
                                
                            }])->where(['type' => 2, 'status' => 0, 'id' => $id])->get();
                $cust_details = [];
                if (isset($cheque_subscription) && (!empty($cheque_subscription->toArray()))) {
                  //dd($cheque_subscription[0]->customer->user->email);
                    $cust_details['contactperson']=$cheque_subscription[0]->customer->contact_person;
                    $cust_details['mailing_address']=$cheque_subscription[0]->customer->user->email;
                    
                    $packageId = $cheque_subscription[0]->package->id;
                    $startDate = date("Y-m-d");
                    $endDate = NULL;
                    if (isset($packageId) && (!empty($packageId)) && ($packageId == 2)) {
                        $endDate = date("Y-m-d", strtotime("+1 month", strtotime($startDate)));
                    } elseif (isset($packageId) && (!empty($packageId)) && ($packageId == 3)) {
                        $endDate = date("Y-m-d", strtotime("+1 year", strtotime($startDate)));
                    }
                    if (isset($endDate) && (!empty($endDate))) {

                        $chequeSubscription = \App\Models\Customer_subscription::find($id);
                        $chequeSubscription->start_date = $startDate;
                        $chequeSubscription->end_date = $endDate;
                        $chequeSubscription->status = 1;
                        if ($chequeSubscription->save()) {
                            $checkdetails['start_date'] = $startDate;
                            $checkdetails['end_date'] = $endDate;

                          Mail::to($cheque_subscription[0]->customer->user->email)->send(new ApproveChequeSubscription($cust_details,$checkdetails));

                            return Response::json(array('message' => "User subscribed successfully", 'status' => TRUE));
                        } else {
                            throw new \Exception("Error while performing action please try again");
                        }
                    } else {
                        throw new \Exception("Error while performing action please try again");
                    }
                } else {
                    throw new \Exception("Error while performing action please try again");
                }
            } else {
                throw new \Exception("Subscription Id is missing");
            }
        } catch (\Exception $ex) {dd($ex);
            return Response::json(array('message' => $ex->getMessage(), 'status

        

        ' => FALSE));
        }
    }

}
