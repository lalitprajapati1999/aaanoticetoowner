<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
class ProjectTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['projectTypes'] = \App\Models\ProjectType::select()->get();
        return view('admin.project_type.list',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       // $data['projectTypes'] = \App\Models\ProjectType::select()->get();
        return view('admin.project_type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $rules = ['type' => 'required',
        ];
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails())
        {
           return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        $result = \App\Models\ProjectType::create([
          'type'      =>  $request->type,
         ]);
        if($result){
            return redirect('admin/project-type')->with('success', 'Project Type Successfully Created'); 
        }else{
            return redirect()->back()->withErrors('Their is problem in creating project type.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['TypesData'] =  \App\Models\ProjectType::find($id);
        return view('admin.project_type.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules = ['type' => 'required',
        ];
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails())
        {
           return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        
        $project_type_obj = \App\Models\ProjectType::where('id',$request->type_id)->first();
    
        $project_type_obj->type = $request->type;

        $result = $project_type_obj->save();
        if($result){
            return redirect('admin/project-type')->with('success', 'Project Type Successfully Updated'); 
        }else{
            return redirect()->back()->withErrors('Their is problem in updating project type.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->id;
        $notice_fields_id = \App\Models\NoticeField::select('id')->where('name','project_type')->get()->toArray();
        foreach ($notice_fields_id as $id => $name) {
            $work_order_fields = \App\Models\WorkOrderFields::select()->where('notice_field_id',$name['id'])->get();
            foreach ($work_order_fields as $key => $value) {
                $fields = \App\Models\WorkOrderFields::select('value')->where('notice_field_id',$value->notice_field_id) ->update(['value' => '']);
             } 
            
        }
        $result = \App\Models\ProjectType::where('id',$request->id)->delete();
        if($result){
            return redirect('admin/project-type')->with('success','Project Type Successfully Deleted');
        }else{
            return redirect()->back()->withErrors('Problem in record deleting');
        }
    }
}
