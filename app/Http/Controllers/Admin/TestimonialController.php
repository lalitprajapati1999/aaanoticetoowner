<?php


namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Testimonial;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Models\RoleUsers;
use Illuminate\Support\Facades\Redirect;
/**
 * Description of EmployeesController
 *
 * @author neosoft
 */
class TestimonialController extends Controller{
    //put your code here
    
    public function index(){
        $data = [];
         
       return view('admin.testimonial.list', $data);
    }
    /*
     * Function is used for listing and filter by employee name
     */
    public function getTestimonialFilter(Request $request){
         $testimonial = \DB::table('notice_testimonial')->orderBy('id','DESC')->get();
    
        return Datatables::of($testimonial)
                        ->addColumn('action', function($testimonial) {
                            return view('admin.testimonial.actions', compact('testimonial'))->render();
                        })
                        // ->filter(function ($instance) use ($request) {
                        //     if (($request->has('content')) && ($request->get('content') != NULL)) {
                        //         $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        //             return Str::contains($row['content'], $request->get('content')) ? true : false;
                        //         });
                        //     }
                        // })
                        ->make(true);
    }
    /*
     * Function is used to view create employee view
     */
    public function createTestimonial(){
          return view('admin.testimonial.create');
    }
    /*
     * Function is used to register employee
     */
    public function storeTestimonial(Request $request){
        //dd($request->all());
       $rules = ['content' => 'required',
            'client_name' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg'
        ];
        $validator = Validator::make(Input::all(), $rules,[
            'image' => 'The image must be a file of type: jpeg,png,jpg,gif,svg'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
         if ($request->hasFile('image')) { 
            $image = $request->file('image');
            
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/images/');
            $image->move($destinationPath, $name);
         }else{
             $name = '';
         }
        $testimonial = Testimonial::create([
            'content' => $request->content,
            'client_name'=>$request->client_name,
            'image'=>$name,
        ]);
      //dd($testimonial);
        //store in role_users
        // if($request->role == 2){
        //  $user->assignRole('admin');
        // }else{
        //     $user->assignRole('account-manager');
        // }
        
       if ($testimonial) {
               return redirect::to('admin/testimonial')->with('success','Testimonial successfully added');
       } else {
        return redirect::back()->with('error','Problem in Tmployee creation');
        }
    }
    
   
    /*
     * Function is used to edit employee view
     */
    public function editTestimonial($testimonial_id){
        $data = [];
        
        $data['testimonial'] = Testimonial::find($testimonial_id);
        
        return view('admin.testimonial.edit',$data);
    }
    /*
     * Function is used to update details
     */
    public function updateTestimonial(Request $request){
          
        $rules = ['content' => 'required',
                  'client_name' => 'required',
                  'image' => 'image|mimes:jpeg,png,jpg,gif,svg'
        ];
        $validator = Validator::make(Input::all(), $rules,[
            'image' => 'The image must be a file of type: jpeg,png,jpg,gif,svg'
        ]);
      
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        $testimonial = Testimonial::find($request->testimonial_id);
        $testimonial->content = $request->content;
        $testimonial->client_name = $request->client_name;
      
       
       if ($request->hasFile('image')) { 
            $image = $request->file('image');
            
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/images/');
            $image->move($destinationPath, $name);
            $testimonial->image = $name;
        }
      
         $testimonial->save();
       if ($testimonial) {
               return redirect::to('admin/testimonial')->with('success','Testimonial successfully updated');
       } else {
        return redirect::back()->with('error','Problem in Testimonial updation');
        }
    }
    /*
     * Function is used to view Employee
     */
    public function viewTestimonial($testimonial_id){
        $data['testimonial'] = Testimonial::find($testimonial_id);
        
        // $data['employee_role'] = RoleUsers::leftjoin('roles','roles.id','role_users.role_id')
        //                                     ->where('user_id','=',$employee_id)
        //                                     ->select('roles.name')
        //                                     ->first();
        
        return view('admin.testimonial.view',$data);
    }
    
    public function changeStatus(Request $request)
    { 
        $id = $request->testimonial_id;
        $status = $request->status;
        $result = Testimonial::find($id);
       
        $result->status = $status;
        $result->save();
        
        return response()->json($result);
    }
    /**
     * Function is used to delete note
     */
    public function deleteTestimonial(Request $request){
      
          $user_notes = \DB::table('notice_testimonial')
                            ->where('id','=',$request->testimonial_id);
          
          $result = $user_notes->delete();
          
          
          return redirect::to('admin/testimonial')->with('success', 'Testimonial successfully deleted.');
    }
   
}
