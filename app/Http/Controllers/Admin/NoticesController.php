<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;

class NoticesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        if (isset($request->state) && $request->state != NULL) {
            $notices = \App\Models\Notice::where('state_id', '=', $request->state)->orderBy('name')->whereIn('type', ['1', '2','3'])->get();
        } else {
            $notices = \App\Models\Notice::orderBy('name')->whereIn('type', ['1', '2','3'])->get();
        }
        $states = \App\Models\State::select()->orderBy('name')->get();

        return view('admin.notices.index', ['notices' => $notices, 'states' => $states, 'request' => $request->all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $data['states'] = \App\Models\State::select()->where('status', 1)->orderBy('name')->get();
        return view('admin.notices.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $validation = $this->validate(request(), [
            'name' => ['required', function ($attribute, $value, $fail) use($request) {

                    $get_notice_sequence = \App\Models\Notice::where('name', $value)->where('state_id', $request->state_id)->first();
                    if (!empty($get_notice_sequence)) {

                        $fail($attribute . ' must be unique.');
                    }
                }],
            'type' => 'required',
            'allow_cyo' => 'required',
            'state_id' => 'required|exists:states,id',
            'allow_deadline_calculator' => 'required'
        ]);

        try {//dd($request->all());
            $get_notice_sequence = \App\Models\Notice::select('notice_sequence')->where('type', $request->type)->orderby('notice_sequence', 'DESC')->first();
//dd($get_notice_sequence);
            $request['notice_sequence'] = $get_notice_sequence->notice_sequence + 1;
            $get_notice_data = \App\Models\Notice::where('id', $request->notice)->first();
            //dd($get_notice_datas);
            //$request['is_bond_of_claim'] = $get_notice_sequence->is_bond_of_claim;

            $request['is_bond_of_claim'] = $get_notice_data->notice_sequence;
            $request['is_rescind'] = $get_notice_data->is_rescind;
            $request['master_notice_id'] = $get_notice_data->master_notice_id;

            $notice_data = \App\Models\Notice::create($request->only('name', 'type', 'allow_cyo', 'state_id', 'allow_deadline_calculator', 'notice_sequence', 'is_claim_of_lien', 'is_bond_of_claim', 'is_rescind','master_notice_id')); 
            if (isset($request->notice) && !empty($request->notice)) {
                $notice_fields = \App\Models\NoticeField::where('notice_id', '=', $request->notice)->get();
                if (isset($notice_fields) && !empty($notice_fields)) {
                    foreach ($notice_fields as $key => $value) {

                        $result = \App\Models\NoticeField::create([
                                    'state_id' => $notice_data->state_id,
                                    'notice_id' => $notice_data->id,
                                    'section' => $value->section,
                                    'name' => $value->name,
                                    'type' => $value->type,
                                    'validation' => $value->validation,
                                    'is_required' => $value->is_required,
                                    'sort_order' => $value->sort_order
                        ]);
                    }
                }
                $notice_template = \App\Models\NoticeTemplate::where('notice_id', '=', $request->notice)->first();
                if (isset($notice_template) && !empty($notice_template)) {
                    $noticeTemplateInsert = \App\Models\NoticeTemplate::create([
                                'state_id' => $notice_data->state_id,
                                'is_parent_work_order' => $notice_template->is_parent_work_order,
                                'notice_id' => $notice_data->id,
                                'name' => $request->name,
                                'no_of_days' => $notice_template->no_of_days,
                                'content' => $notice_template->content
                    ]);
                }
                /* Get previouse notice price  */

              /*  $pricing_listing = \App\Models\Pricing::whereIN('package_id', ['1', '2', '3'])->where('notice_id', $request->notice)->get();
                foreach ($pricing_listing as $pricing_listing_val) {
                    // dd($pricing_listing_val->package_id);
                    $pricing = new \App\Models\Pricing();
                    $pricing->package_id = $pricing_listing_val->package_id;
                    $pricing->notice_id = $notice_data->id;
                    $pricing->lower_limit = $pricing_listing_val->lower_limit;
                    $pricing->upper_limit = $pricing_listing_val->upper_limit;
                    $pricing->charge = $pricing_listing_val->charge;
                    $pricing->description = $pricing_listing_val->description;
                    $pricing->cancelation_charge = $pricing_listing_val->cancelation_charge;
                    $pricing->status = $pricing_listing_val->status;
                    $pricing->pricing_copied_id = $pricing_listing_val->id;
                    $pricing->save();
                }*/
            }
           
        } catch (\Exception $e) {

            logger()->error($e->getMessage());

            \Alert::error(trans('backpack::base.error_saving'))->flash();
            return redirect()->back();
        }

        // show a success message
//        \Alert::success(trans('backpack::crud.insert_success'))->flash();
//        return redirect()->route('admin.notices');
        return \Redirect::to('admin/notices')->with('success', trans('backpack::crud.insert_success'));
    }

    /**
     * Change status the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function changeStatus(Request $request) {
        $request->validate([
            'id' => 'required|exists:notices,id'
        ]);

        try {
            $notice = \App\Models\Notice::find($request->id);
            $notice->status = ($notice->status == 1) ? 0 : 1;
            $notice->save();
        } catch (\Exception $e) {
            logger()->error($e->getMessage());
            \Alert::error(trans('backpack::base.error_saving'))->flash();
            die();
        }

        // show a success message
        \Alert::success(trans('backpack::crud.update_success'))->flash();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(\App\Models\Notice $notice) {
        $data['notice'] = $notice;
        $data['states'] = \App\Models\State::select()->orderBy('name')->get();
        return view('admin.notices.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, \App\Models\Notice $notice) {
        $request->validate([
            'name' => ['required', function ($attribute, $value, $fail) use($request, $notice) {
                    $get_notice_sequence = \App\Models\Notice::where('name', $value)->where('id', '!=', $notice->id)->where('state_id', $request->state_id)->first();
                    if (!empty($get_notice_sequence)) {
                        $fail($attribute . ' must be unique.');
                    }
                }], /* [
              'required',
              Rule::unique('notices', 'name')->ignore($notice->id)
              ], */
            // 'type' => 'required',
            'allow_cyo' => 'required',
            //  'state_id' => 'required|exists:states,id',
            'allow_deadline_calculator' => 'required'
        ]);

        try {
            //   dd($request->all());
            $state_id = $notice->state_id;
            $notice->update($request->only('name', 'allow_cyo', 'state_id', 'allow_deadline_calculator', 'is_claim_of_lien'));

            $notice->save();
            //   $notice_fields = \App\Models\NoticeField::select()->where('state_id', $state_id)->where('notice_id', $notice->id)->update(['state_id' => $request->state_id]);
            //  $notice_template = \App\Models\NoticeTemplate::select()->where('state_id', $state_id)->where('notice_id', $notice->id)->update(['state_id' => $request->state_id]);
        } catch (\Exception $e) {
            logger()->error($e->getMessage());
            \Alert::error(trans('backpack::base.error_saving'))->flash();
            return redirect()->back();
        }

        // show a success message
        \Alert::success(trans('backpack::crud.update_success'))->flash();

        return redirect('admin/notices');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function getAllNotices($hard_soft_type) {

        $data = \App\Models\Notice::leftjoin('states', 'states.id', '=', 'notices.state_id')
                ->where('notices.type', $hard_soft_type)
                ->where('notices.status', 1)->where('state_id', '10')
                ->select('notices.id', 'notices.name', 'states.name AS state_name')
                ->orderBy('notice_sequence', 'ASC')
                ->get();

        return response()->json($data);
    }

}
