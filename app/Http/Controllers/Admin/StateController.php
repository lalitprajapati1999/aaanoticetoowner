<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;

class StateController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        return view('admin.state.list');
    }

    public function StateList(\App\Models\State $state ,Request $request) {
        if (($request->state_name) && ($request->state_name) != NULL) {
            $state->where('name', 'like', '%' . $request->state_name . "%");
        }
        $states = $state->select()->get();
        return Datatables::of($states)
                        ->addColumn('action', function($states) {
                            return view('admin.state.actions', compact('states'))->render();
                        })
                        ->rawColumns(['action'])
                        ->make(true);
        $datatables = Datatables::of($states)->addColumn('actions', function ($states) {
                    return view('admin.state.actions', compact('states'))->render();
                })->rawColumns(['actions', 'is_other']);
        return $datatables->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('admin.state.create');
    }

    public function changestatus(Request $request) {

        $id = $request->input('State_id');
        $status = $request->input('status');

        $result = \App\Models\State::find($id);

        $result->status = $status;
        $result->save();

        return response()->json($result);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $valid_data = [
            'name' => 'required|unique:states,name',
            'abbr' => 'required'
        ];
        $validatedData = $request->validate($valid_data);
        try {
            $states = new \App\Models\State();
            $states->name = $request->name;
            $states->abbr = $request->abbr;
            $states->country_id = '1';
            $states->save();
            return redirect('admin/states')->with('success', 'State has been added successfully');
        } catch (\Exception $e) {
            $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
