<?php


namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\WorkOrder;
use App\Models\Customer;
use Illuminate\Support\Facades\Response;
use Yajra\DataTables\DataTables;

/**
 * Description of NewApplicantsController
 *
 * @author neosoft
 */
class NewApplicantsController extends Controller{
    
    public function index(){ 
        $data = [];
//        
        $data['account_managers'] = User::leftjoin('role_users', 'role_users.user_id', '=', 'users.id')
                ->where('role_users.role_id', '=', '3')
                ->get();

        $data['customers'] = Customer::where('account_manager_id','=',NULL)
                                        ->orderBy('id','DESC')
                                       ->paginate(10);
          foreach ( $data['customers'] AS $customer) {
            $customer->name = $customer->user()->first()->name;
             $cyo = \App\Models\Transaction::where('user_id',$customer->user_id)->doesntExist();
            if($cyo){
                $customer->cyo='No';
            }
            else{
                    $customer->cyo='Yes';
                }
        }
        return view('admin.new_applicants.index',$data);

    }
    
    public function assignAccountManager(Request $request){
        
        $acc_manager = $request->acc_manager;
        $customer_id = $request->customer_id;
        $customerObj = Customer::find($customer_id);
        $customerObj->account_manager_id = $acc_manager;
        $result = $customerObj->save();
        if($result){
        $work_orders = \App\Models\WorkOrder::where('customer_id',$customer_id)->get();
        $orders = $work_orders->toarray();
        foreach ($work_orders as $key => $value) {
            $value->account_manager_id = $acc_manager;
            $value->save();  
        }
         $cyo_work_orders = \App\Models\Cyo_WorkOrders::where('customer_id',$customer_id)->get();
            $cyo_orders = $cyo_work_orders->toarray();
              foreach ($cyo_work_orders as $key => $value) {
                $value->account_manager_id = $acc_manager;
                $value->save();  
            }
        }

      return Response::json(array('message' => "Account manager assigned successfully", 'status' => TRUE));
    }
}
