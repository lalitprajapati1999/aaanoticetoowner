<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data['categories'] = \App\Models\Category::select()->get();
                                                    
        return view('admin.category.list',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['categories'] = \App\Models\Category::select()->get();
        return view('admin.category.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = ['name' => 'required',
        ];
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails())
        {
           return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        $result = \App\Models\Category::create([
          'name'      =>  $request->name,
          'parent_id' => $request->parent_category,
         ]);
        if($result){
            return redirect('admin/category')->with('success', 'Category Successfully Created'); 
        }else{
            return redirect()->back()->withErrors('Their is problem in creating category.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['categories'] = \App\Models\Category::select()->get();
        $data['CategoryData'] =  \App\Models\Category::find($id);
        return view('admin.category.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {//dd($request->all());
        $rules = ['name' => 'required',
        ];
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails())
        {
           return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        
        $category_obj = \App\Models\Category::where('id',$request->category_id)->first();
    
        $category_obj->name = $request->name;
        $category_obj->parent_id = $request->parent_id;

        $result = $category_obj->save();
        if($result){
            return redirect('admin/category')->with('success', 'Category Successfully Updated'); 
        }else{
            return redirect()->back()->withErrors('Their is problem in updating category.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $notice_fields_id = \App\Models\NoticeField::select('id')->where('name','your_role')->get()->toArray();
        foreach ($notice_fields_id as $id => $name) {
            $work_order_fields = \App\Models\WorkOrderFields::select()->where('notice_field_id',$name['id'])->get();
            foreach ($work_order_fields as $key => $value) {
                $fields = \App\Models\WorkOrderFields::select('value')->where('notice_field_id',$value->notice_field_id) ->update(['value' => '']);
             } 
            
        }
      //  $request->category_id
        //Secondary Document Recipients
        $secondary_recipients = \App\Models\SecondaryDocumentRecipient::where('category_id','=',$request->category_id)
                                                                        ->get();
        if(isset($secondary_recipients) && count($secondary_recipients)>0){
            \App\Models\SecondaryDocumentRecipient::where('category_id', $request->category_id)->update(['category_id'=>NULL]);
        } 
        //Work order Recipients
        $work_order_recipients = \App\Models\Recipient::where('category_id','=',$request->category_id)
                                                                        ->get();
        if(isset($work_order_recipients) && count($work_order_recipients)>0){
            \App\Models\Recipient::where('category_id', $request->category_id)->update(['category_id'=>NULL]);
        } 
        //Create your own Work order Recipients
        $cyo_work_order_recipients = \App\Models\Cyo_WorkOrder_Recipient::where('category_id','=',$request->category_id)
                                                                        ->get();
        if(isset($cyo_work_order_recipients) && count($cyo_work_order_recipients)>0){
            \App\Models\Cyo_WorkOrder_Recipient::where('category_id', $request->category_id)->update(['category_id'=>NULL]);
        } 
        
        $result = \App\Models\Category::where('id',$request->category_id)->delete();
        
        if($result){
            return redirect('admin/category')->with('success','Category Successfully Deleted');
        }else{
            return redirect()->back()->withErrors('Problem in record deleting');
        }
    }
}
