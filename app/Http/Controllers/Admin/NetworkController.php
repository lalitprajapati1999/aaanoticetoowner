<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

/**
 * Description of NetworkController
 *
 * @author neosoft
 */
class NetworkController extends Controller {

    public function index() {

        $query = User::leftjoin('role_users', 'role_users.user_id', '=', 'users.id')
                ->where('role_users.role_id', '=', '3')
                ->where('users.allow_secondary_network', '=', 0)
                ->orderBy('users.name');
        if (!empty($_GET)) {
            $search_data = $_GET['search_network'];
            $query = $query->where('name', 'LIKE', "%{$search_data}%");
            $data['search'] = $_GET['search_network'];
            $data['search_network'] = $_GET['search_network'];
        }

        $data['users'] = $query->get();

        //Getting Allowed Network
        $data['allowed_network'] = User::leftjoin('role_users', 'role_users.user_id', '=', 'users.id')
                ->where('role_users.role_id', '=', '3')
                ->where('users.allow_secondary_network', '=', 1)
                ->get();
        return view('admin.network.index', $data);
    }

    public function allowSecondaryNetwork(Request $request) {
        $id = $request->input('userId');
        //Change allow_secondary_network = 1
        $result = User::find($id);
        $result->allow_secondary_network = 1;
        $result->save();

        $status = User::select('allow_secondary_network')->where('id', $id)->get()->first()->toArray();
        if ($status['allow_secondary_network'] == 1) {
            return 1;
        } else {
            return 0;
        }
    }

    public function removeSecondaryNetwork(Request $request) {
        $id = $request->input('userId');
        //Change allow_secondary_network = 1
        $result = User::find($id);
        $result->allow_secondary_network = 0;
        $result->save();

        $status = User::select('allow_secondary_network')->where('id', $id)->get()->first()->toArray();
        if ($status['allow_secondary_network'] == 0) {
            return 1;
        } else {
            return 0;
        }
    }

    public function autoCompleteAccountManager(Request $request) {
        $query = $request->get('term', '');
       
        $names = User::leftjoin('role_users', 'role_users.user_id', '=', 'users.id')
                ->where('users.name', 'LIKE', $query . '%')
                ->where('role_users.role_id', '=', '3')
                ->where('users.allow_secondary_network', '=', 1)
                ->orderBy('users.name')
                ->get();
        $data = array();
        foreach ($names as $name) {
            $data[] = array('value' => $name->name, 'id' => $name->name);
        }
        if (count($data))
            return $data;
        else
            return ['value' => 'No Result Found', 'id' => ''];
    }

}
