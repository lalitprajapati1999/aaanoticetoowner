<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Notice;
use App\Models\NoticeTemplate;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;

/**
 * Description of NoticeTemplateController
 *
 * @author neosoft
 */
class NoticeTemplateController extends Controller {
    /*
     * To display all notices which template has been created
     */

    public function index(Request $request) {
//         if (isset($request->state) && $request->state != NULL) {
//             $data['notice_templates'] = NoticeTemplate::leftjoin('notices','notices.id','=','notice_templates.notice_id')
//                                                    ->leftjoin('states','states.id','=','notice_templates.state_id')
//                                                    ->select('notice_templates.id','notices.name','states.name as state_name')
//                                                    ->where('notice_templates.state_id', '=', $request->state)
//                                                    ->orderBy('name')
//                                                    ->paginate(10);
//        } else {
//        }
        $data['states'] = \App\Models\State::select()->orderBy('name')->get();

        $data['request'] = $request->all();

        return view('admin.notice_template.index', $data);
    }

    public function noticeTemplateDatatable() {
        $notice_templates = NoticeTemplate::leftjoin('notices', 'notices.id', '=', 'notice_templates.notice_id')
                ->leftjoin('states', 'states.id', '=', 'notice_templates.state_id')
                ->select('notice_templates.id', 'notices.name', 'states.name as state_name')
                ->orderBy('name')
               ->whereIN('type', ['1','2','3'])
                ->get();
        return Datatables::of($notice_templates)
                        ->addColumn('action', function($template) {
                            return view('admin.notice_template.actions', compact('template'))->render();
                        })
                        ->make(true);
    }

    /**
     * Function is used to create template view
     * 
     */
    public function createTemplate() {
        $data['states'] = \App\Models\State::select('id', 'name')->where('status', 1)->orderBy('name')->get();

        $data['notices'] = \App\Models\Notice::select('id', 'name')->get();

        return view('admin.notice_template.create', $data);
    }

    /**
     * Function is used to store the notice template data
     * 
     * @param  \Illuminate\Http\Request  $request
     */
    public function storeNoticeTemplate(Request $request) {

        $rules = ['notice_name' => 'required',
            'state' => 'required| exists:states,id',
            'notice_custom_name' => 'required',
            'no_of_days' => 'required|integer',
            'notice_template_content' => 'required'
        ];
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }
        $result = NoticeTemplate::create([
                    'state_id' => $request->state,
                    'is_parent_work_order' => ($request->is_parent_work_order) ? $request->is_parent_work_order : 0,
                    'notice_id' => $request->notice_name,
                    'name' => $request->notice_custom_name,
                    'no_of_days' => $request->no_of_days,
                    'content' => $request->notice_template_content
        ]);
        if ($result) {
            return redirect('admin/notice_templates')->with('success', 'Template Successfully Created');
        } else {
            return redirect()->back()->withErrors('Their is problem in creating template.');
        }
    }

    /**
     * Function is used to delete the notice template
     * 
     * @param  $template_id
     */
    public function destroyNoticeTemplate(Request $request) {

        $result = NoticeTemplate::where('id', $request->noticetemplate_id)->delete();

        if ($result) {
            return redirect('admin/notice_templates')->with('success', 'Template Successfully Deleted');
        } else {
            return redirect()->back()->withErrors('Problem in record deleting');
        }
    }

    /**
     * Function is used to edit template view
     * 
     * @param  $template_id
     */
    public function editNoticeTemplate($template_id) {
        $data['notice_template'] = NoticeTemplate::where('id', $template_id)->first();

        $data['states'] = \App\Models\State::select('id', 'name')->where('status', 1)->orderBy('name')->get();

        $data['notices'] = \App\Models\Notice::select('id', 'name')->get();

        return view('admin.notice_template.edit', $data);
    }

    /**
     * Function is used to updated template details
     * 
     * @param Request $request
     */
    public function updateNoticeTemplateDetails(Request $request) {
        $rules = ['notice_name' => 'required',
            'state' => 'required| exists:states,id',
            'notice_custom_name' => 'required',
            'no_of_days' => 'required|integer',
            'notice_template_content' => 'required'
        ];
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }

        $notice_template_obj = NoticeTemplate::where('id', $request->template_id)->first();

        $notice_template_obj->state_id = $request->state;
        $notice_template_obj->is_parent_work_order = ($request->is_parent_work_order) ? $request->is_parent_work_order : 0;
        $notice_template_obj->notice_id = $request->notice_name;
        $notice_template_obj->name = $request->notice_custom_name;
        $notice_template_obj->no_of_days = $request->no_of_days;
        $notice_template_obj->content = $request->notice_template_content;

        $result = $notice_template_obj->save();
        if ($result) {
            return redirect('admin/notice_templates')->with('success', 'Template Successfully Updated');
        } else {
            return redirect()->back()->withErrors('Their is problem in updating template.');
        }
    }

    public function getNotices(Request $request) {
        $state = $request->state;

        $notices = \App\Models\Notice::select('id', 'name')
                ->where('notices.state_id', '=', $state)
                ->get();

        /* if(isEmpty($contacts)){

          } */
        return $notices;
    }

}
