<?php

namespace App\Http\Controllers\Admin\Backpack\PermissionManager\CRUD;

class PermissionCrudController extends \Backpack\PermissionManager\app\Http\Controllers\PermissionCrudController
{
	public function setup()
	{
		parent::setup();

		$this->crud->addField([
			'name'  => 'name',
			'label' => trans('backpack::permissionmanager.name'),
			'type'  => 'text',
			'attributes' => ['readonly' => 'readonly']
		]);

		// Remove Action Buttons
		if (!config('backpack.permissionmanager.allow_permission_update') && !config('backpack.permissionmanager.allow_permission_delete'))
			$this->crud->removeAllButtonsFromStack('line');
	}
}
