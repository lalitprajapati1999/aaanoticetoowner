<?php

namespace App\Http\Controllers\Admin\Backpack\PermissionManager\CRUD;
use Backpack\CRUD\app\Http\Requests\CrudRequest;
use Backpack\PermissionManager\app\Http\Requests\UserStoreCrudRequest as StoreRequest;
use Backpack\PermissionManager\app\Http\Requests\UserUpdateCrudRequest as UpdateRequest;

class UserCrudController extends \Backpack\PermissionManager\app\Http\Controllers\UserCrudController
{
	public function setup()
	{
		parent::setup();

		$this->crud->setEntityNameStrings(trans('backpack::permissionmanager.employee'), trans('backpack::permissionmanager.employees'));

		// Remove roles and permission section for admin and account managers
		if (\Auth::user()->hasRole(['admin', 'account-manager'])) {

			$this->crud->removeField('roles_and_permissions');
			$this->crud->removeColumn('permissions');

			$this->crud->addClause('whereHas', 'roles', function($query) {
				if (\Auth::user()->hasRole('admin') && \Auth::user()->hasRole('account-manager'))
					$query->adminAccountManager();

				elseif (\Auth::user()->hasRole('admin'))
					$query->admin();

				elseif (\Auth::user()->hasRole('account-manager'))
					$query->accountManager();
			});

			$role_model = config('laravel-permission.models.role');
			$this->crud->addField([
				'label'     => trans('backpack::permissionmanager.roles'),
				'type'      => 'checklist_scope',
				'name'      => 'roles',
				'entity'    => 'roles',
				'attribute' => 'name',
				'model'     => $role_model,
				'pivot'     => true,
				'scope' => (\Auth::user()->hasRole('admin') && \Auth::user()->hasRole('account-manager'))?'adminAccountManager':((\Auth::user()->hasRole('admin'))?'admin':'accountManager'),
				'hint' => '<p class="text-red"><i class="icon fa fa-warning"></i>&nbsp;&nbsp;Roles may associated with crucial access, Be careful !!</p>'
			]);

			$this->crud->afterField('password_confirmation');

			$this->crud->addField([   // CustomHTML
				'name' => 'title',
				'type' => 'custom_html',
				'value' => '<h4 style="background-color:#f7f7f7; font-size: 18px; text-align: center; padding: 7px 10px; margin-top: 0;">Additional Account Manager Info</h4>'
			]);

			$this->crud->afterField('roles');

			$this->crud->addField([
				'name'  => 'contact_number',
				'label' => trans('backpack::permissionmanager.contact_number'),
				'type'  => 'text',
			]);

			$this->crud->afterField('separator');

			$this->crud->addField([
				'name'  => 'emergency_contact',
				'label' => trans('backpack::permissionmanager.emergency_contact'),
				'type'  => 'text',
			]);

			$this->crud->afterField('contact_number');

			$this->crud->addField([
				'name' => 'mailing_access', // the name of the db column
				'label' => 'Allow access on mailing', // the input label
				'type' => 'toggle',
				'inline' => true,
				'options' => [ // the key will be stored in the db, the value will be shown as label; 
					1 => "Yes",
					0 => "No"
				],
				'hide_when' => [
					0 => ['pin'],
				],
				'default' => 0
			]);

			$this->crud->afterField('emergency_contact');

			$this->crud->addField([
				'name'  => 'pin',
				'label' => trans('backpack::permissionmanager.pin'),
				'type'  => 'text',
			]);

			$this->crud->afterField('mailing_access');
		}
	}

    /**
     * Store a newly created resource in the database.
     *
     * @param StoreRequest $request - type injection used for validation using Requests
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreRequest $request)
    {
        $this->handleMailingAccessInput($request);

        return parent::store($request);
    }

    /**
     * Update the specified resource in the database.
     *
     * @param UpdateRequest $request - type injection used for validation using Requests
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateRequest $request)
    {
        $this->handleMailingAccessInput($request);

        return parent::update($request);
    }

    /**
     * Handle password input fields.
     *
     * @param CrudRequest $request
     */
    protected function handleMailingAccessInput(CrudRequest $request)
    {
		// Set Mailing Permission.
		$permissions = []; $permissions_show = [];
		if ($request->input('mailing_access')) {
			# Assign mailing access permission to User

			// Get Mailing permission id
			$id = config('laravel-permission.models.permission')::findByName('admin-mailing')->id;

			$permissions[] = $id;
			$permissions_show[] = $id;
			$permissions = $request->request->has('permissions')?$request->get('permissions')+$permissions:$permissions;
			$permissions_show = $request->request->has('permissions_show')?$request->get('permissions_show')+$permissions_show:$permissions_show;
		}

		$request->request->set('permissions', $permissions);
		$request->request->set('permissions_show', $permissions_show);

		$request->request->remove('mailing_access');
	}
}
