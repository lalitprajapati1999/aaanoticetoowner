<?php

namespace App\Http\Controllers\Admin\Backpack\PermissionManager\CRUD;

class RoleCrudController extends \Backpack\PermissionManager\app\Http\Controllers\RoleCrudController
{
	public function setup()
	{
		parent::setup();

		$this->crud->addField([
			'name'  => 'name',
			'label' => trans('backpack::permissionmanager.name'),
			'type'  => 'text',
			'attributes' => ['readonly' => 'readonly']
		]);

		// Remove Action Buttons Ref. https://github.com/Laravel-Backpack/CRUD/issues/303
		if (!config('backpack.permissionmanager.allow_role_update') && !config('backpack.permissionmanager.allow_role_delete'))
			$this->crud->removeAllButtonsFromStack('line');
	}
}
