<?php


namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\LienBlog;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Models\RoleUsers;
use Illuminate\Support\Facades\Redirect;
/**
 * Description of EmployeesController
 *
 * @author neosoft
 */
class LienBlogController extends Controller{
    //put your code here
    
    public function index(){
        $data = [];
         
       return view('admin.lienblog.list', $data);
    }
    /*
     * Function is used for listing and filter by employee name
     */
    public function getLienblogFilter(Request $request){
         $lienblog = \DB::table('notice_lienblog')->select('id','question','answer','status')->orderBy('id','DESC')->get();
    
        return Datatables::of($lienblog)
                        ->addColumn('action', function($lienblog) {
                            return view('admin.lienblog.actions', compact('lienblog'))->render();
                        })
                        // ->filter(function ($instance) use ($request) {
                        //     if (($request->has('content')) && ($request->get('content') != NULL)) {
                        //         $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                        //             return Str::contains($row['content'], $request->get('content')) ? true : false;
                        //         });
                        //     }
                        // })
                        ->make(true);
    }
    /*
     * Function is used to view create employee view
     */
    public function createLienBlog(){
          return view('admin.lienblog.create');
    }
    /*
     * Function is used to register employee
     */
    public function storeLienBlog(Request $request){
        
       $rules = ['question' => 'required',
            'answer' => 'required',
        ];
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
       
        $lienblog = LienBlog::create([
            'question' => $request->question,
            'answer'=>$request->answer,
        ]);
        
        //store in role_users
        // if($request->role == 2){
        //  $user->assignRole('admin');
        // }else{
        //     $user->assignRole('account-manager');
        // }
        
       if ($lienblog) {
               return redirect::to('admin/lien_blog')->with('success','Lien Blog successfully registered');
       } else {
        return redirect::back()->with('error','Problem in Tmployee creation');
        }
    }
    
   
    /*
     * Function is used to edit employee view
     */
    public function editLienBlog($lienblog_id){
        $data = [];
        
        $data['lienblog'] = LienBlog::find($lienblog_id);
        
        return view('admin.lienblog.edit',$data);
    }
    /*
     * Function is used to update details
     */
    public function updateLienBlog(Request $request){
         
          $rules = ['question' => 'required',
            'answer' => 'required',
        ];
        $validator = Validator::make(Input::all(), $rules);
      
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
       
        $lienblog = LienBlog::find($request->lienblog_id);
        $lienblog->question = $request->question;
        $lienblog->answer = $request->answer;
        $lienblog->save();
      
       if ($lienblog) { 
               return redirect::to('admin/lien_blog')->with('success','Lien Blog successfully updated');
       } else {
        return redirect::back()->with('error','Problem in Lien Blog updation');
        }
    }
    /*
     * Function is used to view Employee
     */
    public function viewLienBlog($lienblog_id){
        $data['lienblog'] = LienBlog::find($lienblog_id);
        
        // $data['employee_role'] = RoleUsers::leftjoin('roles','roles.id','role_users.role_id')
        //                                     ->where('user_id','=',$employee_id)
        //                                     ->select('roles.name')
        //                                     ->first();
        
        return view('admin.lienblog.view',$data);
    }

    public function changeStatus(Request $request)
    { 

        $id = $request->lienblog_id;
        $status = $request->status;
        $result = LienBlog::find($id);
       
        $result->status = $status;
        $result->save();
        
        return response()->json($result);
    }
    
    /**
     * Function is used to delete lien blog
     */
    public function deleteLienBlog(Request $request){
      
          $user_notes = \DB::table('notice_lienblog')
                            ->where('id','=',$request->lienblog_id);
          
          $result = $user_notes->delete();
          
          
          return redirect::to('admin/lien_blog')->with('success', 'Lien blog successfully deleted.');
    }
}
