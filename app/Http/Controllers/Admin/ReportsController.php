<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use PDF;
class ReportsController extends Controller {

    public static function getAccountManagerReports() {

        $data['account_managers'] = \App\Models\User::select('id', 'name')->join('role_users', 'role_users.user_id', 'users.id')
                ->where('role_users.role_id', 3)
                ->get();

        return view('admin.reports.reports_data', $data);
    }

    /*
     * Function is used for listing and filter reports
     */

    public static function getReportsCustomFilter(Request $request) {
        $current_date = "'" . date('Y-m-d') . "'";

        $data = \App\Models\WorkOrder::leftjoin('users', 'users.id', '=', 'work_orders.account_manager_id')
                ->select('work_orders.account_manager_id', 'users.name')
                ->groupBy('work_orders.account_manager_id');

        if ($request->acc_name != 'all') {
            $data = $data->where('work_orders.account_manager_id', '=', $request->acc_name);
        }

        if ($request->report_type == 'daily') {
            $data = $data->whereDate('work_orders.created_at', '=', date('Y-m-d'));
        } elseif ($request->report_type == 'weekly') {
            $data = $data->whereDate('work_orders.created_at', '>=', date('Y-m-d', strtotime($request->from_date)))
                    ->whereDate('work_orders.created_at', '<=', date('Y-m-d', strtotime($request->to_date)));
        } elseif ($request->report_type == 'monthly') {
            $data = $data->whereMonth('work_orders.created_at', '=', date($request->month))
                    ->whereYear('work_orders.created_at', '=', date($request->year));
        } elseif ($request->report_type == 'yearly') {
            $data = $data->whereYear('work_orders.created_at', '=', date($request->year_only));
        }
        $query_result = $data->get();
//        dd($query_result);
        foreach ($query_result As $d) {
            //Request
            $requestCount = \App\Models\WorkOrder::where('status', '=', 1)
                    ->where('account_manager_id', '=', $d->account_manager_id);

            if ($request->report_type == 'daily') {
                $requestCount = $requestCount->whereDate('work_orders.created_at', '=', date('Y-m-d'));
            } elseif ($request->report_type == 'weekly') {
                $requestCount = $requestCount->whereDate('work_orders.created_at', '>=', date('Y-m-d', strtotime($request->from_date)))
                        ->whereDate('work_orders.created_at', '<=', date('Y-m-d', strtotime($request->to_date)));
            } elseif ($request->report_type == 'monthly') {
                $requestCount = $requestCount->whereMonth('work_orders.created_at', '=', date($request->month))
                        ->whereYear('work_orders.created_at', '=', date($request->year));
            } elseif ($request->report_type == 'yearly') {
                $requestCount = $requestCount->whereYear('work_orders.created_at', '=', date($request->year_only));
            }
            $requestCountRes = $requestCount->count();
            $d->request_count = $requestCountRes;

            //Processing
            $processingCount = \App\Models\WorkOrder::where('status', '=', 2)
                    ->where('account_manager_id', '=', $d->account_manager_id);
            if ($request->report_type == 'daily') {
                $processingCount = $processingCount->whereDate('work_orders.created_at', '=', date('Y-m-d'));
            } elseif ($request->report_type == 'weekly') {
                $processingCount = $processingCount->whereDate('work_orders.created_at', '>=', date('Y-m-d', strtotime($request->from_date)))
                        ->whereDate('work_orders.created_at', '<=', date('Y-m-d', strtotime($request->to_date)));
            } elseif ($request->report_type == 'monthly') {
                $processingCount = $processingCount->whereMonth('work_orders.created_at', '=', date($request->month))
                        ->whereYear('work_orders.created_at', '=', date($request->year));
            } elseif ($request->report_type == 'yearly') {
                $processingCount = $processingCount->whereYear('work_orders.created_at', '=', date($request->year_only));
            }
            $processingCountRes = $processingCount->count();
            $d->processing_count = $processingCountRes;

            //Completed
            $completedCount = \App\Models\WorkOrder::where('status', '=', 6)
                    ->where('account_manager_id', '=', $d->account_manager_id);
            if ($request->report_type == 'daily') {
                $completedCount = $completedCount->whereDate('work_orders.created_at', '=', date('Y-m-d'));
            } elseif ($request->report_type == 'weekly') {
                $completedCount = $completedCount->whereDate('work_orders.created_at', '>=', date('Y-m-d', strtotime($request->from_date)))
                        ->whereDate('work_orders.created_at', '<=', date('Y-m-d', strtotime($request->to_date)));
            } elseif ($request->report_type == 'monthly') {
                $completedCount = $completedCount->whereMonth('work_orders.created_at', '=', date($request->month))
                        ->whereYear('work_orders.created_at', '=', date($request->year));
            } elseif ($request->report_type == 'yearly') {
                $completedCount = $completedCount->whereYear('work_orders.created_at', '=', date($request->year_only));
            }
            $completedCountRes = $completedCount->count();
            $d->completed_count = $completedCountRes;

            //Mailed
            $mailedCount = \App\Models\WorkOrder::where('status', '=', 5)
                    ->where('account_manager_id', '=', $d->account_manager_id);
            if ($request->report_type == 'daily') {
                $mailedCount = $mailedCount->whereDate('work_orders.created_at', '=', date('Y-m-d'));
            } elseif ($request->report_type == 'weekly') {
                $mailedCount = $mailedCount->whereDate('work_orders.created_at', '>=', date('Y-m-d', strtotime($request->from_date)))
                        ->whereDate('work_orders.created_at', '<=', date('Y-m-d', strtotime($request->to_date)));
            } elseif ($request->report_type == 'monthly') {
                $mailedCount = $mailedCount->whereMonth('work_orders.created_at', '=', date($request->month))
                        ->whereYear('work_orders.created_at', '=', date($request->year));
            } elseif ($request->report_type == 'yearly') {
                $mailedCount = $mailedCount->whereYear('work_orders.created_at', '=', date($request->year_only));
            }
            $mailedCountRes = $mailedCount->count();
            $d->mailed_count = $mailedCountRes;

            //Cancelled
            $cancelledCount = \App\Models\WorkOrder::where('status', '=', 7)
                    ->where('account_manager_id', '=', $d->account_manager_id);
            if ($request->report_type == 'daily') {
                $cancelledCount = $cancelledCount->whereDate('work_orders.created_at', '=', date('Y-m-d'));
            } elseif ($request->report_type == 'weekly') {
                $cancelledCount = $cancelledCount->whereDate('work_orders.created_at', '>=', date('Y-m-d', strtotime($request->from_date)))
                        ->whereDate('work_orders.created_at', '<=', date('Y-m-d', strtotime($request->to_date)));
            } elseif ($request->report_type == 'monthly') {
                $cancelledCount = $cancelledCount->whereMonth('work_orders.created_at', '=', date($request->month))
                        ->whereYear('work_orders.created_at', '=', date($request->year));
            } elseif ($request->report_type == 'yearly') {
                $cancelledCount = $cancelledCount->whereYear('work_orders.created_at', '=', date($request->year_only));
            }
            $cancelledCountRes = $cancelledCount->count();

            $d->cancelled_count = $cancelledCountRes;

            //Goal
            $d->goal = $d->request_count + $d->processing_count + $d->completed_count + $d->mailed_count +
                    $d->cancelled_count;

            //Total
            $d->total = $d->request_count + $d->processing_count + $d->completed_count + $d->mailed_count +
                    $d->cancelled_count;
        }
        return Datatables::of($query_result)
                        ->make(true);
    }

    /*
     * Function is used to send mail
     */

    public static function sendMailExcel(Request $request) {
       
        $email = $request->email;

        if (!isset($request['report_type'])) {
            $request['report_type'] = 'daily';
        }
        if (!isset($request['account_manager'])) {
            $request['account_manager'] = 'all';
        }

        $user = ReportsController::getFilterData($request['report_type'], $request['account_manager'], $request['from_date'], $request['to_date'], $request['month'], $request['year'])->toArray();

      
        $data = [];
        foreach ($user as $user_val) {

            $data[] = [$user_val['name'], $user_val['request_count'], $user_val['processing_count'], $user_val['completed_count'], $user_val['mailed_count'], $user_val['cancelled_count'], $user_val['goal'], $user_val['total']];
        }
      
//        $file_name = "report";
//        return \Excel::create($file_name, function($excel) use ($data) {
//
//                    $excel->sheet('sheet name', function($sheet) use ($data) {
//                        $headings = array('Account Manager', 'Request Count', 'Processing Count', 'Completed Count', 'Mailed Count', 'Cancelled Count', 'Goal', 'Total');
//                        $sheet->prependRow(1, $headings);
//                        $sheet->rows($data, true);
//                    });
//                })->store('csv', public_path('reports'));


        $maildata = [
            'to' => $email,
            'excel' => public_path() . '/excel/' . 'Notice_to_Owner.xlsx'
        ];
//
        $result = Mail::send('admin.reports.excel_email', $maildata, function ($message)use($mdata) {
                    $message->to($mdata['to']);
                    $message->subject('Reports Excel');
                    $message->attach($mdata['excel']);
                });


        return redirect::to('admin/reports')->with('success', 'Mail successfully sent.');
    }

    public static function exportExcel(Request $request) {


        if (!isset($request['report_type'])) {
            $request['report_type'] = 'daily';
        }
        if (!isset($request['account_manager'])) {
            $request['account_manager'] = 'all';
        }

        $user = ReportsController::getFilterData($request['report_type'], $request['account_manager'], $request['from_date'], $request['to_date'], $request['month'], $request['year'])->toArray();

      
        $data = [];
        foreach ($user as $user_val) {

            $data[] = [$user_val['name'], $user_val['request_count'], $user_val['processing_count'], $user_val['completed_count'], $user_val['mailed_count'], $user_val['cancelled_count'], $user_val['goal'], $user_val['total']];
        }
        if ($request['reporttype'] == 'reportexceldata') {
            $file_name = "report";
            return \Excel::create($file_name, function($excel) use ($data) {

                        $excel->sheet('sheet name', function($sheet) use ($data) {
                            $headings = array('Account Manager', 'Request Count', 'Processing Count', 'Completed Count', 'Mailed Count', 'Cancelled Count', 'Goal', 'Total');
                            $sheet->prependRow(1, $headings);
                            $sheet->rows($data, true);
                        });
                    })->download('csv');
        } else {
$html="<table class='table table-striped' id='report-data' datatable='' width='100%' cellspacing='0'   data-scroll-x='true' scroll-collapse='false'>
            <thead class='thead-dark'>";
                $html.="<tr>
                    <th scope='col' class='action'>Account Manager</th>
                    <th scope='col' class='action'>Request</th>
                    <th scope='col' class='action'>Processing</th>
                    <th scope='col' class='action'>Completed</th>
                    <th scope='col' class='action'>Mailed</th>
                    <th scope='col' class='action'>Cancelled</th>
                    <th scope='col' class='action'>Goal</th>
                    <th scope='col' class='action'>Total</th>
                </tr>
            </thead>
            <tbody>";

                if(isset($user) && !empty($user))
                    {   foreach($user AS $k=>$val)
                        {       
                            $val['name'] = (isset($val['name']) && $val['name']!= NULL)?$val['name']:'N/A';
                            $val['request_count'] = (isset($val['request_count']))?$val['request_count']:'0';
                            $val['processing_count'] = (isset($val['processing_count']))?$val['processing_count']:'0';
                            $val['completed_count'] = (isset($val['completed_count']))?$val['completed_count']:'0';
                            $val['mailed_count'] = (isset($val['mailed_count']))?$val['mailed_count']:'0';
                            $val['cancelled_count'] = (isset($val['cancelled_count']))?$val['cancelled_count']:'0';
                            $val['goal'] = (isset($val['goal']))?$val['goal']:'0';
                            $val['total'] = (isset($val['total']))?$val['total']:'0';
                            $html.="<tr>";
                                $html.="<td>". $val['name']."</td>";
                                $html.="<td>". $val['request_count']."</td>";
                                $html.="<td>". $val['processing_count']."</td>";
                                $html.="<td>". $val['completed_count']."</td>";
                                $html.="<td>". $val['mailed_count']."</td>";
                                $html.="<td>". $val['cancelled_count']."</td>";
                                $html.="<td>". $val['goal']."</td>";
                                $html.="<td>". $val['total']."</td>";
                              
                            $html.="</tr>";
                        }
                    }
            $html.="</tbody>
        </table>";
        //echo $html;
        $pdf = PDF::loadHTML($html)->setPaper('a4'); //->setPaper('letter', 'portrait');



            $file_name = 'reports' . date("ymd") . '.pdf';
             if (!file_exists(public_path() . '/pdf/reports/')) {
                $response = mkdir(public_path() . '/pdf/reports/', 0777, true);
            }
            if (!file_exists(public_path() .'/pdf/reports/' . $file_name)) {
                $pdf->save(public_path() .'/pdf/reports/' . $file_name);

            } else {
                unlink(public_path() . '/pdf/reports/' . $file_name);
                $pdf->save(public_path() .'/pdf/reports/' . $file_name);

            }
            //$pdf->save(public_path() .'/pdf/reports/' . $file_name);
            $mypdf = asset('/') .'pdf/reports/' . $file_name;

            return response()->json(['status' => 'success', 'message' => $mypdf]);


        }
        // return (new \App\Exports\ReportExport($report_type, $account_manager, $from_date, $to_date, $month, $year, $year_only))->download('reports.xlsx');
    }

    public static function getFilterData($report_type = '', $account_manager = '', $from_date = '', $to_date = '', $month = '', $year = '') {
        $current_date = "'" . date('Y-m-d') . "'";

        $data = \App\Models\WorkOrder::leftjoin('users', 'users.id', '=', 'work_orders.account_manager_id')
                ->select('work_orders.account_manager_id', 'users.name')
                ->groupBy('work_orders.account_manager_id');

        if ($account_manager != 'all') {
            $data = $data->where('work_orders.account_manager_id', '=', $account_manager);
        }

        if ($report_type == 'daily') {
            $data = $data->whereDate('work_orders.created_at', '=', date('Y-m-d'));
        } elseif ($report_type == 'weekly') {
            $data = $data->whereDate('work_orders.created_at', '>=', date('Y-m-d', strtotime($from_date)))
                    ->whereDate('work_orders.created_at', '<=', date('Y-m-d', strtotime($to_date)));
        } elseif ($report_type == 'monthly') {
            $data = $data->whereMonth('work_orders.created_at', '=', date($month))
                    ->whereYear('work_orders.created_at', '=', date($year));
        } elseif ($report_type == 'yearly') {
            $data = $data->whereYear('work_orders.created_at', '=', date($year));
        }
        $query_result = $data->get();

        foreach ($query_result As $d) {
            //Request
            $requestCount = \App\Models\WorkOrder::where('status', '=', 1)
                    ->where('account_manager_id', '=', $d->account_manager_id);

            if ($report_type == 'daily') {
                $requestCount = $requestCount->whereDate('work_orders.created_at', '=', date('Y-m-d'));
            } elseif ($report_type == 'weekly') {
                $requestCount = $requestCount->whereDate('work_orders.created_at', '>=', date('Y-m-d', strtotime($from_date)))
                        ->whereDate('work_orders.created_at', '<=', date('Y-m-d', strtotime($to_date)));
            } elseif ($report_type == 'monthly') {
                $requestCount = $requestCount->whereMonth('work_orders.created_at', '=', date($month))
                        ->whereYear('work_orders.created_at', '=', date($year));
            } elseif ($report_type == 'yearly') {
                $requestCount = $requestCount->whereYear('work_orders.created_at', '=', date($year));
            }
            $requestCountRes = $requestCount->count();

            $d->request_count = $requestCountRes;

            //Processing
            $processingCount = \App\Models\WorkOrder::where('status', '=', 2)
                    ->where('account_manager_id', '=', $d->account_manager_id);
            if ($report_type == 'daily') {
                $processingCount = $processingCount->whereDate('work_orders.created_at', '=', date('Y-m-d'));
            } elseif ($report_type == 'weekly') {
                $processingCount = $processingCount->whereDate('work_orders.created_at', '>=', date('Y-m-d', strtotime($from_date)))
                        ->whereDate('work_orders.created_at', '<=', date('Y-m-d', strtotime($to_date)));
            } elseif ($report_type == 'monthly') {
                $processingCount = $processingCount->whereMonth('work_orders.created_at', '=', date($month))
                        ->whereYear('work_orders.created_at', '=', date($year));
            } elseif ($report_type == 'yearly') {
                $processingCount = $processingCount->whereYear('work_orders.created_at', '=', date($year));
            }
            $processingCountRes = $processingCount->count();
            $d->processing_count = $processingCountRes;

            //Completed
            $completedCount = \App\Models\WorkOrder::where('status', '=', 6)
                    ->where('account_manager_id', '=', $d->account_manager_id);
            if ($report_type == 'daily') {
                $completedCount = $completedCount->whereDate('work_orders.created_at', '=', date('Y-m-d'));
            } elseif ($report_type == 'weekly') {
                $completedCount = $completedCount->whereDate('work_orders.created_at', '>=', date('Y-m-d', strtotime($from_date)))
                        ->whereDate('work_orders.created_at', '<=', date('Y-m-d', strtotime($to_date)));
            } elseif ($report_type == 'monthly') {
                $completedCount = $completedCount->whereMonth('work_orders.created_at', '=', date($month))
                        ->whereYear('work_orders.created_at', '=', date($year));
            } elseif ($report_type == 'yearly') {
                $completedCount = $completedCount->whereYear('work_orders.created_at', '=', date($year));
            }
            $completedCountRes = $completedCount->count();
            $d->completed_count = $completedCountRes;

            //Mailed
            $mailedCount = \App\Models\WorkOrder::where('status', '=', 5)
                    ->where('account_manager_id', '=', $d->account_manager_id);
            if ($report_type == 'daily') {
                $mailedCount = $mailedCount->whereDate('work_orders.created_at', '=', date('Y-m-d'));
            } elseif ($report_type == 'weekly') {
                $mailedCount = $mailedCount->whereDate('work_orders.created_at', '>=', date('Y-m-d', strtotime($from_date)))
                        ->whereDate('work_orders.created_at', '<=', date('Y-m-d', strtotime($to_date)));
            } elseif ($report_type == 'monthly') {
                $mailedCount = $mailedCount->whereMonth('work_orders.created_at', '=', date($month))
                        ->whereYear('work_orders.created_at', '=', date($year));
            } elseif ($report_type == 'yearly') {
                $mailedCount = $mailedCount->whereYear('work_orders.created_at', '=', date($year));
            }
            $mailedCountRes = $mailedCount->count();
            $d->mailed_count = $mailedCountRes;

            //Cancelled
            $cancelledCount = \App\Models\WorkOrder::where('status', '=', 7)
                    ->where('account_manager_id', '=', $d->account_manager_id);
            if ($report_type == 'daily') {
                $cancelledCount = $cancelledCount->whereDate('work_orders.created_at', '=', date('Y-m-d'));
            } elseif ($report_type == 'weekly') {
                $cancelledCount = $cancelledCount->whereDate('work_orders.created_at', '>=', date('Y-m-d', strtotime($from_date)))
                        ->whereDate('work_orders.created_at', '<=', date('Y-m-d', strtotime($to_date)));
            } elseif ($report_type == 'monthly') {
                $cancelledCount = $cancelledCount->whereMonth('work_orders.created_at', '=', date($month))
                        ->whereYear('work_orders.created_at', '=', date($year));
            } elseif ($report_type == 'yearly') {
                $cancelledCount = $cancelledCount->whereYear('work_orders.created_at', '=', date($year));
            }
            $cancelledCountRes = $cancelledCount->count();

            $d->cancelled_count = $cancelledCountRes;

            //Goal
            $d->goal = $d->request_count + $d->processing_count + $d->completed_count + $d->mailed_count +
                    $d->cancelled_count;

            //Total
            $d->total = $d->request_count + $d->processing_count + $d->completed_count + $d->mailed_count +
                    $d->cancelled_count;
        }
        return $query_result;
    }

}
