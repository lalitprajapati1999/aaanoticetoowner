<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
//use DataTables;
use Illuminate\Support\Facades\DB;


class CitiesController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('admin.city.list');
    }

    // public function getCity() {
    //     $model = \App\Models\City::query();

    //     return DataTables::eloquent($model)
    //         ->editColumn('states.name', function($cities) {
    //             return $cities->states->name;
    //         })
    //         ->addColumn('action', function($cities) {
    //             return view('admin.city.actions', compact('cities'))->render();
    //         })
    //         ->rawColumns(['action'])
    //         ->make(true);
    // }
    
      public function getCity(Request $request) {
       $model = \App\Models\City::leftjoin('states','states.id','=','cities.state_id')
                ->select('cities.*','states.name as states')
                ->where('cities.name', 'like', '%' . $request->city_name . "%")
                ->Orwhere('cities.county', 'like', '%' . $request->city_name . "%")
                ->Orwhere('cities.zip_code', 'like', '%' . $request->city_name . "%")
                ->Orwhere('states.name', 'like', '%' . $request->city_name . "%")
                ->get();
        $cities=$model;
        return Datatables::of($cities)
            ->addColumn('action', function($cities) {
                return view('admin.city.actions', compact('cities'))->render();
            })
            ->rawColumns(['action'])
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $states = \App\Models\State::select()->where('status', 1)->orderBy('name')->get();
        return view('admin.city.create', compact('states'));
    }

    // public function changestatus(Request $request) {
    //     $id = $request->input('city_id');
    //     $status = $request->input('status');
    //     $result = \App\Models\City::find($id);
    //     // $result->status = $status;
    //     $result->save();
    //     return response()->json($result);
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $valid_data = [
            'name' => 'required',
            'county' => 'required',
            'state_id' => 'required'
        ];
        $validatedData = $request->validate($valid_data);

        try {
            $cities = new \App\Models\City();
            $cities->name = $request->name;
            $cities->county = $request->county;
            $cities->state_id = $request->state_id;
            $cities->zip_code = $request->zip_code;

            $city = $cities->save();
            return redirect('admin/cities')->with('success', '
            City has been added successfully');
        } catch (\Exception $e) {
            $e->getMessage();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $cities = \App\Models\City::with('states')->where('id',$id)->get()->first();
        $states = \App\Models\State::pluck('name', 'id')->toArray();
        return view('admin.city.edit',compact('cities','states'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $result = \App\Models\City::find($id);
        $result->zip_code = $request->zip_code;
        $result->save();
        return redirect('admin/cities');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }
    public function ZipCodeLookup(Request $request){

        $search=$request->zipcode;
        if ($search == '') {
            return response()->json(array(array("value" => "", "label" => "", "id" => "")));
        } else {
            if ($request->cityName) {
                $name = \App\Models\City::where('id', $request->cityName)->first();
                $result = DB::table('zipcodecity')->orderby('name', 'asc')
                    ->select('id', 'zip_code', 'name', 'state', 'county')
                    ->where('name', $name->name)
                    ->where('zip_code', 'like', '%' . $search . '%')->limit(100)->get();
            } else {
                $result = DB::table('zipcodecity')->orderby('name', 'asc')->select('id', 'zip_code', 'name', 'state', 'county')->where('zip_code', 'like', '%' . $search . '%')->limit(100)->get();
            }
        }
        $response = array();
        foreach($result as $results){
           $lable=$results->zip_code .','. $results->name .','.$results->state;
           $response[] = array("value"=>$results->zip_code,"label"=>$lable,"id"=>$results->id);
        }
        // dd($result);
        return response()->json($response);
    }
    public function getStateName(Request $request){
        $states = \App\Models\State::select('*')->where('abbr', $request->statename)->first();
        return response()->json($states);
    }

}

?>