<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Models\RoleUsers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use App\Mail\EmployeeMail;
use App\Mail\AdminMail;

/**
 * Description of EmployeesController
 *
 * @author neosoft
 */
class EmployeesController extends Controller {

    //put your code here

    public function index() {
        $data = [];

        return view('admin.employees.list', $data);
    }

    /*
     * Function is used for listing and filter by employee name
     */

    public function getEmployeesFilter(Request $request) {
        $employees = \DB::table('users')
                ->leftjoin('role_users', 'role_users.user_id', '=', 'users.id')
                ->select('users.name', 'users.id', 'role_users.role_id', 'users.status','users.email','users.created_at')
                ->whereIn('role_users.role_id', ['2', '3'])
                ->orderBy('users.id','DESC')
                ->get();
            foreach ($employees AS $employee) {
                $employee->date_applied=date('M d,Y', strtotime($employee->created_at));
                $cyo = \App\Models\Transaction::where('user_id',$employee->id)->doesntExist();
                if($cyo){
                    $employee->cyo='No';
                }
                else{
                        $employee->cyo='Yes';
                    }

            }

                //dd($employees);
        return Datatables::of($employees)
                        ->addColumn('action', function($employee) {
                            return view('admin.employees.actions', compact('employee'))->render();
                        })
                        ->filter(function ($instance) use ($request) {
                            if (($request->has('employee_name')) && ($request->get('employee_name') != NULL)) {
                                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                                    return Str::contains($row['name'], $request->get('employee_name')) ? true : false;
                                });
                            }
                        })
                        ->make(true);
    }

    /*
     * Function is used to view create employee view
     */

    public function createEmployee() {
        return view('admin.employees.create');
    }

    /*
     * Function is used to register employee
     */

    public function storeEmployee(Request $request) {
      
        $rules = ['name' => 'required|regex:/^[a-zA-Z ]+$/u',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            'retype_password' => 'required|required_with:password|same:password',
            'role' => 'required',
            'contact_number' => 'required',
            'emergency_contact_number' => 'required',
            'mailing_access' => 'required_if:role,==,3',
            'create_pin' => 'required_if:mailing_access,==,yes',
            'admin_create_pin'=>'required_if:role,==,2',
        ];
        $validator = Validator::make(Input::all(), $rules, [
                    'create_pin.digits' => 'The create pin must be 4 digits.',
            'admin_create_pin.required_if'=>'The mailing pin is required'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        if(isset($request->admin_create_pin)){
            $request->create_pin = $request->admin_create_pin;
        }
        
        $user = User::create([
                    'name' => $request->name,
                    'email' => $request->email,
                    'password' => bcrypt($request->password),
                    'password_text' => $request->password,
                    'contact_number' => $request->contact_number,
                    'emergency_contact' => $request->emergency_contact_number,
                    'pin' => $request->create_pin,
                    'status' => 1
        ]);

        //store in role_users
        if ($request->role == 2) {
            $user->assignRole('admin');
        } else {
            $user->assignRole('account-manager');
        }
         if ($request->role == 2) {
             $details = [
                'name' => $user->name,
                'email' => $user->email,
                'password' => $user->password_text
            ];
              Mail::to($request->email)->send(new AdminMail($details));
         }else{
            $details = [
                'name' => $user->name,
                'email' => $user->email,
                'password' => $user->password_text,
                'pin' => $user->pin
            ];
              Mail::to($request->email)->send(new EmployeeMail($details));
         }
      

        if ($user) {
            return redirect::to('admin/employees')->with('success', 'Employee successfully registered');
        } else {
            return redirect::back()->with('error', 'Problem in employee creation');
        }
    }

    /*
     * Function is used to change employee status
     */

    public function changestatus(Request $request) {
        $id = $request->input('user_id');
        $status = $request->input('status');

        $result = \App\Models\User::find($id);

        $result->status = $status;
        $result->save();

//        return response()->json($result);
        return redirect::back()->with('success', 'Employee status successfully changed');
    }

    /*
     * Function is used to add note for employee
     */

    public function addNote(Request $request) {
        $rules = [
            'user_note' => 'required',
        ];

        $validator = Validator::make(Input::all(), $rules);
         if ($validator->fails()) {dd($validator->errors());
            return response()->json(['error' => $validator->errors()->all()]);
        }
        $note = \App\Models\UsersNotes::create([
                    'user_id' => $request->employee_id,
                    'note' => $request->user_note
        ]);
        if ($note) {
           return response()->json(['success' => 'done']);
        } else {
            return response()->json(['error' => 'There is some issue in adding note.']);
        }
    }

    /*
     * Function is to add attachment for employee
     */

    public function addAttachment(Request $request) {
        $validator = Validator::make(Input::all(), [
                    'file_name' => 'required'
        ],[
            'file_name.max' => 'File size should be less than 10MB'
        ]);
        if ($validator->fails()) {dd($validator->errors());
            return response()->json(['error' => $validator->errors()->all()]);
        }
        $file = $request->file('file_name');
        $extension = $request->file('file_name')->getClientOriginalExtension(); // getting excel extension
        $original_file_name = $request->file('file_name')->getClientOriginalName();
        if (!file_exists(public_path() . '/attachment/users_document')) {
            $dir = mkdir(public_path() . '/attachment/users_document', 0777, true);
        } else {
            $dir = public_path() . '/attachment/users_document/';
        }
//        $dir = public_path().'/attachment/users_document/';
        $filename = uniqid() . '_' . time() . '_' . date('Ymd') . '.' . $extension;
        $request->file('file_name')->move($dir, $filename);

        $attachment = \App\Models\UsersAttachments::create([
                    'user_id' => $request->employee_id,
                    'file_name' => $filename,
                    'original_file_name' => $original_file_name
        ]);
        if ($attachment) {
            return response()->json(['success' => 'done']);
        } else {
            return response()->json(['error' => 'There is some issue in uploading document.']);
        }
    }

    /*
     * Function is used to edit employee view
     */

    public function editEmployee($employee_id) {
        $data = [];

        $data['employee'] = \App\User::find($employee_id);
        $data['role'] = RoleUsers::where('user_id', '=', $employee_id)->first();
         $data['employee_role'] = RoleUsers::leftjoin('roles', 'roles.id', 'role_users.role_id')
                ->where('user_id', '=', $employee_id)
                ->select('roles.name')
                ->first();
        return view('admin.employees.edit', $data);
    }

    /*
     * Function is used to update details
     */

    public function updateEmployee(Request $request) {
        
      
        $rules = ['name' => 'required|regex:/^[a-zA-Z ]+$/u',
            'email' => 'required|email',
            'password' => 'sometimes|nullable|min:6',
            'retype_password' => 'required_with:password|same:password',
            'contact_number' => 'required',
            'emergency_contact_number' => 'required',
            'mailing_access' => 'required_if:role,==,3',
            'create_pin' => 'required_if:mailing_access,==,yes|required_if:role,==,2',
            'role'=>'required'
        ];
       
        $validator = Validator::make(Input::all(), $rules,[
                    'create_pin.digits' => 'The create pin must be 4 digits.',
        ]);
        $email_exist = User::where('email', '=', $request->email)
                ->where('id', '!=', $request->employee_id)
                ->get();

        if (isset($email_exist) && count($email_exist) > 0) {
            return redirect::to('admin/employee/edit/' . $request->employee_id)->with('error', 'Email already exist.');
        }
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        //update Role
       
        $user = User::find($request->employee_id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->contact_number = $request->contact_number;
        $user->emergency_contact = $request->emergency_contact_number;


        if (isset($request->password) && $request->password != null) {
            $user->password = bcrypt($request->password);
              $user->password_text = $request->password;
        }
        if($request->mailing_access == 'yes'){
            if (isset($request->create_pin) && $request->create_pin != null) {
                $user->pin = $request->create_pin;
            }
        }else{
             if(isset($request->create_pin)  && $request->create_pin != null){
              $user->pin = $request->create_pin;
            }else{
                 $user->pin = NULL;
            }
        }
        $user->save();

//        //Update role
         \DB::table('role_users')
                   ->where('user_id','=',$user->id)
                  ->update(['role_id'=>$request->role]);
        $role = RoleUsers::where('user_id','=',$user->id)->first();
      
//        if ($request->role == 2) {
//            $role->role_id = 2;
//        } else {
//            $role->role_id = 3;
//        }
//        $role->save();
          if ($role->role_id == 2) {
             $details = [
                'name' => $user->name,
                'email' => $user->email,
                'password' => $user->password_text
            ];
              Mail::to($request->email)->send(new AdminMail($details));
         }else{
            $details = [
                'name' => $user->name,
                'email' => $user->email,
                'password' => $user->password_text,
                'pin' => $user->pin
            ];
              Mail::to($request->email)->send(new EmployeeMail($details));
         }
        if ($user) {
            return redirect::to('admin/employees')->with('success', 'Employee successfully updated');
        } else {
            return redirect::back()->with('error', 'Problem in employee updation');
        }
    }

    /*
     * Function is used to view Employee
     */

    public function viewEmployee($employee_id) {
        $data['employee'] = User::find($employee_id);

        $data['employee_role'] = RoleUsers::leftjoin('roles', 'roles.id', 'role_users.role_id')
                ->where('user_id', '=', $employee_id)
                ->select('roles.name','role_users.role_id')
                ->first();

        return view('admin.employees.view', $data);
    }

    /**
     * Function is used to get all notes of employee
     */
    public function getNotes(Request $request) {
        $user_notes = \DB::table('users_notes')->where('user_id', '=', $request->id)
                ->get();
        foreach($user_notes As $k=>$val){
            $val->created_at = date('m-d-Y',strtotime($val->created_at));
        }
        return json_encode($user_notes);
    }

    /**
     * Function is used to delete note
     */
    public function destroy($note_id) {
        $user_notes = \DB::table('users_notes')
                ->where('id', '=', $note_id);

        $result = $user_notes->delete();


        return redirect::to('admin/employees')->with('success', 'Employee note successfully deleted');
    }

    /**
     * Function is used to get all attachments of employee
     */
    public function getAttachments(Request $request) {
        $user_attachments = \DB::table('users_attachments')
                ->where('user_id', '=', $request->id)
                ->get();

        return json_encode($user_attachments);
    }

    /**
     * Function is used to delete attachment
     */
    public function deleteAttachment($attachment_id) {

        $userAttachment = \DB::table('users_attachments')->find($attachment_id);

        $user_attachments = \DB::table('users_attachments')
                ->where('id', '=', $attachment_id);

        unlink(public_path('attachment/users_document/' . $userAttachment->file_name));

        $result = $user_attachments->delete();

        return redirect::to('admin/employees')->with('success', 'Employee document successfully deleted');
    }

    
    public function autoCompleteEmployee(Request $request){
                $query = $request->get('term','');
        
        $names= \App\User::leftjoin('role_users','role_users.user_id','=','users.id')
                            ->where('name','LIKE',$query.'%')
                            ->whereIn('role_users.role_id',[2,3])
                            ->get();
        
        $data=array();
        foreach ($names as $name) {
                $data[]=array('value'=>$name->name,'id'=>$name->id);
        }
        if(count($data))
             return $data;
        else
            return ['value'=>'No Result Found','id'=>''];
    }

}
