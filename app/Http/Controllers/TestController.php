<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TestController extends Controller
{

    //fetch all items from qbo
    public function index()
    {
        $qbo = new \App\Http\Controllers\QuickBooksController();
        $qboItems = $qbo->generateTokens(config('constants.QBO.SCOPE'), 'queryItem', [
            'customer_id' => 6,
            'query' => 'select+*+from+Item'
        ]);
        dd($qboItems);
    }
}
