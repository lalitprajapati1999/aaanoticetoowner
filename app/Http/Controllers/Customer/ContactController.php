<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use DB;
use Alert;
use Illuminate\Support\Facades\Session;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Response;

class ContactController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id=null) {
        $data = ['id' => $id];

//        $customerId         = Auth::user()->customer->id;
//        $data['contacts']   =  \App\Models\Contact::select()->where('customer_id',$customerId)->paginate(10);
//        
//        if ($data['contacts']->isEmpty()) { 
//               // Alert::success('No record found')->flash();
//           Session::flash('success','No record found');
//        } 


        return view('customer.contact.list', $data);
    }

    /**
     * 
     */
    public function getAddressBookFilterData(Request $request, $id=null) {
        if(!empty($id)) {
            $contacts = \DB::table('contacts')
                    ->select('*')
                    ->where('contacts.customer_id', '=', decrypt($id))
                    ->where('contacts.added_form','!=','work_order')
                    ->orderBy('contacts.id', 'DESC')
                    ->get();
        } else if (Auth::user()->hasRole('account-manager')) {

            $customers = \App\Models\Customer::leftjoin('users', 'users.id', '=', 'customers.user_id')
                            ->select('customers.id')
                            ->where('account_manager_id', '=', Auth::user()->id)
                            ->get()->toArray();
            $ids = [];
            foreach ($customers AS $customer) {
                array_push($ids, $customer['id']);
            }

            $contacts = \DB::table('contacts')
                    ->select('*')
                    ->whereIn('contacts.customer_id', $ids)
                    ->orderBy('contacts.id', 'DESC')
                    ->get();
        } else {
            $contacts = \DB::table('contacts')
                    ->select('*')
                    ->where('contacts.customer_id', '=', Auth::user()->customer->id)
                    ->where('contacts.added_form','!=','work_order')
                    ->orderBy('contacts.id', 'DESC')
                    ->get();
        }

        foreach ($contacts AS $contact) {

            $firstContactPerson = \DB::table('contact_details')
                    ->select('contact_details.name', 'contact_details.mobile')
                    ->where('contact_details.contact_id', '=', $contact->id)
                    ->first();
            $city = \App\Models\City::find($contact->city_id);
            $State = \App\Models\State::find($contact->state_id);

            $contact->city = isset($city) ? $city->name : "";
            $contact->State = isset($State) ? $State->name:"";
            $contact->mailing_address = $contact->mailing_address . ', '. $contact->city .' '. $contact->State .' '.$contact->zip;

            if (isset($firstContactPerson) && $firstContactPerson != NULL) {
                $contact->name = $firstContactPerson->name;
                $contact->mobile = $firstContactPerson->mobile;

            } else {
                $contact->name = '';
                $contact->mobile = '';
            }
           // $city_id = \App\Models\City::find($each_record['city']);
        }
        $dt = Datatables::of($contacts)
                        ->filter(function ($instance) use ($request) {
                            if (($request->has('company_name')) && ($request->get('company_name') != NULL)) {
                                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                                           
                                           if (strpos($row['company_name'], $request->get('company_name')) !== false) {
                                                return true;
                                            }else{
                                                return false;
                                            }
                                    // return strpos($row['company_name'], $request->get('company_name')) ? true : false;

                                });
                            }
                           

                            if (($request->has('contact_name')) && ($request->get('contact_name') != NULL)) {
                                $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                                    if (strpos($row['contact_name'], $request->get('contact_name')) !== false) {
                                                return true;
                                            }else{
                                                return false;
                                            }
                                });
                            }
                        });

        if(empty($id)) {
            $dt = $dt->addColumn('action', function($contact){
                return view('customer.contact.actions', compact('contact'))->render();
            });
        }
        return $dt->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request) {

        $data['cities'] = \App\Models\City::select('id', 'name')->get();
        $data['states'] = \App\Models\State::select('id', 'name')->orderBy('name')->get();
        $data['countries'] = \App\Models\Country::select('id', 'name')->get();
        if (Auth::user()->hasRole('account-manager')) {

            $customers = \App\Models\Customer::leftjoin('users', 'users.id', '=', 'customers.user_id')
                    ->select('customers.id', 'users.name')
                    ->where('account_manager_id', '=', Auth::user()->id)
                    ->get();
            $data['customers'] = $customers;
        }
        return view('customer.contact.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
       // dd($request->all());
       
        try {
            $rules = ['company_name' => 'required',
                'mailing_address' => 'required',
                'city' => 'required',
                'state' => 'required| exists:states,id',
                'zip' => 'required|numeric',
                    //  'country' => 'required| exists:countries,id',
//                'business_phone.0' => 'nullable|regex:/^\(\d{3}\)\s\d{3}-\d{4}$/u',
//                'business_phone.*' => 'nullable|regex:/^\(\d{3}\)\s\d{3}-\d{4}$/u',
//                'business_email' => 'nullable|email',
//                'contact_fax.0'=>'nullable',
//                'contact_fax.*' => 'nullable',
//                'contact_email1.0' => 'nullable|email',
//                'contact_email1.*' => 'nullable|email',
//                'contact_mobile1.0' => 'nullable|numeric',
//                'contact_mobile1.*' => 'nullable|numeric'
            ];
            $validator = Validator::make(Input::all(), $rules, [
//                         'business_phone.0' => 'Business phone is invalid',
//                        'business_phone.*' => 'Business phone is invalid',
//                        'contact_name.*' => 'Contact Name format is invalid',
//                        'contact_fax.*' => 'Contact Fax is invalid',
//                        'contact_email1.*' => 'Contact email format is invalid',
//                        'contact_mobile1.*' => 'Contact mobile format is invalid'
            ]);
            if ($validator->fails()) { //dd($validator->errors());
                return redirect()->back()->withErrors($validator)->withInput();
//                throw new HttpResponseException(response()->json(['status' => FALSE, 'error' => $validator->errors()], 404));
            }
             if(!is_numeric($request->city)){
         //dd('tsets'); 
            $city = \App\Models\City::firstOrNew(array('name' => $request->city));
            //$city = new \App\Models\City();
            $city->name = $request->city;
            $city->state_id = $request->state;
            $city->zip_code = $request->zip;
            $city->save();
            $request->request->set('city',$city->id);
        }
            $contact_count = $request->contact_count;
            //  $sales_count=$request->sales_count;
            $contact_email_string = [];
            $contact_mobile_string = [];
            //  $sales_email_string=[];
            //  $sales_mobile_string=[];
            $input = $request->all();
             foreach ($input['business_phone'] as $key => $value) {
                        if($value==null){
                            unset($input['business_phone'][$key]);
                        }
                    }
            $business_phone_array = $input['business_phone'];

            $business_phone = implode(",", $business_phone_array);

//        if (Auth::user()->hasRole('customer')) {
            if (Auth::user()->hasRole('account-manager')) {
                $customerId = $request->customer_id;
            } else {
                $customerId = Auth::user()->customer->id;
            }
           // dd($request->all());

           // $request->request->set('city', $request->city_value_id);

            $contact = \App\Models\Contact::create([
                        'customer_id' => $customerId,
                        'company_name' => $request->company_name,
                        'added_form' => 'address_book',
                        'mailing_address' => $request->mailing_address,
                        'company_address' => $request->mailing_address,
                        'city_id' => $request->city,
                        'attn' => $request->attn,
                        'state_id' => $request->state,
                        'zip' => $request->zip,
                        'phone' => $business_phone,
                        'email' => $request->business_email
            ]);
            //if (($input['contact_name'][0] != NULL) || ($input['contact_email1'][0] != NULL) || ($input['contact_fax'][0] != NULL) || ($input['designation'][0] != NULL) || ($input['contact_mobile1'][0] != NULL) || ($input['contact_person_array'][0] != NULL) || ($input['contact_email_array'][0] != NULL)) {

            for ($i = 1; $i <= $contact_count; $i++) {
                $contact_email1 = 'contact_email' . $i;
                $contact_mobile1 = 'contact_mobile' . $i;
                foreach ($input[$contact_email1] as $key => $value) {
                    if($value==null){
                            unset($input[$contact_email1][$key]);
                        }
                    }
                foreach ($input[$contact_mobile1] as $key => $value) {
                    if($value==null){
                            unset($input[$contact_mobile1][$key]);
                }
            }
                
                $contact_email_array = $input[$contact_email1];
                $contact_email = implode(",", $contact_email_array);
                $contact_mobile_array = $input[$contact_mobile1];
                $contact_mobile = implode(",", $contact_mobile_array);
                $contact_email_string[$i] = $contact_email;
                $contact_mobile_string[$i] = $contact_mobile;

            }
            //  dd($contact_mobile_string);
//        for($i=1;$i<=$sales_count;$i++)
//        {
//            $sales_email1='sales_person_email'.$i;
//            $sales_mobile1='sales_person_mobile'.$i;
//            $sales_email_array       = $input[$sales_email1];
//            $sales_mobile_array       = $input[$sales_mobile1];
//            $sales_email          = implode(",", $sales_email_array);
//            $sales_mobile          = implode(",", $sales_mobile_array);
//            
//            $sales_email_string[$i]=$sales_email;
//            $sales_mobile_string[$i]=$sales_mobile;
//
//        }  
            $contact_name_array = $input['contact_name'];
            $contact_fax_array = $input['contact_fax'];
            $contact_designation_array = $input['designation'];
//        $sales_name_array        = $input['sales_person_name'];
//        $sales_fax_array         = $input['sales_person_fax'];
//        $sales_person            = $input['sales_person_array'];
            $contact_person = $input['contact_person_array'];
            $contact_person_array = explode(",", $contact_person[0]);
            // $sales_person_array      = explode(",", $sales_person[0]);

            $contactId = $contact->id;
//dd($contact_person_array);

            foreach ($contact_person_array as $key => $value) {
                // echo$contact_person_array[$key] . " -- ";
                // echo ($contact_name_array[$key] != "") . " -- " . ( $contact_email_string[$key + 1] != "") . " -- " . ($contact_fax_array[$key] != "") . " -- " . ( $contact_mobile_string[$key + 1] != "") . " -- " . $contact_designation_array[$key];
                if (($contact_name_array[$key] != "") || ( $contact_email_string[$key + 1] != "") || ($contact_fax_array[$key] != "") || ( $contact_mobile_string[$key + 1] != "") || ($contact_designation_array[$key] != "")) {
                    // dd("ok");
                    $result = \App\Models\ContactDetail::create([
                                'contact_id' => $contactId,
                                'type' => $contact_person_array[$key],
                                'name' => $contact_name_array[$key],
                                'email' => $contact_email_string[$key + 1],
                                'mobile' => $contact_mobile_string[$key + 1],
                                'fax' => $contact_fax_array[$key],
                                'designation' => $contact_designation_array[$key]
                    ]);
                }
            }
            //      }
//                foreach ($sales_person_array as $key => $value)
//                {      
//                            $result= \App\Models\ContactDetail::create([
//                            'contact_id'    => $contactId,
//                            'type'          => $sales_person_array[$key],
//                            'name'          => $sales_name_array[$key],
//                            'email'         => $sales_email_string[$key+1],
//                            'mobile'        => $sales_mobile_string[$key+1],
//                            'fax'           => $sales_fax_array[$key],
//                        
//                               ]);
//                        
//                }
            return redirect::to('customer/contacts')->with('success', 'Contact successfully created');
//            return Response::json(array('message' => 'Contact successfully created', 'status' => TRUE));
        } catch (\Exception $ex) {
            return Response::json(array('message' => $ex->getMessage(), 'status' => FALSE));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

        $data['cities'] = \App\Models\City::select('id', 'name')->get();
        $data['states'] = \App\Models\State::select('id', 'name')->orderBy('name')->get();
        $data['id'] = $id;
        $contact = \App\Models\Contact::select()->where('id', $id)->first();
       // $contact = (array)$contact;
        $data['countries'] = \App\Models\Country::select('id', 'name')->get();
        if (isset($contact->city_id) && $contact ) {

            $city_name = \App\Models\City::where('id', '=', $contact->city_id)
                    ->select('name')
                    ->first();
            $contact->city_name = $city_name->name;
        }
        $data['contact'] = $contact;
       // dd($data['contact']);
        return view('customer.contact.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
       // dd($request->all());
        try {
            $rules = ['company_name' => 'required',
                'mailing_address' => 'required',
                'city' => 'required',
                'state' => 'required| exists:states,id',
                // 'country' => 'required|exists:countries,id',
                'zip' => 'required|numeric',
//                'business_phone.*' => 'required|numeric',
//                  'business_phone.0' => 'nullable|regex:/^([0-9]{3})[0-9]{3}-[0-9]{4}$/',
//                'business_phone.*' => 'nullable|regex:/^([0-9]{3})[0-9]{3}-[0-9]{4}$/',
//                'business_email' => 'nullable|email',
//                'contact_name.*' => 'regex:/^[a-zA-Z ]+$/u',
//                'contact_fax.*' => 'numeric',
//                'contact_email1.*' => 'email',
//                'contact_mobile1.*' => 'numeric',
            ];
            $validator = Validator::make(Input::all(), $rules, [
//                        'business_phone.*' => 'Business phone is invalid',
//                        'contact_name.*' => 'Contact Name format is invalid',
//                        'contact_fax.*' => 'Contact Fax is invalid',
//                        'contact_email1.*' => 'Contact email format is invalid',
//                        'contact_mobile1.*' => 'Contact mobile format is invalid',
            ]);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();

//                throw new HttpResponseException(response()->json(['status' => FALSE, 'error' => $validator->errors()], 404));
            }
             if(!is_numeric($request->city)){
         //dd('tsets'); 
            $city = \App\Models\City::firstOrNew(array('name' => $request->city));
            //$city = new \App\Models\City();
            $city->name = $request->city;
            $city->state_id = $request->state;
            $city->zip_code = $request->contact_zip;
            $city->save();
            $request->request->set('city',$city->id);
        }
            $input = $request->all();
            $contact_count = $request->contact_count;

            //  $sales_count=$request->sales_count;
            $contact_email_string = [];
            $contact_mobile_string = [];
            //   $sales_email_string=[];
            //   $sales_mobile_string=[];
            $input = $request->all();//dd($input);
            foreach ($input['phone'] as $key => $value) {
                        if($value==null){
                            unset($input['phone'][$key]);
                        }
                    }
            $business_phone_array = $input['phone'];

            $business_phone = implode(",", $business_phone_array);

            $contact_obj = \App\Models\Contact::find($id);
            $request->request->set('phone', $business_phone);
            $request->request->set('email', $request->business_email);
            $request->request->set('city_id', $request->city);
            $request->request->set('state_id', $request->state);
            $request->request->set('attn', $request->attn);
            $contact_obj->update($request->only('company_name', 'company_address', 'mailing_address', 'city_id', 'state_id', 'zip', 'phone', 'email', 'attn'));
            $contact_obj->save();
            if (($input['contact_name'][0] != NULL) || ($input['contact_email1'][0] != NULL) || ($input['contact_fax'][0] != NULL) || ($input['designation'][0] != NULL) || ($input['contact_mobile1'][0] != NULL) || ($input['contact_person_array'][0] != NULL) || ($input['contact_email_array'][0] != NULL)) {
                for ($i = 1; $i <= $contact_count; $i++) {
                    $contact_email1 = 'contact_email' . $i;
                    $contact_mobile1 = 'contact_mobile' . $i;
                    foreach ($input[$contact_email1] as $key => $value) {
                        if($value==null){
                            unset($input[$contact_email1][$key]);
                        }
                    }//dd($input[$contact_mobile1]);
                    foreach ($input[$contact_mobile1] as $key => $value) {
                        if($value==null){
                            unset($input[$contact_mobile1][$key]);
                        }
                    }
                    $contact_email_array = $input[$contact_email1];
                    $contact_email = implode(",", $contact_email_array);
                    $contact_mobile_array = $input[$contact_mobile1];
                    $contact_mobile = implode(",", $contact_mobile_array);
                    $contact_email_string[$i] = $contact_email;
                    $contact_mobile_string[$i] = $contact_mobile;
                }
//dd($contact_email_string,$contact_mobile_string);
//        for($i=1;$i<=$sales_count;$i++)
//        {
//            $sales_email1='sales_person_email'.$i;
//            $sales_mobile1='sales_person_mobile'.$i;
//            $sales_email_array       = $input[$sales_email1];
//            $sales_mobile_array       = $input[$sales_mobile1];
//            $sales_email          = implode(",", $sales_email_array);
//            $sales_mobile          = implode(",", $sales_mobile_array);
//            
//            $sales_email_string[$i]=$sales_email;
//            $sales_mobile_string[$i]=$sales_mobile;
//
//        }  

               /* foreach ($input['contact_name'] as $key => $value) {
                        if($value==null){
                            unset($input['contact_name'][$key]);
                        }
                    } 
                foreach ($input['contact_fax'] as $key => $value) {
                        if($value==null){
                            unset($input['contact_fax'][$key]);
                        }
                    }*/
                    //dd($input);
                $contact_name_array = $input['contact_name'];
                $contact_fax_array = $input['contact_fax'];
                $contact_designation_array = $input['designation'];
                $contact_person = $input['contact_person_array'];
                $contact_person_array = explode(",", $contact_person[0]);
               //dd($contact_mobile_string);
                $whereArray = array('contact_id' => $id);
                $affected = DB::table('contact_details')->where($whereArray)->delete();
                for ($i = 0; $i < $contact_count; $i++) {
                    if($contact_name_array[$i]==null && $contact_email_string[$i + 1]=='' &&  $contact_mobile_string[$i + 1]=='' && $contact_fax_array[$i]==null && $contact_designation_array[$i]==null){
                    }else{ 
                        $result1 = \App\Models\ContactDetail::create([
                                'contact_id' => $id,
                                'type' => 0,
                                'name' => $contact_name_array[$i] ? $contact_name_array[$i] : '',
                                'email' => $contact_email_string[$i + 1],
                                'mobile' => $contact_mobile_string[$i + 1],
                                'fax' => $contact_fax_array[$i] ? $contact_fax_array[$i] : '',
                                'designation' => $contact_designation_array[$i] ? $contact_designation_array[$i] : ''
                        ]);
                    }
                }
//                foreach ($sales_person_array as $key => $value)
//                {      
//                            $result2 = \App\Models\ContactDetail::create([
//                            'contact_id'    => $id,
//                            'type'          => $sales_person_array[$key],
//                            'name'          => $sales_name_array[$key],
//                            'email'         => $sales_email_string[$key+1],
//                            'mobile'        => $sales_mobile_string[$key+1],
//                            'fax'           => $sales_fax_array[$key],
//                        
//                               ]);
//                        
//                }
            }
            return redirect::to('customer/contacts')->with('success', 'Contact successfully updated');

//            return Response::json(array('message' => 'Contact successfully updated', 'status' => TRUE));
        } catch (\Exception $ex) {
            return Response::json(array('message' => $ex->getMessage(), 'status' => FALSE));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request) {
        $contact = \App\Models\Contact::findOrFail($request->id);
        $result = $contact->delete();
        if ($result) {
            return redirect::back()->with('success', 'Contact has been successfully deleted');
        } else {
            return redirect::back()->with('success', 'Problem in record deleting');
        }
    }

    /**
     * Function is used to add contact from recipient page
     * 
     * @param Request $request
     */
    public function contactAddFromRecipients(Request $request) {

        $validator = Validator::make($request->all(), [
                    'contact_name' => 'required',
                    'contact_address' => 'required',
                    'contact_city_id' => 'required| exists:cities,id',
                    'contact_state_id' => 'required| exists:states,id',
                    'contact_zip' => 'required|numeric|digits:5',
                        ], [
                    'contact_name.required' => 'The name is required',
                    'contact_address.required' => 'The address is required',
                    'contact_city_id.required' => 'Select City from the list',
                    'contact_state_id.required' => 'The state is required',
                    'contact_zip.required' => 'The zip code is required'
        ]);

        $input = $request->all();
        if (isset($input['contact_no']) && $input['contact_no'] != NULL) {
            if (!preg_match("/^[(][0-9]{3}[)]\s[0-9]{3}-[0-9]{4}$/", $input['contact_no'])) {
                $validator->errors()->add('contact_no', 'The contact number is invalid.');
            }
        }
        $error_count = $validator->errors();

        if (count($error_count) > 0) {
            return Response::json(['errors' => $validator->errors()]);
        }

        if ($validator->passes()) {

            $doc = \App\Models\SecondaryDocument::find($request->doc_id);
            $check_company_name = \App\Models\Contact::where('company_name', '=', $request->contact_name)
                    ->where('customer_id', '=', $doc->customer_id)
                    ->first();

            if (isset($check_company_name) && count($check_company_name) > 0) {
                return Response::json(['error_msg' => 'company name already exist.']);
            }

            // Store your user in database 
            
            $contactObj = new \App\Models\Contact();
            $contactObj->customer_id = $doc->customer_id;
            $contactObj->company_name = $request->contact_name;
            $contactObj->mailing_address = $request->contact_address;
            $contactObj->city_id = $request->contact_city_id;
            $contactObj->state_id = $request->contact_state_id;
            $contactObj->zip = $request->contact_zip;
            $contactObj->phone = $request->contact_no;
            $contactObj->email = $request->contact_email;
            $contactObj->attn = $request->attn;
            // $contactObj->country_id = $request->contact_country;
            $contactObj->save();

            if (isset($contactObj->id) && $contactObj->id != NULL) {
                $contactData = \App\Models\Contact::find($contactObj->id);
                $cityname = \App\Models\City::find($contactData->city_id);
                $contactData->city_name = $cityname->name;
            } else {
                $contactData = '';
            }

            return Response::json(['success' => 'Contact successfully created', 'contact_data' => $contactData
            ]);
        }
    }

    public function getAllContacts(Request $request) {
        $doc_id = $request->doc_id;

        $customer_details = \App\Models\SecondaryDocument::find($doc_id);

        $contacts = \App\Models\Contact::select()
                ->where('contacts.customer_id', '=', $customer_details->customer_id)
                ->orderBy('contacts.id', 'DESC')
                ->get();

        /* if(isEmpty($contacts)){

          } */
        return Response::json(['contacts' => $contacts]);
    }

    /**
     * Function is used to add contact from recipient page in work order
     * 
     * @param Request $request
     */
    public function contactAddFromWorkOrderRecipients(Request $request) {

        $validator = Validator::make($request->all(), [
                    'contact_name' => 'required',
                    'contact_address' => 'required',
                    'contact_city_id' => 'required',
                    'contact_state_id' => 'required| exists:states,id',
                    'contact_zip' => 'required|numeric|digits:5',
                        ], [
                    'contact_name.required' => 'The name is required',
                    'contact_address.required' => 'The address is required',
                    'contact_city_id.required' => 'Select City from the list',
                    'contact_state_id.required' => 'The state is required',
                    'contact_zip.required' => 'The zip code is required',
        ]);

       
        $input = $request->all();
       
        if (isset($input['contact_no']) && $input['contact_no'] != NULL) {
            if (!preg_match("/^[(][0-9]{3}[)]\s[0-9]{3}-[0-9]{4}$/", $input['contact_no'])) {
                $validator->errors()->add('contact_no', 'The contact number is invalid.');
            }
        }
        $error_count = $validator->errors();

        if (count($error_count) > 0) {
            return Response::json(['errors' => $validator->errors()]);
        }
        if ($validator->passes()) {
            if ($request->doc_id != 0) {
                $work_order_id = $request->doc_id;
            } else {
                $work_order_id = '';
            }
            if ($work_order_id != '') {
                $doc = \App\Models\WorkOrder::find($request->doc_id);
            }

            /* if ($work_order_id != '') {
              $check_company_name = \App\Models\Contact::where('company_name', '=', $request->contact_name)
              ->where('customer_id', '=', $doc->customer_id)
              ->first();
              } else {
              $check_company_name = \App\Models\Contact::where('company_name', '=', $request->contact_name)
              ->where('customer_id', '=', Auth::user()->customer->id)
              ->first();
              }
              if (isset($check_company_name) && count($check_company_name) > 0) {
              return Response::json(['error_msg' => 'company name already exist.']);
              } */
            // Store your user in database 
                if(!is_numeric($request->contact_city_id)){
         //dd('tsets'); 
            $city = \App\Models\City::firstOrNew(array('name' => $request->contact_city_id));
            //$city = new \App\Models\City();
            $city->name = $request->contact_city_id;
            $city->state_id = $request->contact_state_id;
            $city->zip_code = $request->contact_zip;
            $city->save();
            $request->request->set('contact_city_id',$city->id);
        }
        if($request->contact_id){
                $contactObj = \App\Models\Contact::find($request->contact_id);
                $contactObj->company_name = $request->contact_name;
                $contactObj->mailing_address = $request->contact_address;
                $contactObj->company_address = $request->contact_address;
                $contactObj->city_id = $request->contact_city_id;
                $contactObj->state_id = $request->contact_state_id;
                $contactObj->zip = $request->contact_zip;
                $contactObj->phone = $request->contact_no;
                $contactObj->email = $request->contact_email;
                $contactObj->attn = $request->attn;
                $contactObj->added_form = 'address_book';
                $contactObj->save();
        }else{
            $contactObj = new \App\Models\Contact();
            if ($work_order_id != '') {
                $contactObj->customer_id = $doc->customer_id;
            } else {
                $contactObj->customer_id = Auth::user()->customer->id;
            }

            $contactObj->company_name = $request->contact_name;
            $contactObj->mailing_address = $request->contact_address;
            $contactObj->company_address = $request->contact_address;
            $contactObj->city_id = $request->contact_city_id;
            $contactObj->state_id = $request->contact_state_id;
            $contactObj->zip = $request->contact_zip;
            $contactObj->phone = $request->contact_no;
            $contactObj->email = $request->contact_email;
            $contactObj->attn = $request->attn;
            $contactObj->added_form = 'address_book';
            $contactObj->save();
        }
            if (isset($contactObj->id) && $contactObj->id != NULL) {
                $contactData = \App\Models\Contact::find($contactObj->id);
                $cityname = \App\Models\City::find($contactData->city_id);
                $contactData->city_name = $cityname->name;
            } else {
                $contactData = '';
            }

            return Response::json(['success' => 'Contact successfully created', 'contact_data' => $contactData
            ]);
        }

        return Response::json(['errors' => $validator->errors()]);
    }

    /*     * Function is used to add contact from recipient page in work order
     * 
     * @param Request $request
     */

    public function contactAddFromCyoWorkOrderRecipients(Request $request) {
        //dd($request->all());
         
        $validator = Validator::make($request->all(), [
                    'contact_name' => 'required',
                    'contact_address' => 'required',
                    'contact_city_id' => 'required',
                    'contact_state_id' => 'required| exists:states,id',
                    'contact_zip' => 'required|numeric|digits:5',
                        ], [
                    'contact_name.required' => 'The name is required',
                    'contact_address.required' => 'The address is required',
                    'contact_city_id.required' => 'Select City from the list',
                    'contact_state_id.required' => 'The state is required',
                    'contact_zip.required' => 'The zip code is required',
        ]);

        $input = $request->all();
        if (isset($input['contact_no']) && $input['contact_no'] != NULL) {
            if (!preg_match("/^[(][0-9]{3}[)]\s[0-9]{3}-[0-9]{4}$/", $input['contact_no'])) {
                $validator->errors()->add('contact_no', 'The contact number is invalid.');
            }
        }
        $error_count = $validator->errors();

        if (count($error_count) > 0) {
            return Response::json(['errors' => $validator->errors()]);
        }
//dd( $request->all());
        if ($validator->passes()) {
            if ($request->doc_id != 0) {
                $work_order_id = $request->doc_id;
            } else {
                $work_order_id = '';
            }
            if ($work_order_id != '') {
                $doc = \App\Models\Cyo_WorkOrders::find($request->doc_id);
            }
//            if ($work_order_id != '') {
//                $check_company_name = \App\Models\Contact::where('company_name', '=', $request->contact_name)
//                        ->where('customer_id', '=', $doc->customer_id)
//                        ->first();
//            } else {
//                $check_company_name = \App\Models\Contact::where('company_name', '=', $request->contact_name)
//                        ->where('customer_id', '=', Auth::user()->customer->id)
//                        ->first();
//            }
//            if (isset($check_company_name) && count($check_company_name) > 0) {
//                return Response::json(['error_msg' => 'company name already exist.']);
//            }

          if(!is_numeric($request->contact_city_id)){
         //dd('tsets'); 
            $city = \App\Models\City::firstOrNew(array('name' => $request->contact_city_id));
            //$city = new \App\Models\City();
            $city->name = $request->contact_city_id;
            $city->state_id = $request->contact_state_id;
            $city->zip_code = $request->contact_zip;
            $city->save();
            $request->request->set('contact_city_id',$city->id);
        }
        if($request->contact_id){
            $contactObj = \App\Models\Contact::find($request->contact_id);
            $contactObj->company_name = $request->contact_name;
            $contactObj->mailing_address = $request->contact_address;
            $contactObj->company_address = $request->contact_address;
            $contactObj->city_id = $request->contact_city_id;
            $contactObj->state_id = $request->contact_state_id;
            $contactObj->zip = $request->contact_zip;
            $contactObj->phone = $request->contact_no;
            $contactObj->email = $request->contact_email;
            $contactObj->attn = $request->attn;
            $contactObj->added_form = 'address_book';
            $contactObj->save();
        }else{
            // Store your user in database 
            $contactObj = new \App\Models\Contact();
            if ($work_order_id != '') {
                $contactObj->customer_id = $doc->customer_id;
            } else {
                $contactObj->customer_id = Auth::user()->customer->id;
            }

            //  $contactObj->customer_id = $doc->customer_id;
            $contactObj->company_name = $request->contact_name;
            $contactObj->mailing_address = $request->contact_address;
            $contactObj->company_address = $request->contact_address;
            $contactObj->city_id = $request->contact_city_id;
            $contactObj->state_id = $request->contact_state_id;
            $contactObj->zip = $request->contact_zip;
            $contactObj->phone = $request->contact_no;
            $contactObj->email = $request->contact_email;
            $contactObj->attn = $request->attn;
            $contactObj->added_form ='address_book';
            $contactObj->save();
        }
            if (isset($contactObj->id) && $contactObj->id != NULL) {
                $contactData = \App\Models\Contact::find($contactObj->id);
                $cityname = \App\Models\City::find($contactData->city_id);
                $contactData->city_name = $cityname->name;
            } else {
                $contactData = '';
            }

            return Response::json(['success' => 'Contact successfully created', 'contact_data' => $contactData
            ]);
        }

        return Response::json(['errors' => $validator->errors()]);
    }

    public function getAllWorkOrderContacts(Request $request) {

        $doc_id = $request->doc_id;
        if ($doc_id != 0) {
            $customer_details = \App\Models\WorkOrder::find($doc_id);
            $customer_id = $customer_details->customer_id;
        } else {
            $customer_id = Auth::user()->customer->id;
        }
        $contacts = \App\Models\Contact::select()
                ->where('contacts.customer_id', '=', $customer_id)
                ->orderBy('contacts.id', 'DESC')
                ->get();

        /* if(isEmpty($contacts)){

          } */
        return Response::json(['contacts' => $contacts]);
    }

    public function getAllCyoWorkOrderContacts(Request $request) {
        $doc_id = $request->doc_id;
        if ($doc_id != 0) {
            $customer_details = \App\Models\Cyo_WorkOrders::find($doc_id);
            $customer_id = $customer_details->customer_id;
        } else {
            $customer_id = Auth::user()->customer->id;
        }
        $contacts = \App\Models\Contact::select()
                ->where('contacts.customer_id', '=', $customer_id)
                ->orderBy('contacts.id', 'DESC')
                ->get();

        /* if(isEmpty($contacts)){

          } */
        return Response::json(['contacts' => $contacts]);
    }

    public function autoCompleteCompanyName(Request $request) {
        $query = $request->get('term', '');



        $names = \App\Models\Contact::where('company_name', 'LIKE', $query . '%')
                ->where('customer_id', '=', Auth::user()->customer->id)
                ->get();

        $data = array();
        foreach ($names as $name) {
            $data[] = array('value' => $name->company_name, 'id' => $name->id);
        }
        if (count($data))
            return $data;
        else
            return ['value' => 'No Result Found', 'id' => ''];
    }

    /**
     * Function is used when edit secondary document recipients update date in address book if customer 
     * want to update
     * @param Request $request
     */
    public function updateSecondaryRecipientContact(Request $request) {

        //Get customer id from secondary document id
        $secondaryDocument = \App\Models\SecondaryDocument::find($request->secondary_doc_id);
        $customer_id = $secondaryDocument->customer_id;


        $contracted_by = $request->name;
        /* check contracted by is exist or not */
        $contracted_by_check = \App\Models\Contact::where('company_name', $contracted_by)->first();
        if (empty($contracted_by_check)) {
            $contact = \App\Models\Contact::create([
                        'customer_id' => Auth::user()->customer->id,
                        'company_name' => $contracted_by,
            ]);
        }

        //Find Contact details
        $contactDetails = \App\Models\Contact::where('customer_id', '=', $customer_id)
                ->where('company_name', '=', $request->name)
                ->get();
        if (isset($contactDetails) && !empty($contactDetails)) {
            foreach ($contactDetails AS $k => $val) {
                $updateContactObject = \App\Models\Contact::find($val->id);
                $updateContactObject->company_name = $request->name;
                $updateContactObject->mailing_address = $request->address;
                $updateContactObject->city_id = $request->recipient_city_id;
                $updateContactObject->state_id = $request->state_id;
                $updateContactObject->zip = $request->zip;
                $updateContactObject->phone = $request->contact;
                $updateContactObject->email = $request->email;
                $updateContactObject->save();
            }
        }
        return \Response::json(['success' => true]);
    }

    /**
     * Function is used when edit work order recipients update date in address book if customer 
     * want to update
     * @param Request $request
     */
    public function updateWorkOrderRecipientContact(Request $request) {

        //Get customer id from secondary document id
        $workorder = \App\Models\WorkOrder::find($request->work_order_id);
        $customer_id = $workorder->customer_id;

        $contracted_by = $request->name;
        $recipt_id = !empty($request->recipt_id) ? $request->recipt_id : "";
        /* check contracted by is exist or not */
        if ($recipt_id != "") {
            $contracted_by_check = \App\Models\Contact::where('id', $recipt_id)->first();
        } else {
            $contracted_by_check = \App\Models\Contact::where('company_name', $contracted_by)->first();
        }

        if (empty($contracted_by_check)) {
            $contact = \App\Models\Contact::create([
                        'customer_id' => Auth::user()->customer->id,
                        'company_name' => $contracted_by,
            ]);
        }

        //Find Contact details
        if ($recipt_id != "") {
            $contactDetails = \App\Models\Contact::where('customer_id', '=', $customer_id)
                    ->where('id', '=', $recipt_id)
                    ->get();
        } else {
            $contactDetails = \App\Models\Contact::where('customer_id', '=', $customer_id)
                    ->where('company_name', '=', $request->name)
                    ->get();
        }
        if (isset($contactDetails) && !empty($contactDetails)) {
            foreach ($contactDetails AS $k => $val) {
                $updateContactObject = \App\Models\Contact::find($val->id);
                $updateContactObject->company_name = $request->name;
                $updateContactObject->mailing_address = $request->address;
                $updateContactObject->city_id = $request->recipient_city_id;
                $updateContactObject->state_id = $request->state_id;
                $updateContactObject->zip = $request->zip;
                $updateContactObject->phone = $request->contact;
                $updateContactObject->email = $request->email;
                $updateContactObject->attn = $request->attn;
                $updateContactObject->save();
            }
        }
        return \Response::json(['success' => true]);
    }

    /**
     * Function is used when edit work order recipients in create your own update date in address book if customer 
     * want to update
     * @param Request $request
     */
    public function updateCyoWorkOrderRecipientContact(Request $request) {

        //Get customer id from secondary document id
        $workorder = \App\Models\Cyo_WorkOrders::find($request->work_order_id);
        $customer_id = $workorder->customer_id;
        $recipt_id = !empty($request->recipt_id) ? $request->recipt_id : "";

        if ($recipt_id != "") {
            $contactDetails = \App\Models\Contact::where('customer_id', '=', $customer_id)
                    ->where('id', '=', $recipt_id)
                    ->get();
        } else {
            //Find Contact details
            $contactDetails = \App\Models\Contact::where('customer_id', '=', $customer_id)
                    ->where('company_name', '=', $request->name)
                    ->get();
        }
        if (isset($contactDetails) && !empty($contactDetails)) {
            foreach ($contactDetails AS $k => $val) {
                $updateContactObject = \App\Models\Contact::find($val->id);
                $updateContactObject->company_name = $request->name;
                $updateContactObject->mailing_address = $request->address;
                $updateContactObject->city_id = $request->recipient_city_id;
                $updateContactObject->state_id = $request->state_id;
                $updateContactObject->zip = $request->zip;
                $updateContactObject->phone = $request->contact;
                $updateContactObject->email = $request->email;
                $updateContactObject->save();
            }
        }
        return \Response::json(['success' => true]);
    }

}
