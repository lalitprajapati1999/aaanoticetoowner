<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\QuickBooksController;
use Auth;
use Illuminate\Support\Facades\Validator;
use LVR\CreditCard\CardCvc;
use LVR\CreditCard\Cards\Card;
use LVR\CreditCard\CardNumber;
use LVR\CreditCard\CardExpirationYear;
use LVR\CreditCard\CardExpirationMonth;
use LVR\CreditCard\Cards\ExpirationDateValidator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use App\Mail\CustomerActivationMail;
use App\Mail\PayoutChequeNotificationMail;


class SubscriptionController extends Controller {

    /**
     * On click of subscription details
     * @param type $id [package id]
     * @return type view
     */
    public function makePayment($id) {
        $data = array();
        $data['user_cards'] = [];
        if (isset($id) && (!empty($id))) {
            $data['id'] = $id;
            $data['package'] = \App\Models\Package::find($id);
            
            $user = Auth::user();
            $userId = Auth::id();
            $data['pricings'] = NULL;
            if(!empty($userId)) {
                $data['pricings'] = \App\Models\Customer_package::join('packages','packages.id','=','customer_packages.package_id')
                    ->join('notices','notices.id','=','customer_packages.notice_id')
                    ->join('customers','customers.id','=','customer_packages.customer_id')
                    ->where(['customer_packages.status'=>1,'customer_packages.package_id'=>$id])
                    ->where('customers.user_id', $userId)
                    ->whereNotIn('notices.master_notice_id', [config('constants.MASTER_NOTICE')['BCOL']['ID'], config('constants.MASTER_NOTICE')['SBC']['ID']])
                    ->whereNull('additional_address')
                    ->groupBy('notices.id')
                    ->orderBy('notices.name', 'asc')
                    ->get(['notices.name','customer_packages.charge']);
            } 
            
            
            if($data['pricings']->count() ==0) {
                $data['pricings'] = \App\Models\Pricing::join('packages','packages.id','=','pricings.package_id')
                ->join('notices','notices.id','=','pricings.notice_id')
                ->where(['pricings.status'=>1,'pricings.package_id'=>$id])
                ->where('pricings.pricing_copied_id',0)
                ->whereNotIn('notices.master_notice_id', [config('constants.MASTER_NOTICE')['BCOL']['ID'], config('constants.MASTER_NOTICE')['SBC']['ID']])
                ->whereNull('additional_address')
                ->groupBy('notices.id')
                ->orderBy('notices.name', 'asc')
                ->get(['notices.name','pricings.charge']);
            }
            
            $data['user_cards'] = [];
            $cards = \App\Models\UserCard::where(['user_id'=>$user->id])->get()->toArray();
            
            if($cards)
            {
                $qbo = new QuickBooksController();       
                $user_cards = $qbo->generateTokens(config('constants.QBO.PAYMENT_SCOPE'),'getSavedCards',['customer_id'=>$user->id,'qbo_customer_id'=>$user->qbo_customer_id,'card_id'=>$cards[0]['card_id']]);   
                if($user_cards['response'] == 'success' && !empty($user_cards['cards']))
                {
                    $data['user_cards'] = $user_cards['cards'];
                }
            }
        }
        
        return view('customer.subscriptions.index', $data);
    }

    public function getpaid(Request $request)
    {

                    
            if(isset($request->card_id) && $request->card_id != '')
            {
                 $user = Auth::user();
                        $card_details = [
                        "amount" => $request->amount,
                        "currency" => "USD",
                        "context" => [
                        "mobile" => "false",
                        "isEcommerce" => "true"
                        ]
                    ];
                $card_details["cardOnFile"] = $request->card_id;
                //$user = Auth::user();
                 
            }
            else
            {
                $validator = Validator::make($request->all(),[
                    'card_number' => 'required|ccn',
                    'credit_card_date' => 'required|ccd',
                    'security_code' => 'required|cvc',
                ],
                [
                    'card_number.ccn' => 'Invalid card number',
                    'credit_card_date.ccd' => 'Invalid card date',
                    'security_code.cvc' =>'Invalid security code'
                ]
                );
                if ($validator->fails()) {
                    //dd('if');
                    return redirect()->back()->withErrors($validator)->withInput();
                }
                else
                {
                   // dd('else');
                    $user = Auth::user();
                        $card_details = [
                        "amount" => $request->amount,
                        "currency" => "USD",
                        "context" => [
                        "mobile" => "false",
                        "isEcommerce" => "true"
                        ]
                    ];
                    $expiry_date = explode('/',$request->credit_card_date);
                    $expiry_month = $expiry_date[0];
                    $expiry_year = $expiry_date[1];
                    $card_number = str_replace(' ','',$request->card_number);
                    $card_details["card"] = [ 
                        "number" => $card_number,
                        "expMonth" => $expiry_month,
                        "expYear" => $expiry_year,
                        "cvc" => $request->security_code,
                    ];
                }
            }

            
            $card_id = ($request->card_id)?$request->card_id:'';
                            //dd($card_details,$user->id,$card_id,$request->package_id);

            $qbo = new QuickBooksController();       
            $response = $qbo->generateTokens(config('constants.QBO.PAYMENT_SCOPE'),'createCharge',['customer_id'=>$user->id,'card_details'=>$card_details,'user_id'=>$user->id,'card_id'=>$card_id,'scope'=>config('constants.QBO.PAYMENT_SCOPE'),'package_id'=>$request->package_id]);
            //dd($response );
            if($response['response'] == 'success')
            {
                $now = date('Y-m-d H:i:s');
                if($request->package_id == 2)
                {
                    $start = date('Y-m-d');
                    $end = date('Y-m-d',strtotime('+1 months'));
                }
                else
                {
                    $start = date('Y-m-d');
                    $end = date('Y-m-d',strtotime('+1 years'));
                }
                
                try
                {
                    $customer_details = Auth::User();
                    $customer_id = $customer_details->customer->id;
                    $insert_array = ['status'=>'1','charges'=>$request->amount,'customer_id'=>$customer_id,'package_id'=>$request->package_id,'type'=>1,'start_date'=>$start,'end_date'=>$end,'created_at'=>$now];
                    \App\Models\Customer_subscription::create($insert_array);
                   
                    return redirect::to('customer/subscription/' . $request->package_id . '/make-payment')->with('success', 'Subscription details saved successfully');
                }
                catch(Exception $e)
                {
                    return redirect::to('customer/subscription/' . $request->package_id . '/make-payment')->with('error', 'Error while saving subscription details');
                }
            }
            return redirect('customer/subscription/'.$response['package_id'].'/make-payment')->with($response['response'],$response['message']);
        
    }
    
    /**
     * Saving of cheque subscription details with removing the old inactive details.
     * @param \App\Http\Requests\SaveChequePost $request
     * @return type redirect with session messages
     */
    public function saveChequeDetails(\App\Http\Requests\SaveChequePost $request) {

        try {
            $data = array();
            $id = $request['id'];
            $package = \App\Models\Package::find($id);
            $customer_id = Auth::User()->customer->id;
            if (isset($package) && (!empty($package)) && (isset($customer_id)) && (!empty($customer_id))) {

                /* Removing the old details */
                $records = \App\Models\Customer_subscription::where(['customer_id'=>$customer_id, 'status'=>0])->get();
                if(isset($records) && (!empty($records))) {
                    foreach($records as $r) {
                        \App\Models\Customer_subscription::find($r->id)->delete();
                    }
                }

                $now = date('Y-m-d H:i:s');
                if($id == 2)
                {
                    $start = date('Y-m-d');
                    $end = date('Y-m-d',strtotime('+1 months'));
                }
                else
                {
                    $start = date('Y-m-d');
                    $end = date('Y-m-d',strtotime('+1 years'));
                }
                /* EOC for the removing the old details */
                $charge = $package['charge'];
                $customersubscription = new \App\Models\Customer_subscription();
                $customersubscription->customer_id = $customer_id;
                $customersubscription->package_id = $id;
                $customersubscription->charges = $charge;
                $customersubscription->type = 2; /* 1 for credit card & 2 for cheque */
                $customersubscription->cheque_no = trim($request['cheque_number']);
                $customersubscription->bank_details = trim($request['bankdetails']);
                $customersubscription->status = 0;
                $customersubscription->start_date = $start;
                $customersubscription->end_date = $end;

                if ($customersubscription->save()) {
                    $admin_email_id = config('constants.TRANSACTION_EMAILS.CONTACT_US_EMAIL');
                    $customersubscription->name=Auth::User()->name;
                    $customersubscription->company_name=Auth::User()->customer->company_name;
                    $customersubscription->email=Auth::User()->email;
                    $customersubscription->package_name=$package->name;

                    Mail::to($admin_email_id)->send(new PayoutChequeNotificationMail($customersubscription));

                    return redirect::to('customer/subscription/' . $id . '/make-payment')->with('success', 'Subscription details saved successfully. Please email us copy of check to: sales@aaanoticetoowner.com.');
                } else {
                    return redirect::to('customer/subscription/' . $id . '/make-payment')->with('error', 'Error while saving subscription details');
                }
            } else {
                return redirect::to('customer/subscription/' . $id . '/make-payment')->with('error', 'Error while saving subscription details');
            }
        } catch (\Exception $ex) {
            dd($ex->getMessage().",line: ".$ex->getLine().",file: ".$ex->getFile());
            return redirect::to('pricing');
        }
    }
     /********* delete customer card ********/
    public function deleteCardDetails(Request $request){
        $user = Auth::user();
        $cards = \App\Models\UserCard::where(['user_id'=>$user->id])->get()->toArray();
        
        if($cards)
        {
            $qbo = new QuickBooksController();       
            $user_cards = $qbo->generateTokens(config('constants.QBO.PAYMENT_SCOPE'),'deleteCard',['customer_id'=>$user->id,'qbo_customer_id'=>$user->qbo_customer_id,'card_id'=>$request['card_id']]);
            return response()->json(array('result' => 'success', 'message' => 'Card deleted successfully'));
        }

    }
    
}
