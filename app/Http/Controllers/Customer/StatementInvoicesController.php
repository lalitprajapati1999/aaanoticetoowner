<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PDF;

/**
 * Description of StatementInvoicesConroller
 *
 * @author neosoft
 */
class StatementInvoicesController extends Controller {

    public function getStatementInvoices() {
//       $WorkOrderFields = \App\Models\WorkOrderFields::select('work_orders.parent_id', 'work_orders.created_at', 'work_orders.user_id', 'work_orders.customer_id', 'work_orders.notice_id as notice_id', 'work_orders.status', 'work_orders.user_id', 'work_orders.account_manager_id', 
//                                'work_order_fields.workorder_id', 'work_order_fields.notice_id',
//                                 \DB::raw("(GROUP_CONCAT(name SEPARATOR '||')) as `field_names`"), DB::raw("(GROUP_CONCAT(value SEPARATOR '||')) as `field_values`"))
//                         ->join('notice_fields', 'notice_fields.id', '=', 'work_order_fields.notice_field_id')
//                        ->join('work_orders', 'work_orders.id', '=', 'work_order_fields.workorder_id')
//                        ->groupBy('work_order_fields.workorder_id', 'work_order_fields.notice_id')
//                        ->where('work_orders.customer_id','=',Auth::user()->customer->id)
//                        ->where('work_orders.status','=',5)
//                        ->get()->toArray();
//      
//
//        $result = [];
//        if (isset($WorkOrderFields) && !empty($WorkOrderFields)) {
//            foreach ($WorkOrderFields as $fields_data) {
//                $notice_name = \App\Models\Notice::find($fields_data['notice_id']);
//                $fields_data['notice_name'] = $notice_name->name;
//                $field_names = explode('||', $fields_data['field_names']);
//                $field_values = explode('||', $fields_data['field_values']);
//                $field_names_values = array_combine($field_names, $field_values);
//                $field_names_values['default'] = '';
//                $result[] = array_merge($fields_data, $field_names_values);
//            }
//        }
//        if(isset($result) && !empty($result)){
//            foreach ($result AS $k=>$each_result){
//               if(isset($each_result['contracted_by'])){
//                   $result[$k]['contracted_by'] = $each_result['contracted_by'];
//               }else{
//                   $result[$k]['contracted_by'] = '';
//               }
//               
//                 //Recipients Info 
//                 $recipients = \App\Models\Recipient::where('work_order_id', '=',$each_result['workorder_id'])
//                                                    ->get();
//                 
//                 if(isset($recipients) && count($recipients) >0){
//                     foreach($recipients AS $k1=>$v){
//                            $state_name = \App\Models\State::find($v->state_id);
//                                $v->state = $state_name->name;
//                                $city_name = \App\Models\City::find($v->city_id);
//                                 $v->city = $city_name->name;
//                     }
//                        $result[$k]['recipients'] = $recipients;
//                 }
//                 
//            }
//        }
//        dd($result);
        return view('customer.statement_invoices.index');
    }

    public static function getStatementInvoicesFilterData(Request $request) {
        $WorkOrderFields = \App\Models\WorkOrderFields::select('work_orders.parent_id', 'work_orders.rush_hour_charges', 'work_orders.rush_hour_amount', 'work_orders.created_at', 'work_orders.user_id', 'work_orders.customer_id', 'work_orders.notice_id as notice_id', 'work_orders.status', 'work_orders.user_id', 'work_orders.account_manager_id', 'work_order_fields.workorder_id', 'work_order_fields.notice_id', \DB::raw("(GROUP_CONCAT(name SEPARATOR '||')) as `field_names`"), DB::raw("(GROUP_CONCAT(value SEPARATOR '||')) as `field_values`"))
                        ->join('notice_fields', 'notice_fields.id', '=', 'work_order_fields.notice_field_id')
                        ->join('work_orders', 'work_orders.id', '=', 'work_order_fields.workorder_id')
                        ->groupBy('work_order_fields.workorder_id', 'work_order_fields.notice_id')
                        ->where('work_orders.customer_id', '=', Auth::user()->customer->id)
// ->where('work_orders.status', '=', 5)
                        ->whereIn('work_orders.status', ['6', '7'])
                        ->whereMonth('work_orders.created_at', '=', date($request->month))
                        ->whereYear('work_orders.created_at', '=', date($request->year))
                        ->get()->toArray();


        $result = [];
        if (isset($WorkOrderFields) && !empty($WorkOrderFields)) {
            foreach ($WorkOrderFields as $fields_data) {

                $notice_name = \App\Models\Notice::find($fields_data['notice_id']);

                $fields_data['notice_name'] = $notice_name->name;
                $field_names = explode('||', $fields_data['field_names']);
                $field_values = explode('||', $fields_data['field_values']);
                $field_names_values = array_combine($field_names, $field_values);
                $field_names_values['default'] = '';

                $notice = \App\Models\Notice::join('work_orders', 'work_orders.notice_id', '=', 'notices.id')
                        ->join('notice_fields', 'notice_fields.notice_id', '=', 'notices.id')
                        ->where(['work_orders.id' => $fields_data['workorder_id']])
                        ->get([\DB::raw('notice_fields.id as notice_field_id'), \DB::raw('work_orders.id as workorder_id'), \DB::raw('notice_fields.name as notice_field_name'), \DB::raw('notices.name as notice_name'), \DB::raw('notices.id as notice_id1')])
                        ->toArray();


                $recipients = \App\Models\Recipient::join('work_orders', 'work_orders.id', '=', 'recipients.work_order_id')
                        ->where(['work_orders.id' => $fields_data['workorder_id']])
                        ->pluck('recipients.id');

                $generated_labels = \App\Models\StampsLabel::whereIn('recipient_id', $recipients)
                        ->where(['generated_label' => 'yes'])
                        ->groupBy(['type_of_label'])
                        ->get([\DB::raw('count(id) as label_count'), 'tracking_number', 'ShipDate', 'DeliveryDate', \DB::raw('SUM(Amount) as total_amount'), 'type_of_label', 'type_of_mailing', 'base_amount_charges'])
                        ->toArray();
                $mailing_info = [];
                if (!empty($generated_labels)) {
                    foreach ($generated_labels as $generated_labels_val) {
                        $mailing_info[] = ['label_count' => $generated_labels_val['label_count'], 'type_of_label' => $generated_labels_val['type_of_label'], 'total_amount' => $generated_labels_val['total_amount'], 'base_amount_charges' => $generated_labels_val['base_amount_charges']];
                    }
                } else {
                    $mailing_info[] = ['label_count' => count($recipients), 'type_of_label' => "-", 'total_amount' => "0", 'base_amount_charges' => "0"];
                }
                $label_count = "";
                $type_of_label = "";
                $total_amount = "";
                $base_amount = "";
                foreach ($mailing_info as $mailing_info_val) {
                    $label_count = $label_count . $mailing_info_val['label_count'] . ",";
                    $type_of_label = $type_of_label . $mailing_info_val['type_of_label'] . ",";
                    $total_amount = $total_amount . $mailing_info_val['total_amount'] . ",";
                    $base_amount = $mailing_info_val['base_amount_charges'];
                }

                $result[] = array_merge($fields_data, $field_names_values, ['label_count' => $label_count], ['type_of_label' => $type_of_label], ['total_amount' => $base_amount . "," . $total_amount]);
            }
        }
        if (isset($result) && !empty($result)) {
            foreach ($result AS $k => $each_result) {
                if (isset($each_result['contracted_by'])) {
                    $result[$k]['contracted_by'] = $each_result['contracted_by'];
                } else {
                    $result[$k]['contracted_by'] = '';
                }

//Recipients Info 
                $recipients = \App\Models\Recipient::where('work_order_id', ' = ', $each_result['workorder_id'])
                        ->get();

                if (isset($recipients) && count($recipients) > 0) {
                    foreach ($recipients AS $k1 => $v) {
                        $state_name = \App\Models\State::find($v->state_id);
                        $v->state = $state_name['name'];
                        $city_name = \App\Models\City::find($v->city_id);
                        $v->city = $city_name['name'];
                    }
                    $result[$k]['recipients'] = $recipients;
                }
            }
        }
// dd($result);

        return Datatables::of($result)
                        ->addColumn('options', function($each_result) {
                            return view('customer.statement_invoices.actions', compact('each_result'))->render();
                        })
                        ->addColumn('document', function($each_result) {
                            return view('customer.statement_invoices.document', compact('each_result'))->render();
                        })->addColumn('your_job_reference_no', function($result) {
                            if (isset($result['your_job_reference_no']))
                                return $result['your_job_reference_no'];
                            else
                                return "-";
                        })
                        ->rawColumns(['options', 'document', 'your_job_reference_no'])
                        ->make(true);
    }

    /**
     * Function is used to print statement invoice
     * 
     * @param type $work_order_id
     */
    public static function printStatementInvoice(Request $request) {
        if (!isset($request->month)) {

            $request->month = date('m');
        }
        if (!isset($request->year)) {
            $request->year = date('Y');
        }

        $WorkOrderFields = \App\Models\WorkOrderFields::select('work_orders.parent_id', 'work_orders.rush_hour_charges', 'work_orders.rush_hour_amount', 'work_orders.created_at', 'work_orders.user_id', 'work_orders.customer_id', 'work_orders.notice_id as notice_id', 'work_orders.status', 'work_orders.user_id', 'work_orders.account_manager_id', 'work_order_fields.workorder_id', 'work_order_fields.notice_id', \DB::raw("(GROUP_CONCAT(name SEPARATOR '||')) as `field_names`"), DB::raw("(GROUP_CONCAT(value SEPARATOR '||')) as `field_values`"))
                        ->join('notice_fields', 'notice_fields.id', '=', 'work_order_fields.notice_field_id')
                        ->join('work_orders', 'work_orders.id', '=', 'work_order_fields.workorder_id')
                        ->groupBy('work_order_fields.workorder_id', 'work_order_fields.notice_id')
                        ->where('work_orders.customer_id', '=', Auth::user()->customer->id)
// ->where('work_orders.status', '=', 5)
                        ->whereIn('work_orders.status', ['6', '7'])
                        ->whereMonth('work_orders.created_at', '=', date($request->month))
                        ->whereYear('work_orders.created_at', '=', date($request->year))
                        ->get()->toArray();


        $result = [];
        $final_total = 0;
        if (isset($WorkOrderFields) && !empty($WorkOrderFields)) {
            foreach ($WorkOrderFields as $fields_data) {

                $notice_name = \App\Models\Notice::find($fields_data['notice_id']);

                $fields_data['notice_name'] = $notice_name->name;
                $field_names = explode('||', $fields_data['field_names']);
                $field_values = explode('||', $fields_data['field_values']);
                $field_names_values = array_combine($field_names, $field_values);
                $field_names_values['default'] = '';

                $notice = \App\Models\Notice::join('work_orders', 'work_orders.notice_id', '=', 'notices.id')
                        ->join('notice_fields', 'notice_fields.notice_id', '=', 'notices.id')
                        ->where(['work_orders.id' => $fields_data['workorder_id']])
                        ->get([\DB::raw('notice_fields.id as notice_field_id'), \DB::raw('work_orders.id as workorder_id'), \DB::raw('notice_fields.name as notice_field_name'), \DB::raw('notices.name as notice_name'), \DB::raw('notices.id as notice_id1')])
                        ->toArray();


                $recipients = \App\Models\Recipient::join('work_orders', 'work_orders.id', '=', 'recipients.work_order_id')
                        ->where(['work_orders.id' => $fields_data['workorder_id']])
                        ->pluck('recipients.id');

                $generated_labels = \App\Models\StampsLabel::whereIn('recipient_id', $recipients)
                        ->where(['generated_label' => 'yes'])
                        ->groupBy(['type_of_label'])
                        ->get([\DB::raw('count(id) as label_count'), 'tracking_number', 'ShipDate', 'DeliveryDate', \DB::raw('SUM(Amount) as total_amount'), 'type_of_label', 'type_of_mailing', 'base_amount_charges'])
                        ->toArray();
                $mailing_info = [];
                if (!empty($generated_labels)) {
                    foreach ($generated_labels as $generated_labels_val) {
                        $mailing_info[] = ['label_count' => $generated_labels_val['label_count'], 'type_of_label' => $generated_labels_val['type_of_label'], 'total_amount' => $generated_labels_val['total_amount'], 'base_amount_charges' => $generated_labels_val['base_amount_charges']];
                    }
                } else {
                    $mailing_info[] = ['label_count' => count($recipients), 'type_of_label' => "-", 'total_amount' => "0", 'base_amount_charges' => '0'];
                }
                $label_count = "";
                $type_of_label = "";
                $total_amount = "";
                $base_amount = "";
                foreach ($mailing_info as $mailing_info_val) {
                    $label_count = $label_count . $mailing_info_val['label_count'] . "<br/>";
                    $type_of_label = $type_of_label . $mailing_info_val['type_of_label'] . "<br/>";
                    $total_amount = $total_amount . $mailing_info_val['total_amount'] . "<br/>";
                    $final_total = $final_total + $mailing_info_val['total_amount'];
                    $base_amount = $mailing_info_val['base_amount_charges'];
                }

                $result[] = array_merge($fields_data, $field_names_values, ['label_count' => $label_count], ['type_of_label' => $type_of_label], ['total_amount' => $base_amount . "<br/>" . $total_amount]);
            }
        }
        if (isset($result) && !empty($result)) {
            foreach ($result AS $k => $each_result) {
                if (isset($each_result['contracted_by'])) {
                    $result[$k]['contracted_by'] = $each_result['contracted_by'];
                } else {
                    $result[$k]['contracted_by'] = '';
                }

                //Recipients Info 
                $recipients = \App\Models\Recipient::where('work_order_id', ' = ', $each_result['workorder_id'])
                        ->get();

                if (isset($recipients) && count($recipients) > 0) {
                    foreach ($recipients AS $k1 => $v) {
                        $state_name = \App\Models\State::find($v->state_id);
                        $v->state = $state_name['name'];
                        $city_name = \App\Models\City::find($v->city_id);
                        $v->city = $city_name['name'];
                    }
                    $result[$k]['recipients'] = $recipients;
                }
            }
        }
        // dd(Auth::user()->customer);
        $statement_year = $request->year;
        $statement_month = date('F', mktime(0, 0, 0, $request->month, 10));
        $customer_company_name = \App\Models\Customer::find(Auth::User()->customer->id);
        $company_city_name = \App\Models\City::find($customer_company_name->mailing_city_id);
        $company_state_name = \App\Models\State::find($customer_company_name->mailing_state_id);
        return view('customer.statement_invoices.print_invoice', compact('result', 'company_city_name', 'customer_company_name', 'final_total', 'statement_month', 'statement_year', 'company_state_name'));
        //dd($file_name);
    }

    public function generateInvoice($work_order_id) {
        $user = \Auth::user();
        $generated_labels = [];
        $notice = [];
        $notice_fields = [];
        $labels = [];
        $quickbooks_labels = [];

        if ($user->qbo_customer_id == null) {
            return redirect('customer/statement-invoices')->with('error', 'Quickbooks customer id not found');
        }

        $work_order = \App\Models\WorkOrder::where(['id' => $work_order_id])->get(['rush_hour_charges'])->toArray();

        $notice = \App\Models\Notice::join('work_orders', 'work_orders.notice_id', ' = ', 'notices.id')
                ->join('notice_fields', 'notice_fields.notice_id', ' = ', 'notices.id')
                ->where(['work_orders.id' => $work_order_id])
                ->get([\DB::raw('notice_fields.id as notice_field_id'), \DB::raw('work_orders.id as workorder_id'), \DB::raw('notice_fields.name as notice_field_name'), \DB::raw('notices.name as notice_name'), \DB::raw('notices.id as notice_id1')])
                ->toArray();

        $recipients = \App\Models\Recipient::join('work_orders', 'work_orders.id', ' = ', 'recipients.work_order_id')
                ->where(['work_orders.id' => $work_order_id])
                ->pluck('recipients.id');

        //Get user subscription
        $customer_subscription = \App\Models\Customer_subscription::join('packages', 'packages.id', ' = ', 'customer_subscriptions.package_id')->join('pricings', 'pricings.package_id', ' = ', 'packages.id')->where(['customer_id' => $user->customer->id])->orderBy('customer_subscriptions.id', 'desc')->limit(1)->get()->toArray();

        if (!empty($customer_subscription)) {
            $label_limits = \App\Models\Package::where(['packages.id' => $customer_subscription[0]['package_id'], 'pricings.notice_id' => $notice['notice_id1']])->first(['lower_limit', 'upper_limit', 'charge']);
        } else {
            $label_limits = [['upper_limit' => 50, 'lower_limit' => 1, 'charge' => 30], ['upper_limit' => 100, 'lower_limit' => 50, 'charge' => 20]];
        }


        if (!empty($notice) && !empty($recipients) && !empty($work_order)) {
            $generated_labels = \App\Models\StampsLabel::whereIn('recipient_id', $recipients)
                    ->where(['generated_label' => 'yes'])
                    ->groupBy(['type_of_label', 'type_of_mailing'])
                    ->get([\DB::raw('count(id) as label_count'), 'tracking_number', 'ShipDate', 'DeliveryDate', \DB::raw('SUM(Amount) as total_amount'), 'type_of_label', 'type_of_mailing'])
                    ->toArray();

            if (!empty($generated_labels)) {
                $label_count = count($generated_labels);
                $base_amount = 150;
                foreach ($label_limits as $label_limit) {
                    if ($label_count >= $label_limit['lower_limit'] && $label_count < $label_limit['upper_limit']) {
                        $base_amount = $label_limit['charge'];
                    }
                }

                foreach ($notice as $key => $val) {
                    $field_value = \App\Models\WorkOrderFields::where(['notice_field_id' => $val['notice_field_id'], 'workorder_id' => $val['workorder_id']])->pluck('value')->toArray();
                    $notice_fields[$val['notice_field_name']] = $field_value[0];

                    $description = $val['notice_name'];
                    $description = 'test notice name';
                    if (isset($notice_fields['contracted_by'])) {
                        $description .= '<br/>Project Address - <br/>' .
                                $description .= $notice_fields['project_address'] . '<br/>';
                    }
                    $description .= (isset($notice_fields['contracted_by'])) ? 'Contracted By - ' . $notice_fields['contracted_by'] : '';
                    $description .= (isset($notice_fields['parent_work_order'])) ? 'Parent Work Order Number - ' . $notice_fields['parent_work_order'] : '';
                    $description .= (isset($notice_fields['work_order_id'])) ? 'Current Work Order Number - ' . $notice_fields['work_order_id'] : '';
                    $description .= (isset($notice_fields['your_job_reference_no'])) ? 'Job Reference Number - ' . $notice_fields['your_job_reference_no'] : '';
                    $quickbooks_labels = [
                        "Line" => [
                            [
                                "Amount" => $base_amount,
                                "Description" => nl2br($description),
                                "DetailType" => "SalesItemLineDetail",
                                "SalesItemLineDetail" => [
                                    "ItemRef" => [
                                        "value" => 23,
                                        "name" => "Notice"
                                    ],
                                    "UnitPrice" => 30,
                                    "Qty" => 1
                                ]
                            ],
                        ]
                    ];

                    $label_description = ['next day delivery' => 'these are high priority delivery mails', 'priority mail' => 'these are priority mails', 'rush hour charges' => 'Rush Fee', 'certified mail' => 'these are certified mails', 'firm mail' => 'Firm Mails'];
                    $item_ref = [
                        'certified mail' => [
                            "value" => 24,
                            "name" => "Certified Mail"
                        ],
                        'next day delivery' => [
                            "value" => 20,
                            "name" => "Next Day Delivery"
                        ],
                        'priority mail' => [
                            "value" => 25,
                            "name" => "Priority Mail"
                        ],
                        'rush hour charges' => [
                            "value" => 26,
                            "name" => "Rush Hour Charges"
                        ],
                        'firm mail' => [
                            "value" => 21,
                            "name" => "Firm Mail"
                        ],
                        'next day delivery' => [
                            "value" => 20,
                            "name" => "Next Day Delivery"
                        ]
                    ];

                    //check if we need to add rush hour charges
                    if (isset($work_order[0]['rush_hour_charges']) && $work_order[0]['rush_hour_charges'] == 'yes') {
                        $quickbooks_labels["Line"][] = [
                            'Amount' => 10,
                            'Description' => $label_description['rush hour charges'],
                            "DetailType" => "SalesItemLineDetail",
                            "SalesItemLineDetail" => [
                                "ItemRef" => $item_ref['rush hour charges'],
                                "Qty" => 1,
                                "UnitPrice" => 10,
                            ]
                        ];
                    }

                    foreach ($generated_labels as $labels) {
                        //Add ons used in stamps label creation
                        if (isset($labels['type_of_mailing'])) {
                            $quickbooks_labels["Line"][] = [
                                'Amount' => $labels['total_amount'],
                                'Description' => ($labels['type_of_mailing']) ? $label_description[$labels['type_of_mailing']] : '',
                                "DetailType" => "SalesItemLineDetail",
                                "SalesItemLineDetail" => [
                                    "ItemRef" => $item_ref[$labels['type_of_mailing']
                                    ],
                                    // "UnitPrice" => $labels['Amount'],
                                    "Qty" => $labels['label_count']
                                ]
                            ];
                        }
                        //Check if firm mail or next day delivery
                        if (isset($labels['type_of_label']) && $labels['type_of_label'] != 'stamps') {
                            $quickbooks_labels["Line"][] = [
                                'Amount' => $labels['total_amount'],
                                'Description' => ($labels['type_of_label']) ? $label_description[$labels['type_of_label']] : '',
                                "DetailType" => "SalesItemLineDetail",
                                "SalesItemLineDetail" => [
                                    "ItemRef" => $item_ref[$labels['type_of_label']],
                                    "Qty" => $labels['label_count']
                                ]
                            ];
                        }
                    }
                    $quickbooks_labels["CustomerRef"] = [
                        "value" => $user->qbo_customer_id
                    ];
                }
            }

            if (!empty($quickbooks_labels)) {
                $qbo = new \App\Http\Controllers\QuickBooksController();
                $qbo_response = $qbo->generateTokens(config('constants.QBO.SCOPE'), 'createInvoice', ['customer_id' => $user->id, 'invoice_details' => $quickbooks_labels]);
                return redirect('customer/statement-invoices')->with($qbo_response['response'], $qbo_response['message']);
            } else {
                return redirect('customer/statement-invoices')->with('error', 'Could not generate invoice');
            }
        }
    }

}
