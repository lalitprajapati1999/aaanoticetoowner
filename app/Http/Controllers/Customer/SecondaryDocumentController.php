<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Models\SecondaryDocument;
use App\Models\SecondaryDocumentRecipient;
use DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Str;
use PDF;
use Illuminate\Support\Facades\Mail;
use App\Mail\SecondaryDocumentNote;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\Mail\SecondaryDocumentAttachmentMail;

class SecondaryDocumentController extends Controller {

    /**
     * Display listing of resource
     * 
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $data = [];
        $data['notices'] = \App\Models\Notice::select('id','name')->where('type',3)->where('status',1)->where('state_id',10)->get();
        $data['state_arr'] = \App\Models\State::select()->orderBy('name')->get();
       
        return view('customer.secondary_document.list', $data);
    }

    public function getCustomeFilterData(Request $request) {//dd($request->all());

        if (Auth::user()->hasRole('account-manager')) {

            $customers = \App\Models\Customer::where('account_manager_id', '=', Auth::user()->id)
                            ->select('customers.id')
                            ->get()->toArray();

            $customer_id = [];
            foreach ($customers AS $each) {
                array_push($customer_id, $each['id']);
            }
          $secondaryDocuments = \App\Models\SecondaryDocumentFields::select('notices.master_notice_id as master_notice_id','notices.name as notice_name', 'notices.is_rescind as rescind_notice', '.is_rescind as rescind_work_order', 'notices.type', 'secondary_documents.work_order_no','secondary_documents.id as secondary_doc_id','secondary_documents.file_name','secondary_documents.created_at','usr.name as customer_name', 'secondary_documents.customer_id', 'usr.name as account_manager_name', 'secondary_documents.notice_id as notice_id', 'secondary_documents.status','secondary_documents.account_manager_id', 'secondary_document_fields.secondary_document_id','customers.company_name as company_name', 'secondary_document_fields.notice_id', DB::raw("(GROUP_CONCAT(notice_fields.name SEPARATOR '||')) as `field_names`"), DB::raw("(GROUP_CONCAT(value SEPARATOR '||')) as `field_values`"))
                ->rightjoin('notice_fields', 'notice_fields.id', '=', 'secondary_document_fields.notice_field_id')
                ->leftjoin('secondary_documents', 'secondary_documents.id', '=', 'secondary_document_fields.secondary_document_id')
                ->leftjoin('notices', 'notices.id', '=', 'secondary_documents.notice_id')
                ->leftjoin('customers', 'customers.id', '=', 'secondary_documents.customer_id')
                ->leftjoin('users as usr', 'usr.id', '=', 'secondary_documents.account_manager_id')->groupBy('secondary_document_fields.secondary_document_id', 'secondary_document_fields.notice_id')
//                ->leftjoin('recipients','recipients.work_order_id','=','secondary_document.id')
//                ->leftjoin('categories','categories.id','=','recipients.category_id')
//                ->whereIn('recipients.category_id',[1,2])
                ->where('secondary_documents.account_manager_id', Auth::user()->id);

       

        /*$secondaryDocuments = \App\Models\SecondaryDocument::select('id', 'customer_id', 'date', 'customer', 'work_order_no', 'address', 'amount', 'due_date', 'first_name', 'signature', 'comment', 'type', 'created_at', 'updated_at', 'file_name')
                    ->whereIn('customer_id', $customer_id);*/
        } elseif (Auth::user()->hasRole('admin')) {
            $secondaryDocuments = \App\Models\SecondaryDocumentFields::select('notices.master_notice_id as master_notice_id','notices.name as notice_name', 'notices.is_rescind as rescind_notice', '.is_rescind as rescind_work_order', 'notices.type','secondary_documents.id as secondary_doc_id','secondary_documents.file_name','secondary_documents.work_order_no', 'secondary_documents.created_at', 'usr.name as customer_name', 'secondary_documents.customer_id', 'usr.name as account_manager_name', 'secondary_documents.notice_id as notice_id', 'secondary_documents.status','secondary_documents.account_manager_id', 'secondary_document_fields.secondary_document_id','customers.company_name as company_name', 'secondary_document_fields.notice_id', DB::raw("(GROUP_CONCAT(notice_fields.name SEPARATOR '||')) as `field_names`"), DB::raw("(GROUP_CONCAT(value SEPARATOR '||')) as `field_values`"))
                ->rightjoin('notice_fields', 'notice_fields.id', '=', 'secondary_document_fields.notice_field_id')
                ->leftjoin('secondary_documents', 'secondary_documents.id', '=', 'secondary_document_fields.secondary_document_id')
                ->leftjoin('notices', 'notices.id', '=', 'secondary_documents.notice_id')
               ->leftjoin('customers', 'customers.id', '=', 'secondary_documents.customer_id')
                ->leftjoin('users as usr', 'usr.id', '=', 'secondary_documents.account_manager_id')->groupBy('secondary_document_fields.secondary_document_id', 'secondary_document_fields.notice_id');
//                ->leftjoin('recipients','recipients.work_order_id','=','secondary_document.id')
//                ->leftjoin('categories','categories.id','=','recipients.category_id')
//                ->whereIn('recipients.category_id',[1,2])
               
           /* $secondaryDocuments = \App\Models\SecondaryDocument::select('id', 'customer_id', 'date', 'customer', 'work_order_no', 'address', 'amount', 'due_date', 'first_name', 'signature', 'comment', 'type', 'created_at', 'updated_at', 'file_name');*/
        } else {
             $secondaryDocuments = \App\Models\SecondaryDocumentFields::select('notices.master_notice_id as master_notice_id','notices.name as notice_name', 'notices.is_rescind as rescind_notice', '.is_rescind as rescind_work_order', 'notices.type', 'secondary_documents.work_order_no', 'secondary_documents.created_at','secondary_documents.file_name','secondary_documents.id as secondary_doc_id', 'usr.name as customer_name', 'secondary_documents.customer_id', 'usr.name as account_manager_name', 'secondary_documents.notice_id as notice_id', 'secondary_documents.status','secondary_documents.created_at as created_date', 'secondary_documents.account_manager_id', 'secondary_document_fields.secondary_document_id', 'secondary_document_fields.notice_id','customers.company_name as company_name', DB::raw("(GROUP_CONCAT(notice_fields.name SEPARATOR '||')) as `field_names`"), DB::raw("(GROUP_CONCAT(value SEPARATOR '||')) as `field_values`"))
                ->rightjoin('notice_fields', 'notice_fields.id', '=', 'secondary_document_fields.notice_field_id')
                ->leftjoin('secondary_documents', 'secondary_documents.id', '=', 'secondary_document_fields.secondary_document_id')
                ->leftjoin('notices', 'notices.id', '=', 'secondary_documents.notice_id')
                ->leftjoin('customers', 'customers.id', '=', 'secondary_documents.customer_id')
                ->leftjoin('users as usr', 'usr.id', '=', 'secondary_documents.account_manager_id')->groupBy('secondary_document_fields.secondary_document_id', 'secondary_document_fields.notice_id')->where('secondary_documents.customer_id', '=', Auth::user()->customer->id);
//                ->leftjoin('recipients','recipients.work_order_id','=','secondary_document.id')
//                ->leftjoin('categories','categories.id','=','recipients.category_id')
//                ->whereIn('recipients.category_id',[1,2])
               
           /* $secondaryDocuments = \App\Models\SecondaryDocument::select('id', 'customer_id', 'date', 'customer', 'work_order_no', 'address', 'amount', 'due_date', 'first_name', 'signature', 'comment', 'type', 'created_at', 'updated_at', 'file_name')
                    ->where('customer_id', '=', Auth::user()->customer->id);*/
        }
        if (($request->has('from_date')) && ($request->get('from_date') != NULL)) {
            $secondaryDocuments->whereDate('secondary_documents.created_at', '>=', date('Y-m-d', strtotime($request->get('from_date'))));
        }
        if (($request->has('to_date')) && ($request->get('to_date') != NULL)) {
            $secondaryDocuments->WhereDate('secondary_documents.created_at', '<=', date('Y-m-d', strtotime($request->get('to_date'))));
        }
          $secondaryDocuments = $secondaryDocuments->get()->toArray();
            $dataTable = [];
          if (isset($secondaryDocuments) && !empty($secondaryDocuments)) {
            foreach ($secondaryDocuments as $fields_data) {
                $field_names = explode('||', $fields_data['field_names']);
                $field_values = explode('||', $fields_data['field_values']);
                $field_names_values = (count($field_names) == count($field_values)) ? array_combine($field_names, $field_values) : [];
                $field_names_values['default'] = '';
                $dataTable[] = array_merge($fields_data, $field_names_values);
            }
          }

       
       
     //dd($dataTable);
        return DataTables::of($dataTable)
                        ->addColumn('action', function($secondaryDocument) {
                            return view('customer.secondary_document.actions', compact('secondaryDocument'))->render();
                        })
                        ->filter(function($query) use ($request) {

//                            if (($request->has('customer_name')) && ($request->get('customer_name') != NULL)) {
//                                $query->where('customer', 'like', "%{$request->get('customer_name')}%");
//                            }
//                            if (($request->has('project_address')) && ($request->get('project_address') != NULL)) {
//                                $query->where('address', 'like', "%{$request->get('project_address')}%");
//                            }
//
                           /* if (($request->has('from_date')) && ($request->get('from_date') != NULL)) {
                                $query->whereDate('date', '>=', date('Y-m-d', strtotime($request->get('from_date'))));
                                //\DB::raw()
                            }
                            if (($request->has('to_date')) && ($request->get('to_date') != NULL)) {
                                $query->whereDate('date', '<=', date('Y-m-d', strtotime($request->get('to_date'))));
                            }*/
                        })
                        ->make(true);
    }


    public function create(Request $request, $partial_final_type) {

        $data['tab'] = 'project';
        $noticeId = $request->notice_id;
        $stateId  = $request->state_id;
        /* get notice fileds list from notice_id and state_id */
        $data['notice_fields'] = \App\Models\NoticeField::select()->where('state_id', $stateId)->where('notice_id',$noticeId)->orderBy('sort_order', 'asc')->get();
        $notice = \App\Models\Notice::select('name', 'id')->where('id', $request->notice_id)->get()->toArray();
        $data['notice'] = $notice[0];                                   
       // $data['notice_fields_arr'] = $notice_fields_arr;
      $data['cities'] = \App\Models\City::select()->get();
        $data['states'] = \App\Models\State::select()->orderBy('name')->get();
        $data['categories'] = \App\Models\Category::select()->get();
        $data['project_roles'] = \App\Models\ProjectRoles::select()->orderBy('name', 'ASC')->get();
        $data['recipients'] = 0;
        $data['count'] = 0;
        $data['countries'] = \App\Models\Country::select()->get();
        $data['secondary_document_id'] = 0;
        $data['partial_final_type'] = $partial_final_type;
        $data['categories'] = \App\Models\Category::select()->get();
        $data['project_roles'] = \App\Models\ProjectRoles::select()->orderBy('name', 'ASC')->get();
//        $data['names'] = \App\Models\ContactDetail::select()
//                ->where('contact_details.type', '=', 0)
//                ->where('contacts.customer_id', '=', Auth::user()->customer->id)
//                ->Join('contacts', 'contacts.id', '=', 'contact_details.contact_id')
//                ->get();

        if (Auth::user()->hasRole('account-manager')) {
            $data['names'] = '';
        } else {
            $data['names'] = \App\Models\Contact::select('contacts.id', 'contacts.company_name')
                    //  ->where('contact_details.type', '=', 0)
                    ->where('contacts.customer_id', '=', Auth::user()->customer->id)
                    // ->Join('contact_details', 'contact_details.contact_id', '=', 'contacts.id')
                    ->orderBy('contacts.id', 'DESC')
                    ->get();
        }

        if (Auth::user()->hasRole('account-manager')) {

            $customers = \App\Models\Customer::leftjoin('users', 'users.id', '=', 'customers.user_id')
                    ->select('customers.id', 'customers.company_name')
                    ->where('account_manager_id', '=', Auth::user()->id)->orderBy('customers.company_name', 'ASC')
                    ->get();
            $data['customers'] = $customers;
        }
        if (Auth::user()->hasRole('admin')) {

            $customers = \App\Models\Customer::leftjoin('users', 'users.id', '=', 'customers.user_id')->orderBy('customers.company_name', 'ASC')
                    ->select('customers.id', 'customers.company_name')
                    ->get();
            $data['customers'] = $customers;
        }
        if (Auth::user()->hasRole('account-manager') || Auth::user()->hasRole('admin')) {


            $data['officers_directors'] = [];
        } else {
            $data['officers_directors'] = \App\Models\CustomerAgent::where('customer_id', '=', Auth::user()->customer->id)
                    ->get();
        }

        return view('customer.secondary_document.create', $data);
    }
     public function store(Request $request) {
      
        $fields = array_merge($request->except(['_token','type','doc_input','edit_document_name','edit_doc_id','remove_file','secondary_doc_attachment','work_order_attachment_id','remove_doc','partial_final_type','submit','recipient','notice_id','work_order_no','parent_country','contracted_by_exist']));
        $fieldsData = [];
        $values = [];
        $i = 0;
        foreach ($fields as $key => $value) {
            $field = explode("_", $key);
            $fieldsData[$i] = $field['0'];
            $values[$i] = $value;
            $i++;
        }

        if ($request->submit == "continue") {

            $rules = [//'customer' => 'required',
//            'work_order_no' => 'required',
               // 'project_address' => 'required',
                // 'project_country' => 'required',
              //  'amount' => 'required',
                //'amount_text' => 'required',
//                'due_date' => 'required',
//            'first_name' => 'required',
              //  'signature' => 'required',
//                'comment' => 'required'
            ];
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }
        } elseif ($request->submit == 'save_for_later') {
            if (Auth::user()->hasRole('account-manager') || Auth::user()->hasRole('admin')) {
                $rules = ['customer_id' => 'required'
                ];
                $validator = Validator::make(Input::all(), $rules, [
                            'customer_id.required' => 'Please select customer'
                ]);
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator)->withInput();
                }
            }
        }

        if (Auth::user()->hasRole('customer')) {
            $request->request->set('customer_id', Auth::user()->customer->id);
            $customer_id = Auth::user()->customer->id;
            $account_manager_id = Auth::user()->customer->account_manager_id;
        } else {
            $request->request->set('customer_id', $request->customer_id);
            $customer_id = $request->customer_id;
            $userId = Auth::user()->id;
            $account_manager_id = Auth::user()->id;
        }
/*
        $request->request->set('project_country', $request->county_id);*/
        //$request->request->set('date',date('Y-m-d', strtotime($request->date)));
       /* $request->request->set('due_date', date('m-d-Y', strtotime($request->due_date)));*/
      
        $SecondaryDocument = new SecondaryDocument();
        $SecondaryDocument->customer_id = $customer_id;
        $SecondaryDocument->order_no = str_random(5);
        $SecondaryDocument->notice_id = $request->notice_id;
         $SecondaryDocument->work_order_no = $request->work_order_no;
        $SecondaryDocument->account_manager_id = $account_manager_id;
        if ($request->submit == "continue") {
            $SecondaryDocument->status = 1;

            $data['status'] = 'request';
        } else if ($request->submit == "save_for_later") {
            /* $SecondaryDocument = new SecondaryDocument();
              $SecondaryDocument->customer_id = $customer_id;
              $SecondaryDocument->date = date('Y-m-d');
              $SecondaryDocument->customer = $request->customer;
              $SecondaryDocument->work_order_no = $request->work_order_no;
              $SecondaryDocument->address = $request->project_address;
              $SecondaryDocument->project_name = $request->project_name;
              $SecondaryDocument->country_id = $request->project_country;
              $SecondaryDocument->amount = $request->amount;
              $SecondaryDocument->amount_in_text = $request->amount_text;
              $SecondaryDocument->due_date = date('m-d-Y', strtotime($request->due_date));
              // $SecondaryDocument->first_name = $request->first_name;
              $SecondaryDocument->signature = $request->signature;
              $SecondaryDocument->title = $request->title;
              $SecondaryDocument->comment = $request->comment;
              $SecondaryDocument->type = $request->partial_final_type;
              $SecondaryDocument->service_labour_furnished = $request->service_labour_furnished; */
            $SecondaryDocument->status = 0;
            //  $SecondaryDocument->save();
            $data['status'] = 'draft';
        }

        $result1 = $SecondaryDocument->save();
        $secondary_doc_id = $SecondaryDocument->id;
     
            foreach ($fieldsData as $key => $value) {
            if ($values[$key] == NULL) {
             
                $result2 = \App\Models\SecondaryDocumentFields::create([
                            'secondary_document_id' => $secondary_doc_id,
                            'notice_id' => $request->notice_id,
                            'notice_field_id' => $value,
                            'value' => '',
                ]);
            } else {
                          //dd($values[$key]);

                $result2 = \App\Models\SecondaryDocumentFields::create([
                            'secondary_document_id' => $secondary_doc_id,
                            'notice_id' => $request->notice_id,
                            'notice_field_id' => $value,
                            'value' => nl2br($values[$key]),
                ]);
            }
        }
        $data['notice_id'] = $request->notice_id;
        //dd($request->work_order_attachment_id[0],$request->all());
         if($request->work_order_attachment_id[0]!=null){
            $work_order_attachment_id = $request->work_order_attachment_id;
            $work_order_attachment_id = explode(',', $work_order_attachment_id[0]);
          //  dd($work_order_attachment_id);

            foreach ($work_order_attachment_id as $key => $value) {//dd($value);
               $work_order_attachment_obj =  \App\Models\SecondaryDocumentAttachments::find($value);
               if(!empty($work_order_attachment_obj)){
               $work_order_attachment_obj->secondary_document_id = $SecondaryDocument->id;
               $work_order_attachment_obj->save();
             }
            }
        }
        if ($SecondaryDocument) {
            if ($request->contracted_by != "") {
                /* check contracted by is exist or not */
                $contracted_by_check = \App\Models\Contact::where('company_name', $request->contracted_by)->first();
                if (empty($contracted_by_check)) {
                    $contact = \App\Models\Contact::create([
                                'customer_id' => $customer_id,
                                'company_name' => $request->contracted_by,
                    ]);
                }
            }
            if ($request->project_owner != "") {
                /* check project_owner is exist or not */
                $project_owner_check = \App\Models\Contact::where('company_name', $request->project_owner)->first();
                if (empty($project_owner_check)) {
                    $contact = \App\Models\Contact::create([
                                'customer_id' => $customer_id,
                                'company_name' => $request->project_owner,
                    ]);
                }
            }

            $admin_email = SecondaryDocument::getAdminEmail();

            /*   if (Auth::user()->hasRole('account-manager') || Auth::user()->hasRole('admin')) {


              if (Auth::user()->hasRole('account-manager')) {
              $account_manager_email = SecondaryDocument::getOtherAccManagerEmailExcludingLoggedIn(Auth::user()->id);
              $customer_email = SecondaryDocument::getCustomerEmail($SecondaryDocument->customer_id);
              if (isset($account_manager_email) && !empty($account_manager_email)) {
              $data['note_emails'] = [
              $customer_email->email,
              $admin_email->email,
              ];
              foreach ($account_manager_email AS $each_emails) {
              array_push($data['note_emails'], $each_emails->email);
              }
              } else {
              $data['note_emails'] = [
              $customer_email->email,
              $admin_email->email,
              ];
              }
              } else {
              $account_manager_email = SecondaryDocument::getAllAccountManagerEmail();
              $customer_email = SecondaryDocument::getCustomerEmail($SecondaryDocument->customer_id);
              if (isset($account_manager_email) && !empty($account_manager_email)) {
              $data['note_emails'] = [
              $customer_email->email
              ];
              foreach ($account_manager_email AS $each_emails) {
              array_push($data['note_emails'], $each_emails->email);
              }
              } else {
              $data['note_emails'] = [
              $customer_email->email
              ];
              }
              }
              } else {

              $account_manager_email = SecondaryDocument::getAccountManagerEmail($SecondaryDocument->customer_id);
              if (isset($account_manager_email) && !empty($account_manager_email)) {
              $data['note_emails'] = [
              $admin_email->email,
              $account_manager_email->email
              ];
              } else {
              $data['note_emails'] = [
              $admin_email->email
              ];
              }
              } */
            $data['note_emails'] = get_user_notice_email($SecondaryDocument->customer_id);
            $data['parent_work_orders'] = \App\Models\WorkOrder::leftjoin('notice_templates', 'notice_templates.notice_id', '=', 'work_orders.notice_id')
                    ->where('customer_id', '=', $SecondaryDocument->customer_id)
                    ->where('notice_templates.is_parent_work_order', '=', 1)
                    ->select('work_orders.id')
                    ->get();
                    //dd($request->all());
            if ($request->secondary_doc_attachment) {
                $attachment = explode(',|,', trim($request->secondary_doc_attachment[0],'|,'));
//dd($attachment);
                if (isset($attachment) && $attachment != null) {
                    foreach ($attachment AS $k => $v) {
                      $v = trim($v,'|,');
                      if($v!= '|'){
                        if (isset($request->remove_doc) && !empty($request->remove_doc) && $request->remove_doc[0] != '') {
                            $remove_doc = explode(',', $request->remove_doc[0]);

                            if ($v != '') {
                                $each_attachment = explode(',', $v); ;
                                //dd($each_attachment,$remove_doc);

                                //if(isset($each_attachment[4])){
                                if (!in_array($each_attachment[4], $remove_doc)) {
                                    if ($v != '') {//dd($remove_doc,$each_attachment,$attachment);

                                        $secondary_attachment_obj = new \App\Models\SecondaryDocumentAttachments();
                                        $secondary_attachment_obj->secondary_document_id = $SecondaryDocument->id;
                                        $secondary_attachment_obj->type = $each_attachment[0];
                                        $secondary_attachment_obj->title = $each_attachment[1];
                                        if(!empty($each_attachment[2])){
                                                        $secondary_attachment_obj->file_name = $SecondaryDocument->id . '_' . $each_attachment[2];
                                                        }else{
                                                         $secondary_attachment_obj->file_name = " ";
                                                        }
                                                        if(!empty($each_attachment[3])){
                                                        $secondary_attachment_obj->original_file_name = $each_attachment[3];
                                                        }else{
                                                           $secondary_attachment_obj->original_file_name = "";
                                                        }
                                       /* $secondary_attachment_obj->file_name = $each_attachment[2];
                                        $secondary_attachment_obj->original_file_name = $each_attachment[3];*/
                                        $secondary_attachment_obj->save();
                                    }
                                  }
                              //  }
                            }
                        } else {
                            if ($v != '') {//dd('h');
                                $each_attachment = explode(',', $v);
                                $secondary_attachment_obj = new \App\Models\SecondaryDocumentAttachments();
                                $secondary_attachment_obj->secondary_document_id = $SecondaryDocument->id;
                                $secondary_attachment_obj->type = $each_attachment[0];
                                $secondary_attachment_obj->title = $each_attachment[1];
                                if(!empty($each_attachment[2])){
                                                        $secondary_attachment_obj->file_name = $SecondaryDocument->id . '_' . $each_attachment[2];
                                                        }else{
                                                         $secondary_attachment_obj->file_name = " ";
                                                        }
                                                        if(!empty($each_attachment[3])){
                                                        $secondary_attachment_obj->original_file_name = $each_attachment[3];
                                                        }else{
                                                           $secondary_attachment_obj->original_file_name = "";
                                                        }
                                /*$secondary_attachment_obj->file_name = $each_attachment[2];
                                $secondary_attachment_obj->original_file_name = $each_attachment[3];*/
                                $secondary_attachment_obj->save();
                            }
                        }
                      }
                    }
                }
            }
            //If parent work order is selected document of parent work order will inserted
            if (isset($request->work_order_no) && $request->work_order_no != NULL) {
                if (isset($request->remove_doc) && !empty($request->remove_doc) && $request->remove_doc[0] != '') {
                    $previous_attachments = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $request->work_order_no)->get();

                    if (isset($previous_attachments) && count($previous_attachments) > 0) {
                        foreach ($previous_attachments AS $k1 => $a1) {
/*
                            $each_remove_doc = explode(',', $request->remove_doc[0]);
                            foreach ($each_remove_doc As $k => $each) {
                                $explode_v = explode('_', $each);
                                $count = count($explode_v);
                                if($count>1){
                                $each_remove_doc[$k] = $explode_v[$count - 3] . '_' . $explode_v[$count - 2] . '_' . $explode_v[$count - 1];
                              }
                            }

                            $true = !in_array($a1->file_name, $each_remove_doc);*/
                            $true = "true";

                            if (isset($true) && $true != '') {

                                $secondary_attachment_obj = new \App\Models\SecondaryDocumentAttachments();
                                $secondary_attachment_obj->secondary_document_id = $SecondaryDocument->id;
                                $secondary_attachment_obj->type = $a1->type;
                                $secondary_attachment_obj->title = $a1->title;
                               
                                if(!empty($a1->file_name)){
                                                        $secondary_attachment_obj->file_name = $SecondaryDocument->id . '_' . $a1->file_name;
                                                        }else{
                                                         $secondary_attachment_obj->file_name = "";
                                                        }
                                                        if(!empty($a1->original_file_name)){
                                                        $secondary_attachment_obj->original_file_name = $a1->original_file_name;
                                                        }else{
                                                           $secondary_attachment_obj->original_file_name = "";
                                                        }
                                if (Auth::user()->hasRole('customer')) {
                                    $secondary_attachment_obj->visibility = 0;
                                } else {
                                    $secondary_attachment_obj->visibility = $a1->visibility;
                                }
                                $secondary_attachment_obj->save();
                                if (file_exists(public_path() . '/attachment/secondary_document/parent_document/' . $a1->file_name) && !empty($a1->file_name)) {
                                    rename(public_path() . '/attachment/secondary_document/parent_document/' . $a1->file_name, public_path() . '/attachment/secondary_document/parent_document/' . $secondary_attachment_obj->file_name);
                                    \File::copy(public_path() . '/attachment/secondary_document/parent_document/' . $secondary_attachment_obj->file_name, public_path() . '/attachment/secondary_document/' . $secondary_attachment_obj->file_name);
                                    unlink(public_path() . '/attachment/secondary_document/parent_document/' . $secondary_attachment_obj->file_name);
                                }
                            }
                        }
                    }
                } else {
                    $remove_doc = explode(',', $request->remove_doc[0]);
                    $previous_attachments = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $request->work_order_no)->get();

                    if (isset($previous_attachments) && count($previous_attachments) > 0) {
                        foreach ($previous_attachments AS $k1 => $a1) {
                            $secondary_attachment_obj = new \App\Models\SecondaryDocumentAttachments();
                            $secondary_attachment_obj->secondary_document_id = $SecondaryDocument->id;
                            $secondary_attachment_obj->type = $a1->type;
                            $secondary_attachment_obj->title = $a1->title;
                             if(!empty($a1->file_name)){
                                                        $secondary_attachment_obj->file_name = $SecondaryDocument->id . '_' . $a1->file_name;
                                                        }else{
                                                         $secondary_attachment_obj->file_name = "";
                                                        }
                                                        if(!empty($a1->original_file_name)){
                                                        $secondary_attachment_obj->original_file_name = $a1->original_file_name;
                                                        }else{
                                                           $secondary_attachment_obj->original_file_name = "";
                                                        }
                            if (Auth::user()->hasRole('customer')) {
                                $secondary_attachment_obj->visibility = 0;
                            } else {
                                $secondary_attachment_obj->visibility = $a1->visibility;
                            }
                            $secondary_attachment_obj->save();

                            if (!empty($a1->file_name) && file_exists(public_path() . '/attachment/secondary_document/parent_document/' . $a1->file_name) && !empty($a1->file_name)) {

                                rename(public_path() . '/attachment/secondary_document/parent_document/' . $a1->file_name, public_path() . '/attachment/secondary_document/parent_document/' . $secondary_attachment_obj->file_name);
                                \File::copy(public_path() . '/attachment/secondary_document/parent_document/' . $secondary_attachment_obj->file_name, public_path() . '/attachment/secondary_document/' . $secondary_attachment_obj->file_name);
                                unlink(public_path() . '/attachment/secondary_document/parent_document/' . $secondary_attachment_obj->file_name);
                            }
                        }
                    }
                }
            }

            Session::flash('success', 'Secondary Document Successfully Created');

            if (Auth::user()->hasRole('account-manager')) {

                $customers = \App\Models\Customer::leftjoin('users', 'users.id', '=', 'customers.user_id')
                        ->select('customers.id', 'customers.company_name')
                        ->where('account_manager_id', '=', Auth::user()->id)
                        ->get();
                $data['customers'] = $customers;
            }
            if (Auth::user()->hasRole('admin')) {

                $customers = \App\Models\Customer::leftjoin('users', 'users.id', '=', 'customers.user_id')
                        ->select('customers.id', 'customers.company_name')
                        ->get();
                $data['customers'] = $customers;
            }
            $data['cities'] = \App\Models\City::select()->get();
            $data['states'] = \App\Models\State::select()->orderBy('name')->get();
            if (isset($request->work_order_no) && $request->work_order_no != NULL) {
                $parent_work_order_recipients = \App\Models\Recipient::select()->where('work_order_id', $request->work_order_no)->get();
                if (isset($parent_work_order_recipients) && count($parent_work_order_recipients) > 0) {
                    foreach ($parent_work_order_recipients As $k => $each_parent_recipient)
                        $SecondaryDocumentRecipient = new SecondaryDocumentRecipient();
                    $SecondaryDocumentRecipient->secondary_document_id = $SecondaryDocument->id;
                    $SecondaryDocumentRecipient->category_id = $each_parent_recipient->category_id;
                    $SecondaryDocumentRecipient->name = $each_parent_recipient->name;
                    $SecondaryDocumentRecipient->contact = $each_parent_recipient->contact;
                    $SecondaryDocumentRecipient->address = $each_parent_recipient->address;
                    $SecondaryDocumentRecipient->city_id = $each_parent_recipient->city_id;
                    $SecondaryDocumentRecipient->state_id = $each_parent_recipient->state_id;
                    $SecondaryDocumentRecipient->zip = $each_parent_recipient->zip;
                    $SecondaryDocumentRecipient->email = $each_parent_recipient->email;
                    $SecondaryDocumentRecipient->attn = $each_parent_recipient->attn;
                    $SecondaryDocumentRecipient->save();
                }
            }
            $data['recipients'] = \App\Models\SecondaryDocumentRecipient::select()->where('secondary_document_id', $SecondaryDocument->id)->get();

            $data['count'] = \App\Models\SecondaryDocumentRecipient::select(DB::raw('COUNT(*) AS total'), 'categories.name')
                    ->groupBy('secondary_document_recipients.category_id')
                    ->join('categories', 'categories.id', '=', 'secondary_document_recipients.category_id')
                    ->where('secondary_document_id', $SecondaryDocument->id)
                    ->get();
            $data['countries'] = \App\Models\Country::select()->get();
            $data['categories'] = \App\Models\Category::select()->get();
            $data['project_roles'] = \App\Models\ProjectRoles::select()->orderBy('name', 'ASC')->get();
            $data['partial_final_type'] = $request->partial_final_type;
            $data['notes'] = \App\Models\SecondaryDocumentNotes::where('customer_id', '=', Auth::user()->customer->id)->where('secondary_document_id', $SecondaryDocument->id)->get();

            /*$data['secondary_doc_id'] = $SecondaryDocument->id;
            $result = \App\Models\SecondaryDocument::select('state_id')->join('notice_fields', 'notice_fields.notice_id', '=', 'secondary_documents.notice_id')->where('secondary_documents.id', $secondary_doc_id)->limit(1)->get()->toArray();

            $notice = \App\Models\Notice::select('name', 'id')->where('id', $request->notice_id)->get()->toArray();
            $data['notice'] = $notice[0];
            $data['notice_id'] = $request->notice_id;
            $data['notice_fields'] = \App\Models\NoticeField::select()->join('secondary_document_fields', 'secondary_document_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('secondary_document_fields.notice_id', $request->notice_id)->where('secondary_document_fields.secondary_document_id',$secondary_doc_id)->orderBy('sort_order', 'asc')->get();*/
            //$secondary_document = SecondaryDocument::find($SecondaryDocument->id);
            //$data['secondary_document'] = $secondary_document;
//            $data['names'] = \App\Models\ContactDetail::select()
//                    ->where('contact_details.type', '=', 0)
//                    ->where('contacts.customer_id', '=', Auth::user()->customer->id)
//                    ->Join('contacts', 'contacts.id', '=', 'contact_details.contact_id')
//                    ->get();
            if (Auth::user()->hasRole('account-manager') || Auth::user()->hasRole('admin')) {

                $data['names'] = \App\Models\Contact::select('contacts.id', 'contacts.company_name')
                        //->where('contact_details.type', '=', 0)
                        ->where('contacts.customer_id', '=', $customer_id)
                        // ->Join('contact_details', 'contact_details.contact_id', '=', 'contacts.id')
                        ->orderBy('contacts.id', 'DESC')
                        ->get();
                $data['officers_directors'] = \App\Models\CustomerAgent::where('customer_id', '=', $customer_id)
                        ->get();
            } else {
                $data['names'] = \App\Models\Contact::select('contacts.id', 'contacts.company_name')
                        //->where('contact_details.type', '=', 0)
                        ->where('contacts.customer_id', '=', Auth::user()->customer->id)
                        // ->Join('contact_details', 'contact_details.contact_id', '=', 'contacts.id')
                        ->orderBy('contacts.id', 'DESC')
                        ->get();
                $data['officers_directors'] = \App\Models\CustomerAgent::where('customer_id', '=', Auth::user()->customer->id)
                        ->get();
            }
            $data['tab'] = 'recipients';
        } else {
            Session::flash('success', 'Problem in Record saving');
        }
        $data['attachment'] = \App\Models\SecondaryDocumentAttachments::where('secondary_document_id', '=', $SecondaryDocument->id)
                ->get();
        $data['notes'] = \App\Models\SecondaryDocumentNotes::leftjoin('users', 'users.id', '=', 'secondary_document_notes.user_id')->where('secondary_document_id', $SecondaryDocument->id)
                ->orderBy('secondary_document_notes.id', 'DESC')
                ->get();
        $data['corrections'] = \App\Models\SecondaryDocumentCorrections::leftjoin('users', 'users.id', '=', 'secondary_document_corrections.user_id')->where('secondary_document_id', $SecondaryDocument->id)
                ->orderBy('secondary_document_corrections.id', 'DESC')
                ->get();

        // return view('customer.secondary_document.create', $data);
        if ($request->submit == 'continue') {
            //return view('customer.secondary_document.edit', $data);
            return redirect('customer/secondary-document/edit/' . $SecondaryDocument->id.'/'.'0');
        } else {
            return redirect('customer/secondary-document');
        }
    }

    public function store_old(Request $request) {

        if ($request->submit == "continue") {

            $rules = [//'customer' => 'required',
//            'work_order_no' => 'required',
                'project_address' => 'required',
                // 'project_country' => 'required',
                'amount' => 'required',
                //'amount_text' => 'required',
//                'due_date' => 'required',
//            'first_name' => 'required',
                'signature' => 'required',
//                'comment' => 'required'
            ];
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }
        } elseif ($request->submit == 'save_for_later') {
            if (Auth::user()->hasRole('account-manager') || Auth::user()->hasRole('admin')) {
                $rules = ['customer_id' => 'required'
                ];
                $validator = Validator::make(Input::all(), $rules, [
                            'customer_id.required' => 'Please select customer'
                ]);
                if ($validator->fails()) {
                    return redirect()->back()->withErrors($validator)->withInput();
                }
            }
        }

        if (Auth::user()->hasRole('customer')) {
            $request->request->set('customer_id', Auth::user()->customer->id);
            $customer_id = Auth::user()->customer->id;
        } else {
            $request->request->set('customer_id', $request->customer_id);
            $customer_id = $request->customer_id;
        }

        $request->request->set('project_country', $request->county_id);
        //$request->request->set('date',date('Y-m-d', strtotime($request->date)));
        $request->request->set('due_date', date('m-d-Y', strtotime($request->due_date)));
//        $SecondaryDocument = \App\Models\SecondaryDocument::create($request->except(['_token']));
        $SecondaryDocument = new SecondaryDocument();
        $SecondaryDocument->customer_id = $customer_id;
        $SecondaryDocument->date = date('Y-m-d');
        $SecondaryDocument->customer = $request->contracted_by;
        $SecondaryDocument->work_order_no = $request->work_order_no;
        $SecondaryDocument->address = $request->project_address;
        $SecondaryDocument->project_name = $request->project_name;
        $SecondaryDocument->country_id = $request->county_id;
        $SecondaryDocument->amount = $request->amount;
        $SecondaryDocument->amount_in_text = $request->amount_text;
        $SecondaryDocument->due_date = $request->due_date;
        // $SecondaryDocument->first_name = $request->first_name;
        $SecondaryDocument->signature = $request->signature;
        $SecondaryDocument->title = $request->title;
        $SecondaryDocument->comment = $request->comment;
        $SecondaryDocument->type = $request->partial_final_type;
        $SecondaryDocument->service_labour_furnished = $request->service_labour_furnished;
        $SecondaryDocument->contracted_by = $request->contracted_by;
        $SecondaryDocument->project_owner = $request->project_owner;
        $SecondaryDocument->notary_seal = isset($request->notary_seal) ? $request->notary_seal : '0';
        $SecondaryDocument->legal_description = $request->legal_description;
        if ($request->submit == "continue") {
            $SecondaryDocument->status = 1;

            $data['status'] = 'request';
        } else if ($request->submit == "save_for_later") {
            /* $SecondaryDocument = new SecondaryDocument();
              $SecondaryDocument->customer_id = $customer_id;
              $SecondaryDocument->date = date('Y-m-d');
              $SecondaryDocument->customer = $request->customer;
              $SecondaryDocument->work_order_no = $request->work_order_no;
              $SecondaryDocument->address = $request->project_address;
              $SecondaryDocument->project_name = $request->project_name;
              $SecondaryDocument->country_id = $request->project_country;
              $SecondaryDocument->amount = $request->amount;
              $SecondaryDocument->amount_in_text = $request->amount_text;
              $SecondaryDocument->due_date = date('m-d-Y', strtotime($request->due_date));
              // $SecondaryDocument->first_name = $request->first_name;
              $SecondaryDocument->signature = $request->signature;
              $SecondaryDocument->title = $request->title;
              $SecondaryDocument->comment = $request->comment;
              $SecondaryDocument->type = $request->partial_final_type;
              $SecondaryDocument->service_labour_furnished = $request->service_labour_furnished; */
            $SecondaryDocument->status = 0;
            //  $SecondaryDocument->save();
            $data['status'] = 'draft';
        }

        $SecondaryDocument->save();
        //dd($request->work_order_attachment_id[0],$request->all());
         if($request->work_order_attachment_id[0]!=null){
            $work_order_attachment_id = $request->work_order_attachment_id;
            $work_order_attachment_id = explode(',', $work_order_attachment_id[0]);
          //  dd($work_order_attachment_id);

            foreach ($work_order_attachment_id as $key => $value) {//dd($value);
               $work_order_attachment_obj =  \App\Models\SecondaryDocumentAttachments::find($value);
               if(!empty($work_order_attachment_obj)){
               $work_order_attachment_obj->secondary_document_id = $SecondaryDocument->id;
               $work_order_attachment_obj->save();
             }
            }
        }
        if ($SecondaryDocument) {
            if ($request->contracted_by != "") {
                /* check contracted by is exist or not */
                $contracted_by_check = \App\Models\Contact::where('company_name', $request->contracted_by)->first();
                if (empty($contracted_by_check)) {
                    $contact = \App\Models\Contact::create([
                                'customer_id' => $customer_id,
                                'company_name' => $request->contracted_by,
                    ]);
                }
            }
            if ($request->project_owner != "") {
                /* check project_owner is exist or not */
                $project_owner_check = \App\Models\Contact::where('company_name', $request->project_owner)->first();
                if (empty($project_owner_check)) {
                    $contact = \App\Models\Contact::create([
                                'customer_id' => $customer_id,
                                'company_name' => $request->project_owner,
                    ]);
                }
            }

            $admin_email = SecondaryDocument::getAdminEmail();

            /*   if (Auth::user()->hasRole('account-manager') || Auth::user()->hasRole('admin')) {


              if (Auth::user()->hasRole('account-manager')) {
              $account_manager_email = SecondaryDocument::getOtherAccManagerEmailExcludingLoggedIn(Auth::user()->id);
              $customer_email = SecondaryDocument::getCustomerEmail($SecondaryDocument->customer_id);
              if (isset($account_manager_email) && !empty($account_manager_email)) {
              $data['note_emails'] = [
              $customer_email->email,
              $admin_email->email,
              ];
              foreach ($account_manager_email AS $each_emails) {
              array_push($data['note_emails'], $each_emails->email);
              }
              } else {
              $data['note_emails'] = [
              $customer_email->email,
              $admin_email->email,
              ];
              }
              } else {
              $account_manager_email = SecondaryDocument::getAllAccountManagerEmail();
              $customer_email = SecondaryDocument::getCustomerEmail($SecondaryDocument->customer_id);
              if (isset($account_manager_email) && !empty($account_manager_email)) {
              $data['note_emails'] = [
              $customer_email->email
              ];
              foreach ($account_manager_email AS $each_emails) {
              array_push($data['note_emails'], $each_emails->email);
              }
              } else {
              $data['note_emails'] = [
              $customer_email->email
              ];
              }
              }
              } else {

              $account_manager_email = SecondaryDocument::getAccountManagerEmail($SecondaryDocument->customer_id);
              if (isset($account_manager_email) && !empty($account_manager_email)) {
              $data['note_emails'] = [
              $admin_email->email,
              $account_manager_email->email
              ];
              } else {
              $data['note_emails'] = [
              $admin_email->email
              ];
              }
              } */
            $data['note_emails'] = get_user_notice_email($SecondaryDocument->customer_id);
            $data['parent_work_orders'] = \App\Models\WorkOrder::leftjoin('notice_templates', 'notice_templates.notice_id', '=', 'work_orders.notice_id')
                    ->where('customer_id', '=', $SecondaryDocument->customer_id)
                    ->where('notice_templates.is_parent_work_order', '=', 1)
                    ->select('work_orders.id')
                    ->get();
                    //dd($request->all());
            if ($request->secondary_doc_attachment) {
                $attachment = explode(',|,', trim($request->secondary_doc_attachment[0],'|,'));
//dd($attachment);
                if (isset($attachment) && $attachment != null) {
                    foreach ($attachment AS $k => $v) {
                      $v = trim($v,'|,');
                      if($v!= '|'){
                        if (isset($request->remove_doc) && !empty($request->remove_doc) && $request->remove_doc[0] != '') {
                            $remove_doc = explode(',', $request->remove_doc[0]);

                            if ($v != '') {
                                $each_attachment = explode(',', $v); ;
                                //dd($each_attachment,$remove_doc);

                                //if(isset($each_attachment[4])){
                                if (!in_array($each_attachment[4], $remove_doc)) {
                                    if ($v != '') {//dd($remove_doc,$each_attachment,$attachment);

                                        $secondary_attachment_obj = new \App\Models\SecondaryDocumentAttachments();
                                        $secondary_attachment_obj->secondary_document_id = $SecondaryDocument->id;
                                        $secondary_attachment_obj->type = $each_attachment[0];
                                        $secondary_attachment_obj->title = $each_attachment[1];
                                        if(!empty($each_attachment[2])){
                                                        $secondary_attachment_obj->file_name = $SecondaryDocument->id . '_' . $each_attachment[2];
                                                        }else{
                                                         $secondary_attachment_obj->file_name = " ";
                                                        }
                                                        if(!empty($each_attachment[3])){
                                                        $secondary_attachment_obj->original_file_name = $each_attachment[3];
                                                        }else{
                                                           $secondary_attachment_obj->original_file_name = "";
                                                        }
                                       /* $secondary_attachment_obj->file_name = $each_attachment[2];
                                        $secondary_attachment_obj->original_file_name = $each_attachment[3];*/
                                        $secondary_attachment_obj->save();
                                    }
                                  }
                              //  }
                            }
                        } else {
                            if ($v != '') {//dd('h');
                                $each_attachment = explode(',', $v);
                                $secondary_attachment_obj = new \App\Models\SecondaryDocumentAttachments();
                                $secondary_attachment_obj->secondary_document_id = $SecondaryDocument->id;
                                $secondary_attachment_obj->type = $each_attachment[0];
                                $secondary_attachment_obj->title = $each_attachment[1];
                                if(!empty($each_attachment[2])){
                                                        $secondary_attachment_obj->file_name = $SecondaryDocument->id . '_' . $each_attachment[2];
                                                        }else{
                                                         $secondary_attachment_obj->file_name = " ";
                                                        }
                                                        if(!empty($each_attachment[3])){
                                                        $secondary_attachment_obj->original_file_name = $each_attachment[3];
                                                        }else{
                                                           $secondary_attachment_obj->original_file_name = "";
                                                        }
                                /*$secondary_attachment_obj->file_name = $each_attachment[2];
                                $secondary_attachment_obj->original_file_name = $each_attachment[3];*/
                                $secondary_attachment_obj->save();
                            }
                        }
                      }
                    }
                }
            }
            //If parent work order is selected document of parent work order will inserted
            if (isset($request->work_order_no) && $request->work_order_no != NULL) {
                if (isset($request->remove_doc) && !empty($request->remove_doc) && $request->remove_doc[0] != '') {
                    $previous_attachments = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $request->work_order_no)->get();

                    if (isset($previous_attachments) && count($previous_attachments) > 0) {
                        foreach ($previous_attachments AS $k1 => $a1) {
/*
                            $each_remove_doc = explode(',', $request->remove_doc[0]);
                            foreach ($each_remove_doc As $k => $each) {
                                $explode_v = explode('_', $each);
                                $count = count($explode_v);
                                if($count>1){
                                $each_remove_doc[$k] = $explode_v[$count - 3] . '_' . $explode_v[$count - 2] . '_' . $explode_v[$count - 1];
                              }
                            }

                            $true = !in_array($a1->file_name, $each_remove_doc);*/
                            $true = "true";

                            if (isset($true) && $true != '') {

                                $secondary_attachment_obj = new \App\Models\SecondaryDocumentAttachments();
                                $secondary_attachment_obj->secondary_document_id = $SecondaryDocument->id;
                                $secondary_attachment_obj->type = $a1->type;
                                $secondary_attachment_obj->title = $a1->title;
                               
                                if(!empty($a1->file_name)){
                                                        $secondary_attachment_obj->file_name = $SecondaryDocument->id . '_' . $a1->file_name;
                                                        }else{
                                                         $secondary_attachment_obj->file_name = "";
                                                        }
                                                        if(!empty($a1->original_file_name)){
                                                        $secondary_attachment_obj->original_file_name = $a1->original_file_name;
                                                        }else{
                                                           $secondary_attachment_obj->original_file_name = "";
                                                        }
                                if (Auth::user()->hasRole('customer')) {
                                    $secondary_attachment_obj->visibility = 0;
                                } else {
                                    $secondary_attachment_obj->visibility = $a1->visibility;
                                }
                                $secondary_attachment_obj->save();
                                if (file_exists(public_path() . '/attachment/secondary_document/parent_document/' . $a1->file_name) && !empty($a1->file_name)) {
                                    rename(public_path() . '/attachment/secondary_document/parent_document/' . $a1->file_name, public_path() . '/attachment/secondary_document/parent_document/' . $secondary_attachment_obj->file_name);
                                    \File::copy(public_path() . '/attachment/secondary_document/parent_document/' . $secondary_attachment_obj->file_name, public_path() . '/attachment/secondary_document/' . $secondary_attachment_obj->file_name);
                                    unlink(public_path() . '/attachment/secondary_document/parent_document/' . $secondary_attachment_obj->file_name);
                                }
                            }
                        }
                    }
                } else {
                    $remove_doc = explode(',', $request->remove_doc[0]);
                    $previous_attachments = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $request->work_order_no)->get();

                    if (isset($previous_attachments) && count($previous_attachments) > 0) {
                        foreach ($previous_attachments AS $k1 => $a1) {
                            $secondary_attachment_obj = new \App\Models\SecondaryDocumentAttachments();
                            $secondary_attachment_obj->secondary_document_id = $SecondaryDocument->id;
                            $secondary_attachment_obj->type = $a1->type;
                            $secondary_attachment_obj->title = $a1->title;
                             if(!empty($a1->file_name)){
                                                        $secondary_attachment_obj->file_name = $SecondaryDocument->id . '_' . $a1->file_name;
                                                        }else{
                                                         $secondary_attachment_obj->file_name = "";
                                                        }
                                                        if(!empty($a1->original_file_name)){
                                                        $secondary_attachment_obj->original_file_name = $a1->original_file_name;
                                                        }else{
                                                           $secondary_attachment_obj->original_file_name = "";
                                                        }
                            if (Auth::user()->hasRole('customer')) {
                                $secondary_attachment_obj->visibility = 0;
                            } else {
                                $secondary_attachment_obj->visibility = $a1->visibility;
                            }
                            $secondary_attachment_obj->save();

                            if (!empty($a1->file_name) && file_exists(public_path() . '/attachment/secondary_document/parent_document/' . $a1->file_name) && !empty($a1->file_name)) {

                                rename(public_path() . '/attachment/secondary_document/parent_document/' . $a1->file_name, public_path() . '/attachment/secondary_document/parent_document/' . $secondary_attachment_obj->file_name);
                                \File::copy(public_path() . '/attachment/secondary_document/parent_document/' . $secondary_attachment_obj->file_name, public_path() . '/attachment/secondary_document/' . $secondary_attachment_obj->file_name);
                                unlink(public_path() . '/attachment/secondary_document/parent_document/' . $secondary_attachment_obj->file_name);
                            }
                        }
                    }
                }
            }

            Session::flash('success', 'Secondary Document Successfully Created');

            if (Auth::user()->hasRole('account-manager')) {

                $customers = \App\Models\Customer::leftjoin('users', 'users.id', '=', 'customers.user_id')
                        ->select('customers.id', 'customers.company_name')
                        ->where('account_manager_id', '=', Auth::user()->id)
                        ->get();
                $data['customers'] = $customers;
            }
            if (Auth::user()->hasRole('admin')) {

                $customers = \App\Models\Customer::leftjoin('users', 'users.id', '=', 'customers.user_id')
                        ->select('customers.id', 'customers.company_name')
                        ->get();
                $data['customers'] = $customers;
            }
            $data['cities'] = \App\Models\City::select()->get();
            $data['states'] = \App\Models\State::select()->orderBy('name')->get();
            if (isset($request->work_order_no) && $request->work_order_no != NULL) {
                $parent_work_order_recipients = \App\Models\Recipient::select()->where('work_order_id', $request->work_order_no)->get();
                if (isset($parent_work_order_recipients) && count($parent_work_order_recipients) > 0) {
                    foreach ($parent_work_order_recipients As $k => $each_parent_recipient)
                        $SecondaryDocumentRecipient = new SecondaryDocumentRecipient();
                    $SecondaryDocumentRecipient->secondary_document_id = $SecondaryDocument->id;
                    $SecondaryDocumentRecipient->category_id = $each_parent_recipient->category_id;
                    $SecondaryDocumentRecipient->name = $each_parent_recipient->name;
                    $SecondaryDocumentRecipient->contact = $each_parent_recipient->contact;
                    $SecondaryDocumentRecipient->address = $each_parent_recipient->address;
                    $SecondaryDocumentRecipient->city_id = $each_parent_recipient->city_id;
                    $SecondaryDocumentRecipient->state_id = $each_parent_recipient->state_id;
                    $SecondaryDocumentRecipient->zip = $each_parent_recipient->zip;
                    $SecondaryDocumentRecipient->email = $each_parent_recipient->email;
                    $SecondaryDocumentRecipient->attn = $each_parent_recipient->attn;
                    $SecondaryDocumentRecipient->save();
                }
            }
            $data['recipients'] = \App\Models\SecondaryDocumentRecipient::select()->where('secondary_document_id', $SecondaryDocument->id)->get();

            $data['count'] = \App\Models\SecondaryDocumentRecipient::select(DB::raw('COUNT(*) AS total'), 'categories.name')
                    ->groupBy('secondary_document_recipients.category_id')
                    ->join('categories', 'categories.id', '=', 'secondary_document_recipients.category_id')
                    ->where('secondary_document_id', $SecondaryDocument->id)
                    ->get();
            $data['countries'] = \App\Models\Country::select()->get();
            $data['categories'] = \App\Models\Category::select()->get();
            $data['project_roles'] = \App\Models\ProjectRoles::select()->orderBy('name', 'ASC')->get();
            $data['partial_final_type'] = $request->partial_final_type;
            $data['notes'] = \App\Models\SecondaryDocumentNotes::where('customer_id', '=', Auth::user()->customer->id)->where('secondary_document_id', $SecondaryDocument->id)->get();

            $data['secondary_doc_id'] = $SecondaryDocument->id;
            $secondary_document = SecondaryDocument::find($SecondaryDocument->id);
            $data['secondary_document'] = $secondary_document;
//            $data['names'] = \App\Models\ContactDetail::select()
//                    ->where('contact_details.type', '=', 0)
//                    ->where('contacts.customer_id', '=', Auth::user()->customer->id)
//                    ->Join('contacts', 'contacts.id', '=', 'contact_details.contact_id')
//                    ->get();
            if (Auth::user()->hasRole('account-manager') || Auth::user()->hasRole('admin')) {

                $data['names'] = \App\Models\Contact::select('contacts.id', 'contacts.company_name')
                        //->where('contact_details.type', '=', 0)
                        ->where('contacts.customer_id', '=', $customer_id)
                        // ->Join('contact_details', 'contact_details.contact_id', '=', 'contacts.id')
                        ->orderBy('contacts.id', 'DESC')
                        ->get();
                $data['officers_directors'] = \App\Models\CustomerAgent::where('customer_id', '=', $customer_id)
                        ->get();
            } else {
                $data['names'] = \App\Models\Contact::select('contacts.id', 'contacts.company_name')
                        //->where('contact_details.type', '=', 0)
                        ->where('contacts.customer_id', '=', Auth::user()->customer->id)
                        // ->Join('contact_details', 'contact_details.contact_id', '=', 'contacts.id')
                        ->orderBy('contacts.id', 'DESC')
                        ->get();
                $data['officers_directors'] = \App\Models\CustomerAgent::where('customer_id', '=', Auth::user()->customer->id)
                        ->get();
            }
            $data['tab'] = 'recipients';
        } else {
            Session::flash('success', 'Problem in Record saving');
        }
        $data['attachment'] = \App\Models\SecondaryDocumentAttachments::where('secondary_document_id', '=', $SecondaryDocument->id)
                ->get();
        $data['notes'] = \App\Models\SecondaryDocumentNotes::leftjoin('users', 'users.id', '=', 'secondary_document_notes.user_id')->where('secondary_document_id', $SecondaryDocument->id)
                ->orderBy('secondary_document_notes.id', 'DESC')
                ->get();
        $data['corrections'] = \App\Models\SecondaryDocumentCorrections::leftjoin('users', 'users.id', '=', 'secondary_document_corrections.user_id')->where('secondary_document_id', $SecondaryDocument->id)
                ->orderBy('secondary_document_corrections.id', 'DESC')
                ->get();

        // return view('customer.secondary_document.create', $data);
        if ($request->submit == 'continue') {
            //return view('customer.secondary_document.edit', $data);
            return redirect('customer/secondary-document/edit/' . $SecondaryDocument->id);
        } else {
            return redirect('customer/secondary-document');
        }
    }
    

    public function store_attachments(Request $request) {
        $file = $request->file('file');
        if (!empty($file)) {
            $extension = $request->file('file')->getClientOriginalExtension(); // getting excel extension
            $original_file_name = $request->file('file')->getClientOriginalName();
            if (!file_exists(public_path() . '/attachment/secondary_document')) {
                $dir = mkdir(public_path() . '/attachment/secondary_document', 0777, true);
            } else {
                $dir = public_path() . '/attachment/secondary_document/';
            }

            $dir = public_path() . '/attachment/secondary_document/';
            $filename = uniqid() . '_' . time() . '_' . date('Ymd') . '.' . $extension;
            $request->file('file')->move($dir, $filename);
        } else {
            $filename = "";
            $original_file_name = "";
            $extension = "";
        }
        $doc_val = "";
        if ($request->doc_value != "") {
            $doc_val = $request->doc_value;
        }
        $data = [];
        $data['db_arr'] = [
            $request->document_type,
            $doc_val,
            $filename,
            $original_file_name,
            $extension
        ];
        if ($request->document_type == 1) {
            $request->document_type = 'Bond';
        } elseif ($request->document_type == 2) {
            $request->document_type = 'Notice to Commencement';
        } elseif ($request->document_type == 3) {
            $request->document_type = 'Permit';
        } elseif ($request->document_type == 4) {
            $request->document_type = 'Contract';
        } elseif ($request->document_type == 5) {
            $request->document_type = 'Invoice';
        } elseif ($request->document_type == 6) {
            $request->document_type = 'Final Release';
        } elseif ($request->document_type == 7) {
            $request->document_type = 'Partial Release';
        } elseif ($request->document_type == 8) {
            $request->document_type = 'Misc Document';
        } elseif ($request->document_type == 9) {
            $request->document_type = 'Signed Document';
        } elseif ($request->document_type == 10) {
            $request->document_type = 'Folio';
        }elseif ($request->document_type == 11) {
            $request->document_type = 'COL Signed';
        }elseif ($request->document_type == 12) {
            $request->document_type = 'Recorded COL/SCOL';
        }elseif ($request->document_type == 13) {
            $request->document_type = 'Pending Signature';
        }

        $data['ajax_arr'] = [
            $request->document_type,
            $doc_val,
            $original_file_name,
            date('d M y h:i A'),
            $extension
        ];


        return json_encode($data);
    }
    public function edit_attachments(Request $request) {
        $file = $request->file('file');
        if (!empty($file)) {
            $extension = $request->file('file')->getClientOriginalExtension(); // getting excel extension
            $original_file_name = $request->file('file')->getClientOriginalName();
            if (!file_exists(public_path() . '/attachment/secondary_document')) {
                $dir = mkdir(public_path() . '/attachment/secondary_document', 0777, true);
            } else {
                $dir = public_path() . '/attachment/secondary_document/';
            }

            $dir = public_path() . '/attachment/secondary_document/';
            $filename = uniqid() . '_' . time() . '_' . date('Ymd') . '.' . $extension;
            $request->file('file')->move($dir, $filename);
        } else {
            if(!empty($request->file_name) && $request->file_original_name!='undefined'  && $request->extension!='undefined'){
            $filename = $request->file_name;
            $original_file_name = $request->file_original_name;
            $extension = $request->extension;
            }else{
            $filename = "";
            $original_file_name = "";
            $extension = "";
            }
        }
        $doc_val = "";
        if ($request->doc_value != "") {
            $doc_val = $request->doc_value;
        }
        $document_type_id = $request->document_type; 
        $data = [];
        $data['db_arr'] = [
            $request->document_type,
            $doc_val,
            $filename,
            $original_file_name
        ];
        if ($request->document_type == 1) {
            $request->document_type = 'Bond';
        } elseif ($request->document_type == 2) {
            $request->document_type = 'Notice to Commencement';
        } elseif ($request->document_type == 3) {
            $request->document_type = 'Permit';
        } elseif ($request->document_type == 4) {
            $request->document_type = 'Contract';
        } elseif ($request->document_type == 5) {
            $request->document_type = 'Invoice';
        } elseif ($request->document_type == 6) {
            $request->document_type = 'Final Release';
        } elseif ($request->document_type == 7) {
            $request->document_type = 'Partial Release';
        } elseif ($request->document_type == 8) {
            $request->document_type = 'Misc Document';
        } elseif ($request->document_type == 9) {
            $request->document_type = 'Signed Document';
        } elseif ($request->document_type == 10) {
            $request->document_type = 'Folio';
        }elseif ($request->document_type == 11) {
            $request->document_type = 'COL Signed';
        }elseif ($request->document_type == 12) {
            $request->document_type = 'Recorded COL/SCOL';
        }elseif ($request->document_type == 13) {
            $request->document_type = 'Pending Signature';
        }

        $data['ajax_arr'] = [
            $request->document_type,
            $doc_val,
            $original_file_name,
            date('d M y h:i A'),
            $extension
        ];
         $remove_file = $request->remove_file;
            if($request->secondary_document_id == 'null'){
                $request->request->set('secondary_document_id',NULL);
            }
          if($request->doc_id==0){
            $attachments = new \App\Models\SecondaryDocumentAttachments;
            $attachments->secondary_document_id = $request->secondary_document_id;
            $attachments->type = $data['db_arr']['0'];
            $attachments->title =  $data['db_arr']['1'];
            //if (!empty($file)) {
            $attachments->file_name = $data['db_arr']['2'];
            $attachments->original_file_name = $data['db_arr']['3'];
            //}
            if($remove_file==1  && empty($file)){
             $attachments->file_name = "";   
             $attachments->original_file_name = "";
            }
            if (Auth::user()->hasRole('account-manager')) {
            $attachments->visibility = 0;
            //$attachments->visibility = $request->visibility;
            }else{
            $attachments->visibility = 0;
            }
            $attachments->save();

            /*$previous_attachments = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $parent_work_order)->get();*/
        }else{
            $attachments = \App\Models\SecondaryDocumentAttachments::find($request->doc_id);
            $attachments->secondary_document_id = $request->secondary_document_id;
            $attachments->type = $data['db_arr']['0'];
            $attachments->title =  $data['db_arr']['1'];
            if (!empty($file)) {
            $attachments->file_name = $data['db_arr']['2'];
            $attachments->original_file_name = $data['db_arr']['3'];
            }else if(!empty($request->file_name) && $request->file_original_name!='undefined'  && $request->extension!='undefined'){
              $attachments->file_name = $data['db_arr']['2'];
              $attachments->original_file_name = $data['db_arr']['3'];
            }
            if($remove_file==1 && empty($file)){
             $attachments->file_name = "";   
             $attachments->original_file_name = "";
            }
            if (Auth::user()->hasRole('account-manager')) {
            $attachments->visibility = $request->visibility;
            }else{
            $attachments->visibility = 0;
            }
            $attachments->save();
        }
        if(!empty($attachments->original_file_name)){
        $extension = explode('.', $attachments->original_file_name);
        $extension = $extension[1]; 
        }else{
          $extension = "";
        }
        $data['ajax_arr'] = [
            $request->document_type,
            $doc_val,
            $attachments->original_file_name,
           // $attachments->original_file_name,
            date('d M y h:i A'),
            $extension
        ];
        //dd($attachments);
        $data['db_arr'] = [
                $attachments->type,
                $doc_val,
                $attachments->file_name,
                $attachments->original_file_name,
                $attachments->id,
                $document_type_id,
                $extension
            ];

        return json_encode($data);
    }

    public function store_recipients(Request $request, $secondary_document_id) {

        $rules = [
            'category_id' => 'required',
            'name' => 'required',
            'address' => 'required',
            'city_id' => 'required',
            'state_id' => 'required',
            'zip' => 'required'
        ];

        $validator = Validator::make(Input::all(), $rules, [
                    'category_id' => 'Type of recipient is required',
                    'city_id' => 'Select City from the list',
                    'state_id' => 'The state is required'
        ]);
        $secondary_document_status = \App\Models\SecondaryDocument::find($secondary_document_id);
        if ($secondary_document_status->status == '1') {
            $data['status'] = 'request';
        } else {
            $data['status'] = 'draft';
        }

        /* if ($validator->fails()) {
          return redirect('post/create')
          ->withErrors($validator)
          ->withInput();
          } */
        // $secondary_document_id = \App\Models\SecondaryDocument::orderBy('created_at', 'desc')->first();
        /* $category = \App\Models\Category::create([
          'name' => $request->category_id,
          ]); */

        /* $request->request->set('category_id', $category->id); */
        $request->request->set('secondary_document_id', $secondary_document_id);
        $request->request->set('city_id', $request->recipient_city_id);

        $contracted_by = $request->name;
        if ($contracted_by != "") {
            /* check contracted by is exist or not */
            $contracted_by_check = \App\Models\Contact::where('company_name', $contracted_by)->first();
            if (empty($contracted_by_check)) {
                $contact = \App\Models\Contact::create([
                            'customer_id' => Auth::user()->customer->id,
                            'company_name' => $contracted_by,
                ]);
            }
        }
        $first_category = \App\Models\Category::where('name', 'Contracted By')->first();
        if ($first_category->id == $request->category_id) {
            /* check contract details exit or not */
            $check_contract_recipient_exist = \App\Models\SecondaryDocumentRecipient::where(['category_id' => $first_category->id, 'secondary_document_id' => $secondary_document_id])->where('id', '!=', $request->recipient_id)->first();

            /* if not empty delete previouse record */
            if (!empty($check_contract_recipient_exist)) {
                $contract_recipient_delete = \App\Models\SecondaryDocumentRecipient::where(['category_id' => $request->category_id, 'secondary_document_id' => $secondary_document_id])->delete();
            }
        }

        $secondary_document = \App\Models\SecondaryDocument::find($secondary_document_id);
        $data['secondary_document'] = $secondary_document;
        if ($request->continue == 'add') {
            $SecondaryDocumentRecipient = \App\Models\SecondaryDocumentRecipient::create(
                            $request->except(['_token', 'country']));
            if ($SecondaryDocumentRecipient) {
                Session::flash('success', 'Recipient successfully added');
            } else {
                Session::flash('success', 'Problem in Record saving');
            }
        } else {
            $SecondaryDocumentRecipient = SecondaryDocumentRecipient::find($request->recipient_id);
            $SecondaryDocumentRecipient->category_id = $request->category_id;
            $SecondaryDocumentRecipient->name = $request->name;
            $SecondaryDocumentRecipient->contact = $request->contact;
            $SecondaryDocumentRecipient->address = $request->address;
            $SecondaryDocumentRecipient->city_id = $request->city_id;
            $SecondaryDocumentRecipient->state_id = $request->state_id;
            $SecondaryDocumentRecipient->zip = $request->zip;
            $SecondaryDocumentRecipient->email = $request->email;
            $SecondaryDocumentRecipient->attn = $request->attn;
            $SecondaryDocumentRecipient->save();
            if ($SecondaryDocumentRecipient) {
                Session::flash('success', 'Recipient successfully updated');
            } else {
                Session::flash('success', 'Problem in Record updating');
            }
        }
        $data['cities'] = \App\Models\City::select()->get();
        $data['states'] = \App\Models\State::select()->orderBy('name')->get();
        $data['categories'] = \App\Models\Category::select()->get();
        $data['project_roles'] = \App\Models\ProjectRoles::select()->orderBy('name', 'ASC')->get();
        $data['recipients'] = \App\Models\SecondaryDocumentRecipient::select()->where('secondary_document_id', $secondary_document_id)->get();
        $data['count'] = \App\Models\SecondaryDocumentRecipient::select(DB::raw('COUNT(*) AS total'), 'categories.name')->groupBy('secondary_document_recipients.category_id')->join('categories', 'categories.id', '=', 'secondary_document_recipients.category_id')->where('secondary_document_id', $secondary_document_id)->get();

        $data['countries'] = \App\Models\Country::select()->get();
        $data['secondary_doc_id'] = $secondary_document_id;
        $data['partial_final_type'] = $request->partial_final_type;
        $admin_email = SecondaryDocument::getAdminEmail();

        /* if (Auth::user()->hasRole('account-manager') || Auth::user()->hasRole('admin')) {


          if (Auth::user()->hasRole('account-manager')) {
          $account_manager_email = SecondaryDocument::getOtherAccManagerEmailExcludingLoggedIn(Auth::user()->id);
          $customer_email = SecondaryDocument::getCustomerEmail($secondary_document->customer_id);
          if (isset($account_manager_email) && !empty($account_manager_email)) {
          $data['note_emails'] = [
          $customer_email->email,
          $admin_email->email,
          ];
          foreach ($account_manager_email AS $each_emails) {
          array_push($data['note_emails'], $each_emails->email);
          }
          } else {
          $data['note_emails'] = [
          $customer_email->email,
          $admin_email->email,
          ];
          }
          } else {
          $account_manager_email = SecondaryDocument::getAllAccountManagerEmail();
          $customer_email = SecondaryDocument::getCustomerEmail($secondary_document->customer_id);
          if (isset($account_manager_email) && !empty($account_manager_email)) {
          $data['note_emails'] = [
          $customer_email->email
          ];
          foreach ($account_manager_email AS $each_emails) {
          array_push($data['note_emails'], $each_emails->email);
          }
          } else {
          $data['note_emails'] = [
          $customer_email->email
          ];
          }
          }
          } else {

          $account_manager_email = SecondaryDocument::getAccountManagerEmail($secondary_document->customer_id);
          if (isset($account_manager_email) && !empty($account_manager_email)) {
          $data['note_emails'] = [
          $admin_email->email,
          $account_manager_email->email
          ];
          } else {
          $data['note_emails'] = [
          $admin_email->email
          ];
          }
          } */
        $data['note_emails'] = get_user_notice_email($secondary_document->customer_id);
        $data['notes'] = \App\Models\SecondaryDocumentNotes::leftjoin('users', 'users.id', '=', 'secondary_document_notes.user_id')->where('secondary_document_id', $secondary_document_id)
                        ->orderBy('secondary_document_notes.id', 'DESC')->get();
        $data['corrections'] = \App\Models\SecondaryDocumentCorrections::leftjoin('users', 'users.id', '=', 'secondary_document_corrections.user_id')->where('secondary_document_id', $secondary_document_id)
                        ->orderBy('secondary_document_corrections.id', 'DESC')->get();

//            $data['names'] = \App\Models\ContactDetail::select()
//                    ->where('contact_details.type', '=', 0)
//                    ->where('contacts.customer_id', '=', Auth::user()->customer->id)
//                    ->Join('contacts', 'contacts.id', '=', 'contact_details.contact_id')
//                    ->get();

        $data['names'] = \App\Models\Contact::select('contacts.id', 'contacts.company_name')
                //  ->where('contact_details.type', '=', 0)
                ->where('contacts.customer_id', '=', Auth::user()->customer->id)
                //  ->Join('contact_details', 'contact_details.contact_id', '=', 'contacts.id')
                ->orderBy('contacts.id', 'DESC')
                ->get();
        $data['parent_work_orders'] = \App\Models\WorkOrder::leftjoin('notice_templates', 'notice_templates.notice_id', '=', 'work_orders.notice_id')
                ->where('customer_id', '=', $secondary_document->customer_id)
                ->where('notice_templates.is_parent_work_order', '=', 1)
                ->select('work_orders.id')
                ->get();
        if (Auth::user()->hasRole('account-manager')) {

            $customers = \App\Models\Customer::leftjoin('users', 'users.id', '=', 'customers.user_id')
                    ->select('customers.id', 'users.name')
                    ->where('account_manager_id', '=', Auth::user()->id)
                    ->get();
            $data['customers'] = $customers;
        }
        if (Auth::user()->hasRole('admin')) {

            $customers = \App\Models\Customer::leftjoin('users', 'users.id', '=', 'customers.user_id')
                    ->select('customers.id', 'users.name')
                    ->get();
            $data['customers'] = $customers;
        }
        $data['attachment'] = \App\Models\SecondaryDocumentAttachments::where('secondary_document_id', '=', $secondary_document_id)
                ->get();

        $data['tab'] = 'recipients';
        return view('customer.secondary_document.edit', $data);
    }

    public function get_contacts(Request $request) {

        $name = $request->name;
        $recipt_id = !empty($request->recipt_id) ? $request->recipt_id : "";
        if (Auth::user()->hasRole('customer')) {
            $customer_id = Auth::user()->customer->id;
        } else {
            $customer_id = $request->customer_id;
        }
//        $contacts = \App\Models\ContactDetail::select()
//                ->where('contact_details.name', 'like', '%' . $name . '%')
//                ->where('contacts.customer_id', '=', Auth::user()->customer->id)
//                ->where('contact_details.type', '=', 0)
//                ->Join('contacts', 'contacts.id', '=', 'contact_details.contact_id')
//                ->get();
        if ($recipt_id != "") {
            $contacts = \App\Models\Contact::select()
                    ->where('contacts.customer_id', '=', $customer_id)
                    ->where('contacts.id', '=', $recipt_id)
                    ->first();
        } else {
            $contacts = \App\Models\Contact::select()
                    ->where('contacts.customer_id', '=', $customer_id)
                    ->where('contacts.company_name', '=', $name)
                    ->first();
        }

        if (isset($contacts) ) {

            $city_name = \App\Models\City::where('id', '=', $contacts->city_id)
                    ->select('name','id')
                    ->first();
            if (isset($city_name) && $city_name != NULL) {
                $contacts->city_id = $city_name->id;
                $contacts->city_name = $city_name->name;
            } else {
                $contacts->city_name = '';
                $contacts->city_id = '';
            }
        }


        /* if(isEmpty($contacts)){

          } */
        return $contacts;
    }

    public function delete($id) {
        $category = \App\Models\SecondaryDocumentRecipient::select('category_id')->where('id', $id)->get();
        $result = \App\Models\SecondaryDocumentRecipient::where('id', $id)->delete();

        foreach ($category as $key => $value) {
            $delete_category = \App\Models\Category::select()->where('id', $value->category_id)->delete();
        }

        if ($result) {
            Session::flash('The item has been deleted successfully');
        } else {
            Session::flash('Problem in record deleting');
        }

        return redirect()->back();
    }

    /**
     * Function is used to get project address
     * 
     * @param Request $request
     */
    public function getProjectAddress(Request $request) {

        if (Auth::user()->hasRole('account-manager')) {

            $customers = \App\Models\Customer::where('account_manager_id', '=', Auth::user()->id)
                            ->select('customers.id')
                            ->get()->toArray();

            $customer_id = [];
            foreach ($customers AS $each) {
                array_push($customer_id, $each['id']);
            }
           
            $project_address = \App\Models\SecondaryDocumentFields::select('notices.master_notice_id as master_notice_id','notices.name as notice_name', 'notices.is_rescind as rescind_notice', '.is_rescind as rescind_work_order', 'notices.type', 'secondary_documents.work_order_no', 'secondary_documents.created_at','usr.name as customer_name', 'secondary_documents.customer_id', 'usr.name as account_manager_name', 'secondary_documents.notice_id as notice_id', 'secondary_documents.status','secondary_documents.account_manager_id', 'secondary_document_fields.secondary_document_id','customers.company_name as company_name', 'secondary_document_fields.notice_id', DB::raw("(GROUP_CONCAT(notice_fields.name SEPARATOR '||')) as `field_names`"), DB::raw("(GROUP_CONCAT(value SEPARATOR '||')) as `field_values`"))
                ->rightjoin('notice_fields', 'notice_fields.id', '=', 'secondary_document_fields.notice_field_id')
                ->leftjoin('secondary_documents', 'secondary_documents.id', '=', 'secondary_document_fields.secondary_document_id')
                ->leftjoin('notices', 'notices.id', '=', 'secondary_documents.notice_id')
                ->leftjoin('customers', 'customers.id', '=', 'secondary_documents.customer_id')
                ->leftjoin('users as usr', 'usr.id', '=', 'secondary_documents.account_manager_id')->groupBy('secondary_document_fields.secondary_document_id', 'secondary_document_fields.notice_id')->where('secondary_documents.account_manager_id', '=',Auth::user()->id);
            $project_address = $project_address->get()->toArray();
            $addresses = [];
            if (isset($project_address) && !empty($project_address)) {
                foreach ($project_address as $fields_data) {
                    $field_names = explode('||', $fields_data['field_names']);
                    $field_values = explode('||', $fields_data['field_values']);
                    $field_names_values = (count($field_names) == count($field_values)) ? array_combine($field_names, $field_values) : [];
                    $field_names_values['default'] = '';
                    $addresses[] = array_merge($fields_data, $field_names_values);
                }
        }
         $projectAddress = [];
        foreach ($addresses as $k => $each_record) {
            if (isset($each_record['project_address']) && $each_record['project_address'] != null) {
                $projectAddress[$k] = $each_record['project_address'];
            }
        }

        /*    $project_address = SecondaryDocument::whereIn('customer_id', $customer_id)
                    ->select('address')
                    ->where('address', '!=', 'NULL')
                    ->orderBY('address', 'ASC')
                    ->get();*/
        } elseif (Auth::user()->hasRole('admin')) {
            $project_address = \App\Models\SecondaryDocumentFields::select('notices.master_notice_id as master_notice_id','notices.name as notice_name', 'notices.is_rescind as rescind_notice', '.is_rescind as rescind_work_order', 'notices.type', 'secondary_documents.work_order_no', 'secondary_documents.created_at', 'usr.name as customer_name', 'secondary_documents.customer_id', 'usr.name as account_manager_name', 'secondary_documents.notice_id as notice_id', 'secondary_documents.status','secondary_documents.account_manager_id', 'secondary_document_fields.secondary_document_id','customers.company_name as company_name','secondary_document_fields.notice_id', DB::raw("(GROUP_CONCAT(notice_fields.name SEPARATOR '||')) as `field_names`"), DB::raw("(GROUP_CONCAT(value SEPARATOR '||')) as `field_values`"))
                ->rightjoin('notice_fields', 'notice_fields.id', '=', 'secondary_document_fields.notice_field_id')
                ->leftjoin('secondary_documents', 'secondary_documents.id', '=', 'secondary_document_fields.secondary_document_id')
                ->leftjoin('notices', 'notices.id', '=', 'secondary_documents.notice_id')
                 ->leftjoin('customers', 'customers.id', '=', 'secondary_documents.customer_id')
                ->leftjoin('users as usr', 'usr.id', '=', 'secondary_documents.account_manager_id')->groupBy('secondary_document_fields.secondary_document_id', 'secondary_document_fields.notice_id');
           $project_address = $project_address->get()->toArray();
            $addresses = [];
            if (isset($project_address) && !empty($project_address)) {
                foreach ($project_address as $fields_data) {
                    $field_names = explode('||', $fields_data['field_names']);
                    $field_values = explode('||', $fields_data['field_values']);
                    $field_names_values = (count($field_names) == count($field_values)) ? array_combine($field_names, $field_values) : [];
                    $field_names_values['default'] = '';
                    $addresses[] = array_merge($fields_data, $field_names_values);
                }
        }
         $projectAddress = [];
        foreach ($addresses as $k => $each_record) {
            if (isset($each_record['project_address']) && $each_record['project_address'] != null) {
                $projectAddress[$k] = $each_record['project_address'];
            }
        }

         /*   $project_address = SecondaryDocument::select('address')
                    ->where('address', '!=', 'NULL')
                    ->orderBY('address', 'ASC')
                    ->get();*/
        } else {
          $project_address = \App\Models\SecondaryDocumentFields::select('notices.master_notice_id as master_notice_id','notices.name as notice_name', 'notices.is_rescind as rescind_notice', '.is_rescind as rescind_work_order', 'notices.type', 'secondary_documents.work_order_no', 'secondary_documents.created_at','usr.name as customer_name', 'secondary_documents.customer_id', 'usr.name as account_manager_name', 'secondary_documents.notice_id as notice_id', 'secondary_documents.status','secondary_documents.account_manager_id', 'secondary_document_fields.secondary_document_id', 'secondary_document_fields.notice_id','customers.company_name as company_name', DB::raw("(GROUP_CONCAT(notice_fields.name SEPARATOR '||')) as `field_names`"), DB::raw("(GROUP_CONCAT(value SEPARATOR '||')) as `field_values`"))
                ->rightjoin('notice_fields', 'notice_fields.id', '=', 'secondary_document_fields.notice_field_id')
                ->leftjoin('secondary_documents', 'secondary_documents.id', '=', 'secondary_document_fields.secondary_document_id')
                ->leftjoin('notices', 'notices.id', '=', 'secondary_documents.notice_id')
               ->leftjoin('customers', 'customers.id', '=', 'secondary_documents.customer_id')
                ->leftjoin('users as usr', 'usr.id', '=', 'secondary_documents.account_manager_id')->groupBy('secondary_document_fields.secondary_document_id', 'secondary_document_fields.notice_id')->where('customer_id', '=',  Auth::user()->customer->id);
            $project_address = $project_address->get()->toArray();
            $addresses = [];
            if (isset($project_address) && !empty($project_address)) {
                foreach ($project_address as $fields_data) {
                    $field_names = explode('||', $fields_data['field_names']);
                    $field_values = explode('||', $fields_data['field_values']);
                    $field_names_values = (count($field_names) == count($field_values)) ? array_combine($field_names, $field_values) : [];
                    $field_names_values['default'] = '';
                    $addresses[] = array_merge($fields_data, $field_names_values);
                }
        }

        $projectAddress = [];
        foreach ($addresses as $k => $each_record) {
            if (isset($each_record['project_address']) && $each_record['project_address'] != null) {
                $projectAddress[$k] = $each_record['project_address'];
            }
        }
            /*$project_address = SecondaryDocument::where('customer_id', '=', Auth::user()->customer->id)
                    ->select('address')
                    ->where('address', '!=', 'NULL')
                    ->orderBY('address', 'ASC')
                    ->get();*/
        }
//dd($projectAddress);
        return response()->json($projectAddress);
    }

    /**
     * Function is used to get secondary doc ids
     * 
     * @param Request $request
     */
    public function getSecondaryDocIds(Request $request) {

        if (Auth::user()->hasRole('account-manager')) {

            $customers = \App\Models\Customer::where('account_manager_id', '=', Auth::user()->id)
                            ->select('customers.id')
                            ->get()->toArray();

            $customer_id = [];
            foreach ($customers AS $each) {
                array_push($customer_id, $each['id']);
            }
            $secondaryDocIds = SecondaryDocument::whereIn('customer_id', $customer_id)
                    ->select('id')
                    ->orderBY('id', 'DESC')
                    ->get();
        } elseif (Auth::user()->hasRole('admin')) {
            $secondaryDocIds = SecondaryDocument::select('id')
                    ->orderBY('id', 'DESC')
                    ->get();
        } else {
            $secondaryDocIds = SecondaryDocument::where('customer_id', '=', Auth::user()->customer->id)
                    ->select('id')
                    ->orderBY('id', 'DESC')
                    ->get();
        }
        return response()->json($secondaryDocIds);
    }

    /**
     * Function is used to get secondary doc ids
     * 
     * @param Request $request
     */
    public function getSecondaryDocIds_V1(Request $request) {
        $query = $request->get('term', '');

        if (Auth::user()->hasRole('account-manager')) {

            $customers = \App\Models\Customer::where('account_manager_id', '=', Auth::user()->id)
                            ->pluck('customers.id')
                            ->toArray();

            $customer_id = [];
            foreach ($customers AS $each) {
                array_push($customer_id, $each['id']);
            }
            $secondaryDocIds = SecondaryDocument::where('account_manager_id', Auth::user()->id)
                    ->where('id', 'LIKE', '%' . $query . '%')
                    ->pluck('id')
                    ->toArray();
        } elseif (Auth::user()->hasRole('admin')) {
            $secondaryDocIds = SecondaryDocument::where('id', 'LIKE', '%' . $query . '%')
                    ->pluck('id')
                    ->toArray();
        } else {
            $secondaryDocIds = SecondaryDocument::where('customer_id', '=', Auth::user()->customer->id)
                    ->where('id', 'LIKE', '%' . $query . '%')
                    ->pluck('id')
                    ->toArray();
        }
        return response()->json($secondaryDocIds);
    }

    /**
     * Function is used to get parent work order ids
     * 
     * @param Request $request
     */
    public function getSecondaryParentWorkOrder(Request $request) {
        if (Auth::user()->hasRole('account-manager')) {
            $customers = \App\Models\Customer::where('account_manager_id', '=', Auth::user()->id)
                            ->select('customers.id')
                            ->get()->toArray();

            $customer_id = [];
            foreach ($customers AS $each) {
                array_push($customer_id, $each['id']);
            }
            $parenworkorder = SecondaryDocument::whereIn('customer_id', $customer_id)
                    ->whereNotNull('work_order_no')
                    ->select('work_order_no')
                    ->orderBY('id', 'DESC')->groupBy('work_order_no')
                    ->get();
        } elseif (Auth::user()->hasRole('admin')) {
            $parenworkorder = SecondaryDocument::select('work_order_no')
                    ->whereNotNull('work_order_no')->groupBy('work_order_no')
                    ->orderBY('id', 'DESC')
                    ->get();
        } else {
            $parenworkorder = SecondaryDocument::where('customer_id', '=', Auth::user()->customer->id)
                    ->whereNotNull('work_order_no')
                    ->select('work_order_no')
                    ->orderBY('id', 'DESC')->groupBy('work_order_no')
                    ->get();
        }
        return response()->json($parenworkorder);
    }

    /**
     * Function is used to get parent work order ids
     * 
     * @param Request $request
     */
    public function getSecondaryParentWorkOrder_V1(Request $request) {
        $query = $request->get('term', '');

        if (Auth::user()->hasRole('account-manager')) {
            $customers = \App\Models\Customer::where('account_manager_id', '=', Auth::user()->id)
                            ->pluck('customers.id')
                            ->toArray();

            $customer_id = [];
            foreach ($customers AS $each) {
                array_push($customer_id, $each['id']);
            }
            $parenworkorder = SecondaryDocument::where('account_manager_id','=', Auth::user()->id)
                    ->where('work_order_no', 'LIKE', '%' . $query . '%')
                    ->whereNotNull('work_order_no')
                    ->pluck('work_order_no')
                    ->toArray();
        } elseif (Auth::user()->hasRole('admin')) {
        $parenworkorder = SecondaryDocument::where('work_order_no', 'LIKE', '%' . $query . '%')
                    ->whereNotNull('work_order_no')
                    ->groupBy('work_order_no')
                    ->pluck('work_order_no')
                    ->toArray();
        } else {
            $parenworkorder = SecondaryDocument::where('customer_id', '=', Auth::user()->customer->id)
                    ->where('work_order_no', 'LIKE', '%' . $query . '%')
                    ->whereNotNull('work_order_no')
                    ->groupBy('work_order_no')
                    ->pluck('work_order_no')
                    ->toArray();
        }
        return response()->json($parenworkorder);
    }

    /**
     * Function is used to get work order number
     * @param Request $request
     */
    public function getWorkOrdeNumber(Request $request) {
        if (Auth::user()->hasRole('account-manager') || Auth::user()->hasRole('admin')) {

            $data = '';
        } else {
            $data = \App\Models\WorkOrder::leftjoin('notice_templates', 'notice_templates.notice_id', '=', 'work_orders.notice_id')
                    ->where('customer_id', '=', Auth::user()->customer->id)
                    ->where('notice_templates.is_parent_work_order', '=', 1)
                    ->where('work_orders.id','!=',$request->work_order_id)
                    ->select('work_orders.id')
                    ->groupBy('work_orders.id')
                    ->get();
        }
        //dd($data);
        return response()->json($data);
    }
    
    /**
     * Function is used to get work order number
     * @param Request $request
     */
    public function getWorkOrdeNumberV1(Request $request) {
        $query = $request->get('term', '');
        if (Auth::user()->hasRole('account-manager') || Auth::user()->hasRole('admin')) {

            $data = '';
        } else {
            $data = \App\Models\WorkOrder::leftjoin('notice_templates', 'notice_templates.notice_id', '=', 'work_orders.notice_id')
                    ->where('customer_id', '=', Auth::user()->customer->id)
                    ->where('notice_templates.is_parent_work_order', '=', 1)
                    ->where('work_orders.id','!=',$request->work_order_id)
                    ->where('work_orders.id', 'LIKE', '%' . $query . '%')
                    ->select('work_orders.id')
                    ->groupBy('work_orders.id')
                    ->pluck('work_orders.id')->toArray();
        }
        return response()->json($data);
    }

    /**
     * Function is used to get work order number in account manager login
     * @param Request $request
     */
    public function getAccountManagerWorkOrdeNumber(Request $request) {
        $data = \App\Models\WorkOrder::leftjoin('notice_templates', 'notice_templates.notice_id', '=', 'work_orders.notice_id')
                ->where('customer_id', '=', $request->customer_id)
                ->where('notice_templates.is_parent_work_order', '=', 1)
                ->select('work_orders.id')
                ->get();
        return response()->json($data);
    }

    /**
     * Function is used to get authorise agent in account manager login
     * @param Request $request
     */
    public function getAccountManagerAuthoriseAgent(Request $request) {

        $data['officers_directors'] = \App\Models\CustomerAgent::where('customer_id', '=', $request->customer_id)
                ->get();
        return response()->json($data);
    }

    /**
     * Function is used to get work order details
     */
    public function getWorkOrderDetails(Request $request) {
        $work_order_no = $request->work_order_no;

        $result = \App\Models\WorkOrderFields::leftjoin('notice_fields', 'notice_fields.id', '=', 'work_order_fields.notice_field_id')
                ->where('work_order_fields.workorder_id', '=', $work_order_no)
                ->get();
        foreach ($result AS $k => $val) {
            if ($val->name == 'county' && $val->value != "") {
                $county_name = \App\Models\City::select(['id', \DB::raw('concat(county) as name')])->find($val->value);
                $val->county_name = $county_name->name;
            }
        }

        if (Auth::user()->hasRole('account-manager') || Auth::user()->hasRole('admin')) {
            $parent_attachment = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $work_order_no)
                    ->select('type', 'title', 'original_file_name', 'created_at', 'file_name')
                    ->get();
        } else {
            $parent_attachment = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $work_order_no)
                    ->select('type', 'title', 'original_file_name', 'created_at', 'file_name')
                    ->where('visibility', '=', 0)
                    ->get();
        }
        $owner_recipient = \App\Models\Recipient::where('work_order_id', '=', $work_order_no)->whereIn('category_id', array('1','22'))
                ->get();
        $attachment = [];
        if (isset($parent_attachment) && count($parent_attachment) > 0) {
            foreach ($parent_attachment As $k => $v) {
                $extension = "";
                if ($v->original_file_name != "") {
                    $extension = explode('.', $v->original_file_name);
                }
                //if (file_exists(public_path() . '/attachment/work_order_document/' . $v->file_name)) {
                $attachment[$k]['db_arr'] = [
                    $v->type,
                    $v->title,
                    $v->file_name,
                    $v->original_file_name
                ];
                //}
                if ($v->type == 1) {
                    $v->type = 'Bond';
                } elseif ($v->type == 2) {
                    $v->type = 'Notice to Commencement';
                } elseif ($v->type == 3) {
                    $v->type = 'Permit';
                } elseif ($v->type == 4) {
                    $v->type = 'Contract';
                } elseif ($v->type == 5) {
                    $v->type = 'Invoice';
                } elseif ($v->type == 6) {
                    $v->type = 'Final Release';
                } elseif ($v->document_type == 7) {
                    $v->type = 'Partial Release';
                } elseif ($v->type == 8) {
                    $v->type = 'Misc Document';
                } elseif ($v->type == 9) {
                    $v->type = 'Signed Document';
                } elseif ($v->type == 10) {
                    $v->type = 'Folio';
                }elseif ($v->type == 11) {
                    $v->type = 'COL Signed';
                }elseif ($v->type == 12) {
                    $v->type = 'Recorded COL/SCOL';
                }elseif ($v->type == 13) {
                    $v->type = 'Pending Signature';
                }
                if (!file_exists(public_path() . '/attachment/secondary_document/parent_document')) {
                    $dir = mkdir(public_path() . '/attachment/secondary_document/parent_document', 0777, true);
                } else {
                    $dir = public_path() . '/attachment/secondary_document/parent_document/';
                }
                if ($v->file_name != "" && file_exists(public_path() . '/attachment/work_order_document/' . $v->file_name)) {

                    \File::copy(public_path() . '/attachment/work_order_document/' . $v->file_name, public_path() . '/attachment/secondary_document/parent_document/' . $v->file_name);
                }
                $created_date = date('d M y h:i A', strtotime($v->created_at));
                $parent_attachment[$k]['created_date'] = $created_date;
                $parent_attachment[$k]['extension'] = "";

                if (!empty($extension) && isset($extension[1])) {
                    $parent_attachment[$k]['extension'] = $extension[1];
                }
                $doc_value = "";
                if ($v->title != "") {
                    $doc_value = $v->title;
                }
                $original_file_name = "";
                if ($v->original_file_name != "") {
                    $original_file_name = $v->original_file_name;
                }
                $attachment[$k]['ajax_arr'] = [
                    $v->type,
                    $doc_value,
                    $original_file_name,
                    $parent_attachment[$k]['created_date'],
                    $parent_attachment[$k]['extension']
                ];
            }
        }

        return response()->json(['success' => $result, 'parent_attachment' => $attachment, 'owner_recipient' => $owner_recipient]);
    }

    /**
     * Function is used to get work order details
     */
    public function getWorkOrderDocumentDetails(Request $request) {
        $work_order_no = $request->work_order_no;
        
        $result = \App\Models\WorkOrderFields::leftjoin('notice_fields', 'notice_fields.id', '=', 'work_order_fields.notice_field_id')
                ->where('work_order_fields.workorder_id', '=', $work_order_no)
                ->get();
        foreach ($result AS $k => $val) {
            if ($val->name == 'county' && $val->value != "") {
                $county_name = \App\Models\City::select(['id', \DB::raw('concat(county) as name')])->find($val->value);
                $val->county_name = $county_name->name;
            }
        }
        if (Auth::user()->hasRole('account-manager') || Auth::user()->hasRole('admin')) {
            $parent_attachment = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $work_order_no)
                    ->select('type', 'title', 'original_file_name', 'created_at', 'file_name','id')
                    ->get();
        } else {
            $parent_attachment = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $work_order_no)
                    ->select('type', 'title', 'original_file_name', 'created_at', 'file_name','id')
                    ->where('visibility', '=', 0)
                    ->get();
        }


        $owner_recipient = \App\Models\Recipient::where('work_order_id', '=', $work_order_no)->whereIn('category_id', array('1','22'))
                ->get();

        $general_contracted_recipient = \App\Models\Recipient::where('work_order_id', '=', $work_order_no)->where('category_id', '2')
                ->get();



        // dd($parent_attachment);
        $attachment = [];
        if (isset($parent_attachment) && count($parent_attachment) > 0) {
            foreach ($parent_attachment As $k => $v) {
                $extension = "";
                if ($v->original_file_name != "") {
                    $extension = explode('.', $v->original_file_name);
                }
                // if (file_exists(public_path() . '/attachment/work_order_document/' . $v->file_name)) {
                $attachment[$k]['db_arr'] = [
                    $v->type,
                    $v->title,
                    $v->file_name,
                    $v->original_file_name,
                    $v->id
                ];
                //  }

                if ($v->type == 1) {
                    $v->type = 'Bond';
                } elseif ($v->type == 2) {
                    $v->type = 'Notice to Commencement';
                } elseif ($v->type == 3) {
                    $v->type = 'Permit';
                } elseif ($v->type == 4) {
                    $v->type = 'Contract';
                } elseif ($v->type == 5) {
                    $v->type = 'Invoice';
                } elseif ($v->type == 6) {
                    $v->type = 'Final Release';
                } elseif ($v->type == 7) {
                    $v->type = 'Partial Release';
                } elseif ($v->type == 8) {
                    $v->type = 'Misc Document';
                } elseif ($v->type == 9) {
                    $v->type = 'Signed Document';
                } elseif ($v->type == 10) {
                    $v->type = 'Folio';
                }elseif ($v->type == 11) {
                    $v->type = 'COL Signed';
                }elseif ($v->type == 12) {
                    $v->type = 'Recorded COL/SCOL';
                }elseif ($v->type == 13) {
                    $v->type = 'Pending Signature';
                }
                if (!file_exists(public_path() . '/attachment/work_order_document/parent_document')) {

                    $dir = mkdir(public_path() . '/attachment/work_order_document/parent_document', 0777, true);
                } else {
                    $dir = public_path() . '/attachment/work_order_document/parent_document/';
                }
                if ($v->file_name != "" && file_exists(public_path() . '/attachment/work_order_document/' . $v->file_name)) {
                    \File::copy(public_path() . '/attachment/work_order_document/' . $v->file_name, public_path() . '/attachment/work_order_document/parent_document/' . $v->file_name);
                }
                $created_date = date('d M y h:i A', strtotime($v->created_at));
                $parent_attachment[$k]['created_date'] = $created_date;
                $parent_attachment[$k]['extension'] = "";

                if (!empty($extension)) {
                  if(!empty($extension[1])){
                    $parent_attachment[$k]['extension'] = $extension[1];
                  }
                }
                $doc_value = "";
                if ($v->title != "") {
                    $doc_value = $v->title;
                }
                $original_file_name = "";
                if ($v->original_file_name != "") {
                    $original_file_name = $v->original_file_name;
                }
                $attachment[$k]['ajax_arr'] = [
                    $v->type,
                    $doc_value,
                    $original_file_name,
                    $parent_attachment[$k]['created_date'],
                    $parent_attachment[$k]['extension'],
                    //$parent_attachment[$k]['id']
                ];
            }
        }

        return response()->json(['success' => $result, 'parent_attachment' => $attachment, 'owner_recipient' => $owner_recipient, 'general_contracted_recipient' => $general_contracted_recipient]);
    }

    /**
     * Function is used to store note
     */
    public function storeNote(Request $request) {

        $rules = ['note' => 'required'
        ];
        if (Auth::user()->hasRole('customer')) {
            $rules = ['email' => 'required', 'note' => 'required',];
        }
        $validator = \Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }
        $secondaryDoc = SecondaryDocument::find($request->doc_id);
        $result = \App\Models\SecondaryDocumentNotes::create([
                    'secondary_document_id' => $request->doc_id,
                    'customer_id' => $secondaryDoc->customer_id,
                    'note' => $request->note,
                    'email' => ($request->email) ? $request->email : NULL,
                    'user_id' => Auth::user()->id,
        ]);
        $notes = \App\Models\SecondaryDocumentNotes::leftjoin('users', 'users.id', '=', 'secondary_document_notes.user_id')->where('customer_id', '=', $secondaryDoc->customer_id)
                ->select('secondary_document_notes.note', 'secondary_document_notes.email', 'users.name')
                ->where('secondary_document_notes.id', '=', $result->id)
                ->first();
        /* $senderName = \App\Models\Customer::leftjoin('users', 'users.id', '=', 'customers.user_id')->select('users.name')->where('customers.id', '=',$secondaryDoc->customer_id)->first();  */
        $notes->doc_id = $request->doc_id;
        if ($request->email != NULL) {
            Mail::to($request->email)->send(new SecondaryDocumentNote($notes));
        }
        $data['secondary_doc_id'] = $secondaryDoc->id;
        if (Auth::user()->hasRole('account-manager')) {

            $customers = \App\Models\Customer::leftjoin('users', 'users.id', '=', 'customers.user_id')
                    ->select('customers.id', 'users.name')
                    ->where('account_manager_id', '=', Auth::user()->id)
                    ->get();
            $data['customers'] = $customers;
        }
        if (Auth::user()->hasRole('admin')) {

            $customers = \App\Models\Customer::leftjoin('users', 'users.id', '=', 'customers.user_id')
                    ->select('customers.id', 'users.name')
                    ->get();
            $data['customers'] = $customers;
        }
        if (Auth::user()->hasRole('account-manager') || Auth::user()->hasRole('admin')) {

            $data['names'] = \App\Models\Contact::select('contacts.id', 'contacts.company_name')
                    //->where('contact_details.type', '=', 0)
                    ->where('contacts.customer_id', '=', $secondaryDoc->customer_id)
                    // ->Join('contact_details', 'contact_details.contact_id', '=', 'contacts.id')
                    ->orderBy('contacts.id', 'DESC')
                    ->get();
            $data['officers_directors'] = \App\Models\CustomerAgent::where('customer_id', '=', $secondaryDoc->customer_id)
                    ->get();
        } else {
            $data['names'] = \App\Models\Contact::select('contacts.id', 'contacts.company_name')
                    //->where('contact_details.type', '=', 0)
                    ->where('contacts.customer_id', '=', Auth::user()->customer->id)
                    // ->Join('contact_details', 'contact_details.contact_id', '=', 'contacts.id')
                    ->orderBy('contacts.id', 'DESC')
                    ->get();
            $data['officers_directors'] = \App\Models\CustomerAgent::where('customer_id', '=', Auth::user()->customer->id)
                    ->get();
        }
        if ($result) {
            return response()->json(['success' => 'Note submitted successfully', 'notes' => $notes]);
        }
    }

    /**
     * 
     * @param Request $request
     */
    public function sendMailPdf(Request $request) {
        $email = $request->email;
        $secondary_doc_id = $request->secondary_doc_id;
      //  $file_org_name=$request->file('file')->originalName;
        $ext = $request->file('file')->getClientOriginalExtension();
        $secondaryDocObj = SecondaryDocument::find($secondary_doc_id);

        //$file_name = $secondaryDocObj->file_name;

        $file_path = public_path() . '/secondary_document/';
        $filename = $secondary_doc_id . '_' . time().'.'.$ext;
        
        $request->file('file')->move($file_path, $filename);
       // die();
        $data = [
            'customer' => $secondaryDocObj->customer,
            'to' => $email,
            'pdf' => $file_path.$filename
        ];

         Mail::to($data['to'])->send(new SecondaryDocumentAttachmentMail($data));
          // Mail::send('emails.welcome', $data, function ($message) {
          //   $message->to($email);
          //   $message->attach($file_path);
          // });
        return redirect::to('customer/secondary-document')->with('success', 'Mail successfully sent.');
    }

    public function destroy($id, $secondary_document_id, $partial_final_type) {


        $result = \App\Models\SecondaryDocumentRecipient::where('id', $id)->delete();
        $secondary_document = SecondaryDocument::find($secondary_document_id);
        $data['secondary_document'] = $secondary_document;
        $data['cities'] = \App\Models\City::select()->get();
        $data['states'] = \App\Models\State::select()->where('status', 1)->orderBy('name')->get();

        $data['recipients'] = \App\Models\SecondaryDocumentRecipient::select()->where('secondary_document_id', $secondary_document_id)->get();
        $data['count'] = \App\Models\SecondaryDocumentRecipient::select(DB::raw('COUNT(*) AS total'), 'categories.name')->groupBy('secondary_document_recipients.category_id')->join('categories', 'categories.id', '=', 'secondary_document_recipients.category_id')->where('secondary_document_id', $secondary_document_id)->get();
        $data['categories'] = \App\Models\Category::select()->get();
        $data['project_roles'] = \App\Models\ProjectRoles::select()->orderBy('name', 'ASC')->get();
        $data['countries'] = \App\Models\Country::select()->get();
        $data['secondary_doc_id'] = $secondary_document_id;
        $data['partial_final_type'] = $partial_final_type;
        $admin_email = SecondaryDocument::getAdminEmail();

        /* if (Auth::user()->hasRole('account-manager') || Auth::user()->hasRole('admin')) {


          if (Auth::user()->hasRole('account-manager')) {
          $account_manager_email = SecondaryDocument::getOtherAccManagerEmailExcludingLoggedIn(Auth::user()->id);
          $customer_email = SecondaryDocument::getCustomerEmail($secondary_document->customer_id);
          if (isset($account_manager_email) && !empty($account_manager_email)) {
          $data['note_emails'] = [
          $customer_email->email,
          $admin_email->email,
          ];
          foreach ($account_manager_email AS $each_emails) {
          array_push($data['note_emails'], $each_emails->email);
          }
          } else {
          $data['note_emails'] = [
          $customer_email->email,
          $admin_email->email,
          ];
          }
          } else {
          $account_manager_email = SecondaryDocument::getAllAccountManagerEmail();
          $customer_email = SecondaryDocument::getCustomerEmail($secondary_document->customer_id);
          if (isset($account_manager_email) && !empty($account_manager_email)) {
          $data['note_emails'] = [
          $customer_email->email
          ];
          foreach ($account_manager_email AS $each_emails) {
          array_push($data['note_emails'], $each_emails->email);
          }
          } else {
          $data['note_emails'] = [
          $customer_email->email
          ];
          }
          }
          } else {

          $account_manager_email = SecondaryDocument::getAccountManagerEmail($secondary_document->customer_id);
          if (isset($account_manager_email) && !empty($account_manager_email)) {
          $data['note_emails'] = [
          $admin_email->email,
          $account_manager_email->email
          ];
          } else {
          $data['note_emails'] = [
          $admin_email->email
          ];
          }
          } */
        $data['note_emails'] = get_user_notice_email($secondary_document->customer_id);
        $data['notes'] = \App\Models\SecondaryDocumentNotes::leftjoin('users', 'users.id', '=', 'secondary_document_notes.user_id')->where('secondary_document_id', $secondary_document_id)
                        ->orderBy('secondary_document_notes.id', 'DESC')->get();
        $data['corrections'] = \App\Models\SecondaryDocumentCorrections::leftjoin('users', 'users.id', '=', 'secondary_document_corrections.user_id')->where('secondary_document_id', $secondary_document_id)
                        ->orderBy('secondary_document_corrections.id', 'DESC')->get();

        $data['names'] = \App\Models\Contact::select()
                //  ->where('contact_details.type', '=', 0)
                ->where('contacts.customer_id', '=', $secondary_document->customer_id)
                //  ->Join('contact_details', 'contact_details.contact_id', '=', 'contacts.id')
                ->orderBy('contacts.id', 'DESC')
                ->get();
        $data['parent_work_orders'] = \App\Models\WorkOrder::leftjoin('notice_templates', 'notice_templates.notice_id', '=', 'work_orders.notice_id')
                ->where('customer_id', '=', $secondary_document->customer_id)
                ->where('notice_templates.is_parent_work_order', '=', 1)
                ->select('work_orders.id')
                ->get();
        $secondary_document_status = \App\Models\SecondaryDocument::find($secondary_document_id);
        if ($secondary_document_status->status == '1') {
            $data['status'] = 'request';
        } else {
            $data['status'] = 'draft';
        }

        if (Auth::user()->hasRole('account-manager')) {

            $customers = \App\Models\Customer::leftjoin('users', 'users.id', '=', 'customers.user_id')
                    ->select('customers.id', 'users.name')
                    ->where('account_manager_id', '=', Auth::user()->id)
                    ->get();
            $data['customers'] = $customers;
        }
        if (Auth::user()->hasRole('admin')) {

            $customers = \App\Models\Customer::leftjoin('users', 'users.id', '=', 'customers.user_id')
                    ->select('customers.id', 'users.name')
                    ->get();
            $data['customers'] = $customers;
        }
        $data['attachment'] = \App\Models\SecondaryDocumentAttachments::where('secondary_document_id', '=', $secondary_document_id)
                ->get();
        $data['tab'] = 'recipients';

        return view('customer.secondary_document.edit', $data);
    }

    public function editSecondaryDocument($id,$edit_flag=NULL) {
        $data['edit_flag'] = $edit_flag;
        $data['tab'] = "project";
        $secondary_document = SecondaryDocument::find($id);

      //dd($secondary_document);
        $secondary_document->due_date = str_replace('-', '/', $secondary_document->due_date);

        //$secondary_document->due_date = date('d/m/Y',strtotime($secondary_document->due_date));
        $data['secondary_document'] = $secondary_document;
        $data['secondary_doc_id'] = $secondary_document->id;
        $result = \App\Models\SecondaryDocument::select('state_id')->join('notice_fields', 'notice_fields.notice_id', '=', 'secondary_documents.notice_id')->where('secondary_documents.id', $id)->limit(1)->get()->toArray();

        $notice = \App\Models\Notice::select('name', 'id')->where('id',$secondary_document->notice_id)->get()->toArray();
        $data['notice'] = $notice[0];
        $data['notice_id'] = $secondary_document->notice_id;
        $data['notice_fields']=[];
        if(count($result)>0){
           $data['notice_fields'] = \App\Models\NoticeField::select()->join('secondary_document_fields', 'secondary_document_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('secondary_document_fields.notice_id', $secondary_document->notice_id)->where('secondary_document_fields.secondary_document_id',$secondary_document->id)->orderBy('sort_order', 'asc')->get(); 
        }
        

        $data['notes'] = \App\Models\SecondaryDocumentNotes::leftjoin('users', 'users.id', '=', 'secondary_document_notes.user_id')->where('secondary_document_id', $id)
                        ->orderBy('secondary_document_notes.id', 'DESC')->get();
        $data['corrections'] = \App\Models\SecondaryDocumentCorrections::leftjoin('users', 'users.id', '=', 'secondary_document_corrections.user_id')->where('secondary_document_id', $id)
                        ->orderBy('secondary_document_corrections.id', 'DESC')->get();
        if ($secondary_document->status == '1') {
            $data['status'] = 'request';
        } else {
            $data['status'] = 'draft';
        }
        $data['attachment'] = \App\Models\SecondaryDocumentAttachments::where('secondary_document_id', '=', $id)
                ->get();

        $data['partial_final_type'] = $secondary_document->type;
        $data['secondary_doc_id'] = $id;
//        $data['cities'] = \App\Models\City::select()->get();
        $data['states'] = \App\Models\State::select()->where('status', '=', 1)->orderBy('name')->get();
        $data['recipients'] = \App\Models\SecondaryDocumentRecipient::select()->where('secondary_document_id', $id)->get();
        $data['count'] = \App\Models\SecondaryDocumentRecipient::select(DB::raw('COUNT(*) AS total'), 'categories.name')->groupBy('secondary_document_recipients.category_id')->join('categories', 'categories.id', '=', 'secondary_document_recipients.category_id')->where('secondary_document_id', $id)->get();

        $data['categories'] = \App\Models\Category::select()->get();
        $data['project_roles'] = \App\Models\ProjectRoles::select()->orderBy('name', 'ASC')->get();
//        $data['countries'] = \App\Models\Country::select()->get();
//        $data['names'] = \App\Models\ContactDetail::select()
//                ->where('contact_details.type', '=', 0)
//                ->where('contacts.customer_id', '=', Auth::user()->customer->id)
//                ->Join('contacts', 'contacts.id', '=', 'contact_details.contact_id')
//                ->get();
        $data['parent_work_orders'] = \App\Models\WorkOrder::leftjoin('notice_templates', 'notice_templates.notice_id', '=', 'work_orders.notice_id')
                ->where('customer_id', '=', $secondary_document->customer_id)
                ->where('notice_templates.is_parent_work_order', '=', 1)
                ->select('work_orders.id')
                ->get();

        $data['names'] = \App\Models\Contact::select()
                //    ->where('contact_details.type', '=', 0)
                ->where('contacts.customer_id', '=', $secondary_document->customer_id)
                //     ->Join('contact_details', 'contact_details.contact_id', '=', 'contacts.id')
                ->orderBy('contacts.id', 'DESC')
                ->get();
        $data['officers_directors'] = \App\Models\CustomerAgent::where('customer_id', '=', $secondary_document->customer_id)
                ->get();
        $admin_email = SecondaryDocument::getAdminEmail();

        /*  if (Auth::user()->hasRole('account-manager') || Auth::user()->hasRole('admin')) {


          if (Auth::user()->hasRole('account-manager')) {
          $account_manager_email = SecondaryDocument::getOtherAccManagerEmailExcludingLoggedIn(Auth::user()->id);
          $customer_email = SecondaryDocument::getCustomerEmail($secondary_document->customer_id);
          if (isset($account_manager_email) && !empty($account_manager_email)) {
          $data['note_emails'] = [
          $customer_email->email,
          $admin_email->email,
          ];
          foreach ($account_manager_email AS $each_emails) {
          array_push($data['note_emails'], $each_emails->email);
          }
          } else {
          $data['note_emails'] = [
          $customer_email->email,
          $admin_email->email,
          ];
          }
          } else {
          $account_manager_email = SecondaryDocument::getAllAccountManagerEmail();
          $customer_email = SecondaryDocument::getCustomerEmail($secondary_document->customer_id);
          if (isset($account_manager_email) && !empty($account_manager_email)) {
          $data['note_emails'] = [
          $customer_email->email
          ];
          foreach ($account_manager_email AS $each_emails) {
          array_push($data['note_emails'], $each_emails->email);
          }
          } else {
          $data['note_emails'] = [
          $customer_email->email
          ];
          }
          }
          } else {

          $account_manager_email = SecondaryDocument::getAccountManagerEmail($secondary_document->customer_id);
          if (isset($account_manager_email) && !empty($account_manager_email)) {
          $data['note_emails'] = [
          $admin_email->email,
          $account_manager_email->email
          ];
          } else {
          $data['note_emails'] = [
          $admin_email->email
          ];
          }
          } */

        $data['note_emails'] = get_user_notice_email($secondary_document->customer_id);
        
        return view('customer.secondary_document.edit', $data);
    }

    public function updateSecondaryDocument(Request $request) {
       /* $rules = [//'customer' => 'required',
//            'work_order_no' => 'required',
            'project_address' => 'required',
            //  'project_country' => 'required',
            'amount' => 'required',
            // 'amount_text' => 'required',
//            'due_date' => 'required',
//            'first_name' => 'required',
            'signature' => 'required',
//            'comment' => 'required'
        ];
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }*/

        $fields = array_merge($request->except(['_token','edit_secondary_doc_id','notice_id','type','doc_input','edit_document_name','edit_doc_id','remove_file','secondary_doc_attachment','remove_doc','submit','work_order_no','parent_country','contracted_by_exist']));
        $fieldsData = [];
        $values = [];
        $i = 0;
        foreach ($fields as $key => $value) {
            $field = explode("_", $key);
            $fieldsData[$i] = $field['0'];
            $values[$i] = $value;
            $i++;
        }
       
        $secondaryDocObj = SecondaryDocument::find($request->edit_secondary_doc_id);
        $request->request->set('due_date', date('m-d-Y', strtotime($request->due_date)));
        $customer_id = $secondaryDocObj->customer_id;
        $prev_work_order_no = $secondaryDocObj->work_order_no;
        $request->request->set('project_country', $request->county_id);
        $data['officers_directors'] = \App\Models\CustomerAgent::where('customer_id', '=', $secondaryDocObj->customer_id)
                ->get();
        $secondaryDocObj->work_order_no = $request->work_order_no;
       // $secondaryDocObj->account_manager_id = $account_manager_id;
      /*  $secondaryDocObj->customer = $request->contracted_by;
        $secondaryDocObj->work_order_no = $request->work_order_no;
        $secondaryDocObj->address = $request->project_address;
        $secondaryDocObj->project_name = $request->project_name;
        $secondaryDocObj->country_id = $request->county_id;
        $secondaryDocObj->amount = $request->amount;
        $secondaryDocObj->amount_in_text = $request->amount_text;
        $secondaryDocObj->due_date = $request->due_date;
        // $secondaryDocObj->first_name = $request->first_name;
        $secondaryDocObj->signature = $request->signature;
        $secondaryDocObj->title = $request->title;
        $secondaryDocObj->comment = $request->comment;
        $secondaryDocObj->service_labour_furnished = $request->service_labour_furnished;
        $secondaryDocObj->contracted_by = $request->contracted_by;
        $secondaryDocObj->project_owner = $request->project_owner;
        $secondaryDocObj->notary_seal = isset($request->notary_seal) ? $request->notary_seal : '0';
        $secondaryDocObj->legal_description = $request->legal_description;*/
        if ($request->submit == "continue") {

            $secondaryDocObj->status = 1;
            // $secondaryDocObj->save();

            $data['status'] = 'request';
        } else if ($request->submit == "save_for_later") {
            /*    $secondaryDocObj->customer = $request->customer;
              $secondaryDocObj->work_order_no = $request->work_order_no;
              $secondaryDocObj->address = $request->project_address;
              $secondaryDocObj->project_name = $request->project_name;
              $secondaryDocObj->country_id = $request->project_country;
              $secondaryDocObj->amount = $request->amount;
              $secondaryDocObj->amount_in_text = $request->amount_text;
              $secondaryDocObj->due_date = date('m-d-Y', strtotime($request->due_date));
              // $secondaryDocObj->first_name = $request->first_name;
              $secondaryDocObj->signature = $request->signature;
              $secondaryDocObj->comment = $request->comment;
              $secondaryDocObj->service_labour_furnished = $request->service_labour_furnished; */
            $secondaryDocObj->status = 0;

            $data['status'] = 'draft';
        }
        $secondaryDocObj->save(); 

        $secondary_doc_id = $secondaryDocObj->id;
        $result2 = \App\Models\SecondaryDocumentFields::select()->where('secondary_document_id', $secondary_doc_id)->delete();

            foreach ($fieldsData as $key => $value) {
            if ($values[$key] == NULL) {
                $result2 = \App\Models\SecondaryDocumentFields::create([
                            'secondary_document_id' => $secondary_doc_id,
                            'notice_id' => $request->notice_id,
                            'notice_field_id' => $value,
                            'value' => '',
                ]);
            } else {
                $result2 = \App\Models\SecondaryDocumentFields::create([
                            'secondary_document_id' => $secondary_doc_id,
                            'notice_id' => $request->notice_id,
                            'notice_field_id' => $value,
                            'value' => nl2br($values[$key]),
                ]);
            }
        }

        if ($request->contracted_by != "") {
            /* check contracted by is exist or not */
            $contracted_by_check = \App\Models\Contact::where('company_name', $request->contracted_by)->first();
            if (empty($contracted_by_check)) {
                $contact = \App\Models\Contact::create([
                            'customer_id' => $customer_id,
                            'company_name' => $request->contracted_by,
                ]);
            }
        }
        if ($request->project_owner != "") {
            /* check project_owner is exist or not */
            $project_owner_check = \App\Models\Contact::where('company_name', $request->project_owner)->first();
            if (empty($project_owner_check)) {
                $contact = \App\Models\Contact::create([
                            'customer_id' => $customer_id,
                            'company_name' => $request->project_owner,
                ]);
            }
        }
        $data['parent_work_orders'] = \App\Models\WorkOrder::leftjoin('notice_templates', 'notice_templates.notice_id', '=', 'work_orders.notice_id')
                ->where('customer_id', '=', $secondaryDocObj->customer_id)
                ->where('notice_templates.is_parent_work_order', '=', 1)
                ->select('work_orders.id')
                ->get();
        if ($request->secondary_doc_attachment) {
            $attachment = explode(',|,', trim($request->secondary_doc_attachment[0],'|,'));
            //  if (isset($attachment) && $attachment != null) {
            foreach ($attachment AS $k => $v) {
              $v = trim($v,'|,');
              if($v!='|'){//dd($request->all());
                if (isset($request->remove_doc) && !empty($request->remove_doc) && $request->remove_doc[0] != '') {
                    $remove_doc = explode(',', $request->remove_doc[0]);
//dd($remove_doc);
                    $each_attachment = explode(',', $v);
                    if ($each_attachment[0] != '' && !empty($each_attachment[4])) {
                        $true = !in_array($each_attachment[4], $remove_doc);
                    } else {
                        $true = '';
                    }//dd($each_attachment,$remove_doc,$true);
                    //dd($v);
                    if (isset($true) && $true != '') {
                        if ($v != '') {

                            $secondary_attachment_obj = new \App\Models\SecondaryDocumentAttachments();
                            $secondary_attachment_obj->secondary_document_id = $request->edit_secondary_doc_id;
                            $secondary_attachment_obj->type = $each_attachment[0];
                            $secondary_attachment_obj->title = $each_attachment[1];
                            $secondary_attachment_obj->file_name = $each_attachment[2];
                            $secondary_attachment_obj->original_file_name = $each_attachment[3];
                            $secondary_attachment_obj->save();
                        }
                    }
                } else {
                    if ($v != '') {
                        $each_attachment = explode(',', $v);
                        $secondary_attachment_obj = new \App\Models\SecondaryDocumentAttachments();
                        $secondary_attachment_obj->secondary_document_id = $request->edit_secondary_doc_id;
                        $secondary_attachment_obj->type = $each_attachment[0];
                        $secondary_attachment_obj->title = $each_attachment[1];
                        $secondary_attachment_obj->file_name = $each_attachment[2];
                        $secondary_attachment_obj->original_file_name = $each_attachment[3];
                        $secondary_attachment_obj->save();
                    }
                }
            }
             }
        }

        //If parent work order is selected document of parent work order will inserted
        if (isset($request->work_order_no) && $request->work_order_no != NULL) {
            if ($prev_work_order_no != $request->work_order_no) {
                if (isset($request->remove_doc) && !empty($request->remove_doc) && $request->remove_doc[0] != '') {
                    $previous_attachments = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $request->work_order_no)->get();

                    if (isset($previous_attachments) && count($previous_attachments) > 0) {
                        foreach ($previous_attachments AS $k1 => $a1) {

                           /* $each_remove_doc = explode(',', $request->remove_doc[0]);
                            foreach ($each_remove_doc As $k => $each) {
                                $explode_v = explode('_', $each);
                                $count = count($explode_v);
                                 if($count>1){
                                $each_remove_doc[$k] = $explode_v[$count - 3] . '_' . $explode_v[$count - 2] . '_' . $explode_v[$count - 1];
                              }
                            }

                            $true = !in_array($a1->file_name, $each_remove_doc);*/
                            $true = "true";

                            if (isset($true) && $true != '') {

                                $secondary_attachment_obj = new \App\Models\SecondaryDocumentAttachments();
                                $secondary_attachment_obj->secondary_document_id = $request->edit_secondary_doc_id;
                                $secondary_attachment_obj->type = $a1->type;
                                $secondary_attachment_obj->title = $a1->title;
                               if(!empty($a1->file_name)){
                                                        $secondary_attachment_obj->file_name = $SecondaryDocument->id . '_' . $a1->file_name;
                                                        }else{
                                                         $secondary_attachment_obj->file_name = "";
                                                        }
                                                        if(!empty($a1->original_file_name)){
                                                        $secondary_attachment_obj->original_file_name = $a1->original_file_name;
                                                        }else{
                                                           $secondary_attachment_obj->original_file_name = "";
                                                        }
                                if (Auth::user()->hasRole('customer')) {
                                    $secondary_attachment_obj->visibility = 0;
                                } else {
                                    $secondary_attachment_obj->visibility = $a1->visibility;
                                }
                                $secondary_attachment_obj->save();
                                if ($v->file_name != "" && file_exists(public_path() . '/attachment/secondary_document/parent_document' . $v->file_name) && !empty($a1->file_name)) {

                                    rename(public_path() . '/attachment/secondary_document/parent_document/' . $a1->file_name, public_path() . '/attachment/secondary_document/parent_document/' . $secondary_attachment_obj->file_name);
                                    \File::copy(public_path() . '/attachment/secondary_document/parent_document/' . $secondary_attachment_obj->file_name, public_path() . '/attachment/secondary_document/' . $secondary_attachment_obj->file_name);
                                    unlink(public_path() . '/attachment/secondary_document/parent_document/' . $secondary_attachment_obj->file_name);
                                }
                            }
                        }
                    }
                } else {
                    $remove_doc = explode(',', $request->remove_doc[0]);
                    $previous_attachments = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $request->work_order_no)->get();

                    if (isset($previous_attachments) && count($previous_attachments) > 0) {
                        foreach ($previous_attachments AS $k1 => $a1) {
                            $secondary_attachment_obj = new \App\Models\SecondaryDocumentAttachments();
                            $secondary_attachment_obj->secondary_document_id = $request->edit_secondary_doc_id;
                            $secondary_attachment_obj->type = $a1->type;
                            $secondary_attachment_obj->title = $a1->title;
                            if(!empty($a1->file_name)){
                                                        $secondary_attachment_obj->file_name = $request->edit_secondary_doc_id . '_' . $a1->file_name;
                                                        }else{
                                                         $secondary_attachment_obj->file_name = " ";
                                                        }
                                                        if(!empty($a1->original_file_name)){
                                                        $secondary_attachment_obj->original_file_name = $a1->original_file_name;
                                                        }else{
                                                           $secondary_attachment_obj->original_file_name = "";
                                                        }
                            if (Auth::user()->hasRole('customer')) {
                                $secondary_attachment_obj->visibility = 0;
                            } else {
                                $secondary_attachment_obj->visibility = $a1->visibility;
                            }
                            $secondary_attachment_obj->save();

                            if ($a1->file_name != "" && file_exists(public_path() . '/attachment/secondary_document/parent_document' . $a1->file_name) && !empty($a1->file_name)) {
                                rename(public_path() . '/attachment/secondary_document/parent_document/' . $a1->file_name, public_path() . '/attachment/secondary_document/parent_document/' . $secondary_attachment_obj->file_name);
                                \File::copy(public_path() . '/attachment/secondary_document/parent_document/' . $secondary_attachment_obj->file_name, public_path() . '/attachment/secondary_document/' . $secondary_attachment_obj->file_name);
                                unlink(public_path() . '/attachment/secondary_document/parent_document/' . $secondary_attachment_obj->file_name);
                            }
                        }
                    }
                }
            }
        }

        $secondary_document = SecondaryDocument::find($request->edit_secondary_doc_id);

        $data['secondary_document'] = $secondary_document;

        $data['cities'] = \App\Models\City::select()->get();
        $data['states'] = \App\Models\State::select()->orderBy('name')->get();
        if (isset($request->work_order_no) && $request->work_order_no != NULL) {
            if ($prev_work_order_no != $request->work_order_no) {
                $parent_work_order_recipients = \App\Models\Recipient::select()->where('work_order_id', $request->work_order_no)->get();
                if (isset($parent_work_order_recipients) && count($parent_work_order_recipients) > 0) {
                    foreach ($parent_work_order_recipients As $k => $each_parent_recipient)
                        $SecondaryDocumentRecipient = new SecondaryDocumentRecipient();
                    $SecondaryDocumentRecipient->secondary_document_id = $request->edit_secondary_doc_id;
                    $SecondaryDocumentRecipient->category_id = $each_parent_recipient->category_id;
                    $SecondaryDocumentRecipient->name = $each_parent_recipient->name;
                    $SecondaryDocumentRecipient->contact = $each_parent_recipient->contact;
                    $SecondaryDocumentRecipient->address = $each_parent_recipient->address;
                    $SecondaryDocumentRecipient->city_id = $each_parent_recipient->city_id;
                    $SecondaryDocumentRecipient->state_id = $each_parent_recipient->state_id;
                    $SecondaryDocumentRecipient->zip = $each_parent_recipient->zip;
                    $SecondaryDocumentRecipient->email = $each_parent_recipient->email;
                    $SecondaryDocumentRecipient->attn = $each_parent_recipient->attn;
                    $SecondaryDocumentRecipient->save();
                }
            }
        }
        $data['recipients'] = \App\Models\SecondaryDocumentRecipient::select()->where('secondary_document_id', $request->edit_secondary_doc_id)->get();
        $data['count'] = \App\Models\SecondaryDocumentRecipient::select(DB::raw('COUNT(*) AS total'), 'categories.name')->groupBy('secondary_document_recipients.category_id')->join('categories', 'categories.id', '=', 'secondary_document_recipients.category_id')->where('secondary_document_id', $request->edit_secondary_doc_id)->get();


        $data['countries'] = \App\Models\Country::select()->get();
        $data['categories'] = \App\Models\Category::select()->get();
        $data['project_roles'] = \App\Models\ProjectRoles::select()->orderBy('name', 'ASC')->get();
        $data['partial_final_type'] = $secondaryDocObj->type;
        $data['notes'] = \App\Models\SecondaryDocumentNotes::leftjoin('users', 'users.id', '=', 'secondary_document_notes.user_id')->where('secondary_document_id', $request->edit_secondary_doc_id)
                        ->orderBy('secondary_document_notes.id', 'DESC')->get();
        $data['corrections'] = \App\Models\SecondaryDocumentCorrections::leftjoin('users', 'users.id', '=', 'secondary_document_corrections.user_id')->where('secondary_document_id', $request->edit_secondary_doc_id)
                        ->orderBy('secondary_document_corrections.id', 'DESC')->get();

        $data['secondary_doc_id'] = $request->edit_secondary_doc_id;
        $data['tab'] = 'recipients';
//        $data['names'] = \App\Models\ContactDetail::select()
//                ->where('contact_details.type', '=', 0)
//                ->where('contacts.customer_id', '=', Auth::user()->customer->id)
//                ->Join('contacts', 'contacts.id', '=', 'contact_details.contact_id')
//                ->get();
        $data['names'] = \App\Models\Contact::select()
                //->where('contact_details.type', '=', 0)
                ->where('contacts.customer_id', '=', $secondaryDocObj->customer_id)
                // ->leftjoin('contact_details', 'contact_details.contact_id', '=', 'contacts.id')
                ->orderBy('contacts.id', 'DESC')
                ->get();
        $data['attachment'] = \App\Models\SecondaryDocumentAttachments::where('secondary_document_id', '=', $request->edit_secondary_doc_id)
                ->get();
        $admin_email = SecondaryDocument::getAdminEmail();

        /*   if (Auth::user()->hasRole('account-manager') || Auth::user()->hasRole('admin')) {


          if (Auth::user()->hasRole('account-manager')) {
          $account_manager_email = SecondaryDocument::getOtherAccManagerEmailExcludingLoggedIn(Auth::user()->id);
          $customer_email = SecondaryDocument::getCustomerEmail($secondary_document->customer_id);
          if (isset($account_manager_email) && !empty($account_manager_email)) {
          $data['note_emails'] = [
          $customer_email->email,
          $admin_email->email,
          ];
          foreach ($account_manager_email AS $each_emails) {
          array_push($data['note_emails'], $each_emails->email);
          }
          } else {
          $data['note_emails'] = [
          $customer_email->email,
          $admin_email->email,
          ];
          }
          } else {
          $account_manager_email = SecondaryDocument::getAllAccountManagerEmail();
          $customer_email = SecondaryDocument::getCustomerEmail($secondary_document->customer_id);
          if (isset($account_manager_email) && !empty($account_manager_email)) {
          $data['note_emails'] = [
          $customer_email->email
          ];
          foreach ($account_manager_email AS $each_emails) {
          array_push($data['note_emails'], $each_emails->email);
          }
          } else {
          $data['note_emails'] = [
          $customer_email->email
          ];
          }
          }
          } else {

          $account_manager_email = SecondaryDocument::getAccountManagerEmail($secondary_document->customer_id);
          if (isset($account_manager_email) && !empty($account_manager_email)) {
          $data['note_emails'] = [
          $admin_email->email,
          $account_manager_email->email
          ];
          } else {
          $data['note_emails'] = [
          $admin_email->email
          ];
          }
          } */
        $data['note_emails'] = get_user_notice_email($secondary_document->customer_id);
        $secondary_document = SecondaryDocument::find($request->edit_secondary_doc_id);

      //dd($secondary_document);
        //$secondary_document->due_date = str_replace('-', '/', $secondary_document->due_date);

        //$secondary_document->due_date = date('d/m/Y',strtotime($secondary_document->due_date));
        $data['secondary_document'] = $secondary_document;
        $data['secondary_doc_id'] = $secondary_document->id;
        $result = \App\Models\SecondaryDocument::select('state_id')->join('notice_fields', 'notice_fields.notice_id', '=', 'secondary_documents.notice_id')->where('secondary_documents.id', $secondary_document->id)->limit(1)->get()->toArray();

        $notice = \App\Models\Notice::select('name', 'id')->where('id',$secondary_document->notice_id)->get()->toArray();
        $data['notice'] = $notice[0];
        $data['notice_id'] = $secondary_document->notice_id;
        $data['notice_fields']=[];
        if(count($result)>0){
          $data['notice_fields'] = \App\Models\NoticeField::select()->join('secondary_document_fields', 'secondary_document_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('secondary_document_fields.notice_id', $secondary_document->notice_id)->where('secondary_document_fields.secondary_document_id',$secondary_document->id)->orderBy('sort_order', 'asc')->get();  
        }
      //  dd('jio');
        Session::flash('success', 'Your work order #'.$secondary_document->id.' successfully saved ');
        
        if($request->edit_flag==1){
            return redirect('customer/secondary-document');
        }else{
            return redirect("home");
        }
        
/*
        if ($request->submit == 'continue') {
            return view('customer.secondary_document.edit', $data);
        } else {

            return redirect('customer/secondary-document');
        }*/
    }

    public function update_recipients(Request $request, $secondary_doc_id) {
      
        if ($request->submit == "recepient") {
            // $secondary_document_id = \App\Models\SecondaryDocument::orderBy('created_at', 'desc')->first();

            $request->request->set('secondary_document_id', $secondary_doc_id);
            $request->request->set('country_id', $request->country);
            $SecondaryDocumentRecipient = \App\Models\SecondaryDocumentRecipient::create(
                            $request->except(['_token']));
            if ($SecondaryDocumentRecipient) {
                Session::flash('success', 'New Record Successfully');
            } else {
                Session::flash('success', 'Problem in Record saving');
            }
            $data['cities'] = \App\Models\City::select()->get();
            $data['states'] = \App\Models\State::select()->orderBy('name')->get();
            $data['categories'] = \App\Models\Category::select()->get();
            $data['project_roles'] = \App\Models\ProjectRoles::select()->orderBy('name', 'ASC')->get();
            $data['recipients'] = \App\Models\SecondaryDocumentRecipient::select()->where('secondary_document_id', $secondary_doc_id)->get();
            $data['count'] = \App\Models\SecondaryDocumentRecipient::select(DB::raw('COUNT(*) AS total'), 'categories.name')->groupBy('secondary_document_recipients.category_id')->join('categories', 'categories.id', '=', 'secondary_document_recipients.category_id')->where('secondary_document_id', $secondary_doc_id)->get();
            $secondary_document = SecondaryDocument::find($secondary_doc_id);
            $data['secondary_document'] = $secondary_document;
            $data['countries'] = \App\Models\Country::select()->get();
            $data['secondary_doc_id'] = $secondary_doc_id;
            $data['partial_final_type'] = $request->partial_final_type;
            $data['correction'] = \App\Models\SecondaryDocumentNotes::leftjoin('users', 'users.id', '=', 'secondary_document_notes.user_id')->where('secondary_document_notes.customer_id', '=', Auth::user()->customer->id)->where('secondary_document_id', $secondary_doc_id)->get();

            $data['parent_work_orders'] = \App\Models\WorkOrder::leftjoin('notice_templates', 'notice_templates.notice_id', '=', 'work_orders.notice_id')
                    ->where('customer_id', '=', $secondary_document->customer_id)
                    ->where('notice_templates.is_parent_work_order', '=', 1)
                    ->select('work_orders.id')
                    ->get();
//            $data['names'] = \App\Models\ContactDetail::select()
//                    ->where('contact_details.type', '=', 0)
//                    ->where('contacts.customer_id', '=', Auth::user()->customer->id)
//                    ->Join('contacts', 'contacts.id', '=', 'contact_details.contact_id')
//                    ->get();
            $data['names'] = \App\Models\Contact::select()
                    //  ->where('contact_details.type', '=', 0)
                    ->where('contacts.customer_id', '=', $secondary_document->customer_id)
                    // ->Join('contact_details', 'contact_details.contact_id', '=', 'contacts.id')
                    ->orderBy('contacts.id', 'DESC')
                    ->get();
            $data['tab'] = 'recipients';
            $admin_email = SecondaryDocument::getAdminEmail();

            /*   if (Auth::user()->hasRole('account-manager') || Auth::user()->hasRole('admin')) {


              if (Auth::user()->hasRole('account-manager')) {
              $account_manager_email = SecondaryDocument::getOtherAccManagerEmailExcludingLoggedIn(Auth::user()->id);
              $customer_email = SecondaryDocument::getCustomerEmail($secondary_document->customer_id);
              if (isset($account_manager_email) && !empty($account_manager_email)) {
              $data['note_emails'] = [
              $customer_email->email,
              $admin_email->email,
              ];
              foreach ($account_manager_email AS $each_emails) {
              array_push($data['note_emails'], $each_emails->email);
              }
              } else {
              $data['note_emails'] = [
              $customer_email->email,
              $admin_email->email,
              ];
              }
              } else {
              $account_manager_email = SecondaryDocument::getAllAccountManagerEmail();
              $customer_email = SecondaryDocument::getCustomerEmail($secondary_document->customer_id);
              if (isset($account_manager_email) && !empty($account_manager_email)) {
              $data['note_emails'] = [
              $customer_email->email
              ];
              foreach ($account_manager_email AS $each_emails) {
              array_push($data['note_emails'], $each_emails->email);
              }
              } else {
              $data['note_emails'] = [
              $customer_email->email
              ];
              }
              }
              } else {

              $account_manager_email = SecondaryDocument::getAccountManagerEmail($secondary_document->customer_id);
              if (isset($account_manager_email) && !empty($account_manager_email)) {
              $data['note_emails'] = [
              $admin_email->email,
              $account_manager_email->email
              ];
              } else {
              $data['note_emails'] = [
              $admin_email->email
              ];
              }
              } */
            $data['note_emails'] = get_user_notice_email($secondary_document->customer_id);
            $data['notes'] = \App\Models\SecondaryDocumentNotes::leftjoin('users', 'users.id', '=', 'secondary_document_notes.user_id')->where('secondary_document_id', $secondary_doc_id)
                            ->orderBy('secondary_document_notes.id', 'DESC')->get();
            $data['corrections'] = \App\Models\SecondaryDocumentCorrections::leftjoin('users', 'users.id', '=', 'secondary_document_corrections.user_id')->where('secondary_document_id', $secondary_doc_id)
                            ->orderBy('secondary_document_corrections.id', 'DESC')->get();

            return view('customer.secondary_document.edit', $data);
        } else if ($request->submit == "submit") {
            
        }
    }


    public function printSecondaryDocument($secondary_doc_id, $print_status = 1) {

        $secondaryDocument = SecondaryDocument::find($secondary_doc_id);
        $notice_id = $secondaryDocument->notice_id;

        //Secondary Document ID
        $secondary_order_no = 'Work Order No: ' . $secondaryDocument->id;

        //Parent Work Order
        $parent_work_order = ($secondaryDocument->work_order_no) ? 'Parent Work Order No:' . $secondaryDocument->work_order_no : '';
        //Amount in dollar
        $amount = '$' . number_format($secondaryDocument->amount);

        //Amount Text
        $amount_text = $secondaryDocument->amount_in_text;

        //Project ddress Details
        $project_address = strtoupper($secondaryDocument->address);
        // $state_name = \App\Models\State::find($secondaryDocument->state_id);
        $country_name = \App\Models\City::find($secondaryDocument->country_id);
        $project_country = "";
        //  $project_state = $state_name->name;
        if ($country_name != "") {
            $project_country = $country_name->county;
        }
      
        $project_address = $project_address;
        $project_name = $secondaryDocument->project_name;
        //Noc ,folio no and bond no
        $document = \App\Models\SecondaryDocumentAttachments::where('secondary_document_id', '=', $secondaryDocument->id)->get();

        $bond = '';
        $noc = '';
        $folio = '';
        foreach ($document As $key => $each_document) {
            if ($each_document->type == 1 || $each_document->type == 2 || $each_document->type == 10) {

                if ($each_document->type == 1) {
                    $bond = 'Bond:' . $each_document->title;
                }
                //Noc
                if ($each_document->type == 2) {
                    $noc = 'NOC BK:' . $each_document->title;
                }

                //Folio
                if ($each_document->type == 10) {
                    $folio = 'Folio No: ' . $each_document->title;
                }
            }
        }
        //Company Details
        $customer_company_name = \App\Models\Customer::find($secondaryDocument->customer_id);
        $company_name = $customer_company_name->company_name;
        $company_address = $customer_company_name->mailing_address;
        $company_state_name = \App\Models\State::find($customer_company_name->mailing_state_id);
        $company_state = !empty($company_state_name) ? $company_state_name->name : '';
        $company_city_name = \App\Models\City::find($customer_company_name->mailing_city_id);
        $company_city = !empty($company_city_name) ? $company_city_name->name : '';
        $company_zip = $customer_company_name->mailing_zip;


        //Printing Date
        $printing_date = date('F d,Y');

        //Printing Day
        $printing_day = date('d'); //October 2018
        //Printing Month
        $printing_month = date('F Y');

        $recipients = SecondaryDocumentRecipient::where('secondary_document_id', '=', $secondary_doc_id)
                ->get();
        $logo = public_path() . '/img/logo.png';

        
        $notice_state = \App\Models\Notice::where('id', '=', $notice_id)
                ->first();
        $notice_state = $notice_state->state_id;
        $template_content = \App\Models\NoticeTemplate::where('state_id', '=', $notice_state)
                ->where('notice_id', '=', $notice_id)
                ->first();
        $content = $template_content->content;

    

         $secondaryDocuments = \App\Models\SecondaryDocumentFields::select('notices.master_notice_id as master_notice_id','notices.name as notice_name', 'notices.is_rescind as rescind_notice', '.is_rescind as rescind_work_order', 'notices.type', 'secondary_documents.work_order_no', 'secondary_documents.created_at','secondary_documents.file_name','secondary_documents.id as secondary_doc_id', 'usr.name as customer_name', 'secondary_documents.customer_id', 'usr.name as account_manager_name', 'secondary_documents.notice_id as notice_id','customers.company_name as company_name','secondary_documents.status','secondary_documents.created_at as created_date', 'secondary_documents.account_manager_id', 'secondary_document_fields.secondary_document_id','secondary_document_fields.notice_id', DB::raw("(GROUP_CONCAT(notice_fields.name SEPARATOR '||')) as `field_names`"), DB::raw("(GROUP_CONCAT(value SEPARATOR '||')) as `field_values`"))
                ->rightjoin('notice_fields', 'notice_fields.id', '=', 'secondary_document_fields.notice_field_id')
                ->leftjoin('secondary_documents', 'secondary_documents.id', '=', 'secondary_document_fields.secondary_document_id')
                ->leftjoin('notices', 'notices.id', '=', 'secondary_documents.notice_id')
                ->leftjoin('customers', 'customers.id', '=', 'secondary_documents.customer_id')
                ->leftjoin('users as usr', 'usr.id', '=', 'secondary_documents.account_manager_id')->groupBy('secondary_document_fields.secondary_document_id', 'secondary_document_fields.notice_id')->where('secondary_documents.id', '=', $secondary_doc_id);

            $secondaryDocuments = $secondaryDocuments->get()->toArray();
          $result = [];
          if (isset($secondaryDocuments) && !empty($secondaryDocuments)) {
            foreach ($secondaryDocuments as $fields_data) {
                $field_names = explode('||', $fields_data['field_names']);
                $field_values = explode('||', $fields_data['field_values']);
                $field_names_values = array_combine($field_names, $field_values);
                $field_names_values['default'] = '';
                $result[] = array_merge($fields_data, $field_names_values);
            }
          }
        
        if(count($result)>0){
            $data = $result[0];
        }

        $arr_variable = [];
        $arr_replace = [];
        $amount_paid_date = '';
        $contracted_by = '';
        $legal_description = '';
        $bond_claim_number = '';
        $your_job_reference_no = '';
        $title = '';
        $recorded_day = '';
        $recorded_month = '';
        $project_name = '';
        $office_director_title = '';
        $office_director = '';
        $service_labour_furnished = '';
        $project_owner = '';
        $owners = '';
        //service_labour_furnished
        if (isset($content) && $content != NULL) {
            if (isset($data) && !empty($data)) {
                foreach ($data AS $key => $value) {
                     array_push($arr_variable, '{{' . $key . '}}');
                    if($key == 'date_request' || $key == 'enter_into_agreement' ||
                            $key == 'last_date_of_labor_service_furnished') {
                        if (isset($value) && $value != NULL && $value != '') {
                            $value = date('M d,Y', strtotime($value));
//                          $date = \DateTime::createFromFormat("m-d-Y", $value->value);
//                           
//                            $date = $date->format('m-d-Y'); 
//                            $value->value = $date;
                        }
                    }else if($key == 'due_date'){
                        if (isset($value) && $value != NULL && $value != '') {
                          $date = \DateTime::createFromFormat("m-d-Y", $value);
                            $amount_paid_date = $date->format('F d,Y');
                        }
                    }else if ($key == 'workorder_id') {
                      if (isset($work_order_type) && $work_order_type == 'cyo') {
                        $value = 'CYO Work Order No:' . $value;
                      }else{
                        $value = 'Work Order No:' . $value;
                      }
                    } else if ($key == 'parent_work_order') {
                        if (isset($value) && !empty($value)) {
                            $value = 'Parent Work Order:' . $value;
                        }
                    } else if ($key == 'project_name') {
                     // dd($value);
                        if (isset($value) && !empty($value)) {
                            $project_name = 'Project:' . $value;
                        } else {
                            $project_name = '';
                        }

                    }else if ($key == 'service_labour_furnished') {
                     // dd($value);
                        if (isset($value) && !empty($value)) {
                            $service_labour_furnished = $value;
                        } else {
                            $service_labour_furnished = '';
                        }

                    }else if ($key == 'signature') {
                     // dd($value);
                        if (isset($value) && !empty($value)) {
                            $office_director = $value;
                        } else {
                            $office_director = '';
                        }

                    }else if ($key == 'title') {
                     // dd($value);
                        if (isset($value) && !empty($value)) {
                            $office_director_title = $value;
                        } else {
                            $office_director_title = '';
                        }

                    } else if ($key == 'total_amount_satisfied') {
                      if (isset($value) && !empty($value)) {
                                    if(is_numeric($value)){
                                        $value = number_format($value,2);
                                      }
                                } else {
                                    $value = '00.00';
                                }
                    }else if ($key == 'completed_date') {
                                if (isset($value) && !empty($value)) {
                                $value = date('d M Y', strtotime($value));
                                $value = explode(' ', $value);
                                $value = $value[0].' day of '.$value[1].', '.$value[2]; 
                                }else{
                                    $value = '___ day of _________, 20__';
                                }
                    } else if ($key == 'estimate_value_of_labor') {
                       if (isset($value) && !empty($value)) {
                                    if(is_numeric($value)){
                                        $value = number_format($value,2);
                                      }
                                } else {
                                    $value = '00.00';
                                }
                    }else if ($key == 'total_value') {
                      if (isset($value) && !empty($value)) {
                                    if(is_numeric($value)){
                                        $value = number_format($value,2);
                                      }
                                } else {
                                    $value = '00.00';
                                }
                    }else if ($key == 'unpaid_amount') {
                      if (isset($value) && !empty($value)) {
                                    if(is_numeric($value)){
                                        $value = number_format($value,2);
                                      }
                                } else {
                                    $value = '00.00';
                                }
                    }else if ($key == 'contract_price_or_estimated_value_of_labor') {
                       if (isset($value) && !empty($value)) {
                                    if(is_numeric($value)){
                                        $value = number_format($value,2);
                                      }
                                } else {
                                    $value = '00.00';
                                }
                    }else if ($key == 'estimate_of_amount_owed') {
                       if (isset($value) && !empty($value)) {
                                    if(is_numeric($value)){
                                        $value = number_format($value,2);
                                      }
                                } else {
                                    $value = '00.00';
                                }
                    }else if ($key == 'final_payment') {
                        if (isset($value) && !empty($value)) {
                                    if(is_numeric($value)){
                                        $value = number_format($value,2);
                                      }
                                } else {
                                    $value = '00.00';
                                }
                    }else if ($key == 'amount_due') {
                       if (isset($value) && !empty($value)) {
                                    if(is_numeric($value)){
                                        $value = number_format($value,2);
                                      }
                                } else {
                                    $value = '00.00';
                                }
                    }else if ($key == 'project_address') {
                                if (isset($value) && !empty($value)) {
                                    $value = str_replace("<p>", "", $value);
                                    $value = str_replace("<br />", "", $value);
                                    $value = str_replace("**", "<br />**", $value);
                                    $value = strtoupper($value);
                                    $project_address = 'Project:' . $value;
                                } else {
                                    $project_address = '';
                                }
                   }else if ($key == 'county' || $key == 'notary_county' || $key == 'recorded_county') {
                        if (isset($value) && !empty($value)) {
                             if(is_numeric($value)){
                            $county = \App\Models\City::select('county')->find($value);

                            $value = $county->county;
                        }else{
                            $value= $value;
                        }
                        }
                    } else if ($key == 'notary_state' || $key == 'recorded_state') {
                        if (isset($value) && !empty($value)) {
                            $notary_state_name = \App\Models\State::find($value);
                            $value = $notary_state_name->name;
                        }
                    } else if ($key == 'owner_received_type_mailing' || $key == 'contractor_received_type_mailing' ||
                            $key == 'subcontractor_received_type_mailing' || $key == 'sub_subcontractor_received_type_mailing') {
                        if (isset($value) && !empty($value)) {
                            $value = 'by ' . $value;
                        }
                    } else if ($key == 'authorise_agent') {
                        if (isset($value) && !empty($value)) {
//                            $signature = \App\Models\CustomerAgent::find($value);
//                            $value = $signature->first_name . ' ' . $signature->last_name;
//                            $title = $signature->title;
                            $value = $value;
                        }else{
                            $value = "";
                        }
                    } else if ($key == 'recorded_date') {
                        if (isset($value) && !empty($value)) {
                            $recorded_day = date('d', strtotime($value)); //October 2018
                            //Recorded Month
                            $recorded_month = date('F Y', strtotime($value));
                        }
                    } else if ($key == 'contracted_by') {
                        if (isset($value) && !empty($value)) {
                            $contracted_by = $value;
                        } else {
                            $contracted_by = '';
                        }
                    } else if ($key == 'legal_description') {
                        if (isset($value) && !empty($value)) {
                            $legal_description = $value;
                        } else {
                            $legal_description = '';
                        }
                    } else if ($key == 'bond_claim_number') {
                        if (isset($value) && !empty($value)) {
                            $bond_claim_number = $value;
                        } else {
                            $bond_claim_number = '';
                        }
                        // array_push($arr_replace, $value);
                    }else if ($key == 'amout_value_of_payoff') {
                        if (isset($value) && !empty($value)) {
                            if(is_numeric($value)){
                           $amount = '$' . number_format($value);
                            }
                        } else {
                            $amount = '';
                        }
                        // array_push($arr_replace, $value);
                    }else if ($key == 'your_job_reference_no') {
                        if (isset($value) && !empty($value)) {
                            $your_job_reference_no = $value;
                        } else {
                            $your_job_reference_no = '';
                        }
                        // array_push($arr_replace, $value);
                    } else if ($key == 'collection_for_attorneys_fees') {
                        if (isset($value) && !empty($value)) {
                            $value = 'these charges along with interest accruing at the rate of ' . $value . ' monthly and attorneys fees, ';
                        } else {
                            $value = '';
                        }
                    } /*else if ($key == 'job_start_date' || $key == 'last_date_on_the_job' || $key == 'owner_received_nto_date' ||
                            $key == 'contractor_received_nto_date' || $key == 'subcontractor_received_nto_date' ||
                            $key == 'sub_subcontractor_received_nto_date') {
                        if (isset($value) && !empty($value)) {
                            $value = date('F d,Y', strtotime($value));
                        }
                    } */else if ($key == 'job_start_date' || $key == 'last_date_on_the_job') {
                        if (isset($value) && !empty($value)) {
                            $value = date('F d,Y', strtotime($value));
                        }
                    } else if ($key == "check_clear") {

                        if ($value) {
                            $value = "This form of release is expressly conditioned upon the undersigned’s receiving the actual amount referenced above and payment has been properly endorsed and funds paid/cleared by bank. ";
                        } else {
                            $value = "";
                        }
                    } else if ($key == "notary_seal") {
                        if ($value) {
                           $value = "<table style='width: 100%;'>
                                        <tbody>
                                           <tr>
                                                <td colspan='2'>
                                                    <table style='width: 50%;'>
                                                        <tbody>
                                                                <tr>
                                                                        <td style='vertical-align: top;width: 35%;padding: 1px 0; font-size:10px; line-height: 10px;'>State:</td>
                                                                        <td style='width: 50%;padding: 1px 0; font-size:10px; line-height: 10px;'>_________________________________________________________</td>
                                                                </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                             </tr>
                                            <tr>
                                                <td colspan='2'>
                                                    <table style='width: 50%;'>
                                                        <tbody>
                                                                <tr>
                                                                        <td style='vertical-align: top;width: 35%;padding: 1px 0; font-size:10px; line-height: 10px;'>County:</td>
                                                                        <td style='width: 50%;padding: 1px 0; font-size:10px; line-height: 10px;'>_________________________________________________________</td>
                                                                </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                             </tr>
                                            
                                             
                                           <tr>
                                                <td colspan='2'>
                                                    <table style='width: 100%;'>
                                                        <tbody>
                                                                <tr>
                                                                        <td style='vertical-align: top;width: 35%;padding: 2px 0;'> Sworn to (or affirmed) and subscribed before me this _______________(Month) ______(Date) _______________(Year)
                                                   BY_______________personally know_______________Produce Identification __________________________________(Type of Identification)      
                                              </td>
                                                                      
                                                                </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td colspan='2'>
                                                    <table style='width: 50%;'>
                                                        <tbody>
                                                                <tr>
                                                                        <td style='vertical-align: top;width: 35%;padding: 2px 0;'>Notary Signature:</td>
                                                                        <td style='width: 50%;padding: 2px 0;'>_________________________________________________________</td>
                                                                </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                             </tr>
                                              <tr>
                                                <td colspan='2'>
                                                    <table style='width: 50%;'>
                                                        <tbody>
                                                                <tr>
                                                                        <td style='vertical-align: top;width: 35%;padding: 2px 0;'>Notary Stamp</td>
                                                                        <td style='width: 50%;padding: 2px 0;'></td>
                                                                </tr>
                                                                       <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                                <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                                <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                                <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                                <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                                <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                                <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                                <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                                <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                                <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                                <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                                <tr><td 'padding: 1px 0;  line-height: 10px;'></td></tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                             </tr>
                                    </tbody></table>";
            
                            // dd($value);
                        } else {
                            $value = "";
                        }
                    }
                    array_push($arr_replace, $value);
                }
            }
        }
        //Your customer
        $your_customer = $secondaryDocument->customer;
        //$legal_description = $secondaryDocument->legal_description;
            if (isset($recipients) && count($recipients) > 0) {
                foreach ($recipients AS $each_recipient) {
                    $category_name = \App\Models\Category::find($each_recipient->category_id);
                    if ($category_name->name == 'Owner' || $category_name->name == 'owner') {
                        $owner = $each_recipient->name;
                    }
                }
            }
            $year = date('Y', strtotime($secondaryDocument->created_at));
            $notary_seal = "";
        //if($secondaryDocument->notary_seal) {
                
       //parent work order legal description
        $legal_description = '';
        $owners = '';
        if(isset($secondaryDocument->work_order_no) && !empty($secondaryDocument->work_order_no)){
         $WorkOrderFields = \App\Models\WorkOrderFields::select('work_orders.id', 'work_orders.created_at', 'work_orders.user_id', 'work_orders.customer_id','work_orders.is_rescind as rescind_work_order',  'work_orders.notice_id as notice_id', 'work_orders.status', 'work_orders.user_id', 'work_orders.account_manager_id', 'work_order_fields.workorder_id', 'work_order_fields.notice_id', DB::raw("(GROUP_CONCAT(name SEPARATOR '||')) as `field_names`"), DB::raw("(GROUP_CONCAT(value SEPARATOR '||')) as `field_values`"))->join('notice_fields', 'notice_fields.id', '=', 'work_order_fields.notice_field_id')
                            ->join('work_orders', 'work_orders.id', '=', 'work_order_fields.workorder_id')->groupBy('work_order_fields.workorder_id', 'work_order_fields.notice_id')
                            ->where('work_orders.id', $secondaryDocument->work_order_no)
                            ->get()->toArray();
        $result = [];
        if (isset($WorkOrderFields) && !empty($WorkOrderFields)) {
            foreach ($WorkOrderFields as $fields_data) {
                $field_names = explode('||', $fields_data['field_names']);
                $field_values = explode('||', $fields_data['field_values']);
                $field_names_values = array_combine($field_names, $field_values);
                $field_names_values['default'] = '';
                $result[] = array_merge($fields_data, $field_names_values);
            }
        }
        $data = $result[0];
      
       if (isset($data) && !empty($data)) {
                foreach ($data AS $key => $value) {
                  if ($key == 'legal_description') {
                        if (isset($value) && !empty($value)) {
                            $legal_description = $value;
                        } else {
                            $legal_description = '';
                        }
                    }
                }
              }

        //add project owner of parent work order
        $recipients = \App\Models\Recipient::leftjoin('stamps_label', 'stamps_label.recipient_id', '=', 'recipients.id')
                    ->where('work_order_id', '=', $secondaryDocument->work_order_no)
                    ->whereIn('category_id',['1','22'])->select('name','category_id')->get()->toArray();
        if (isset($recipients) && count($recipients) > 0) {
            foreach ($recipients AS $k => $each_recipient) {
                    if(count($recipients)==1){
                    $owners = $each_recipient['name'];
                    }else if((count($recipients)-1)==$k){
                    $owners = $owners . ' and ' .$each_recipient['name']; 
                    }else if($k == (count($recipients)-2)){
                    $owners = $owners .' '.$each_recipient['name']; 
                    }else{
                    $owners = $owners .$each_recipient['name'].' , '; 
                    }
                  }                    
            }
            if(!in_array("{{legal_description}}", $arr_variable)){
              array_push($arr_variable,"{{legal_description}}");
              array_push($arr_replace,$legal_description);
            }else{
              $index = array_search('{{legal_description}}',$arr_variable);
              if($arr_replace[$index]==""){
               $arr_replace[$index] = $legal_description;
              }
            }

            if(!in_array("{{project_owner}}", $arr_variable)){
              array_push($arr_variable,"{{project_owner}}");
              array_push($arr_replace,$owners);
            }else{
              $index = array_search('{{project_owner}}',$arr_variable);
              if($arr_replace[$index]==""){
               $arr_replace[$index] = $owners;
              }
            }
        }
          
        $static_array_var = ['{{secondary_order_no}}', '{{parent_work_order}}', '{{project_name}}', '{{amount_dollar}}', '{{your_customer}}',
                '{{project_address}}', '{{noc}}', '{{bond}}', '{{folio}}', '{{amount_text}','{{printing_date}}', '{{company_name}}',
                '{{company_address}}', '{{company_city}}', '{{company_state}}', '{{company_zip}}', '{{printing_day}}', '{{printing_month}}', '{{office_director}}','{{year}}', '{{title}}', '{{country}}', '{{amount_date}}', '{{project_owner}}', '{{legal_description}}', '{{notary_seal}}','{{owners}}'];
        $static_array_replace = [$secondary_order_no, $parent_work_order, $project_name, $amount, $your_customer, $project_address, $noc, $bond, $folio, $amount_text, $printing_date, $company_name, $company_address, $company_city, $company_state, $company_zip,$printing_day, $printing_month, $office_director, $year, $office_director_title, $project_country, $amount_paid_date, $project_owner, $legal_description, $notary_seal, $owners];

        $variable_arr = array_merge($arr_variable, $static_array_var);
        $replace_arr = array_merge($arr_replace, $static_array_replace);
       $content = str_replace($variable_arr, $replace_arr, $content);

         /*$static_array_var = ['{{work_order_no}}', '{{parent_work_order}}', '{{project_name}}', '{{amount_dollar}}', '{{your_customer}}',
                '{{project_address}}', '{{noc}}', '{{bond}}', '{{folio}}', '{{amount_text}}', '{{printing_date}}', '{{company_name}}',
                '{{company_address}}', '{{company_city}}', '{{company_state}}', '{{company_zip}}', '{{printing_day}}', '{{printing_month}}', '{{office_director}}',
                '{{title}}', '{{country}}', '{{amount_date}}', '{{project_owner}}', '{{notary_seal}}',
                '{{printing_date}}', '{{owners}}', '{{owners_with_label_property_owner}}','{{owners_with_label_to_property_owner}}', '{{owner_name}}', '{{your_job_reference_no}}','{{folio}}', '{{bond}}', '{{noc}}', '{{permit}}', '{{company_name}}', '{{company_address}}', '{{company_city_state_zip}}','{{logo}}', '{{printing_day}}', '{{printing_month}}',
            '{{state}}','{{legal_description}}'];
        $static_array_replace = [$printing_date, $owners, $owners,$owners, $owner_name, $your_job_reference_no,
          $folio, $bond, $noc, $permit, $company_name, $company_address, $company_city_state_zip,
          $logo, $printing_day, $printing_month, $contractor_name,
          $sub_contractor_name, $sub_sub_contractor_name, $title, $year, $recorded_day, $recorded_month,
          $noticestate, $contracted_by, $legal_description, $bond_claim_number, $project_name, $company_city, $company_state,$rescind_work_order,$amendment,$is_bond_of_claim,$general_contractor,$project_address,$recipient_owners,$sub_contractor,$contracted_by_recipient,$surety_recipient];*/
        // }
//$subcontractor_html, '{{subcontractor_html}}'
              
     
        $pdf = PDF::loadHTML($content);
        //  dd($content);

        if ($secondaryDocument->file_name == NULL) {
            $file_name = $secondary_doc_id . '_' . uniqid() . '.pdf';
            //update file name
            $secondaryDocument->file_name = $file_name;
            $secondaryDocument->save();
        } else {
            $file_name = $secondaryDocument->file_name;
            //$edit_file_name = $secondary_doc_id . '_' . uniqid() . '.pdf';
            $secondaryDocument->file_name = $file_name;
            $secondaryDocument->save();
        }

        if (!file_exists(public_path() . '/pdf/secondary_document')) {
            mkdir(public_path() . '/pdf/secondary_document', 0777, true);
        }

        if (!file_exists(public_path() . '/pdf/secondary_document/' . $file_name)) {
            $pdf->save(public_path() . '/pdf/secondary_document/' . $file_name);
        } else {
            unlink(public_path('pdf/secondary_document/' . $file_name));

            $pdf->save(public_path() . '/pdf/secondary_document/' . $file_name);
        }
        if ($print_status == 1) {
            return $pdf->stream($file_name);
        } else {
            return $pdf->download(public_path() . '/pdf/secondary_document/' . $file_name);
        }
    }

    /**
     * Function is used to print secondary document
     * @param type $secondary_doc_id
     * @return type
     */
    public function printCreatedSecondaryDocument($secondary_doc_id) {
        $data = [];
        $secondaryDocument = SecondaryDocument::find($secondary_doc_id);
        $file_name = $secondaryDocument->file_name;


        if (file_exists(public_path() . '/pdf/secondary_document/' . $file_name)) {

            $mypdf = asset('/') . 'pdf/secondary_document/' . $file_name;
            echo "<script>window.open('$mypdf');</script>";
        }

//        return view('customer.secondary_document.list', $data);
    }

    /**
     * Function is used to delete attachment
     */
    public function deleteAttachment(Request $request) {

        if ($request->file != "" && file_exists(public_path('attachment/secondary_document/' . $request->file))) {
            unlink(public_path('attachment/secondary_document/' . $request->file));
        }

        return response()->json(['success' => 1]);
    }

    public function deleteParentAttachment(Request $request) {

        if ($request->file != "" && file_exists(public_path('attachment/secondary_document/parent_document/' . $request->file))) {
            unlink(public_path('attachment/secondary_document/parent_document/' . $request->file));
        }

        return response()->json(['success' => 1]);
    }

    public function deleteEditDocAttachment(Request $request) {
        $secondaryAttachment = \DB::table('secondary_document_attachments')->find($request->id);
        $secondary_doc_id = $secondaryAttachment->secondary_document_id;
        $secondaryAttachment = \DB::table('secondary_document_attachments')
                ->where('id', '=', $request->id);
        if ($request->file != "" && file_exists(public_path('attachment/secondary_document/' . $request->file))) {
            unlink(public_path('attachment/secondary_document/' . $request->file));
        }
        $result = $secondaryAttachment->delete();

//          return redirect::to('customer/secondary-document/edit/'.$secondary_doc_id);
        return response()->json(['success' => $result]);
    }

    function updateStatus($id) {
        $result = \App\Models\SecondaryDocument::find($id);
        $result->status = 0;
        $result->save();
        Session::flash('success', 'Work Order Saved Successfully');
        return redirect('customer/secondary-document');
    }

    /**
     * Function is used to when click on edit recipient get data on recipient page
     */
    public function getRecipientData(Request $request) {
        $recipient_id = $request->id;
        $secondary_doc_recipient = \App\Models\SecondaryDocumentRecipient::with('secondaryDocUspsAddress')->find($recipient_id);
        $secondary_doc = SecondaryDocument::find($secondary_doc_recipient->secondary_document_id);
        $customer_id = $secondary_doc->customer_id;
        $names = \App\Models\Contact::select()
                //  ->where('contact_details.type', '=', 0)
                ->where('contacts.customer_id', '=', $customer_id)
                // ->Join('contact_details', 'contact_details.contact_id', '=', 'contacts.id')
                ->orderBy('contacts.id', 'DESC')
                ->get();

        $recipient = SecondaryDocumentRecipient::find($recipient_id);
        if (isset($recipient) && count($recipient) > 0) {

            $city_name = \App\Models\City::where('id', '=', $recipient->city_id)
                    ->select('name')
                    ->first();
            $recipient->city_name = $city_name->name;

            if(!empty($recipient->secondaryDocUspsAddress)) {
                $recipient->address2 = $recipient->secondaryDocUspsAddress->address2;
                $recipient->address3 = $recipient->secondaryDocUspsAddress->address3;
            }
        }
        return \Response::json(['success' => true, 'recipient' => $recipient, 'contacts' => $names
        ]);
    }

    public function autoCompleteCity(Request $request) {
        $query = $request->get('term', '');

        $names = \App\Models\City::where('name', 'LIKE', $query . '%')->groupBy('name')
                ->get(['id', \DB::raw('name as name')]);
        //->get(['id', \DB::raw('concat(name,", ",county) as name')]);

        $data = array();
        foreach ($names as $name) {
            $data[] = array('value' => $name->name, 'id' => $name->id);
        }
        if (count($data)) {
            return $data;
        } else {
            $data[] = array('value' => 'No Result Found', 'id' => '');
            return $data;
        }
    }

    public function autoCompleteCounty(Request $request, $notice_id = 0) {
        $query = $request->get('term', '');

        $get_state = \App\Models\Notice::find($notice_id);
        if (empty($get_state)) {
            $names = \App\Models\City::where('county', 'LIKE', '%' . $query . '%')->groupBy('county')->orderBy('id')
                    ->get(['id', \DB::raw('county as name')]);
        } else {
            $names = \App\Models\City::where('county', 'LIKE', '%' . $query . '%')->where('state_id', $get_state->state_id)->groupBy('county')->orderBy('id')
                    ->get(['id', \DB::raw('county as name')]);
        }
        $data = array();
        foreach ($names as $name) {
            $data[] = array('value' => $name->name, 'id' => $name->id);
        }
        if (count($data)) {
            return $data;
        } else {
            $data[] = array('value' => 'No Result Found', 'id' => '');
            return $data;
        }
    }

    public function autoCompleteCityZip(Request $request) {
        $query = $request->get('term', '');
        $city = $request->get('city', '');
        if ($city != "") {
            $names = \App\Models\City::where('zip_code', 'LIKE','%'. $query . '%')->where('name', 'LIKE','%'.$city . '%')
                    ->get(['id', \DB::raw('zip_code')]);
        } else {
            $names = \App\Models\City::where('zip_code', 'LIKE','%'.$query . '%')
                    ->get(['id', \DB::raw('zip_code')]);
        }
        //->get(['id', \DB::raw('concat(name,", ",county) as name')]);

        $data = array();
        foreach ($names as $name) {
            $data[] = array('value' => $name->zip_code, 'id' => $name->id);
        }
        if (count($data)) {
            return $data;
        } else {
            $data[] = array('value' => 'No Result Found', 'id' => '');
            return $data;
        }
    }

    /**
     * Function is used to store correction
     */
    public function storeCorrection(Request $request) {
        $rules = ['correction' => 'required'
        ];
        if (Auth::user()->hasRole('customer')) {
            $rules = ['email' => 'required', 'correction' => 'required',];
        }
        $validator = \Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }
        $secondaryDoc = SecondaryDocument::find($request->secondary_doc_id);
        $result = \App\Models\SecondaryDocumentCorrections::create([
                    'secondary_document_id' => $request->secondary_doc_id,
                    'customer_id' => $secondaryDoc->customer_id,
                    'correction' => $request->correction,
                    'email' => ($request->email) ? $request->email : NULL,
                    'user_id' => Auth::user()->id,
        ]);
        $corrections = \App\Models\SecondaryDocumentCorrections::leftjoin('users', 'users.id', '=', 'secondary_document_corrections.user_id')->where('customer_id', '=', $secondaryDoc->customer_id)
                ->select('secondary_document_corrections.correction', 'secondary_document_corrections.email', 'users.name')
                ->where('secondary_document_corrections.id', '=', $result->id)
                ->first();
        /*  $senderName = \App\Models\Customer::leftjoin('users', 'users.id', '=', 'customers.user_id')->select('users.name')->where('customers.id', '=',$secondaryDoc->customer_id)->first(); */
        // $correction->doc_id = $request->doc_id;
        if ($request->email != NULL) {
            Mail::to($request->email)->send(new \App\Mail\SecondaryDocumentCorrection($result));
        }
        $data['secondary_doc_id'] = $secondaryDoc->id;
        if (Auth::user()->hasRole('account-manager')) {

            $customers = \App\Models\Customer::leftjoin('users', 'users.id', '=', 'customers.user_id')
                    ->select('customers.id', 'users.name')
                    ->where('account_manager_id', '=', Auth::user()->id)
                    ->get();
            $data['customers'] = $customers;
        }
        if (Auth::user()->hasRole('admin')) {

            $customers = \App\Models\Customer::leftjoin('users', 'users.id', '=', 'customers.user_id')
                    ->select('customers.id', 'users.name')
                    ->get();
            $data['customers'] = $customers;
        }
        if (Auth::user()->hasRole('account-manager') || Auth::user()->hasRole('admin')) {

            $data['names'] = \App\Models\Contact::select('contacts.id', 'contacts.company_name')
                    //->where('contact_details.type', '=', 0)
                    ->where('contacts.customer_id', '=', $secondaryDoc->customer_id)
                    // ->Join('contact_details', 'contact_details.contact_id', '=', 'contacts.id')
                    ->orderBy('contacts.id', 'DESC')
                    ->get();
            $data['officers_directors'] = \App\Models\CustomerAgent::where('customer_id', '=', $secondaryDoc->customer_id)
                    ->get();
        } else {
            $data['names'] = \App\Models\Contact::select('contacts.id', 'contacts.company_name')
                    //->where('contact_details.type', '=', 0)
                    ->where('contacts.customer_id', '=', Auth::user()->customer->id)
                    // ->Join('contact_details', 'contact_details.contact_id', '=', 'contacts.id')
                    ->orderBy('contacts.id', 'DESC')
                    ->get();
            $data['officers_directors'] = \App\Models\CustomerAgent::where('customer_id', '=', Auth::user()->customer->id)
                    ->get();
        }
        if ($result) {
            return response()->json(['success' => 'Correction submitted successfully', 'corrections' => $corrections]);
        }
    }
    public function cityList(Request $request){
      $query = $request->name;
     // $cityList = \App\Models\City::select('id','name')->take(10)->get();
      $names = \App\Models\City::select('id','name')->where('name', 'LIKE', $query . '%')->groupBy('name')->take(100)->get();
      $data = array();
    foreach ($names as $key => $value) {
       # code...
      $data[] = array("id"=>$value->id, "text"=>$value->name);
     }    
      
  
     $data = json_encode($data);
      return $data;
    }

     /*get notices from stateId */
    public function getNotices($encStateId = null)
    {
      $stateId = base64_decode(($encStateId));
      
      $notices_arr = \App\Models\Notice::where('state_id',$stateId)
                                       ->where('type',3)
                                       ->where('status',1)
                                       ->get()->toArray();

      if($notices_arr)
      {
        $response['status'] = 'SUCCESS';        
      }
      else
      {
        $response['status'] = 'FAILURE';
      }

      $response['notices_arr'] = $notices_arr;

      return response()->json($response);
    }

      public function autoCompleteCustomer(Request $request) {
        $query = $request->get('term', '');
        if(Auth::user()->hasRole('account-manager') || Auth::user()->hasRole('admin')) {

                $names = \App\Models\Contact::select('contacts.id', 'contacts.company_name')
                        //->where('contact_details.type', '=', 0)
                        ->where('contacts.company_name', 'LIKE', '%' . $query . '%')
                        // ->Join('contact_details', 'contact_details.contact_id', '=', 'contacts.id')
                        ->get();
            } else {
                $names = \App\Models\Contact::select('contacts.id', 'contacts.company_name')
                        //->where('contact_details.type', '=', 0)
                        ->where('contacts.company_name','LIKE', '%' . $query . '%')
                        // ->Join('contact_details', 'contact_details.contact_id', '=', 'contacts.id')
                        ->get();
            }
            $data = array();
                foreach ($names as $name) {
                    $data[] = array('value' => $name->company_name, 'id' => $name->company_name);
                }
            if (count($data))
            return $data;
            else
            return ['value' => 'No Result Found', 'id' => ''];
    }
}
