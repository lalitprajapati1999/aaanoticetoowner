<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Models\City;
use App\Models\State;
use App\Models\WorkOrder;
use App\Models\Notice;
use Illuminate\Support\Facades\Validator;
use Alert;
use Illuminate\Support\Facades\Input;
use DB;
use Illuminate\Support\Facades\Session;
use App\Models\WorkOrderAttachment;
use Illuminate\Support\Facades\Mail;
use App\Mail\WorkOrderNote;
use Illuminate\Support\Facades\Redirect;
use App\User;

class WorkOrderController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $data['hard_notices'] = Notice::select()->where('type', 1)->get();
        $data['states'] = \App\Models\State::select()->where('status', 1)->orderBy('name', 'asc')->get();
        $data['soft_notices'] = Notice::select()->where('type', 2)->get();
        $data['customer_details'] = Auth::user()->customer;
        $data['categories'] = \App\Models\Category::select()->orderBy('name', 'ASC')->get();
        $data['project_roles'] = \App\Models\ProjectRoles::select()->orderBy('name', 'ASC')->get();
        return view('customer.workorder.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request) {
        /*
          $rules = [
          'notice_id1' => 'exists:notices,id',

          // 'notice_id2' => 'exists:notices,id',
          'project_type1' => 'exists:states,id',

          //'project_type2' => 'exists:states,id',
          ];
          $messages = [
          'notice_id1.exists' => 'Selected notice type invalid',
          'project_type1.exists' => "Selected project type is invalid",
          ];
          $validator = Validator::make(Input::all(), $rules, $messages); */

        /* if ($validator->fails())
          {
          return redirect()->back()->withErrors($validator)->withInput();
          } */
        $data['customer_details'] = Auth::user()->customer;

        $data['categories'] = \App\Models\Category::select()->orderBy('name', 'ASC')->get();
        $data['project_roles'] = \App\Models\ProjectRoles::select()->orderBy('name', 'ASC')->get();
        $data['parent_work_order_id'] = \App\Models\WorkOrder::select('id')->get();
        $data['cities'] = \App\Models\City::select()->get();
        $data['countries'] = \App\Models\Country::select()->get();
        $data['states'] = \App\Models\State::select()->orderBy('name', 'asc')->get();
        $data['officers_directors'] = \App\Models\CustomerAgent::where('customer_id', '=', Auth::user()->customer->id)
                ->get();
        $data['projectTypes'] = \App\Models\ProjectType::select()->get();
        $data['id'] = 0;
        $data['count'] = 0;
        //$data['recipients'] = \App\Models\Recipient::select()->get();
        $data['names'] = \App\Models\Contact::select('contacts.id', 'contacts.company_name')
                //->where('contact_details.type', '=', 0)
                ->where('contacts.customer_id', '=', Auth::user()->customer->id)
                //->Join('contact_details', 'contact_details.contact_id', '=', 'contacts.id')
                ->orderBy('contacts.company_name', 'ASC')
                ->get();
        if (is_numeric($request->input('notice_id1'))) {
            $notice = \App\Models\Notice::select('name', 'id','master_notice_id')->where('id', $request->notice_id1)->get()->toArray();
            $data['notice'] = $notice[0];
            $data['notice_id'] = $request->input('notice_id1');
            $data['notice_fields'] = \App\Models\NoticeField::select()->where('state_id', $request->project_type1)->where('notice_id', $request->notice_id1)->orderBy('sort_order', 'asc')->get();
            $notice = \App\Models\Notice::select('name')->where('id', $request->notice_id1)->get()->toArray();

            $data['tab'] = 'project';
            $data['notes'] = \App\Models\WorkOrderNotes::where('customer_id', '=', Auth::user()->customer->id)
                    ->get();

            return view('customer.workorder.create', $data);
        } else if (is_numeric($request->input('notice_id2'))) {
            $notice = \App\Models\Notice::select('id', 'name','master_notice_id')->where('id', $request->notice_id2)->get()->toArray();
            $data['notice'] = $notice[0];
            $data['notice_id'] = $request->input('notice_id2');
            $data['notice_fields_section1'] = \App\Models\NoticeField::select()->where('state_id', $request->project_type2)->where('notice_id', $request->notice_id2)->where('section', 1)->orderBy('sort_order', 'asc')->get();
            $data['notice_fields_section2'] = \App\Models\NoticeField::select()->where('state_id', $request->project_type2)->where('notice_id', $request->notice_id2)->where('section', 2)->orderBy('sort_order', 'asc')->get();
            //dd($data['notice_fields_section1']);
            $notice = \App\Models\Notice::select('name')->where('id', $request->notice_id2)->get()->toArray();
            $data['tab'] = 'project';

            return view('customer.workorder.create_soft_notice', $data);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
               

        $data = [];

        $data['customer_details'] = Auth::user()->customer;


        //  $fields = array_merge($request->except(['_token', 'continue', 'type', 'notice_id', 'work_order_attachment', 'file_name', 'doc_input', 'document_type', 'remove_doc', 'recipient_county_id', 'visibility', 'recipient_notary_county_id', 'recipient_project_county_id', 'recipient_recorded_county_id']));
        $fields = array_merge($request->except(['_token', 'continue', 'type', 'notice_id', 'edit_soft_notices_attachment', 'file_name', 'doc_input', 'hidden_city_val', 'duplicate', 'remove_doc', 'parent_work_order_previous', 'recipient_county_id', 'contracted_by_exist', 'document_type', 'work_order_attachment', 'visibility', 'recipient_notary_county_id', 'recipient_project_county_id', 'recipient_recorded_county_id','work_order_attachment_id']));

        
        $fieldsData = [];
        $values = [];
        $i = 0;
        foreach ($fields as $key => $value) {
            $field = explode("_", $key);
            $fieldsData[$i] = $field['0'];
            $values[$i] = $value;
            $i++;
        }
        //  dd($request->all());
        $admin_email = \App\User::leftjoin('role_users', 'role_users.user_id', '=', 'users.id')
                ->where('role_users.role_id', '=', 2)
                ->select('users.email')
                ->first();
        $account_manager_email = \App\Models\Customer::leftjoin('users', 'users.id', '=', 'customers.account_manager_id')
                ->select('users.email')
                ->find(Auth::user()->customer->id);

        /* if (isset($account_manager_email) && !empty($account_manager_email)) {
          $data['note_emails'] = [
          $admin_email->email,
          $account_manager_email->email
          ];
          } else {
          $data['note_emails'] = [
          $admin_email->email
          ];

          } */
        $data['note_emails'] = get_user_notice_email(Auth::user()->customer->id);
        $customerId = Auth::user()->customer->id;
        $account_manager_id = Auth::user()->customer->account_manager_id;
        $userId = Auth::user()->id;
        if ($request->continue == 'continue') {
            $result1 = WorkOrder::create([
                        'notice_id' => $request->notice_id,
                        'customer_id' => $customerId,
                        'order_no' => str_random(5),
                        'user_id' => $userId,
                        'status' => '1',
                        'account_manager_id' => $account_manager_id,
            ]);
            $data['status'] = 'request';
            Session::flash('success', 'Your work order # ' . $result1->id . ' project information has been successfully completed. Please add recipients.');
        } else {
            $result1 = WorkOrder::create([
                        'notice_id' => $request->notice_id,
                        'customer_id' => $customerId,
                        'order_no' => str_random(5),
                        'user_id' => $userId,
                        'status' => '0',
                        'account_manager_id' => $account_manager_id,
            ]);
            $data['status'] = 'draft';
            Session::flash('success', 'Your work order # ' . $result1->id . ' has been saved as a draft, You must complete Work Order and submit at a later time.<a href="' . url('customer/work-order/view/' . $result1->id) . '">Click here to print.</a>');
        }
//dd($request->all());
        if($request->work_order_attachment_id[0]!=null){
        $work_order_attachment_id = $request->work_order_attachment_id;
        $work_order_attachment_id = explode(',', $work_order_attachment_id[0]);
     //  dd($work_order_attachment_id);
        foreach ($work_order_attachment_id as $key => $value) {
           $work_order_attachment_obj =  \App\Models\WorkOrderAttachment::find($value);
           if(!empty($work_order_attachment_obj)){
           $work_order_attachment_obj->work_order_id = $result1->id;
           $work_order_attachment_obj->save();
            }
        }
    }


        if ($result1) {
//dd($request->work_order_attachment[0]);
            $attachment = explode(',|,', trim($request->work_order_attachment[0],'|,'));
        //dd($attachment,$request->remove_doc);
            if (isset($attachment) && $attachment != null) {
                foreach ($attachment AS $k => $v) {
                    $v = trim($v,'|,');
                    if($v!= '|'){
                    if (isset($request->remove_doc) && !empty($request->remove_doc) && $request->remove_doc[0] != '') {
                        $remove_doc = explode(',', $request->remove_doc[0]);
                        if ($v != '') {
                            $each_attachment = explode(',', $v);
                            if (!in_array($each_attachment[4], $remove_doc)) {
                                if ($v != '') {
                                    $work_order_attachment_obj = new \App\Models\WorkOrderAttachment();
                                    $work_order_attachment_obj->work_order_id = $result1->id;
                                    $work_order_attachment_obj->type = $each_attachment[0];
                                    $work_order_attachment_obj->title = $each_attachment[1];
                                    $work_order_attachment_obj->file_name = $each_attachment[2];
                                    $work_order_attachment_obj->original_file_name = $each_attachment[3];
                                    $work_order_attachment_obj->save();
                                }
                            }
                        }
                    } else {
                        if ($v != '') {
                            $each_attachment = explode(',', $v);
                            $work_order_attachment_obj = new \App\Models\WorkOrderAttachment();
                            $work_order_attachment_obj->work_order_id = $result1->id;
                            $work_order_attachment_obj->type = $each_attachment[0];
                            $work_order_attachment_obj->title = $each_attachment[1];
                            $work_order_attachment_obj->file_name = $each_attachment[2];
                            $work_order_attachment_obj->original_file_name = $each_attachment[3];
                            $work_order_attachment_obj->save();
                        }
                    }
                }
                }
            }

            foreach ($fields as $key => $value) {
                $field = explode("_", $key);

                if (isset($field[3])) {
                    $check_field = $field[1] . '_' . $field[2] . '_' . $field[3];
                    if ($check_field == 'parent_work_order') {
                        $parent_work_order = $value;
                        $data['parent_work_order'] = $parent_work_order;
                        $parent_work_order_previous = $request->parent_work_order_previous;
                        //dd($parent_work_order,$parent_work_order_previous );
                        if ($parent_work_order_previous != $parent_work_order) {
                            //If parent work order is selected document of parent work order will inserted
                            if ($parent_work_order != '') {
                                if (isset($request->remove_doc) && !empty($request->remove_doc) && $request->remove_doc[0] != '') {
                                    $previous_attachments = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $parent_work_order)->get();

                                    if (isset($previous_attachments) && count($previous_attachments) > 0) {
                                        foreach ($previous_attachments AS $k1 => $a1) {

                                           /* $each_remove_doc = explode(',', $request->remove_doc[0]);
                                            foreach ($each_remove_doc As $k => $each) {
                                                $explode_v = explode('_', $each);
                                                $count = count($explode_v);
                                                 if($count>1){
                                                $each_remove_doc[$k] = $explode_v[$count - 3] . '_' . $explode_v[$count - 2] . '_' . $explode_v[$count - 1];
                                            }
                                            }

                                            $true = !in_array($a1->file_name, $each_remove_doc);*/
                                            $true = "true";

                                            if (isset($true) && $true != '') {

                                                $work_order_attachment_obj = new \App\Models\WorkOrderAttachment();
                                                $work_order_attachment_obj->work_order_id = $result1->id;
                                                $work_order_attachment_obj->type = $a1->type;
                                                $work_order_attachment_obj->title = $a1->title;
                                                if(!empty($a1->file_name)){
                                                        $work_order_attachment_obj->file_name = $result1->id . '_' . $a1->file_name;
                                                        }else{
                                                         $work_order_attachment_obj->file_name = "";
                                                        }
                                                        if(!empty($a1->original_file_name)){
                                                        $work_order_attachment_obj->original_file_name = $a1->original_file_name;
                                                        }else{
                                                           $work_order_attachment_obj->original_file_name = "";
                                                        }
                                                if (Auth::user()->hasRole('account-manager')) {
                                                    $work_order_attachment_obj->visibility = $a1->visibility;
                                                } else {
                                                    $work_order_attachment_obj->visibility = 0;
                                                }
                                                $work_order_attachment_obj->save();
                                                if (file_exists(public_path() . '/attachment/work_order_document/parent_document/' . $a1->file_name)) {
                                                      if ($a1->file_name != "") {
                                                    rename(public_path() . '/attachment/work_order_document/parent_document/' . $a1->file_name, public_path() . '/attachment/work_order_document/parent_document/' . $work_order_attachment_obj->file_name);
                                                    \File::copy(public_path() . '/attachment/work_order_document/parent_document/' . $work_order_attachment_obj->file_name, public_path() . '/attachment/work_order_document/' . $work_order_attachment_obj->file_name);
                                                    unlink(public_path() . '/attachment/work_order_document/parent_document/' . $work_order_attachment_obj->file_name);
                                                 }
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    $remove_doc = explode(',', $request->remove_doc[0]);
                                    $previous_attachments = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $parent_work_order)->get();

                                    if (isset($previous_attachments) && count($previous_attachments) > 0) {
                                        foreach ($previous_attachments AS $k1 => $a1) {
                                            $work_order_attachment_obj = new \App\Models\WorkOrderAttachment();
                                            $work_order_attachment_obj->work_order_id = $result1->id;
                                            $work_order_attachment_obj->type = $a1->type;
                                            $work_order_attachment_obj->title = $a1->title;
                                            if(!empty($a1->file_name)){
                                                        $work_order_attachment_obj->file_name = $result1->id . '_' . $a1->file_name;
                                                        }else{
                                                         $work_order_attachment_obj->file_name = " ";
                                                        }
                                                        if(!empty($a1->original_file_name)){
                                                        $work_order_attachment_obj->original_file_name = $a1->original_file_name;
                                                        }else{
                                                           $work_order_attachment_obj->original_file_name = "";
                                                        }
                                            if (Auth::user()->hasRole('account-manager')) {
                                                $work_order_attachment_obj->visibility = $a1->visibility;
                                            } else {
                                                $work_order_attachment_obj->visibility = 0;
                                            }
                                            $work_order_attachment_obj->save();

                                            if (file_exists(public_path() . '/attachment/work_order_document/parent_document/' . $a1->file_name)) {
                                                if ($a1->file_name != "") {
                                                    rename(public_path() . '/attachment/work_order_document/parent_document/' . $a1->file_name, public_path() . '/attachment/work_order_document/parent_document/' . $work_order_attachment_obj->file_name);
                                                    \File::copy(public_path() . '/attachment/work_order_document/parent_document/' . $work_order_attachment_obj->file_name, public_path() . '/attachment/work_order_document/' . $work_order_attachment_obj->file_name);
                                                    unlink(public_path() . '/attachment/work_order_document/parent_document/' . $work_order_attachment_obj->file_name);
                                                }
                                            }
                                        }
                                    }
                                }
                                $data['parent_work_order'] = $parent_work_order;
                                $first_category = \App\Models\Category::where('name', 'Contracted By')->first();
                                /* save parent work order recipient  */
                                if (isset($request->contracted_by_exist)) {
                                    $parent_work_order_recipients = \App\Models\Recipient::select()->where('work_order_id', $parent_work_order)->where('category_id', '!=', $first_category->id)->get();
                                } else {
                                    $parent_work_order_recipients = \App\Models\Recipient::select()->where('work_order_id', $parent_work_order)->get();
                                }
                                \App\Models\Recipient::where( 'work_order_id', $result1->id)->where('parent_work_order','!=',NULL)->delete(); 
                                if (isset($parent_work_order_recipients) && count($parent_work_order_recipients) > 0) {
                                    foreach ($parent_work_order_recipients As $k => $each_parent_recipient) {
                                        $save_recipients = \App\Models\Recipient::create([
                                                    'work_order_id' => $result1->id,
                                                    'category_id' => $each_parent_recipient->category_id,
                                                    'name' => $each_parent_recipient->name,
                                                    'contact' => $each_parent_recipient->contact,
                                                    'address' => $each_parent_recipient->address,
                                                    'city_id' => $each_parent_recipient->city_id,
                                                    'attn' => $each_parent_recipient->attn,
                                                    'state_id' => $each_parent_recipient->state_id,
                                                    'zip' => $each_parent_recipient->zip,
                                                    'email' => $each_parent_recipient->email,
                                                    'attn' => $each_parent_recipient->attn,
                                                    'parent_work_order'=> $parent_work_order,
                                                ])->id;

                                        //Check if the recipient address is usps verified, if yes copy the usps address record too for the new recipient
                                        $usps_address = \App\Models\UspsAddress::where(['recipient_id' => $each_parent_recipient->id])->get()->toArray();
                                        if (!empty($usps_address[0])) {
                                            $usps_address[0]['id'] = '';
                                            $usps_address[0]['recipient_id'] = $save_recipients;
                                            \App\Models\UspsAddress::insert($usps_address[0]);
                                        }
                                    }
                                }
                            }
                        }
                    }
                   

                }

                //Insert contracted by is first recipient
                if (isset($field[2])) {
                    $check_contracted_by_field = $field[1] . '_' . $field[2];
                    if ($check_contracted_by_field == 'contracted_by') {
                        $first_category = \App\Models\Category::where('name', 'Contracted By')->first();
                        $contracted_by = $value;
//dd( $first_category->id);
                        if ($contracted_by != "") {
                            /* check contracted by is exist or not */
                            $contracted_by_check = \App\Models\Contact::where('company_name', $contracted_by)->first();
                            if (empty($contracted_by_check)) {
                                $contact = \App\Models\Contact::create([
                                            'customer_id' => Auth::user()->customer->id,
                                            'company_name' => $contracted_by,
                                ]);
                            }
                        }
                        $contactdetails = \App\Models\Contact::where('company_name', '=', $contracted_by)
                                ->where('customer_id', '=', $customerId)
                                ->first();

                        if (isset($contactdetails) && $contactdetails != null) {
                            /* check contract details exit or not */
                            $check_contract_recipient_exist = \App\Models\Recipient::where(['category_id' => $first_category->id, 'work_order_id' => $result1->id])->first();
                            /* if not empty delete previouse record */
                            if (!empty($check_contract_recipient_exist)) {
                                if ($check_contract_recipient_exist->name != $contracted_by) {
                                    $contract_recipient_delete = \App\Models\Recipient::where(['category_id' => $first_category->id, 'work_order_id' => $result1->id])->where('id', '!=', $request->recipient_id)->delete();
                                    $check_contract_recipient_exist = [];
                                }
                            }
                            if (empty($check_contract_recipient_exist)) {
                                $Recipient = \App\Models\Recipient::create([
                                            'work_order_id' => $result1->id,
                                            'category_id' => $first_category->id,
                                            'name' => $contactdetails->company_name,
                                            'contact' => $contactdetails->phone,
                                            'address' => $contactdetails->mailing_address,
                                            'city_id' => $contactdetails->city_id,
                                            'state_id' => $contactdetails->state_id,
                                            'zip' => $contactdetails->zip,
                                            'email' => $contactdetails->email,
                                            'attn' => $contactdetails->attn
                                ]);
                            }
                        }
                    }
                }
            }
        }


        //dd($fieldsData,$values);
        $work_order_id = $result1->id;
        foreach ($fieldsData as $key => $value) {
            if ($values[$key] == NULL && $value>0) {
             //dd($value);
                $result2 = \App\Models\WorkOrderFields::create([
                            'workorder_id' => $work_order_id,
                            'notice_id' => $request->notice_id,
                            'notice_field_id' => $value,
                            'value' => '',
                ]);
            } else if($value>0) {
                          //dd($values[$key]);

                $result2 = \App\Models\WorkOrderFields::create([
                            'workorder_id' => $work_order_id,
                            'notice_id' => $request->notice_id,
                            'notice_field_id' => $value,
                            'value' => nl2br($values[$key]),
                ]);
            }

        }
      //  dd(strpos('33_last_date_on_the_job', 'last_date_on_the_job'));
     $arr=array_filter($fields, function($k) {
           return strpos($k, 'last_date_on_the_job')>0;
     }, ARRAY_FILTER_USE_KEY);
      $last_date_on_the_job_val=array_pop($arr);

     $arr_parent_work_order=array_filter($fields, function($k) {
        return strpos($k, 'parent_work_order')>0;
     }, ARRAY_FILTER_USE_KEY); 

     $parent_work_order_id=array_pop($arr_parent_work_order);

      $parent_notice_id = WorkOrder::select('notice_id')->where('id', $parent_work_order_id)->first();
          
      if(!empty($parent_notice_id) && !empty($last_date_on_the_job_val)){
          $notice_field_id = \App\Models\NoticeField::select('id')->where('notice_id', $parent_notice_id->notice_id)
          ->where('name','last_date_on_the_job')->first();
        if(!empty($notice_field_id)){
            if($last_date_on_the_job_val==NULL){
                $wrk_order_fields=\App\Models\WorkOrderFields::where('workorder_id',$parent_work_order)
                ->where('notice_id',$parent_notice_id->notice_id)
                ->where('notice_field_id',$notice_field_id->id)
                ->update(['value'=>'']);
            }else{
                $wrk_order_fields=\App\Models\WorkOrderFields::where('workorder_id',$parent_work_order)
                ->where('notice_id',$parent_notice_id->notice_id)
                ->where('notice_field_id',$notice_field_id->id)
                ->update(['value'=>$last_date_on_the_job_val]);
            }
        }
      }



        $notice_id = $result1->notice_id;
        $id = $result1->id;
        $notice = \App\Models\Notice::select('name', 'id', 'type','master_notice_id')->where('id', $notice_id)->get()->toArray();
        $data['notice'] = $notice[0];
        $data['cities'] = \App\Models\City::select()->get();
        $data['countries'] = \App\Models\Country::select()->get();
        $data['states'] = \App\Models\State::select()->orderBy('name', 'asc')->get();
        $data['recipients'] = \App\Models\Recipient::select()->where('work_order_id', $id)->get();
        $data['count'] = \App\Models\Recipient::select(DB::raw('COUNT(*) AS total'), 'categories.name')->groupBy('recipients.category_id')->join('categories', 'categories.id', '=', 'recipients.category_id')->where('work_order_id', $id)->get();
        $data['id'] = $id;
        $data['notice_id'] = $notice_id;
        $data['officers_directors'] = \App\Models\CustomerAgent::where('customer_id', '=', Auth::user()->customer->id)
                ->get();
        $data['projectTypes'] = \App\Models\ProjectType::select()->get();
        $data['customer_id'] = Auth::user()->customer->id;
        $data['categories'] = \App\Models\Category::select()->orderBy('name', 'ASC')->get();
        $data['project_roles'] = \App\Models\ProjectRoles::select()->orderBy('name', 'ASC')->get();
        $result = \App\Models\WorkOrder::select('state_id')->join('notice_fields', 'notice_fields.notice_id', '=', 'work_orders.notice_id')->where('work_orders.id', $id)->limit(1)->get()->toArray();
        $data['names'] = \App\Models\Contact::select('contacts.id', 'contacts.company_name')
                //  ->where('contact_details.type', '=', 0)
                ->where('contacts.customer_id', '=', Auth::user()->customer->id)
                //  ->Join('contact_details', 'contact_details.contact_id', '=', 'contacts.id')
                ->orderBy('contacts.company_name', 'ASC')
                ->get();
        $data['operation'] = "new_work_order";
        $data['attachment'] = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $id)
                ->get();
        $data['notes'] = \App\Models\WorkOrderNotes::leftjoin('users', 'users.id', '=', 'work_order_notes.user_id')->where('customer_id', '=', Auth::user()->customer->id)
                ->where('work_order_notes.visibility', '=', 0)
                ->where('work_order_notes.work_order_id', '=', $id)
                ->orderBy('work_order_notes.id', 'DESC')
                ->get();
        $data['corrections'] = \App\Models\WorkOrderCorrections::leftjoin('users', 'users.id', '=', 'work_order_corrections.user_id')->where('customer_id', '=', Auth::user()->customer->id)
                ->where('work_order_corrections.visibility', '=', 0)
                ->where('work_order_corrections.work_order_id', '=', $id)
                ->orderBy('work_order_corrections.id', 'DESC')
                ->get();

       /* $data['senderName'] = \App\Models\Customer::leftjoin('users', 'users.id', '=', 'customers.user_id')
                            ->select('users.name')
                            ->where('customers.id', '=',Auth::user()->customer->id)
                            ->first();*/
        if ($request->continue == 'continue') {
            /*if ($notice[0]['type'] == 2) {
                $data['notice_fields_section1'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.workorder_id', $id)->where('work_order_fields.notice_id', $notice_id)->where('notice_fields.section', 1)->orderBy('sort_order', 'asc')->get();

                $data['notice_fields_section2'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.notice_id', $notice_id)->where('work_order_fields.workorder_id', $id)->where('notice_fields.section', 2)->orderBy('sort_order', 'asc')->get();
                $data['tab'] = 'recipients';

                return view('customer.workorder.edit_soft_notice', $data);
            } else {
                $data['notice_fields'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.notice_id', $notice_id)->where('work_order_fields.workorder_id', $id)->orderBy('sort_order', 'asc')->get();
                $data['tab'] = 'recipients';
                return view('customer.workorder.edit', $data);
            }*/
            /*return redirect()->to('customer/work-order/edit-new/' . $id . '/' . $notice_id . '/' . $is_rescind);*/
            return redirect('customer/new-work-order/create/'.$id.'/'.$notice_id);
        } else {
            return redirect('customer/view-work-order');
        }
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function workorderStoreNextStep(Request $request,$id,$notice_id) {
        //dd($request->all());
        $data = [];

        $data['customer_details'] = Auth::user()->customer;


        //  $fields = array_merge($request->except(['_token', 'continue', 'type', 'notice_id', 'work_order_attachment', 'file_name', 'doc_input', 'document_type', 'remove_doc', 'recipient_county_id', 'visibility', 'recipient_notary_county_id', 'recipient_project_county_id', 'recipient_recorded_county_id']));
        $fields = array_merge($request->except(['_token', 'continue', 'type', 'notice_id', 'edit_soft_notices_attachment', 'file_name', 'doc_input', 'hidden_city_val', 'duplicate', 'remove_doc', 'parent_work_order_previous', 'recipient_county_id', 'contracted_by_exist', 'document_type', 'work_order_attachment', 'visibility', 'recipient_notary_county_id', 'recipient_project_county_id', 'recipient_recorded_county_id','work_order_attachment_id']));

       
        //  dd($request->all());
        $admin_email = \App\User::leftjoin('role_users', 'role_users.user_id', '=', 'users.id')
                ->where('role_users.role_id', '=', 2)
                ->select('users.email')
                ->first();
        $account_manager_email = \App\Models\Customer::leftjoin('users', 'users.id', '=', 'customers.account_manager_id')
                ->select('users.email')
                ->find(Auth::user()->customer->id);

        /* if (isset($account_manager_email) && !empty($account_manager_email)) {
          $data['note_emails'] = [
          $admin_email->email,
          $account_manager_email->email
          ];
          } else {
          $data['note_emails'] = [
          $admin_email->email
          ];

          } */
        $data['note_emails'] = get_user_notice_email(Auth::user()->customer->id);
        $customerId = Auth::user()->customer->id;
        $account_manager_id = Auth::user()->customer->account_manager_id;
        $userId = Auth::user()->id;
       
        $notice_id = $notice_id;
        $workorderiddetails = \App\Models\WorkOrder::find($id);
        $userId = $workorderiddetails->user_id;
        $data['status'] = $workorderiddetails->status;
        $data['parent_work_order'] = $workorderiddetails->parent_id;
        $work_order_id = $id;
        //$id = $result1->id;
        $notice = \App\Models\Notice::select('name', 'id', 'type','master_notice_id')->where('id', $notice_id)->get()->toArray();
        $data['notice'] = $notice[0];
        $data['cities'] = \App\Models\City::select()->get();
        $data['countries'] = \App\Models\Country::select()->get();
        $data['states'] = \App\Models\State::select()->orderBy('name', 'asc')->get();
        $data['recipients'] = \App\Models\Recipient::select()->where('work_order_id', $id)->get();
        $data['count'] = \App\Models\Recipient::select(DB::raw('COUNT(*) AS total'), 'categories.name')->groupBy('recipients.category_id')->join('categories', 'categories.id', '=', 'recipients.category_id')->where('work_order_id', $id)->get();
        $data['id'] = $id;
        $data['notice_id'] = $notice_id;
        $data['officers_directors'] = \App\Models\CustomerAgent::where('customer_id', '=', Auth::user()->customer->id)
                ->get();
        $data['projectTypes'] = \App\Models\ProjectType::select()->get();
        $data['customer_id'] = Auth::user()->customer->id;
        $data['categories'] = \App\Models\Category::select()->orderBy('name', 'ASC')->get();
        $data['project_roles'] = \App\Models\ProjectRoles::select()->orderBy('name', 'ASC')->get();
        $result = \App\Models\WorkOrder::select('state_id')->join('notice_fields', 'notice_fields.notice_id', '=', 'work_orders.notice_id')->where('work_orders.id', $id)->limit(1)->get()->toArray();
        $data['names'] = \App\Models\Contact::select('contacts.id', 'contacts.company_name')
                //  ->where('contact_details.type', '=', 0)
                ->where('contacts.customer_id', '=', Auth::user()->customer->id)
                //  ->Join('contact_details', 'contact_details.contact_id', '=', 'contacts.id')
                ->orderBy('contacts.company_name', 'ASC')
                ->get();
        $data['operation'] = "new_work_order";
        $data['attachment'] = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $id)
                ->get();
        $data['notes'] = \App\Models\WorkOrderNotes::leftjoin('users', 'users.id', '=', 'work_order_notes.user_id')->where('customer_id', '=', Auth::user()->customer->id)
                ->where('work_order_notes.visibility', '=', 0)
                ->where('work_order_notes.work_order_id', '=', $id)
                ->orderBy('work_order_notes.id', 'DESC')
                ->get();
        $data['corrections'] = \App\Models\WorkOrderCorrections::leftjoin('users', 'users.id', '=', 'work_order_corrections.user_id')->where('customer_id', '=', Auth::user()->customer->id)
                ->where('work_order_corrections.visibility', '=', 0)
                ->where('work_order_corrections.work_order_id', '=', $id)
                ->orderBy('work_order_corrections.id', 'DESC')
                ->get();

   
       // if ($request->continue == 'continue') {
            if ($notice[0]['type'] == 2) {
                $data['notice_fields_section1'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.workorder_id', $id)->where('work_order_fields.notice_id', $notice_id)->where('notice_fields.section', 1)->orderBy('sort_order', 'asc')->get();

                $data['notice_fields_section2'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.notice_id', $notice_id)->where('work_order_fields.workorder_id', $id)->where('notice_fields.section', 2)->orderBy('sort_order', 'asc')->get();
                $data['tab'] = 'recipients';

                return view('customer.workorder.edit_soft_notice', $data);
            } else {
                $data['notice_fields'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.notice_id', $notice_id)->where('work_order_fields.workorder_id', $id)->orderBy('sort_order', 'asc')->get();
                $data['tab'] = 'recipients';
                return view('customer.workorder.edit', $data);
            }
           // return redirect('customer/new-work-order/create');
       // } 
    }
    public function store_recipients(Request $request, $id, $notice_id) {
       // dd($request->all());
        if(!is_numeric($request->city_id)){ 
            $city = \App\Models\City::firstOrNew(array('name' => $request->city_id));
            //$city = new \App\Models\City();
            $city->name = $request->city_id;
            $city->state_id = $request->state_id;
            $city->zip_code = $request->zip;
            $city->save();
            $request->request->set('city_id',$city->id);
        }

        $data['customer_details'] = Auth::user()->customer;

        /* $category = \App\Models\Category::create([
          'name' => $request->category_id]); */
        //$workorder_id = \App\Models\WorkOrder::orderBy('created_at', 'desc')->first();
        // $request->request->set('category_id',$category->id);
        $request->request->set('work_order_id', $id);
        $request->request->set('city_id', $request->city_id);
        // dd($request->all());
        $contracted_by = $request->name;

        $contact_id = $request->recipt_id;
        if ($contracted_by != "") {
            /* check contracted by is exist or not */
            $contracted_by_check = \App\Models\Contact::where('company_name', $contracted_by)->first();
            if (empty($contracted_by_check)) {
                $contact = \App\Models\Contact::create([
                            'customer_id' => Auth::user()->customer->id,
                            'company_name' => $contracted_by,
                            'company_address'=>$request->address,
                            'mailing_address'=>$request->address,
                            'city_id'=>$request->city_id,
                            'state_id'=>$request->state_id,
                            'zip'=>$request->zip,
                            'phone'=>$request->contact,
                            'email'=>$request->email,
                            'attn'=>$request->attn,
                ]);
                $contact_id = $contact->id;
            }
        }
        $request->request->set('contact_id', $contact_id);
        $first_category = \App\Models\Category::where('name', 'Contracted By')->first();
        if ($first_category->id == $request->category_id) {
            /* check contract details exit or not */
            $check_contract_recipient_exist = \App\Models\Recipient::where(['category_id' => $first_category->id, 'work_order_id' => $request->work_order_id])->where('id', '!=', $request->recipient_id)->first();

            /* if not empty delete previouse record */
            if (!empty($check_contract_recipient_exist)) {
                $contract_recipient_delete = \App\Models\Recipient::where(['category_id' => $request->category_id, 'work_order_id' => $request->work_order_id])->delete();
            }
        }
//dd($request->all());

        if ($request->continue == 'add') {
            $Recipient = \App\Models\Recipient::create(
                            $request->except(['_token', 'country_id']));//dd($Recipient);
            if ($Recipient) {
                Session::flash('success', 'Your work order # ' . $request->work_order_id . ' recipient has been submitted.');
            } else {
                Session::flash('success', 'Your work order # ' . $request->work_order_id . '  problem in record submitting.');
            }
        } else {
            $workorderRecipient = \App\Models\Recipient::find($request->recipient_id);
            $workorderRecipient->category_id = $request->category_id;
            $workorderRecipient->name = $request->name;
            $workorderRecipient->contact = $request->contact;
            $workorderRecipient->address = $request->address;
            $workorderRecipient->city_id = $request->city_id;
            $workorderRecipient->state_id = $request->state_id;
            $workorderRecipient->zip = $request->zip;
            $workorderRecipient->email = $request->email;
            $workorderRecipient->attn = $request->attn;
            $workorderRecipient->contact_id = $request->contact_id;
            $workorderRecipient->save();
            if ($workorderRecipient) {
                Session::flash('success', 'Your work order # ' . $request->work_order_id . ' recipient successfully updated .');
                $work_orders = \App\Models\WorkOrderFields::select('workorder_id')->join('notice_fields','work_order_fields.notice_field_id','notice_fields.id')->join('work_orders','work_orders.id','work_order_fields.workorder_id')->where('notice_fields.name','parent_work_order')->whereNotIn('work_orders.status',[5,6])->where('work_order_fields.value',$id)->get()->toArray();
             
              $parent_work_order_recipients = \App\Models\Recipient::select()->where('work_order_id',$id)->where('category_id','!=',21)->get()->toArray();

              if(isset($work_orders) && count($work_orders) > 0) {

              foreach ($work_orders As $k => $work_order) {

                $delete_previous_recipients = \App\Models\Recipient::where('work_order_id',$work_order['workorder_id'])->where('parent_work_order',$id)->delete();
                if (isset($parent_work_order_recipients) && count($parent_work_order_recipients) > 0) {
                          foreach ($parent_work_order_recipients As $k => $each_parent_recipient) {
                            $usps_recipient = \App\Models\UspsAddress::select()->where('recipient_id',$each_parent_recipient['id'])->get()->toArray();
                            
                            $save_recipients = \App\Models\Recipient::create([
                                                        'work_order_id' => $work_order['workorder_id'],
                                                        'category_id' => $each_parent_recipient['category_id'],
                                                        'name' => $each_parent_recipient['name'],
                                                        'contact' => $each_parent_recipient['contact'],
                                                        'address' => $each_parent_recipient['address'],
                                                        'city_id' => $each_parent_recipient['city_id'],
                                                        'attn' => $each_parent_recipient['attn'],
                                                        'state_id' => $each_parent_recipient['state_id'],
                                                        'zip' => $each_parent_recipient['zip'],
                                                        'email' => $each_parent_recipient['email'],
                                                        'attn' => $each_parent_recipient['attn'],
                                                         'parent_work_order'=> $id,

                                            ]);
                          // dd($usps_recipient);
                          if(!empty($save_recipients) && !empty($usps_recipient)){
                             $usps_address = ['address' => $usps_recipient[0]['address'], 'city' => $usps_recipient[0]['city'], 'state' => $usps_recipient[0]['state'], 'zipcode' => $usps_recipient[0]['zipcode'], 'zipcode_add_on' => $usps_recipient[0]['zipcode_add_on'], 'dpb' => $usps_recipient[0]['dpb'], 'check_digit' => $usps_recipient[0]['check_digit'], 'comment' => $usps_recipient[0]['comment'], 'override_hash' => $usps_recipient[0]['override_hash']];
                            $usps_address['recipient_id'] = $save_recipients->id;
                            \App\Models\UspsAddress::insert($usps_address);
                          }

                    }
                  }
               }
             }

            } else {
                Session::flash('success', 'Your work order # ' . $request->work_order_id . ' problem in record submitting.');
            }
        }
        $notice = \App\Models\Notice::select('name', 'id', 'type','master_notice_id')->where('id', $notice_id)->get()->toArray();
        $data['notice'] = $notice[0];
        $data['cities'] = \App\Models\City::select()->get();
        $data['countries'] = \App\Models\Country::select()->get();
        $data['categories'] = \App\Models\Category::select()->orderBy('name', 'ASC')->get();
        $data['project_roles'] = \App\Models\ProjectRoles::select()->orderBy('name', 'ASC')->get();
        $data['states'] = \App\Models\State::select()->orderBy('name', 'asc')->get();
        $data['recipients'] = \App\Models\Recipient::select()->where('work_order_id', $id)->get();
        $data['officers_directors'] = \App\Models\CustomerAgent::where('customer_id', '=', Auth::user()->customer->id)
                ->get();
        $data['projectTypes'] = \App\Models\ProjectType::select()->get();
        $data['count'] = \App\Models\Recipient::select(DB::raw('COUNT(*) AS total'), 'categories.name')->groupBy('recipients.category_id')->join('categories', 'categories.id', '=', 'recipients.category_id')->where('work_order_id', $id)->get();
        $data['names'] = \App\Models\Contact::select('contacts.id', 'contacts.company_name')
                // ->where('contact_details.type', '=', 0)
                ->where('contacts.customer_id', '=', Auth::user()->customer->id)
                //  ->Join('contact_details', 'contact_details.contact_id', '=', 'contacts.id')
                ->orderBy('contacts.company_name', 'ASC')
                ->get();

        /*  $data['count']      = \App\Models\Category::select(DB::raw('COUNT(*) AS total'),'name')->groupBy('name')->get(); */
        //dd($data['count']);
        $data['id'] = $id;
        $data['notice_id'] = $notice_id;
//        $result = \App\Models\WorkOrder::select('state_id')->join('notice_fields', 'notice_fields.id', '=', 'work_orders.notice_id')->where('work_orders.id', $id)->get()->toArray();
        $result = \App\Models\WorkOrder::select('state_id', 'customer_id')->join('notice_fields', 'notice_fields.notice_id', '=', 'work_orders.notice_id')->where('work_orders.id', $id)->limit(1)->get()->toArray();
        $work_order_status = \App\Models\WorkOrder::find($id);
        if ($work_order_status->status == 1) {
            $data['status'] = 'request';
        } else if ($work_order_status->status == 0) {
            $data['status'] = 'draft';
        }
        $data['attachment'] = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $id)
                ->get();
        $admin_email = \App\User::leftjoin('role_users', 'role_users.user_id', '=', 'users.id')
                ->where('role_users.role_id', '=', 2)
                ->select('users.email')
                ->first();
        $account_manager_email = \App\Models\Customer::leftjoin('users', 'users.id', '=', 'customers.account_manager_id')
                ->select('users.email')
                ->find(Auth::user()->customer->id);

        /* if (isset($account_manager_email) && !empty($account_manager_email)) {
          $data['note_emails'] = [
          $admin_email->email,
          $account_manager_email->email
          ];
          } else {
          $data['note_emails'] = [
          $admin_email->email
          ];

          } */
        $data['note_emails'] = get_user_notice_email($result[0]['customer_id']);

        $data['notes'] = \App\Models\WorkOrderNotes::leftjoin('users', 'users.id', '=', 'work_order_notes.user_id')->where('customer_id', '=', Auth::user()->customer->id)
                ->where('work_order_notes.visibility', '=', 0)
                ->where('work_order_notes.work_order_id', '=', $id)
                ->orderBy('work_order_notes.id', 'DESC')
                ->get();
        $data['corrections'] = \App\Models\WorkOrderCorrections::leftjoin('users', 'users.id', '=', 'work_order_corrections.user_id')->where('customer_id', '=', Auth::user()->customer->id)
                ->where('work_order_corrections.visibility', '=', 0)
                ->where('work_order_corrections.work_order_id', '=', $id)
                ->orderBy('work_order_corrections.id', 'DESC')
                ->get();
      
        if ($notice[0]['type'] == 2) {
            $data['notice_fields_section1'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.workorder_id', $id)->where('work_order_fields.notice_id', $notice_id)->where('notice_fields.section', 1)->orderBy('sort_order', 'asc')->get();
            $data['notice_fields_section2'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.notice_id', $notice_id)->where('work_order_fields.workorder_id', $id)->where('notice_fields.section', 2)->orderBy('sort_order', 'asc')->get();
            $data['tab'] = 'recipients';
            //To display parent work order
            foreach ($data['notice_fields_section1'] as $key => $value) {
                $key = explode("_", $key);

                if ($value) {

                    if ($value->name == 'parent_work_order') {
                        $parent_work_order = $value->value;
                        $data['parent_work_order'] = $parent_work_order;
                    }
                }
            }
            //return view('customer.workorder.edit_soft_notice', $data);
        } else {
            $data['notice_fields'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.notice_id', $notice_id)->where('work_order_fields.workorder_id', $id)->orderBy('sort_order', 'asc')->get();
            $data['tab'] = 'recipients';
            //To display parent work order
            foreach ($data['notice_fields'] as $key => $value) {
                $key = explode("_", $key);

                if ($value) {

                    if ($value->name == 'parent_work_order') {
                        $parent_work_order = $value->value;
                        $data['parent_work_order'] = $parent_work_order;
                    }
                }
            }
            //return view('customer.workorder.edit', $data);
        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $notice_id) {

//dd($request->all());
        $data['customer_details'] = Auth::user()->customer;



        //  $fields = array_merge($request->except(['_token', 'continue', 'type', 'notice_id', 'edit_soft_notices_attachment', 'file_name', 'doc_input', 'hidden_city_val', 'duplicate', 'remove_doc', 'parent_work_order_previous', 'recipient_county_id', 'contracted_by_exist', 'document_type', 'work_order_attachment']));
        $fields = array_merge($request->except(['_token', 'continue', 'type', 'notice_id', 'edit_soft_notices_attachment', 'file_name', 'doc_input', 'hidden_city_val', 'duplicate', 'remove_doc', 'parent_work_order_previous', 'recipient_county_id', 'contracted_by_exist', 'document_type', 'work_order_attachment', 'visibility', 'recipient_notary_county_id', 'recipient_project_county_id', 'recipient_recorded_county_id']));

        $fieldsData = [];
        $values = [];
        $i = 0;
        foreach ($fields as $key => $value) {
            $field = explode("_", $key);
            $fieldsData[$i] = $field['0'];
            $values[$i] = $value;
            $i++;
        }
        // dd($request->all());
        $customerId = Auth::user()->customer->id;
        $userId = Auth::user()->id;
        $account_manager_id = Auth::user()->customer->account_manager_id;

        if ($request->duplicate == 'duplicate') {

            if ($request->continue == 'Continue') {
                $result1 = WorkOrder::create([
                            'notice_id' => $request->notice_id,
                            'customer_id' => $customerId,
                            'order_no' => str_random(5),
                            'user_id' => $userId,
                            'status' => '1',
                            'account_manager_id' => $account_manager_id,
                ]);
                $data['status'] = 'request';
                /*Session::flash('success', 'Your work order # ' . $result1->id . ' has been successfully submitted. <a href="' . url('customer/work-order/view/' . $result1->id) . '">Click here to print.</a>');*/
            } else {
                $result1 = WorkOrder::create([
                            'notice_id' => $request->notice_id,
                            'customer_id' => $customerId,
                            'order_no' => str_random(5),
                            'user_id' => $userId,
                            'status' => '0',
                            'account_manager_id' => $account_manager_id,
                ]);
                $data['status'] = 'draft';
                Session::flash('success', 'Your work order # ' . $result1->id . ' has been saved as a draft you must complete Work Order and submit at a later time.<a href="' . url('customer/work-order/view/' . $result1->id) . '">Click here to print.</a> ');
            }
            $recipients = \App\Models\Recipient::select('id')->where('work_order_id', $id)->get()->toArray();
            foreach ($recipients as $key => $value) {
                # code...

                $recipient = \App\Models\Recipient::find($value['id']);
                \App\Models\Recipient::create([
                    'work_order_id' => $result1->id,
                    'category_id' => $recipient->category_id,
                    'name' => $recipient->name,
                    'contact' => $recipient->contact,
                    'mobile' => $recipient->mobile,
                    'address' => $recipient->address,
                    'city_id' => $recipient->city_id,
                    'state_id' => $recipient->state_id,
                    'zip' => $recipient->zip,
                    'fax' => $recipient->fax,
                    'attn' => $recipient->attn,
                ]);
            }
            $data['recipients'] = \App\Models\Recipient::select()->where('work_order_id', $id)->get();
            /* if ($result1) {
              Session::flash('success', 'Record Created Successfully');
              } else {
              Session::flash('success', 'Problem in Record saving');
              } */
        } else {
            if ($request->continue == 'Continue') {
                $result1 = \App\Models\WorkOrder::find($id);
                $result1->notice_id = $request->notice_id;
                $result1->customer_id = $customerId;
                $result1->order_no = str_random(5);
                $result1->user_id = $userId;
                $result1->status = '1';
                $result1->save();
                $data['status'] = 'request';
               /* Session::flash('success', 'Your work order # ' . $result1->id . ' has been Created Successfully.<a href="' . url('customer/work-order/view/' . $result1->id) . '">Click here to print.</a>');*/
            } else {
                $result1 = \App\Models\WorkOrder::find($id);
                $result1->notice_id = $request->notice_id;
                $result1->customer_id = $customerId;
                $result1->order_no = str_random(5);
                $result1->user_id = $userId;
                $result1->status = '0';
                $result1->save();
                $data['status'] = 'draft';
                Session::flash('success', 'Your work order # ' . $result1->id . ' has been Saved Successfully.<a href="' . url('customer/work-order/view/' . $result1->id) . '">Click here to print.</a>');
            }
            $data['recipients'] = \App\Models\Recipient::select()->where('work_order_id', $result1->id)->get();
        }
        if ($request->duplicate != 'duplicate') {
            if ($result1) {

                $attachment = explode(',|,', trim($request->edit_soft_notices_attachment[0],'|,'));
           //dd($attachment);
//            if (isset($attachment) && !empty($attachment)) {  
                foreach ($attachment AS $k => $v) {
                    if($v!="|"){
                        $v = trim($v,'|,');
                    if (isset($request->remove_doc) && !empty($request->remove_doc) && $request->remove_doc[0] != '') {
                        $remove_doc = explode(',', $request->remove_doc[0]);

                        $each_attachment = explode(',', $v);//dd($attachment,$each_attachment,$remove_doc);
                        if ($each_attachment[0] != '' && isset($each_attachment[4])) {
                            $true = !in_array($each_attachment[4], $remove_doc);
                        } else {
                            $true = '';
                        }
                        if (isset($true) && $true != '') {
                            if ($v != '') {

                                $work_order_attachment_obj = new \App\Models\WorkOrderAttachment();
                                $work_order_attachment_obj->work_order_id = $result1->id;
                                $work_order_attachment_obj->type = $each_attachment[0];
                                $work_order_attachment_obj->title = $each_attachment[1];
                                $work_order_attachment_obj->file_name = $each_attachment[2];
                                $work_order_attachment_obj->original_file_name = $each_attachment[3];
                                $work_order_attachment_obj->save();
                            }
                        }
                    } else {
                        if ($v != '') {
                            $each_attachment = explode(',', $v);
                            $work_order_attachment_obj = new \App\Models\WorkOrderAttachment();
                            $work_order_attachment_obj->work_order_id = $result1->id;
                            $work_order_attachment_obj->type = $each_attachment[0];
                            $work_order_attachment_obj->title = $each_attachment[1];
                            $work_order_attachment_obj->file_name = $each_attachment[2];
                            $work_order_attachment_obj->original_file_name = $each_attachment[3];
                            $work_order_attachment_obj->save();
                        }
                    }
                }
            }
                //For parent work document and recipient and contracted by
                foreach ($fields as $key => $value) {
                    $field = explode("_", $key);



                    if (isset($field[3])) {
                        $check_field = $field[1] . '_' . $field[2] . '_' . $field[3];

                        if ($check_field == 'parent_work_order') {
                            $parent_work_order = $value;

                            $parent_work_order_previous = $request->parent_work_order_previous;
                            //dd($parent_work_order_previous);
                            if ($parent_work_order_previous != $parent_work_order) {
                                //If parent work order is selected document of parent work order will inserted
                                if ($check_field == 'parent_work_order' && $parent_work_order != '') {
                                    if (isset($request->remove_doc) && !empty($request->remove_doc) && $request->remove_doc[0] != '') {

                                        $previous_attachments = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $parent_work_order)->get();

                                        if (isset($previous_attachments) && count($previous_attachments) > 0) {
                                            foreach ($previous_attachments AS $k1 => $a1) {

                                                /*$each_remove_doc = explode(',', $request->remove_doc[0]);
                                                foreach ($each_remove_doc As $k => $each) {
                                                    $explode_v = explode('_', $each);
                                                    $count = count($explode_v);
                                                    if($count>1){
                                                    $each_remove_doc[$k] = $explode_v[$count - 3] . '_' . $explode_v[$count - 2] . '_' . $explode_v[$count - 1];
                                                    }
                                                }*/

                                               // $true = !in_array($a1->file_name, $each_remove_doc);
                                                $true = "true";

                                                if (isset($true) && $true != '') {

                                                    $work_order_attachment_obj = new \App\Models\WorkOrderAttachment();
                                                    $work_order_attachment_obj->work_order_id = $result1->id;
                                                    $work_order_attachment_obj->type = $a1->type;
                                                    $work_order_attachment_obj->title = $a1->title;
                                                     if(!empty($a1->file_name)){
                                                        $work_order_attachment_obj->file_name = $result1->id . '_' . $a1->file_name;
                                                        }else{
                                                         $work_order_attachment_obj->file_name = "";
                                                        }
                                                        if(!empty($a1->original_file_name)){
                                                        $work_order_attachment_obj->original_file_name = $a1->original_file_name;
                                                        }else{
                                                           $work_order_attachment_obj->original_file_name = "";
                                                        }
                                                    if (Auth::user()->hasRole('account-manager')) {
                                                        $work_order_attachment_obj->visibility = $a1->visibility;
                                                    } else {
                                                        $work_order_attachment_obj->visibility = 0;
                                                    }
                                                    $work_order_attachment_obj->save();
                                                    if (file_exists(public_path() . '/attachment/work_order_document/parent_document/' . $a1->file_name) && !empty($a1->file_name)) {
                                                        rename(public_path() . '/attachment/work_order_document/parent_document/' . $a1->file_name, public_path() . '/attachment/work_order_document/parent_document/' . $work_order_attachment_obj->file_name);
                                                        \File::copy(public_path() . '/attachment/work_order_document/parent_document/' . $work_order_attachment_obj->file_name, public_path() . '/attachment/work_order_document/' . $work_order_attachment_obj->file_name);
                                                        unlink(public_path() . '/attachment/work_order_document/parent_document/' . $work_order_attachment_obj->file_name);
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        $remove_doc = explode(',', $request->remove_doc[0]);
                                        $previous_attachments = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $parent_work_order)->get();

                                        if (isset($previous_attachments) && count($previous_attachments) > 0) {
                                            foreach ($previous_attachments AS $k1 => $a1) {
                                              //  dd($previous_attachments);
                                                $work_order_attachment_obj = new \App\Models\WorkOrderAttachment();
                                                $work_order_attachment_obj->work_order_id = $result1->id;
                                                $work_order_attachment_obj->type = $a1->type;
                                                $work_order_attachment_obj->title = $a1->title;
                                                if(!empty($a1->file_name)){
                                                $work_order_attachment_obj->file_name = $result1->id . '_' . $a1->file_name;
                                                }else{
                                                 $work_order_attachment_obj->file_name = "";
                                                }
                                                if(!empty($a1->original_file_name)){
                                                $work_order_attachment_obj->original_file_name = $a1->original_file_name;
                                                }else{
                                                   $work_order_attachment_obj->original_file_name ="";
                                                }
                                                if (Auth::user()->hasRole('account-manager')) {
                                                    $work_order_attachment_obj->visibility = $a1->visibility;
                                                } else {
                                                    $work_order_attachment_obj->visibility = 0;
                                                }
                                                $work_order_attachment_obj->save();

                                                if (file_exists(public_path() . '/attachment/work_order_document/parent_document/' . $a1->file_name) && !empty($a1->file_name)) {
                                                    rename(public_path() . '/attachment/work_order_document/parent_document/' . $a1->file_name, public_path() . '/attachment/work_order_document/parent_document/' . $work_order_attachment_obj->file_name);
                                                    \File::copy(public_path() . '/attachment/work_order_document/parent_document/' . $work_order_attachment_obj->file_name, public_path() . '/attachment/work_order_document/' . $work_order_attachment_obj->file_name);
                                                    unlink(public_path() . '/attachment/work_order_document/parent_document/' . $work_order_attachment_obj->file_name);
                                                }
                                            }
                                        }
                                    }

                                    $data['parent_work_order'] = $parent_work_order;

                                    $first_category = \App\Models\Category::where('name', 'Contracted By')->first();
                                    /* save parent work order recipient  */
                                    if (isset($request->contracted_by_exist)) {
                                        $parent_work_order_recipients = \App\Models\Recipient::select()->where('work_order_id', $parent_work_order)->where('category_id', '!=', $first_category->id)->get();
                                    } else {
                                        $parent_work_order_recipients = \App\Models\Recipient::select()->where('work_order_id', $parent_work_order)->get();
                                    }
                                    \App\Models\Recipient::where( 'work_order_id', $result1->id)->where('parent_work_order','!=',NULL)->delete();
                                    // echo $check_contracted_by_field_exist;
                                    //dd($parent_work_order_recipients);
                                    //die;
                                    if (isset($parent_work_order_recipients) && count($parent_work_order_recipients) > 0) {
                                        foreach ($parent_work_order_recipients As $k => $each_parent_recipient) {
                                            $save_recipients = \App\Models\Recipient::create([
                                                        'work_order_id' => $result1->id,
                                                        'category_id' => $each_parent_recipient->category_id,
                                                        'name' => $each_parent_recipient->name,
                                                        'contact' => $each_parent_recipient->contact,
                                                        'address' => $each_parent_recipient->address,
                                                        'city_id' => $each_parent_recipient->city_id,
                                                        'attn' => $each_parent_recipient->attn,
                                                        'state_id' => $each_parent_recipient->state_id,
                                                        'zip' => $each_parent_recipient->zip,
                                                        'email' => $each_parent_recipient->email,
                                                        'attn' => $each_parent_recipient->attn,
                                                        'parent_work_order'=> $parent_work_order,

                                            ]);
                                        }
                                    }
                                }
                            }
                            $data['parent_work_order'] = $parent_work_order;
                        }
                    }

                    //Insert contracted by is first recipient
                    if (isset($field[2])) {
                        $check_contracted_by_field = $field[1] . '_' . $field[2];
                        if ($check_contracted_by_field == 'contracted_by') {
                            $check_contracted_by_field_exist = '1';
                            $first_category = \App\Models\Category::where('name', 'Contracted By')->first();
                            $contracted_by = $value;
                            if ($contracted_by != "") {
                                /* check contracted by is exist or not */
                                $contracted_by_check = \App\Models\Contact::where('company_name', $contracted_by)->first();
                                if (empty($contracted_by_check)) {
                                    $contact = \App\Models\Contact::create([
                                                'customer_id' => Auth::user()->customer->id,
                                                'company_name' => $contracted_by,
                                    ]);
                                }
                            }
                            $contactdetails = \App\Models\Contact::where('company_name', '=', $contracted_by)
                                    ->where('customer_id', '=', $customerId)
                                    ->first();
                            if (isset($contactdetails) && $contactdetails != null) {
                                /* check contract details exit or not */
                                $check_contract_recipient_exist = \App\Models\Recipient::where(['category_id' => $first_category->id, 'work_order_id' => $result1->id])->first();
                                /* if not empty delete previouse record */
                                if (!empty($check_contract_recipient_exist)) {
                                    if ($check_contract_recipient_exist->name != $contracted_by) {
                                        $contract_recipient_delete = \App\Models\Recipient::where(['category_id' => $first_category->id, 'work_order_id' => $result1->id])->delete();
                                        $check_contract_recipient_exist = [];
                                    }
                                }
                                if (empty($check_contract_recipient_exist)) {
                                    $Recipient = \App\Models\Recipient::create([
                                                'work_order_id' => $result1->id,
                                                'category_id' => $first_category->id,
                                                'name' => $contactdetails->company_name,
                                                'contact' => $contactdetails->phone,
                                                'address' => $contactdetails->mailing_address,
                                                'city_id' => $contactdetails->city_id,
                                                'state_id' => $contactdetails->state_id,
                                                'zip' => $contactdetails->zip,
                                                'email' => $contactdetails->email,
                                                'attn' => $contactdetails->attn,
                                    ]);
                                }
                            }
                        }
                    }
                }
            }
        }

        $work_order_id = $result1->id;

        $result2 = \App\Models\WorkOrderFields::select()->where('workorder_id', $work_order_id)->delete();

        foreach ($fieldsData as $key => $value) {

            if ($values[$key] == NULL) {
                $result2 = \App\Models\WorkOrderFields::create([
                            'workorder_id' => $work_order_id,
                            'notice_id' => $request->notice_id,
                            'notice_field_id' => $value,
                            'value' => '',
                ]);
            } else
                $result2 = \App\Models\WorkOrderFields::create([
                            'workorder_id' => $work_order_id,
                            'notice_id' => $request->notice_id,
                            'notice_field_id' => $value,
                            'value' => nl2br($values[$key]),
                ]);
        }
        /* if ($result1 && $result3) {
          Session::flash('success', 'New Record Successfully');
          } else {
          Session::flash('success', 'Problem in Record saving');
          } */

        $notice = \App\Models\Notice::select('name', 'id', 'type','master_notice_id')->where('id', $notice_id)->get()->toArray();
        $data['notice'] = $notice[0];
        $data['cities'] = \App\Models\City::select()->get();
        $data['countries'] = \App\Models\Country::select()->get();
        $data['states'] = \App\Models\State::select()->orderBy('name', 'asc')->get();
        $data['officers_directors'] = \App\Models\CustomerAgent::where('customer_id', '=', Auth::user()->customer->id)
                ->get();
        $data['projectTypes'] = \App\Models\ProjectType::select()->get();
        $data['recipients'] = \App\Models\Recipient::select()->where('work_order_id', $id)->get();
        //dd($data['recipients']);
        $data['categories'] = \App\Models\Category::select()->orderBy('name', 'ASC')->get();
        $data['project_roles'] = \App\Models\ProjectRoles::select()->orderBy('name', 'ASC')->get();
        $data['duplicate'] = $request->duplicate;
        $data['count'] = \App\Models\Recipient::select(DB::raw('COUNT(*) AS total'), 'categories.name')->groupBy('recipients.category_id')->join('categories', 'categories.id', '=', 'recipients.category_id')->where('work_order_id', $id)->get();
        $data['id'] = $id;
        $data['notice_id'] = $notice_id;
//        $result = \App\Models\WorkOrder::select('state_id')->join('notice_fields', 'notice_fields.id', '=', 'work_orders.notice_id')->where('work_orders.id', $id)->get()->toArray();
        $result = \App\Models\WorkOrder::select('state_id', 'customer_id')->join('notice_fields', 'notice_fields.notice_id', '=', 'work_orders.notice_id')->where('work_orders.id', $id)->limit(1)->get()->toArray();
        $data['names'] = \App\Models\Contact::select('contacts.id', 'contacts.company_name')
                //   ->where('contact_details.type', '=', 0)
                ->where('contacts.customer_id', '=', Auth::user()->customer->id)
                //  ->Join('contact_details', 'contact_details.contact_id', '=', 'contacts.id')
                ->orderBy('contacts.company_name', 'ASC')
                ->get();
        $data['attachment'] = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $id)
                ->get();
        $data['operation'] = 'update_work_order';
        $admin_email = \App\User::leftjoin('role_users', 'role_users.user_id', '=', 'users.id')
                ->where('role_users.role_id', '=', 2)
                ->select('users.email')
                ->first();
        $account_manager_email = \App\Models\Customer::leftjoin('users', 'users.id', '=', 'customers.account_manager_id')
                ->select('users.email')
                ->find(Auth::user()->customer->id);

        /* if (isset($account_manager_email) && !empty($account_manager_email)) {
          $data['note_emails'] = [
          $admin_email->email,
          $account_manager_email->email
          ];
          } else {
          $data['note_emails'] = [
          $admin_email->email
          ];

          } */
        $data['note_emails'] = get_user_notice_email($result[0]['customer_id']);
        $data['notes'] = \App\Models\WorkOrderNotes::leftjoin('users', 'users.id', '=', 'work_order_notes.user_id')->where('customer_id', '=', Auth::user()->customer->id)
                ->where('work_order_notes.work_order_id', '=', $id)
                ->where('work_order_notes.visibility', '=', 0)
                ->orderBy('work_order_notes.id', 'DESC')
                ->get();
        $data['corrections'] = \App\Models\WorkOrderCorrections::leftjoin('users', 'users.id', '=', 'work_order_corrections.user_id')->where('customer_id', '=', Auth::user()->customer->id)
                ->where('work_order_corrections.work_order_id', '=', $id)
                ->where('work_order_corrections.visibility', '=', 0)
                ->orderBy('work_order_corrections.id', 'DESC')
                ->get();
       //dd($request->all());
        if ($request->continue == 'Continue') {
            return redirect()->to('customer/work-order/create/' . $id . '/' . $notice_id);
        } else {
            return redirect('customer/view-work-order');
        }
        /*if ($request->continue == 'Continue') {
            if ($notice[0]['type'] == 2) {
                $data['notice_fields_section1'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.workorder_id', $id)->where('work_order_fields.notice_id', $notice_id)->where('notice_fields.section', 1)->orderBy('sort_order', 'asc')->get();

                $data['notice_fields_section2'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.notice_id', $notice_id)->where('work_order_fields.workorder_id', $id)->where('notice_fields.section', 2)->orderBy('sort_order', 'asc')->get();
                $data['tab'] = 'recipients';

                return view('customer.workorder.edit_soft_notice', $data);
            } else {
                $data['notice_fields'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.notice_id', $notice_id)->where('work_order_fields.workorder_id', $id)->orderBy('sort_order', 'asc')->get();

                $data['tab'] = 'recipients';
                return view('customer.workorder.edit', $data);
            }
        } else {
            return redirect('customer/view-work-order');
        }*/
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function researchEditNextStep(Request $request, $id, $notice_id) {

        //dd($request->all());
        $data['customer_details'] = Auth::user()->customer;



        //  $fields = array_merge($request->except(['_token', 'continue', 'type', 'notice_id', 'edit_soft_notices_attachment', 'file_name', 'doc_input', 'hidden_city_val', 'duplicate', 'remove_doc', 'parent_work_order_previous', 'recipient_county_id', 'contracted_by_exist', 'document_type', 'work_order_attachment']));
        $fields = array_merge($request->except(['_token', 'continue', 'type', 'notice_id', 'edit_soft_notices_attachment', 'file_name', 'doc_input', 'hidden_city_val', 'duplicate', 'remove_doc', 'parent_work_order_previous', 'recipient_county_id', 'contracted_by_exist', 'document_type', 'work_order_attachment', 'visibility', 'recipient_notary_county_id', 'recipient_project_county_id', 'recipient_recorded_county_id']));

        // dd($request->all());
        $customerId = Auth::user()->customer->id;
        $userId = Auth::user()->id;
        $account_manager_id = Auth::user()->customer->account_manager_id;

        
        $work_order_id = $id;

/*        $result2 = \App\Models\WorkOrderFields::select()->where('workorder_id', $work_order_id)->delete();*/

      
        /* if ($result1 && $result3) {
          Session::flash('success', 'New Record Successfully');
          } else {
          Session::flash('success', 'Problem in Record saving');
          } */

        $notice = \App\Models\Notice::select('name', 'id', 'type','master_notice_id')->where('id', $notice_id)->get()->toArray();
        $data['notice'] = $notice[0];
        $data['cities'] = \App\Models\City::select()->get();
        $data['countries'] = \App\Models\Country::select()->get();
        $data['states'] = \App\Models\State::select()->orderBy('name', 'asc')->get();
        $data['officers_directors'] = \App\Models\CustomerAgent::where('customer_id', '=', Auth::user()->customer->id)
                ->get();
        $data['projectTypes'] = \App\Models\ProjectType::select()->get();
        $data['recipients'] = \App\Models\Recipient::select()->where('work_order_id', $id)->get();
        //dd($data['recipients']);
        $data['categories'] = \App\Models\Category::select()->orderBy('name', 'ASC')->get();
        $data['project_roles'] = \App\Models\ProjectRoles::select()->orderBy('name', 'ASC')->get();
        $data['duplicate'] = $request->duplicate;
        $data['count'] = \App\Models\Recipient::select(DB::raw('COUNT(*) AS total'), 'categories.name')->groupBy('recipients.category_id')->join('categories', 'categories.id', '=', 'recipients.category_id')->where('work_order_id', $id)->get();
        $data['id'] = $id;
        $data['notice_id'] = $notice_id;
//        $result = \App\Models\WorkOrder::select('state_id')->join('notice_fields', 'notice_fields.id', '=', 'work_orders.notice_id')->where('work_orders.id', $id)->get()->toArray();
        $result = \App\Models\WorkOrder::select('state_id', 'customer_id')->join('notice_fields', 'notice_fields.notice_id', '=', 'work_orders.notice_id')->where('work_orders.id', $id)->limit(1)->get()->toArray();
        $data['names'] = \App\Models\Contact::select('contacts.id', 'contacts.company_name')
                //   ->where('contact_details.type', '=', 0)
                ->where('contacts.customer_id', '=', Auth::user()->customer->id)
                //  ->Join('contact_details', 'contact_details.contact_id', '=', 'contacts.id')
                ->orderBy('contacts.company_name', 'ASC')
                ->get();
        $data['attachment'] = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $id)
                ->get();
        $data['operation'] = 'update_work_order';
        $admin_email = \App\User::leftjoin('role_users', 'role_users.user_id', '=', 'users.id')
                ->where('role_users.role_id', '=', 2)
                ->select('users.email')
                ->first();
        $account_manager_email = \App\Models\Customer::leftjoin('users', 'users.id', '=', 'customers.account_manager_id')
                ->select('users.email')
                ->find(Auth::user()->customer->id);

        /* if (isset($account_manager_email) && !empty($account_manager_email)) {
          $data['note_emails'] = [
          $admin_email->email,
          $account_manager_email->email
          ];
          } else {
          $data['note_emails'] = [
          $admin_email->email
          ];

          } */
        $data['note_emails'] = get_user_notice_email($result[0]['customer_id']);
        $data['notes'] = \App\Models\WorkOrderNotes::leftjoin('users', 'users.id', '=', 'work_order_notes.user_id')->where('customer_id', '=', Auth::user()->customer->id)
                ->where('work_order_notes.work_order_id', '=', $id)
                ->where('work_order_notes.visibility', '=', 0)
                ->orderBy('work_order_notes.id', 'DESC')
                ->get();
        $data['corrections'] = \App\Models\WorkOrderCorrections::leftjoin('users', 'users.id', '=', 'work_order_corrections.user_id')->where('customer_id', '=', Auth::user()->customer->id)
                ->where('work_order_corrections.work_order_id', '=', $id)
                ->where('work_order_corrections.visibility', '=', 0)
                ->orderBy('work_order_corrections.id', 'DESC')
                ->get();
        $workorderiddetails = \App\Models\WorkOrder::find($id);
        $data['status'] = $workorderiddetails->status;
        $data['parent_work_order'] = $workorderiddetails->parent_id;
        //dd($workorderiddetails);
      // dd($id,$notice_id);
        /*if ($request->continue == 'Continue') {
            return redirect()->to('customer/work-order/create/' . $id . '/' . $notice_id);
        } else {
            return redirect('customer/view-work-order');
        }*/

            if ($notice[0]['type'] == 2) {
                $data['notice_fields_section1'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.workorder_id', $id)->where('work_order_fields.notice_id', $notice_id)->where('notice_fields.section', 1)->orderBy('sort_order', 'asc')->get();

                $data['notice_fields_section2'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.notice_id', $notice_id)->where('work_order_fields.workorder_id', $id)->where('notice_fields.section', 2)->orderBy('sort_order', 'asc')->get();
                $data['tab'] = 'recipients';

                return view('customer.workorder.edit_soft_notice', $data);
            } else {
                $data['notice_fields'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.notice_id', $notice_id)->where('work_order_fields.workorder_id', $id)->orderBy('sort_order', 'asc')->get();
                //dd($data);
                $data['tab'] = 'recipients';
                return view('customer.workorder.edit', $data);
            }
       // } 
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $notice_id, $work_order_id) {

        //  $category   =   \App\Models\Recipient::select('category_id')->where('id',$id)->get();
        $result = \App\Models\Recipient::where('id', $id)->delete();
        if ($result) {
            Session::flash('success', 'Your work order # ' . $work_order_id . ' Record delete Successfully.');
        } else {
            Session::flash('success', 'Your work order # ' . $work_order_id . ' Problem in Record saving.');
        }
        /* foreach ($category as $key => $value) {
          $delete_category = \App\Models\Category::select()->where('id',$value->category_id)->delete();
          } */
        $notice = \App\Models\Notice::select('name', 'id', 'type','master_notice_id')->where('id', $notice_id)->get()->toArray();
        $data['notice'] = $notice[0];
        $data['cities'] = \App\Models\City::select()->get();
        $data['countries'] = \App\Models\Country::select()->get();
        $data['states'] = \App\Models\State::select()->orderBy('name', 'asc')->get();
        $data['recipients'] = \App\Models\Recipient::select()->where('work_order_id', $work_order_id)->get();
        $data['categories'] = \App\Models\Category::select()->orderBy('name', 'ASC')->get();
        $data['project_roles'] = \App\Models\ProjectRoles::select()->orderBy('name', 'ASC')->get();
        $data['count'] = \App\Models\Recipient::select(DB::raw('COUNT(*) AS total'), 'categories.name')->groupBy('recipients.category_id')->join('categories', 'categories.id', '=', 'recipients.category_id')->where('work_order_id', $work_order_id)->get();
        $data['id'] = $work_order_id;
        $data['notice_id'] = $notice_id;
        $data['officers_directors'] = \App\Models\CustomerAgent::where('customer_id', '=', Auth::user()->customer->id)
                ->get();
        $data['projectTypes'] = \App\Models\ProjectType::select()->get();
//        $result = \App\Models\WorkOrder::select('state_id')->join('notice_fields', 'notice_fields.id', '=', 'work_orders.notice_id')->where('work_orders.id', $work_order_id)->get()->toArray();
        $result = \App\Models\WorkOrder::select('state_id', 'customer_id')->join('notice_fields', 'notice_fields.notice_id', '=', 'work_orders.notice_id')->where('work_orders.id', $work_order_id)->limit(1)->get()->toArray();
        $work_orders_status = \App\Models\WorkOrder::find($work_order_id);
        if ($work_orders_status->status == 1) {
            $data['status'] = 'request';
        } else if ($work_orders_status->status == 0) {
            $data['status'] = 'draft';
        }
        $data['names'] = \App\Models\Contact::select('contacts.id', 'contacts.company_name')
                // ->where('contact_details.type', '=', 0)
                ->where('contacts.customer_id', '=', Auth::user()->customer->id)
                //  ->Join('contact_details', 'contact_details.contact_id', '=', 'contacts.id')
                ->orderBy('contacts.company_name', 'ASC')
                ->get();
        $data['attachment'] = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $work_order_id)
                ->get();
        $admin_email = \App\User::leftjoin('role_users', 'role_users.user_id', '=', 'users.id')
                ->where('role_users.role_id', '=', 2)
                ->select('users.email')
                ->first();
        $account_manager_email = \App\Models\Customer::leftjoin('users', 'users.id', '=', 'customers.account_manager_id')
                ->select('users.email')
                ->find(Auth::user()->customer->id);

        /* if (isset($account_manager_email) && !empty($account_manager_email)) {
          $data['note_emails'] = [
          $admin_email->email,
          $account_manager_email->email
          ];
          } else {
          $data['note_emails'] = [
          $admin_email->email
          ];

          } */
        $data['note_emails'] = get_user_notice_email($result[0]['customer_id']);

        $data['customer_details'] = Auth::user()->customer;
        $data['notes'] = \App\Models\WorkOrderNotes::leftjoin('users', 'users.id', '=', 'work_order_notes.user_id')->where('customer_id', '=', Auth::user()->customer->id)
                ->where('work_order_notes.work_order_id', '=', $work_order_id)
                ->where('work_order_notes.visibility', '=', 0)
                ->orderBy('work_order_notes.id', 'DESC')
                ->get();
        $data['corrections'] = \App\Models\WorkOrderCorrections::leftjoin('users', 'users.id', '=', 'work_order_corrections.user_id')->where('customer_id', '=', Auth::user()->customer->id)
                ->where('work_order_corrections.work_order_id', '=', $work_order_id)
                ->where('work_order_corrections.visibility', '=', 0)
                ->orderBy('work_order_corrections.id', 'DESC')
                ->get();
        
        /*if ($notice[0]['type'] == 2) {
            $data['notice_fields_section1'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.workorder_id', $work_order_id)->where('work_order_fields.notice_id', $notice_id)->where('notice_fields.section', 1)->orderBy('sort_order', 'asc')->get();

            $data['notice_fields_section2'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.notice_id', $notice_id)->where('work_order_fields.workorder_id', $work_order_id)->where('notice_fields.section', 2)->orderBy('sort_order', 'asc')->get();
            $data['tab'] = 'recipients';
            //To display parent work order
            foreach ($data['notice_fields_section1'] as $key => $value) {
                $key = explode("_", $key);

                if ($value) {

                    if ($value->name == 'parent_work_order') {
                        $parent_work_order = $value->value;
                        $data['parent_work_order'] = $parent_work_order;
                    }
                }
            }
            //return view('customer.workorder.edit_soft_notice', $data);
        } else {
            $data['notice_fields'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.notice_id', $notice_id)->where('work_order_fields.workorder_id', $work_order_id)->orderBy('sort_order', 'asc')->get();
            $data['tab'] = 'recipients';
            //To display parent work order
            foreach ($data['notice_fields'] as $key => $value) {
                $key = explode("_", $key);

                if ($value) {

                    if ($value->name == 'parent_work_order') {
                        $parent_work_order = $value->value;
                        $data['parent_work_order'] = $parent_work_order;
                    }
                }
            }
           // return view('customer.workorder.edit', $data);
        }*/
        return redirect()->back();
    }

    public function getParentOrderId(Request $request) {
        $ParentWorkOrders = \App\Models\WorkOrder::select('id')->get();
        return $ParentWorkOrders;
    }

    public function store_attachments(Request $request) {
        $file = $request->file('file');
        if (!empty($file)) {
            $extension = $request->file('file')->getClientOriginalExtension(); // getting excel extension
            $original_file_name = $request->file('file')->getClientOriginalName();
            if (!file_exists(public_path() . '/attachment/work_order_document')) {
                $dir = mkdir(public_path() . '/attachment/work_order_document', 0777, true);
            } else {
                $dir = public_path() . '/attachment/work_order_document/';
            }

            $dir = public_path() . '/attachment/work_order_document/';
            $filename = uniqid() . '_' . time() . '_' . date('Ymd') . '.' . $extension;
            $request->file('file')->move($dir, $filename);
        } else {
            $filename = "";
            $original_file_name = "";
            $extension = "";
        }
        $data = [];
        $doc_val = "";
        if ($request->doc_value != "") {
            $doc_val = $request->doc_value;
        }
        if (Auth::user()->hasRole('account-manager')) {
            $data['db_arr'] = [
                $request->document_type,
                $doc_val,
                $filename,
                $original_file_name,
                $request->visibility,
                $extension
            ];
        } else {
            $data['db_arr'] = [
                $request->document_type,
                $doc_val,
                $filename,
                $original_file_name,
                $extension
            ];
        }
        if ($request->document_type == 1) {
            $request->document_type = 'Bond';
        } elseif ($request->document_type == 2) {
            $request->document_type = 'Notice to Commencement';
        } elseif ($request->document_type == 3) {
            $request->document_type = 'Permit';
        } elseif ($request->document_type == 4) {
            $request->document_type = 'Contract';
        } elseif ($request->document_type == 5) {
            $request->document_type = 'Invoice';
        } elseif ($request->document_type == 6) {
            $request->document_type = 'Final Release';
        } elseif ($request->document_type == 7) {
            $request->document_type = 'Partial Release';
        } elseif ($request->document_type == 8) {
            $request->document_type = 'Misc Document';
        } elseif ($request->document_type == 9) {
            $request->document_type = 'Signed Document';
        } elseif ($request->document_type == 10) {
            $request->document_type = 'Folio';
        }elseif ($request->document_type == 11) {
            $request->document_type = 'COL Signed';
        }elseif ($request->document_type == 12) {
            $request->document_type = 'Recorded COL/SCOL';
        }elseif ($request->document_type == 13) {
            $request->document_type = 'Pending Signature';
        }

        $data['ajax_arr'] = [
            $request->document_type,
            $doc_val,
            $original_file_name,
            date('d M y h:i A'),
            $extension
        ];


        return json_encode($data);
    }
    //edit attachment
    public function edit_attachments(Request $request) {
        

        $file = $request->file('file');

        if (!empty($file)) {
            $extension = $request->file('file')->getClientOriginalExtension(); // getting excel extension
            $original_file_name = $request->file('file')->getClientOriginalName();
            if (!file_exists(public_path() . '/attachment/work_order_document')) {
                $dir = mkdir(public_path() . '/attachment/work_order_document', 0777, true);
            } else {
                $dir = public_path() . '/attachment/work_order_document/';
            }

            $dir = public_path() . '/attachment/work_order_document/';
            $filename = uniqid() . '_' . time() . '_' . date('Ymd') . '.' . $extension;
            $request->file('file')->move($dir, $filename);
        } else { 
            if(!empty($request->file_name) && $request->file_original_name!='undefined'  && $request->extension!='undefined'){
            $filename = $request->file_name;
            $original_file_name = $request->file_original_name;
            $extension = $request->extension;
            }else{
            $filename = "";
            $original_file_name = "";
            $extension = "";
            }
        }
        $data = [];
        $doc_val = "";
        if ($request->doc_value != "") {
            $doc_val = $request->doc_value;
        }
        $document_type_id = $request->document_type; 
        if (Auth::user()->hasRole('account-manager')) {
            $data['db_arr'] = [
                $request->document_type,
                $doc_val,
                $filename,
                $original_file_name,
                $request->visibility
            ];
        } else {
            $data['db_arr'] = [
                $request->document_type,
                $doc_val,
                $filename,
                $original_file_name
            ];
        }
        if ($request->document_type == 1) {
            $request->document_type = 'Bond';
        } elseif ($request->document_type == 2) {
            $request->document_type = 'Notice to Commencement';
        } elseif ($request->document_type == 3) {
            $request->document_type = 'Permit';
        } elseif ($request->document_type == 4) {
            $request->document_type = 'Contract';
        } elseif ($request->document_type == 5) {
            $request->document_type = 'Invoice';
        } elseif ($request->document_type == 6) {
            $request->document_type = 'Final Release';
        } elseif ($request->document_type == 7) {
            $request->document_type = 'Partial Release';
        } elseif ($request->document_type == 8) {
            $request->document_type = 'Misc Document';
        } elseif ($request->document_type == 9) {
            $request->document_type = 'Signed Document';
        } elseif ($request->document_type == 10) {
            $request->document_type = 'Folio';
        }elseif ($request->document_type == 11) {
            $request->document_type = 'COL Signed';
        }elseif ($request->document_type == 12) {
            $request->document_type = 'Recorded COL/SCOL';
        }elseif ($request->document_type == 13) {
            $request->document_type = 'Pending Signature';
        }

        $data['ajax_arr'] = [
            $request->document_type,
            $doc_val,
            $original_file_name,
            date('d M y h:i A'),
            $extension
        ];
//dd($request->all(),$data['db_arr']);
        //dd($request->work_order_id);
            $remove_file = $request->remove_file;
            if($request->work_order_id == 'null'){
                $request->request->set('work_order_id',NULL);
            }
           // dd($request->all());
        if($request->doc_id==0){
            $attachments = new \App\Models\WorkOrderAttachment;
            $attachments->work_order_id = $request->work_order_id;
            $attachments->type = $data['db_arr']['0'];
            $attachments->title =  $data['db_arr']['1'];
            //if (!empty($file)) {
            $attachments->file_name = $data['db_arr']['2'];
            $attachments->original_file_name = $data['db_arr']['3'];
            
            if($remove_file==1 && empty($file)){
             $attachments->file_name = "";   
             $attachments->original_file_name = "";
            }
            if (Auth::user()->hasRole('account-manager')) {
            $attachments->visibility = $request->visibility;
            }else{
            $attachments->visibility = 0;
            }
            $attachments->save();

            /*$previous_attachments = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $parent_work_order)->get();*/
        }else{
            $attachments = \App\Models\WorkOrderAttachment::find($request->doc_id);
            $attachments->work_order_id = $request->work_order_id;
            $attachments->type = $data['db_arr']['0'];
            $attachments->title =  $data['db_arr']['1'];
            if (!empty($file)) {
            $attachments->file_name = $data['db_arr']['2'];
            $attachments->original_file_name = $data['db_arr']['3'];
            }else if(!empty($request->file_name) && $request->file_original_name!='undefined'  && $request->extension!='undefined'){
            $attachments->file_name = $data['db_arr']['2'];
            $attachments->original_file_name = $data['db_arr']['3']; 
            }
            if($remove_file==1 && empty($file)){
             $attachments->file_name = "";   
             $attachments->original_file_name = "";
            }
            if (Auth::user()->hasRole('account-manager')) {
            $attachments->visibility = $request->visibility;
            }else{
            $attachments->visibility = 0;
            }
            $attachments->save();
        }
        if(!empty($attachments->original_file_name)){
        $extension = explode('.', $attachments->original_file_name);
        $extension = $extension[1]; 
        }else{
            $extension = "";
        }
        $data['ajax_arr'] = [
            $request->document_type,
            $doc_val,
            //$attachments->file_name,
            $attachments->original_file_name,
            date('d M y h:i A'),
            $extension
        ];
       if (Auth::user()->hasRole('account-manager')) {
            $data['db_arr'] = [
                $attachments->type,
                $doc_val,
                $attachments->file_name,
                $attachments->original_file_name,
                $request->visibility,
                $attachments->id,
                $document_type_id,
                $extension
            ];
        } else {
            $data['db_arr'] = [
                $attachments->type,
                $doc_val,
                $attachments->file_name,
                $attachments->original_file_name,
                $attachments->id,
                $document_type_id,
                $extension
            ];
        }

        return json_encode($data);
    }

    /**
     * Function is used to store note
     */
    public function storeNote(Request $request) {

        $rules = ['note' => 'required',
        ];
        if (Auth::user()->hasRole('customer')) {
            $rules = ['email' => 'required', 'note' => 'required',];
        }
        $validator = \Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }
        $workOrder = WorkOrder::find($request->work_order);
        $result = \App\Models\WorkOrderNotes::create([
                    'work_order_id' => $request->work_order,
                    'customer_id' => $workOrder->customer_id,
                    'note' => $request->note,
                    'email' => ($request->email) ? $request->email : NULL,
                    'visibility' => ($request->visibility) ? $request->visibility : 0,
                    'user_id'  => Auth::user()->id,
        ]);
        $notes = \App\Models\WorkOrderNotes::leftjoin('users', 'users.id', '=', 'work_order_notes.user_id')->where('customer_id', '=', $workOrder->customer_id)
                ->select('work_order_notes.note', 'work_order_notes.email','users.name')
                ->where('work_order_notes.id', '=', $result->id)
                ->first();
        /*$senderName = \App\User::select('user.name as user_name')->join('customer','user.id','=','customer.user_id')->where('customer.id','=',$workOrder->customer_id)->get();*/
        /*$senderName = \App\Models\WorkOrderNotes::leftjoin('users', 'users.id', '=', 'work_order_notes.user_id')->select('users.name')->first();*/
       
        //$senderName = $senderName->name;

        // $correction->doc_id = $request->doc_id;

        if ($request->email != NULL) {
           Mail::to($request->email)->send(new WorkOrderNote($result));
        }
        if ($result) {
            return response()->json(['success' => 'Note submitted successfully', 'notes' => $notes]);
        }
    }

    /**
     * Function is used to store correction
     */
    public function storeCorrection(Request $request) {
        $rules = ['correction' => 'required'
        ];
        if (Auth::user()->hasRole('customer')) {
            $rules = ['email' => 'required', 'correction' => 'required',];
        }
        $validator = \Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }
        $workOrder = WorkOrder::find($request->work_order);
        $result = \App\Models\WorkOrderCorrections::create([
                    'work_order_id' => $request->work_order,
                    'customer_id' => $workOrder->customer_id,
                    'correction' => $request->correction,
                    'email' => ($request->email) ? $request->email : NULL,
                    'visibility' => ($request->visibility) ? $request->visibility : 0,
                    'user_id'  => Auth::user()->id,
        ]);
        $corrections = \App\Models\WorkOrderCorrections::leftjoin('users', 'users.id', '=', 'work_order_corrections.user_id')->where('customer_id', '=', $workOrder->customer_id)
                ->select('work_order_corrections.correction', 'work_order_corrections.email','users.name')
                ->where('work_order_corrections.id', '=', $result->id)
                ->first();

       /* $senderName = \App\Models\Customer::leftjoin('users', 'users.id', '=', 'customers.user_id')
                            ->select('users.name')
                            ->where('customers.id', '=',$workOrder->customer_id)
                            ->first();*/

        // $correction->doc_id = $request->doc_id;
        if ($request->email != NULL) {
            Mail::to($request->email)->send(new \App\Mail\WorkOrderCorrection($result));
        }
        if ($result) {
            return response()->json(['success' => 'Correction submitted successfully', 'corrections' => $corrections]);
        }
    }

    public function getHardNotices(Request $request) {
        $notices = \App\Models\Notice::select()->where('type', 1)->where('state_id', $request->state_id)->where('status', '1')->orderby('notice_sequence', 'ASC')->get();
        return $notices;
    }

    public function getSoftNotices(Request $request) {
        $notices = \App\Models\Notice::select()->where('type', 2)->where('state_id', $request->state_id)->where('status', '1')->orderby('notice_sequence', 'ASC')->get();
        return $notices;
    }

    /**
     * Function is used to delete attachment
     */
    public function deleteAttachment(Request $request) {
        if ($request->file != "") {
            if (file_exists(public_path('attachment/work_order_document/' . $request->file))) {
                unlink(public_path('attachment/work_order_document/' . $request->file));
            }
        }
//        return redirect::back();
        return response()->json(['success' => 1]);
    }

    public function deleteParentAttachment(Request $request) {

        if ($request->file != "" && file_exists(public_path('attachment/work_order_document/parent_document/' . $request->file))) {
            unlink(public_path('attachment/work_order_document/parent_document/' . $request->file));
        }

        return response()->json(['success' => 1]);
    }

    public function deleteEditDocAttachment(Request $request) {

        $workorderAttachment = \DB::table('work_order_attachments')->find($request->id);

        $workorderAttachment = \DB::table('work_order_attachments')
                ->where('id', '=', $request->id);
        if ($request->file != "") {
            if (file_exists(public_path('attachment/work_order_document/' . $request->file))) {

                unlink(public_path('attachment/work_order_document/' . $request->file));
            }
        }
        $result = $workorderAttachment->delete();


//        return redirect::back();
        return response()->json(['success' => $result]);
    }

    function updateStatus($id, $status) {
        $result = \App\Models\WorkOrder::find($id);
        if ($status == '0') {
            $result->status = 0;
            $result->save();
            Session::flash('success', 'Your work order # ' . $id . ' has been Saved Successfully <a href="' . url('customer/work-order/view/' . $id) . '">Click here to print.</a>');
            return redirect('customer/view-work-order');
        } else if ($status == '1') {
            /* $Fields = \App\Models\WorkOrder::find($id);

              $notice_Field_ids = \App\Models\NoticeField::select()->where('notice_id',$Fields->notice_id)->where('is_required',1)->get()->toArray();
              $count=0;
              foreach ($notice_Field_ids as $notice_field_id)
              {
              // return $notice_field_id;
              $WorkOrderFields = \App\Models\WorkOrderFields::select('value')->where('workorder_id',$id)->where('notice_field_id',$notice_field_id['id'])->get()->toArray();

              if($WorkOrderFields[0]['value']=='')
              {
              $count = $count+1;
              }
              }

              if($count == 0)
              {
              $result->status = 1;
              }
              else
              {
              $result->status = 0;
              }
              $result; */
            $result->status = '1';
            $result->save();
            Session::flash('success', 'New Work Order Created Successfully. Your work order no is #' . $id . '. <a href="' . url('customer/work-order/view/' . $id) . '">Click here to print.</a>');
            return redirect('home');
        }
    }

    /**
     * Function is used to when click on edit recipient get data on recipient page
     */
    public function getRecipientData(Request $request) {
        $recipient_id = $request->id;

        $recipient = \App\Models\Recipient::with('uspsAddress')->find($recipient_id);
        
        if (isset($recipient) && !empty($recipient)) {
            if (!empty($recipient->city_id)) {
                $city_name = \App\Models\City::where('id', '=', $recipient->city_id)
                        ->select('name')
                        ->first();
                $recipient->city_name = $city_name->name;
            } else {
                $recipient->city_name = "";
            }
            if(!empty($recipient->uspsAddress)) {
                $recipient->address2 = $recipient->uspsAddress->address2;
                $recipient->address3 = $recipient->uspsAddress->address3;
            }
        }
        unset($recipient->uspsAddress);
        return \Response::json(['success' => true, 'recipient' => $recipient
        ]);
    }

}
