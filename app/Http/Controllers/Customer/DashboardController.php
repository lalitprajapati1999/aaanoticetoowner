<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\State;
use App\Models\Notice;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use App\Models\NoticeTemplate;
use Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use App\Mail\Contactus;
use App\Http\Controllers\QuickBooksController;

/**
 * Description of DeadlineCalculatorController
 *
 * @author neosoft
 */
class DashboardController extends Controller {

    public function index() {
        $data['states'] = State::select('id', 'name')->orderBy('name')->get();
        // $data['notices'] = Notice::select('id','name')->where('status',1)->get();
        return view('customer.deadline_calculator', $data);
    }

    public function getNotices(Request $request) {
        $notices = NoticeTemplate::leftjoin('notices', 'notices.id', '=', 'notice_templates.notice_id')
                ->select('notices.id', 'notices.name')
                ->where('notice_templates.state_id', '=', $request->state)
                ->where('notices.status', 1)
                ->where('allow_deadline_calculator', 1)
                ->get();

        return $notices;
    }

    public function calculate_date(Request $request) {
        //Validate Data
        $validation = Validator::make(Input::all(), [
                    'state' => 'required| exists:states,id',
                    'document_type' => 'required | exists:notices,id',
                    'month' => 'required',
                    'day' => 'required',
                    'year' => 'required'
        ]);

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation)->withInput();
        }

        $input = $request->all();
        //dd($input);

        $day = $input['day'];
        $month = $input['month'];
        $year = $input['year'];
        $state_id = $input['state'];
        $document_id = $input['document_type'];

        //Days Required to calculate end date
        $result = NoticeTemplate::where('state_id', '=', $state_id)
                ->where('notice_id', '=', $document_id)
                ->select('no_of_days')
                ->first();

        if ($result) {
            $days = $result->no_of_days;
        }
        //Get State Name
        $state = State::where('id', $state_id)->select('name')->first();
        $data['state'] = $state;

        //Get Document Type
        $document_type = Notice::where('id', $document_id)->select('name')->first();
        $data['document_type'] = $document_type;
        $data['days'] = $days;

        //Start Date
        $data['job_start_date'] = $input['month'] . '/' . $input['day'] . '/' . $input['year'];
        //  dd($data['job_start_date']);
        //Calculate end date
        $start_date = $input['year'] . '-' . $input['month'] . '-' . $input['day'];
        $dayStr = $days == 1 ? 'day' : 'days';
        $end_date = date('m/d/Y', strtotime('+ ' . $days . ' ' . $dayStr, strtotime($start_date)));

        $data['last_day_on_job'] = $end_date;
        $data['due_date'] = $end_date;
        $data['document_id'] = $document_id;
        return view('customer.deadline', $data);
    }

    public function getlienblogs() {
        $data['lien_blogs'] = \App\Models\LienBlog::where('status', '=', 1)
                                        ->orderBy('id','DESC')
                ->get();

        return view('customer.lien_laws', $data);
    }
    
     public function viewcontactus(){
        $data['logged_in_user'] = Auth::user();
       
        return view('customer.customer_contactus',$data);
    }
    
    public function storeContactUs(Request $request){
        //dd($request->all());
          $validation = Validator::make(Input::all(), [
                    'name' => 'required',
                    'email' => 'required|email',
                    'contact' => 'required',
                    'query' => 'required'
                   
        ],[
            'contact.required' => 'Telephone is required',
            'query.required' => 'Query is required'
        ]);

        if ($validation->fails()) {
            return redirect()->back()->withErrors($validation)->withInput();
        }
          $input = $request->all();
        
        $contactusObj = new \App\Models\ContactUs();
        $contactusObj->name = $input['name'];
        $contactusObj->email = $input['email'];        
        $contactusObj->phone = $input['contact'];
        $contactusObj->query = $input['query'];
        $contactusObj->customer_id = Auth::user()->customer->id;
        $contactusObj->save();
      
        // $contactusdetails = \App\Models\ContactUs::find($contactusObj->id);
        Mail::to(config('constants.TRANSACTION_EMAILS.SALES_EMAIL'))->send(new Contactus($contactusObj));
         
        return redirect::to('customer/contact-us')->with('success', 'Thanks for contacting us! We will be in touch with you shortly.');
    }
    
    public function checkCreditCard(Request $request)
    {      
        $validator = Validator::make($request->all(),[
            'card_number' => 'required|ccn',
            'credit_card_date' => 'required|ccd',
            'security_code' => 'required|cvc',
        ],
        [
            'card_number.ccn' => 'Invalid card number',
            'credit_card_date.ccd' => 'Invalid card date',
            'security_code.cvc' =>'Invalid security code'
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        else
        {
            $qbo_customer_id = Auth::user()->qbo_customer_id;
            $customer_id = Auth::user()->id;
            
            // store card for customer in quickbooks            
            if($qbo_customer_id != 0)
            {
                $expiry_date = explode('/',$request->credit_card_date);
                $expiry_month = $expiry_date[0];
                $expiry_year = $expiry_date[1];
                $card_number = str_replace(' ','',$request->card_number);
                $card_details = ['name'=>$request->credit_card_name,'number'=>$card_number,'expMonth'=>$expiry_month,'expYear'=>$expiry_year];
                
                $qbo = new QuickBooksController();       
                $qbo_response = $qbo->generateTokens(config('constants.QBO.PAYMENT_SCOPE'),'addCard',['customer_id'=>$customer_id,'qbo_customer_id'=>$qbo_customer_id,'card_details'=>$card_details]);
                return redirect('admin/edit-account-info');
            }
            else
            {
                return redirect('admin/edit-account-info')->with('error','You are not registered on quickbooks');
            }
        }
    }
}
