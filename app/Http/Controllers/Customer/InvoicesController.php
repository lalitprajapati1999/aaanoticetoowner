<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\OAuth2InvoiceResponse;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Response;
use Auth;

class InvoicesController extends Controller
{
    public function __construct(OAuth2InvoiceResponse $OAuth2InvoiceResponse)
    {    
        $this->OAuth2InvoiceResponse = $OAuth2InvoiceResponse;
        $this->arr_view_data = [];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('customer.statement_invoice.statement_invoices');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function addCard(Request $request)
    {

    }

    /*Show invoices listing*/
    public function listing()
    {
        $this->arr_view_data['page_title'] = 'Invoices';

        return view('customer.statement_invoices.listing',$this->arr_view_data);        
    }

    public function getInvoices(Request $request) {

        $invoices = $this->OAuth2InvoiceResponse->where('qbo_user_id',Auth::user()->qbo_customer_id)->get();       

        return Datatables::of($invoices)                        
                        ->filter(function ($instance) use ($request) {})
                        ->make(true);
    }

    /* show invoice details*/
    public function view($encInvoiceId = null)
    {
        $invoice_arr = [];
        
        $invoiceId = base64_decode($encInvoiceId);

        $invoiceObj = $this->OAuth2InvoiceResponse->where('id',$invoiceId)->first();

        if($invoiceObj)
        {
            $invoice_arr = $invoiceObj->toArray();
        }

        $billing_address = json_decode($invoice_arr['bill_addr']);
        $billing_address = json_decode(json_encode($billing_address),1);
        $other_details_arr = json_decode($invoice_arr['other_details']);
        $other_details_arr = json_decode(json_encode($other_details_arr),1);
        // dd($invoice_arr);

        $this->arr_view_data['other_details_arr'] = $other_details_arr;
        $this->arr_view_data['billing_address'] = $billing_address;
        $this->arr_view_data['invoice_arr']     = $invoice_arr;
        $this->arr_view_data['page_title']      = 'Invoice Details';
        
        $html = \View::make('customer.statement_invoices.view', $this->arr_view_data)->render();

        return Response::json(['html' => $html]);

        // return view('customer.statement_invoices.view',$this->arr_view_data);
    }
}
