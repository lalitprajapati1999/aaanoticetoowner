<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\WorkOrderNotes;
use Illuminate\Support\Facades\Mail;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;
use App\Models\WorkOrder, App\Models\WorkOrderFields, App\Models\NoticeField;
use Auth;
use DB;
use Alert;
use to_date;
use Collection;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use PDF;

class ViewWorkOrderController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        
        $customer_id = Auth::User()->customer->id;
        $data['account_managers'] = \App\Models\User::select('id', 'name')->join('role_users', 'role_users.user_id', 'users.id')->where('role_users.role_id', 3)->orderBy('name', 'ASC')->get();
        $data['categories'] = \App\Models\Category::select()->orderBy('name', 'ASC')->get();
        $data['project_roles'] = \App\Models\ProjectRoles::select()->orderBy('name', 'ASC')->get();
        
        $data['contracted_by'] = \App\Models\Contact::select()->where('customer_id', Auth::user()->customer->id)->groupBy('company_name')->orderBy('company_name', 'ASC')->get();
        
        $data['customers'] = \App\Models\Customer::join('users', 'users.id', '=', 'customers.user_id')->orderBy('name', 'ASC')->get();
        $data['WorkOrders'] = \App\Models\WorkOrder::select()->where('customer_id', $customer_id)->paginate(10);
        $data['WorkOrderFields'] = \App\Models\WorkOrderFields::select()->join('notice_fields', 'notice_fields.id', '=', 'work_order_fields.notice_field_id')->join('work_orders', 'work_orders.id', '=', 'work_order_fields.workorder_id')->where('notice_fields.name', 'project_address')->where('work_orders.customer_id', $customer_id)->get();
        $data['states'] = \App\Models\State::select()->orderBy('name')->get();
        $data['note_emails'] = get_customer_user_notice_email(Auth::user()->customer->id);
        return view('customer.work_order_histry.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
//
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_recipients(Request $request, $id, $notice_id) {
        if(!is_numeric($request->city_id)){ 
            $city = \App\Models\City::firstOrNew(array('name' => $request->city_id));
            //$city = new \App\Models\City();
            $city->name = $request->city_id;
            $city->state_id = $request->state_id;
            $city->zip_code = $request->zip;
            $city->save();
            $request->request->set('city_id',$city->id);
        }

        $data['customer_details'] = Auth::user()->customer;


        /* $category = \App\Models\Category::create([
          'name' => $request->category_id]); */
//$workorder_id = \App\Models\WorkOrder::orderBy('created_at', 'desc')->first();
// $request->request->set('category_id',$category->id);
        $request->request->set('work_order_id', $id);
        //$request->request->set('city_id', $request->recipient_city_id);
        $request->request->set('city_id', $request->city_id);
        $contracted_by = $request->name;
        $contact_id = $request->recipt_id;
        if ($contracted_by != "") {
            /* check contracted by is exist or not */
            $contracted_by_check = \App\Models\Contact::where('company_name', $contracted_by)->first();
            if (empty($contracted_by_check)) {
                $contact = \App\Models\Contact::create([
                            'customer_id' => Auth::user()->customer->id,
                            'company_name' => $contracted_by,
                            'company_address'=>$request->address,
                            'mailing_address'=>$request->address,
                            'city_id'=>$request->city_id,
                            'state_id'=>$request->state_id,
                            'zip'=>$request->zip,
                            'phone'=>$request->contact,
                            'email'=>$request->email,
                            'attn'=>$request->attn,
                ]);
                $contact_id = $contact->id;
            }
        }
        $request->request->set('contact_id', $contact_id);
        $first_category = \App\Models\Category::where('name', 'Contracted By')->first();
        if ($first_category->id == $request->category_id) {
            /* check contract details exit or not */
            $check_contract_recipient_exist = \App\Models\Recipient::where(['category_id' => $first_category->id, 'work_order_id' => $request->work_order_id])->where('id', '!=', $request->recipient_id)->first();

            /* if not empty delete previouse record */
            if (!empty($check_contract_recipient_exist)) {
                $contract_recipient_delete = \App\Models\Recipient::where(['category_id' => $request->category_id, 'work_order_id' => $request->work_order_id])->where('id', '!=', $request->recipient_id)->delete();
            }
        }

        if ($request->continue == 'add') {
            $Recipient = \App\Models\Recipient::create(
                            $request->except(['_token', 'country_id']));
            if ($Recipient) {
                Session::flash('success', 'Your work order # ' . $id . ' recipient has been submitted.');
            } else {
                Session::flash('success', 'Your work order # ' . $id . '  problem in record submitting.');
            }
        } else {
            $workorderRecipient = \App\Models\Recipient::find($request->recipient_id);
            $workorderRecipient->category_id = $request->category_id;
            $workorderRecipient->name = $request->name;
            $workorderRecipient->contact = $request->contact;
            $workorderRecipient->address = $request->address;
            $workorderRecipient->city_id = $request->city_id;
            $workorderRecipient->state_id = $request->state_id;
            $workorderRecipient->zip = $request->zip;
            $workorderRecipient->attn = $request->attn;
            $workorderRecipient->email = $request->email;
            $workorderRecipient->contact_id = $request->contact_id;
            $workorderRecipient->save();
            if ($workorderRecipient) {
                Session::flash('success', 'Your work order # ' . $id . ' recipient successfully updated.');
                $work_orders = \App\Models\WorkOrderFields::select('workorder_id')->join('notice_fields','work_order_fields.notice_field_id','notice_fields.id')->join('work_orders','work_orders.id','work_order_fields.workorder_id')->where('notice_fields.name','parent_work_order')->whereNotIn('work_orders.status',[5,6])->where('work_order_fields.value',$id)->get()->toArray();
             
              $parent_work_order_recipients = \App\Models\Recipient::select()->where('work_order_id',$id)->where('category_id','!=',21)->get()->toArray();

              if(isset($work_orders) && count($work_orders) > 0) {

              foreach ($work_orders As $k => $work_order) {

                $delete_previous_recipients = \App\Models\Recipient::where('work_order_id',$work_order['workorder_id'])->where('parent_work_order',$id)->delete();
                if (isset($parent_work_order_recipients) && count($parent_work_order_recipients) > 0) {
                          foreach ($parent_work_order_recipients As $k => $each_parent_recipient) {
                            $usps_recipient = \App\Models\UspsAddress::select()->where('recipient_id',$each_parent_recipient['id'])->get()->toArray();
                            
                            $save_recipients = \App\Models\Recipient::create([
                                                        'work_order_id' => $work_order['workorder_id'],
                                                        'category_id' => $each_parent_recipient['category_id'],
                                                        'name' => $each_parent_recipient['name'],
                                                        'contact' => $each_parent_recipient['contact'],
                                                        'address' => $each_parent_recipient['address'],
                                                        'city_id' => $each_parent_recipient['city_id'],
                                                        'attn' => $each_parent_recipient['attn'],
                                                        'state_id' => $each_parent_recipient['state_id'],
                                                        'zip' => $each_parent_recipient['zip'],
                                                        'email' => $each_parent_recipient['email'],
                                                        'attn' => $each_parent_recipient['attn'],
                                                         'parent_work_order'=> $id,

                                            ]);
                          // dd($usps_recipient);
                          if(!empty($save_recipients) && !empty($usps_recipient)){
                             $usps_address = [
                                'fullname' => $usps_recipient[0]['fullname'], 
                                'company' => $usps_recipient[0]['company'], 
                                'department' => $usps_recipient[0]['department'], 
                                'address' => $usps_recipient[0]['address'], 
                                'address2' => $usps_recipient[0]['address2'], 
                                'address3' => $usps_recipient[0]['address3'], 
                                'city' => $usps_recipient[0]['city'], 
                                'state' => $usps_recipient[0]['state'], 
                                'zipcode' => $usps_recipient[0]['zipcode'], 
                                'zipcode_add_on' => $usps_recipient[0]['zipcode_add_on'], 
                                'country' => $usps_recipient[0]['country'], 
                                'dpb' => $usps_recipient[0]['dpb'], 
                                'check_digit' => $usps_recipient[0]['check_digit'], 
                                'comment' => $usps_recipient[0]['comment'], 
                                'override_hash' => $usps_recipient[0]['override_hash'],
                                'created_at' => date("Y-m-d H:i:s"),
                                'updated_at' => date("Y-m-d H:i:s"),
                            ];

                            $usps_address['recipient_id'] = $save_recipients->id;
                            \App\Models\UspsAddress::insert($usps_address);
                          }

                    }
                  }
               }
             }

            } else {
                Session::flash('success', 'Your work order # ' . $id . '  problem in record submitting.');
            }
        }
        $notice = \App\Models\Notice::select('name', 'id', 'type','master_notice_id')->where('id', $notice_id)->get()->toArray();
        $data['notice'] = $notice[0];
        $data['cities'] = \App\Models\City::select()->get();
        $data['countries'] = \App\Models\Country::select()->get();
        $data['categories'] = \App\Models\Category::select()->orderBy('name', 'ASC')->get();
        $data['project_roles'] = \App\Models\ProjectRoles::select()->orderBy('name', 'ASC')->get();
        $data['states'] = \App\Models\State::select()->orderBy('name', 'asc')->get();
        $data['recipients'] = \App\Models\Recipient::select()->where('work_order_id', $id)->get();
        $data['officers_directors'] = \App\Models\CustomerAgent::where('customer_id', '=', Auth::user()->customer->id)
                ->get();
        $data['projectTypes'] = \App\Models\ProjectType::select()->get();
        $data['count'] = \App\Models\Recipient::select(DB::raw('COUNT(*) AS total'), 'categories.name')->groupBy('recipients.category_id')->join('categories', 'categories.id', '=', 'recipients.category_id')->where('work_order_id', $id)->get();
        $data['names'] = \App\Models\Contact::select('contacts.id', 'contacts.company_name')
// ->where('contact_details.type', '=', 0)
                ->where('contacts.customer_id', '=', Auth::user()->customer->id)
//  ->Join('contact_details', 'contact_details.contact_id', '=', 'contacts.id')
                ->orderBy('contacts.company_name', 'ASC')
                ->get();
        $data['attachment'] = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $id)
                ->get();

        /*  $data['count']      = \App\Models\Category::select(DB::raw('COUNT(*) AS total'),'name')->groupBy('name')->get(); */
//dd($data['count']);
        $data['id'] = $id;
        $data['notice_id'] = $notice_id;
//                $result = \App\Models\WorkOrder::select('state_id')->join('notice_fields','notice_fields.id','=','work_orders.notice_id')->where('work_orders.id',$id)->get()->toArray();

        $result = \App\Models\WorkOrder::select('state_id', 'customer_id')->join('notice_fields', 'notice_fields.notice_id', '=', 'work_orders.notice_id')->where('work_orders.id', $id)->limit(1)->get()->toArray();
        $work_order_status = \App\Models\WorkOrder::find($id);
        if ($work_order_status->status == 1) {
            $data['status'] = 'request';
        } else if ($work_order_status->status == 0) {
            $data['status'] = 'draft';
        }
        $admin_email = \App\User::leftjoin('role_users', 'role_users.user_id', '=', 'users.id')
                ->where('role_users.role_id', '=', 2)
                ->select('users.email')
                ->first();
        $account_manager_email = \App\Models\Customer::leftjoin('users', 'users.id', '=', 'customers.account_manager_id')
                ->select('users.email')
                ->find(Auth::user()->customer->id);

        /*  if (isset($account_manager_email) && !empty($account_manager_email)) {
          $data['note_emails'] = [
          $admin_email->email,
          $account_manager_email->email
          ];
          } else {
          $data['note_emails'] = [
          $admin_email->email
          ];

          } */
        $data['note_emails'] = get_user_notice_email($work_order_status->customer_id);
        $data['notes'] = \App\Models\WorkOrderNotes::leftjoin('users', 'users.id', '=', 'work_order_notes.user_id')
                ->select('work_order_notes.*','users.name')
                ->where('customer_id', '=', Auth::user()->customer->id)
                ->where('work_order_notes.work_order_id', '=', $id)
                ->where('work_order_notes.visibility', '=', 0)
                ->orderBy('work_order_notes.id', 'DESC')
                ->get();
        $data['corrections'] = \App\Models\WorkOrderCorrections::leftjoin('users', 'users.id', '=', 'work_order_corrections.user_id')
                ->select('work_order_corrections.*','users.name')
                ->where('customer_id', '=', Auth::user()->customer->id)
                ->where('work_order_corrections.work_order_id', '=', $id)
                ->where('work_order_corrections.visibility', '=', 0)
                ->orderBy('work_order_corrections.id', 'DESC')
                ->get();

        if ($notice[0]['type'] == 2) {
            $data['notice_fields_section1'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.workorder_id', $id)->where('work_order_fields.notice_id', $notice_id)->where('notice_fields.section', 1)->orderBy('sort_order', 'asc')->get();
            $data['notice_fields_section2'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.notice_id', $notice_id)->where('work_order_fields.workorder_id', $id)->where('notice_fields.section', 2)->orderBy('sort_order', 'asc')->get();
            $data['tab'] = 'recipients';
            foreach ($data['notice_fields_section1'] as $key => $value) {
                $key = explode("_", $key);

                if ($value) {

                    if ($value->name == 'parent_work_order') {
                        $parent_work_order = $value->value;
                        $data['parent_work_order'] = $parent_work_order;
                    }
                }
            }
          //  return view('customer.work_order_histry.edit_soft_notices', $data);
        } else {
            $data['notice_fields'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.notice_id', $notice_id)->where('work_order_fields.workorder_id', $id)->orderBy('sort_order', 'asc')->get();
            $data['tab'] = 'recipients';
            //To display parent work order
            foreach ($data['notice_fields'] as $key => $value) {
                $key = explode("_", $key);

                if ($value) {

                    if ($value->name == 'parent_work_order') {
                        $parent_work_order = $value->value;
                        $data['parent_work_order'] = $parent_work_order;
                    }
                }
            }
           // return view('customer.work_order_histry.edit', $data);
        }
        return redirect()->back();
    }

    public function store(Request $request) {
//
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
//
    }

//date range custom filter
    public function getCustomeFilterData(Request $request) {//dd($request->all());
        $WorkOrderFields = \App\Models\WorkOrderFields::select('work_orders.parent_id', 'work_orders.created_at', 'work_orders.customer_id', 'work_orders.notice_id as notice_id', 'work_orders.status', 'work_orders.user_id', 'work_orders.is_rescind', 'work_orders.account_manager_id', 'work_order_fields.workorder_id', 'work_order_fields.notice_id', DB::raw("(GROUP_CONCAT(name SEPARATOR '||')) as `field_names`"), DB::raw("(GROUP_CONCAT(value SEPARATOR '||')) as `field_values`"))->join('notice_fields', 'notice_fields.id', '=', 'work_order_fields.notice_field_id')->join('work_orders', 'work_orders.id', '=', 'work_order_fields.workorder_id')->groupBy('work_order_fields.workorder_id', 'work_order_fields.notice_id')->get()->toArray();
        $data_table = [];
        foreach ($WorkOrderFields as $fields_data) {
            $field_names = explode('||', $fields_data['field_names']);
            $field_values = explode('||', $fields_data['field_values']);
            $field_names_values = array_combine($field_names, $field_values);
            $field_names_values['default'] = '';
            $data_table[] = array_merge($fields_data, $field_names_values);
        }

        return DataTables::of($data_table)
                        ->addColumn('action', function($workorder) {
                            return view('customer.work_order_histry.actionworkorder', compact('workorder'))->render();
                        })
                        ->filter(function($query) use ($request) {
                            if (($request->has('from_date')) && ($request->get('from_date') != NULL)) {
                                $query->where('date', '>', date('Y-m-d', strtotime($request->get('from_date'))));
//\DB::raw()
                            }
                            if (($request->has('to_date')) && ($request->get('to_date') != NULL)) {
                                $query->where('date', '<', date('Y-m-d', strtotime($request->get('to_date'))));
                            }
                        })
                        ->make(true);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $notice_id, $is_rescind) {
        $data['is_rescind'] = $is_rescind;
        $data['customer_details'] = Auth::user()->customer;


        $notice = \App\Models\Notice::select('name', 'id', 'type','master_notice_id')->where('id', $notice_id)->get()->toArray();
        $data['notice'] = $notice[0];
        $data['cities'] = \App\Models\City::select()->get();
        $data['countries'] = \App\Models\Country::select()->get();
        $data['states'] = \App\Models\State::select()->orderBy('name', 'asc')->get();
        $data['recipients'] = \App\Models\Recipient::select()->where('work_order_id', $id)->get();
        $data['officers_directors'] = \App\Models\CustomerAgent::where('customer_id', '=', Auth::user()->customer->id)
                ->get();
        $data['projectTypes'] = \App\Models\ProjectType::select()->get();
        $data['categories'] = \App\Models\Category::select()->orderBy('name', 'ASC')->get();
        $data['project_roles'] = \App\Models\ProjectRoles::select()->orderBy('name', 'ASC')->get();
        $data['count'] = \App\Models\Recipient::select(DB::raw('COUNT(*) AS total'), 'categories.name')->groupBy('recipients.category_id')->join('categories', 'categories.id', '=', 'recipients.category_id')->where('work_order_id', $id)->get();
        $data['id'] = $id;
        $data['notice_id'] = $notice_id;
//        $result = \App\Models\WorkOrder::select('state_id')->join('notice_fields', 'notice_fields.id', '=', 'work_orders.notice_id')->where('work_orders.id', $id)->get()->toArray();
        $result = \App\Models\WorkOrder::select('state_id', 'is_rescind')->join('notice_fields', 'notice_fields.notice_id', '=', 'work_orders.notice_id')->where('work_orders.id', $id)->limit(1)->get()->toArray();
        $data['attachment'] = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $id)
                ->get();

        $data['names'] = \App\Models\Contact::select('contacts.id', 'contacts.company_name')
//   ->where('contact_details.type', '=', 0)
                ->where('contacts.customer_id', '=', Auth::user()->customer->id)
// ->Join('contact_details', 'contact_details.contact_id', '=', 'contacts.id')
                ->orderBy('contacts.company_name', 'ASC')
                ->get();




        $work_order_status = \App\Models\WorkOrder::find($id);
        $account_manager_email = \App\Models\SecondaryDocument::getAccountManagerEmail(Auth::user()->customer->id);
        $admin_email = \App\Models\SecondaryDocument::getAdminEmail();
        /* if (isset($account_manager_email) && !empty($account_manager_email)) {
          $data['note_emails'] = [
          $admin_email->email,
          $account_manager_email->email
          ];
          } else {
          $data['note_emails'] = [
          $admin_email->email
          ];

          } */
        $data['note_emails'] = get_user_notice_email($work_order_status->customer_id);
        $data['notes'] = \App\Models\WorkOrderNotes::leftjoin('users', 'users.id', '=', 'work_order_notes.user_id')
                ->select('work_order_notes.*','users.name')
                ->where('customer_id', '=', Auth::user()->customer->id)
                ->where('work_order_notes.work_order_id', '=', $id)
                ->where('work_order_notes.visibility', '=', 0)
                ->orderBy('work_order_notes.id', 'DESC')
                ->get();
        $data['corrections'] = \App\Models\WorkOrderCorrections::leftjoin('users', 'users.id', '=', 'work_order_corrections.user_id')
                ->select('work_order_corrections.*','users.name')
                ->where('customer_id', '=', Auth::user()->customer->id)
                ->where('work_order_corrections.work_order_id', '=', $id)
                ->where('work_order_corrections.visibility', '=', 0)
                ->orderBy('work_order_corrections.id', 'DESC')
                ->get();

        $work_order_status = \App\Models\WorkOrder::find($id);
        if ($work_order_status->status == 1) {
            $data['status'] = 'request';
        } else if ($work_order_status->status == 0) {
            $data['status'] = 'draft';
        }

        if ($notice[0]['type'] == 2) {
            $data['notice_fields_section1'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.notice_id', $notice_id)->where('work_order_fields.workorder_id', $id)->where('notice_fields.section', 1)->orderBy('sort_order', 'asc')->get();
            $data['notice_fields_section2'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.notice_id', $notice_id)->where('work_order_fields.workorder_id', $id)->where('notice_fields.section', 2)->orderBy('sort_order', 'asc')->get();
//  dd($data['notice_fields_section1']);
            $data['tab'] = 'project';
            //To display read only parent work order
            foreach ($data['notice_fields_section1'] as $key => $value) {
                $key = explode("_", $key);

                if ($value) {

                    if ($value->name == 'parent_work_order') {
                        $parent_work_order = $value->value;
                        $data['parent_work_order'] = $parent_work_order;
                    }
                }
            }

            return view('customer.work_order_histry.edit_soft_notices', $data);
        } else {
            $notice = \App\Models\Notice::select('name', 'id','master_notice_id')->where('id', $notice_id)->get()->toArray();
            $data['notice'] = $notice[0];
            $data['notice_id'] = $notice_id;
            $data['notice_fields'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.notice_id', $notice_id)->where('work_order_fields.workorder_id', $id)->orderBy('sort_order', 'asc')->get();
            $notice = \App\Models\Notice::select('name')->where('id', $notice_id)->get()->toArray();
            $data['tab'] = 'project';
            //To display read only parent work order
            foreach ($data['notice_fields'] as $key => $value) {
                $key = explode("_", $key);

                if ($value) {

                    if ($value->name == 'parent_work_order') {
                        $parent_work_order = $value->value;
                        $data['parent_work_order'] = $parent_work_order;
                    }
                }
            }
            return view('customer.work_order_histry.edit', $data);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $notice_id, $is_rescind) {
//dd($request->all());
        $data['customer_details'] = Auth::user()->customer;


        //  $fields = array_merge($request->except(['_token', 'work_order', 'continue', 'type', 'notice_id', 'edit_soft_notices_attachment', 'file_name', 'doc_input', 'hidden_city_val', 'duplicate', 'remove_doc', 'recipient_county_id', 'visibility', 'recipient_notary_county_id', 'recipient_project_county_id', 'recipient_recorded_county_id', 'parent_work_order_previous']));
        $fields = array_merge($request->except(['_token', 'work_order', 'continue', 'type', 'notice_id', 'edit_soft_notices_attachment', 'file_name', 'doc_input', 'hidden_city_val', 'duplicate', 'remove_doc', 'parent_work_order_previous', 'recipient_county_id', 'contracted_by_exist', 'document_type', 'work_order_attachment', 'visibility', 'recipient_notary_county_id', 'recipient_project_county_id', 'recipient_recorded_county_id']));


        $fieldsData = [];
        $values = [];
        $i = 0;
        foreach ($fields as $key => $value) {
            $field = explode("_", $key);
            $fieldsData[$i] = $field['0'];
            $values[$i] = $value;
            $i++;
        }

        $customerId = Auth::user()->customer->id;
        $userId = Auth::user()->id;
        $account_manager_id = Auth::user()->customer->account_manager_id;
        if ($request->duplicate == 'duplicate') {

            if ($request->continue == 'Continue') {
                $result1 = WorkOrder::create([
                            'notice_id' => $request->notice_id,
                            'customer_id' => $customerId,
                            'order_no' => str_random(5),
                            'user_id' => $userId,
                            'status' => '1',
                            'account_manager_id' => $account_manager_id,
                ]);
                $data['status'] = 'request';
                Session::flash('success', 'Your work order # ' . $result1->id . ' project information has been successfully completed. Please add recipients.');
            } else {
                $result1 = WorkOrder::create([
                            'notice_id' => $request->notice_id,
                            'customer_id' => $customerId,
                            'order_no' => str_random(5),
                            'user_id' => $userId,
                            'status' => '0',
                            'account_manager_id' => $account_manager_id,
                ]);
                $data['status'] = 'draft';
                Session::flash('success', 'Your work order # ' . $result1->id . ' has been saved as a draft you must complete Work Order and submit at a later time.<a href="' . url('customer/work-order/view/' . $result1->id) . '">Click here to print.</a>');
            }

            $recipients = \App\Models\Recipient::select('id')->where('work_order_id', $id)->get()->toArray();
            foreach ($recipients as $key => $value) {
                $recipient = \App\Models\Recipient::find($value['id']);
                \App\Models\Recipient::create([
                    'work_order_id' => $result1->id,
                    'category_id' => $recipient->category_id,
                    'name' => $recipient->name,
                    'contact' => $recipient->contact,
                    'mobile' => $recipient->mobile,
                    'address' => $recipient->address,
                    'city_id' => $recipient->city_id,
                    'state_id' => $recipient->state_id,
                    'zip' => $recipient->zip,
                    'fax' => $recipient->fax,
                    'attn' => $recipient->attn,
                ]);
            }
            $data['recipients'] = \App\Models\Recipient::select()->where('work_order_id', $result1->id)->get();
            /* if ($result1) {
              Session::flash('success', 'Record Created Successfully');
              } else {
              Session::flash('success', 'Problem in Record saving');
              } */
            $attachment = explode(',|,', trim($request->edit_soft_notices_attachment[0],'|,'));

            foreach ($attachment AS $k => $v) {
                $v = trim($v,'|,');
                if (isset($request->remove_doc) && !empty($request->remove_doc) && $request->remove_doc[0] != '') {
                    $remove_doc = explode(',', $request->remove_doc[0]);

                    $each_attachment = explode(',', $v);
                    if ($each_attachment[0] != '') {
                        $true = !in_array($each_attachment[2], $remove_doc);
                    } else {
                        $true = '';
                    }
                    if (isset($true) && $true != '') {
                        if ($v != '') {

                            $work_order_attachment_obj = new \App\Models\WorkOrderAttachment();
                            $work_order_attachment_obj->work_order_id = $result1->id;
                            $work_order_attachment_obj->type = $each_attachment[0];
                            $work_order_attachment_obj->title = $each_attachment[1];
                            $work_order_attachment_obj->file_name = $each_attachment[2];
                            $work_order_attachment_obj->original_file_name = $each_attachment[3];
                            if (Auth::user()->hasRole('account-manager')) {
                                $work_order_attachment_obj->visibility = $each_attachment[4];
                            } else {
                                $work_order_attachment_obj->visibility = 0;
                            }
                            $work_order_attachment_obj->save();
                        }
                    }
//  else{ 
                    $previous_attachments = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $id)->get();

                    if (isset($previous_attachments) && count($previous_attachments) > 0) {
                        foreach ($previous_attachments AS $k1 => $a1) {

                           /* $each_remove_doc = explode(',', $request->remove_doc[0]);
                            foreach ($each_remove_doc As $k => $each) {
                                $explode_v = explode('_', $each);
                                $count = count($explode_v);
                                if($count>1){
                                $each_remove_doc[$k] = $explode_v[$count - 3] . '_' . $explode_v[$count - 2] . '_' . $explode_v[$count - 1];
                            }
                            }*/
                            $true = "true";

                           // $true = !in_array($a1->file_name, $each_remove_doc);

                            if (isset($true) && $true != '') {

                                $work_order_attachment_obj = new \App\Models\WorkOrderAttachment();
                                $work_order_attachment_obj->work_order_id = $result1->id;
                                $work_order_attachment_obj->type = $a1->type;
                                $work_order_attachment_obj->title = $a1->title;
                                $work_order_attachment_obj->file_name = date('H') . '_' . $a1->file_name;
                                $work_order_attachment_obj->original_file_name = $a1->original_file_name;
                                if (Auth::user()->hasRole('account-manager')) {
                                    $work_order_attachment_obj->visibility = $a1->visibility;
                                } else {
                                    $work_order_attachment_obj->visibility = 0;
                                }
                                $work_order_attachment_obj->save();
                            }
                        }
                    }
//  }
                } else {

                    if ($v != '') {

                        $each_attachment = explode(',', $v);
                        $work_order_attachment_obj = new \App\Models\WorkOrderAttachment();
                        $work_order_attachment_obj->work_order_id = $result1->id;
                        $work_order_attachment_obj->type = $each_attachment[0];
                        $work_order_attachment_obj->title = $each_attachment[1];
                        $work_order_attachment_obj->file_name = $each_attachment[2];
                        $work_order_attachment_obj->original_file_name = $each_attachment[3];
                        if (Auth::user()->hasRole('account-manager')) {
                            $work_order_attachment_obj->visibility = $each_attachment[4];
                        } else {
                            $work_order_attachment_obj->visibility = 0;
                        }
                        $work_order_attachment_obj->save();
                    }

                    $previous_attachments = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $id)->get();

                    if (isset($previous_attachments) && count($previous_attachments) > 0) {
                        foreach ($previous_attachments AS $k1 => $a1) {
                            $work_order_attachment_obj = new \App\Models\WorkOrderAttachment();
                            $work_order_attachment_obj->work_order_id = $result1->id;
                            $work_order_attachment_obj->type = $a1->type;
                            $work_order_attachment_obj->title = $a1->title;
                            $work_order_attachment_obj->file_name = date('H') . '_' . $a1->file_name;
                            $work_order_attachment_obj->original_file_name = $a1->original_file_name;
                            if (Auth::user()->hasRole('account-manager')) {
                                $work_order_attachment_obj->visibility = $a1->visibility;
                            } else {
                                $work_order_attachment_obj->visibility = 0;
                            }
                            $work_order_attachment_obj->save();
                        }
                    }
                }
            }
            $attachment = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $result1->id)
                    ->get();
        } else {
            if ($request->continue == 'Continue') {
                $result1 = \App\Models\WorkOrder::find($id);
                $result1->notice_id = $request->notice_id;
                $result1->customer_id = $customerId;
                $result1->order_no = str_random(5);
                $result1->user_id = $userId;
                $result1->status = '1';
                $result1->save();
                $data['status'] = 'request';
                Session::flash('success', 'Your work order # ' . $result1->id . ' project information has been successfully completed. Please add recipients.');
            } else {
                $result1 = \App\Models\WorkOrder::find($id);
                $result1->notice_id = $request->notice_id;
                $result1->customer_id = $customerId;
                $result1->order_no = str_random(5);
                $result1->user_id = $userId;
                $result1->status = '0';
                $result1->save();
                $data['status'] = 'draft';
                Session::flash('success', 'Your work order # ' . $result1->id . ' has been saved as a draft you must complete Work Order and submit at a later time.<a href="' . url('customer/work-order/view/' . $result1->id) . '">Click here to print.</a>');
            }
            $data['recipients'] = \App\Models\Recipient::select()->where('work_order_id', $result1->id)->get();
        }

        $account_manager_email = \App\Models\SecondaryDocument::getAccountManagerEmail(Auth::user()->customer->id);
        $admin_email = \App\Models\SecondaryDocument::getAdminEmail();
        /* if (isset($account_manager_email) && !empty($account_manager_email)) {
          $data['note_emails'] = [
          $admin_email->email,
          $account_manager_email->email
          ];
          } else {
          $data['note_emails'] = [
          $admin_email->email
          ];

          } */
        $data['note_emails'] = get_user_notice_email($customerId);


        $data['notes'] = \App\Models\WorkOrderNotes::leftjoin('users', 'users.id', '=', 'work_order_notes.user_id')
                ->select('work_order_notes.*','users.name')
                ->where('customer_id', '=', Auth::user()->customer->id)
                ->where('work_order_notes.work_order_id', '=', $id)
                ->where('work_order_notes.visibility', '=', 0)
                ->orderBy('work_order_notes.id', 'DESC')
                ->get();
        $data['corrections'] = \App\Models\WorkOrderCorrections::leftjoin('users', 'users.id', '=', 'work_order_corrections.user_id')
                ->select('work_order_corrections.*','users.name')
                ->where('customer_id', '=', Auth::user()->customer->id)
                ->where('work_order_corrections.work_order_id', '=', $id)
                ->where('work_order_corrections.visibility', '=', 0)
                ->orderBy('work_order_corrections.id', 'DESC')
                ->get();

        if ($request->duplicate != 'duplicate') {
            if ($result1) {
//dd($request->remove_doc);
                $attachment = explode(',|,', trim($request->edit_soft_notices_attachment[0],'|,'));
//            if (isset($attachment) && !empty($attachment)) {
                foreach ($attachment AS $k => $v) {
                    $v = trim($v,'|,');
                    if($v!="|"){
                    if (isset($request->remove_doc) && !empty($request->remove_doc) && $request->remove_doc[0] != '') {
                        $remove_doc = explode(',', $request->remove_doc[0]);

                        $each_attachment = explode(',', $v);//dd($remove_doc,$each_attachment,$attachment);
                        if ($each_attachment[0] != '' && isset($each_attachment[4])) {
                            $true = !in_array($each_attachment[4], $remove_doc);
                        }else {
                            $true = '';
                        }
                        if (isset($true) && $true != '') {
                            if ($v != '') {

                                $work_order_attachment_obj = new \App\Models\WorkOrderAttachment();
                                $work_order_attachment_obj->work_order_id = $result1->id;
                                $work_order_attachment_obj->type = $each_attachment[0];
                                $work_order_attachment_obj->title = $each_attachment[1];
                                $work_order_attachment_obj->file_name = $each_attachment[2];
                                $work_order_attachment_obj->original_file_name = $each_attachment[3];
                                $work_order_attachment_obj->save();
                            }
                        }
                    } else {
                        if ($v != '') {//dd($v);
                            $each_attachment = explode(',', $v);
                            $work_order_attachment_obj = new \App\Models\WorkOrderAttachment();
                            $work_order_attachment_obj->work_order_id = $result1->id;
                            $work_order_attachment_obj->type = $each_attachment[0];
                            $work_order_attachment_obj->title = $each_attachment[1];
                            $work_order_attachment_obj->file_name = $each_attachment[2];
                            $work_order_attachment_obj->original_file_name = $each_attachment[3];
                            $work_order_attachment_obj->save();
                        }
                    }
                }
            }
            }
        }
        foreach ($fields as $key => $value) {
            $field = explode("_", $key);

            if (isset($field[3])) {
                $check_field = $field[1] . '_' . $field[2] . '_' . $field[3];
                if ($check_field == 'parent_work_order') {
                    $parent_work_order = $value;
                    $data['parent_work_order'] = $parent_work_order;
//If parent work order is selected document of parent work order will inserted
                    $parent_work_order_previous = $request->parent_work_order_previous;
                    if ($parent_work_order_previous != $parent_work_order) {
                        if ($parent_work_order != '') {
                            if (isset($request->remove_doc) && !empty($request->remove_doc) && $request->remove_doc[0] != '') {
                                $previous_attachments = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $parent_work_order)->get();

                                if (isset($previous_attachments) && count($previous_attachments) > 0) {
                                    foreach ($previous_attachments AS $k1 => $a1) {

                                       /* $each_remove_doc = explode(',', $request->remove_doc[0]);
                                        foreach ($each_remove_doc As $k => $each) {
                                            if($each!=""){
                                            $explode_v = explode('_', $each);
                                            $count = count($explode_v);
                                             if($count>1){
                                            $each_remove_doc[$k] = $explode_v[$count - 3] . '_' . $explode_v[$count - 2] . '_' . $explode_v[$count - 1];
                                        }
                                        }
                                        }*/
                                        $true = "true";

                                        //$true = !in_array($a1->file_name, $each_remove_doc);

                                        if (isset($true) && $true != '') {

                                            $work_order_attachment_obj = new \App\Models\WorkOrderAttachment();
                                            $work_order_attachment_obj->work_order_id = $result1->id;
                                            $work_order_attachment_obj->type = $a1->type;
                                            $work_order_attachment_obj->title = $a1->title;
                                           if(!empty($a1->file_name)){
                                                        $work_order_attachment_obj->file_name = $result1->id . '_' . $a1->file_name;
                                                        }else{
                                                         $work_order_attachment_obj->file_name = "";
                                                        }
                                                        if(!empty($a1->original_file_name)){
                                                        $work_order_attachment_obj->original_file_name = $a1->original_file_name;
                                                        }else{
                                                           $work_order_attachment_obj->original_file_name = "";
                                                        }
                                            if (Auth::user()->hasRole('account-manager')) {
                                                $work_order_attachment_obj->visibility = $a1->visibility;
                                            } else {
                                                $work_order_attachment_obj->visibility = 0;
                                            }
                                            $work_order_attachment_obj->save();
                                            if (file_exists(public_path() . '/attachment/work_order_document/parent_document/' . $a1->file_name) && !empty($a1->file_name)) {
                                                rename(public_path() . '/attachment/work_order_document/parent_document/' . $a1->file_name, public_path() . '/attachment/work_order_document/parent_document/' . $work_order_attachment_obj->file_name);
                                                \File::copy(public_path() . '/attachment/work_order_document/parent_document/' . $work_order_attachment_obj->file_name, public_path() . '/attachment/work_order_document/' . $work_order_attachment_obj->file_name);
                                                unlink(public_path() . '/attachment/work_order_document/parent_document/' . $work_order_attachment_obj->file_name);
                                            }
                                        }
                                    }
                                }
                            } else {
                                $remove_doc = explode(',', $request->remove_doc[0]);
                                $previous_attachments = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $parent_work_order)->get();

                                if (isset($previous_attachments) && count($previous_attachments) > 0) {
                                    foreach ($previous_attachments AS $k1 => $a1) {
                                        $work_order_attachment_obj = new \App\Models\WorkOrderAttachment();
                                        $work_order_attachment_obj->work_order_id = $result1->id;
                                        $work_order_attachment_obj->type = $a1->type;
                                        $work_order_attachment_obj->title = $a1->title;
                                        if(!empty($a1->file_name)){
                                                        $work_order_attachment_obj->file_name = $result1->id . '_' . $a1->file_name;
                                                        }else{
                                                         $work_order_attachment_obj->file_name = "";
                                                        }
                                                        if(!empty($a1->original_file_name)){
                                                        $work_order_attachment_obj->original_file_name = $a1->original_file_name;
                                                        }else{
                                                           $work_order_attachment_obj->original_file_name = "";
                                                        }
                                        if (Auth::user()->hasRole('account-manager')) {
                                            $work_order_attachment_obj->visibility = $a1->visibility;
                                        } else {
                                            $work_order_attachment_obj->visibility = 0;
                                        }
                                        $work_order_attachment_obj->save();

                                        if ($a1->file_name != "" && file_exists(public_path() . '/attachment/work_order_document/parent_document/' . $a1->file_name)  && !empty($a1->file_name)) {
                                            rename(public_path() . '/attachment/work_order_document/parent_document/' . $a1->file_name, public_path() . '/attachment/work_order_document/parent_document/' . $work_order_attachment_obj->file_name);
                                            \File::copy(public_path() . '/attachment/work_order_document/parent_document/' . $work_order_attachment_obj->file_name, public_path() . '/attachment/work_order_document/' . $work_order_attachment_obj->file_name);
                                            unlink(public_path() . '/attachment/work_order_document/parent_document/' . $work_order_attachment_obj->file_name);
                                        }
                                    }
                                }
                            }
                            $data['parent_work_order'] = $parent_work_order;
                            $first_category = \App\Models\Category::where('name', 'Contracted By')->first();
                            /* save parent work order recipient  */
                            if (isset($request->contracted_by_exist)) {
                                $parent_work_order_recipients = \App\Models\Recipient::select()->where('work_order_id', $parent_work_order)->where('category_id', '!=', $first_category->id)->get();
                            } else {
                                $parent_work_order_recipients = \App\Models\Recipient::select()->where('work_order_id', $parent_work_order)->get();
                            }
                            \App\Models\Recipient::where( 'work_order_id', $result1->id)->where('parent_work_order','!=',NULL)->delete();
                            if (isset($parent_work_order_recipients) && count($parent_work_order_recipients) > 0) {
                                foreach ($parent_work_order_recipients As $k => $each_parent_recipient) {
                                    $save_recipients = \App\Models\Recipient::create([
                                                'work_order_id' => $result1->id,
                                                'category_id' => $each_parent_recipient->category_id,
                                                'name' => $each_parent_recipient->name,
                                                'contact' => $each_parent_recipient->contact,
                                                'address' => $each_parent_recipient->address,
                                                'city_id' => $each_parent_recipient->city_id,
                                                'attn' => $each_parent_recipient->attn,
                                                'state_id' => $each_parent_recipient->state_id,
                                                'zip' => $each_parent_recipient->zip,
                                                'email' => $each_parent_recipient->email,
                                                'attn' => $each_parent_recipient->attn,
                                                'parent_work_order'=> $parent_work_order,
                                    ]);
                                }
                            }
                        }
                    }
                }
            }
            //Insert contracted by is first recipient
            if (isset($field[2])) {
                $check_contracted_by_field = $field[1] . '_' . $field[2];
                if ($check_contracted_by_field == 'contracted_by') {

                    $first_category = \App\Models\Category::where('name', 'Contracted By')->first();
                    $contracted_by = $value;
                    if ($contracted_by != "") {
                        /* check contracted by is exist or not */
                        $contracted_by_check = \App\Models\Contact::where('company_name', $contracted_by)->first();
                        if (empty($contracted_by_check)) {
                            $contact = \App\Models\Contact::create([
                                        'customer_id' => Auth::user()->customer->id,
                                        'company_name' => $contracted_by,
                            ]);
                        }
                    }
                    $contactdetails = \App\Models\Contact::where('company_name', '=', $contracted_by)
                            ->where('customer_id', '=', $customerId)
                            ->first();
                    if (isset($contactdetails) && $contactdetails != null) {
                        /* check contract details exit or not */
                        $check_contract_recipient_exist = \App\Models\Recipient::where(['category_id' => $first_category->id, 'work_order_id' => $result1->id])->first();
                        /* if not empty delete previouse record */

                        if (!empty($check_contract_recipient_exist)) {
                            if ($check_contract_recipient_exist->name != $contracted_by) {
                                $contract_recipient_delete = \App\Models\Recipient::where(['category_id' => $first_category->id, 'work_order_id' => $result1->id])->delete();
                                $check_contract_recipient_exist = [];
                            }
                        }//echo$contract_recipient_delete;
                        //  echo $result1->id . "--" . $first_category->id . "<br/>";
                        // dd($first_category->id);
                        if (empty($check_contract_recipient_exist)) {
                            $Recipient = \App\Models\Recipient::create([
                                        'work_order_id' => $result1->id,
                                        'category_id' => $first_category->id,
                                        'name' => $contactdetails->company_name,
                                        'contact' => $contactdetails->phone,
                                        'address' => $contactdetails->mailing_address,
                                        'city_id' => $contactdetails->city_id,
                                        'state_id' => $contactdetails->state_id,
                                        'zip' => $contactdetails->zip,
                                        'email' => $contactdetails->email,
                                        'attn' => $contactdetails->attn
                            ]);
                            // dd($Recipient);
                        }
                    }
                }
            }
        }
        $work_order_id = $result1->id;

        $result2 = \App\Models\WorkOrderFields::select()->where('workorder_id', $work_order_id)->delete();

        foreach ($fieldsData as $key => $value) {

            if ($values[$key] == NULL) {
                $result2 = \App\Models\WorkOrderFields::create([
                            'workorder_id' => $work_order_id,
                            'notice_id' => $request->notice_id,
                            'notice_field_id' => $value,
                            'value' => '',
                ]);
            } else
                $result2 = \App\Models\WorkOrderFields::create([
                            'workorder_id' => $work_order_id,
                            'notice_id' => $request->notice_id,
                            'notice_field_id' => $value,
                            'value' => nl2br($values[$key]),
                ]);
        }
        $arr=array_filter($fields, function($k) {
           return strpos($k, 'last_date_on_the_job')>0;
     }, ARRAY_FILTER_USE_KEY);
      $last_date_on_the_job_val=array_pop($arr);

     $arr_parent_work_order=array_filter($fields, function($k) {
        return strpos($k, 'parent_work_order')>0;
     }, ARRAY_FILTER_USE_KEY); 

     $parent_work_order_id=array_pop($arr_parent_work_order);

      $parent_notice_id = WorkOrder::select('notice_id')->where('id', $parent_work_order_id)->first();
          
      if(!empty($parent_notice_id) && !empty($last_date_on_the_job_val)){
       //dd($parent_notice_id);
          $notice_field_id = \App\Models\NoticeField::select('id')->where('notice_id', $parent_notice_id->notice_id)
          ->where('name','last_date_on_the_job')->first();
            if(!empty($notice_field_id)){
                if($last_date_on_the_job_val==NULL){
                        $wrk_order_fields=\App\Models\WorkOrderFields::where('workorder_id',$parent_work_order)
                        ->where('notice_id',$parent_notice_id->notice_id)
                        ->where('notice_field_id',$notice_field_id->id)
                        ->update(['value'=>'']);
                }else{
                    $wrk_order_fields=\App\Models\WorkOrderFields::where('workorder_id',$parent_work_order)
                        ->where('notice_id',$parent_notice_id->notice_id)
                        ->where('notice_field_id',$notice_field_id->id)
                        ->update(['value'=>$last_date_on_the_job_val]);
                }
            }
      }


        $notice = \App\Models\Notice::select('name', 'id', 'type','master_notice_id')->where('id', $notice_id)->get()->toArray();
        $data['notice'] = $notice[0];
        $data['cities'] = \App\Models\City::select()->get();
        $data['countries'] = \App\Models\Country::select()->get();
        $data['states'] = \App\Models\State::select()->orderBy('name', 'asc')->get();
        $data['officers_directors'] = \App\Models\CustomerAgent::where('customer_id', '=', Auth::user()->customer->id)
                ->get();
        $data['projectTypes'] = \App\Models\ProjectType::select()->get();
        $data['recipients'] = \App\Models\Recipient::select()->where('work_order_id', $id)->get();
//dd($data['recipients']);
        $data['categories'] = \App\Models\Category::select()->orderBy('name', 'ASC')->get();
        $data['project_roles'] = \App\Models\ProjectRoles::select()->orderBy('name', 'ASC')->get();
        $data['duplicate'] = $request->duplicate;
        $data['count'] = \App\Models\Recipient::select(DB::raw('COUNT(*) AS total'), 'categories.name')->groupBy('recipients.category_id')->join('categories', 'categories.id', '=', 'recipients.category_id')->where('work_order_id', $id)->get();
        $data['id'] = $id;
        $data['notice_id'] = $notice_id;

        $result = \App\Models\WorkOrder::select('state_id')->join('notice_fields', 'notice_fields.notice_id', '=', 'work_orders.notice_id')->where('work_orders.id', $id)->limit(1)->get()->toArray();
        $data['names'] = \App\Models\Contact::select('contacts.id', 'contacts.company_name')
//   ->where('contact_details.type', '=', 0)
                ->where('contacts.customer_id', '=', Auth::user()->customer->id)
//  ->Join('contact_details', 'contact_details.contact_id', '=', 'contacts.id')
                ->orderBy('contacts.company_name', 'ASC')
                ->get();
        $data['operation'] = 'update_work_order';
        $data['attachment'] = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $id)
                ->get();
        $data['is_rescind'] = $is_rescind;
         if ($request->continue == 'Continue') {
            return redirect()->to('customer/work-order/edit-new/' . $id . '/' . $notice_id . '/' . $is_rescind);
        } else {
            return redirect('customer/view-work-order');
        }
       /* if ($request->continue == 'Continue') {
            if ($notice[0]['type'] == 2) {
                $data['notice_fields_section1'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.workorder_id', $id)->where('work_order_fields.notice_id', $notice_id)->where('notice_fields.section', 1)->orderBy('sort_order', 'asc')->get();

                $data['notice_fields_section2'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.notice_id', $notice_id)->where('work_order_fields.workorder_id', $id)->where('notice_fields.section', 2)->orderBy('sort_order', 'asc')->get();
                $data['tab'] = 'recipients';

                return view('customer.work_order_histry.edit_soft_notices', $data);
            } else {
                $data['notice_fields'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.notice_id', $notice_id)->where('work_order_fields.workorder_id', $id)->orderBy('sort_order', 'asc')->get();

                $data['tab'] = 'recipients';
                return view('customer.work_order_histry.edit', $data);
            }
        } else {
            return redirect('customer/view-work-order');
        }*/
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function researchEditNextStep(Request $request, $id, $notice_id, $is_rescind) {
    //dd($request->all());
        $data['customer_details'] = Auth::user()->customer;


        //  $fields = array_merge($request->except(['_token', 'work_order', 'continue', 'type', 'notice_id', 'edit_soft_notices_attachment', 'file_name', 'doc_input', 'hidden_city_val', 'duplicate', 'remove_doc', 'recipient_county_id', 'visibility', 'recipient_notary_county_id', 'recipient_project_county_id', 'recipient_recorded_county_id', 'parent_work_order_previous']));
        $fields = array_merge($request->except(['_token', 'work_order', 'continue', 'type', 'notice_id', 'edit_soft_notices_attachment', 'file_name', 'doc_input', 'hidden_city_val', 'duplicate', 'remove_doc', 'parent_work_order_previous', 'recipient_county_id', 'contracted_by_exist', 'document_type', 'work_order_attachment', 'visibility', 'recipient_notary_county_id', 'recipient_project_county_id', 'recipient_recorded_county_id']));


       

        $customerId = Auth::user()->customer->id;
        $userId = Auth::user()->id;
        $account_manager_id = Auth::user()->customer->account_manager_id;
        $recipients = \App\Models\Recipient::select('id')->where('work_order_id', $id)->get()->toArray();

        $data['recipients'] = \App\Models\Recipient::select()->where('work_order_id', $id)->get();
          
        $data['recipients'] = \App\Models\Recipient::select()->where('work_order_id', $id)->get();
        

        $account_manager_email = \App\Models\SecondaryDocument::getAccountManagerEmail(Auth::user()->customer->id);
        $admin_email = \App\Models\SecondaryDocument::getAdminEmail();
       
        $data['note_emails'] = get_user_notice_email($customerId);
        $data['notes'] = \App\Models\WorkOrderNotes::leftjoin('users', 'users.id', '=', 'work_order_notes.user_id')->where('customer_id', '=', Auth::user()->customer->id)
                ->where('work_order_notes.work_order_id', '=', $id)
                ->where('work_order_notes.visibility', '=', 0)
                ->orderBy('work_order_notes.id', 'DESC')
                ->get();
        $data['corrections'] = \App\Models\WorkOrderCorrections::leftjoin('users', 'users.id', '=', 'work_order_corrections.user_id')->where('customer_id', '=', Auth::user()->customer->id)
                ->where('work_order_corrections.work_order_id', '=', $id)
                ->where('work_order_corrections.visibility', '=', 0)
                ->orderBy('work_order_corrections.id', 'DESC')
                ->get();
        $workorderiddetails = \App\Models\WorkOrder::find($id);
        $userId = $workorderiddetails->user_id;
        $data['status'] = $workorderiddetails->status;
        $data['parent_work_order'] = $workorderiddetails->parent_id;
        $work_order_id = $id;

        $notice = \App\Models\Notice::select('name', 'id', 'type','master_notice_id')->where('id', $notice_id)->get()->toArray();
        $data['notice'] = $notice[0];
        $data['cities'] = \App\Models\City::select()->get();
        $data['countries'] = \App\Models\Country::select()->get();
        $data['states'] = \App\Models\State::select()->orderBy('name', 'asc')->get();
        $data['officers_directors'] = \App\Models\CustomerAgent::where('customer_id', '=', Auth::user()->customer->id)
                ->get();
        $data['projectTypes'] = \App\Models\ProjectType::select()->get();
        $data['recipients'] = \App\Models\Recipient::select()->where('work_order_id', $id)->get();
//dd($data['recipients']);
        $data['categories'] = \App\Models\Category::select()->orderBy('name', 'ASC')->get();
        $data['project_roles'] = \App\Models\ProjectRoles::select()->orderBy('name', 'ASC')->get();
        $data['duplicate'] = $request->duplicate;
        $data['count'] = \App\Models\Recipient::select(DB::raw('COUNT(*) AS total'), 'categories.name')->groupBy('recipients.category_id')->join('categories', 'categories.id', '=', 'recipients.category_id')->where('work_order_id', $id)->get();
        $data['id'] = $id;
        $data['notice_id'] = $notice_id;

        $result = \App\Models\WorkOrder::select('state_id')->join('notice_fields', 'notice_fields.notice_id', '=', 'work_orders.notice_id')->where('work_orders.id', $id)->limit(1)->get()->toArray();
        $data['names'] = \App\Models\Contact::select('contacts.id', 'contacts.company_name')
//   ->where('contact_details.type', '=', 0)
                ->where('contacts.customer_id', '=', Auth::user()->customer->id)
//  ->Join('contact_details', 'contact_details.contact_id', '=', 'contacts.id')
                ->orderBy('contacts.company_name', 'ASC')
                ->get();
        $data['operation'] = 'update_work_order';
        $data['attachment'] = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $id)
                ->get();
        $data['is_rescind'] = $is_rescind;
        
            if ($notice[0]['type'] == 2) {
                $data['notice_fields_section1'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.workorder_id', $id)->where('work_order_fields.notice_id', $notice_id)->where('notice_fields.section', 1)->orderBy('sort_order', 'asc')->get();

                $data['notice_fields_section2'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.notice_id', $notice_id)->where('work_order_fields.workorder_id', $id)->where('notice_fields.section', 2)->orderBy('sort_order', 'asc')->get();
                $data['tab'] = 'recipients';

                return view('customer.work_order_histry.edit_soft_notices', $data);
            } else {
                $data['notice_fields'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.notice_id', $notice_id)->where('work_order_fields.workorder_id', $id)->orderBy('sort_order', 'asc')->get();

                $data['tab'] = 'recipients';
                return view('customer.work_order_histry.edit', $data);
            }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cancel($id) {
        //dd($id);
        $result = \App\Models\WorkOrder::find($id);
        
        $updateData = ['status' => 7, 'cancelled_at' => date("Y-m-d H:i:s")];
        //if status is draft/request & updated by customer then will consider as no-cancellation-charge on invoice
        if(!empty($result) && ( $result->status == 0 ||  $result->status == 1) ) {
            $updateData += ['is_billable' => 0];
        }
        $result->update($updateData);

        if ($result) {
            \Artisan::call("GeneratingQuickbooksInvoice:generatingInvoice");
            Session::flash('success', 'Your work order # ' . $id . ' Cancel Successfully.');
        } else {
            Session::flash('success', 'Your work order # ' . $id . ' Problem in Record saving');
        }
        return redirect()->back();
    }

    public function proceed($id) {
        $result = \App\Models\WorkOrder::find($id);
        $result->update(['status' => 2]);
        $result->save();
        if ($result) {
            Session::flash('success', 'Your work order # ' . $id . ' updated Successfully.<a href="' . url('customer/work-order/view/' . $id) . '">Click here to print.</a>');
        } else {
            Session::flash('success', 'Your work order # ' . $id . ' Problem in Record saving');
        }
        return redirect()->back();
    }

    public function duplicate($id, $notice_id, $is_rescind) {
        $workorder_data = WorkOrder::find($id);
        if ($workorder_data->is_rescind == '1' || $is_rescind == 1) {
            $is_rescind = 1;
        }
        if (Auth::user()->hasRole('account-manager') || Auth::user()->hasRole('admin')) {
            $customerId = $workorder_data->customer_id;
        } else {
            $customerId = Auth::user()->customer->id;
        }
        $data['customer_details'] = Auth::user()->customer;
        $userId = Auth::user()->id;
        $account_manager_id = Auth::user()->customer->account_manager_id;
        $result1 = WorkOrder::create([
                    'notice_id' => $notice_id,
                    'customer_id' => $customerId,
                    'order_no' => str_random(5),
                    'user_id' => $userId,
                    'status' => '0',
                    'account_manager_id' => $account_manager_id,
                    'is_rescind' => $is_rescind,
        ]);
        $data['status'] = 'draft';
        $flashMsg = 'Your work order #' . $id . ' has been '.(($is_rescind == 1) ? 'rescinded' : 'duplicated');
        Session::flash('success', $flashMsg);


        $recipients = \App\Models\Recipient::select('id')->where('work_order_id', $id)->get()->toArray();

        foreach ($recipients as $key => $value) {
# code...
            $recipient = \App\Models\Recipient::find($value['id']);
            $new_recipient_data =  \App\Models\Recipient::create([
                'work_order_id' => $result1->id,
                'category_id' => $recipient->category_id,
                'name' => $recipient->name,
                'contact' => $recipient->contact,
                'mobile' => $recipient->mobile,
                'address' => $recipient->address,
                'city_id' => $recipient->city_id,
                'attn' => $recipient->attn,
                'state_id' => $recipient->state_id,
                'zip' => $recipient->zip,
                'fax' => $recipient->fax,
                'attn' => $recipient->attn,
                'email' => $recipient->email,
                'contact_id' => $recipient->contact_id,
            ]);
            /* save UPS address verified */
            $recipients_uspsAddress = \App\Models\UspsAddress::where('recipient_id', $value['id'])->get();
            
            foreach ($recipients_uspsAddress as $recipients_uspsAddress_val) {
                $usps_address = [
                    'fullname' => $recipients_uspsAddress_val->fullname,
                    'company' => $recipients_uspsAddress_val->company,
                    'department' => $recipients_uspsAddress_val->department,
                    'address' => $recipients_uspsAddress_val->address,
                    'address2' => $recipients_uspsAddress_val->address2,
                    'address3' => $recipients_uspsAddress_val->address3,
                    'city' => $recipients_uspsAddress_val->city,
                    'state' => $recipients_uspsAddress_val->state,
                    'zipcode' => $recipients_uspsAddress_val->zipcode,
                    'zipcode_add_on' => $recipients_uspsAddress_val->zipcode_add_on,
                    'phone_number' => $recipients_uspsAddress_val->phone_number,
                    'email' => $recipients_uspsAddress_val->email,
                    'dpb' => $recipients_uspsAddress_val->dpb,
                    'check_digit' => $recipients_uspsAddress_val->check_digit,
                    'comment' => $recipients_uspsAddress_val->comment,
                    'override_hash' => $recipients_uspsAddress_val->override_hash,
                    'recipient_id' => $new_recipient_data->id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),];
                \App\Models\UspsAddress::insert($usps_address);
            }
        }

        $previous_attachments = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $id)->get();

        if ($previous_attachments && count($previous_attachments) > 0) {
            foreach ($previous_attachments AS $k => $val) {
                if ($val->original_file_name != "") {
                    $original_file_name = explode('.', $val->original_file_name);
                    $val->original_file_name = !empty($original_file_name[1]) ? '2_' . $original_file_name[0] . '.' . $original_file_name[1] : "";
                }
                if ($val->file_name != "" && file_exists(public_path() . '/attachment/work_order_document/' . $val->file_name)) {
                    $file_name = explode('.', $val->file_name);
                    if (!empty($file_name[1])) {
                        $new_file_name = date('H') . '_' . $file_name[0] . '.' . $file_name[1];

                        if (!file_exists(public_path() . '/attachment/work_order_document/temp_duplicate')) {
                            $dir = mkdir(public_path() . '/attachment/work_order_document/temp_duplicate', 0777, true);
                            if ($val->file_name != "") {
                                \File::copy(public_path() . '/attachment/work_order_document/' . $val->file_name, public_path() . '/attachment/work_order_document/temp_duplicate/' . $val->file_name);
                            }
                        } else {
                            if ($val->file_name != "" && file_exists(public_path() . '/attachment/work_order_document/' . $val->file_name)) {
                                \File::copy(public_path() . '/attachment/work_order_document/' . $val->file_name, public_path() . '/attachment/work_order_document/temp_duplicate/' . $val->file_name);
                            }
                        }
                        if ($val->file_name != "" && file_exists(public_path() . '/attachment/work_order_document/temp_duplicate/' . $val->file_name)) {
                            rename(public_path() . '/attachment/work_order_document/temp_duplicate/' . $val->file_name, public_path() . '/attachment/work_order_document/temp_duplicate/' . $new_file_name);
                            //\Storage::move(public_path().'/attachment/work_order_document/temp_duplicate/'.$new_file_name, public_path().'/attachment/work_order_document/'.$new_file_name);
                            \File::copy(public_path() . '/attachment/work_order_document/temp_duplicate/' . $new_file_name, public_path() . '/attachment/work_order_document/' . $new_file_name);
                            unlink(public_path() . '/attachment/work_order_document/temp_duplicate/' . $new_file_name);
                        }
                        $val->file_name = $new_file_name;
                    } else {
                        $val->file_name = "";
                    }
                } else {
                    $val->file_name = "";
                }
                $work_order_attachment_obj = new \App\Models\WorkOrderAttachment();
                $work_order_attachment_obj->work_order_id = $result1->id;
                $work_order_attachment_obj->type = $val->type;
                $work_order_attachment_obj->title = $val->title;
                $work_order_attachment_obj->file_name = $val->file_name;
                $work_order_attachment_obj->original_file_name = $val->original_file_name;
                if (Auth::user()->hasRole('account-manager')) {
                    $work_order_attachment_obj->visibility = $val->visibility;
                } else {
                    $work_order_attachment_obj->visibility = 0;
                }
                $work_order_attachment_obj->save();
            }
        }
//dd($result1);
        $notice = \App\Models\Notice::select('name', 'id', 'type', 'state_id')->where('id', $notice_id)->get()->toArray();

        /* parent work order notice field id */
        $parent_workorder_notice_field = \App\Models\NoticeField::where(['notice_id' => $notice_id, 'name' => 'parent_work_order'])->first();

        $fieldsData = \App\Models\WorkOrderFields::leftJoin('work_orders', 'work_orders.id', '=', 'work_order_fields.workorder_id')->select()->where('workorder_id', $id)->get();
        $rescind_flag = $result1->is_rescind;
        
        foreach ($fieldsData as $value) {

            $work_val = nl2br($value->value);
            if(!empty($parent_workorder_notice_field)){
                if ($parent_workorder_notice_field->id == $value->notice_field_id && $is_rescind != 1) {
                    $work_val = "";
                }

                //if wo is resind, then parent w/o should be this w/o
                if ($parent_workorder_notice_field->id == $value->notice_field_id && $is_rescind == 1) {
                    $work_val = $id;
                }
            }
            
            $result2 = \App\Models\WorkOrderFields::create([
                        'workorder_id' => $result1->id,
                        'notice_id' => $value->notice_id,
                        'notice_field_id' => $value->notice_field_id,
                        'value' => $work_val,
                    ]);
        }//dd($is_rescind,$rescind_flag);
        if ($is_rescind == 1) {
            $amendment_fields = \App\Models\NoticeField::select()->where('state_id', $notice[0]['state_id'])->where('notice_id', $notice_id)->where('name', 'amendment')->orderBy('sort_order', 'asc')->get()->toArray();
            $amendmentValue = \App\Models\WorkOrderFields::select('value')->where('notice_field_id', $amendment_fields[0]['id'])->where('workorder_id', $result1->id)->get();
            if ($amendment_fields && $amendmentValue->isEmpty()) {
                $result2 = \App\Models\WorkOrderFields::create([
                            'workorder_id' => $result1->id,
                            'notice_id' => $notice_id,
                            'notice_field_id' => $amendment_fields[0]['id'],
                            'value' => ' ',
                ]);
            }
        }

        return redirect("customer/work-order/edit/" . $result1->id . "/" . $notice_id . "/" . $is_rescind);
    }

    /*  public function duplicate($id, $notice_id) {
      $notice = \App\Models\Notice::select('name', 'id', 'type')->where('id', $notice_id)->get()->toArray();
      $data['notice'] = $notice[0];
      $data['cities'] = \App\Models\City::select()->get();
      $data['countries'] = \App\Models\Country::select()->get();
      $data['states'] = \App\Models\State::select()->get();
      $data['recipients'] = \App\Models\Recipient::select()->where('work_order_id', $id)->get();

      $data['count'] = \App\Models\Recipient::select(DB::raw('COUNT(*) AS total'), 'categories.name')->groupBy('recipients.category_id')->join('categories', 'categories.id', '=', 'recipients.category_id')->where('work_order_id', $id)->get();
      $data['id'] = $id;
      $data['categories'] = \App\Models\Category::select()->get();
      $data['notice_id'] = $notice_id;
      //        $result = \App\Models\WorkOrder::select('state_id', 'customer_id')->join('notice_fields', 'notice_fields.id', '=', 'work_orders.notice_id')->where('work_orders.id', $id)->get()->toArray();
      $result = \App\Models\WorkOrder::select('state_id', 'customer_id')->join('notice_fields', 'notice_fields.notice_id', '=', 'work_orders.notice_id')->where('work_orders.id', $id)->limit(1)->get()->toArray();
      $data['customer_id'] = $result[0]['customer_id'];
      //$data['attachment'] = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $id)
      // ->get();
      //dd($data['attachment']);
      $data['projectTypes'] = \App\Models\ProjectType::select()->get();
      $data['officers_directors'] = \App\Models\CustomerAgent::where('customer_id', '=', Auth::user()->customer->id)
      ->get();
      $data['duplicate'] = 'duplicate';
      $data['status'] = 'draft';
      $data['attachment'] = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $id)
      ->where('visibility', '=', 0)
      ->get();
      $data['names'] = \App\Models\Contact::select('contacts.id', 'contacts.company_name')
      //   ->where('contact_details.type', '=', 0)
      ->where('contacts.customer_id', '=', Auth::user()->customer->id)
      // ->Join('contact_details', 'contact_details.contact_id', '=', 'contacts.id')
      ->orderBy('contacts.id', 'DESC')
      ->get();


      $account_manager_email = \App\Models\SecondaryDocument::getAccountManagerEmail(Auth::user()->customer->id);
      $admin_email = \App\Models\SecondaryDocument::getAdminEmail();
      if (isset($account_manager_email) && !empty($account_manager_email)) {
      $data['note_emails'] = [
      $admin_email->email,
      $account_manager_email->email
      ];
      } else {
      $data['note_emails'] = [
      $admin_email->email
      ];
      }
      $data['notes'] = \App\Models\WorkOrderNotes::where('customer_id', '=', Auth::user()->customer->id)
      ->where('work_order_id', '=', $id)
      ->where('visibility', '=', 0)
      ->orderBy('id', 'DESC')
      ->get();
      $data['corrections'] = \App\Models\WorkOrderCorrections::where('customer_id', '=', Auth::user()->customer->id)
      ->where('work_order_id', '=', $id)
      ->where('visibility', '=', 0)
      ->orderBy('id', 'DESC')
      ->get();
      if ($data['attachment'] && count($data['attachment']) > 0) {
      foreach ($data['attachment'] AS $k => $val) {

      $original_file_name = explode('.', $val->original_file_name);
      $val->original_file_name = '2_' . $original_file_name[0] . '.' . $original_file_name[1];

      $file_name = explode('.', $val->file_name);
      $new_file_name = date('H') . '_' . $file_name[0] . '.' . $file_name[1];

      if (!file_exists(public_path() . '/attachment/work_order_document/temp_duplicate')) {
      $dir = mkdir(public_path() . '/attachment/work_order_document/temp_duplicate', 0777, true);
      \File::copy(public_path() . '/attachment/work_order_document/' . $val->file_name, public_path() . '/attachment/work_order_document/temp_duplicate/' . $val->file_name);
      } else {
      if (file_exists(public_path() . '/attachment/work_order_document/' . $val->file_name)) {
      \File::copy(public_path() . '/attachment/work_order_document/' . $val->file_name, public_path() . '/attachment/work_order_document/temp_duplicate/' . $val->file_name);
      }
      }
      if (file_exists(public_path() . '/attachment/work_order_document/temp_duplicate/' . $val->file_name)) {
      rename(public_path() . '/attachment/work_order_document/temp_duplicate/' . $val->file_name, public_path() . '/attachment/work_order_document/temp_duplicate/' . $new_file_name);
      //\Storage::move(public_path().'/attachment/work_order_document/temp_duplicate/'.$new_file_name, public_path().'/attachment/work_order_document/'.$new_file_name);
      \File::copy(public_path() . '/attachment/work_order_document/temp_duplicate/' . $new_file_name, public_path() . '/attachment/work_order_document/' . $new_file_name);
      unlink(public_path() . '/attachment/work_order_document/temp_duplicate/' . $new_file_name);
      }
      $val->file_name = $new_file_name;
      }
      }
      if ($notice[0]['type'] == 2) {
      $data['notice_fields_section1'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.workorder_id', $id)->where('work_order_fields.notice_id', $notice_id)->where('notice_fields.section', 1)->orderBy('sort_order', 'asc')->get();

      $data['notice_fields_section2'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.notice_id', $notice_id)->where('work_order_fields.workorder_id', $id)->where('notice_fields.section', 2)->orderBy('sort_order', 'asc')->get();
      $data['tab'] = 'project';
      //dd($data);
      return view('customer.work_order_histry.edit_soft_notices', $data);
      } else {
      $data['tab'] = 'project';
      $data['notice_fields'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.notice_id', $notice_id)->where('work_order_fields.workorder_id', $id)->orderBy('sort_order', 'asc')->get();
      //dd($data);
      return view('customer.work_order_histry.edit', $data);
      }



      // return view('account_manager.research.edit', $data);
      }
     */

    public function get_workorders(Request $request) {
        //dd($request->all());
        $input = $request->all();
        //dd($input['order']['0']['column']);
        //$col = $input['columns'][$input['order']['0']['column']]['data'];
        //$orderbyvalue = $input['order']['0']['dir'];
        //echo "col -".$col;
        //echo "<br>orderbyvalue -".$orderbyvalue;die;

        $WorkOrderFields = \App\Models\WorkOrderFields::select('notices.master_notice_id as master_notice_id','notices.master_notice_id as master_notice_id','notices.name as notice_name', 'notices.is_rescind as rescind_notice', 'work_orders.is_rescind as rescind_work_order', 'notices.type', 'work_orders.parent_id', 'work_orders.created_at', 'work_orders.user_id', 'users.name as customer_name', 'work_orders.customer_id','customers.company_name', 'usr.name as account_manager_name', 'work_orders.notice_id as notice_id', 'work_orders.status', 'work_orders.user_id', 'work_orders.account_manager_id', 'work_order_fields.workorder_id', 'work_order_fields.notice_id', DB::raw("(GROUP_CONCAT(notice_fields.name SEPARATOR '||')) as `field_names`"), DB::raw("(GROUP_CONCAT(value SEPARATOR '||')) as `field_values`"))
                ->rightjoin('notice_fields', 'notice_fields.id', '=', 'work_order_fields.notice_field_id')
                ->leftjoin('work_orders', 'work_orders.id', '=', 'work_order_fields.workorder_id')
                ->leftjoin('notices', 'notices.id', '=', 'work_orders.notice_id')
                ->leftjoin('users', 'users.id', '=', 'work_orders.user_id')
                ->leftjoin('users as usr', 'usr.id', '=', 'work_orders.account_manager_id')->groupBy('work_order_fields.workorder_id', 'work_order_fields.notice_id')
                ->leftjoin('customers', 'customers.id', '=', 'work_orders.customer_id')
//                ->leftjoin('recipients','recipients.work_order_id','=','work_orders.id')
//                ->leftjoin('categories','categories.id','=','recipients.category_id')
//                ->whereIn('recipients.category_id',[1,2])
                ->where('work_orders.customer_id', Auth::user()->customer->id);

        if (($request->has('from_date')) && ($request->get('from_date') != NULL)) {
            $WorkOrderFields->whereDate('work_orders.created_at', '>=', date('Y-m-d', strtotime($request->get('from_date'))));
        }
        if (($request->has('to_date')) && ($request->get('to_date') != NULL)) {
            $WorkOrderFields->WhereDate('work_orders.created_at', '<=', date('Y-m-d', strtotime($request->get('to_date'))));
        }

        //$WorkOrderFields = $WorkOrderFields->orderBy("notice_fields.name",$orderbyvalue)->get()->toArray();
        $WorkOrderFields = $WorkOrderFields->get()->toArray();

        
        $data_table = [];
        if (isset($WorkOrderFields) && !empty($WorkOrderFields)) {
            foreach ($WorkOrderFields as $fields_data) {


                $field_names = explode('||', $fields_data['field_names']);
                $field_values = explode('||', $fields_data['field_values']);

//                
                $recipients = \App\Models\Recipient::where('work_order_id', '=', $fields_data['workorder_id'])
                        ->whereIn('category_id', [1, 2])
                        ->select('category_id', 'name')
                        ->orderBy('category_id', 'asc')
                        ->get();
                $owner = 0;
                $general_contracted = 0;
                if (isset($recipients) && !empty($recipients)) {
                    array_push($field_names, 'project_owner');
                    array_push($field_names, 'general_contracted');
                    foreach ($recipients AS $k => $v) {

                        if (isset($v->category_id) && $v->category_id == 1) {
                            if ($owner == 0) {
                                array_push($field_values, $v->name);
                                $owner = 1;
                            }
                        }
                        if (isset($v->category_id) && $v->category_id == 2) {
                            if ($general_contracted == 0) {
                                array_push($field_values, $v->name);
                                $general_contracted = 1;
                            }
                        }
                    }
                    if ($owner == 0) {
                        array_push($field_values, "");
                    }
                    if ($general_contracted == 0) {
                        array_push($field_values, "");
                    }
                }


                $field_names_values = (count($field_names) == count($field_values)) ? array_combine($field_names, $field_values) : [];
                $field_names_values['default'] = '';
                $data_table[] = array_merge($fields_data, $field_names_values);
            }
        }

        foreach ($data_table as $k => $each_record) {
            if (isset($each_record['city']) && $each_record['city'] != null) {
                $city_id = \App\Models\City::find($each_record['city']);
                $data_table[$k]['city'] = $city_id->name;
            }
        }

        foreach ($data_table as $k => $each_record) {
            if (isset($each_record['state']) && $each_record['state'] != null) {
                $state_id = \App\Models\State::find($each_record['state']);
                $data_table[$k]['state'] = $state_id->name;
            }
        }
        foreach ($data_table as $k => $each_record) {
            if (isset($each_record['rescind_work_order']) && $each_record['rescind_work_order'] == 1) {
                $data_table[$k]['notice_name'] = $data_table[$k]['notice_name'].' Amendment';
            }
        }
        foreach ($data_table as $k => $each_record) {
            if (isset($each_record['notice_id']) && $each_record['notice_id'] != null) {
                $notice_state_id = \App\Models\Notice::where('id', '=', $each_record['notice_id'])->first();

                $state_id = \App\Models\State::find($notice_state_id->state_id);
                $data_table[$k]['project_state'] = $state_id->name;
            }
        }

        //Calculation fof due date 40 days from job start date
        //and row highlighted at 30 days
       $due_date_master_ids= [config('constants.MASTER_NOTICE')['NTO']['ID'],config('constants.MASTER_NOTICE')['COL']['ID']];

        if (isset($data_table) && !empty($data_table)) {
            foreach ($data_table As $key => $value) {
                $master_notice_id = $value['master_notice_id'];
                foreach ($value AS $k1 => $v1) {

                    if ($k1 == 'job_start_date' && $v1!=""){
                      if($master_notice_id==config('constants.MASTER_NOTICE')['NTO']['ID']){
                        $duedateDays = 40 . 'days';
                        $highlightDays = 30 . 'days';
                        $data_table[$key]['due_date'] = date('m/d/Y', strtotime('+ ' . $duedateDays, strtotime($v1)));
                        $data_table[$key]['highligted_date'] = date('m/d/Y', strtotime('+ ' . $highlightDays, strtotime($v1)));
                      }
                    }

                    if ($k1 == 'clerk_of_court_recorded_date' && $v1!=""){
                    if($master_notice_id==config('constants.MASTER_NOTICE')['COL']['ID']){
                      $duedateDays = 270 . 'days';
                      $highlightDays = 270 . 'days';
                      $data_table[$key]['due_date'] = date('m/d/Y', strtotime('+ ' . $duedateDays, strtotime($v1)));
                      $data_table[$key]['highligted_date'] = date('m/d/Y', strtotime('+ ' . $highlightDays, strtotime($v1)));
                    }
                    }else if($k1 == 'clerk_of_court_recorded_date' && $v1==""){
                        $data_table[$key]['clerk_of_court_recorded_date'] = 'NA';
                    }
                    if(!isset($data_table[$key]['amount_due'])){
                        $data_table[$key]['amount_due'] = 'NA';
                    }
                    if(!isset($data_table[$key]['clerk_of_court_recorded_date'])){
                        $data_table[$key]['clerk_of_court_recorded_date'] = 'NA';
                    }

                    
                    
                    /*if ($k1 == 'last_date_on_the_job'){
                        if($master_notice_id==config('constants.MASTER_NOTICE')['COL']['ID'] || $master_notice_id==config('constants.MASTER_NOTICE')['NPN']['ID']){
                         // $duedateDays = 270 . 'days';
                          $highlightDays = 70 . 'days';
                          //$data_table[$key]['due_date'] = date('m/d/Y', strtotime('+ ' . $duedateDays, strtotime($v1)));
                          $data_table[$key]['highligted_date'] = date('m/d/Y', strtotime('+ ' . $highlightDays, strtotime($v1)));
                        }
                    }*/
                }
            }
        }  


        $generatePDFNotAllowedNotices = [ config('constants.MASTER_NOTICE')['COL']['ID'], 
                    config('constants.MASTER_NOTICE')['SOL']['ID'],
                    config('constants.MASTER_NOTICE')['NPN']['ID'],
                    config('constants.MASTER_NOTICE')['ITL']['ID'],
                    config('constants.MASTER_NOTICE')['BCOL']['ID'],
                    config('constants.MASTER_NOTICE')['SBC']['ID'],
                ];
                
        return DataTables::of($data_table)
            ->addColumn('actions', function($workorder) use ($generatePDFNotAllowedNotices) {
                //return view('customer.work_order_histry.actionworkorder', compact('workorder'))->render();
                if ($workorder['status'] == 0) {
                    return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                            <option value=''>Select</option>
                            <option value='cancel_work_order'>Cancel This Work Order</option>
                            <option value='add_note'>Add Note</option>
                            <option value='continue_working'>Continue Working On This Work Order</option>
                            <option value='view_requested_work_order'>View Request Work Order</option>
                            
                        </select>";
                } elseif ($workorder['status'] == 1) {
                    if ($workorder['rescind_notice'] == 1 && $workorder['rescind_work_order'] == 1) {
                        return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                            <option value=''>Select</option>
                            <option value='cancel_work_order'>Cancel This Work Order</option>
                            <option value='add_note'>Add Note</option>
                            <option value='view_requested_work_order'>View Request Work Order</option>
                            
                        </select>";
                    }else if($workorder['master_notice_id'] == config('constants.MASTER_NOTICE')['COL']['ID']) 
                    {
                        return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                            <option value=''>Select</option>
                            <option value='cancel_work_order'>Cancel This Work Order</option>
                            <option value='add_note'>Add Note</option>
                            <option value='view_requested_work_order'>View Request Work Order</option>
                            
                        </select>";
                    }else{
                            return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                            <option value=''>Select</option>
                            <option value='cancel_work_order'>Cancel This Work Order</option>
                            <option value='add_note'>Add Note</option>
                            <option value='duplicate_work_order'>Duplicate This Work Order</option>
                            <option value='view_requested_work_order'>View Request Work Order</option>
                            
                        </select>";
                    }
                } elseif ($workorder['status'] == 2 || $workorder['status'] == 3 ) {
                    if ($workorder['rescind_notice'] == 1 && $workorder['rescind_work_order'] == 1) {
                        return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                            <option value=''>Select</option>
                            
                            <option value='add_note'>Add Note</option>
                            <option value='view_requested_work_order'>View Request Work Order</option>
                            
                        </select>";
                    }else if($workorder['master_notice_id'] == config('constants.MASTER_NOTICE')['COL']['ID']) {
                            return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                            <option value=''>Select</option>
                            
                            <option value='add_note'>Add Note</option>
                    
                            <option value='view_requested_work_order'>View Request Work Order</option>
                            
                        </select>";
                    } else {
                        return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                            <option value=''>Select</option>
                            
                            <option value='add_note'>Add Note</option>
                            <option value='duplicate_work_order'>Duplicate This Work Order</option>
                            <option value='view_requested_work_order'>View Request Work Order</option>
                            
                        </select>";
                    }
                } elseif ($workorder['status'] == 4) {
                    $dropdown = "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'> <option value=''>Select</option>
                    <option value='add_note'>Add Note</option>
                    <option value='view_requested_work_order'>View Request Work Order</option>";
                    if ($workorder['rescind_notice'] == 1 && $workorder['rescind_work_order'] == 1) {
                    }else if($workorder['master_notice_id'] == config('constants.MASTER_NOTICE')['COL']['ID']) {
                    } else {
                        $dropdown .= "<option value='duplicate_work_order'>Duplicate This Work Order</option>";
                    }
                    if(!in_array($workorder['master_notice_id'], $generatePDFNotAllowedNotices)) {
                        $dropdown .= "<option value='generate_work_order_pdf'>Generate PDF</option>";
                    }
                    $dropdown .= "</select>";
                    return $dropdown;
                }elseif ($workorder['status'] == 5) {
                    if ($workorder['rescind_notice'] == 1 && $workorder['rescind_work_order'] == 1) {
                        return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                            <option value=''>Select</option>
                            <option value='add_note'>Add Note</option>
                            <option value='view_requested_work_order'>View Request Work Order</option>
                        </select>";
                    }else if($workorder['master_notice_id'] == config('constants.MASTER_NOTICE')['COL']['ID']) {
                        return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                            <option value=''>Select</option>
                            <option value='add_note'>Add Note</option>
                            <option value='view_requested_work_order'>View Request Work Order</option>
                        </select>";
                    } else {
                        return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                            <option value=''>Select</option>
                            <option value='duplicate_work_order'>Duplicate This Work Order</option>
                            <option value='add_note'>Add Note</option>
                            <option value='view_requested_work_order'>View Request Work Order</option>
                            
                        </select>";
                    }
                } elseif ($workorder['status'] == 6) {
                    if ($workorder['rescind_notice'] == 1 && $workorder['rescind_work_order'] == 0) {
                        if($workorder['master_notice_id'] == config('constants.MASTER_NOTICE')['COL']['ID']) {
                            $selectDropdown = "
                            <option value='add_note'>Add Note</option>
                            <option value='rescind_work_order'>Rescind This Work Order</option>
                            
                            <option value='view_requested_work_order'>View Request Work Order</option>";
                        }else{
                            $selectDropdown = "
                            <option value='add_note'>Add Note</option>
                            <option value='duplicate_work_order'>Duplicate This Work Order</option>
                            <option value='rescind_work_order'>Rescind This Work Order</option>
                            <option value='view_requested_work_order'>View Request Work Order</option>";
                        }
                    } else if ($workorder['rescind_notice'] == 1 && $workorder['rescind_work_order'] == 1) {
                        $selectDropdown = "
                            <option value='add_note'>Add Note</option>
                            <option value='view_requested_work_order'>View Request Work Order</option>";
                    } else {
                        if($workorder['master_notice_id'] == config('constants.MASTER_NOTICE')['COL']['ID']) {
                            $selectDropdown = "
                            <option value='add_note'> Add Note</option>
                            <option value='view_requested_work_order'>View Request Work Order</option>";
                        }else{
                            $selectDropdown = "
                            <option value='add_note'>Add Note</option>
                            <option value='duplicate_work_order'>Duplicate This Work Order</option>
                            <option value='view_requested_work_order'>View Request Work Order</option>";
                        }
                    }

                    if( $workorder['master_notice_id'] == config('constants.MASTER_NOTICE')['COL']['ID']
                     || $workorder['master_notice_id'] == config('constants.MASTER_NOTICE')['NTO']['ID']) {
                        $selectDropdown .= "<option value='add_ldonj'>Add Last Date On The Job</option>";
                    }

                    if (
                        $workorder['master_notice_id'] == config('constants.MASTER_NOTICE')['COL']['ID']
                    ) {
                        $selectDropdown .= "<option value='add_ccrd'>Add Clerk of Court Recorded date</option>";
                    }

                    if(!in_array($workorder['master_notice_id'], $generatePDFNotAllowedNotices)) {
                        $selectDropdown .= "<option value='view_work_order'>View Completed Work Order</option>
                            <option value='generate_work_order_pdf'>Generate PDF</option>";
                    }
                    return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                        <option value=''>Select</option>"
                        .$selectDropdown
                        ."</select>";
                    
                } elseif ($workorder['status'] == 7) {
                    if ($workorder['rescind_notice'] == 1 && $workorder['rescind_work_order'] == 1) {
                        return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                            <option value=''>Select</option>
                            <option value='add_note'>Add Note</option>
                            
                        </select>";
                    }else if($workorder['master_notice_id'] == config('constants.MASTER_NOTICE')['COL']['ID']) { 
                            return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                            <option value=''>Select</option>
                            <option value='add_note'>Add Note</option>
                            
                        </select>";
                    }else {
                        return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                            <option value=''>Select</option>
                            <option value='duplicate_work_order'>Duplicate This Work Order</option>
                            <option value='add_note'>Add Note</option>
                            
                        </select>";
                    }
                } elseif ($workorder['status'] == 8) {
                    if ($workorder['rescind_notice'] == 1 && $workorder['rescind_work_order'] == 1) {
                        return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                            <option value=''>Select</option>
                            <option value='add_note'>Add Note</option>
                            <option value='view_requested_work_order'>View Request Work Order</option>
                        
                        </select>";
                    }else if($workorder['master_notice_id'] == config('constants.MASTER_NOTICE')['COL']['ID']) { 
                            return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                            <option value=''>Select</option>
                            <option value='add_note'>Add Note</option>
                            <option value='view_requested_work_order'>View Request Work Order</option>
                            
                        </select>";
                    } else {
                        return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                            <option value=''>Select</option>
                            <option value='duplicate_work_order'>Duplicate This Work Order</option>
                            <option value='add_note'>Add Note</option>
                            <option value='view_requested_work_order'>View Request Work Order</option>
                            
                        </select>";
                    }
                } else {
                    if ($workorder['rescind_notice'] == 1 && $workorder['rescind_work_order'] == 1) {
                        return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                            <option value=''>Select</option>
                            <option value='add_note'>Add Note</option>
                            <option value='view_requested_work_order'>View Request Work Order</option>
                            
                        </select>";
                    }else if($workorder['master_notice_id'] == config('constants.MASTER_NOTICE')['COL']['ID']) { 
                            return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                            <option value=''>Select</option>
                            <option value='add_note'>Add Note</option>
                            <option value='view_requested_work_order'>View Request Work Order</option>
                            
                        </select>";
                    } else {
                        return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                            <option value=''>Select</option>
                            <option value='duplicate_work_order'>Duplicate This Work Order</option>
                            <option value='add_note'>Add Note</option>
                            <option value='view_requested_work_order'>View Request Work Order</option>
                            
                        </select>";
                    }
                }
            })
            ->rawColumns(['actions'])
            ->make(true);
    }

    public function add_note(Request $request) {
        //dd($request->all());
        $rules = ['note' => 'required',
            'email' => 'required|email',
        ];
        $validator = \Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return response()->json(['result'=>'errors','message' => $validator->errors()->all()]);
        }

        $request->request->set('customer_id', Auth::user()->customer->id);


        $result = \App\Models\WorkOrderNotes::create($request->all());

        Mail::to($request->email)->send(new \App\Mail\WorkOrderNote($result));
        return response()->json(['result'=>'success','message' => 'Note submitted successfully.']);
    }

    public function printWorkOrder($work_order_id) {
        $secondaryDocument = SecondaryDocument::find($secondary_doc_id);
        $state = $secondaryDocument->state_id;

//Work order details
        $work_order_no = $secondaryDocument->work_order_no;
        $WorkOrderFields = \App\Models\WorkOrderFields::select('work_orders.parent_id', 'work_orders.created_at', 'work_orders.customer_id', 'work_orders.notice_id as notice_id', 'work_orders.status', 'work_orders.user_id', 'work_orders.account_manager_id', 'work_order_fields.workorder_id', 'work_order_fields.notice_id', DB::raw("(GROUP_CONCAT(name SEPARATOR '||')) as `field_names`"), DB::raw("(GROUP_CONCAT(value SEPARATOR '||')) as `field_values`"))->join('notice_fields', 'notice_fields.id', '=', 'work_order_fields.notice_field_id')->join('work_orders', 'work_orders.id', '=', 'work_order_fields.workorder_id')->groupBy('work_order_fields.workorder_id', 'work_order_fields.notice_id')
                        ->where('work_order_fields.workorder_id', '=', $work_order_no)
                        ->get()->toArray();
        $result_arr = [];
        foreach ($WorkOrderFields as $fields_data) {
            $field_names = explode('||', $fields_data['field_names']);
            $field_values = explode('||', $fields_data['field_values']);
            $field_names_values = array_combine($field_names, $field_values);
            $field_names_values['default'] = '';
            $result_arr[] = array_merge($fields_data, $field_names_values);
        }

//Get notice template
        if ($partial_final_type == 0) {
            $notice_id = 8;
            $template_content = \App\Models\NoticeTemplate::where('state_id', '=', $state)
                    ->where('notice_id', '=', $notice_id)
                    ->first();
            $content = $template_content->content;
            foreach ($result_arr[0] AS $k => $val) {

                if ($k == 'project_address') {
                    $project_address = strtoupper($val);
                }
                if ($k == 'state') {
                    $state_id = $val;
                    $state_name = \App\Models\State::find($state_id);
                    $country_name = \App\Models\Country::find($state_name->country_id);
                    $project_state = $state_name->name;
                    $project_country = $country_name->name;
                }
                if ($k == 'city') {
                    $city_id = $val;
                    $city_name = \App\Models\City::find($city_id);
                    $project_city = $city_name->name;
                }
                if ($k == 'zip') {
                    $project_zip = $val;
                }

                if ($k == 'contracted_by') {
                    $contractor = $val;
                }
            }
            $project_address = $project_address . ' ' . $project_city . ' ' . $project_zip . ' ' . $project_state . ' ' . $project_country;
            $arr_variable = ['{{project_address}}', '{{contractor}}'];

            $arr_replace = [$project_address, $contractor];
            $content = str_replace($arr_variable, $arr_replace, $content);
        } else {
            $notice_id = 13;
            $template_content = \App\Models\NoticeTemplate::where('state_id', '=', $state)
                    ->where('notice_id', '=', $notice_id)
                    ->first();
            $content = $template_content->content;
            foreach ($result_arr[0] AS $k => $val) {
                if ($k == 'project_name') {
                    $project_name = strtoupper($val);
                }
                if ($k == 'project_address') {
                    $project_address = strtoupper($val);
                }
                if ($k == 'state') {
                    $state_id = $val;
                    $state_name = \App\Models\State::find($state_id);
                    $country_name = \App\Models\Country::find($state_name->country_id);
                    $project_state = $state_name->name;
                    $project_country = $country_name->name;
                }
                if ($k == 'city') {
                    $city_id = $val;
                    $city_name = \App\Models\City::find($city_id);
                    $project_city = $city_name->name;
                }
                if ($k == 'zip') {
                    $project_zip = $val;
                }

                if ($k == 'date') {
                    $date = $val;
                    $date = date('d F Y', strtotime($date));
                }
                if ($k == 'legal_description') {
                    $lienors_name = $val;
                }
            }
            $project_address = $project_address . ' ' . $project_city . ' ' . $project_zip . ' ' . $project_state . ' ' . $project_country;
            $arr_variable = ['{{amount}}', '{{project_name}}', '{{project_address}}', '{{date}}', '{{customer}}', '{{lienors_name}}'];

            $arr_replace = [$secondaryDocument->amount, $project_name, $project_address, $date, $secondaryDocument->customer, $lienors_name];
            $content = str_replace($arr_variable, $arr_replace, $content);
        }

        $pdf = PDF::loadHTML($content);
        $file_name = $secondary_doc_id . '_' . uniqid() . '.pdf';

//update file name
        $secondaryDocument->file_name = $file_name;
        $secondaryDocument->save();

        $pdf->save(public_path() . '/pdf/secondary_document/' . $file_name);
        return $pdf->stream($file_name);
    }

    /**
     * print work order pdf
     */
    public function printWO($work_order_id) {

        $file_name = \App\Models\WorkOrder::find($work_order_id);
        if (file_exists(public_path() . '/pdf/work_order_document/' . $file_name->file_name)) {

            $mypdf = asset('/') . 'pdf/work_order_document/' . $file_name->file_name;
            echo "<script>window.open('$mypdf', '_blank');</script>";
        }

        $customer_id = Auth::User()->customer->id;

        $data['account_managers'] = \App\Models\User::select('id', 'name')->join('role_users', 'role_users.user_id', 'users.id')->where('role_users.role_id', 3)->get();
// dd($data['account_managers']);
        $data['categories'] = \App\Models\Category::select()->orderBy('name', 'ASC')->get();
        $data['project_roles'] = \App\Models\ProjectRoles::select()->orderBy('name', 'ASC')->get();
        $data['customers'] = \App\Models\Customer::paginate();
        $data['WorkOrders'] = \App\Models\WorkOrder::select()->where('customer_id', $customer_id)->paginate(10);
        $data['WorkOrderFields'] = \App\Models\WorkOrderFields::select()->join('notice_fields', 'notice_fields.id', '=', 'work_order_fields.notice_field_id')->join('work_orders', 'work_orders.id', '=', 'work_order_fields.workorder_id')->where('notice_fields.name', 'project_address')->where('work_orders.customer_id', $customer_id)->get();
        $data['states'] = \App\Models\State::select()->orderBy('name')->get();
        return view('customer.work_order_histry.list', $data);
    }

    function updateStatus($id, $status) {
        $result = \App\Models\WorkOrder::find($id);
        if ($status == '0') {
            $result->status = 0;
            $result->save();
            Session::flash('success', 'Your work order # ' . $id . '  Updated Successfully.<a href="' . url('customer/work-order/view/' . $id) . '">Click here to print.</a>');
            return redirect('customer/view-work-order');
        } else if ($status == '1') {
            $result->status = 1;
            $result->save();
            Session::flash('success', 'Your work order # ' . $id . '  Updated Successfully.<a href="' . url('customer/work-order/view/' . $id) . '">Click here to print.</a>');
            return redirect('customer/view-work-order');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $notice_id, $work_order_id) {

//  $category   =   \App\Models\Recipient::select('category_id')->where('id',$id)->get();
        $result = \App\Models\Recipient::where('id', $id)->delete();
        if ($result) {
            Session::flash('success', 'Your work order # ' . $work_order_id . '  delete Successfully.');
        } else {
            Session::flash('success', 'Your work order # ' . $work_order_id . ' Problem in Record saving.');
        }
        /* foreach ($category as $key => $value) {
          $delete_category = \App\Models\Category::select()->where('id',$value->category_id)->delete();
          } */
        $notice = \App\Models\Notice::select('name', 'id', 'type','master_notice_id')->where('id', $notice_id)->get()->toArray();
        $data['notice'] = $notice[0];
        $data['cities'] = \App\Models\City::select()->get();
        $data['countries'] = \App\Models\Country::select()->get();
        $data['states'] = \App\Models\State::select()->orderBy('name', 'asc')->get();
        $data['recipients'] = \App\Models\Recipient::select()->where('work_order_id', $work_order_id)->get();
        $data['categories'] = \App\Models\Category::select()->orderBy('name', 'ASC')->get();
        $data['project_roles'] = \App\Models\ProjectRoles::select()->orderBy('name', 'ASC')->get();
        $data['count'] = \App\Models\Recipient::select(DB::raw('COUNT(*) AS total'), 'categories.name')->groupBy('recipients.category_id')->join('categories', 'categories.id', '=', 'recipients.category_id')->where('work_order_id', $work_order_id)->get();
        $data['id'] = $work_order_id;
        $data['notice_id'] = $notice_id;
        $data['projectTypes'] = \App\Models\ProjectType::select()->get();
        $data['officers_directors'] = \App\Models\CustomerAgent::where('customer_id', '=', Auth::user()->customer->id)
                ->get();
//                $result = \App\Models\WorkOrder::select('state_id')->join('notice_fields','notice_fields.id','=','work_orders.notice_id')->where('work_orders.id',$work_order_id)->get()->toArray();
        $result = \App\Models\WorkOrder::select('state_id', 'customer_id')->join('notice_fields', 'notice_fields.notice_id', '=', 'work_orders.notice_id')->where('work_orders.id', $work_order_id)->limit(1)->get()->toArray();

        $data['names'] = \App\Models\Contact::select('contacts.id', 'contacts.company_name')
// ->where('contact_details.type', '=', 0)
                ->where('contacts.customer_id', '=', Auth::user()->customer->id)
//  ->Join('contact_details', 'contact_details.contact_id', '=', 'contacts.id')
                ->orderBy('contacts.company_name', 'ASC')
                ->get();
        $data['customer_details'] = Auth::user()->customer;
        $work_order_status = \App\Models\WorkOrder::find($work_order_id);
        if ($work_order_status->status == 1) {
            $data['status'] = 'request';
        } else if ($work_order_status->status == '0') {
            $data['status'] = 'draft';
        }
        $account_manager_email = \App\Models\SecondaryDocument::getAccountManagerEmail(Auth::user()->customer->id);
        $admin_email = \App\Models\SecondaryDocument::getAdminEmail();
        /* if (isset($account_manager_email) && !empty($account_manager_email)) {
          $data['note_emails'] = [
          $admin_email->email,
          $account_manager_email->email
          ];
          } else {
          $data['note_emails'] = [
          $admin_email->email
          ];

          } */
        $data['note_emails'] = get_user_notice_email($result[0]['customer_id']);
        $data['notes'] = \App\Models\WorkOrderNotes::leftjoin('users', 'users.id', '=', 'work_order_notes.user_id')->where('customer_id', '=', Auth::user()->customer->id)
                ->where('work_order_notes.work_order_id', '=', $work_order_id)
                ->where('work_order_notes.visibility', '=', 0)
                ->orderBy('work_order_notes.id', 'DESC')
                ->get();
        $data['corrections'] = \App\Models\WorkOrderCorrections::leftjoin('users', 'users.id', '=', 'work_order_corrections.user_id')->where('customer_id', '=', Auth::user()->customer->id)
                ->where('work_order_corrections.work_order_id', '=', $work_order_id)
                ->where('work_order_corrections.visibility', '=', 0)
                ->orderBy('work_order_corrections.id', 'DESC')
                ->get();

        if ($notice[0]['type'] == 2) {
            $data['notice_fields_section1'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.workorder_id', $work_order_id)->where('work_order_fields.notice_id', $notice_id)->where('notice_fields.section', 1)->orderBy('sort_order', 'asc')->get();

            $data['notice_fields_section2'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.notice_id', $notice_id)->where('work_order_fields.workorder_id', $work_order_id)->where('notice_fields.section', 2)->orderBy('sort_order', 'asc')->get();
            $data['tab'] = 'recipients';
            foreach ($data['notice_fields_section1'] as $key => $value) {
                $key = explode("_", $key);

                if ($value) {

                    if ($value->name == 'parent_work_order') {
                        $parent_work_order = $value->value;
                        $data['parent_work_order'] = $parent_work_order;
                    }
                }
            }
          //  return view('customer.work_order_histry.edit_soft_notices', $data);
        } else {
            $data['notice_fields'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.notice_id', $notice_id)->where('work_order_fields.workorder_id', $work_order_id)->orderBy('sort_order', 'asc')->get();
            $data['tab'] = 'recipients';
            foreach ($data['notice_fields'] as $key => $value) {
                $key = explode("_", $key);

                if ($value) {

                    if ($value->name == 'parent_work_order') {
                        $parent_work_order = $value->value;
                        $data['parent_work_order'] = $parent_work_order;
                    }
                }
            }
           // return view('customer.work_order_histry.edit', $data);
        }
        return redirect()->back();
    }

    public function viewWorkOrder($id) {
        $work_order = WorkOrder::find($id);
        
        if ($work_order->is_rescind == '1') {
            $data['is_rescind'] = 1;
        }
        $notice_id = $work_order->notice_id;
        $notice = \App\Models\Notice::select('name', 'id', 'type','master_notice_id')->where('id', $notice_id)->get()->toArray();

        $data['notice'] = $notice[0];

        $data['recipients'] = \App\Models\Recipient::select()->where('work_order_id', $id)->get();

        $data['count'] = \App\Models\Recipient::select(DB::raw('COUNT(*) AS total'), 'categories.name')->groupBy('recipients.category_id')->join('categories', 'categories.id', '=', 'recipients.category_id')->where('work_order_id', $id)->get();
        $data['id'] = $id;

        $data['notice_id'] = $notice_id;

//        $result = \App\Models\WorkOrder::select('state_id', 'customer_id')->join('notice_fields', 'notice_fields.id', '=', 'work_orders.notice_id')->where('work_orders.id', $id)->get()->toArray();
        $result = \App\Models\WorkOrder::select('state_id', 'customer_id')->join('notice_fields', 'notice_fields.notice_id', '=', 'work_orders.notice_id')->where('work_orders.id', $id)->limit(1)->get()->toArray();
        $data['customer_id'] = $result[0]['customer_id'];
        //$data['attachment'] = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $id)
        // ->get();
        //dd($data['attachment']);

        if (Auth::user()->hasRole('customer')) {
            $data['attachment'] = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $id)
                    ->where('visibility', '=', 0)
                    ->get();
            $data['notes'] = \App\Models\WorkOrderNotes::leftjoin('users', 'users.id', '=', 'work_order_notes.user_id')
                    ->select('work_order_notes.*','users.name')
                    ->where('work_order_id', '=', $id)
                    ->where('work_order_notes.visibility', '=', 0)
                    ->orderBy('work_order_notes.id', 'DESC')
                    ->get();
                    
            $data['corrections'] = \App\Models\WorkOrderCorrections::leftjoin('users', 'users.id', '=', 'work_order_corrections.user_id')
                    ->select('work_order_corrections.*','users.name')
                    ->where('work_order_corrections.work_order_id', '=', $id)
                    ->where('work_order_corrections.visibility', '=', 0)
                    ->orderBy('work_order_corrections.id', 'DESC')
                    ->get();

            $data['customer_details'] = Auth::user()->customer;
        } else {
            $data['attachment'] = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $id)
                    ->get();
            $data['notes'] = \App\Models\WorkOrderNotes::leftjoin('users', 'users.id', '=', 'work_order_notes.user_id')
            ->select('work_order_notes.*','users.name')
            ->where('work_order_notes.work_order_id', '=', $id)
                    ->orderBy('work_order_notes.id', 'DESC')
                    ->get();
            $data['corrections'] = \App\Models\WorkOrderCorrections::leftjoin('users', 'users.id', '=', 'work_order_corrections.user_id')
                    ->select('work_order_corrections.*','users.name')
                    ->where('work_order_corrections.work_order_id', '=', $id)
                    ->orderBy('work_order_corrections.id', 'DESC')
                    ->get();
            $data['customer_details'] = \App\Models\Customer::where('id', $result[0]['customer_id'])->first();
        }
        
        $account_manager_email = \App\Models\SecondaryDocument::getAccountManagerEmail(Auth::user()->customer->id);
        $admin_email = \App\Models\SecondaryDocument::getAdminEmail();

        /* if (isset($account_manager_email) && !empty($account_manager_email)) {
          $data['note_emails'] = [
          $admin_email->email,
          $account_manager_email->email
          ];
          } else {
          $data['note_emails'] = [
          $admin_email->email,
          ];
          }
         */
        $data['note_emails'] = get_user_notice_email($result[0]['customer_id']);



        if ($notice[0]['type'] == 2) {
            $data['notice_fields_section1'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.workorder_id', $id)->where('work_order_fields.notice_id', $notice_id)->where('notice_fields.section', 1)->orderBy('sort_order', 'asc')->get();
            if (isset($data['notice_fields_section1']) && !empty($data['notice_fields_section1'])) {
                foreach ($data['notice_fields_section1'] AS $key => $value) {
                    if ($value->name == 'enter_into_agreement' ||
                            $value->name == 'last_date_of_labor_service_furnished' || $value->name == 'job_start_date') {
                        if (isset($value) && $value != NULL && $value != '') {

                            $value->value = date('m-d-Y', strtotime($value->value));
                            // dd($value->value);
//                          $date = \DateTime::createFromFormat("m-d-Y", $value->value);
//                           
//                            $date = $date->format('m-d-Y'); 
//                            $value->value = $date;
                        }
                    } elseif ($value->name == 'county' || $value->name == 'notary_county' || $value->name == 'recorded_county' ||
                            $value->name == 'project_county') {
                        if (isset($value) && $value != NULL && $value->value != '') {
                            $county = \App\Models\City::find($value->value);
                            $value->value = $county->county;
                        }
                    } elseif ($value->name == 'notary_state' || $value->name == 'recorded_state') {
                        if (isset($value) && $value != NULL && $value->value != '') {
                            $state = \App\Models\State::find($value->value);
                            $value->value = $county->name;
                        }
                    } elseif ($value->name == 'your_role') {
                        if (isset($value) && $value != NULL && $value->value != '') {
                            $category = \App\Models\ProjectRoles::find($value->value);
                            $value->value = $category->name;
                        }
                    } elseif ($value->name == 'project_type') {
                        if (isset($value) && $value != NULL && $value->value != '') {
                            $project_type = \App\Models\ProjectType::find($value->value);

                            $value->value = $project_type->type;
                        }
                    }
                    if ($value->name == 'parent_work_order') {
                        $parent_work_order = $value->value;
                        $data['parent_work_order'] = $parent_work_order;
                    }
                }
            }

            $data['notice_fields_section2'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.notice_id', $notice_id)->where('work_order_fields.workorder_id', $id)->where('notice_fields.section', 2)->orderBy('sort_order', 'asc')->get();
            $data['tab'] = 'project';

            return view('customer.work_order_histry.view_soft_notices', $data);
        } else {
            $data['tab'] = 'project';
            $data['notice_fields'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.notice_id', $notice_id)->where('work_order_fields.workorder_id', $id)->orderBy('sort_order', 'asc')->get();

            if (isset($data['notice_fields']) && !empty($data['notice_fields'])) {
                foreach ($data['notice_fields'] AS $key => $value) {
                    if ($value->name == 'enter_into_agreement' || $value->name == 'last_date_of_labor_service_furnished' || $value->name == 'job_start_date') {
                        if (isset($value) && $value != NULL && $value != '') {
                            $value->value = date('m-d-Y', strtotime($value->value));
//                          $date = \DateTime::createFromFormat("m-d-Y", $value->value);
//                           
//                            $date = $date->format('m-d-Y'); 
//                            $value->value = $date;
                        }
                    } elseif ($value->name == 'county' || $value->name == 'notary_county' || $value->name == 'recorded_county' ||
                            $value->name == 'project_county') {
                        if (isset($value) && $value != NULL && $value != '') {
                            $county = \App\Models\City::find($value->value);
                            if (isset($county) && !empty($county)) {
                                $value->value = $county->county;
                            }
                        }
                    } elseif ($value->name == 'notary_state' || $value->name == 'recorded_state') {
                        if (isset($value) && $value != NULL && $value != '') {
                            $state = \App\Models\State::find($value->value);
                            if (isset($state) && !empty($state)) {
                                $value->value = $state->name;
                            }
                        }
                    } elseif ($value->name == 'your_role') {
                        if (isset($value) && $value != NULL) {
                            $category = \App\Models\ProjectRoles::find($value->value);
                            $value->value = $category->name;
                        }
                    } elseif ($value->name == 'project_type') {
                        if (isset($value) && $value != NULL) {
                            $project_type = \App\Models\ProjectType::find($value->value);
                            $value->value = $project_type->type;
                        }
                    }

                    if ($value->name == 'parent_work_order') {
                        $parent_work_order = $value->value;
                        $data['parent_work_order'] = $parent_work_order;
                    }
                }
            }

            return view('customer.work_order_histry.view_hard_notices', $data);
        }
    }

    /**
     *  Function is used for account maanger after completed work order to upload
     *  recipiet
     */
    public function postViewWorkOrder(Request $request) {
        //dd($request->all());
        $fields = array_merge($request->except(['_token', 'work_order_id', 'notice_id', 'visibility',
                    'doc_input', 'file_name', 'attachments', 'remove_doc', 'continue']));

        //dd($fields);
        $fieldsData = [];
        $values = [];
        $i = 0;
        foreach ($fields as $key => $value) {
            $field = explode("_", $key);
            $fieldsData[$i] = $field['0'];
            $values[$i] = $value;
            $i++;
        }
        $work_order_id = $request->work_order_id;
        foreach ($fieldsData as $key => $value) {
            if ($values[$key] != NULL) {
                $workorderFieldsObj = \App\Models\WorkOrderFields::where('workorder_id', '=', $work_order_id)
                        ->where('notice_field_id', '=', $value)
                        ->first();

                if (isset($workorderFieldsObj) && !empty($workorderFieldsObj)) {

                    $workorderFieldsObj->value = $values[$key];
                    $workorderFieldsObj->save();
                }
            }
        }
       /* Session::flash('success', 'Your work order # ' . $work_order_id . ' has been successfully submitted. <a href="' . url('customer/work-order/view/' . $work_order_id) . '">Click here to print.</a>');*/
        $notice = \App\Models\Notice::select('name', 'id', 'type','master_notice_id')->where('id', $request->notice_id)->get()->toArray();
        $data['recipients'] = \App\Models\Recipient::select()->where('work_order_id', $work_order_id)->get();
        $data['notice'] = $notice[0];
        $data['id'] = $request->work_order_id;
        $data['customer_details'] = Auth::user()->customer;
        $account_manager_email = \App\Models\SecondaryDocument::getAccountManagerEmail(Auth::user()->customer->id);
        $admin_email = \App\Models\SecondaryDocument::getAdminEmail();
        /* if (isset($account_manager_email) && !empty($account_manager_email)) {
          $data['note_emails'] = [
          $admin_email->email,
          $account_manager_email->email
          ];
          } else {
          $data['note_emails'] = [
          $admin_email->email
          ];
          } */
        $result = \App\Models\WorkOrder::select('state_id', 'customer_id', 'is_rescind')->join('notice_fields', 'notice_fields.notice_id', '=', 'work_orders.notice_id')->where('work_orders.id', $request->work_order_id)->limit(1)->get()->toArray();
        $data['is_rescind'] = $result[0]['is_rescind'];
        $data['note_emails'] = get_user_notice_email($result[0]['customer_id']);
        // if (Auth::user()->hasRole('account-manager')) {
        $attachment = explode(',|,', trim($request->attachments[0],'|,'));

//            if (isset($attachment) && !empty($attachment)) {
        foreach ($attachment AS $k => $v) {
            $v = trim($v,'|,');
            if (isset($request->remove_doc) && !empty($request->remove_doc) && $request->remove_doc[0] != '') {
                $remove_doc = explode(',', $request->remove_doc[0]);

                $each_attachment = explode(',', $v);
                if ($each_attachment[0] != '') {
                    $true = !in_array($each_attachment[2], $remove_doc);
                } else {
                    $true = '';
                }
                if (isset($true) && $true != '') {
                    if ($v != '') {

                        $work_order_attachment_obj = new \App\Models\WorkOrderAttachment();
                        $work_order_attachment_obj->work_order_id = $request->work_order_id;
                        $work_order_attachment_obj->type = $each_attachment[0];
                        $work_order_attachment_obj->title = $each_attachment[1];
                        $work_order_attachment_obj->file_name = $each_attachment[2];
                        $work_order_attachment_obj->original_file_name = $each_attachment[3];
                        if (Auth::user()->hasRole('customer')) {
                            $work_order_attachment_obj->visibility = 0;
                        } else {

                            $work_order_attachment_obj->visibility = $each_attachment[4];
                        }
                        $work_order_attachment_obj->save();
                    }
                }
            } else {
                if ($v != '') {
                    $each_attachment = explode(',', $v);
                    $work_order_attachment_obj = new \App\Models\WorkOrderAttachment();
                    $work_order_attachment_obj->work_order_id = $request->work_order_id;
                    $work_order_attachment_obj->type = $each_attachment[0];
                    $work_order_attachment_obj->title = $each_attachment[1];
                    $work_order_attachment_obj->file_name = $each_attachment[2];
                    $work_order_attachment_obj->original_file_name = $each_attachment[3];
                    if (Auth::user()->hasRole('customer')) {
                        $work_order_attachment_obj->visibility = 0;
                    } else {
                        $work_order_attachment_obj->visibility = $each_attachment[4];
                    }
                    $work_order_attachment_obj->save();
                }
            }
        }
//            }
        // }
        if (Auth::user()->hasRole('customer')) {
            $data['attachment'] = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $request->work_order_id)
                    ->where('visibility', '=', 0)
                    ->get();

            $data['notes'] = \App\Models\WorkOrderNotes::leftjoin('users', 'users.id', '=', 'work_order_notes.user_id')
                    ->select('work_order_notes.*','users.name')
                    ->where('customer_id', '=', Auth::user()->customer->id)
                    ->where('work_order_notes.work_order_id', '=', $request->work_order_id)
                    ->where('work_order_notes.visibility', '=', 0)
                    ->orderBy('work_order_notes.id', 'DESC')
                    ->get();

            $data['corrections'] = \App\Models\WorkOrderCorrections::leftjoin('users', 'users.id', '=', 'work_order_corrections.user_id')
                    ->select('work_order_corrections.*','users.name')
                    ->where('customer_id', '=', Auth::user()->customer->id)
                    ->where('work_order_corrections.work_order_id', '=', $request->work_order_id)
                    ->where('work_order_corrections.visibility', '=', 0)
                    ->orderBy('work_order_corrections.id', 'DESC')
                    ->get();
        } else {
            $data['attachment'] = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $request->work_order_id)
                    ->get();

            $data['notes'] = \App\Models\WorkOrderNotes::leftjoin('users', 'users.id', '=', 'work_order_notes.user_id')
                    ->select('work_order_notes.*','users.name')
                    ->where('customer_id', '=', Auth::user()->customer->id)
                    ->where('work_order_notes.work_order_id', '=', $request->work_order_id)
                    ->orderBy('work_order_notes.id', 'DESC')
                    ->get();
            $data['corrections'] = \App\Models\WorkOrderCorrections::leftjoin('users', 'users.id', '=', 'work_order_corrections.user_id')
                    ->select('work_order_corrections.*','users.name')
                    ->where('customer_id', '=', Auth::user()->customer->id)
                    ->where('work_order_corrections.work_order_id', '=', $request->work_order_id)
                    ->orderBy('work_order_corrections.id', 'DESC')
                    ->get();
        }
        if ($notice[0]['type'] == 2) {
            if ($request->continue == 'Continue') {
                $data['tab'] = 'recipients';
                $data['notice_fields_section1'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.workorder_id', $request->work_order_id)->where('work_order_fields.notice_id', $request->notice_id)->where('notice_fields.section', 1)->orderBy('sort_order', 'asc')->get();
                if (isset($data['notice_fields_section1']) && !empty($data['notice_fields_section1'])) {
                    foreach ($data['notice_fields_section1'] AS $key => $value) {
                        if ($value->name == 'enter_into_agreement' ||
                                $value->name == 'last_date_of_labor_service_furnished' || $value->name == 'job_start_date') {
                            if (isset($value) && $value != NULL && $value != '') {
                                $value->value = date('m-d-Y', strtotime($value->value));
//                          $date = \DateTime::createFromFormat("m-d-Y", $value->value);
//                           
//                            $date = $date->format('m-d-Y'); 
//                            $value->value = $date;
                            }
                        } elseif ($value->name == 'county' || $value->name == 'notary_county' || $value->name == 'recorded_county' ||
                                $value->name == 'project_county') {
                            if (isset($value) && $value != NULL) {
                                $county = \App\Models\City::find($value->value);
                                if (isset($county) && !empty($county)) {
                                    $value->value = $county->county;
                                }
                            }
                        } elseif ($value->name == 'notary_state' || $value->name == 'recorded_state') {
                            if (isset($value) && $value != NULL) {
                                $state = \App\Models\State::find($value->value);
                                if (isset($state) && !empty($state)) {
                                    $value->value = $county->name;
                                }
                            }
                        } elseif ($value->name == 'your_role') {
                            if (isset($value) && $value != NULL) {
                                $category = \App\Models\ProjectRoles::find($value->value);
                                $value->value = $category->name;
                            }
                        } elseif ($value->name == 'project_type') {
                            if (isset($value) && $value != NULL) {
                                $project_type = \App\Models\ProjectType::find($value->value);
                                $value->value = $project_type->type;
                            }
                        }
                        if ($value->name == 'parent_work_order') {
                            $parent_work_order = $value->value;
                            $data['parent_work_order'] = $parent_work_order;
                        }
                    }
                }

                $data['notice_fields_section2'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.notice_id', $request->notice_id)->where('work_order_fields.workorder_id', $request->work_order_id)->where('notice_fields.section', 2)->orderBy('sort_order', 'asc')->get();
                return view('customer.work_order_histry.view_soft_notices', $data);
                //return redirect('/customer/work-order/view/' . $work_order_id);
            }
        } else {
            if ($request->continue == 'Continue') {
                $data['tab'] = 'recipients';
                $data['notice_fields'] = \App\Models\NoticeField::select()->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('work_order_fields.notice_id', $request->notice_id)->where('work_order_fields.workorder_id', $request->work_order_id)->orderBy('sort_order', 'asc')->get();

                if (isset($data['notice_fields']) && !empty($data['notice_fields'])) {
                    foreach ($data['notice_fields'] AS $key => $value) {
                        if ($value->name == 'enter_into_agreement' ||
                                $value->name == 'last_date_of_labor_service_furnished' || $value->name == 'job_start_date') {
                            if (isset($value) && $value != NULL && $value != '') {
                                $value->value = date('m-d-Y', strtotime($value->value));
//                          $date = \DateTime::createFromFormat("m-d-Y", $value->value);
//                           
//                            $date = $date->format('m-d-Y'); 
//                            $value->value = $date;
                            }
                        } elseif ($value->name == 'county' || $value->name == 'notary_county' || $value->name == 'recorded_county' ||
                                $value->name == 'project_county') {
                            if (isset($value) && $value != NULL) {
                                $county = \App\Models\City::find($value->value);
                                if (isset($county) && !empty($county)) {
                                    $value->value = $county->county;
                                }
                            }
                        } elseif ($value->name == 'notary_state' || $value->name == 'recorded_state') {
                            if (isset($value) && $value != NULL) {
                                $state = \App\Models\State::find($value->value);
                                if (isset($state) && !empty($state)) {
                                    $value->value = $county->name;
                                }
                            }
                        } elseif ($value->name == 'your_role') {
                            if (isset($value) && $value != NULL) {
                                $category = \App\Models\ProjectRoles::find($value->value);
                                $value->value = $category->name;
                            }
                        } elseif ($value->name == 'project_type') {
                            if (isset($value) && $value != NULL) {
                                $project_type = \App\Models\ProjectType::find($value->value);
                                $value->value = $project_type->type;
                            }
                        }
                        if ($value->name == 'parent_work_order') {
                            $parent_work_order = $value->value;
                            $data['parent_work_order'] = $parent_work_order;
                        }
                    }
                }
                return view('customer.work_order_histry.view_hard_notices', $data);
                //return redirect('/customer/work-order/view/' . $work_order_id);
            }
        }
    }

    public function printrequesteworkorder($work_order_id) {
        
           if (Auth::user()->hasRole('account-manager') || Auth::user()->hasRole('admin')) {
                $result1 = \App\Models\WorkOrder::find($work_order_id);
                if($result1->status == 1){
                    $result = \App\Models\WorkOrder::find($work_order_id);
                    $result->update(['status' => 2]);
                    $result->save();
                }
           }
        $workorderdetails = WorkOrder::find($work_order_id);
        
        if ($workorderdetails->status == 0) {
            $status = 'Draft';
        } elseif ($workorderdetails->status == 1) {
            $status = 'Request';
        } elseif ($workorderdetails->status == 2) {
            $status = 'Processing';
        } elseif ($workorderdetails->status == 3) {
            $status = 'Recording';
        } elseif ($workorderdetails->status == 4) {
            $status = 'Pending Signature';
        } elseif ($workorderdetails->status == 5) {
            $status = 'Mailed';
        } elseif ($workorderdetails->status == 6) {
            $status = 'Completed';
        } elseif ($workorderdetails->status == 7) {
            $status = 'Cancelled';
        } elseif ($workorderdetails->status == 8) {
            $status = 'Restricted';
        }
        $customer_company_name = \App\Models\Customer::find($workorderdetails->customer_id);
        $company_name = $customer_company_name->company_name;
        $contact_name = $customer_company_name->contact_person;
        $company_address = $customer_company_name->mailing_address;
        $company_state_name = \App\Models\State::find($customer_company_name->mailing_state_id);
        $company_state = !empty($company_state_name) ? $company_state_name->name: '';
        $company_city_name = \App\Models\City::find($customer_company_name->mailing_city_id);
        $company_city = !empty($company_city_name) ? $company_city_name->name : '';
        $full_address = $company_address . '<br>' . $company_city . ' ' . $company_state . ' ' . $customer_company_name->mailing_zip;

        $WorkOrderFields = \App\Models\WorkOrderFields::select('work_orders.parent_id', 'work_orders.created_at', 'work_orders.user_id', 'work_orders.customer_id', 'work_orders.notice_id as notice_id', 'work_orders.status', 'work_orders.user_id', 'work_orders.account_manager_id', 'work_order_fields.workorder_id', 'work_order_fields.notice_id', DB::raw("(GROUP_CONCAT(name SEPARATOR '||')) as `field_names`"), DB::raw("(GROUP_CONCAT(value SEPARATOR '||')) as `field_values`"))->join('notice_fields', 'notice_fields.id', '=', 'work_order_fields.notice_field_id')
                        ->join('work_orders', 'work_orders.id', '=', 'work_order_fields.workorder_id')->groupBy('work_order_fields.workorder_id', 'work_order_fields.notice_id')
                        ->where('work_orders.id', $work_order_id)
                        ->get()->toArray();
        $result = [];
        if (isset($WorkOrderFields) && !empty($WorkOrderFields)) {
            foreach ($WorkOrderFields as $fields_data) {
                $field_names = explode('||', $fields_data['field_names']);
                $field_values = explode('||', $fields_data['field_values']);
                $field_names_values = array_combine($field_names, $field_values);
                $field_names_values['default'] = '';
                $result[] = array_merge($fields_data, $field_names_values);
            }
        }
        $data = $result[0];
        $contracted_by_name = '';
        $contracted_by_address = '';
        $contracted_by_phone = '';
        if (isset($data['contracted_by']) && !empty($data['contracted_by'])) {
            $contracted_by = \App\Models\Contact::where('company_name', '=', $data['contracted_by'])
                    ->where('customer_id', '=', $workorderdetails->customer_id)
                    ->first();
            if (!empty($contracted_by)) {
                $contracted_by_name = $contracted_by->company_name;
                $contracted_by_address = $contracted_by->mailing_address;
                $contracted_by_phone = $contracted_by->phone;
            }
        }
        $notice = \App\Models\Notice::select('name', 'id', 'type','master_notice_id')->where('id', $workorderdetails->notice_id)->get()->toArray();

        $data['notice'] = $notice[0];
        $data['is_rescind'] = $workorderdetails->is_rescind;
        $notice_fields = \App\Models\NoticeField::select()
                        ->join('work_order_fields', 'work_order_fields.notice_field_id', '=', 'notice_fields.id')
                        ->where('work_order_fields.notice_id', $workorderdetails->notice_id)
                        ->where('work_order_fields.workorder_id', $work_order_id)
                        ->orderBy('sort_order', 'asc')->get();
        if (isset($notice_fields) && !empty($notice_fields)) {
            foreach ($notice_fields AS $key => $value) {
                if ($value->name == 'date_request' || $value->name == 'enter_into_agreement' ||
                        $value->name == 'last_date_of_labor_service_furnished' || $value->name == 'job_start_date') {
                    if (isset($value) && $value != NULL && $value != '') {
                        $value->value = date('m-d-Y', strtotime($value->value));
//                          $date = \DateTime::createFromFormat("m-d-Y", $value->value);
//                           
//                            $date = $date->format('m-d-Y'); 
//                            $value->value = $date;
                    }
                } elseif ($value->name == 'county' || $value->name == 'notary_county' || $value->name == 'recorded_county' ||
                        $value->name == 'project_county') {
                    if (isset($value) && $value != NULL && $value->value != "") {
                        $county = \App\Models\City::find($value->value);
                        if (isset($county) && !empty($county)) {
                            $value->value = $county->county;
                        }
                    }
                } elseif ($value->name == 'notary_state' || $value->name == 'recorded_state') {
                    if (isset($value) && $value != NULL && $value->value != "") {
                        $state = \App\Models\State::find($value->value);
                        if (isset($state) && !empty($state)) {
                            $value->value = $state->name;
                        }
                    }
                } elseif ($value->name == 'your_role') {
                    if (isset($value) && $value != NULL && $value->value != "") {
                        $category = \App\Models\ProjectRoles::find($value->value);
                        $value->value = $category->name;
                    }
                } elseif ($value->name == 'project_type') {
                    if (isset($value) && $value != NULL && $value->value != "") {
                        $project_type = \App\Models\ProjectType::find($value->value);
                        $value->value = $project_type->type;
                    }
                }
            }
        }
        $recipients = \App\Models\Recipient::select()->where('work_order_id', $work_order_id)->get();
        if (Auth::user()->hasRole('customer')) {
            $notes = \App\Models\WorkOrderNotes::leftjoin('users', 'users.id', '=', 'work_order_notes.user_id')->where('work_order_notes.work_order_id', $work_order_id)->where('visibility', '!=', '1')->select('work_order_notes.created_at','work_order_notes.note' )->get();
        } else {
            $notes = \App\Models\WorkOrderNotes::leftjoin('users', 'users.id', '=', 'work_order_notes.user_id')->where('work_order_notes.work_order_id', $work_order_id)->select('work_order_notes.created_at','work_order_notes.note' )->get();
        }

        if (Auth::user()->hasRole('customer')) {
            $correction = \App\Models\WorkOrderCorrections::leftjoin('users', 'users.id', '=', 'work_order_corrections.user_id')->where('work_order_corrections.work_order_id', $work_order_id)->where('visibility', '!=', '1')->select('work_order_corrections.created_at','work_order_corrections.correction' )->get();
        } else {
            $correction = \App\Models\WorkOrderCorrections::leftjoin('users', 'users.id', '=', 'work_order_corrections.user_id')->where('work_order_corrections.work_order_id', $work_order_id)->select('work_order_corrections.created_at','work_order_corrections.correction' )->get();
        }

        $account_manager = \App\User::where('id', $workorderdetails->account_manager_id)->first();
        if (isset($account_manager) && $account_manager != NULL) {
            $account_manager_name = $account_manager->name;
        } else {
            $account_manager_name = '';
        }
        if (Auth::user()->hasRole('customer')) {
            $attachement = \App\Models\WorkOrderAttachment::where('work_order_id', $work_order_id)->where('visibility', '!=', '1')->get();
        } else {
            $attachement = \App\Models\WorkOrderAttachment::where('work_order_id', $work_order_id)->get();
        }
        
        $content = '<!DOCTYPE html>
                    <html>
                    <head>
                            <title>Work Order Info</title>
                    </head>
                    <body>
                            <table style="width:700px;font-size: 10px;margin: 0 auto;table-layout: fixed; word-wrap:break-word;" cellspacing="0" cellpadding="0">
                                    <tr>
                                            <td valign="top" colspan="4" style="padding:0 5px;"><h5 style="text-transform: uppercase;border-top: 1px solid #000;border-bottom: 1px solid #000;padding: 5px 0;margin:0;">order information</h5></td>
                                    </tr>

                                    <tr>

                                            <td valign="top" style="padding:0 5px;" width="20%"> 
                                              
                                                       Work Order No #' . $work_order_id . '
                                                    
                                             </td>
                                            <td valign="top" style="padding:0 5px;font-weight: bold" width="65%">
                                                (Status: ' . $status . ') ' . $data['notice']['name'] . (($data['is_rescind'] == 1) ? ' Amendment ' : ''). '
                                                        
                                             </td>
                                            <td valign="top" style="padding:0 5px;text-align:right" align="right" width="15%">
                                                
                                                        Submitted Date
                                                   
                                             </td>
                                            <td valign="top" style="padding:0 5px;font-weight: bold" width="15%">
                                               ' . date('d M Y', strtotime($workorderdetails->created_at)) . '
                                                      
                                             </td>

                                    </tr>
                                    <tr>
			<td valign="top" colspan="4" style="padding:0 5px;"><h5 style="text-transform: uppercase;border-top: 1px solid #000;border-bottom: 1px solid #000;padding:5px 0;margin:0;">your information</h5></td>
		</tr>
                <tr>
		   <td colspan="4" valign="top">
                        <table cellspacing="0" cellpadding="0">
                             <tr>
                                <td valign="top" width="130px">
                                        <table  cellspacing="0" cellpadding="0">
                                                <tr>
                                                        <td valign="top" style="padding:0 5px;">Company</td>
                                                </tr>
                                        </table>
                                </td>
                                <td valign="top" width="200px">
                                        <table cellspacing="0" cellpadding="0">
                                                <tr>
                                                        <td valign="top" style="padding:0 5px;">Contact Name</td>
                                                </tr>
                                        </table>
                                </td >
                                <td valign="top" width="200px">
                                <table cellspacing="0" cellpadding="0">
                                        <tr>
                                                <td valign="top" style="padding:0 5px;">Address</td>
                                        </tr>
                                </table>
                               </td>
                               <td valign="top" width="80px">
                                    <table cellspacing="0" cellpadding="0">
                                            <tr>
                                                    <td valign="top" style="padding:0 5px;">Phone</td>
                                            </tr>
                                    </table>
                               </td>
                               <td valign="top" width="80px">
                                    <table cellspacing="0" cellpadding="0">
                                            <tr>
                                                    <td valign="top" style="padding:0 5px;">Fax</td>
                                            </tr>
                                    </table>
                                </td>
                                <!-- <td valign="top" width="100px">
                                    <table  cellspacing="0" cellpadding="0">
                                            <tr>
                                                    <td valign="top" style="padding:0 5px;">Email</td>
                                            </tr>
                                    </table>
                                </td>-->
                            </tr>
                          <tr>
                                <td valign="top" >
                                        <table cellspacing="0" cellpadding="0">
                                                <tr>
                                                        <td valign="top" style="padding:0 5px;font-weight: bold;">' . $company_name . '</td>
                                                </tr>
                                        </table>
                                </td>
                                <td valign="top">
                                    <table  cellspacing="0" cellpadding="0">
                                            <tr>
                                                    <td valign="top" style="padding:0 5px;font-weight: bold;">' . $contact_name . '</td>
                                            </tr>
                                    </table>
                                </td>
                                <td valign="top">
                                    <table  cellspacing="0" cellpadding="0">
                                        <tr>
                                                                <td valign="top" style="padding:0 5px;font-weight: bold;">' . $full_address . '</td>
                                                        </tr>
                                                </table>
                                </td>
                                 <td valign="top">
                                                <table  cellspacing="0" cellpadding="0">
                                                        <tr>
                                                                <td style="padding:0 5px;font-weight: bold;">' . $customer_company_name->office_number . '</td>
                                                        </tr>
                                                </table>
                                 </td>
                                 <td valign="top">
                                                <table  cellspacing="0" cellpadding="0">
                                                        <tr>
                                                                <td style="padding:0 5px;font-weight: bold;">' . $customer_company_name->fax_number . '</td>
                                                        </tr>
                                                </table>
                                </td>
                                <!-- <td valign="top" colspan="4"  style="line-height:10px;">
                                                <table  cellspacing="0" cellpadding="0">
                                                        <tr>
                                                                <td style="padding:0 5px;font-weight: bold;">' . $customer_company_name->company_email . '</td>
                                                        </tr>
                                                </table>
                                </td> -->
                            </tr>
                        </table>
                   </td>
        </tr>
        <tr>
			<td valign="top" colspan="4" style="padding:0 5px;"><h5 style="text-transform: uppercase;border-top: 1px solid #000;border-bottom: 1px solid #000;padding: 5px 0;margin:0;">your customers information</h5></td>
		</tr>

		<tr>
			<td colspan="4" valign="top">
				<table cellspacing="0" cellpadding="0">
					<tr>
						<td valign="top" width="100px">
							<table  cellspacing="0" cellpadding="0" style="width:100px">
								<tr>
									<td style="padding:0 5px;font-weight: bold;">' . $contracted_by_name . '</td>
								</tr>
							</table>
						</td>
						<td valign="top" width="120px">
							<table cellspacing="0" cellpadding="0" style="width:120px">
								<tr>
									<td style="padding:0 5px;font-weight: bold;">-</td>
								</tr>
							</table>
						</td>
						<td valign="top" width="100px">
							<table  cellspacing="0" cellpadding="0" style="width:100px;">
								<tr>
									<td style="padding:0 5px;font-weight: bold;">' . $contracted_by_address . '</td>
								</tr>
							</table>
						</td>
						<td valign="top" width="80px">
							<table  cellspacing="0" cellpadding="0" style="width:80px">
								<tr>
									<td style="padding:0 5px;font-weight: bold;">' . $contracted_by_phone . '</td>
								</tr>
							</table>
						</td>
						<td valign="top" width="100px">
							<table cellspacing="0" cellpadding="0" style="width:100px">
								<tr>
									<td style="padding:0 5px;font-weight: bold;"></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
                        
		</tr>
                <tr>
			<td valign="top" colspan="4" style="padding:0 5px;"><h5 style="text-transform: uppercase;border-top: 1px solid #000;border-bottom: 1px solid #000;padding: 5px 0;margin:0;">project information</h5></td>
		</tr>	<tr>
			<td colspan="4" valign="top">
				<table>';
        $projectinfo = '<tr>';
//dd($content);
        if (isset($notice_fields) && !empty($notice_fields)) {

            foreach ($notice_fields as $fields) {
                if ($fields->name != 'date_request') {
                    $label = str_replace("_", " ", $fields->name);
                    if ($fields->name == 'your_job_reference_no') {
                        $fields->value = '#' . str_replace("_", " ", $fields->value);
                    } else {
                        $fields->value = str_replace("_", " ", $fields->value);
                    }
                    if ($fields->name == 'parent_work_order' || $fields->name == 'your_job_reference_no') {
                        $projectinfo = $projectinfo . '
						<td >
							<table cellspacing="0" cellpadding="0">
								<tr>
									<td valign="top" width="30%">
										<table style="width: 120px" cellspacing="0" cellpadding="0">
											<tr>
												<td style="padding:0px 1px;width:120px;">' . ucfirst(trans($label)) . '</td>
											</tr>
										</table>
									</td>
									<td valign="top" width="70%">
										<table style="width: 200px" cellspacing="0" cellpadding="0">
											<tr>
												<td style="padding:0px 1px;font-weight: bold;width:200px;">' . $fields->value . '</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					';
                    }
                    /* check for check clear and notary seal option */
                    if ($fields->name == 'check_clear' || $fields->name == 'notary_seal') {
                        if ($fields->value)
                            $fields->value = "Optional Yes";
                        else
                            $fields->value = "Optional No";
                    }
                }
            }
            $projectinfo = $projectinfo . '</tr>';
            $i = 1;
            $fieldshtml = '';
            foreach ($notice_fields as $fields) {

                if ($fields->name != 'date_request' && $fields->name != 'parent_work_order' && $fields->name != 'your_job_reference_no') {
                    if ($i == 1) {
                        $fieldshtml = $fieldshtml . '<tr>';
                    }

                    $label = str_replace("_", " ", $fields->name);
                    $fieldshtml = $fieldshtml . '<td>
							<table>
								<tr>
									<td valign="top" width="30%">
										<table style="width: 120px" cellspacing="0" cellpadding="0">
											<tr>
												<td style="padding:0px 1px;width:120px;">' . ucfirst(trans($label)) . '</td>
											</tr>
										</table>
									</td>
									<td valign="top" width="70%">
										<table style="width: 200px;" cellspacing="0" cellpadding="0">
											<tr>
												<td style="padding:0px 1px;font-weight: bold;width:200px;">' . $fields->value . '</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>';
                    if ($i == 2) {
                        $fieldshtml = $fieldshtml . '</tr>';
                        $i = 0;
                    }
                    $i++;
                }
            }

            $projectinfo = $projectinfo . $fieldshtml;
        }

        $content = $content . $projectinfo;
        $content = $content . '	</table>
						</td>
					</tr><tr>
			<td colspan="4" valign="top" style="padding:0 5px;"><h5 style="text-transform: uppercase;border-top: 1px solid #000;border-bottom: 1px solid #000;padding: 2px 0;margin:0;">receipent information</h5></td>
		</tr>
                <tr>
			<td colspan="4" valign="top">
				<table cellspacing="0" cellpadding="0">
					<tr>
						<td valign="top" width="100px">
							<table style="width: 100px;" cellspacing="0" cellpadding="0">
								<tr>
									<td style="padding:0 5px;">Type</td>
								</tr>
							</table>
						</td>
						<td valign="top" width="150px">
							<table style="width: 150px;" cellspacing="0" cellpadding="0">
								<tr>
									<td style="padding:0 5px;">Company</td>
								</tr>
							</table>
						</td>
						<td valign="top" width="170px">
							<table style="width: 170px;" cellspacing="0" cellpadding="0">
								<tr>
									<td style="padding:0 5px;">Address</td>
								</tr>
							</table>
						</td>
						<td valign="top" width="60px">
							<table style="width: 60px;" cellspacing="0" cellpadding="0">
								<tr>
									<td style="padding:0 5px;">Phone</td>
								</tr>
							</table>
						</td>
						<td valign="top" width="80px">
							<table style="width: 80px;" cellspacing="0" cellpadding="0">
								<tr>
									<td style="padding:0 5px;">Fax</td>
								</tr>
							</table>
						</td>
					</tr>';

        $recipient_html = '';
        if (isset($recipients) && !empty($recipients)) {
            foreach ($recipients AS $k => $val) {
                $recipient_html = $recipient_html . '<tr>
						<td valign="top">
							<table style="width: 160px;" cellspacing="0" cellpadding="0">
								<tr>
									<td style="padding:0 5px;font-weight: bold;">' . $val->category->name . '</td>
								</tr>
							</table>
						</td>
						<td valign="top">
							<table style="width: 180px;" cellspacing="0" cellpadding="0">
								<tr>
									<td style="padding:0 5px;font-weight: bold;">' . $val->name . '</td>
								</tr>
							</table>
						</td>
						<td valign="top">
							<table style="width: 200px;" cellspacing="0" cellpadding="0">
								<tr>
									<td style="padding:0 5px;font-weight: bold;;word-wrap: break-word;">' . $val->address . ' ' . $val->city->name . ' ' . $val->state->name . ' ' . $val->zip . '</td>
								</tr>
							</table>
						</td>
						<td valign="top">
							<table style="width: 125px;" cellspacing="0" cellpadding="0">
								<tr>
									<td style="padding:0 5px;font-weight: bold;">' . $val->contact . '</td>
								</tr>
							</table>
						</td>
						<td valign="top">
							<table style="width: 125px;" cellspacing="0" cellpadding="0">
								<tr>
									<td style="padding:0 5px;font-weight: bold;">-</td>
								</tr>
							</table>
						</td>
					</tr>';
            }
        }

        $content = $content . $recipient_html;
        $content = $content . '</table>
                                            </td>
                                           </tr>
                                           <tr>
			<td colspan="4" valign="top" style="padding:0 5px;"><h5 style="text-transform: uppercase;border-top: 1px solid #000;border-bottom: 1px solid #000;padding: 5px 0;margin:0;">order notes</h5></td>
		</tr>';

        $notes_html = '';
        if (isset($notes) && !empty($notes)) {

            foreach ($notes AS $k => $val) {
                $notes_html = $notes_html . '<tr>
			<td colspan="4" valign="top">
				<table cellspacing="0" cellpadding="0">
					<tr>
						<td valign="top" style="width: 120px">
							<table cellspacing="0" cellpadding="0">
								<tr>
									<td valign="top" style="padding:0 5px;font-weight: bold;">' . date('m-d-Y', strtotime($val->created_at)) . '</td>
								</tr>
							</table>
						</td>
						<td valign="top" style="width: 120px">
							<table cellspacing="0" cellpadding="0">
								<tr>
									<td valign="top" style="padding:0 5px;font-weight: bold;">' . date('H:i A', strtotime($val->created_at)) . '</td>
								</tr>
							</table>
						</td>
						<td valign="top" style="width: 280px">
							<table cellspacing="0" cellpadding="0">
								<tr>
									<td valign="top" style="padding:0 5px;font-weight: bold;;text-align:justify">' . $val->note . '</td>
								</tr>
							</table>
						</td>
						<td valign="top"  style="width: 120px">
							<table cellspacing="0" cellpadding="0">
								<tr>

									<td valign="top" style="padding:0 5px;font-weight: bold;;">' . $account_manager_name . '</td>

								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>';
            }
        }




        $content = $content . $notes_html;

        $content = $content . '<tr>
			<td colspan="4" valign="top" style="padding:0 5px;"><h5 style="text-transform: uppercase;border-top: 1px solid #000;border-bottom: 1px solid #000;padding: 5px 0;margin:0;">order Corrections</h5></td>
		</tr>';


        $correction_html = '';
        if (isset($correction) && !empty($correction)) {
            foreach ($correction AS $k => $val) {
                $correction_html = $correction_html . '<tr>
			<td colspan="4" valign="top">
				<table cellspacing="0" cellpadding="0">
					<tr>
						<td valign="top" style="width: 120px">
							<table cellspacing="0" cellpadding="0">
								<tr>
									<td valign="top" style="padding:0 5px;font-weight: bold;">' . date('m-d-Y', strtotime($val->created_at)) . '</td>
								</tr>
							</table>
						</td>
						<td valign="top" style="width: 120px">
							<table cellspacing="0" cellpadding="0">
								<tr>
									<td valign="top" style="padding:0 5px;font-weight: bold;">' . date('H:i A', strtotime($val->created_at)) . '</td>
								</tr>
							</table>
						</td>
						<td valign="top" style="width: 280px">
							<table cellspacing="0" cellpadding="0">
								<tr>
									<td valign="top" style="padding:0 5px;font-weight: bold;;text-align:justify">' . $val->correction . '</td>
								</tr>
							</table>
						</td>
						<td valign="top"  style="width: 120px">
							<table cellspacing="0" cellpadding="0">
								<tr>

									<td valign="top" style="padding:0 5px;font-weight: bold;;">' . $account_manager_name . '</td>

								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>';
            }
        }



        $content = $content . $correction_html;


        $content = $content . '<tr>
			<td colspan="4" valign="top" style="padding:0 5px;"><h5 style="text-transform: uppercase;border-top: 1px solid #000;border-bottom: 1px solid #000;padding: 5px 0;margin:0;">attached documents</h5></td>
		</tr>';
        $attachment_html = '';

       /* if (isset($attachement) && !empty($attachement)) {
            foreach ($attachement AS $k => $val) {
                $attachment_html = $attachment_html . '	<tr>
			<td colspan="4" valign="top">
				<table >
					<tr>
						<td valign="top" style="width: 115px">
							<table cellspacing="0" cellpadding="0">
								<tr>
									<td valign="top" style="padding:0 5px;font-weight: bold;">' . date('m-d-Y', strtotime($val->created_at)) . '</td>
								</tr>
							</table>
						</td>
						<td valign="top" style="width: 115px">
							<table cellspacing="0" cellpadding="0">
								<tr>
									<td valign="top" style="padding:0 5px;font-weight: bold;">' . date('H:i A', strtotime($val->created_at)) . '</td>
								</tr>
							</table>
						</td>
						<td valign="top" style="width: 275px">
							<table cellspacing="0" cellpadding="0">
								<tr>
									<td valign="top" style="padding:0 5px;font-weight: bold;">' . $val->original_file_name . '</td>
								</tr>
							</table>
						</td>
						<td valign="top" style="width: 110px">
							<table cellspacing="0" cellpadding="0">
								<tr>
									<td valign="top" style="padding:0 5px;font-weight: bold;">' . $val->title . '</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>';*/
      $attachment_html =  $attachment_html .' <tr>
            <td colspan="4" valign="top">
                <table cellspacing="0" cellpadding="0">';

        if (isset($attachement) && !empty($attachement)) {
            foreach ($attachement AS $k => $val) {
               
                     $attachment_html = $attachment_html .'<tr>
                       
                                    <td valign="top" width="200px" style="font-weight: bold;padding:0 5px;word-break:break-all">' . date('m-d-Y', strtotime($val->created_at)) . '</td>
                               
                                    <td valign="top" width="200px" style="font-weight: bold;padding:0 5px;word-break:break-all">' . date('H:i A', strtotime($val->created_at)) . '</td>
                               
                                    <td valign="top" width="800px" style="font-weight: bold;padding:0 5px;word-break:break-all">' . $val->original_file_name . '</td>
                               
                                    <td valign="top" width="200px" style="font-weight: bold;padding:0 5px;word-break:break-all">' . $val->title . '</td>
                               
                    </tr>';
                }
        }
        $attachment_html  = $attachment_html . '</table>
            </td>
        </tr>';
            

        $content = $content . $attachment_html;
        $content = $content . '</table>
                                      </body>
                                      </html>';
         echo $content;exit;
        $pdf = PDF::loadHTML($content);

        $file_name = 'VW' . $work_order_id . '_' . uniqid() . '.pdf';


        return $pdf->stream($file_name);
    }

    /*
     * Load Modal to update last date on job
     * 
     */
    public function loadModalAddLastDateOnJob(Request $request, $wo_id,$cyo){
        $workOrderFieldId = $lastDateOnJob = $noticeFieldId = null;
        try{
            if($cyo=="1"){
                $workOrder = \App\Models\Cyo_WorkOrders::findOrFail($wo_id);
            }else{
                $workOrder = WorkOrder::findOrFail($wo_id); 
            }
           
            $notice_id = $workOrder->notice_id;
            if($workOrder->notice->count() > 0) {
                $noticeType = $workOrder->notice->name;
                
                //get notice field id
                $noticeField = $workOrder->notice->noticeFields
                ->where('name', 'last_date_on_the_job')
                ->where('state_id', $workOrder->notice->state_id)->first();
                
                if($noticeField) {
                    $noticeFieldId = $noticeField->id;
                    //get date
                    $workOrderFields = $workOrder->woFields->where('notice_field_id', $noticeFieldId)->first();
                    if($workOrderFields) {
                        $lastDateOnJob = $workOrderFields->value;
                        $workOrderFieldId = $workOrderFields->id;
                    }
                }
                if($cyo=="1"){
                   return view('customer.create_your_own.modal_ldonj', compact('lastDateOnJob', 'workOrderFieldId', 'workOrder', 'noticeFieldId', 'noticeType', 'notice_id'));
                }else{
                   return view('customer.work_order_histry.modal_ldonj', compact('lastDateOnJob', 'workOrderFieldId', 'workOrder', 'noticeFieldId', 'noticeType', 'notice_id'));  
                }
            }
            return 'Something went wrong...';
            
        } catch(\Exception $e) {
            return $e->getMessage();
        }

    }

    /*
     * Load Modal to update clerk of court recorded date
     * 
     */
    public function loadModalClerkCourtRecordedDate(Request $request, $wo_id,$cyo){
        $workOrderFieldId = $clerCourtRecordedDate = $noticeFieldId = null;
        try{
            if($cyo=="1"){
                $workOrder = \App\Models\Cyo_WorkOrders::findOrFail($wo_id);
            }else{
                $workOrder = WorkOrder::findOrFail($wo_id); 
            }
           
            $notice_id = $workOrder->notice_id;
            if($workOrder->notice->count() > 0) {
                $noticeType = $workOrder->notice->name;
                
                //get notice field id
                $noticeField = $workOrder->notice->noticeFields
                ->where('name', 'clerk_of_court_recorded_date')
                ->where('state_id', $workOrder->notice->state_id)->first();
                
                if($noticeField) {
                    $noticeFieldId = $noticeField->id;
                    //get date
                    $workOrderFields = $workOrder->woFields->where('notice_field_id', $noticeFieldId)->first();
                    if($workOrderFields) {
                        $clerCourtRecordedDate = $workOrderFields->value;
                        $workOrderFieldId = $workOrderFields->id;
                    }
                }
                if($cyo=="1"){
                   return view('customer.create_your_own.modal_ccrd', compact('clerCourtRecordedDate', 'workOrderFieldId', 'workOrder', 'noticeFieldId', 'noticeType', 'notice_id'));
                }else{
                   return view('customer.work_order_histry.modal_ccrd', compact('clerCourtRecordedDate', 'workOrderFieldId', 'workOrder', 'noticeFieldId', 'noticeType', 'notice_id'));  
                }
            }
            return 'Something went wrong...';
            
        } catch(\Exception $e) {
            return $e->getMessage();
        }

    }

    /*
     * Insert/update last date on job & to its respective child work orders
     * 
     */
    public function saveLastDateOnJob(Request $request,$cyo) {
        $woId = $request->wo_id;
        $ldonj = $request->ldonj;
        $noticeId = $request->notice_id;
        try {
            if($cyo=="1"){
                if(!empty($request->wo_field_id)) {
                    //update by work order field id
                    if($ldonj==NULL || $ldonj==""){
                        \App\Models\Cyo_WorkOrder_fields::where('id', $request->wo_field_id)
                                        ->update(['value' => ""]);
                    }else{
                        \App\Models\Cyo_WorkOrder_fields::where('id', $request->wo_field_id)
                                        ->update(['value' => $ldonj]);
                    }
                  
                } else if(!empty($request->notice_id) 
                        && !empty($noticeId) 
                        && !empty($woId)) {
                    $wof = new \App\Models\Cyo_WorkOrder_fields();
                    $wof->workorder_id = $woId;
                    $wof->notice_id = $request->notice_id;
                    $wof->notice_field_id = $request->notice_field_id;
                    if($ldonj==NULL || $ldonj==""){
                    $wof->value = "";
                    }else{
                    $wof->value = $ldonj;  
                    }
                    $wof->save();
                } 
                 $WorkOrderFields = \App\Models\Cyo_WorkOrder_fields::select('notices.id as notice_id','cyo__work_orders.id as work_order_id', DB::raw("(GROUP_CONCAT(notice_fields.name SEPARATOR '||')) as `field_names`"), DB::raw("(GROUP_CONCAT(value SEPARATOR '||')) as `field_values`"))
                        ->rightjoin('notice_fields', 'notice_fields.id', '=', 'cyo__work_order_fields.notice_field_id')
                        ->join('cyo__work_orders', 'cyo__work_orders.id', '=', 'cyo__work_order_fields.workorder_id')
                        ->leftjoin('notices', 'notices.id', '=', 'cyo__work_orders.notice_id')
                        ->groupBy('cyo__work_order_fields.workorder_id', 'cyo__work_order_fields.notice_id');
                         $WorkOrderFields = $WorkOrderFields->get()->toArray();
                   
                          $data_table = [];
                    if (isset($WorkOrderFields) && !empty($WorkOrderFields)) {
                        foreach ($WorkOrderFields as $fields_data) {
                           
                            $field_names = explode('||', $fields_data['field_names']);
                            $field_values = explode('||', $fields_data['field_values']);
                            $field_names_values = (count($field_names) == count($field_values)) ? array_combine($field_names, $field_values) : [];
                            //$field_names_values = array_combine($field_names, $field_values);
                            $field_names_values['default'] = '';
                            $data_table[] = array_merge($fields_data, $field_names_values);
                        
                        }
                     }    
                     foreach ($data_table as $k => $each_record) {
                        if (isset($each_record['parent_work_order']) && $each_record['parent_work_order'] != null) {
                                if($each_record['parent_work_order']==$woId){
                                    $notice_field_id = \App\Models\NoticeField::select('id')->where("notice_id",$each_record['notice_id'])->where('name',"last_date_on_the_job")->get();
                                     if(!empty($notice_field_id) && $notice_field_id->count()){
                                        if($ldonj==NULL || $ldonj==""){
                                            $update_child = \App\Models\Cyo_WorkOrder_fields::where('workorder_id', $each_record['work_order_id'])->where('notice_field_id',$notice_field_id['0']['id'])->update(['value' => ""]);
                                        }else{
                                            $update_child = \App\Models\Cyo_WorkOrder_fields::where('workorder_id', $each_record['work_order_id'])->where('notice_field_id',$notice_field_id['0']['id'])->update(['value' => $ldonj]);
                                        }
                                       // ->update(['value' => $ldonj]);   
                                    }
                            }
                        }
                    }
           }else{
                if(!empty($request->wo_field_id)) {
                    //update by work order field id
                    if($ldonj==NULL || $ldonj==""){
                        WorkOrderFields::where('id', $request->wo_field_id)
                                        ->update(['value' => ""]);
                    }else{
                        WorkOrderFields::where('id', $request->wo_field_id)
                                        ->update(['value' => $ldonj]);
                    }
                } else if(!empty($request->notice_id) 
                        && !empty($noticeId) 
                        && !empty($woId)) {
                    $wof = new WorkOrderFields();
                    $wof->workorder_id = $woId;
                    $wof->notice_id = $request->notice_id;
                    $wof->notice_field_id = $request->notice_field_id;
                    if($ldonj==NULL || $ldonj==""){
                        $wof->value = "";
                    }else{
                        $wof->value = $ldonj; 
                    }
                   
                    $wof->save();
                } 
                 $WorkOrderFields = \App\Models\WorkOrderFields::select('notices.id as notice_id','work_orders.id as work_order_id', DB::raw("(GROUP_CONCAT(notice_fields.name SEPARATOR '||')) as `field_names`"), DB::raw("(GROUP_CONCAT(value SEPARATOR '||')) as `field_values`"))
                        ->rightjoin('notice_fields', 'notice_fields.id', '=', 'work_order_fields.notice_field_id')
                        ->join('work_orders', 'work_orders.id', '=', 'work_order_fields.workorder_id')
                        ->leftjoin('notices', 'notices.id', '=', 'work_orders.notice_id')
                        ->groupBy('work_order_fields.workorder_id', 'work_order_fields.notice_id');
                        $WorkOrderFields = $WorkOrderFields->get()->toArray();
                   
                          $data_table = [];
                    foreach ($WorkOrderFields as $fields_data) {
                        $field_names = explode('||', $fields_data['field_names']);
                        $field_values = explode('||', $fields_data['field_values']);
                        $field_names_values = (count($field_names) == count($field_values)) ? array_combine($field_names, $field_values) : [];

                        $field_names_values['default'] = '';
                        $data_table[] = array_merge($fields_data, $field_names_values);
                    } 
                     foreach ($data_table as $k => $each_record) {
                        if (isset($each_record['parent_work_order']) && $each_record['parent_work_order'] != null) {
                                if($each_record['parent_work_order']==$woId ){// dd($each_record,$ldonj);
                                    $notice_field_id = \App\Models\NoticeField::select('id')->where("notice_id",$each_record['notice_id'])->where('name',"last_date_on_the_job")->get();
                                     if(!empty($notice_field_id) && $notice_field_id->count()){
                                        if($ldonj==NULL || $ldonj==""){
                                             $update_child = \App\Models\WorkOrderFields::where('workorder_id', $each_record['work_order_id'])->where('notice_field_id',$notice_field_id['0']['id'])->update(['value' => ""]);
                                        }else{
                                             $update_child = \App\Models\WorkOrderFields::where('workorder_id', $each_record['work_order_id'])->where('notice_field_id',$notice_field_id['0']['id'])->update(['value' => $ldonj]);
                                        }
                                       

                                       // ->update(['value' => $ldonj]);   
                                    }
                            }
                        }
                    }
           }
            return redirect()->back();


            //if work order having child orders
            // then update all child orders 
            /* if(!empty($request->notice_id) 
            && !empty($noticeId) 
            && !empty($woId)) {
                //SELECT * FROM `notice_fields`, work_order_fields WHERE `name` LIKE 'parent_work_order' and work_order_fields.notice_field_id = notice_fields.id and work_order_fields.value=29
                $result = NoticeField::with('noticeField')->
                            whereHas( 'noticeField', function($q) use($woId) {
                                return $q->select('workorder_id')->where('value' , $woId);
                            } )->get();
                $childWO = $result->map( function($row, $v) {
                                return $row->noticeField->workorder_id;
                            })->toArray();
                dd($result, $childWO);
                //get last_date_on_job field id
                $res = WorkOrder::with('notice.noticeFields')->whereHas('notice.noticeFields', function($q) {
                    return $q->where('name', 'last_date_on_the_job');
                })->whereIn('id', $childWO)->get();
                dd($res);
                $res->map( function($row, $v) use($ldonj) {
                    $row->notice->noticeFields->map( function($a, $b) use($row, $ldonj) {
                        if($a->name == 'last_date_on_the_job') {
                            WorkOrderFields::where('workorder_id', $row->id)
                                            ->where('notice_id', $row->notice_id,)
                                            ->where('notice_field_id', $a->id)
                                            ->update(['value' => $ldonj]);
                        }
                    });
                });             
            }
            return redirect()->back();
            */
        } catch(\Exception $e) {
            return redirect()->back()->with('error', 'Error while processing.'.$e->getMessage());
        } 
        
    }


    /*
     * Insert/update last date on job & to its respective child work orders
     * 
     */
    public function saveClerkCourtRecordedDate(Request $request,$cyo) {
        $woId = $request->wo_id;
        $ldonj = $request->ccrd;
        $noticeId = $request->notice_id;
        try {
            if($cyo=="1"){
                if(!empty($request->wo_field_id)) {
                    //update by work order field id
                    if($ldonj==NULL || $ldonj==""){
                        \App\Models\Cyo_WorkOrder_fields::where('id', $request->wo_field_id)
                                        ->update(['value' => ""]);
                    }else{
                        \App\Models\Cyo_WorkOrder_fields::where('id', $request->wo_field_id)
                                        ->update(['value' => $ldonj]);
                    }
                  
                } else if(!empty($request->notice_id) 
                        && !empty($noticeId) 
                        && !empty($woId)) {
                    $wof = new \App\Models\Cyo_WorkOrder_fields();
                    $wof->workorder_id = $woId;
                    $wof->notice_id = $request->notice_id;
                    $wof->notice_field_id = $request->notice_field_id;
                    if($ldonj==NULL || $ldonj==""){
                    $wof->value = "";
                    }else{
                    $wof->value = $ldonj;  
                    }
                    $wof->save();
                } 
                 $WorkOrderFields = \App\Models\Cyo_WorkOrder_fields::select('notices.id as notice_id','cyo__work_orders.id as work_order_id', DB::raw("(GROUP_CONCAT(notice_fields.name SEPARATOR '||')) as `field_names`"), DB::raw("(GROUP_CONCAT(value SEPARATOR '||')) as `field_values`"))
                        ->rightjoin('notice_fields', 'notice_fields.id', '=', 'cyo__work_order_fields.notice_field_id')
                        ->join('cyo__work_orders', 'cyo__work_orders.id', '=', 'cyo__work_order_fields.workorder_id')
                        ->leftjoin('notices', 'notices.id', '=', 'cyo__work_orders.notice_id')
                        ->groupBy('cyo__work_order_fields.workorder_id', 'cyo__work_order_fields.notice_id');
                         $WorkOrderFields = $WorkOrderFields->get()->toArray();
                   
                          $data_table = [];
                    if (isset($WorkOrderFields) && !empty($WorkOrderFields)) {
                        foreach ($WorkOrderFields as $fields_data) {
                           
                            $field_names = explode('||', $fields_data['field_names']);
                            $field_values = explode('||', $fields_data['field_values']);
                            $field_names_values = (count($field_names) == count($field_values)) ? array_combine($field_names, $field_values) : [];
                            //$field_names_values = array_combine($field_names, $field_values);
                            $field_names_values['default'] = '';
                            $data_table[] = array_merge($fields_data, $field_names_values);
                        
                        }
                     }    
                     foreach ($data_table as $k => $each_record) {
                        if (isset($each_record['parent_work_order']) && $each_record['parent_work_order'] != null) {
                                if($each_record['parent_work_order']==$woId){
                                    $notice_field_id = \App\Models\NoticeField::select('id')->where("notice_id",$each_record['notice_id'])->where('name',"clerk_of_court_recorded_date")->get();
                                     if(!empty($notice_field_id) && $notice_field_id->count()){
                                        if($ldonj==NULL || $ldonj==""){
                                            $update_child = \App\Models\Cyo_WorkOrder_fields::where('workorder_id', $each_record['work_order_id'])->where('notice_field_id',$notice_field_id['0']['id'])->update(['value' => ""]);
                                        }else{
                                            $update_child = \App\Models\Cyo_WorkOrder_fields::where('workorder_id', $each_record['work_order_id'])->where('notice_field_id',$notice_field_id['0']['id'])->update(['value' => $ldonj]);
                                        }
                                       // ->update(['value' => $ldonj]);   
                                    }
                            }
                        }
                    }
           }else{
                if(!empty($request->wo_field_id)) {
                    //update by work order field id
                    if($ldonj==NULL || $ldonj==""){
                        WorkOrderFields::where('id', $request->wo_field_id)
                                        ->update(['value' => ""]);
                    }else{
                        WorkOrderFields::where('id', $request->wo_field_id)
                                        ->update(['value' => $ldonj]);
                    }
                } else if(!empty($request->notice_id) 
                        && !empty($noticeId) 
                        && !empty($woId)) {
                    $wof = new WorkOrderFields();
                    $wof->workorder_id = $woId;
                    $wof->notice_id = $request->notice_id;
                    $wof->notice_field_id = $request->notice_field_id;
                    if($ldonj==NULL || $ldonj==""){
                        $wof->value = "";
                    }else{
                        $wof->value = $ldonj; 
                    }
                   
                    $wof->save();
                } 
                 $WorkOrderFields = \App\Models\WorkOrderFields::select('notices.id as notice_id','work_orders.id as work_order_id', DB::raw("(GROUP_CONCAT(notice_fields.name SEPARATOR '||')) as `field_names`"), DB::raw("(GROUP_CONCAT(value SEPARATOR '||')) as `field_values`"))
                        ->rightjoin('notice_fields', 'notice_fields.id', '=', 'work_order_fields.notice_field_id')
                        ->join('work_orders', 'work_orders.id', '=', 'work_order_fields.workorder_id')
                        ->leftjoin('notices', 'notices.id', '=', 'work_orders.notice_id')
                        ->groupBy('work_order_fields.workorder_id', 'work_order_fields.notice_id');
                        $WorkOrderFields = $WorkOrderFields->get()->toArray();
                   
                          $data_table = [];
                    foreach ($WorkOrderFields as $fields_data) {
                        $field_names = explode('||', $fields_data['field_names']);
                        $field_values = explode('||', $fields_data['field_values']);
                        $field_names_values = (count($field_names) == count($field_values)) ? array_combine($field_names, $field_values) : [];

                        $field_names_values['default'] = '';
                        $data_table[] = array_merge($fields_data, $field_names_values);
                    } 
                     foreach ($data_table as $k => $each_record) {
                        if (isset($each_record['parent_work_order']) && $each_record['parent_work_order'] != null) {
                                if($each_record['parent_work_order']==$woId ){// dd($each_record,$ldonj);
                                    $notice_field_id = \App\Models\NoticeField::select('id')->where("notice_id",$each_record['notice_id'])->where('name',"clerk_of_court_recorded_date")->get();
                                     if(!empty($notice_field_id) && $notice_field_id->count()){
                                        if($ldonj==NULL || $ldonj==""){
                                             $update_child = \App\Models\WorkOrderFields::where('workorder_id', $each_record['work_order_id'])->where('notice_field_id',$notice_field_id['0']['id'])->update(['value' => ""]);
                                        }else{
                                             $update_child = \App\Models\WorkOrderFields::where('workorder_id', $each_record['work_order_id'])->where('notice_field_id',$notice_field_id['0']['id'])->update(['value' => $ldonj]);
                                        }
                                    }
                            }
                        }
                    }
           }
            return redirect()->back();

        } catch(\Exception $e) {
            return redirect()->back()->with('error', 'Error while processing.'.$e->getMessage());
        } 
        
    }
}
