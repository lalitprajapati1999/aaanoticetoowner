<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\AddNote;
use Illuminate\Support\Facades\Mail;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Session;
use App\Models\WorkOrder;
use Auth;
use DB;
use Alert;
use to_date;
use Collection;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use App\Mail\WorkOrderNote;
use PDF;
use Storage;
use Illuminate\Support\Str;
use App\Mail\CyoDocPaymentInvoiceMail;


class OwnWorkOrderController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $data['states'] = \App\Models\State::select()->where('status', 1)->orderBy('name', 'asc')->get();
        return view('customer.create_your_own.dashboard', $data);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function notice_list(Request $request) {
        /* check  customer_subscriptions */
        $cutomer_subscription_info = \App\Models\Customer_subscription::where('customer_id', Auth::user()->customer->id)->where('end_date', '>=', date('Y-m-d'))->where('status','1')->first();
        if (empty($cutomer_subscription_info)) {
            /* redirect to pricing */
            Session::flash('warning', 'Your subscription for Create your own has been expired, Please subscribe again.');
            return redirect('/pricing#showsubscribe');
        }

        $data['notices'] = \App\Models\Notice::select()->where('state_id', $request->state_id)->where('allow_cyo', 1)->where('status', 1)->orderBy('type', 'desc')->get();
        $data['state_id'] = $request->state_id;
        return view('customer.create_your_own.list', $data);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function WorkOrderHitory() {

        $customer_id = Auth::User()->customer->id;

        $data['account_managers'] = \App\Models\User::select('id', 'name')->join('role_users', 'role_users.user_id', 'users.id')->where('role_users.role_id', 3)->get();
        $data['categories'] = \App\Models\Category::select()->orderBy('name', 'ASC')->get();
          $data['customers'] = \App\Models\Customer::orderBy('company_name', 'ASC')->get();
         $data['states'] = \App\Models\State::select()->orderBy('name')->get();
         $data['contracted_by'] = \App\Models\Contact::select()->where('customer_id', Auth::user()->customer->id)->groupBy('company_name')->orderBy('company_name', 'ASC')->get();
         $data['extraTitle'] = ' - Create Your Own';
        $data['note_emails'] = get_user_notice_email(Auth::user()->customer->id);
        return view('customer.create_your_own.work_order_history', $data);
    }

    public function get_workorders(Request $request) {
        $WorkOrderFields = \App\Models\Cyo_WorkOrder_fields::select('notices.master_notice_id as master_notice_id','notices.name as notice_name', 'notices.is_rescind as rescind_notice', 'cyo__work_orders.is_rescind as rescind_work_order', 'notices.type', 'cyo__work_orders.parent_id', 'users.name as customer_name', 'cyo__work_orders.created_at', 'cyo__work_orders.user_id', 'cyo__work_orders.customer_id', 'cyo__work_orders.notice_id as notice_id', 'usr.name as account_manager_name', 'cyo__work_orders.status', 'cyo__work_orders.user_id', 'cyo__work_orders.account_manager_id', 'cyo__work_order_fields.workorder_id','customers.company_name', 'cyo__work_order_fields.notice_id', DB::raw("(GROUP_CONCAT(notice_fields.name SEPARATOR '||')) as `field_names`"), DB::raw("(GROUP_CONCAT(value SEPARATOR '||')) as `field_values`"))
                        ->rightjoin('notice_fields', 'notice_fields.id', '=', 'cyo__work_order_fields.notice_field_id')
                        ->leftjoin('cyo__work_orders', 'cyo__work_orders.id', '=', 'cyo__work_order_fields.workorder_id')
                        ->leftjoin('notices', 'notices.id', '=', 'cyo__work_orders.notice_id')
                        ->leftjoin('users', 'users.id', '=', 'cyo__work_orders.user_id')
                        ->leftjoin('users as usr', 'usr.id', '=', 'cyo__work_orders.account_manager_id')
                        ->leftjoin('customers', 'customers.id', '=', 'cyo__work_orders.customer_id')
                        ->groupBy('cyo__work_order_fields.workorder_id', 'cyo__work_order_fields.notice_id')->where('cyo__work_orders.customer_id', Auth::user()->customer->id);

        if (($request->has('from_date')) && ($request->get('from_date') != NULL)) {
            $WorkOrderFields->whereDate('cyo__work_orders.created_at', '>=', date('Y-m-d', strtotime($request->get('from_date'))));
        }
        if (($request->has('to_date')) && ($request->get('to_date') != NULL)) {
            $WorkOrderFields->whereDate('cyo__work_orders.created_at', '<=', date('Y-m-d', strtotime($request->get('to_date'))));
        }
        $WorkOrderFields = $WorkOrderFields->get()->toArray();

        $data_table = [];
        if (isset($WorkOrderFields) && !empty($WorkOrderFields)) {
            foreach ($WorkOrderFields as $fields_data) {
                $field_names = explode('||', $fields_data['field_names']);
                $field_values = explode('||', $fields_data['field_values']);
                $recipients = \App\Models\Cyo_WorkOrder_Recipient::where('work_order_id', '=', $fields_data['workorder_id'])
                        ->whereIn('category_id', [1, 2])
                        ->select('category_id', 'name')
                        ->orderBy('category_id', 'asc')
                        ->get();
                $owner = 0;
                $general_contracted = 0;
                if (isset($recipients) && !empty($recipients)) {
                    array_push($field_names, 'project_owner');
                    array_push($field_names, 'general_contracted');
                    foreach ($recipients AS $k => $v) {

                        if (isset($v->category_id) && $v->category_id == 1) {
                            if ($owner == 0) {
                                array_push($field_values, $v->name);
                                $owner = 1;
                            }
                        }
                        if (isset($v->category_id) && $v->category_id == 2) {
                            if ($general_contracted == 0) {
                                array_push($field_values, $v->name);
                                $general_contracted = 1;
                            }
                        }
                    }
                    if ($owner == 0) {
                        array_push($field_values, "");
                    }
                    if ($general_contracted == 0) {
                        array_push($field_values, "");
                    }
                }
                $field_names_values = array_combine($field_names, $field_values);
                $field_names_values['default'] = '';
                $data_table[] = array_merge($fields_data, $field_names_values);
            }
        }
        foreach ($data_table as $k => $each_record) {
            if (isset($each_record['city']) && $each_record['city'] != null) {
                $city_id = \App\Models\City::find($each_record['city']);
                $data_table[$k]['city'] = $city_id->name;
            }
        }
        foreach ($data_table as $k => $each_record) {
            if (isset($each_record['state']) && $each_record['state'] != null) {
                $state_id = \App\Models\State::find($each_record['state']);
                $data_table[$k]['state'] = $state_id->name;
            }
        }
        foreach ($data_table as $k => $each_record) {
            if (isset($each_record['rescind_work_order']) && $each_record['rescind_work_order'] == 1) {
                $data_table[$k]['notice_name'] = $data_table[$k]['notice_name'].' Amendment';
            }
        }
         foreach ($data_table as $k => $each_record) {
            if (isset($each_record['notice_id']) && $each_record['notice_id'] != null) {
                $notice_state_id = \App\Models\Notice::where('id', '=', $each_record['notice_id'])->first();

                $state_id = \App\Models\State::find($notice_state_id->state_id);
                $data_table[$k]['project_state'] = $state_id->name;
            }
        }

        //Calculationo fof due date 40 days from job start date
        //and row highlighted at 30 days
               if (isset($data_table) && !empty($data_table)) {
            foreach ($data_table As $key => $value) {
                $master_notice_id = $value['master_notice_id'];
                foreach ($value AS $k1 => $v1) {
                    
                    if ($k1 == 'job_start_date' && $v1!=""){
                      if($master_notice_id==config('constants.MASTER_NOTICE')['NTO']['ID']){
                        $duedateDays = 40 . 'days';
                        $highlightDays = 30 . 'days';
                        $data_table[$key]['due_date'] = date('m/d/Y', strtotime('+ ' . $duedateDays, strtotime($v1)));
                        $data_table[$key]['highligted_date'] = date('m/d/Y', strtotime('+ ' . $highlightDays, strtotime($v1)));
                      }
                    }
                    if ($k1 == 'clerk_of_court_recorded_date' && $v1!=""){
                    if($master_notice_id==config('constants.MASTER_NOTICE')['COL']['ID']){
                      $duedateDays = 270 . 'days';
                      $highlightDays = 270 . 'days';
                      $data_table[$key]['due_date'] = date('m/d/Y', strtotime('+ ' . $duedateDays, strtotime($v1)));
                      $data_table[$key]['highligted_date'] = date('m/d/Y', strtotime('+ ' . $highlightDays, strtotime($v1)));
                    }
                    }else if($k1 == 'clerk_of_court_recorded_date' && $v1==""){
                        $data_table[$key]['clerk_of_court_recorded_date'] = 'NA';
                    }
                    if(!isset($data_table[$key]['amount_due'])){
                        $data_table[$key]['amount_due'] = 'NA';
                    }
                    if(!isset($data_table[$key]['clerk_of_court_recorded_date'])){
                        $data_table[$key]['clerk_of_court_recorded_date'] = 'NA';
                    }
                    /*if ($k1 == 'last_date_on_the_job'){
                        if($master_notice_id==config('constants.MASTER_NOTICE')['COL']['ID'] || $master_notice_id==config('constants.MASTER_NOTICE')['NPN']['ID']){
                         // $duedateDays = 270 . 'days';
                          $highlightDays = 70 . 'days';
                          //$data_table[$key]['due_date'] = date('m/d/Y', strtotime('+ ' . $duedateDays, strtotime($v1)));
                          $data_table[$key]['highligted_date'] = date('m/d/Y', strtotime('+ ' . $highlightDays, strtotime($v1)));
                        }
                    }*/
                }
            }
        } 
        //dd($data_table);
        return DataTables::of($data_table)
                        ->addColumn('actions', function($workorder) {
                            $generatePdf = '';
                            //if( in_array($workorder['master_notice_id'], [ config('constants.MASTER_NOTICE')['SOL']['ID'], config('constants.MASTER_NOTICE')['COL']['ID'] ])) {
                            if( in_array($workorder['master_notice_id'], [ config('constants.MASTER_NOTICE')['FROL']['ID']])) {
                                $generatePdf .= "<option value='generate_work_order_pdf'>Generate PDF</option>";
                            }
                            //return view('customer.create_your_own.action', compact('workorder'))->render();
                            if ($workorder['status'] == 0) {
                                return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                                      <option value=''>Select</option>
                                      <option value='cancel_work_order'>Cancel This Work Order</option>
                                      <option value='add_note'>Add Note</option>
                                      <option value='continue_working'>Continue Working On This Work Order </option>
                                      <option value='view_requested_work_order'>View Request Work Order</option>
                                      $generatePdf
                                   </select>";
                            } elseif ($workorder['status'] == 1) {
                                if ($workorder['rescind_notice'] == 1 && $workorder['rescind_work_order'] == 1) {
                                    return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                                      <option value=''>Select</option>
                                      <option value='cancel_work_order'>Cancel This Work Order</option>
                                       <option value='add_note'>Add Note</option>
                                        <option value='view_requested_work_order'>View Request Work Order</option>
                                        $generatePdf
                                  </select>";
                                } else {
                                    return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                                        <option value=''>Select</option>
                                        <option value='cancel_work_order'>Cancel This Work Order</option>
                                        <option value='duplicate_work_order'>Duplicate This Work Order</option>
                                        <option value='proceed_work_order'>Proceed With Work Order Order</option>
                                        <option value='add_note'>Add Note</option>
                                        <option value='view_requested_work_order'>View Request Work Order</option>
                                        $generatePdf
                                    </select>";
                                }
                            } elseif ($workorder['status'] == 3) {
                                    return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                                      <option value=''>Select</option>
                                      <option value='add_note'>Add Note</option>
                                      <option value='view_requested_work_order'>View Request Work Order</option>
                                      $generatePdf
                                   </select>";
                                } elseif ($workorder['status'] == 2 || $workorder['status'] == 4) {
                                    if ($workorder['rescind_notice'] == 1 && $workorder['rescind_work_order'] == 1) {
                                        return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                                          <option value=''>Select</option>
                                          <option value='cancel_work_order'>Cancel This Work Order</option>
                                          <option value='add_note'>Add Note</option>
                                          <option value='continue_working'>Continue Working On This Work Order</option>
                                          <option value='view_requested_work_order'>View Request Work Order</option>
                                          $generatePdf
                                       </select>";
                                    } else {
                                    return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                                      <option value=''>Select</option>
                                      <option value='cancel_work_order'>Cancel This Work Order</option>
                                         <option value='duplicate_work_order'>Duplicate This Work Order</option>
                                      <option value='add_note'>Add Note</option>
                                      <option value='continue_working'>Continue Working On This Work Order</option>
                                      <option value='view_requested_work_order'>View Request Work Order</option>
                                      $generatePdf
                                   </select>";
                                }
                            } elseif ($workorder['status'] == 3 || $workorder['status'] == 4) {
                                $selectDropdown = '';
                                
                                if ($workorder['rescind_notice'] == 1 && $workorder['rescind_work_order'] == 1) {
                                    return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                                      <option value=''>Select</option>
                                      <option value='add_note'>Add Note</option>
                                      <option value='continue_working'>Continue Working On This Work Order</option>
                                      <option value='view_requested_work_order'>View Request Work Order</option>
                                      $generatePdf";
                                } else { 
                                    return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                                        <option value=''>Select</option>
                                        <option value='duplicate_work_order'>Duplicate This Work Order</option>
                                        <option value='add_note'>Add Note</option>
                                        <option value='continue_working'>Continue Working On This Work Order</option>
                                        <option value='view_requested_work_order'>View Request Work Order</option>
                                        $generatePdf";
                                }
                                if($workorder['master_notice_id'] == config('constants.MASTER_NOTICE')['FROL']['ID']) {
                                    $selectDropdown .= "<option value='cancel_work_order'>Cancel This Work Order</option>";
                                }
                                $selectDropdown .= "</select>";
                                return $selectDropdown;
                            } elseif ($workorder['status'] == 5) {
                                $selectDropdown = '';
                                
                                if ($workorder['rescind_notice'] == 1 && $workorder['rescind_work_order'] == 1) {
                                    $selectDropdown .= "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                                      <option value=''>Select</option>
                                      <option value='add_note'>Add Note</option>
                                      <option value='view_requested_work_order'>View Request Work Order</option>";
                                } else {
                                    $selectDropdown .= "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                                      <option value=''>Select</option>
                                         <option value='duplicate_work_order'>Duplicate This Work Order</option>
                                      <option value='add_note'>Add Note</option>
                                      <option value='view_requested_work_order'>View Request Work Order</option>";
                                }
                                if($workorder['master_notice_id'] == config('constants.MASTER_NOTICE')['FROL']['ID']) {
                                    $selectDropdown .= "<option value='cancel_work_order'>Cancel This Work Order</option>";
                                }
                                if( in_array($workorder['master_notice_id'], [ config('constants.MASTER_NOTICE')['NPN']['ID'], config('constants.MASTER_NOTICE')['COL']['ID'], config('constants.MASTER_NOTICE')['SOL']['ID'] ])) {
                                    $selectDropdown .= "<option value='generate_work_order_pdf'>Generate PDF</option>";
                                }
                                $selectDropdown .= "</select>";
                                return $selectDropdown;
                            } elseif ($workorder['status'] == 6) {
                                $selectDropdown = '';
                                if($workorder['master_notice_id'] == config('constants.MASTER_NOTICE')['FROL']['ID']) {
                                    $selectDropdown .= "<option value='cancel_work_order'>Cancel This Work Order</option>";
                                }
                                if ($workorder['rescind_notice'] == 1 && $workorder['rescind_work_order'] == 0) {
                                     $selectDropdown = "
                                      <option value='duplicate_work_order'>Duplicate This Work Order</option>
                                       <option value='rescind_work_order'>Rescind This Work Order</option>
                                      <option value='add_note'>Add Note</option>
                                      <option value='view_work_order'>View Completed Work Order</option>
                                      <option value='view_requested_work_order'>View Request Work Order</option>";
                                   
                                } else if ($workorder['rescind_notice'] == 1 && $workorder['rescind_work_order'] == 1) {
                                     $selectDropdown = "
                                      <option value='add_note'>Add Note</option>
                                      <option value='view_work_order'>View Completed Work Order</option>
                                      <option value='view_requested_work_order'>View Request Work Order</option>";
                                  
                                } else {
                                    $selectDropdown = " <option value='cancel_work_order'>Cancel This Work Order</option>
                                      <option value='duplicate_work_order'>Duplicate This Work Order</option>
                                      <option value='add_note'>Add Note</option>
                                      <option value='view_work_order'>View Completed Work Order</option>
                                      <option value='view_requested_work_order'>View Request Work Order</option>";
                                 
                                }
                                if( $workorder['master_notice_id'] == config('constants.MASTER_NOTICE')['COL']['ID']
                                 || $workorder['master_notice_id'] == config('constants.MASTER_NOTICE')['NTO']['ID']) {
                                    $selectDropdown .= "<option value='add_ldonj'>Add Last Date On The Job</option>";
                                }
                                
                                if (
                                    $workorder['master_notice_id'] == config('constants.MASTER_NOTICE')['COL']['ID']
                                ) {
                                    $selectDropdown .= "<option value='add_ccrd'>Add Clerk of Court Recorded date</option>";
                                }

                                if( in_array($workorder['master_notice_id'], [ config('constants.MASTER_NOTICE')['ITL']['ID'], config('constants.MASTER_NOTICE')['NTO']['ID'], config('constants.MASTER_NOTICE')['NPN']['ID'] ])) {
                                    $selectDropdown .= $generatePdf;
                                }
                                $selectDropdown .= "<option value='generate_work_order_pdf'>Generate PDF</option>";
                                return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                                    <option value=''>Select</option>"
                                    .$selectDropdown
                                    ."</select>";
                            
                            } elseif ($workorder['status'] == 9) {
                                $selectDropdown = '';
                                // if ($workorder['rescind_notice'] == 1 && $workorder['rescind_work_order'] == 0) {
                                //     $selectDropdown = "
                                //                       <option value='duplicate_work_order'>Duplicate This Work Order</option>
                                //                        <option value='rescind_work_order'>Rescind This Work Order</option>
                                //                       <option value='add_note'>Add Note</option>
                                //                       <option value='view_requested_work_order'>View Request Work Order</option>";
                                // } else if ($workorder['rescind_notice'] == 1 && $workorder['rescind_work_order'] == 1) {
                                //     $selectDropdown = "
                                //                       <option value='add_note'>Add Note</option>
                                //                       <option value='view_requested_work_order'>View Request Work Order</option>";
                                // } else {
                                //     $selectDropdown = " <option value='cancel_work_order'>Cancel This Work Order</option>
                                //                       <option value='duplicate_work_order'>Duplicate This Work Order</option>
                                //                       <option value='add_note'>Add Note</option>
                                //                       <option value='view_requested_work_order'>View Request Work Order</option>";
                                // }
                                if ( $workorder['master_notice_id'] == config('constants.MASTER_NOTICE')['NTO']['ID']) {
                                    $selectDropdown .= " <option value='add_note'>Add Note</option>";
                                    // $selectDropdown .= "<option value='add_ldonj'>Add Last Date On The Job</option>";
                                    $selectDropdown .= "<option value='cancel_work_order'>Cancel This Work Order</option>";
                                    // $selectDropdown .= "<option value='rescind_work_order'>Rescind This Work Order</option>";
                                    $selectDropdown .= "<option value='duplicate_work_order'>Duplicate This Work Order</option>";
                                    $selectDropdown .= "<option value='continue_working'>Continue Working On This Work Order </option>";
                                    $selectDropdown .= "<option value='view_requested_work_order'>View Request Work Order</option>";
            
                                }
                              
                                $selectDropdown .= "<option value='generate_work_order_pdf'>Generate PDF</option>";
                                return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                                                    <option value=''>Select</option>"
                                    . $selectDropdown
                                    . "</select>";
                
                            } elseif ($workorder['status'] == 7) {
                                $selectDropdown = '';
                                if ($workorder['rescind_notice'] == 1 && $workorder['rescind_work_order'] == 1) {
                                    return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                                      <option value=''>Select</option>
                                      <option value='add_note'>Add Note</option>
                                      <option value='view_requested_work_order'>View Request Work Order</option>";
                                } else {
                                    return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                                      <option value=''>Select</option>
                                         <option value='duplicate_work_order'>Duplicate This Work Order</option>
                                      <option value='add_note'>Add Note</option>
                                      <option value='view_requested_work_order'>View Request Work Order</option>";
                                }
                                
                                if($workorder['master_notice_id'] == config('constants.MASTER_NOTICE')['FROL']['ID']) {
                                    $selectDropdown .= "<option value='cancel_work_order'>Cancel This Work Order</option>$generatePdf";
                                }
                                $selectDropdown .= "</select>";
                                return $selectDropdown;
                            } else {
                                if ($workorder['rescind_notice'] == 1 && $workorder['rescind_work_order'] == 1) {
                                    return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                                      <option value=''>Select</option>
                                         <option value='view_requested_work_order'>View Request Work Order</option>
                                           $generatePdf
                                   </select>";
                                } else {
                                    return "<select name='select' data-id = " . $workorder['workorder_id'] . " onchange='select_action(this.value," . $workorder['workorder_id'] . "," . $workorder['notice_id'] . ")' id='select_action' class='options_list'>
                                      <option value=''>Select</option>
                                         <option value='duplicate_work_order'>Duplicate This Work Order</option>
                                         <option value='view_requested_work_order'>View Request Work Order</option>
                                           $generatePdf
                                   </select>";
                                }
                            }
                        })
                        ->rawColumns(['actions'])
                        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($notice_type, $notice_id, $state_id) {
        /* check  customer_subscriptions */
        $cutomer_subscription_info = \App\Models\Customer_subscription::where('customer_id', Auth::user()->customer->id)->where('end_date', '>=', date('Y-m-d'))->first();
        if (empty($cutomer_subscription_info)) {
            /* redirect to pricing */
            Session::flash('warning', 'Your subscription for Create your own has been expired, Please subscribe again.');
            return redirect('/pricing#showsubscribe');
        }


        $data['is_rescind'] = 0;
        $notice_type = $notice_type;
        $notice_id = $notice_id;
        $state_id = $state_id;
        $data['id'] = 0;
        $data['work_order_cyo_doc_payment'] = 0;
        $data['categories'] = \App\Models\Category::select()->orderBy('name', 'ASC')->get();
        $data['project_roles'] = \App\Models\ProjectRoles::select()->orderBy('name', 'ASC')->get();
        $data['parent_work_order_id'] = \App\Models\WorkOrder::select('id')->get();
        $data['cities'] = \App\Models\City::select()->get();
        $data['countries'] = \App\Models\Country::select()->get();
        $data['states'] = \App\Models\State::select()->orderBy('name', 'asc')->get();
        $data['officers_directors'] = \App\Models\CustomerAgent::where('customer_id', '=', Auth::user()->customer->id)
                ->get();
        $data['projectTypes'] = \App\Models\ProjectType::select()->get();
        //$data['recipients'] = \App\Models\Recipient::select()->get();
        $data['names'] = \App\Models\Contact::select('contacts.id', 'contacts.company_name')
//                ->where('contact_details.type', '=', 0)
                ->where('contacts.customer_id', '=', Auth::user()->customer->id)
//                ->Join('contact_details', 'contact_details.contact_id', '=', 'contacts.id')
                ->orderBy('contacts.id', 'DESC')
                ->get();
        $data['recipients'] = [];
        $data["verified_address_count"] = 0;
        $data['service_types'] = \App\Models\StampsServices::where(['status' => '1'])->get(['description', 'abbr'])->toArray();
        if ($notice_type == 1) {
            $notice = \App\Models\Notice::select('name', 'id', 'is_claim_of_lien','master_notice_id')->where('id', $notice_id)->where('state_id', $state_id)->get()->toArray();
            $data['notice'] = $notice[0];
            $data['notice_id'] = $notice_id;
            $data['notice_fields'] = \App\Models\NoticeField::select()->where('notice_id', $notice_id)->orderBy('sort_order', 'asc')->get();
            $notice = \App\Models\Notice::select('name')->where('id', $notice_id)->get()->toArray();

            $data['tab'] = 'project';
            $data['correction'] = \App\Models\Cyo_WorkOrder_Notes::leftjoin('users', 'users.id', '=', 'cyo__work_order__notes.user_id')
            ->select('cyo__work_order__notes.*','users.name')
                ->where('cyo__work_order__notes.customer_id', '=', Auth::user()->customer->id)
                    ->get();
            $data['extraTitle'] = ' - Create Your Own';
            return view('customer.create_your_own.create', $data);
        } else if ($notice_type == 2) {
            $notice = \App\Models\Notice::select('id', 'name', 'is_claim_of_lien','master_notice_id')->where('id', $notice_id)->where('state_id', $state_id)->get()->toArray(); 

            $data['notice'] = $notice[0];
            $data['notice_id'] = $notice_id;
            $data['notice_fields_section1'] = \App\Models\NoticeField::select()->where('notice_id', $notice_id)->where('section', 1)->where('state_id', $state_id)->orderBy('sort_order', 'asc')->get();
            $data['notice_fields_section2'] = \App\Models\NoticeField::select()->where('notice_id', $notice_id)->where('section', 2)->where('state_id', $state_id)->orderBy('sort_order', 'asc')->get();
            // dd($data['notice_fields_section1']);
            $notice = \App\Models\Notice::select('name')->where('id', $notice_id)->get()->toArray();
            $data['tab'] = 'project';
            $data['correction'] = \App\Models\Cyo_WorkOrder_Notes::leftjoin('users', 'users.id', '=', 'cyo__work_order__notes.user_id')
                ->select('cyo__work_order__notes.*','users.name')
                ->where('cyo__work_order__notes.customer_id', '=', Auth::user()->customer->id)
                    ->get();
            $data['extraTitle'] = ' - Create Your Own';
            return view('customer.create_your_own.create_soft_notice', $data);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {//dd($request->all());
        // $data['is_rescind'] = 0;
        /* check  customer_subscriptions */
        $cutomer_subscription_info = \App\Models\Customer_subscription::where('customer_id', Auth::user()->customer->id)->where('end_date', '>=', date('Y-m-d'))->first();
        if (empty($cutomer_subscription_info)) {
            /* redirect to pricing */
            Session::flash('warning', 'Your subscription for Create your own has been expired, Please subscribe again.');
            return redirect('/pricing#showsubscribe');
        }
        $data['service_types'] = \App\Models\StampsServices::where(['status' => '1'])->get(['description', 'abbr', 'id'])->toArray();
        $data['package_types'] = \App\Models\StampsAddOns::join('stamps_service_package_addon', 'stamps_service_package_addon.add_on', '=', 'stamps_add_ons.id')
                        ->join('stamps_service_type', 'stamps_service_type.id', '=', 'stamps_service_package_addon.service_type')
                        ->join('stamps_packages', 'stamps_packages.id', '=', 'stamps_service_package_addon.package')
                        ->where(['stamps_packages.status' => '1'])
                        ->get([\DB::raw('DISTINCT(stamps_packages.name)'), 'stamps_packages.id'])->toArray();
        $data['add_ons'] = \App\Models\StampsAddOns::join('stamps_service_package_addon', 'stamps_service_package_addon.add_on', '=', 'stamps_add_ons.id')
                        ->join('stamps_service_type', 'stamps_service_type.id', '=', 'stamps_service_package_addon.service_type')
                        ->join('stamps_packages', 'stamps_packages.id', '=', 'stamps_service_package_addon.package')
                        ->where(['stamps_add_ons.status' => '1'])
                        ->where(['stamps_service_type.id' => 1])
                        ->where(['stamps_packages.id' => 2])
                        ->get([\DB::raw('DISTINCT(stamps_add_ons.id)'), 'stamps_add_ons.description'])->toArray();

        if (!empty($data['add_ons'])) {
            for ($counter = 0; $counter < count($data['add_ons']); $counter++) {
                $prohibited = \App\Models\StampsAddOnsProhibited::join('stamps_add_ons', 'stamps_add_ons_prohibited.prohibited', '=', 'stamps_add_ons.id')
                                ->where(['stamps_add_ons_prohibited.add_on' => $data['add_ons'][$counter]['id']])
                                ->pluck('prohibited')->toArray();

                $required = \App\Models\StampsAddOnsRequired::join('stamps_add_ons', 'stamps_add_ons_required.required', '=', 'stamps_add_ons.id')
                                ->where(['stamps_add_ons_required.add_on' => $data['add_ons'][$counter]['id']])
                                ->pluck('required')->toArray();
                $data['add_ons'][$counter]['prohibited'] = json_encode($prohibited);
                $data['add_ons'][$counter]['required'] = json_encode($required);
            }
        }
        $data['label_generated_count'] = 0;
        //$fields = array_merge($request->except(['_token', 'continue', 'type', 'notice_id', 'work_order_attachment', 'file_name', 'doc_input', 'document_type', 'remove_doc', 'recipient_county_id', 'visibility', 'recipient_notary_county_id', 'recipient_project_county_id', 'recipient_recorded_county_id']));
        $fields = array_merge($request->except(['_token', 'continue', 'type', 'notice_id', 'edit_soft_notices_attachment', 'work_order_attachment', 'file_name', 'doc_input', 'hidden_city_val', 'duplicate', 'remove_doc', 'parent_work_order_previous', 'recipient_county_id', 'contracted_by_exist', 'document_type', 'work_order_attachment', 'visibility', 'recipient_notary_county_id', 'recipient_project_county_id', 'recipient_recorded_county_id','work_order_attachment_id','edit','remove']));

        $fieldsData = [];
        $values = [];
        $i = 0;
        foreach ($fields as $key => $value) {
            $field = explode("_", $key);
            $fieldsData[$i] = $field['0'];
            $values[$i] = $value;
            $i++;
        }
        $customerId = Auth::user()->customer->id;
        $account_manager_id = Auth::user()->customer->account_manager_id;
        $userId = Auth::user()->id;

       // dd($request->all());

        if ($request->continue == 'continue') {
            $result1 = \App\Models\Cyo_WorkOrders::create([
                        'notice_id' => $request->notice_id,
                        'customer_id' => $customerId,
                        'order_no' => str_random(5),
                        'user_id' => $userId,
                        'status' => '1',
                        'account_manager_id' => $account_manager_id,
            ]);
            $data['status'] = 'request';
            Session::flash('success', 'Your work order # ' . $result1->id . ' has been successfully created. <a href="' . url('customer/create-your-own/view/' . $result1->id) . '">Click here to print.</a>');
        } else {
            $result1 = \App\Models\Cyo_WorkOrders::create([
                        'notice_id' => $request->notice_id,
                        'customer_id' => $customerId,
                        'order_no' => str_random(5),
                        'user_id' => $userId,
                        'status' => '0',
                        'account_manager_id' => $account_manager_id,
            ]);
            $data['status'] = 'draft';
            Session::flash('success', 'Your work # ' . $result1->id . ' has been saved as a draft you must complete Work Order and submit at a later time.<a href="' . url('customer/create-your-own/view/' . $result1->id) . '">Click here to print.</a>');
        }
    if($request->work_order_attachment_id[0]!=null){
        $work_order_attachment_id = $request->work_order_attachment_id;
        $work_order_attachment_id = explode(',', $work_order_attachment_id[0]);
        //dd($work_order_attachment_id);
        if(!empty($work_order_attachment_id)){
        foreach ($work_order_attachment_id as $key => $value) {
           $work_order_attachment_obj =  \App\Models\Cyo_WorkOrder_Attachment::find($value);
           if(!empty($work_order_attachment_obj)){
           $work_order_attachment_obj->work_order_id = $result1->id;
           $work_order_attachment_obj->save();
         }
        }
      }
    }
//dd($request->all());
        if ($result1) {
//dd($request->work_order_attachment);
            $attachment = explode(',|,',trim($request->work_order_attachment[0],'|,'));
          // dd($attachment);
            if (isset($attachment) && $attachment != null) {
                foreach ($attachment AS $k => $v) {
                  $v = trim($v,'|,');
                  if($v!= '|' && $v!='|,|'){
                    if (isset($request->remove_doc) && !empty($request->remove_doc) && $request->remove_doc[0] != '') {
                        $remove_doc = explode(',', $request->remove_doc[0]);
//dd($remove_doc,$attachment);
                        if ($v != '') {
                            $each_attachment = explode(',', $v);
                            if (!in_array($each_attachment[4], $remove_doc)) {
                                if ($v != '') {
                                    $work_order_attachment_obj = new \App\Models\Cyo_WorkOrder_Attachment();
                                    $work_order_attachment_obj->work_order_id = $result1->id;
                                    $work_order_attachment_obj->type = $each_attachment[0];
                                    $work_order_attachment_obj->title = $each_attachment[1];
                                    $work_order_attachment_obj->file_name = $each_attachment[2];
                                    $work_order_attachment_obj->original_file_name = $each_attachment[3];
                                    $work_order_attachment_obj->save();
                                
                            }
                        }
                      }
                    } else {
                        if ($v != '') {//dd('hi');
                            $each_attachment = explode(',', $v);
                            $work_order_attachment_obj = new \App\Models\Cyo_WorkOrder_Attachment();
                            $work_order_attachment_obj->work_order_id = $result1->id;
                            $work_order_attachment_obj->type = $each_attachment[0];
                            $work_order_attachment_obj->title = $each_attachment[1];
                            $work_order_attachment_obj->file_name = $each_attachment[2];
                            $work_order_attachment_obj->original_file_name = $each_attachment[3];
                            $work_order_attachment_obj->save();
                        }
                    }
                }
              }
            }

//dd('out');
            foreach ($fields as $key => $value) {
                $field = explode("_", $key);

                if (isset($field[3])) {
                    $check_field = $field[1] . '_' . $field[2] . '_' . $field[3];

                    if ($check_field == 'parent_work_order') {
                        $parent_work_order = $value;
                        $parent_work_order_previous = $request->parent_work_order_previous;
                        if ($parent_work_order_previous != $parent_work_order) {
                            //If parent work order is selected document of parent work order will inserted
                            if ($parent_work_order != '') {
                                if (isset($request->remove_doc) && !empty($request->remove_doc) && $request->remove_doc[0] != '') {
                                  $previous_attachments = \App\Models\Cyo_WorkOrder_Attachment::where('work_order_id', '=', $parent_work_order)->get();
                                    // $previous_attachments = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $parent_work_order)->get();
//dd($previous_attachments);
                                    if (isset($previous_attachments) && count($previous_attachments) > 0) {
                                        foreach ($previous_attachments AS $k1 => $a1) {

                                          /*  $each_remove_doc = explode(',', $request->remove_doc[0]);
                                            foreach ($each_remove_doc As $k => $each) {
                                                $explode_v = explode('_', $each);
                                                $count = count($explode_v);
                                                if($count>1){
                                                $each_remove_doc[$k] = $explode_v[$count - 3] . '_' . $explode_v[$count - 2] . '_' . $explode_v[$count - 1];
                                              }
                                            }

                                            $true = !in_array($a1->file_name, $each_remove_doc);*/
                                            $true = "true";

                                            if (isset($true) && $true != '') {

                                                $cyo_work_order_attachment_obj = new \App\Models\Cyo_WorkOrder_Attachment();
                                                $cyo_work_order_attachment_obj->work_order_id = $result1->id;
                                                $cyo_work_order_attachment_obj->type = $a1->type;
                                                $cyo_work_order_attachment_obj->title = $a1->title;
                                                if(!empty($a1->file_name)){
                                                        $cyo_work_order_attachment_obj->file_name = $result1->id . '_' . $a1->file_name;
                                                        }else{
                                                         $cyo_work_order_attachment_obj->file_name = "";
                                                        }
                                                        if(!empty($a1->original_file_name)){
                                                        $cyo_work_order_attachment_obj->original_file_name = $a1->original_file_name;
                                                        }else{
                                                           $cyo_work_order_attachment_obj->original_file_name = "";
                                                        }
                                                if (Auth::user()->hasRole('account-manager')) {
                                                    $cyo_work_order_attachment_obj->visibility = $a1->visibility;
                                                } else {
                                                    $cyo_work_order_attachment_obj->visibility = 0;
                                                }
                                                $cyo_work_order_attachment_obj->save();
                                                if (file_exists(public_path() . '/attachment/cyo_work_order_document/parent_document/' . $a1->file_name) && !empty($a1->file_name)) {
                                                    rename(public_path() . '/attachment/cyo_work_order_document/parent_document/' . $a1->file_name, public_path() . '/attachment/cyo_work_order_document/parent_document/' . $cyo_work_order_attachment_obj->file_name);
                                                    \File::copy(public_path() . '/attachment/cyo_work_order_document/parent_document/' . $cyo_work_order_attachment_obj->file_name, public_path() . '/attachment/cyo_work_order_document/' . $cyo_work_order_attachment_obj->file_name);
                                                    unlink(public_path() . '/attachment/cyo_work_order_document/parent_document/' . $cyo_work_order_attachment_obj->file_name);
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    $remove_doc = explode(',', $request->remove_doc[0]);
                                    $previous_attachments = \App\Models\Cyo_WorkOrder_Attachment::where('work_order_id', '=', $parent_work_order)->get();

                                    if (isset($previous_attachments) && count($previous_attachments) > 0) {
                                        foreach ($previous_attachments AS $k1 => $a1) {
                                            $cyo_work_order_attachment_obj = new \App\Models\Cyo_WorkOrder_Attachment();
                                            $cyo_work_order_attachment_obj->work_order_id = $result1->id;
                                            $cyo_work_order_attachment_obj->type = $a1->type;
                                            $cyo_work_order_attachment_obj->title = $a1->title;
                                             if(!empty($a1->file_name)){
                                                        $cyo_work_order_attachment_obj->file_name = $result1->id . '_' . $a1->file_name;
                                                        }else{
                                                         $cyo_work_order_attachment_obj->file_name = " ";
                                                        }
                                                        if(!empty($a1->original_file_name)){
                                                        $cyo_work_order_attachment_obj->original_file_name = $a1->original_file_name;
                                                        }else{
                                                           $cyo_work_order_attachment_obj->original_file_name = "";
                                                        }
                                            if (Auth::user()->hasRole('account-manager')) {
                                                $cyo_work_order_attachment_obj->visibility = $a1->visibility;
                                            } else {
                                                $cyo_work_order_attachment_obj->visibility = 0;
                                            }
                                            $cyo_work_order_attachment_obj->save();

                                            if (file_exists(public_path() . '/attachment/cyo_work_order_document/parent_document/' . $a1->file_name) && !empty($a1->file_name)) {
                                                rename(public_path() . '/attachment/cyo_work_order_document/parent_document/' . $a1->file_name, public_path() . '/attachment/cyo_work_order_document/parent_document/' . $cyo_work_order_attachment_obj->file_name);
                                                \File::copy(public_path() . '/attachment/cyo_work_order_document/parent_document/' . $cyo_work_order_attachment_obj->file_name, public_path() . '/attachment/cyo_work_order_document/' . $cyo_work_order_attachment_obj->file_name);
                                                unlink(public_path() . '/attachment/cyo_work_order_document/parent_document/' . $cyo_work_order_attachment_obj->file_name);
                                            }
                                        }
                                    }
                                }
                                $data['parent_work_order'] = $parent_work_order;
                                $first_category = \App\Models\Category::where('name', 'Contracted By')->first();
                                /* save parent work order recipient  */
                                if (isset($request->contracted_by_exist)) {
                                    $parent_work_order_recipients = \App\Models\Cyo_WorkOrder_Recipient::select()->where('work_order_id', $parent_work_order)->where('category_id', '!=', $first_category->id)->get();
                                } else {
                                    $parent_work_order_recipients = \App\Models\Cyo_WorkOrder_Recipient::select()->where('work_order_id', $parent_work_order)->get();
                                }
                                \App\Models\Cyo_WorkOrder_Recipient::where( 'work_order_id', $result1->id)->where('parent_work_order','!=',NULL)->delete();
                                if (isset($parent_work_order_recipients) && count($parent_work_order_recipients) > 0) {
                                    foreach ($parent_work_order_recipients As $k => $each_parent_recipient) {
                                        $save_recipients = \App\Models\Cyo_WorkOrder_Recipient::create([
                                                    'work_order_id' => $result1->id,
                                                    'category_id' => $each_parent_recipient->category_id,
                                                    'name' => $each_parent_recipient->name,
                                                    'contact' => $each_parent_recipient->contact,
                                                    'address' => $each_parent_recipient->address,
                                                    'city_id' => $each_parent_recipient->city_id,
                                                    'state_id' => $each_parent_recipient->state_id,
                                                    'zip' => $each_parent_recipient->zip,
                                                    'email' => $each_parent_recipient->email,
                                                    'attn' => $each_parent_recipient->attn,
                                                    'parent_work_order'=> $parent_work_order,
                                                ])->id;

                                        //Check if the recipient address is usps verified, if yes copy the usps address record too for the new recipient
                                        $cyo_usps_address = \App\Models\Cyo_UspsAddress::where(['recipient_id' => $each_parent_recipient->id])->get()->toArray();
                                        if (!empty($cyo_usps_address[0])) {
                                            $cyo_usps_address[0]['id'] = '';
                                            $cyo_usps_address[0]['recipient_id'] = $save_recipients;
                                            \App\Models\Cyo_UspsAddress::insert($cyo_usps_address[0]);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                //Insert contracted by is first recipient
                if (isset($field[2])) {
                    $check_contracted_by_field = $field[1] . '_' . $field[2];
                    if ($check_contracted_by_field == 'contracted_by') {
                        $first_category = \App\Models\Category::where('name', 'Contracted By')->first();
                        $contracted_by = $value;
//dd( $first_category->id);
                        if ($contracted_by != "") {
                            /* check contracted by is exist or not */
                            $contracted_by_check = \App\Models\Contact::where('company_name', $contracted_by)->first();
                            if (empty($contracted_by_check)) {
                                $contact = \App\Models\Contact::create([
                                            'customer_id' => Auth::user()->customer->id,
                                            'company_name' => $contracted_by,
                                ]);
                            }
                        }

                        $contactdetails = \App\Models\Contact::where('company_name', '=', $contracted_by)
                                ->where('customer_id', '=', $customerId)
                                ->first();

                        if (isset($contactdetails) && $contactdetails != null) {

                            /* check contract details exit or not */
                            $check_contract_recipient_exist = \App\Models\Cyo_WorkOrder_Recipient::where(['category_id' => $first_category->id, 'work_order_id' => $result1->id])->first();
                            /* if not empty delete previouse record */
                            if (!empty($check_contract_recipient_exist)) {
                                if ($check_contract_recipient_exist->name != $contracted_by) {
                                    $contract_recipient_delete = \App\Models\Cyo_WorkOrder_Recipient::where(['category_id' => $first_category->id, 'work_order_id' => $result1->id])->where('id', '!=', $request->recipient_id)->delete();
                                    $check_contract_recipient_exist = [];
                                }
                            }
                            if (empty($check_contract_recipient_exist)) {
                                $Recipient = \App\Models\Cyo_WorkOrder_Recipient::create([
                                            'work_order_id' => $result1->id,
                                            'category_id' => $first_category->id,
                                            'name' => $contactdetails->company_name,
                                            'contact' => $contactdetails->phone,
                                            'address' => $contactdetails->mailing_address,
                                            'city_id' => $contactdetails->city_id,
                                            'state_id' => $contactdetails->state_id,
                                            'zip' => $contactdetails->zip,
                                            'email' => $contactdetails->email,
                                            'attn' => $contactdetails->attn
                                ]);
                            }
                        }
                    }
                }
            }
        }

        $work_order_id = $result1->id;

        foreach ($fieldsData as $key => $value) {
            if ($values[$key] == NULL) {
                $result2 = \App\Models\Cyo_WorkOrder_fields::create([
                            'workorder_id' => $work_order_id,
                            'notice_id' => $request->notice_id,
                            'notice_field_id' => $value,
                            'value' => '',
                ]);
            } else
                $result2 = \App\Models\Cyo_WorkOrder_fields::create([
                            'workorder_id' => $work_order_id,
                            'notice_id' => $request->notice_id,
                            'notice_field_id' => $value,
                            'value' => nl2br($values[$key]),
                ]);
        }

        $arr=array_filter($fields, function($k) {
           return strpos($k, 'last_date_on_the_job')>0;
     }, ARRAY_FILTER_USE_KEY);
      $last_date_on_the_job_val=array_pop($arr);
      //dd($last_date_on_the_job_val);
     $arr_parent_work_order=array_filter($fields, function($k) {
        return strpos($k, 'parent_work_order')>0;
     }, ARRAY_FILTER_USE_KEY); 

     $parent_work_order_id=array_pop($arr_parent_work_order);

      $parent_notice_id = \App\Models\Cyo_WorkOrders::select('notice_id')->where('id', $parent_work_order_id)->first();
          //dd($parent_notice_id);
     if(!empty($parent_notice_id) && !empty($last_date_on_the_job_val)){
          $notice_field_id = \App\Models\NoticeField::select('id')->where('notice_id', $parent_notice_id->notice_id)
          ->where('name','last_date_on_the_job')->first();
          
          if(!empty($notice_field_id)){
             if($last_date_on_the_job_val==NULL){
                $wrk_order_fields=\App\Models\Cyo_WorkOrder_fields::where('workorder_id',$parent_work_order)
                ->where('notice_id',$parent_notice_id->notice_id)
                ->where('notice_field_id',$notice_field_id->id)
                ->update(['value'=>'']);
            }else{
              $wrk_order_fields=\App\Models\Cyo_WorkOrder_fields::where('workorder_id',$parent_work_order)
                ->where('notice_id',$parent_notice_id->notice_id)
                ->where('notice_field_id',$notice_field_id->id)
                ->update(['value'=>$last_date_on_the_job_val]);
            }
        }
      }

        $data['work_order_status'] = $result1['status'];
        $notice_id = $result1->notice_id;
        $id = $result1->id;
        $notice = \App\Models\Notice::select('name', 'id', 'type', 'is_claim_of_lien','master_notice_id')->where('id', $notice_id)->get()->toArray();
        $data['notice'] = $notice[0];
        $data['cities'] = \App\Models\City::select()->get();
        $data['countries'] = \App\Models\Country::select()->get();
        $data['states'] = \App\Models\State::select()->orderBy('name', 'asc')->get();
        // $data['recipients'] = \App\Models\Cyo_WorkOrder_Recipient::select()->where('work_order_id', $id)->get();
        $data['recipients'] = \App\Models\Cyo_WorkOrder_Recipient::select('cyo__work_order__recipients.id', 'work_order_id', 'category_id', 'name', 'contact', 'mobile', 'cyo__work_order__recipients.address', 'city_id', 'state_id', 'zip', 'fax', 'cyo__work_order__recipients.created_at', 'cyo__work_order__recipients.updated_at', \DB::raw('cyo__usps_addresses.address as cyo_usps_address'), \DB::raw('cyo__usps_addresses.city as usps_city'), \DB::raw('cyo__usps_addresses.state as usps_state'), 'zipcode', 'zipcode_add_on', 'dpb', 'check_digit', 'comment')->leftJoin('cyo__usps_addresses', 'cyo__usps_addresses.recipient_id', '=', 'cyo__work_order__recipients.id')->where('work_order_id', $id)->get();

        $verified_address_count = \App\Models\Cyo_WorkOrder_Recipient::select(\DB::raw('COUNT(cyo__usps_addresses.recipient_id) as verified_address_count'))->leftJoin('cyo__usps_addresses', 'cyo__usps_addresses.recipient_id', '=', 'cyo__work_order__recipients.id')->where('work_order_id', $id)->get();
        $data['verified_address_count'] = ($verified_address_count) ? $verified_address_count[0]['verified_address_count'] : 0;
        $data['count'] = \App\Models\Cyo_WorkOrder_Recipient::select(DB::raw('COUNT(*) AS total'), 'categories.name')->groupBy('cyo__work_order__recipients.category_id')->join('categories', 'categories.id', '=', 'cyo__work_order__recipients.category_id')->where('work_order_id', $id)->get();
        $data['projectTypes'] = \App\Models\ProjectType::select()->get();
        $data['id'] = $id;
        $data['officers_directors'] = \App\Models\CustomerAgent::where('customer_id', '=', Auth::user()->customer->id)
                ->get();
        $data['notice_id'] = $notice_id;
        $data['customer_id'] = Auth::user()->customer->id;
        $data['categories'] = \App\Models\Category::select()->orderBy('name', 'ASC')->get();
        $data['project_roles'] = \App\Models\ProjectRoles::select()->orderBy('name', 'ASC')->get();
//        $result = \App\Models\Cyo_WorkOrders::select('state_id')->join('notice_fields','notice_fields.id','=','cyo__work_orders.notice_id')->where('cyo__work_orders.id',$id)->get()->toArray();
        $result = \App\Models\Cyo_WorkOrders::select('state_id')->join('notice_fields', 'notice_fields.notice_id', '=', 'cyo__work_orders.notice_id')->where('cyo__work_orders.id', $id)->limit(1)->get()->toArray();
        $data['names'] = \App\Models\Contact::select('contacts.id', 'contacts.company_name')
//                ->where('contact_details.type', '=', 0)
                ->where('contacts.customer_id', '=', Auth::user()->customer->id)
//                ->Join('contact_details', 'contact_details.contact_id', '=', 'contacts.id')
                ->orderBy('contacts.id', 'DESC')
                ->get();
        $admin_email = \App\User::leftjoin('role_users', 'role_users.user_id', '=', 'users.id')
                ->where('role_users.role_id', '=', 2)
                ->select('users.email')
                ->first();
        $account_manager_email = \App\Models\Customer::leftjoin('users', 'users.id', '=', 'customers.account_manager_id')
                ->select('users.email')
                ->find(Auth::user()->customer->id);

        /* if (isset($account_manager_email) && !empty($account_manager_email)) {
          $data['note_emails'] = [
          $admin_email->email,
          $account_manager_email->email
          ];
          } else {
          $data['note_emails'] = [
          $admin_email->email
          ];
          } */
        $data['note_emails'] = get_user_notice_email(Auth::user()->customer->id);
        $data['notes'] = \App\Models\Cyo_WorkOrder_Notes::leftjoin('users', 'users.id', '=', 'cyo__work_order__notes.user_id')->where('customer_id', '=', Auth::user()->customer->id)
                ->where('work_order_id', '=', $id)
                ->where('visibility', '=', 0)
                ->orderBy('cyo__work_order__notes.id', 'DESC')
                ->get();
        $data['corrections'] = \App\Models\Cyo_WorkOrder_Corrections::leftjoin('users', 'users.id', '=', 'cyo__work_order__corrections.user_id')->where('customer_id', '=', Auth::user()->customer->id)
                ->where('cyo__work_order__corrections.work_order_id', '=', $id)
                ->where('cyo__work_order__corrections.visibility', '=', 0)
                ->orderBy('cyo__work_order__corrections.id', 'DESC')
                ->get();
        $data['attachment'] = \App\Models\Cyo_WorkOrder_Attachment::where('work_order_id',$id)->get();
       // dd($request->all());
        if ($request->continue == 'continue') {

           /* if ($notice[0]['type'] == 2) {
                $data['notice_fields_section1'] = \App\Models\NoticeField::select()->join('cyo__work_order_fields', 'cyo__work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('cyo__work_order_fields.workorder_id', $id)->where('cyo__work_order_fields.notice_id', $notice_id)->where('notice_fields.section', 1)->orderBy('sort_order', 'asc')->get();

                $data['notice_fields_section2'] = \App\Models\NoticeField::select()->join('cyo__work_order_fields', 'cyo__work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('cyo__work_order_fields.notice_id', $notice_id)->where('cyo__work_order_fields.workorder_id', $id)->where('notice_fields.section', 2)->orderBy('sort_order', 'asc')->get();
                $data['tab'] = 'recipients';

                return view('customer.create_your_own.edit_soft_notices', $data);
            } else {
                $data['notice_fields'] = \App\Models\NoticeField::select()->join('cyo__work_order_fields', 'cyo__work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('cyo__work_order_fields.notice_id', $notice_id)->where('cyo__work_order_fields.workorder_id', $id)->orderBy('sort_order', 'asc')->get();
                $data['tab'] = 'recipients';*/

                //return view('customer.create_your_own.edit', $data);
                //dd('ji');

              return redirect('customer/create-your-own/create-new/'.$id.'/'.$notice_id);
            }else {
            return redirect('/customer/create-your-own/work-order-history');
          }
       /* } else {
            return redirect('/customer/create-your-own/work-order-history');
        }*/
    }


     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function cyoStoreNextStep(Request $request,$id,$notice_id) {

        // $data['is_rescind'] = 0;
        /* check  customer_subscriptions */
        $cutomer_subscription_info = \App\Models\Customer_subscription::where('customer_id', Auth::user()->customer->id)->where('end_date', '>=', date('Y-m-d'))->first();
        if (empty($cutomer_subscription_info)) {
            /* redirect to pricing */
            Session::flash('warning', 'Your subscription for Create your own has been expired, Please subscribe again.');
            return redirect('/pricing#showsubscribe');
        }
        $data['service_types'] = \App\Models\StampsServices::where(['status' => '1'])->get(['description', 'abbr', 'id'])->toArray();
        $data['package_types'] = \App\Models\StampsAddOns::join('stamps_service_package_addon', 'stamps_service_package_addon.add_on', '=', 'stamps_add_ons.id')
                        ->join('stamps_service_type', 'stamps_service_type.id', '=', 'stamps_service_package_addon.service_type')
                        ->join('stamps_packages', 'stamps_packages.id', '=', 'stamps_service_package_addon.package')
                        ->where(['stamps_packages.status' => '1'])
                        ->get([\DB::raw('DISTINCT(stamps_packages.name)'), 'stamps_packages.id'])->toArray();
        $data['add_ons'] = \App\Models\StampsAddOns::join('stamps_service_package_addon', 'stamps_service_package_addon.add_on', '=', 'stamps_add_ons.id')
                        ->join('stamps_service_type', 'stamps_service_type.id', '=', 'stamps_service_package_addon.service_type')
                        ->join('stamps_packages', 'stamps_packages.id', '=', 'stamps_service_package_addon.package')
                        ->where(['stamps_add_ons.status' => '1'])
                        ->where(['stamps_service_type.id' => 1])
                        ->where(['stamps_packages.id' => 2])
                        ->get([\DB::raw('DISTINCT(stamps_add_ons.id)'), 'stamps_add_ons.description'])->toArray();

        if (!empty($data['add_ons'])) {
            for ($counter = 0; $counter < count($data['add_ons']); $counter++) {
                $prohibited = \App\Models\StampsAddOnsProhibited::join('stamps_add_ons', 'stamps_add_ons_prohibited.prohibited', '=', 'stamps_add_ons.id')
                                ->where(['stamps_add_ons_prohibited.add_on' => $data['add_ons'][$counter]['id']])
                                ->pluck('prohibited')->toArray();

                $required = \App\Models\StampsAddOnsRequired::join('stamps_add_ons', 'stamps_add_ons_required.required', '=', 'stamps_add_ons.id')
                                ->where(['stamps_add_ons_required.add_on' => $data['add_ons'][$counter]['id']])
                                ->pluck('required')->toArray();
                $data['add_ons'][$counter]['prohibited'] = json_encode($prohibited);
                $data['add_ons'][$counter]['required'] = json_encode($required);
            }
        }
        $data['label_generated_count'] = 0;
        //$fields = array_merge($request->except(['_token', 'continue', 'type', 'notice_id', 'work_order_attachment', 'file_name', 'doc_input', 'document_type', 'remove_doc', 'recipient_county_id', 'visibility', 'recipient_notary_county_id', 'recipient_project_county_id', 'recipient_recorded_county_id']));
        $fields = array_merge($request->except(['_token', 'continue', 'type', 'notice_id', 'edit_soft_notices_attachment', 'work_order_attachment', 'file_name', 'doc_input', 'hidden_city_val', 'duplicate', 'remove_doc', 'parent_work_order_previous', 'recipient_county_id', 'contracted_by_exist', 'document_type', 'work_order_attachment', 'visibility', 'recipient_notary_county_id', 'recipient_project_county_id', 'recipient_recorded_county_id','work_order_attachment_id']));

        
        $customerId = Auth::user()->customer->id;
        $account_manager_id = Auth::user()->customer->account_manager_id;
        $userId = Auth::user()->id;

      

        $work_order_id = $id;
        $workorderiddetails = \App\Models\Cyo_WorkOrders::find($id);

        $data['work_order_status'] = $workorderiddetails->status;
        $data['parent_work_order'] = $workorderiddetails->parent_id;
       

       // $data['work_order_status'] = $result1['status'];
        //$notice_id = $workorderiddetails->notice_id;
        $id = $workorderiddetails->id;
        $notice = \App\Models\Notice::select('name', 'id', 'type', 'is_claim_of_lien','master_notice_id')->where('id', $notice_id)->get()->toArray();//dd($notice,$notice_id);
        $data['notice'] = $notice[0];
        $data['cities'] = \App\Models\City::select()->get();
        $data['countries'] = \App\Models\Country::select()->get();
        $data['states'] = \App\Models\State::select()->orderBy('name', 'asc')->get();
        // $data['recipients'] = \App\Models\Cyo_WorkOrder_Recipient::select()->where('work_order_id', $id)->get();
        $data['recipients'] = \App\Models\Cyo_WorkOrder_Recipient::select('cyo__work_order__recipients.id', 'work_order_id', 'category_id', 'name', 'contact', 'mobile', 'cyo__work_order__recipients.address', 'city_id', 'state_id', 'zip', 'fax', 'cyo__work_order__recipients.created_at', 'cyo__work_order__recipients.updated_at', \DB::raw('cyo__usps_addresses.address as cyo_usps_address'), \DB::raw('cyo__usps_addresses.city as usps_city'), \DB::raw('cyo__usps_addresses.state as usps_state'), 'zipcode', 'zipcode_add_on', 'dpb', 'check_digit', 'comment')->leftJoin('cyo__usps_addresses', 'cyo__usps_addresses.recipient_id', '=', 'cyo__work_order__recipients.id')->where('work_order_id', $id)->get();
        if ($data['recipients']) {
            for ($cnt = 0; $cnt < count($data['recipients']); $cnt++) {
                $labels = \App\Models\Cyo_StampsLabel::leftJoin('stamps_service_type', 'stamps_service_type.id', 'cyo__stamps_labels.service_type')
                        ->leftJoin('stamps_packages', 'stamps_packages.id', 'cyo__stamps_labels.package_type')
                        ->where(['recipient_id' => $data['recipients'][$cnt]['id']])
                        ->first(['cyo__stamps_labels.*']);
                if (!empty($labels) && $labels['generated_label'] == 'yes') {
                    $data['label_generated_count'] ++;
                    $data['recipients'][$cnt]['label_generated'] = 'yes';
                } else {
                    $data['recipients'][$cnt]['label_generated'] = 'no';
                }
                $data['recipients'][$cnt]['recipient_shipping_labels'] = $labels;
                $data['recipients'][$cnt]['add_ons'] = \App\Models\StampsAddOns::join('stamps_service_package_addon', 'stamps_service_package_addon.add_on', '=', 'stamps_add_ons.id')
                                ->join('stamps_service_type', 'stamps_service_type.id', '=', 'stamps_service_package_addon.service_type')
                                ->join('stamps_packages', 'stamps_packages.id', '=', 'stamps_service_package_addon.package')
                                ->where(['stamps_add_ons.status' => '1'])
                                ->where('stamps_service_type.id', '=', 1)
                                ->where('stamps_packages.id', '=', 2)
                                // ->where(function($query) use ($labels) {  
                                //     if(isset($labels) && !empty($labels))
                                //     {
                                //         $query->where('stamps_service_type.id','=',$labels['service_type']);
                                //         $query->where('stamps_packages.id','=',$labels['package_type']);
                                //     }  
                                // })
                                ->get([\DB::raw('DISTINCT(stamps_add_ons.id)'), 'stamps_add_ons.description'])->toArray();

                if (!empty($data['recipients'][$cnt]['add_ons'])) {
                    $cnt1 = 1;
                    $temp_add_ons = $data['recipients'][$cnt]['add_ons'];
                    for ($counter = 0; $counter < count($temp_add_ons); $counter++) {
                        $prohibited = \App\Models\StampsAddOnsProhibited::join('stamps_add_ons', 'stamps_add_ons_prohibited.prohibited', '=', 'stamps_add_ons.id')
                                        ->where(['stamps_add_ons_prohibited.add_on' => $temp_add_ons[$counter]['id']])
                                        ->pluck('prohibited')->toArray();

                        $required = \App\Models\StampsAddOnsRequired::join('stamps_add_ons', 'stamps_add_ons_required.required', '=', 'stamps_add_ons.id')
                                        ->where(['stamps_add_ons_required.add_on' => $data['recipients'][$cnt]['add_ons'][$counter]['id']])
                                        ->pluck('required')->toArray();
                        $temp_add_ons[$counter]['prohibited'] = json_encode($prohibited);
                        $temp_add_ons[$counter]['required'] = json_encode($required);
                    }
                    unset($data['recipients'][$cnt]['add_ons']);
                    $data['recipients'][$cnt]['add_ons'] = $temp_add_ons;
                }
            }
        }

        $verified_address_count = \App\Models\Cyo_WorkOrder_Recipient::select(\DB::raw('COUNT(cyo__usps_addresses.recipient_id) as verified_address_count'))->leftJoin('cyo__usps_addresses', 'cyo__usps_addresses.recipient_id', '=', 'cyo__work_order__recipients.id')->where('work_order_id', $id)->get();
        $data['verified_address_count'] = ($verified_address_count) ? $verified_address_count[0]['verified_address_count'] : 0;
        $data['count'] = \App\Models\Cyo_WorkOrder_Recipient::select(DB::raw('COUNT(*) AS total'), 'categories.name')->groupBy('cyo__work_order__recipients.category_id')->join('categories', 'categories.id', '=', 'cyo__work_order__recipients.category_id')->where('work_order_id', $id)->get();
        $data['projectTypes'] = \App\Models\ProjectType::select()->get();
        $data['id'] = $id;
        $data['officers_directors'] = \App\Models\CustomerAgent::where('customer_id', '=', Auth::user()->customer->id)
                ->get();
        $data['notice_id'] = $notice_id;
        $data['customer_id'] = Auth::user()->customer->id;
        $data['categories'] = \App\Models\Category::select()->orderBy('name', 'ASC')->get();
        $data['project_roles'] = \App\Models\ProjectRoles::select()->orderBy('name', 'ASC')->get();
//        $result = \App\Models\Cyo_WorkOrders::select('state_id')->join('notice_fields','notice_fields.id','=','cyo__work_orders.notice_id')->where('cyo__work_orders.id',$id)->get()->toArray();
        $result = \App\Models\Cyo_WorkOrders::select('state_id')->join('notice_fields', 'notice_fields.notice_id', '=', 'cyo__work_orders.notice_id')->where('cyo__work_orders.id', $id)->limit(1)->get()->toArray();
        $data['names'] = \App\Models\Contact::select('contacts.id', 'contacts.company_name')
//                ->where('contact_details.type', '=', 0)
                ->where('contacts.customer_id', '=', Auth::user()->customer->id)
//                ->Join('contact_details', 'contact_details.contact_id', '=', 'contacts.id')
                ->orderBy('contacts.id', 'DESC')
                ->get();
        $admin_email = \App\User::leftjoin('role_users', 'role_users.user_id', '=', 'users.id')
                ->where('role_users.role_id', '=', 2)
                ->select('users.email')
                ->first();
        $account_manager_email = \App\Models\Customer::leftjoin('users', 'users.id', '=', 'customers.account_manager_id')
                ->select('users.email')
                ->find(Auth::user()->customer->id);

        /* if (isset($account_manager_email) && !empty($account_manager_email)) {
          $data['note_emails'] = [
          $admin_email->email,
          $account_manager_email->email
          ];
          } else {
          $data['note_emails'] = [
          $admin_email->email
          ];
          } */
        $data['note_emails'] = get_user_notice_email(Auth::user()->customer->id);
        $data['notes'] = \App\Models\Cyo_WorkOrder_Notes::leftjoin('users', 'users.id', '=', 'cyo__work_order__notes.user_id')->where('customer_id', '=', Auth::user()->customer->id)
                ->where('work_order_id', '=', $id)
                ->where('visibility', '=', 0)
                ->orderBy('cyo__work_order__notes.id', 'DESC')
                ->get();
        $data['corrections'] = \App\Models\Cyo_WorkOrder_Corrections::leftjoin('users', 'users.id', '=', 'cyo__work_order__corrections.user_id')->where('customer_id', '=', Auth::user()->customer->id)
                ->where('cyo__work_order__corrections.work_order_id', '=', $id)
                ->where('cyo__work_order__corrections.visibility', '=', 0)
                ->orderBy('cyo__work_order__corrections.id', 'DESC')
                ->get();

        $data['attachment'] = \App\Models\Cyo_WorkOrder_Attachment::where('work_order_id',$id)->get();
        $data['work_order_cyo_doc_payment'] =   \App\Models\Cyo_Doc_Payment_History::where(['work_order_id'=>$id])->count();  
        $data['extraTitle'] = ' - Create Your Own';
    //   dd($notice);
      //  if ($request->continue == 'continue') {
            if ($notice[0]['type'] == 2) {
                $data['notice_fields_section1'] = \App\Models\NoticeField::select()->join('cyo__work_order_fields', 'cyo__work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('cyo__work_order_fields.workorder_id', $id)->where('cyo__work_order_fields.notice_id', $notice_id)->where('notice_fields.section', 1)->orderBy('sort_order', 'asc')->get();

                $data['notice_fields_section2'] = \App\Models\NoticeField::select()->join('cyo__work_order_fields', 'cyo__work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('cyo__work_order_fields.notice_id', $notice_id)->where('cyo__work_order_fields.workorder_id', $id)->where('notice_fields.section', 2)->orderBy('sort_order', 'asc')->get();
                $data['tab'] = 'recipients';

                return view('customer.create_your_own.edit_soft_notices', $data);
            } else {
                $data['notice_fields'] = \App\Models\NoticeField::select()->join('cyo__work_order_fields', 'cyo__work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('cyo__work_order_fields.notice_id', $notice_id)->where('cyo__work_order_fields.workorder_id', $id)->orderBy('sort_order', 'asc')->get();
                $data['tab'] = 'recipients';

                return view('customer.create_your_own.edit', $data);
            }
       /* } else {
            return redirect('/customer/create-your-own/work-order-history');
        }*/
    }

    /**
      * store recipents
     */
    public function store_recipients(Request $request, $id, $notice_id) {
      //dd($request->all());
        if(!is_numeric($request->city_id)){ 
            $city = \App\Models\City::firstOrNew(array('name' => $request->city_id));
            //$city = new \App\Models\City();
            $city->name = $request->city_id;
            $city->state_id = $request->state_id;
            $city->zip_code = $request->zip;
            $city->save();
            $request->request->set('city_id',$city->id);
        }

        $data['service_types'] = \App\Models\StampsServices::where(['status' => '1'])->get(['description', 'abbr', 'id'])->toArray();
        $data['package_types'] = \App\Models\StampsAddOns::join('stamps_service_package_addon', 'stamps_service_package_addon.add_on', '=', 'stamps_add_ons.id')
                        ->join('stamps_service_type', 'stamps_service_type.id', '=', 'stamps_service_package_addon.service_type')
                        ->join('stamps_packages', 'stamps_packages.id', '=', 'stamps_service_package_addon.package')
                        ->where(['stamps_packages.status' => '1'])
                        ->get([\DB::raw('DISTINCT(stamps_packages.name)'), 'stamps_packages.id'])->toArray();
        $data['add_ons'] = \App\Models\StampsAddOns::join('stamps_service_package_addon', 'stamps_service_package_addon.add_on', '=', 'stamps_add_ons.id')
                        ->join('stamps_service_type', 'stamps_service_type.id', '=', 'stamps_service_package_addon.service_type')
                        ->join('stamps_packages', 'stamps_packages.id', '=', 'stamps_service_package_addon.package')
                        ->where(['stamps_add_ons.status' => '1'])
                        ->where(['stamps_service_type.id' => 1])
                        ->where(['stamps_packages.id' => 2])
                        ->get([\DB::raw('DISTINCT(stamps_add_ons.id)'), 'stamps_add_ons.description'])->toArray();

        if (!empty($data['add_ons'])) {
            for ($counter = 0; $counter < count($data['add_ons']); $counter++) {
                $prohibited = \App\Models\StampsAddOnsProhibited::join('stamps_add_ons', 'stamps_add_ons_prohibited.prohibited', '=', 'stamps_add_ons.id')
                                ->where(['stamps_add_ons_prohibited.add_on' => $data['add_ons'][$counter]['id']])
                                ->pluck('prohibited')->toArray();

                $required = \App\Models\StampsAddOnsRequired::join('stamps_add_ons', 'stamps_add_ons_required.required', '=', 'stamps_add_ons.id')
                                ->where(['stamps_add_ons_required.add_on' => $data['add_ons'][$counter]['id']])
                                ->pluck('required')->toArray();
                $data['add_ons'][$counter]['prohibited'] = json_encode($prohibited);
                $data['add_ons'][$counter]['required'] = json_encode($required);
            }
        }
        $data['label_generated_count'] = 0;

        $request->request->set('work_order_id', $id);
        $request->request->set('city_id', $request->city_id);
        $contracted_by = $request->name;
        $contact_id = $request->recipt_id;
        if ($contracted_by != "") {
            /* check contracted by is exist or not */
            $contracted_by_check = \App\Models\Contact::where('company_name', $contracted_by)->first();
            if (empty($contracted_by_check)) {
                $contact = \App\Models\Contact::create([
                            'customer_id' => Auth::user()->customer->id,
                            'company_name' => $contracted_by,
                            'company_address'=>$request->address,
                            'mailing_address'=>$request->address,
                            'city_id'=>$request->city_id,
                            'state_id'=>$request->state_id,
                            'zip'=>$request->zip,
                            'phone'=>$request->contact,
                            'email'=>$request->email,
                            'attn'=>$request->attn,
                ]);
                $contact_id = $contact->id;
            }
        }
        $request->request->set('contact_id', $contact_id);
        $first_category = \App\Models\Category::where('name', 'Contracted By')->first();
        if ($first_category->id == $request->category_id) {
            /* check contract details exit or not */
            $check_contract_recipient_exist = \App\Models\Cyo_WorkOrder_Recipient::where(['category_id' => $first_category->id, 'work_order_id' => $request->work_order_id])->where('id', '!=', $request->recipient_id)->first();

            /* if not empty delete previouse record */
            if (!empty($check_contract_recipient_exist)) {
                $contract_recipient_delete = \App\Models\Cyo_WorkOrder_Recipient::where(['category_id' => $request->category_id, 'work_order_id' => $request->work_order_id])->delete();
            }
        }

        if (isset($request->verified_cyo_usps_address)) {
            $clean_address = json_decode($request->verified_cyo_usps_address, true);
            $cyo_usps_address = [
                'fullname' => $clean_address['Address']['FullName'] ?? '',
                'company' => $clean_address['Address']['Company'] ?? '',
                'department' => $clean_address['Address']['Department'] ?? '',
                'address' => $clean_address['Address']['Address1'],
                'address2' => $clean_address['Address']['Address2'] ?? '',
                'address3' => $clean_address['Address']['Address3'] ?? '',
                'city' => $clean_address['Address']['City'], 
                'state' => $clean_address['Address']['State'],
                'zipcode' => $clean_address['Address']['ZIPCode'],
                'zipcode_add_on' => $clean_address['Address']['ZIPCodeAddOn'],
                'phone_number' => $clean_address['Address']['PhoneNumber'] ?? '',
                'email' => $clean_address['Address']['EmailAddress'] ?? '',
                'country' => 'US',
                'dpb' => $clean_address['Address']['DPB'],
                'check_digit' => $clean_address['Address']['CheckDigit'],
                'comment' => $clean_address['AddressCleansingResult'],
                'override_hash' => $clean_address['Address']['OverrideHash'],
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ];
        } else {
            $usps_address = [];
        }

        if ($request->continue_val == 'add') {
            $Recipient = \App\Models\Cyo_WorkOrder_Recipient::create(
                            $request->except(['_token', 'recipient_county']));
            if ($Recipient) {
                if (!empty($cyo_usps_address)) {
                    $cyo_usps_address['recipient_id'] = $Recipient->id;
                    \App\Models\Cyo_UspsAddress::insert($cyo_usps_address);
                }
                Session::flash('success', 'Your work # ' . $id . 'Recipient successfully added.');
            } else {
                Session::flash('success', 'Your work # ' . $id . 'Problem in Record saving');
            }
        } else {
            $cyoworkorderRecipient = \App\Models\Cyo_WorkOrder_Recipient::find($request->recipient_id);
            $cyoworkorderRecipient->category_id = $request->category_id;
            $cyoworkorderRecipient->name = $request->name;
            $cyoworkorderRecipient->contact = $request->contact;
            $cyoworkorderRecipient->address = $request->address;
            $cyoworkorderRecipient->city_id = $request->city_id;
            $cyoworkorderRecipient->state_id = $request->state_id;
            $cyoworkorderRecipient->zip = $request->zip;
            $cyoworkorderRecipient->email = $request->email;
            $cyoworkorderRecipient->attn = $request->attn;
            $cyoworkorderRecipient->contact_id = $request->contact_id;
            $cyoworkorderRecipient->save();
            if ($cyoworkorderRecipient) {
                //Remove stamps label entry if generated for this recipient since the address has been changed
                $chk_label_record = \App\Models\Cyo_StampsLabel::where(['recipient_id' => $request->recipient_id])->orderBy('id', 'desc')->limit(1)->get()->toArray();
                if (count($chk_label_record) > 0) {
                    \App\Models\Cyo_StampsLabel::where(['recipient_id' => $request->recipient_id])->delete();
                }
                if (!empty($cyo_usps_address)) {
                    //Check if there is cyo_usps_address entry for this recipient then update else insert
                    $check_usps_record = \App\Models\Cyo_UspsAddress::where(['recipient_id' => $request->recipient_id])->count();
                    if (isset($check_usps_record) && $check_usps_record > 0) {
                        $cyo_usps_address['recipient_id'] = $request->recipient_id;
                        \App\Models\Cyo_UspsAddress::where(['recipient_id' => $request->recipient_id])->update($cyo_usps_address);
                    } else {
                        $cyo_usps_address['recipient_id'] = $request->recipient_id;
                        \App\Models\Cyo_UspsAddress::insert($cyo_usps_address);
                    }
                }
                Session::flash('success', 'Your work # ' . $id . ' recipient successfully updated.');
              $work_orders = \App\Models\Cyo_WorkOrder_fields::select('cyo__work_order_fields.workorder_id')->join('notice_fields','cyo__work_order_fields.notice_field_id','notice_fields.id')->join('cyo__work_orders','cyo__work_orders.id','cyo__work_order_fields.workorder_id')->where('notice_fields.name','parent_work_order')->whereNotIn('cyo__work_orders.status',[5,6])->where('cyo__work_order_fields.value',$id)->get()->toArray();
             
              $parent_work_order_recipients = \App\Models\Cyo_WorkOrder_Recipient::select()->where('work_order_id',$id)->where('category_id','!=',21)->get()->toArray();

              if(isset($work_orders) && count($work_orders) > 0) {

              foreach ($work_orders As $k => $work_order) {

                $delete_previous_recipients = \App\Models\Cyo_WorkOrder_Recipient::where('work_order_id',$work_order['workorder_id'])->where('parent_work_order',$id)->delete();
                if (isset($parent_work_order_recipients) && count($parent_work_order_recipients) > 0) {
                          foreach ($parent_work_order_recipients As $k => $each_parent_recipient) {
                            $usps_recipient = \App\Models\Cyo_UspsAddress::select()->where('recipient_id',$each_parent_recipient['id'])->get()->toArray();
                            
                            $save_recipients = \App\Models\Cyo_WorkOrder_Recipient::create([
                                                        'work_order_id' => $work_order['workorder_id'],
                                                        'category_id' => $each_parent_recipient['category_id'],
                                                        'name' => $each_parent_recipient['name'],
                                                        'contact' => $each_parent_recipient['contact'],
                                                        'address' => $each_parent_recipient['address'],
                                                        'city_id' => $each_parent_recipient['city_id'],
                                                        'attn' => $each_parent_recipient['attn'],
                                                        'state_id' => $each_parent_recipient['state_id'],
                                                        'zip' => $each_parent_recipient['zip'],
                                                        'email' => $each_parent_recipient['email'],
                                                        'attn' => $each_parent_recipient['attn'],
                                                         'parent_work_order'=> $id,

                                            ]);
                         
                          if(!empty($save_recipients) && !empty($usps_recipient)){
                              $usps_address = [
                                'fullname' => $usps_recipient[0]['fullname'], 
                                'company' => $usps_recipient[0]['company'], 
                                'department' => $usps_recipient[0]['department'], 
                                'address' => $usps_recipient[0]['address'], 
                                'address2' => $usps_recipient[0]['address2'], 
                                'address3' => $usps_recipient[0]['address3'], 
                                'city' => $usps_recipient[0]['city'], 
                                'state' => $usps_recipient[0]['state'], 
                                'zipcode' => $usps_recipient[0]['zipcode'], 
                                'zipcode_add_on' => $usps_recipient[0]['zipcode_add_on'], 
                                'country' => $usps_recipient[0]['country'], 
                                'dpb' => $usps_recipient[0]['dpb'], 
                                'check_digit' => $usps_recipient[0]['check_digit'], 
                                'comment' => $usps_recipient[0]['comment'], 
                                'override_hash' => $usps_recipient[0]['override_hash'],
                                'created_at' => date("Y-m-d H:i:s"),
                                'updated_at' => date("Y-m-d H:i:s"),
                            ];

                            $usps_address['recipient_id'] = $save_recipients->id;
                            \App\Models\Cyo_UspsAddress::insert($usps_address);
                          }

                    }
                  }
               }
             }
            } else {
                Session::flash('success', 'Your work # ' . $id . ' problem in record submitting.');
            }
        }


        $notice = \App\Models\Notice::select('name', 'id', 'type', 'is_claim_of_lien','master_notice_id')->where('id', $notice_id)->get()->toArray();
        $data['notice'] = $notice[0];
        $data['cities'] = \App\Models\City::select()->get();
        $data['countries'] = \App\Models\Country::select()->get();
        $data['categories'] = \App\Models\Category::select()->orderBy('name', 'ASC')->get();
        $data['project_roles'] = \App\Models\ProjectRoles::select()->orderBy('name', 'ASC')->get();
        $data['states'] = \App\Models\State::select()->orderBy('name', 'asc')->get();
        $data['recipients'] = \App\Models\Cyo_WorkOrder_Recipient::select('cyo__work_order__recipients.id', 'work_order_id', 'category_id', 'name', 'contact', 'mobile', 'cyo__work_order__recipients.address', 'city_id', 'state_id', 'zip', 'fax', 'cyo__work_order__recipients.created_at', 'cyo__work_order__recipients.updated_at', \DB::raw('cyo__usps_addresses.address as cyo_usps_address'), \DB::raw('cyo__usps_addresses.city as usps_city'), \DB::raw('cyo__usps_addresses.state as usps_state'), 'zipcode', 'zipcode_add_on', 'dpb', 'check_digit', 'comment')->leftJoin('cyo__usps_addresses', 'cyo__usps_addresses.recipient_id', '=', 'cyo__work_order__recipients.id')->where('work_order_id', $id)->get();
        $verified_address_count = \App\Models\Cyo_WorkOrder_Recipient::select(\DB::raw('COUNT(cyo__usps_addresses.recipient_id) as verified_address_count'))->leftJoin('cyo__usps_addresses', 'cyo__usps_addresses.recipient_id', '=', 'cyo__work_order__recipients.id')->where('work_order_id', $id)->get();
        $data['verified_address_count'] = ($verified_address_count) ? $verified_address_count[0]['verified_address_count'] : 0;

        $data['projectTypes'] = \App\Models\ProjectType::select()->get();
        $data['count'] = \App\Models\Cyo_WorkOrder_Recipient::select(DB::raw('COUNT(*) AS total'), 'categories.name')->groupBy('cyo__work_order__recipients.category_id')->join('categories', 'categories.id', '=', 'cyo__work_order__recipients.category_id')->where('work_order_id', $id)->get();
        $data['names'] = \App\Models\Contact::select('contacts.id', 'contacts.company_name')
//                ->where('contact_details.type', '=', 0)
                ->where('contacts.customer_id', '=', Auth::user()->customer->id)
//                ->Join('contact_details', 'contact_details.contact_id', '=', 'contacts.id')
                ->orderBy('contacts.id', 'DESC')
                ->get();

        $data['id'] = $id;
        $data['notice_id'] = $notice_id;

        $result = \App\Models\Cyo_WorkOrders::select('state_id')->join('notice_fields', 'notice_fields.notice_id', '=', 'cyo__work_orders.notice_id')->where('cyo__work_orders.id', $id)->limit(1)->get()->toArray();
        $work_order_status = \App\Models\Cyo_WorkOrders::find($id);
        if ($work_order_status->status == '0') {
            $data['status'] = 'draft';
        } else {
            $data['status'] = 'request';
        }
        $data['work_order_status'] = $work_order_status->status;
        if ($data['recipients']) {
            for ($cnt = 0; $cnt < count($data['recipients']); $cnt++) {
                $labels = \App\Models\Cyo_StampsLabel::leftJoin('stamps_service_type', 'stamps_service_type.id', 'cyo__stamps_labels.service_type')
                        ->leftJoin('stamps_packages', 'stamps_packages.id', 'cyo__stamps_labels.package_type')
                        ->where(['recipient_id' => $data['recipients'][$cnt]['id']])
                        ->first(['cyo__stamps_labels.*']);
                if (!empty($labels) && $labels['generated_label'] == 'yes') {
                    $data['label_generated_count'] ++;
                    $data['recipients'][$cnt]['label_generated'] = 'yes';
                } else {
                    $data['recipients'][$cnt]['label_generated'] = 'no';
                }
                $data['recipients'][$cnt]['recipient_shipping_labels'] = $labels;
                $data['recipients'][$cnt]['add_ons'] = \App\Models\StampsAddOns::join('stamps_service_package_addon', 'stamps_service_package_addon.add_on', '=', 'stamps_add_ons.id')
                                ->join('stamps_service_type', 'stamps_service_type.id', '=', 'stamps_service_package_addon.service_type')
                                ->join('stamps_packages', 'stamps_packages.id', '=', 'stamps_service_package_addon.package')
                                ->where(['stamps_add_ons.status' => '1'])
                                ->where('stamps_service_type.id', '=', 1)
                                ->where('stamps_packages.id', '=', 2)
                                // ->where(function($query) use ($labels) {  
                                //     if(isset($labels) && !empty($labels))
                                //     {
                                //         $query->where('stamps_service_type.id','=',$labels['service_type']);
                                //         $query->where('stamps_packages.id','=',$labels['package_type']);
                                //     }	
                                // })
                                ->get([\DB::raw('DISTINCT(stamps_add_ons.id)'), 'stamps_add_ons.description'])->toArray();

                if (!empty($data['recipients'][$cnt]['add_ons'])) {
                    $cnt1 = 1;
                    $temp_add_ons = $data['recipients'][$cnt]['add_ons'];
                    for ($counter = 0; $counter < count($temp_add_ons); $counter++) {
                        $prohibited = \App\Models\StampsAddOnsProhibited::join('stamps_add_ons', 'stamps_add_ons_prohibited.prohibited', '=', 'stamps_add_ons.id')
                                        ->where(['stamps_add_ons_prohibited.add_on' => $temp_add_ons[$counter]['id']])
                                        ->pluck('prohibited')->toArray();

                        $required = \App\Models\StampsAddOnsRequired::join('stamps_add_ons', 'stamps_add_ons_required.required', '=', 'stamps_add_ons.id')
                                        ->where(['stamps_add_ons_required.add_on' => $data['recipients'][$cnt]['add_ons'][$counter]['id']])
                                        ->pluck('required')->toArray();
                        $temp_add_ons[$counter]['prohibited'] = json_encode($prohibited);
                        $temp_add_ons[$counter]['required'] = json_encode($required);
                    }
                    unset($data['recipients'][$cnt]['add_ons']);
                    $data['recipients'][$cnt]['add_ons'] = $temp_add_ons;
                }
            }
        }
        // dd($data['recipients']);
        $data['attachment'] = \App\Models\Cyo_WorkOrder_Attachment::where('work_order_id', '=', $id)
                ->get();
        $admin_email = \App\User::leftjoin('role_users', 'role_users.user_id', '=', 'users.id')
                ->where('role_users.role_id', '=', 2)
                ->select('users.email')
                ->first();
        $account_manager_email = \App\Models\Customer::leftjoin('users', 'users.id', '=', 'customers.account_manager_id')
                ->select('users.email')
                ->find(Auth::user()->customer->id);

        /*   if (isset($account_manager_email) && !empty($account_manager_email)) {
          $data['note_emails'] = [
          $admin_email->email,
          $account_manager_email->email
          ];
          } else {
          $data['note_emails'] = [
          $admin_email->email
          ];
          } */
        $data['note_emails'] = get_user_notice_email($work_order_status->customer_id);
        $data['notes'] = \App\Models\Cyo_WorkOrder_Notes::leftjoin('users', 'users.id', '=', 'cyo__work_order__notes.user_id')->where('customer_id', '=', Auth::user()->customer->id)
                ->where('work_order_id', '=', $id)
                ->where('visibility', '=', 0)
                ->orderBy('cyo__work_order__notes.id', 'DESC')
                ->get();
        $data['corrections'] = \App\Models\Cyo_WorkOrder_Corrections::leftjoin('users', 'users.id', '=', 'cyo__work_order__corrections.user_id')->where('customer_id', '=', Auth::user()->customer->id)
                ->where('cyo__work_order__corrections.work_order_id', '=', $id)
                ->where('cyo__work_order__corrections.visibility', '=', 0)
                ->orderBy('cyo__work_order__corrections.id', 'DESC')
                ->get();

        if ($notice[0]['type'] == 2) {
            $data['notice_fields_section1'] = \App\Models\NoticeField::select()->join('cyo__work_order_fields', 'cyo__work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('cyo__work_order_fields.workorder_id', $id)->where('cyo__work_order_fields.notice_id', $notice_id)->where('notice_fields.section', 1)->orderBy('sort_order', 'asc')->get();
            $data['notice_fields_section2'] = \App\Models\NoticeField::select()->join('cyo__work_order_fields', 'cyo__work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('cyo__work_order_fields.notice_id', $notice_id)->where('cyo__work_order_fields.workorder_id', $id)->where('notice_fields.section', 2)->orderBy('sort_order', 'asc')->get();
            $data['tab'] = 'recipients';
            //To display parent work order
            foreach ($data['notice_fields_section1'] as $key => $value) {
                $key = explode("_", $key);

                if ($value) {

                    if ($value->name == 'parent_work_order') {
                        $parent_work_order = $value->value;
                        $data['parent_work_order'] = $parent_work_order;
                    }
                }
            }
            //return view('customer.create_your_own.edit_soft_notices', $data);
        } else {
            $data['notice_fields'] = \App\Models\NoticeField::select()->join('cyo__work_order_fields', 'cyo__work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('cyo__work_order_fields.notice_id', $notice_id)->where('cyo__work_order_fields.workorder_id', $id)->orderBy('sort_order', 'asc')->get();
            $data['tab'] = 'recipients';
            //To display parent work order
            foreach ($data['notice_fields'] as $key => $value) {
                $key = explode("_", $key);

                if ($value) {

                    if ($value->name == 'parent_work_order') {
                        $parent_work_order = $value->value;
                        $data['parent_work_order'] = $parent_work_order;
                    }
                }
            }
           // return view('customer.create_your_own.edit', $data);
        }
        return redirect()->back();
    }

    /**
      store recipents
     */
    public function edit_recipients(Request $request, $id, $notice_id) {

        /* $category = \App\Models\Category::create([
          'name' => $request->category_id]); */
        //$workorder_id = \App\Models\WorkOrder::orderBy('created_at', 'desc')->first();
        // $request->request->set('category_id',$category->id);
        $request->request->set('work_order_id', $id);
        $request->request->set('city_id', $request->recipient_city_id);
        $request->request->set('contact_id', $request->recipt_id);
        $Recipient = \App\Models\Cyo_WorkOrder_Recipient::create(
                        $request->except(['_token']));
        if ($Recipient) {
            Session::flash('success', 'Your work # ' . $id . 'Recipient successfully added.');
        } else {
            Session::flash('success', 'Your work # ' . $id . 'Problem in Record saving');
        }

        //Verify recipient address using stamps.com API
        $wsdl           = config('constants.STAMPS.WSDL');
        $integrationID  = config('constants.STAMPS.INTEGRATION_ID');
        $username       = config('constants.STAMPS.USERNAME');
        $password       = config('constants.STAMPS.PASSWORD');
        
        $stamps = new \App\Helpers\StampsHelper($wsdl, $integrationID, $username, $password);
        $city = \App\Models\City::where(['id' => $request->city_id])->first(['name'])->toArray();
        $state = \App\Models\State::where(['id' => $request->state_id])->first(['name'])->toArray();
        $country = \App\Models\Country::where(['id' => 1])->first(['name'])->toArray();
        $address = ['FirstName' => 'Shamal', 'LastName' => 'Test', 'FullName' => $request->name, 'Address1' => $request->address, 'City' => $city['name'], 'State' => $state['name'], 'ZIPCode' => $request->zip];
        $clean_address = $stamps->CleanseAddress($address);

        //Store cleansed address in database alongwith the error message if present
        $cyo_usps_address = ['address' => $clean_address->Address->Address1, 'city' => $clean_address->Address->City, 'state' => $clean_address->Address->State, 'zipcode' => $clean_address->Address->ZIPCode, 'zipcode_add_on' => $clean_address->Address->ZIPCodeAddOn, 'dpb' => $clean_address->Address->DPB, 'check_digit' => $clean_address->Address->CheckDigit, 'recipient_id' => $Recipient->id, 'comment' => $clean_address->AddressCleansingResult, 'override_hash' => $clean_address->Address->OverrideHash];
        $usps = \App\Models\Cyo_Cyo_UspsAddress::insert($cyo_usps_address);

        $notice = \App\Models\Notice::select('name', 'id', 'type', 'is_claim_of_lien','master_notice_id')->where('id', $notice_id)->get()->toArray();
        $data['notice'] = $notice[0];
        $data['cities'] = \App\Models\City::select()->get();
        $data['countries'] = \App\Models\Country::select()->get();
        $data['categories'] = \App\Models\Category::select()->orderBy('name', 'ASC')->get();
        $data['project_roles'] = \App\Models\ProjectRoles::select()->orderBy('name', 'ASC')->get();
        $data['states'] = \App\Models\State::select()->orderBy('name', 'asc')->get();
        $data['recipients'] = \App\Models\Cyo_WorkOrder_Recipient::select()->where('work_order_id', $id)->get();

        $data['count'] = \App\Models\Cyo_WorkOrder_Recipient::select(DB::raw('COUNT(*) AS total'), 'categories.name')->groupBy('cyo__work_order__recipients.category_id')->join('categories', 'categories.id', '=', 'cyo__work_order__recipients.category_id')->where('work_order_id', $id)->get();
        $data['names'] = \App\Models\Contact::select('contacts.id', 'contacts.company_name')
//                ->where('contact_details.type', '=', 0)
                ->where('contacts.customer_id', '=', Auth::user()->customer->id)
//                ->Join('contact_details', 'contact_details.contact_id', '=', 'contacts.id')
                ->orderBy('contacts.id', 'DESC')
                ->get();
        $data['projectTypes'] = \App\Models\ProjectType::select()->get();
        /*  $data['count']      = \App\Models\Category::select(DB::raw('COUNT(*) AS total'),'name')->groupBy('name')->get(); */
        //dd($data['count']);
        $data['id'] = $id;
        $data['notice_id'] = $notice_id;
//                $result = \App\Models\Cyo_WorkOrders::select('state_id')->join('notice_fields','notice_fields.id','=','cyo__work_orders.notice_id')->where('cyo__work_orders.id',$id)->get()->toArray();
        $result = \App\Models\Cyo_WorkOrders::select('state_id')->join('notice_fields', 'notice_fields.notice_id', '=', 'cyo__work_orders.notice_id')->where('cyo__work_orders.id', $id)->limit(1)->get()->toArray();
        if ($notice[0]['type'] == 2) {
            $data['notice_fields_section1'] = \App\Models\NoticeField::select()->join('cyo__work_order_fields', 'cyo__work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('cyo__work_order_fields.workorder_id', $id)->where('cyo__work_order_fields.notice_id', $notice_id)->where('notice_fields.section', 1)->orderBy('sort_order', 'asc')->get();
            $data['notice_fields_section2'] = \App\Models\NoticeField::select()->join('cyo__work_order_fields', 'cyo__work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('cyo__work_order_fields.notice_id', $notice_id)->where('cyo__work_order_fields.workorder_id', $id)->where('notice_fields.section', 2)->orderBy('sort_order', 'asc')->get();
            $data['tab'] = 'recipients';
            return view('customer.create_your_own.edit_soft_notices', $data);
        } else {
            $data['notice_fields'] = \App\Models\NoticeField::select()->join('cyo__work_order_fields', 'cyo__work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('cyo__work_order_fields.notice_id', $notice_id)->where('cyo__work_order_fields.workorder_id', $id)->orderBy('sort_order', 'asc')->get();
            $data['tab'] = 'recipients';
            return view('customer.create_your_own.edit', $data);
        }
    }

    public function duplicate($id, $notice_id, $is_rescind) {
        /* check  customer_subscriptions */
        $cutomer_subscription_info = \App\Models\Customer_subscription::where('customer_id', Auth::user()->customer->id)->where('end_date', '>=', date('Y-m-d'))->first();
        if (empty($cutomer_subscription_info)) {
            /* redirect to pricing */
            Session::flash('warning', 'Your subscription for Create your own has been expired, Please subscribe again.');
            return redirect('/pricing#showsubscribe');
        }
        $workorder_data = \App\Models\Cyo_WorkOrders::find($id);
        if ($workorder_data->is_rescind == '1') {
            $is_rescind = 1;
        }
        if (Auth::user()->hasRole('account-manager') || Auth::user()->hasRole('admin')) {
            $customerId = $workorder_data->customer_id;
        } else {
            $customerId = Auth::user()->customer->id;
        }

        $userId = Auth::user()->id;
        $account_manager_id = Auth::user()->customer->account_manager_id;
        $result1 = \App\Models\Cyo_WorkOrders::create([
                    'notice_id' => $notice_id,
                    'customer_id' => $customerId,
                    'order_no' => str_random(5),
                    'user_id' => $userId,
                    'status' => '2',
                    'account_manager_id' => $account_manager_id,
                    'is_rescind' => $is_rescind,
        ]);
        $data['status'] = 'processing';
        $flashMsg = 'Your work order #' . $id . ' has been '.(($is_rescind == 1) ? 'rescinded' : 'duplicated');
        Session::flash('success', $flashMsg);


        $recipients = \App\Models\Cyo_WorkOrder_Recipient::select('id')->where('work_order_id', $id)->get()->toArray();
        foreach ($recipients as $key => $value) {
# code...
//dd('ji');
            $recipient = \App\Models\Cyo_WorkOrder_Recipient::find($value['id']);
            $new_recipient_data = \App\Models\Cyo_WorkOrder_Recipient::create([
                'work_order_id' => $result1->id,
                'category_id' => $recipient->category_id,
                'name' => $recipient->name,
                'contact' => $recipient->contact,
                'mobile' => $recipient->mobile,
                'address' => $recipient->address,
                'city_id' => $recipient->city_id,
                'state_id' => $recipient->state_id,
                'zip' => $recipient->zip,
                'fax' => $recipient->fax,
                'email' => $recipient->email,
                'attn' => $recipient->attn,
                'contact_id' => $recipient->contact_id,
            ]);

             /* save UPS address verified */
            $recipients_uspsAddress = \App\Models\Cyo_UspsAddress::where('recipient_id', $value['id'])->get();

            foreach ($recipients_uspsAddress as $recipients_uspsAddress_val) {
                $usps_address = [
                    'fullname' => $recipients_uspsAddress_val->fullname,
                    'company' => $recipients_uspsAddress_val->company,
                    'department' => $recipients_uspsAddress_val->department,
                    'address' => $recipients_uspsAddress_val->address,
                    'address2' => $recipients_uspsAddress_val->address2,
                    'address3' => $recipients_uspsAddress_val->address3,
                    'city' => $recipients_uspsAddress_val->city,
                    'state' => $recipients_uspsAddress_val->state,
                    'zipcode' => $recipients_uspsAddress_val->zipcode,
                    'zipcode_add_on' => $recipients_uspsAddress_val->zipcode_add_on,
                    'phone_number' => $recipients_uspsAddress_val->phone_number,
                    'email' => $recipients_uspsAddress_val->email,
                    'dpb' => $recipients_uspsAddress_val->dpb,
                    'check_digit' => $recipients_uspsAddress_val->check_digit,
                    'comment' => $recipients_uspsAddress_val->comment,
                    'override_hash' => $recipients_uspsAddress_val->override_hash,
                    'recipient_id' => $new_recipient_data->id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),];

                \App\Models\Cyo_UspsAddress::insert($usps_address);
            }
        }

        $previous_attachments = \App\Models\Cyo_WorkOrder_Attachment::where('work_order_id', '=', $id)->get();



        if ($previous_attachments && count($previous_attachments) > 0) {
            foreach ($previous_attachments AS $k => $val) {

                $original_file_name = explode('.', $val->original_file_name);
                if ($val->original_file_name != "") {
                    $val->original_file_name = !empty($original_file_name[1]) ? ( '2_' . $original_file_name[0] . '.' . $original_file_name[1]) : "";
                }
                if ($val->file_name != ""  && file_exists(public_path() . '/attachment/cyo_work_order_document/' . $val->file_name)) {
                    $file_name = explode('.', $val->file_name);
                    if (!empty($file_name[1])) {
                        $new_file_name = date('H') . '_' . $file_name[0] . '.' . $file_name[1];

                        if (!file_exists(public_path() . '/attachment/cyo_work_order_document/temp_duplicate')) {
                            $dir = mkdir(public_path() . '/attachment/cyo_work_order_document/temp_duplicate', 0777, true);
                            if (file_exists(public_path() . '/attachment/cyo_work_order_document/' . $val->file_name)) {
                                \File::copy(public_path() . '/attachment/cyo_work_order_document/' . $val->file_name, public_path() . '/attachment/cyo_work_order_document/temp_duplicate/' . $val->file_name);
                            }
                        } else {
                            if (file_exists(public_path() . '/attachment/cyo_work_order_document/' . $val->file_name)) {
                                \File::copy(public_path() . '/attachment/cyo_work_order_document/' . $val->file_name, public_path() . '/attachment/cyo_work_order_document/temp_duplicate/' . $val->file_name);
                            }
                        }
                        if (file_exists(public_path() . '/attachment/cyo_work_order_document/temp_duplicate/' . $val->file_name)) {
                            rename(public_path() . '/attachment/cyo_work_order_document/temp_duplicate/' . $val->file_name, public_path() . '/attachment/cyo_work_order_document/temp_duplicate/' . $new_file_name);
                            //\Storage::move(public_path().'/attachment/work_order_document/temp_duplicate/'.$new_file_name, public_path().'/attachment/work_order_document/'.$new_file_name);
                            \File::copy(public_path() . '/attachment/cyo_work_order_document/temp_duplicate/' . $new_file_name, public_path() . '/attachment/cyo_work_order_document/' . $new_file_name);
                            unlink(public_path() . '/attachment/cyo_work_order_document/temp_duplicate/' . $new_file_name);
                        }
                        $val->file_name = $new_file_name;
                        $val->file_name = date('H') . '_' . $val->file_name;
                    } else {
                        $val->file_name = "";
                    }
                } else {
                    $val->file_name = "";
                }
                $work_order_attachment_obj = new \App\Models\Cyo_WorkOrder_Attachment();
                $work_order_attachment_obj->work_order_id = $result1->id;
                $work_order_attachment_obj->type = $val->type;
                $work_order_attachment_obj->title = $val->title;
                $work_order_attachment_obj->file_name = $val->file_name;
                $work_order_attachment_obj->original_file_name = $val->original_file_name;
                if (Auth::user()->hasRole('account-manager')) {
                    $work_order_attachment_obj->visibility = $val->visibility;
                } else {
                    $work_order_attachment_obj->visibility = 0;
                }
                $work_order_attachment_obj->save();
            }
        }

        $notice = \App\Models\Notice::select('name', 'id', 'type', 'is_claim_of_lien', 'state_id')->where('id', $notice_id)->get()->toArray();
        /* parent work order notice field id */
        $parent_workorder_notice_field = \App\Models\NoticeField::where(['notice_id' => $notice_id, 'name' => 'parent_work_order'])->first();

        /* $fieldsData = \App\Models\Cyo_WorkOrder_fields::select()->where('workorder_id', $id)->get(); */
        $fieldsData = \App\Models\Cyo_WorkOrder_fields::leftJoin('cyo__work_orders', 'cyo__work_orders.id', '=', 'cyo__work_order_fields.workorder_id')->select()->where('workorder_id', $id)->get();
        $rescind_flag = $result1->is_rescind;
        foreach ($fieldsData as $value) {
            $work_val = nl2br($value->value);
            if ($parent_workorder_notice_field->id == $value->notice_field_id && $is_rescind != 1) {
                $work_val = "";
            }

            //if wo is resind, then parent w/o should be this w/o
            if ($parent_workorder_notice_field->id == $value->notice_field_id && $is_rescind == 1) {
                $work_val = $id;
            }

            $result2 = \App\Models\Cyo_WorkOrder_fields::create([
                        'workorder_id' => $result1->id,
                        'notice_id' => $value->notice_id,
                        'notice_field_id' => $value->notice_field_id,
                        'value' => $work_val,
            ]);
        }

        if ($is_rescind == 1) {

            $amendment_fields = \App\Models\NoticeField::select()->where('state_id', $notice[0]['state_id'])->where('notice_id', $notice_id)->where('name', 'amendment')->orderBy('sort_order', 'asc')->get()->toArray();
            $amendmentValue = \App\Models\Cyo_WorkOrder_fields::select('value')->where('notice_field_id', $amendment_fields[0]['id'])->where('workorder_id', $result1->id)->get(); //dd($amendmentValue->isEmpty());
            if ($amendment_fields && $amendmentValue->isEmpty()) {
                $result2 = \App\Models\Cyo_WorkOrder_fields::create([
                            'workorder_id' => $result1->id,
                            'notice_id' => $notice_id,
                            'notice_field_id' => $amendment_fields[0]['id'],
                            'value' => ' ',
                ]);
            }
        }

//dd($is_rescind,$rescind_flag);
        return redirect("customer/create-your-own/edit/" . $result1->id . "/" . $notice_id . "/" . $is_rescind);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, $notice_id, $is_rescind) {
     //dd("jkl");
        $data['is_rescind'] = $is_rescind;
        /* check  customer_subscriptions */
        $cutomer_subscription_info = \App\Models\Customer_subscription::where('customer_id', Auth::user()->customer->id)->where('end_date', '>=', date('Y-m-d'))->first();
        if (empty($cutomer_subscription_info)) {
            /* redirect to pricing */
            Session::flash('warning', 'Your subscription for Create your own has been expired, Please subscribe again.');
            return redirect('/pricing#showsubscribe');
        }
        $notice = \App\Models\Notice::select('name', 'id', 'type', 'is_claim_of_lien','master_notice_id')->where('id', $notice_id)->get()->toArray();
        $data['notice'] = $notice[0];
        $data['label_generated_count'] = 0;
        $data['cities'] = \App\Models\City::select()->get();
        $data['countries'] = \App\Models\Country::select()->get();
        $data['states'] = \App\Models\State::select()->orderBy('name', 'asc')->get();
        // $data['recipients'] = \App\Models\Cyo_WorkOrder_Recipient::select()->where('work_order_id', $id)->get();
        $data['recipients'] = \App\Models\Cyo_WorkOrder_Recipient::select('cyo__work_order__recipients.id', 'work_order_id', 'category_id', 'name', 'contact', 'mobile', 'cyo__work_order__recipients.address', 'city_id', 'state_id', 'zip', 'fax', 'cyo__work_order__recipients.created_at', 'cyo__work_order__recipients.updated_at', \DB::raw('cyo__usps_addresses.address as cyo_usps_address'), \DB::raw('cyo__usps_addresses.city as usps_city'), \DB::raw('cyo__usps_addresses.state as usps_state'), 'zipcode', 'zipcode_add_on', 'dpb', 'check_digit', 'comment')->leftJoin('cyo__usps_addresses', 'cyo__usps_addresses.recipient_id', '=', 'cyo__work_order__recipients.id')->where('work_order_id', $id)->get();
        $data['rush_hour'] = \App\Models\Cyo_WorkOrders::select('rush_hour_charges','rush_hour_amount')->where('id',$id)->get();
        //dd($data['rush_hour_charges']);
        $verified_address_count = \App\Models\Cyo_WorkOrder_Recipient::select(\DB::raw('COUNT(cyo__usps_addresses.recipient_id) as verified_address_count'))->leftJoin('cyo__usps_addresses', 'cyo__usps_addresses.recipient_id', '=', 'cyo__work_order__recipients.id')->where('work_order_id', $id)->get();
        $data['verified_address_count'] = ($verified_address_count) ? $verified_address_count[0]['verified_address_count'] : 0;

        $data['officers_directors'] = \App\Models\CustomerAgent::where('customer_id', '=', Auth::user()->customer->id)
                ->get();
        $data['categories'] = \App\Models\Category::select()->orderBy('name', 'ASC')->get();
        $data['project_roles'] = \App\Models\ProjectRoles::select()->orderBy('name', 'ASC')->get();
        $data['count'] = \App\Models\Cyo_WorkOrder_Recipient::select(DB::raw('COUNT(*) AS total'), 'categories.name')->groupBy('cyo__work_order__recipients.category_id')->join('categories', 'categories.id', '=', 'cyo__work_order__recipients.category_id')->where('work_order_id', $id)->get();
        $data['id'] = $id;
        $data['notice_id'] = $notice_id;
        $result = \App\Models\Cyo_WorkOrders::select('state_id', 'is_rescind')->join('notice_fields', 'notice_fields.notice_id', '=', 'cyo__work_orders.notice_id')->where('cyo__work_orders.id', $id)->limit(1)->get()->toArray();
        if ($result[0]['is_rescind'] == '1') {
            $data['is_rescind'] = 1;
        }
        $data['attachment'] = \App\Models\Cyo_WorkOrder_Attachment::where('work_order_id', '=', $id)
                ->get();
        $data['projectTypes'] = \App\Models\ProjectType::select()->get();
        $data['names'] = \App\Models\Contact::select('contacts.id', 'contacts.company_name')
//                ->where('contact_details.type', '=', 0)
                ->where('contacts.customer_id', '=', Auth::user()->customer->id)
//                ->Join('contact_details', 'contact_details.contact_id', '=', 'contacts.id')
                ->orderBy('contacts.id', 'DESC')
                ->get();

        $data['service_types'] = \App\Models\StampsServices::where(['status' => '1'])->get(['description', 'abbr', 'id'])->toArray();
        $data['package_types'] = \App\Models\StampsAddOns::join('stamps_service_package_addon', 'stamps_service_package_addon.add_on', '=', 'stamps_add_ons.id')
                        ->join('stamps_service_type', 'stamps_service_type.id', '=', 'stamps_service_package_addon.service_type')
                        ->join('stamps_packages', 'stamps_packages.id', '=', 'stamps_service_package_addon.package')
                        ->where(['stamps_packages.status' => '1'])
                        ->get([\DB::raw('DISTINCT(stamps_packages.name)'), 'stamps_packages.id'])->toArray();

        if ($data['recipients']) {
            for ($cnt = 0; $cnt < count($data['recipients']); $cnt++) {
                $labels = \App\Models\Cyo_StampsLabel::leftJoin('stamps_service_type', 'stamps_service_type.id', 'cyo__stamps_labels.service_type')
                        ->leftJoin('stamps_packages', 'stamps_packages.id', 'cyo__stamps_labels.package_type')
                        ->where(['recipient_id' => $data['recipients'][$cnt]['id']])
                        ->first(['cyo__stamps_labels.*']);
                if (!empty($labels) && $labels['generated_label'] == 'yes') {
                    $data['label_generated_count'] ++;
                    $data['recipients'][$cnt]['label_generated'] = 'yes';
                } else {
                    $data['recipients'][$cnt]['label_generated'] = 'no';
                }
                $data['recipients'][$cnt]['recipient_shipping_labels'] = $labels;
                $data['recipients'][$cnt]['add_ons'] = \App\Models\StampsAddOns::join('stamps_service_package_addon', 'stamps_service_package_addon.add_on', '=', 'stamps_add_ons.id')
                                ->join('stamps_service_type', 'stamps_service_type.id', '=', 'stamps_service_package_addon.service_type')
                                ->join('stamps_packages', 'stamps_packages.id', '=', 'stamps_service_package_addon.package')
                                ->where(['stamps_add_ons.status' => '1'])
                                ->where('stamps_service_type.id', '=', 1)
                                ->where('stamps_packages.id', '=', 2)
                                // ->where(function($query) use ($labels) {  
                                //     if(isset($labels) && !empty($labels))
                                //     {
                                //         $query->where('stamps_service_type.id','=',$labels['service_type']);
                                //         $query->where('stamps_packages.id','=',$labels['package_type']);
                                //     }	
                                // })
                                ->get([\DB::raw('DISTINCT(stamps_add_ons.id)'), 'stamps_add_ons.description'])->toArray();

                if (!empty($data['recipients'][$cnt]['add_ons'])) {
                    $cnt1 = 1;
                    $temp_add_ons = $data['recipients'][$cnt]['add_ons'];
                    for ($counter = 0; $counter < count($temp_add_ons); $counter++) {
                        $prohibited = \App\Models\StampsAddOnsProhibited::join('stamps_add_ons', 'stamps_add_ons_prohibited.prohibited', '=', 'stamps_add_ons.id')
                                        ->where(['stamps_add_ons_prohibited.add_on' => $temp_add_ons[$counter]['id']])
                                        ->pluck('prohibited')->toArray();

                        $required = \App\Models\StampsAddOnsRequired::join('stamps_add_ons', 'stamps_add_ons_required.required', '=', 'stamps_add_ons.id')
                                        ->where(['stamps_add_ons_required.add_on' => $data['recipients'][$cnt]['add_ons'][$counter]['id']])
                                        ->pluck('required')->toArray();
                        $temp_add_ons[$counter]['prohibited'] = json_encode($prohibited);
                        $temp_add_ons[$counter]['required'] = json_encode($required);
                    }
                    unset($data['recipients'][$cnt]['add_ons']);
                    $data['recipients'][$cnt]['add_ons'] = $temp_add_ons;
                }
            }
        }

        $work_order_status = \App\Models\Cyo_WorkOrders::find($id);
        if ($work_order_status->status == '0') {
            $data['status'] = 'draft';
        } else {
            $data['status'] = 'request';
        }
        $data['work_order_status'] = $work_order_status->status;
        $admin_email = \App\User::leftjoin('role_users', 'role_users.user_id', '=', 'users.id')
                ->where('role_users.role_id', '=', 2)
                ->select('users.email')
                ->first();
        $account_manager_email = \App\Models\Customer::leftjoin('users', 'users.id', '=', 'customers.account_manager_id')
                ->select('users.email')
                ->find(Auth::user()->customer->id);

        /*  if (isset($account_manager_email) && !empty($account_manager_email)) {
          $data['note_emails'] = [
          $admin_email->email,
          $account_manager_email->email
          ];
          } else {
          $data['note_emails'] = [
          $admin_email->email
          ];
          } */
        $data['note_emails'] = get_user_notice_email($work_order_status->customer_id);
        $data['notes'] = \App\Models\Cyo_WorkOrder_Notes::leftjoin('users', 'users.id', '=', 'cyo__work_order__notes.user_id')->where('customer_id', '=', Auth::user()->customer->id)
                ->where('work_order_id', '=', $id)
                ->where('visibility', '=', 0)
                ->orderBy('cyo__work_order__notes.id', 'DESC')
                ->get();
        $data['corrections'] = \App\Models\Cyo_WorkOrder_Corrections::leftjoin('users', 'users.id', '=', 'cyo__work_order__corrections.user_id')->where('customer_id', '=', Auth::user()->customer->id)
                ->where('cyo__work_order__corrections.work_order_id', '=', $id)
                ->where('cyo__work_order__corrections.visibility', '=', 0)
                ->orderBy('cyo__work_order__corrections.id', 'DESC')
                ->get();//dd('hi');
        $data['work_order_cyo_doc_payment'] =   \App\Models\Cyo_Doc_Payment_History::where(['work_order_id'=>$id])->count();       
        /* if ($request->continue == 'Continue') {
            return redirect()->to('customer/create-your-own/edit-new/' . $id . '/' . $notice_id . '/'. $is_rescind);
        } else {
            return redirect('customer/create-your-own/work-order-history');
        }*/
        $data['extraTitle'] = ' - Create Your Own';
        if ($notice[0]['type'] == 2) {
            $data['notice_fields_section1'] = \App\Models\NoticeField::select()->join('cyo__work_order_fields', 'cyo__work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('cyo__work_order_fields.notice_id', $notice_id)->where('cyo__work_order_fields.workorder_id', $id)->where('notice_fields.section', 1)->orderBy('sort_order', 'asc')->get();

            $data['notice_fields_section2'] = \App\Models\NoticeField::select()->join('cyo__work_order_fields', 'cyo__work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('cyo__work_order_fields.notice_id', $notice_id)->where('cyo__work_order_fields.workorder_id', $id)->where('notice_fields.section', 2)->orderBy('sort_order', 'asc')->get();

            $data['tab'] = 'project';
            return view('customer.create_your_own.edit_soft_notices', $data);
        } else {
            $notice = \App\Models\Notice::select('name', 'id', 'is_claim_of_lien','master_notice_id')->where('id', $notice_id)->get()->toArray();
            $data['notice'] = $notice[0];
            $data['notice_id'] = $notice_id;
            $data['notice_fields'] = \App\Models\NoticeField::select()->join('cyo__work_order_fields', 'cyo__work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('cyo__work_order_fields.notice_id', $notice_id)->where('cyo__work_order_fields.workorder_id', $id)->orderBy('sort_order', 'asc')->get();
            $notice = \App\Models\Notice::select('name')->where('id', $notice_id)->get()->toArray();
            $data['tab'] = 'project';

            return view('customer.create_your_own.edit', $data);
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function researchEditNextStep(Request $request, $id, $notice_id, $is_rescind) {
     
        $data['is_rescind'] = $is_rescind;
        $data['label_generated_count'] = 0;
        $data['work_order_cyo_doc_payment'] = 0;
        
        /* check  customer_subscriptions */
        $cutomer_subscription_info = \App\Models\Customer_subscription::where('customer_id', Auth::user()->customer->id)->where('end_date', '>=', date('Y-m-d'))->first();
        if (empty($cutomer_subscription_info)) {
            /* redirect to pricing */
            Session::flash('warning', 'Your subscription for Create your own has been expired, Please subscribe again.');
            return redirect('/pricing#showsubscribe');
        }
        //  $fields = array_merge($request->except(['_token', 'work_order', 'continue', 'type', 'notice_id', 'edit_soft_notices_attachment', 'file_name', 'doc_input', 'hidden_city_val', 'duplicate', 'remove_doc', 'recipient_county_id', 'visibility', 'recipient_notary_county_id', 'recipient_project_county_id', 'recipient_recorded_county_id']));
        $fields = array_merge($request->except(['_token', 'work_order', 'continue', 'type', 'notice_id', 'edit_soft_notices_attachment', 'work_order_attachment', 'file_name', 'doc_input', 'hidden_city_val', 'duplicate', 'remove_doc', 'parent_work_order_previous', 'recipient_county_id', 'contracted_by_exist', 'document_type', 'work_order_attachment', 'visibility', 'recipient_notary_county_id', 'recipient_project_county_id', 'recipient_recorded_county_id']));

        $customerId = Auth::user()->customer->id;
        $userId = Auth::user()->id;
        $account_manager_id = Auth::user()->customer->account_manager_id;
        
        $data['recipients'] = \App\Models\Cyo_WorkOrder_Recipient::select()->where('work_order_id', $id)->get();
        
       // $work_order_id = $result1->id;
        $workorderiddetails = \App\Models\Cyo_WorkOrders::find($id);
        $userId = $workorderiddetails->user_id;
        $data['work_order_status'] = $workorderiddetails->status;

        $work_order_id = $id;

        $notice_id = $workorderiddetails->notice_id;
        $notice = \App\Models\Notice::select('name', 'id', 'type', 'is_claim_of_lien','master_notice_id')->where('id', $notice_id)->get()->toArray();
        $data['notice'] = $notice[0];
        $data['cities'] = \App\Models\City::select()->get();
        $data['countries'] = \App\Models\Country::select()->get();
        $data['states'] = \App\Models\State::select()->orderBy('name', 'asc')->get();
        $data['categories'] = \App\Models\Category::select()->orderBy('name', 'ASC')->get();
        $data['project_roles'] = \App\Models\ProjectRoles::select()->orderBy('name', 'ASC')->get();
        $data['duplicate'] = $request->duplicate;
        $data['count'] = \App\Models\Cyo_WorkOrder_Recipient::select(DB::raw('COUNT(*) AS total'), 'categories.name')->groupBy('cyo__work_order__recipients.category_id')->join('categories', 'categories.id', '=', 'cyo__work_order__recipients.category_id')->where('work_order_id', $id)->get();
        $data['id'] = $id;
        $data['officers_directors'] = \App\Models\CustomerAgent::where('customer_id', '=', Auth::user()->customer->id)
                ->get();
        $data['projectTypes'] = \App\Models\ProjectType::select()->get();
        $data['notice_id'] = $notice_id;

        $result = \App\Models\Cyo_WorkOrders::select('state_id')->join('notice_fields', 'notice_fields.notice_id', '=', 'cyo__work_orders.notice_id')->where('cyo__work_orders.id', $id)->limit(1)->get()->toArray();
        $data['names'] = \App\Models\Contact::select('contacts.id', 'contacts.company_name')
//                ->where('contact_details.type', '=', 0)
                ->where('contacts.customer_id', '=', Auth::user()->customer->id)
//                ->Join('contact_details', 'contact_details.contact_id', '=', 'contacts.id')
                ->orderBy('contacts.id', 'DESC')
                ->get();//dd($data['recipients'],$id );
        /*$data['recipients'] = \App\Models\Cyo_WorkOrder_Recipient::select('cyo__work_order__recipients.id', 'work_order_id', 'category_id', 'name', 'contact', 'mobile', 'cyo__work_order__recipients.address', 'city_id', 'state_id', 'zip', 'fax', 'cyo__work_order__recipients.created_at', 'cyo__work_order__recipients.updated_at', \DB::raw('cyo__usps_addresses.address as cyo_usps_address'), \DB::raw('cyo__usps_addresses.city as usps_city'), \DB::raw('cyo__usps_addresses.state as usps_state'), 'zipcode', 'zipcode_add_on', 'dpb', 'check_digit', 'comment')->leftJoin('cyo__usps_addresses', 'cyo__usps_addresses.recipient_id', '=', 'cyo__work_order__recipients.id')->where('work_order_id', $id)->get();*/
         $data['recipients'] = \App\Models\Cyo_WorkOrder_Recipient::select('cyo__work_order__recipients.id', 'work_order_id', 'category_id', 'name', 'contact', 'mobile', 'cyo__work_order__recipients.address', 'city_id', 'state_id', 'zip', 'fax', 'cyo__work_order__recipients.created_at', 'cyo__work_order__recipients.updated_at', \DB::raw('cyo__usps_addresses.address as cyo_usps_address'), \DB::raw('cyo__usps_addresses.city as usps_city'), \DB::raw('cyo__usps_addresses.state as usps_state'), 'zipcode', 'zipcode_add_on', 'dpb', 'check_digit', 'comment')->leftJoin('cyo__usps_addresses', 'cyo__usps_addresses.recipient_id', '=', 'cyo__work_order__recipients.id')->where('work_order_id', $id)->get();
         $data['rush_hour'] = \App\Models\Cyo_WorkOrders::select('rush_hour_charges','rush_hour_amount')->where('id',$id)->get();
        $verified_address_count = \App\Models\Cyo_WorkOrder_Recipient::select(\DB::raw('COUNT(cyo__usps_addresses.recipient_id) as verified_address_count'))->leftJoin('cyo__usps_addresses', 'cyo__usps_addresses.recipient_id', '=', 'cyo__work_order__recipients.id')->where('work_order_id', $id)->get();
        $data['verified_address_count'] = ($verified_address_count) ? $verified_address_count[0]['verified_address_count'] : 0;
        
        /*$verified_address_count = \App\Models\Cyo_WorkOrder_Recipient::select(\DB::raw('COUNT(cyo__usps_addresses.recipient_id) as verified_address_count'))->leftJoin('cyo__usps_addresses', 'cyo__usps_addresses.recipient_id', '=', 'cyo__work_order__recipients.id')->where('work_order_id', $id)->get();
        $data['verified_address_count'] = ($verified_address_count) ? $verified_address_count[0]['verified_address_count'] : 0;*/

        $data['service_types'] = \App\Models\StampsServices::where(['status' => '1'])->get(['description', 'abbr'])->toArray();
        $admin_email = \App\User::leftjoin('role_users', 'role_users.user_id', '=', 'users.id')
                ->where('role_users.role_id', '=', 2)
                ->select('users.email')
                ->first();
        $account_manager_email = \App\Models\Customer::leftjoin('users', 'users.id', '=', 'customers.account_manager_id')
                ->select('users.email')
                ->find(Auth::user()->customer->id);
        $data['attachment'] = \App\Models\Cyo_WorkOrder_Attachment::where('work_order_id', '=', $id)
                ->get();
        $data['work_order_cyo_doc_payment'] =   \App\Models\Cyo_Doc_Payment_History::where(['work_order_id' => $id])->count();
        /* if (isset($account_manager_email) && !empty($account_manager_email)) {
          $data['note_emails'] = [
          $admin_email->email,
          $account_manager_email->email
          ];
          } else {
          $data['note_emails'] = [
          $admin_email->email
          ];
          } */
        $data['note_emails'] = get_user_notice_email(Auth::user()->customer->id);
        $data['notes'] = \App\Models\Cyo_WorkOrder_Notes::leftjoin('users', 'users.id', '=', 'cyo__work_order__notes.user_id')->where('customer_id', '=', Auth::user()->customer->id)
                ->where('work_order_id', '=', $id)
                ->where('visibility', '=', 0)
                ->orderBy('cyo__work_order__notes.id', 'DESC')
                ->get();
        $data['corrections'] = \App\Models\Cyo_WorkOrder_Corrections::leftjoin('users', 'users.id', '=', 'cyo__work_order__corrections.user_id')->where('customer_id', '=', Auth::user()->customer->id)
                ->where('cyo__work_order__corrections.work_order_id', '=', $id)
                ->where('cyo__work_order__corrections.visibility', '=', 0)
                ->orderBy('cyo__work_order__corrections.id', 'DESC')
                ->get();
        $data['work_order_cyo_doc_payment'] =   \App\Models\Cyo_Doc_Payment_History::where(['work_order_id' => $id])->count();

        $work_order_status = \App\Models\Cyo_WorkOrders::where('id', $id)->limit(1)->get()->toArray();
        $data['work_order_status'] = $work_order_status[0]['status'];
        if ($data['recipients']) {
            for ($cnt = 0; $cnt < count($data['recipients']); $cnt++) {
                $labels = \App\Models\Cyo_StampsLabel::leftJoin('stamps_service_type', 'stamps_service_type.id', 'cyo__stamps_labels.service_type')
                        ->leftJoin('stamps_packages', 'stamps_packages.id', 'cyo__stamps_labels.package_type')
                        ->where(['recipient_id' => $data['recipients'][$cnt]['id']])
                        ->first(['cyo__stamps_labels.*']);
                if (!empty($labels) && $labels['generated_label'] == 'yes') {
                    $data['label_generated_count'] ++;
                    $data['recipients'][$cnt]['label_generated'] = 'yes';
                } else {
                    $data['recipients'][$cnt]['label_generated'] = 'no';
                }
                $data['recipients'][$cnt]['recipient_shipping_labels'] = $labels;
                $data['recipients'][$cnt]['add_ons'] = \App\Models\StampsAddOns::join('stamps_service_package_addon', 'stamps_service_package_addon.add_on', '=', 'stamps_add_ons.id')
                                ->join('stamps_service_type', 'stamps_service_type.id', '=', 'stamps_service_package_addon.service_type')
                                ->join('stamps_packages', 'stamps_packages.id', '=', 'stamps_service_package_addon.package')
                                ->where(['stamps_add_ons.status' => '1'])
                                ->where('stamps_service_type.id', '=', 1)
                                ->where('stamps_packages.id', '=', 2)
                                // ->where(function($query) use ($labels) {  
                                //     if(isset($labels) && !empty($labels))
                                //     {
                                //         $query->where('stamps_service_type.id','=',$labels['service_type']);
                                //         $query->where('stamps_packages.id','=',$labels['package_type']);
                                //     }  
                                // })
                                ->get([\DB::raw('DISTINCT(stamps_add_ons.id)'), 'stamps_add_ons.description'])->toArray();

                if (!empty($data['recipients'][$cnt]['add_ons'])) {
                    $cnt1 = 1;
                    $temp_add_ons = $data['recipients'][$cnt]['add_ons'];
                    for ($counter = 0; $counter < count($temp_add_ons); $counter++) {
                        $prohibited = \App\Models\StampsAddOnsProhibited::join('stamps_add_ons', 'stamps_add_ons_prohibited.prohibited', '=', 'stamps_add_ons.id')
                                        ->where(['stamps_add_ons_prohibited.add_on' => $temp_add_ons[$counter]['id']])
                                        ->pluck('prohibited')->toArray();

                        $required = \App\Models\StampsAddOnsRequired::join('stamps_add_ons', 'stamps_add_ons_required.required', '=', 'stamps_add_ons.id')
                                        ->where(['stamps_add_ons_required.add_on' => $data['recipients'][$cnt]['add_ons'][$counter]['id']])
                                        ->pluck('required')->toArray();
                        $temp_add_ons[$counter]['prohibited'] = json_encode($prohibited);
                        $temp_add_ons[$counter]['required'] = json_encode($required);
                    }
                    unset($data['recipients'][$cnt]['add_ons']);
                    $data['recipients'][$cnt]['add_ons'] = $temp_add_ons;
                }
            }
        }
        //dd($data);
        $data['extraTitle'] = ' - Create Your Own';
            if ($notice[0]['type'] == 2) {
                $data['notice_fields_section1'] = \App\Models\NoticeField::select()->join('cyo__work_order_fields', 'cyo__work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('cyo__work_order_fields.workorder_id', $id)->where('cyo__work_order_fields.notice_id', $notice_id)->where('notice_fields.section', 1)->orderBy('sort_order', 'asc')->get();

                $data['notice_fields_section2'] = \App\Models\NoticeField::select()->join('cyo__work_order_fields', 'cyo__work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('cyo__work_order_fields.notice_id', $notice_id)->where('cyo__work_order_fields.workorder_id', $id)->where('notice_fields.section', 2)->orderBy('sort_order', 'asc')->get();
                $data['tab'] = 'recipients';

                return view('customer.create_your_own.edit_soft_notices', $data);
            } else {
                $data['notice_fields'] = \App\Models\NoticeField::select()->join('cyo__work_order_fields', 'cyo__work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('cyo__work_order_fields.notice_id', $notice_id)->where('cyo__work_order_fields.workorder_id', $id)->orderBy('sort_order', 'asc')->get();

                $data['tab'] = 'recipients';
                return view('customer.create_your_own.edit', $data);
            }
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, $notice_id, $is_rescind) {
        $data['is_rescind'] = $is_rescind;
        $data['label_generated_count'] = 0;
        /* check  customer_subscriptions */
        $cutomer_subscription_info = \App\Models\Customer_subscription::where('customer_id', Auth::user()->customer->id)->where('end_date', '>=', date('Y-m-d'))->first();
        if (empty($cutomer_subscription_info)) {
            /* redirect to pricing */
            Session::flash('warning', 'Your subscription for Create your own has been expired, Please subscribe again.');
            return redirect('/pricing#showsubscribe');
        }
        //  $fields = array_merge($request->except(['_token', 'work_order', 'continue', 'type', 'notice_id', 'edit_soft_notices_attachment', 'file_name', 'doc_input', 'hidden_city_val', 'duplicate', 'remove_doc', 'recipient_county_id', 'visibility', 'recipient_notary_county_id', 'recipient_project_county_id', 'recipient_recorded_county_id']));
        $fields = array_merge($request->except(['_token', 'work_order', 'continue', 'type', 'notice_id', 'edit_soft_notices_attachment', 'work_order_attachment', 'file_name', 'doc_input', 'hidden_city_val', 'duplicate', 'remove_doc', 'parent_work_order_previous', 'recipient_county_id', 'contracted_by_exist', 'document_type', 'work_order_attachment', 'visibility', 'recipient_notary_county_id', 'recipient_project_county_id', 'recipient_recorded_county_id']));

        $fieldsData = [];
        $values = [];
        $i = 0;
        foreach ($fields as $key => $value) {
            $field = explode("_", $key);
            $fieldsData[$i] = $field['0'];
            $values[$i] = $value;
            $i++;
        }

        $customerId = Auth::user()->customer->id;
        $userId = Auth::user()->id;
        $account_manager_id = Auth::user()->customer->account_manager_id;
        if ($request->duplicate == 'duplicate') {

            if ($request->continue == 'Continue') {
                $result1 = \App\Models\Cyo_WorkOrders::create([
                            'notice_id' => $request->notice_id,
                            'customer_id' => $customerId,
                            'order_no' => str_random(5),
                            'user_id' => $userId,
                            'status' => '1',
                            'account_manager_id' => $account_manager_id,
                ]);
                $data['status'] = 'request';
            } else {
                $result1 = \App\Models\Cyo_WorkOrders::create([
                            'notice_id' => $request->notice_id,
                            'customer_id' => $customerId,
                            'order_no' => str_random(5),
                            'user_id' => $userId,
                            'status' => '0',
                            'account_manager_id' => $account_manager_id,
                ]);
                $data['status'] = 'draft';
            }
            $recipients = \App\Models\Cyo_WorkOrder_Recipient::select('id')->where('work_order_id', $id)->get()->toArray();
            foreach ($recipients as $key => $value) {
                # code...
                //dd('ji');
                $recipient = \App\Models\Cyo_WorkOrder_Recipient::find($value['id']);
                \App\Models\Cyo_WorkOrder_Recipient::create([
                    'work_order_id' => $result1->id,
                    'category_id' => $recipient->category_id,
                    'name' => $recipient->name,
                    'contact' => $recipient->contact,
                    'mobile' => $recipient->mobile,
                    'address' => $recipient->address,
                    'city_id' => $recipient->city_id,
                    'state_id' => $recipient->state_id,
                    'zip' => $recipient->zip,
                    'email' => $recipient->email,
                    'attn' => $recipient->attn,
                ]);
            }
            $data['recipients'] = \App\Models\Cyo_WorkOrder_Recipient::select()->where('work_order_id', $result1->id)->get();
            /* if ($result1) {
              Session::flash('success', 'Record Created Successfully');
              } else {
              Session::flash('success', 'Problem in Record saving');
              } */
            $attachment = explode(',|,', trim($request->edit_soft_notices_attachment[0],'|,'));

            foreach ($attachment AS $k => $v) {
              $v = trim($v,'|,');
                if (isset($request->remove_doc) && !empty($request->remove_doc) && $request->remove_doc[0] != '') {
                    $remove_doc = explode(',', $request->remove_doc[0]);

                    $each_attachment = explode(',', $v);
                    if ($each_attachment[0] != '') {
                        $true = !in_array($each_attachment[2], $remove_doc);
                    } else {
                        $true = '';
                    }
                    if (isset($true) && $true != '') {
                        if ($v != '') {

                            $work_order_attachment_obj = new \App\Models\Cyo_WorkOrder_Attachment();
                            $work_order_attachment_obj->work_order_id = $result1->id;
                            $work_order_attachment_obj->type = $each_attachment[0];
                            $work_order_attachment_obj->title = $each_attachment[1];
                            $work_order_attachment_obj->file_name = $each_attachment[2];
                            $work_order_attachment_obj->original_file_name = $each_attachment[3];
                             if(!empty($each_attachment[2])){
                                                        $work_order_attachment_obj->file_name = $result1->id . '_' . $each_attachment[2];
                                                        }else{
                                                         $work_order_attachment_obj->file_name = " ";
                                                        }
                                                        if(!empty($each_attachment[3])){
                                                        $work_order_attachment_obj->original_file_name = $each_attachment[3];
                                                        }else{
                                                           $work_order_attachment_obj->original_file_name = "";
                                                        }

                            $work_order_attachment_obj->save();
                        }
                    }
                    //  else{ 
                    $previous_attachments = \App\Models\Cyo_WorkOrder_Attachment::where('work_order_id', '=', $id)->get();

                    if (isset($previous_attachments) && count($previous_attachments) > 0) {
                        foreach ($previous_attachments AS $k1 => $a1) {

                           /* $each_remove_doc = explode(',', $request->remove_doc[0]);
                            foreach ($each_remove_doc As $k => $each) {
                                $explode_v = explode('_', $each);
                                $count = count($explode_v);
                                if($count>1){
                                $each_remove_doc[$k] = $explode_v[$count - 3] . '_' . $explode_v[$count - 2] . '_' . $explode_v[$count - 1];
                              }
                            }

                            $true = !in_array($a1->file_name, $each_remove_doc);*/
                            $true = 'true';

                            if (isset($true) && $true != '') {

                                $work_order_attachment_obj = new \App\Models\Cyo_WorkOrder_Attachment();
                                $work_order_attachment_obj->work_order_id = $result1->id;
                                $work_order_attachment_obj->type = $a1->type;
                                $work_order_attachment_obj->title = $a1->title;
                                $work_order_attachment_obj->file_name = date('H') . '_' . $a1->file_name;
                                //$work_order_attachment_obj->original_file_name = $a1->original_file_name;
                                                        if(!empty($a1->original_file_name)){
                                                        $work_order_attachment_obj->original_file_name = $a1->original_file_name;
                                                        }else{
                                                           $work_order_attachment_obj->original_file_name = "";
                                                        }


                                $work_order_attachment_obj->save();
                            }
                        }
                    }
                    //  }
                } else {

                    if ($v != '') {
                        $each_attachment = explode(',', $v);
                        $work_order_attachment_obj = new \App\Models\Cyo_WorkOrder_Attachment();
                        $work_order_attachment_obj->work_order_id = $result1->id;
                        $work_order_attachment_obj->type = $each_attachment[0];
                        $work_order_attachment_obj->title = $each_attachment[1];
                        $work_order_attachment_obj->file_name = $each_attachment[2];
                        $work_order_attachment_obj->original_file_name = $each_attachment[3];

                        $work_order_attachment_obj->save();
                    }

                    $previous_attachments = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $id)->get();

                    if (isset($previous_attachments) && count($previous_attachments) > 0) {
                        foreach ($previous_attachments AS $k1 => $a1) {
                            $work_order_attachment_obj = new \App\Models\Cyo_WorkOrder_Attachment();
                            $work_order_attachment_obj->work_order_id = $result1->id;
                            $work_order_attachment_obj->type = $a1->type;
                            $work_order_attachment_obj->title = $a1->title;
                            $work_order_attachment_obj->file_name = date('H') . '_' . $a1->file_name;
                            $work_order_attachment_obj->original_file_name = $a1->original_file_name;

                            $work_order_attachment_obj->save();
                        }
                    }
                }
            }
            $attachment = \App\Models\Cyo_WorkOrder_Attachment::where('work_order_id', '=', $result1->id)
                    ->get();
        } else {
            if ($request->continue == 'Continue') {
                $result1 = \App\Models\Cyo_WorkOrders::find($id);
                $result1->notice_id = $request->notice_id;
                $result1->customer_id = $customerId;
                $result1->order_no = str_random(5);
                $result1->user_id = $userId;
                if ($result1->status == '0') {
                    $result1->status = 1;
                }

                $result1->save();
                $data['status'] = 'request';
                Session::flash('success', 'Your work order # ' . $result1->id . ' Project information has been successfully saved. Please add recipients.');
            } else {
                $result1 = \App\Models\Cyo_WorkOrders::find($id);
                $result1->notice_id = $request->notice_id;
                $result1->customer_id = $customerId;
                $result1->order_no = str_random(5);
                $result1->user_id = $userId;
                if ($result1->status == '0') {
                    $result1->status = 0;
                }

                $result1->save();
                $data['status'] = 'draft';
                Session::flash('success', 'Your work order # ' . $result1->id . ' has been saved as a draft, You must complete Work Order and submit at a later time.<a href="' . url('customer/create-your-own/view/' . $result1->id) . '">Click here to print.</a>');
            }
            $data['recipients'] = \App\Models\Cyo_WorkOrder_Recipient::select()->where('work_order_id', $result1->id)->get();
        }
        if ($request->duplicate != 'duplicate') {
            if ($result1) {
                $attachment = explode(',|,', trim($request->edit_soft_notices_attachment[0],'|,'));
                // if (isset($attachment) && !empty($attachment)) {
                foreach ($attachment AS $k => $v) {
                  $v = trim($v,'|,');
                  if($v!='|' && $v!='|,|'){
                    if (isset($request->remove_doc) && !empty($request->remove_doc) && $request->remove_doc[0] != '') {
                        $remove_doc = explode(',', $request->remove_doc[0]);

                        $each_attachment = explode(',', $v);
                        if ($each_attachment[0] != '' && !empty($each_attachment[4])) {
                            $true = !in_array($each_attachment[4], $remove_doc);
                        } else {
                            $true = '';
                        }
                        if (isset($true) && $true != '') {
                            if ($v != '') {

                                $work_order_attachment_obj = new \App\Models\Cyo_WorkOrder_Attachment();
                                $work_order_attachment_obj->work_order_id = $result1->id;
                                $work_order_attachment_obj->type = $each_attachment[0];
                                $work_order_attachment_obj->title = $each_attachment[1];
                                $work_order_attachment_obj->file_name = $each_attachment[2];
                                $work_order_attachment_obj->original_file_name = $each_attachment[3];
                                $work_order_attachment_obj->save();
                            }
                        }
                    } else {
                        if ($v != '') {
                            $each_attachment = explode(',', $v);
                            $work_order_attachment_obj = new \App\Models\Cyo_WorkOrder_Attachment();
                            $work_order_attachment_obj->work_order_id = $result1->id;
                            $work_order_attachment_obj->type = $each_attachment[0];
                            $work_order_attachment_obj->title = $each_attachment[1];
                            $work_order_attachment_obj->file_name = $each_attachment[2];
                            $work_order_attachment_obj->original_file_name = $each_attachment[3];
                            $work_order_attachment_obj->save();
                        }
                    }
                }
                }
                //For parent work document and recipient and contracted by
                foreach ($fields as $key => $value) {
                    $field = explode("_", $key);


                    if (isset($field[3])) {
                        $check_field = $field[1] . '_' . $field[2] . '_' . $field[3];
                        if ($check_field == 'parent_work_order') {
                            $parent_work_order = $value;
                            $parent_work_order_previous = $request->parent_work_order_previous;
                            if ($parent_work_order_previous != $parent_work_order) {
                                //If parent work order is selected document of parent work order will inserted
                                if ($check_field == 'parent_work_order' && $parent_work_order != '') {
                                    if (isset($request->remove_doc) && !empty($request->remove_doc) && $request->remove_doc[0] != '') {
                                        $previous_attachments = \App\Models\Cyo_WorkOrder_Attachment::where('work_order_id', '=', $parent_work_order)->get();

                                        if (isset($previous_attachments) && count($previous_attachments) > 0) {
                                            foreach ($previous_attachments AS $k1 => $a1) {

                                              /*  $each_remove_doc = explode(',', $request->remove_doc[0]);
                                                foreach ($each_remove_doc As $k => $each) {
                                                    $explode_v = explode('_', $each);
                                                    $count = count($explode_v);
                                                    if($count>1){
                                                    $each_remove_doc[$k] = $explode_v[$count - 3] . '_' . $explode_v[$count - 2] . '_' . $explode_v[$count - 1];
                                                  }
                                                }

                                                $true = !in_array($a1->file_name, $each_remove_doc);

*/
                                                $true = "true";
                                                if (isset($true) && $true != '') {

                                                    $work_order_attachment_obj = new \App\Models\Cyo_WorkOrder_Attachment();
                                                    $work_order_attachment_obj->work_order_id = $result1->id;
                                                    $work_order_attachment_obj->type = $a1->type;
                                                    $work_order_attachment_obj->title = $a1->title;

                                                   if(!empty($a1->file_name)){
                                                        $work_order_attachment_obj->file_name = $result1->id . '_' . $a1->file_name;
                                                        }else{
                                                         $work_order_attachment_obj->file_name = "";
                                                        }
                                                        if(!empty($a1->original_file_name)){
                                                        $work_order_attachment_obj->original_file_name = $a1->original_file_name;
                                                        }else{
                                                           $work_order_attachment_obj->original_file_name = "";
                                                        }
                                                        
                                                    if (Auth::user()->hasRole('account-manager')) {
                                                        $work_order_attachment_obj->visibility = $a1->visibility;
                                                    } else {
                                                        $work_order_attachment_obj->visibility = 0;
                                                    }
                                                    $work_order_attachment_obj->save();
                                                    if (file_exists(public_path() . '/attachment/cyo_work_order_document/parent_document/' . $a1->file_name) && !empty($a1->file_name)) {
                                                        rename(public_path() . '/attachment/cyo_work_order_document/parent_document/' . $a1->file_name, public_path() . '/attachment/cyo_work_order_document/parent_document/' . $work_order_attachment_obj->file_name);
                                                        \File::copy(public_path() . '/attachment/cyo_work_order_document/parent_document/' . $work_order_attachment_obj->file_name, public_path() . '/attachment/cyo_work_order_document/' . $work_order_attachment_obj->file_name);
                                                        unlink(public_path() . '/attachment/cyo_work_order_document/parent_document/' . $work_order_attachment_obj->file_name);
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        $remove_doc = explode(',', $request->remove_doc[0]);
                                        $previous_attachments = \App\Models\Cyo_WorkOrder_Attachment::where('work_order_id', '=', $parent_work_order)->get();

                                        if (isset($previous_attachments) && count($previous_attachments) > 0) {
                                            foreach ($previous_attachments AS $k1 => $a1) {
                                                $cyo_work_order_attachment_obj = new \App\Models\Cyo_WorkOrder_Attachment();
                                                $cyo_work_order_attachment_obj->work_order_id = $result1->id;
                                                $cyo_work_order_attachment_obj->type = $a1->type;
                                                $cyo_work_order_attachment_obj->title = $a1->title;
                                                
                                                if(!empty($a1->file_name)){
                                                        $cyo_work_order_attachment_obj->file_name = $result1->id . '_' . $a1->file_name;
                                                        }else{
                                                         $cyo_work_order_attachment_obj->file_name = "";
                                                        }
                                                        if(!empty($a1->original_file_name)){
                                                        $cyo_work_order_attachment_obj->original_file_name = $a1->original_file_name;
                                                        }else{
                                                           $cyo_work_order_attachment_obj->original_file_name = "";
                                                        }
                                                if (Auth::user()->hasRole('account-manager')) {
                                                    $cyo_work_order_attachment_obj->visibility = $a1->visibility;
                                                } else {
                                                    $cyo_work_order_attachment_obj->visibility = 0;
                                                }
                                                $cyo_work_order_attachment_obj->save();

                                                if (file_exists(public_path() . '/attachment/cyo_work_order_document/parent_document/' . $a1->file_name) && !empty($a1->file_name)) {
                                                    rename(public_path() . '/attachment/cyo_work_order_document/parent_document/' . $a1->file_name, public_path() . '/attachment/cyo_work_order_document/parent_document/' . $cyo_work_order_attachment_obj->file_name);
                                                    \File::copy(public_path() . '/attachment/cyo_work_order_document/parent_document/' . $cyo_work_order_attachment_obj->file_name, public_path() . '/attachment/cyo_work_order_document/' . $cyo_work_order_attachment_obj->file_name);
                                                    unlink(public_path() . '/attachment/cyo_work_order_document/parent_document/' . $cyo_work_order_attachment_obj->file_name);
                                                }
                                            }
                                        }
                                    }
                                    $data['parent_work_order'] = $parent_work_order;

                                    $first_category = \App\Models\Category::where('name', 'Contracted By')->first();
                                    /* save parent work order recipient  */
                                    if (isset($request->contracted_by_exist)) {
                                        $parent_work_order_recipients = \App\Models\Cyo_WorkOrder_Recipient::select()->where('work_order_id', $parent_work_order)->where('category_id', '!=', $first_category->id)->get();
                                    } else {
                                        $parent_work_order_recipients = \App\Models\Cyo_WorkOrder_Recipient::select()->where('work_order_id', $parent_work_order)->get();
                                    }
                                    \App\Models\Cyo_WorkOrder_Recipient::where( 'work_order_id', $result1->id)->where('parent_work_order','!=',NULL)->delete();
                                    if (isset($parent_work_order_recipients) && count($parent_work_order_recipients) > 0) {
                                        foreach ($parent_work_order_recipients As $k => $each_parent_recipient) {
                                            $save_recipients = \App\Models\Cyo_WorkOrder_Recipient::create([
                                                        'work_order_id' => $result1->id,
                                                        'category_id' => $each_parent_recipient->category_id,
                                                        'name' => $each_parent_recipient->name,
                                                        'contact' => $each_parent_recipient->contact,
                                                        'address' => $each_parent_recipient->address,
                                                        'city_id' => $each_parent_recipient->city_id,
                                                        'state_id' => $each_parent_recipient->state_id,
                                                        'zip' => $each_parent_recipient->zip,
                                                        'email' => $each_parent_recipient->email,
                                                        'attn' => $each_parent_recipient->attn,
                                                        'parent_work_order'=> $parent_work_order,
                                            ]);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //Insert contracted by is first recipient
                    if (isset($field[2])) {
                        $check_contracted_by_field = $field[1] . '_' . $field[2];
                        if ($check_contracted_by_field == 'contracted_by') {
                            $first_category = \App\Models\Category::where('name', 'Contracted By')->first();
                            $contracted_by = $value;
                            if ($contracted_by != "") {
                                /* check contracted by is exist or not */
                                $contracted_by_check = \App\Models\Contact::where('company_name', $contracted_by)->first();
                                if (empty($contracted_by_check)) {
                                    $contact = \App\Models\Contact::create([
                                                'customer_id' => Auth::user()->customer->id,
                                                'company_name' => $contracted_by,
                                    ]);
                                }
                            }
                            $contactdetails = \App\Models\Contact::where('company_name', '=', $contracted_by)
                                    ->where('customer_id', '=', $customerId)
                                    ->first();
                            if (isset($contactdetails) && $contactdetails != null) {
                                /* check contract details exit or not */
                                $check_contract_recipient_exist = \App\Models\Cyo_WorkOrder_Recipient::where(['category_id' => $first_category->id, 'work_order_id' => $result1->id])->first();
                                /* if not empty delete previouse record */
                                if (!empty($check_contract_recipient_exist)) {
                                    if ($check_contract_recipient_exist->name != $contracted_by) {
                                        $contract_recipient_delete = \App\Models\Cyo_WorkOrder_Recipient::where(['category_id' => $first_category->id, 'work_order_id' => $result1->id])->delete();
                                        $check_contract_recipient_exist = [];
                                    }
                                }
                                if (empty($check_contract_recipient_exist)) {
                                    $Recipient = \App\Models\Cyo_WorkOrder_Recipient::create([
                                                'work_order_id' => $result1->id,
                                                'category_id' => $first_category->id,
                                                'name' => $contactdetails->company_name,
                                                'contact' => $contactdetails->phone,
                                                'address' => $contactdetails->mailing_address,
                                                'city_id' => $contactdetails->city_id,
                                                'state_id' => $contactdetails->state_id,
                                                'zip' => $contactdetails->zip,
                                                'email' => $contactdetails->email,
                                                'attn' => $contactdetails->attn
                                    ]);
                                }
                            }
                        }
                    }
                }
            }
        }
        $work_order_id = $result1->id;

        $result2 = \App\Models\Cyo_WorkOrder_fields::select()->where('workorder_id', $work_order_id)->delete();

        foreach ($fieldsData as $key => $value) {

            if ($values[$key] == NULL) {
                $result2 = \App\Models\Cyo_WorkOrder_fields::create([
                            'workorder_id' => $work_order_id,
                            'notice_id' => $request->notice_id,
                            'notice_field_id' => $value,
                            'value' => '',
                ]);
            } else
                $result2 = \App\Models\Cyo_WorkOrder_fields::create([
                            'workorder_id' => $work_order_id,
                            'notice_id' => $request->notice_id,
                            'notice_field_id' => $value,
                            'value' => nl2br($values[$key]),
                ]);
        }


     $arr=array_filter($fields, function($k) {
           return strpos($k, 'last_date_on_the_job')>0;
     }, ARRAY_FILTER_USE_KEY);
      $last_date_on_the_job_val=array_pop($arr);
      //dd($last_date_on_the_job_val);
     $arr_parent_work_order=array_filter($fields, function($k) {
        return strpos($k, 'parent_work_order')>0;
     }, ARRAY_FILTER_USE_KEY); 

     $parent_work_order_id=array_pop($arr_parent_work_order);

      $parent_notice_id = \App\Models\Cyo_WorkOrders::select('notice_id')->where('id', $parent_work_order_id)->first();
         // dd($parent_notice_id);
      if(isset($parent_notice_id)){
          $notice_field_id = \App\Models\NoticeField::select('id')->where('notice_id', $parent_notice_id->notice_id)
          ->where('name','last_date_on_the_job')->first();
          if(!empty($notice_field_id)){
             if($last_date_on_the_job_val==NULL){
                $wrk_order_fields=\App\Models\Cyo_WorkOrder_fields::where('workorder_id',$parent_work_order)
                ->where('notice_id',$parent_notice_id->notice_id)
                ->where('notice_field_id',$notice_field_id->id)
                ->update(['value'=>'']);
            }else{
                $wrk_order_fields=\App\Models\Cyo_WorkOrder_fields::where('workorder_id',$parent_work_order)
                ->where('notice_id',$parent_notice_id->notice_id)
                ->where('notice_field_id',$notice_field_id->id)
                ->update(['value'=>$last_date_on_the_job_val]);
            }
        }
      }


        $notice = \App\Models\Notice::select('name', 'id', 'type', 'is_claim_of_lien','master_notice_id')->where('id', $notice_id)->get()->toArray();
        $data['notice'] = $notice[0];
        $data['cities'] = \App\Models\City::select()->get();
        $data['countries'] = \App\Models\Country::select()->get();
        $data['states'] = \App\Models\State::select()->orderBy('name', 'asc')->get();
        $data['categories'] = \App\Models\Category::select()->orderBy('name', 'ASC')->get();
        $data['project_roles'] = \App\Models\ProjectRoles::select()->orderBy('name', 'ASC')->get();
        $data['duplicate'] = $request->duplicate;
        $data['count'] = \App\Models\Cyo_WorkOrder_Recipient::select(DB::raw('COUNT(*) AS total'), 'categories.name')->groupBy('cyo__work_order__recipients.category_id')->join('categories', 'categories.id', '=', 'cyo__work_order__recipients.category_id')->where('work_order_id', $id)->get();
        $data['id'] = $id;
        $data['officers_directors'] = \App\Models\CustomerAgent::where('customer_id', '=', Auth::user()->customer->id)
                ->get();
        $data['projectTypes'] = \App\Models\ProjectType::select()->get();
        $data['notice_id'] = $notice_id;

        $result = \App\Models\Cyo_WorkOrders::select('state_id')->join('notice_fields', 'notice_fields.notice_id', '=', 'cyo__work_orders.notice_id')->where('cyo__work_orders.id', $id)->limit(1)->get()->toArray();
        $data['names'] = \App\Models\Contact::select('contacts.id', 'contacts.company_name')
//                ->where('contact_details.type', '=', 0)
                ->where('contacts.customer_id', '=', Auth::user()->customer->id)
//                ->Join('contact_details', 'contact_details.contact_id', '=', 'contacts.id')
                ->orderBy('contacts.id', 'DESC')
                ->get();
        $data['recipients'] = \App\Models\Cyo_WorkOrder_Recipient::select('cyo__work_order__recipients.id', 'work_order_id', 'category_id', 'name', 'contact', 'mobile', 'cyo__work_order__recipients.address', 'city_id', 'state_id', 'zip', 'fax', 'cyo__work_order__recipients.created_at', 'cyo__work_order__recipients.updated_at', \DB::raw('cyo__usps_addresses.address as cyo_usps_address'), \DB::raw('cyo__usps_addresses.city as usps_city'), \DB::raw('cyo__usps_addresses.state as usps_state'), 'zipcode', 'zipcode_add_on', 'dpb', 'check_digit', 'comment')->leftJoin('cyo__usps_addresses', 'cyo__usps_addresses.recipient_id', '=', 'cyo__work_order__recipients.id')->where('work_order_id', $id)->get();
        $data['rush_hour'] = \App\Models\Cyo_WorkOrders::select('rush_hour_charges','rush_hour_amount')->where('id',$id)->get();
        $verified_address_count = \App\Models\Cyo_WorkOrder_Recipient::select(\DB::raw('COUNT(cyo__usps_addresses.recipient_id) as verified_address_count'))->leftJoin('cyo__usps_addresses', 'cyo__usps_addresses.recipient_id', '=', 'cyo__work_order__recipients.id')->where('work_order_id', $id)->get();
        $data['verified_address_count'] = ($verified_address_count) ? $verified_address_count[0]['verified_address_count'] : 0;

        $data['service_types'] = \App\Models\StampsServices::where(['status' => '1'])->get(['description', 'abbr'])->toArray();
        $admin_email = \App\User::leftjoin('role_users', 'role_users.user_id', '=', 'users.id')
                ->where('role_users.role_id', '=', 2)
                ->select('users.email')
                ->first();
        $account_manager_email = \App\Models\Customer::leftjoin('users', 'users.id', '=', 'customers.account_manager_id')
                ->select('users.email')
                ->find(Auth::user()->customer->id);
        $data['attachment'] = \App\Models\Cyo_WorkOrder_Attachment::where('work_order_id', '=', $id)
                ->get();
        /* if (isset($account_manager_email) && !empty($account_manager_email)) {
          $data['note_emails'] = [
          $admin_email->email,
          $account_manager_email->email
          ];
          } else {
          $data['note_emails'] = [
          $admin_email->email
          ];
          } */
        $data['note_emails'] = get_user_notice_email(Auth::user()->customer->id);
        $data['notes'] = \App\Models\Cyo_WorkOrder_Notes::leftjoin('users', 'users.id', '=', 'cyo__work_order__notes.user_id')->where('customer_id', '=', Auth::user()->customer->id)
                ->where('work_order_id', '=', $id)
                ->where('visibility', '=', 0)
                ->orderBy('cyo__work_order__notes.id', 'DESC')
                ->get();
        $data['corrections'] = \App\Models\Cyo_WorkOrder_Corrections::leftjoin('users', 'users.id', '=', 'cyo__work_order__corrections.user_id')->where('customer_id', '=', Auth::user()->customer->id)
                ->where('cyo__work_order__corrections.work_order_id', '=', $id)
                ->where('cyo__work_order__corrections.visibility', '=', 0)
                ->orderBy('cyo__work_order__corrections.id', 'DESC')
                ->get();

        $work_order_status = \App\Models\Cyo_WorkOrders::where('id', $id)->limit(1)->get()->toArray();
        $data['work_order_status'] = $work_order_status[0]['status'];
         if ($request->continue == 'Continue') {
            return redirect()->to('customer/create-your-own/edit-new/' . $id . '/' . $notice_id . '/'. $is_rescind);
        } else {
            return redirect('customer/create-your-own/work-order-history');
        }
        /*if ($request->continue == 'Continue') {
            if ($notice[0]['type'] == 2) {
                $data['notice_fields_section1'] = \App\Models\NoticeField::select()->join('cyo__work_order_fields', 'cyo__work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('cyo__work_order_fields.workorder_id', $id)->where('cyo__work_order_fields.notice_id', $notice_id)->where('notice_fields.section', 1)->orderBy('sort_order', 'asc')->get();

                $data['notice_fields_section2'] = \App\Models\NoticeField::select()->join('cyo__work_order_fields', 'cyo__work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('cyo__work_order_fields.notice_id', $notice_id)->where('cyo__work_order_fields.workorder_id', $id)->where('notice_fields.section', 2)->orderBy('sort_order', 'asc')->get();
                $data['tab'] = 'recipients';

                return view('customer.create_your_own.edit_soft_notices', $data);
            } else {
                $data['notice_fields'] = \App\Models\NoticeField::select()->join('cyo__work_order_fields', 'cyo__work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('cyo__work_order_fields.notice_id', $notice_id)->where('cyo__work_order_fields.workorder_id', $id)->orderBy('sort_order', 'asc')->get();

                $data['tab'] = 'recipients';
                return view('customer.create_your_own.edit', $data);
            }
        } else {
            return redirect('customer/create-your-own/work-order-history');
        }*/
    }


    public function viewWorkOrder($id) {
        $work_order = \App\Models\Cyo_WorkOrders::find($id);
        $data['is_rescind'] = $work_order->is_rescind;
        $notice_id = $work_order->notice_id;
        $notice = \App\Models\Notice::select('name', 'id', 'type', 'is_claim_of_lien','master_notice_id')->where('id', $notice_id)->get()->toArray();

        $data['notice'] = $notice[0];
        $data['work_order'] = $work_order;

        $data['recipients'] = \App\Models\Cyo_WorkOrder_Recipient::select()->where('work_order_id', $id)->get();

        $data['count'] = \App\Models\Cyo_WorkOrder_Recipient::select(DB::raw('COUNT(*) AS total'), 'categories.name')->groupBy('cyo__work_order__recipients.category_id')->join('categories', 'categories.id', '=', 'cyo__work_order__recipients.category_id')->where('work_order_id', $id)->get();
        $data['id'] = $id;

        $data['notice_id'] = $notice_id;
        $data['customer_details'] = Auth::user()->customer;
//        $result = \App\Models\WorkOrder::select('state_id', 'customer_id')->join('notice_fields', 'notice_fields.id', '=', 'work_orders.notice_id')->where('work_orders.id', $id)->get()->toArray();
        $result = \App\Models\Cyo_WorkOrders::select('state_id', 'customer_id')->join('notice_fields', 'notice_fields.notice_id', '=', 'cyo__work_orders.notice_id')->where('cyo__work_orders.id', $id)->limit(1)->get()->toArray();
        $data['customer_id'] = $result[0]['customer_id'];
        //$data['attachment'] = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $id)
        // ->get();
        //dd($data['attachment']);


        $data['attachment'] = \App\Models\Cyo_WorkOrder_Attachment::where('work_order_id', '=', $id)
                ->where('visibility', '=', 0)
                ->get();

        $account_manager_email = \App\Models\SecondaryDocument::getAccountManagerEmail(Auth::user()->customer->id);
        $admin_email = \App\Models\SecondaryDocument::getAdminEmail();
        /* if (isset($account_manager_email) && !empty($account_manager_email)) {
          $data['note_emails'] = [
          $admin_email->email,
          $account_manager_email->email
          ];
          } else {
          $data['note_emails'] = [
          $admin_email->email
          ];
          } */
        $data['note_emails'] = get_user_notice_email($result[0]['customer_id']);
        $data['notes'] = \App\Models\Cyo_WorkOrder_Notes::leftjoin('users', 'users.id', '=', 'cyo__work_order__notes.user_id')->where('work_order_id', '=', $id)
                ->where('visibility', '=', 0)
                ->orderBy('cyo__work_order__notes.id', 'DESC')
                ->get();
        $data['corrections'] = \App\Models\Cyo_WorkOrder_Corrections::leftjoin('users', 'users.id', '=', 'cyo__work_order__corrections.user_id')->where('work_order_id', '=', $id)
                ->where('cyo__work_order__corrections.visibility', '=', 0)
                ->orderBy('cyo__work_order__corrections.id', 'DESC')
                ->get();

//dd( $data['corrections']);
        if ($notice[0]['type'] == 2) {
            $data['notice_fields_section1'] = \App\Models\NoticeField::select()->join('cyo__work_order_fields', 'cyo__work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('cyo__work_order_fields.workorder_id', $id)->where('cyo__work_order_fields.notice_id', $notice_id)->where('notice_fields.section', 1)->orderBy('sort_order', 'asc')->get();
            if (isset($data['notice_fields_section1']) && !empty($data['notice_fields_section1'])) {
                foreach ($data['notice_fields_section1'] AS $key => $value) {
                    if ($value->name == 'enter_into_agreement' ||
                            $value->name == 'last_date_of_labor_service_furnished' || $value->name == 'job_start_date') {
                        if (isset($value) && $value != NULL && $value != '') {

                            $value->value = date('m-d-Y', strtotime($value->value));
                            // dd($value->value);
//                          $date = \DateTime::createFromFormat("m-d-Y", $value->value);
//                           
//                            $date = $date->format('m-d-Y'); 
//                            $value->value = $date;
                        }
                    } elseif ($value->name == 'county' || $value->name == 'notary_county' || $value->name == 'recorded_county' ||
                            $value->name == 'project_county') {
                        if (isset($value) && $value != NULL) {
                            $county = \App\Models\City::find($value->value);
                            $value->value = $county->county;
                        }
                    } elseif ($value->name == 'notary_state' || $value->name == 'recorded_state') {
                        if (isset($value) && $value != NULL) {
                            $state = \App\Models\State::find($value->value);
                            $value->value = $county->name;
                        }
                    } elseif ($value->name == 'your_role') {
                        if (isset($value) && $value != NULL) {
                            $category = \App\Models\ProjectRoles::find($value->value);//dd($category);
                            if(!empty($category)){
                            $value->value = $category->name;
                          }
                        }
                    } elseif ($value->name == 'project_type') {
                        if (isset($value) && $value != NULL) {
                            $project_type = \App\Models\ProjectType::find($value->value);

                            $value->value = $project_type->type;
                        }
                    }
                    if ($value->name == 'parent_work_order') {
                        $parent_work_order = $value->value;
                        $data['parent_work_order'] = $parent_work_order;
                    }
                }
            }

            $data['notice_fields_section2'] = \App\Models\NoticeField::select()->join('cyo__work_order_fields', 'cyo__work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('cyo__work_order_fields.notice_id', $notice_id)->where('cyo__work_order_fields.workorder_id', $id)->where('notice_fields.section', 2)->orderBy('sort_order', 'asc')->get();
            $data['tab'] = 'project';
            $data['extraTitle'] = ' - Create Your Own';
            return view('customer.create_your_own.view_soft_notices', $data);
        } else {
            $data['tab'] = 'project';
            $data['notice_fields'] = \App\Models\NoticeField::select()->join('cyo__work_order_fields', 'cyo__work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('cyo__work_order_fields.notice_id', $notice_id)->where('cyo__work_order_fields.workorder_id', $id)->orderBy('sort_order', 'asc')->get();

            if (isset($data['notice_fields']) && !empty($data['notice_fields'])) {
                foreach ($data['notice_fields'] AS $key => $value) {
                    if ($value->name == 'enter_into_agreement' ||
                            $value->name == 'last_date_of_labor_service_furnished' || $value->name == 'job_start_date') {
                        if (isset($value) && $value != NULL && $value != '') {
                            $value->value = date('m-d-Y', strtotime($value->value));
//                          $date = \DateTime::createFromFormat("m-d-Y", $value->value);
//                           
//                            $date = $date->format('m-d-Y'); 
//                            $value->value = $date;
                        }
                    } elseif ($value->name == 'county' || $value->name == 'notary_county' || $value->name == 'recorded_county' ||
                            $value->name == 'project_county') {
                        if (isset($value) && $value != NULL && $value != '') {
                            $county = \App\Models\City::find($value->value);
                            if (isset($county) && !empty($county)) {
                                $value->value = $county->county;
                            }
                        }
                    } elseif ($value->name == 'notary_state' || $value->name == 'recorded_state') {
                        if (isset($value) && $value != NULL && $value != '') {
                            $state = \App\Models\State::find($value->value);
                            if (isset($state) && !empty($state)) {
                                $value->value = $state->name;
                            }
                        }
                    } elseif ($value->name == 'your_role') {
                        if (isset($value) && $value != NULL) {
                            $category = \App\Models\ProjectRoles::find($value->value);
                            $value->value = $category->name;
                        }
                    } elseif ($value->name == 'project_type') {
                        if (isset($value) && $value != NULL) {
                            $project_type = \App\Models\ProjectType::find($value->value);
                            $value->value = $project_type->type;
                        }
                    }

                    if ($value->name == 'parent_work_order') {
                        $parent_work_order = $value->value;
                        $data['parent_work_order'] = $parent_work_order;
                    }
                }
            }
            $data['extraTitle'] = ' - Create Your Own';
            return view('customer.create_your_own.view_hard_notices', $data);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function create_own_work_order(Request $request, $id, $notice_id) {
        $fields = array_merge($request->except(['_token', 'continue', 'type', 'notice_id', 'edit_soft_notices_attachment', 'file_name', 'doc_input', 'hidden_city_val', 'duplicate']));


        $fieldsData = [];
        $values = [];
        $i = 0;
        foreach ($fields as $key => $value) {
            $field = explode("_", $key);
            $fieldsData[$i] = $field['0'];
            $values[$i] = $value;
            $i++;
        }

        $customerId = Auth::user()->customer->id;
        $userId = Auth::user()->id;
        $account_manager_id = Auth::user()->customer->account_manager_id;
        if ($request->duplicate == 'duplicate') {

            if ($request->continue == 'Continue') {
                $result1 = \App\Models\Cyo_WorkOrders::create([
                            'notice_id' => $request->notice_id,
                            'customer_id' => $customerId,
                            'order_no' => str_random(5),
                            'user_id' => $userId,
                            'status' => '1',
                            'account_manager_id' => $account_manager_id,
                ]);
            } else {
                $result1 = \App\Models\Cyo_WorkOrders::create([
                            'notice_id' => $request->notice_id,
                            'customer_id' => $customerId,
                            'order_no' => str_random(5),
                            'user_id' => $userId,
                            'status' => '0',
                            'account_manager_id' => $account_manager_id,
                ]);
            }
            $recipients = \App\Models\Cyo_WorkOrder_Recipient::select('id')->where('work_order_id', $id)->get()->toArray();
            foreach ($recipients as $key => $value) {
                # code...
                //dd('ji');
                $recipient = \App\Models\Cyo_WorkOrder_Recipient::find($value['id']);
                \App\Models\Cyo_WorkOrder_Recipient::create([
                    'work_order_id' => $result1->id,
                    'category_id' => $recipient->category_id,
                    'name' => $recipient->name,
                    'contact' => $recipient->contact,
                    'mobile' => $recipient->mobile,
                    'address' => $recipient->address,
                    'city_id' => $recipient->city_id,
                    'state_id' => $recipient->state_id,
                    'zip' => $recipient->zip,
                    'email' => $recipient->email,
                    'attn' => $recipient->attn
                ]);
            }
            $data['recipients'] = \App\Models\Cyo_WorkOrder_Recipient::select()->where('work_order_id', $id)->get();
            /* if ($result1) {
              Session::flash('success', 'Record Created Successfully');
              } else {
              Session::flash('success', 'Problem in Record saving');
              } */
        } else {
            if ($request->continue == 'Continue') {
                $result1 = \App\Models\Cyo_WorkOrders::find($id);
                $result1->notice_id = $request->notice_id;
                $result1->customer_id = $customerId;
                $result1->order_no = str_random(5);
                $result1->user_id = $userId;
                $result1->status = '1';
                $result1->save();
            } else {
                $result1 = \App\Models\Cyo_WorkOrders::find($id);
                $result1->notice_id = $request->notice_id;
                $result1->customer_id = $customerId;
                $result1->order_no = str_random(5);
                $result1->user_id = $userId;
                $result1->status = '0';
                $result1->save();
            }
            $data['recipients'] = \App\Models\Cyo_WorkOrder_Recipient::select()->where('work_order_id', $result1->id)->get();
        }
        if ($result1) {

            $attachment = explode(',|,', trim($request->edit_soft_notices_attachment[0],'|,'));
            if (isset($attachment) && !empty($attachment)) {
                foreach ($attachment AS $k => $v) {
                  $v = trim($v,'|,');
                    if ($v != '') {
                        $each_attachment = explode(',', $v);
                        $work_order_attachment_obj = new \App\Models\Cyo_WorkOrder_Attachment();
                        $work_order_attachment_obj->work_order_id = $result1->id;
                        $work_order_attachment_obj->type = $each_attachment[0];
                        $work_order_attachment_obj->title = $each_attachment[1];
                        $work_order_attachment_obj->file_name = $each_attachment[2];
                        $work_order_attachment_obj->original_file_name = $each_attachment[3];
                        $work_order_attachment_obj->save();
                    }
                }
            }
        }
        $work_order_id = $result1->id;

        $result2 = \App\Models\Cyo_WorkOrder_fields::select()->where('workorder_id', $work_order_id)->delete();

        foreach ($fieldsData as $key => $value) {

            if ($values[$key] == NULL) {
                $result2 = \App\Models\Cyo_WorkOrder_fields::create([
                            'workorder_id' => $work_order_id,
                            'notice_id' => $request->notice_id,
                            'notice_field_id' => $value,
                            'value' => '',
                ]);
            } else
                $result2 = \App\Models\Cyo_WorkOrder_fields::create([
                            'workorder_id' => $work_order_id,
                            'notice_id' => $request->notice_id,
                            'notice_field_id' => $value,
                            'value' => nl2br($values[$key]),
                ]);
        }
        /* if ($result1 && $result3) {
          Session::flash('success', 'New Record Successfully');
          } else {
          Session::flash('success', 'Problem in Record saving');
          } */

        $notice = \App\Models\Notice::select('name', 'id', 'type', 'is_claim_of_lien','master_notice_id')->where('id', $notice_id)->get()->toArray();
        $data['notice'] = $notice[0];
        $data['cities'] = \App\Models\City::select()->get();
        $data['countries'] = \App\Models\Country::select()->get();
        $data['states'] = \App\Models\State::select()->orderBy('name', 'asc')->get();
        $data['categories'] = \App\Models\Category::select()->orderBy('name', 'ASC')->get();
        $data['project_roles'] = \App\Models\ProjectRoles::select()->orderBy('name', 'ASC')->get();
        $data['duplicate'] = $request->duplicate;
        $data['count'] = \App\Models\Cyo_WorkOrder_Recipient::select(DB::raw('COUNT(*) AS total'), 'categories.name')->groupBy('cyo__work_order__recipients.category_id')->join('categories', 'categories.id', '=', 'cyo__work_order__recipients.category_id')->where('work_order_id', $id)->get();
        $data['id'] = $id;
        $data['officers_directors'] = \App\Models\CustomerAgent::where('customer_id', '=', Auth::user()->customer->id)
                ->get();
        $data['projectTypes'] = \App\Models\ProjectType::select()->get();
        $data['notice_id'] = $notice_id;
//        $result = \App\Models\Cyo_WorkOrders::select('state_id')->join('notice_fields', 'notice_fields.id', '=', 'cyo__work_orders.notice_id')->where('cyo__work_orders.id', $id)->get()->toArray();
        $result = \App\Models\Cyo_WorkOrders::select('state_id')->join('notice_fields', 'notice_fields.notice_id', '=', 'cyo__work_orders.notice_id')->where('cyo__work_orders.id', $id)->limit(1)->get()->toArray();
        $data['names'] = \App\Models\Contact::select('contacts.id', 'contacts.company_name')
//                ->where('contact_details.type', '=', 0)
                ->where('contacts.customer_id', '=', Auth::user()->customer->id)
//                ->Join('contact_details', 'contact_details.contact_id', '=', 'contacts.id')
                ->orderBy('contacts.id', 'DESC')
                ->get();

        $data['recipients'] = \App\Models\Cyo_WorkOrder_Recipient::select('cyo__work_order__recipients.id', 'work_order_id', 'category_id', 'name', 'contact', 'mobile', 'cyo__work_order__recipients.address', 'city_id', 'state_id', 'zip', 'fax', 'cyo__work_order__recipients.created_at', 'cyo__work_order__recipients.updated_at', \DB::raw('cyo__usps_addresses.address as cyo_usps_address'), \DB::raw('cyo__usps_addresses.city as usps_city'), \DB::raw('cyo__usps_addresses.state as usps_state'), 'zipcode', 'zipcode_add_on', 'dpb', 'check_digit', 'comment')->leftJoin('cyo__usps_addresses', 'cyo__usps_addresses.recipient_id', '=', 'cyo__work_order__recipients.id')->where('work_order_id', $id)->get();
        $data['rush_hour'] = \App\Models\Cyo_WorkOrders::select('rush_hour_charges','rush_hour_amount')->where('id',$id)->get();

        $data['extraTitle'] = ' - Create Your Own';
        if ($notice[0]['type'] == 2) {
            $data['notice_fields_section1'] = \App\Models\NoticeField::select()->join('cyo__work_order_fields', 'cyo__work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('cyo__work_order_fields.workorder_id', $id)->where('cyo__work_order_fields.notice_id', $notice_id)->where('notice_fields.section', 1)->orderBy('sort_order', 'asc')->get();

            $data['notice_fields_section2'] = \App\Models\NoticeField::select()->join('cyo__work_order_fields', 'cyo__work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('cyo__work_order_fields.notice_id', $notice_id)->where('cyo__work_order_fields.workorder_id', $id)->where('notice_fields.section', 2)->orderBy('sort_order', 'asc')->get();
            $data['tab'] = 'recipients';

            return view('customer.create_your_own.soft_notice_recipients', $data);
        } else {
            $data['notice_fields'] = \App\Models\NoticeField::select()->join('cyo__work_order_fields', 'cyo__work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('cyo__work_order_fields.notice_id', $notice_id)->where('cyo__work_order_fields.workorder_id', $id)->orderBy('sort_order', 'asc')->get();

            $data['tab'] = 'recipients';
            return view('customer.create_your_own.hard_notice_recipients', $data);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cancel($id) {

        $result = \App\Models\Cyo_WorkOrders::find($id);
        
        $updateData = ['status' => 7, 'cancelled_at' => date("Y-m-d H:i:s")];
        //if status is draft/request & updated by customer then will consider as no-cancellation-charge on invoice
        if(!empty($result) && ( $result->status == 0 ||  $result->status == 1) ) {
            $updateData += ['is_billable' => 0];
        }
        $result->update($updateData);
        
        if ($result) {
            \Artisan::call("GeneratingCyoQuickbooksInvoice:generatingCyoInvoice");
            Session::flash('success', 'Your work # ' . $id . 'Record Cancel Successfully.');
        } else {
            Session::flash('success', 'Your work # ' . $id . 'Problem in Record saving');
        }
        return redirect()->back();
    }

    public function add_note(Request $request) {
        $rules = ['note' => 'required',
            'email' => 'required|email',
        ];
        $validator = \Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return response()->json(['result'=>'errors','message' => $validator->errors()->all()]);
        }
        $request->request->set('customer_id', Auth::user()->customer->id);
        $result = \App\Models\Cyo_WorkOrder_Notes::create($request->all());
        //Mail::to($request->email)->send(new AddNote());
        Mail::to($request->email)->send(new WorkOrderNote($result));

        return response()->json(['result'=>'success','message' => 'Note submitted successfully.']);

        //return redirect::to('customer/create-your-own/work-order-history')->with('success', 'Note submitted successfully.');
        // return redirect('customer/view-work-order');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, $notice_id, $work_order_id) {
        $usps_address = \App\Models\Cyo_UspsAddress::where('recipient_id', $id)->delete();
        $data['label_generated_count'] = 0;
        $result = \App\Models\Cyo_WorkOrder_Recipient::where('id', $id)->delete();
        if ($result) {
            Session::flash('success', 'Your work # ' . $work_order_id . ' Record delete Successfully.');
        } else {
            Session::flash('success', 'Your work # ' . $work_order_id . ' Problem in Record saving');
        }

        $notice = \App\Models\Notice::select('name', 'id', 'type', 'is_claim_of_lien','master_notice_id')->where('id', $notice_id)->get()->toArray();
        $data['notice'] = $notice[0];
        $data['cities'] = \App\Models\City::select()->get();
        $data['countries'] = \App\Models\Country::select()->get();
        $data['states'] = \App\Models\State::select()->orderBy('name', 'asc')->get();
        // $data['recipients'] = \App\Models\Cyo_WorkOrder_Recipient::select()->where('work_order_id', $work_order_id)->get();
        $data['recipients'] = \App\Models\Cyo_WorkOrder_Recipient::select('cyo__work_order__recipients.id', 'work_order_id', 'category_id', 'name', 'contact', 'mobile', 'cyo__work_order__recipients.address', 'city_id', 'state_id', 'zip', 'fax', 'cyo__work_order__recipients.created_at', 'cyo__work_order__recipients.updated_at', \DB::raw('cyo__usps_addresses.address as cyo_usps_address'), \DB::raw('cyo__usps_addresses.city as usps_city'), \DB::raw('cyo__usps_addresses.state as usps_state'), 'zipcode', 'zipcode_add_on', 'dpb', 'check_digit', 'comment')->leftJoin('cyo__usps_addresses', 'cyo__usps_addresses.recipient_id', '=', 'cyo__work_order__recipients.id')->where('work_order_id', $work_order_id)->get();
        $data['rush_hour'] = \App\Models\Cyo_WorkOrders::select('rush_hour_charges','rush_hour_amount')->where('id',$work_order_id)->get();
        $verified_address_count = \App\Models\Cyo_WorkOrder_Recipient::select(\DB::raw('COUNT(cyo__usps_addresses.recipient_id) as verified_address_count'))->leftJoin('cyo__usps_addresses', 'cyo__usps_addresses.recipient_id', '=', 'cyo__work_order__recipients.id')->where('work_order_id', $work_order_id)->get();
        $data['verified_address_count'] = ($verified_address_count) ? $verified_address_count[0]['verified_address_count'] : 0;

        $data['categories'] = \App\Models\Category::select()->orderBy('name', 'ASC')->get();
        $data['project_roles'] = \App\Models\ProjectRoles::select()->orderBy('name', 'ASC')->get();
        $data['count'] = \App\Models\Cyo_WorkOrder_Recipient::select(DB::raw('COUNT(*) AS total'), 'categories.name')->groupBy('cyo__work_order__recipients.category_id')->join('categories', 'categories.id', '=', 'cyo__work_order__recipients.category_id')->where('work_order_id', $work_order_id)->get();
        $data['id'] = $work_order_id;
        $data['notice_id'] = $notice_id;
        $data['officers_directors'] = \App\Models\CustomerAgent::where('customer_id', '=', Auth::user()->customer->id)
                ->get();
//                $result = \App\Models\Cyo_WorkOrders::select('state_id')->join('notice_fields','notice_fields.id','=','cyo__work_orders.notice_id')->where('cyo__work_orders.id',$work_order_id)->get()->toArray();
        $result = \App\Models\Cyo_WorkOrders::select('state_id')->join('notice_fields', 'notice_fields.notice_id', '=', 'cyo__work_orders.notice_id')->where('cyo__work_orders.id', $work_order_id)->limit(1)->get()->toArray();
        $data['names'] = \App\Models\Contact::select('contacts.id', 'contacts.company_name')
//                ->where('contact_details.type', '=', 0)
                ->where('contacts.customer_id', '=', Auth::user()->customer->id)
//                ->Join('contact_details', 'contact_details.contact_id', '=', 'contacts.id')
                ->orderBy('contacts.id', 'DESC')
                ->get();
        $data['projectTypes'] = \App\Models\ProjectType::select()->get();
        $work_order_status = \App\Models\Cyo_WorkOrders::find($work_order_id);
        if ($work_order_status->status == '0') {
            $data['status'] = 'draft';
        } else {
            $data['status'] = 'request';
        }
        $data['work_order_status'] = $work_order_status->status;
        $data['service_types'] = \App\Models\StampsServices::where(['status' => '1'])->get(['description', 'abbr', 'id'])->toArray();
        $data['package_types'] = \App\Models\StampsAddOns::join('stamps_service_package_addon', 'stamps_service_package_addon.add_on', '=', 'stamps_add_ons.id')
                        ->join('stamps_service_type', 'stamps_service_type.id', '=', 'stamps_service_package_addon.service_type')
                        ->join('stamps_packages', 'stamps_packages.id', '=', 'stamps_service_package_addon.package')
                        ->where(['stamps_packages.status' => '1'])
                        ->get([\DB::raw('DISTINCT(stamps_packages.name)'), 'stamps_packages.id'])->toArray();

        if ($data['recipients']) {
            for ($cnt = 0; $cnt < count($data['recipients']); $cnt++) {
                $labels = \App\Models\Cyo_StampsLabel::leftJoin('stamps_service_type', 'stamps_service_type.id', 'cyo__stamps_labels.service_type')
                        ->leftJoin('stamps_packages', 'stamps_packages.id', 'cyo__stamps_labels.package_type')
                        ->where(['recipient_id' => $data['recipients'][$cnt]['id']])
                        ->first(['cyo__stamps_labels.*']);
                if (!empty($labels) && $labels['generated_label'] == 'yes') {
                    $data['label_generated_count'] ++;
                    $data['recipients'][$cnt]['label_generated'] = 'yes';
                } else {
                    $data['recipients'][$cnt]['label_generated'] = 'no';
                }
                $data['recipients'][$cnt]['recipient_shipping_labels'] = $labels;
                $data['recipients'][$cnt]['add_ons'] = \App\Models\StampsAddOns::join('stamps_service_package_addon', 'stamps_service_package_addon.add_on', '=', 'stamps_add_ons.id')
                                ->join('stamps_service_type', 'stamps_service_type.id', '=', 'stamps_service_package_addon.service_type')
                                ->join('stamps_packages', 'stamps_packages.id', '=', 'stamps_service_package_addon.package')
                                ->where('stamps_service_type.id', '=', 1)
                                ->where('stamps_packages.id', '=', 2)
                                ->where('stamps_add_ons.status', '=', '1')
                                // ->where(function($query) use ($labels) {  
                                //     if(isset($labels) && !empty($labels))
                                //     {
                                //         $query->where('stamps_service_type.id','=',$labels['service_type']);
                                //         $query->where('stamps_packages.id','=',$labels['package_type']);
                                //     }	
                                // })
                                ->get([\DB::raw('DISTINCT(stamps_add_ons.id)'), 'stamps_add_ons.description'])->toArray();


                if (!empty($data['recipients'][$cnt]['add_ons'])) {
                    $cnt1 = 1;
                    $temp_add_ons = $data['recipients'][$cnt]['add_ons'];
                    for ($counter = 0; $counter < count($temp_add_ons); $counter++) {
                        $prohibited = \App\Models\StampsAddOnsProhibited::join('stamps_add_ons', 'stamps_add_ons_prohibited.prohibited', '=', 'stamps_add_ons.id')
                                        ->where(['stamps_add_ons_prohibited.add_on' => $temp_add_ons[$counter]['id']])
                                        ->pluck('prohibited')->toArray();

                        $required = \App\Models\StampsAddOnsRequired::join('stamps_add_ons', 'stamps_add_ons_required.required', '=', 'stamps_add_ons.id')
                                        ->where(['stamps_add_ons_required.add_on' => $data['recipients'][$cnt]['add_ons'][$counter]['id']])
                                        ->pluck('required')->toArray();
                        $temp_add_ons[$counter]['prohibited'] = json_encode($prohibited);
                        $temp_add_ons[$counter]['required'] = json_encode($required);
                    }
                    unset($data['recipients'][$cnt]['add_ons']);
                    $data['recipients'][$cnt]['add_ons'] = $temp_add_ons;
                }
            }
        }

        $admin_email = \App\User::leftjoin('role_users', 'role_users.user_id', '=', 'users.id')
                ->where('role_users.role_id', '=', 2)
                ->select('users.email')
                ->first();
        $account_manager_email = \App\Models\Customer::leftjoin('users', 'users.id', '=', 'customers.account_manager_id')
                ->select('users.email')
                ->find(Auth::user()->customer->id);

        /* if (isset($account_manager_email) && !empty($account_manager_email)) {
          $data['note_emails'] = [
          $admin_email->email,
          $account_manager_email->email
          ];
          } else {
          $data['note_emails'] = [
          $admin_email->email
          ];
          } */
        $data['note_emails'] = get_user_notice_email($work_order_status->customer_id);
        $data['notes'] = \App\Models\Cyo_WorkOrder_Notes::leftjoin('users', 'users.id', '=', 'cyo__work_order__notes.user_id')->where('customer_id', '=', Auth::user()->customer->id)
                ->where('work_order_id', '=', $id)
                ->where('visibility', '=', 0)
                ->orderBy('cyo__work_order__notes.id', 'DESC')
                ->get();
        $data['corrections'] = \App\Models\Cyo_WorkOrder_Corrections::leftjoin('users', 'users.id', '=', 'cyo__work_order__corrections.user_id')->where('customer_id', '=', Auth::user()->customer->id)
                ->where('cyo__work_order__corrections.work_order_id', '=', $id)
                ->where('cyo__work_order__corrections.visibility', '=', 0)
                ->orderBy('cyo__work_order__corrections.id', 'DESC')
                ->get();

        if ($notice[0]['type'] == 2) {
            $data['notice_fields_section1'] = \App\Models\NoticeField::select()->join('cyo__work_order_fields', 'cyo__work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('cyo__work_order_fields.workorder_id', $work_order_id)->where('cyo__work_order_fields.notice_id', $notice_id)->where('notice_fields.section', 1)->orderBy('sort_order', 'asc')->get();

            $data['notice_fields_section2'] = \App\Models\NoticeField::select()->join('cyo__work_order_fields', 'cyo__work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('cyo__work_order_fields.notice_id', $notice_id)->where('cyo__work_order_fields.workorder_id', $work_order_id)->where('notice_fields.section', 2)->orderBy('sort_order', 'asc')->get();
            $data['tab'] = 'recipients';
            //return view('customer.create_your_own.edit_soft_notices', $data);
        } else {
            $data['notice_fields'] = \App\Models\NoticeField::select()->join('cyo__work_order_fields', 'cyo__work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('cyo__work_order_fields.notice_id', $notice_id)->where('cyo__work_order_fields.workorder_id', $work_order_id)->orderBy('sort_order', 'asc')->get();
            $data['tab'] = 'recipients';
           // return view('customer.create_your_own.edit', $data);
        }
        return redirect()->back();
    }

    public function getWorkOrdeNumber(Request $request) {
 /*       $data = \App\Models\Cyo_WorkOrders::where('customer_id', '=', Auth::user()->customer->id)
        ->where('id','!=',$request->work_order_id)->groupBy('id')->get();*/
        if(Auth::user()->hasRole('customer')){
            $customerId = Auth::user()->customer->id;
        }else{
            $customer_id = \App\Models\Cyo_WorkOrders::find($request->work_order_id);
            $customerId = $customer_id->customer_id;
        }
        $data = \App\Models\Cyo_WorkOrders::select('cyo__work_orders.*', 'notices.master_notice_id')
            ->join('notices', 'cyo__work_orders.notice_id', '=', 'notices.id')
            ->where('customer_id', '=', $customerId)
            ->where('cyo__work_orders.id','!=',$request->work_order_id)
            ->where('cyo__work_orders.status', 6)
            ->where('master_notice_id', config('constants.MASTER_NOTICE')['NTO']['ID'])
            ->groupBy('cyo__work_orders.id')
            ->orderBy('id', 'desc')
            ->get();
        
        return response()->json($data);
    }

    public function getWorkOrdeNumberV1(Request $request) {
        $query = $request->get('term', '');

        if(Auth::user()->hasRole('customer')){
            $customerId = Auth::user()->customer->id;
        }else{
            $customer_id = \App\Models\Cyo_WorkOrders::find($request->work_order_id);
            $customerId = $customer_id->customer_id;
        }
        $data = \App\Models\Cyo_WorkOrders::join('notices', 'cyo__work_orders.notice_id', '=', 'notices.id')
                   ->where('customer_id', '=', $customerId)
                   ->where('cyo__work_orders.id','!=',$request->work_order_id)
                   ->where('cyo__work_orders.status', 6)
                   ->where('master_notice_id', config('constants.MASTER_NOTICE')['NTO']['ID'])
                   ->where('cyo__work_orders.id', 'LIKE', '%' . $query . '%')
                   ->groupBy('cyo__work_orders.id')
                   ->orderBy('cyo__work_orders.id', 'desc')
                   ->pluck('cyo__work_orders.id')->toArray();
               
        return $data;
    }

    public function getParentOrderId(Request $request) {
        $ParentWorkOrders = \App\Models\Cyo_WorkOrders::select('id')->get();
        return $ParentWorkOrders;
    }

    /**
     * Function is used to get work order details
     */
    public function getWorkOrderDetails(Request $request) {
        $work_order_no = $request->work_order_no;

        $result = \App\Models\Cyo_WorkOrder_fields::leftjoin('notice_fields', 'notice_fields.id', '=', 'cyo__work_order_fields.notice_field_id')
                ->where('cyo__work_order_fields.workorder_id', '=', $work_order_no)
                ->get();
        foreach ($result AS $k => $val) {
            if ($val->name == 'county' && $val->value != "") {
                //$county_name = \App\Models\City::select(['id', \DB::raw('concat(county,", ",name) as name')])->find($val->value);
                $county_name = \App\Models\City::select(['id', \DB::raw('county as name')])->find($val->value);
                $val->county_name = $county_name->name;
            }
        }

        if (Auth::user()->hasRole('account-manager') || Auth::user()->hasRole('admin')) {
            $parent_attachment = \App\Models\Cyo_WorkOrder_Attachment::where('work_order_id', '=', $work_order_no)
                    ->select('type', 'title', 'original_file_name', 'created_at', 'file_name','id')
                    ->get();
        } else {
            $parent_attachment = \App\Models\Cyo_WorkOrder_Attachment::where('work_order_id', '=', $work_order_no)
                    ->select('type', 'title', 'original_file_name', 'created_at', 'file_name','id')
                    //  ->where('visibility', '=', 0)
                    ->get();
        }
       //dd($parent_attachment); 
        $owner_recipient = \App\Models\Cyo_WorkOrder_Recipient::where('work_order_id', '=', $work_order_no)->whereIn('category_id', array('1','22'))
                ->get();
        $attachment = [];
        if (isset($parent_attachment) && count($parent_attachment) > 0) {
            foreach ($parent_attachment As $k => $v) {
                $extension = explode('.', $v->original_file_name);

                if (file_exists(public_path() . '/attachment/cyo_work_order_document/' . $v->file_name)) {
                    $attachment[$k]['db_arr'] = [
                        $v->type,
                        $v->title,
                        $v->file_name,
                        $v->original_file_name,
                        $v->id,
                    ];
                }

                if ($v->type == 1) {
                    $v->type = 'Bond';
                } elseif ($v->type == 2) {
                    $v->type = 'Notice to Commencement';
                } elseif ($v->type == 3) {
                    $v->type = 'Permit';
                } elseif ($v->type == 4) {
                    $v->type = 'Contract';
                } elseif ($v->type == 5) {
                    $v->type = 'Invoice';
                } elseif ($v->type == 6) {
                    $v->type = 'Final Release';
                } elseif ($v->type == 7) {
                    $v->type = 'Partial Release';
                } elseif ($v->type == 8) {
                    $v->type = 'Misc Document';
                } elseif ($v->type == 9) {
                    $v->type = 'Signed Document';
                } elseif ($v->type == 10) {
                    $v->type = 'Folio';
                } elseif ($v->type == 11) {
                    $v->type = 'COL Signed';
                } elseif ($v->type == 12) {
                    $v->type = 'Recorded COL/SCOL';
                } elseif ($v->type == 13) {
                    $v->type = 'Pending Signature';
                }
                if (!file_exists(public_path() . '/attachment/cyo_work_order_document/parent_document')) {

                    $dir = mkdir(public_path() . '/attachment/cyo_work_order_document/parent_document', 0777, true);
                } else {
                    $dir = public_path() . '/attachment/cyo_work_order_document/parent_document/';
                }
                if (file_exists(public_path() . '/attachment/cyo_work_order_document/' . $v->file_name) && !empty($v->file_name)) {
                    \File::copy(public_path() . '/attachment/cyo_work_order_document/' . $v->file_name, public_path() . '/attachment/cyo_work_order_document/parent_document/' . $v->file_name);
                   } 

                    $created_date = date('d M y h:i A', strtotime($v->created_at));
                    $parent_attachment[$k]['created_date'] = $created_date;
                    if(isset($v->original_file_name) && !empty($v->original_file_name)){
                      $parent_attachment[$k]['extension'] = $extension[1];
                      
                    }else{
                      $parent_attachment[$k]['extension'] = "";
                    }
                    $doc_value = "";
                    if ($v->title != "") {
                    $doc_value = $v->title;
                     }
                    $attachment[$k]['ajax_arr'] = [
                        $v->type,
                        $doc_value,
                        // $request->doc_value,
                        $v->original_file_name,
                        $parent_attachment[$k]['created_date'],
                        $parent_attachment[$k]['extension']
                    ];
                
            }
        }


        return response()->json(['success' => $result, 'parent_attachment' => $attachment, 'owner_recipient' => $owner_recipient]);
    }

    /* store note */

    public function storeNote(Request $request) {
        $rules = ['note' => 'required'
        ];
        if (Auth::user()->hasRole('customer')) {
            $rules = ['email' => 'required', 'note' => 'required',];
        }
        $validator = \Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }
        $workOrder = \App\Models\Cyo_WorkOrders::find($request->work_order);
        if (Auth::user()->hasRole('account-manager')){
           $result = \App\Models\Cyo_WorkOrder_Notes::create([
                    'work_order_id' => $request->work_order,
                    'customer_id' => $workOrder->customer_id,
                    'note' => $request->note,
                    'visibility' => $request->visibility,
                    'email' => ($request->email) ? $request->email : NULL,
                    'user_id' => Auth::user()->id,
          ]);
        }else{
          $result = \App\Models\Cyo_WorkOrder_Notes::create([
                    'work_order_id' => $request->work_order,
                    'customer_id' => $workOrder->customer_id,
                    'note' => $request->note,
                    'email' => ($request->email) ? $request->email : NULL,
                    'user_id' => Auth::user()->id,
        ]);
        }
        
        $notes = \App\Models\Cyo_WorkOrder_Notes::leftjoin('users', 'users.id', '=', 'cyo__work_order__notes.user_id')
                ->where('cyo__work_order__notes.customer_id', '=', $workOrder->customer_id)
                ->select('cyo__work_order__notes.note', 'cyo__work_order__notes.email', 'users.name')
                ->where('cyo__work_order__notes.id', '=', $result->id)
                ->first();
        /* $senderName = \App\Models\Customer::leftjoin('users', 'users.id', '=', 'customers.user_id')
          ->select('users.name')
          ->where('customers.id', '=',Auth::user()->customer->id)
          ->first(); */

        if ($request->email != NULL) {
            Mail::to($request->email)->send(new WorkOrderNote($result));
        }

        if ($result) {
            return response()->json(['success' => 'Note submitted successfully', 'notes' => $notes]);
        }
    }

    /**
     * Function is used to store correction
     */
    public function storeCorrection(Request $request) {
        $rules = ['correction' => 'required'
        ];

        if (Auth::user()->hasRole('customer')) {
            $rules = ['email' => 'required', 'correction' => 'required',];
        }
        $validator = \Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()->all()]);
        }
        $workOrder = WorkOrder::find($request->work_order);
        $result = \App\Models\Cyo_WorkOrder_Corrections::create([
                    'work_order_id' => $request->work_order,
                    'customer_id' =>$workOrder->customer_id,
                    'correction' => $request->correction,
                    'email' => ($request->email) ? $request->email : NULL,
                    'user_id' => Auth::user()->id,
        ]);
        $corrections = \App\Models\Cyo_WorkOrder_Corrections::leftjoin('users', 'users.id', '=', 'cyo__work_order__corrections.user_id')->where('customer_id', '=', $workOrder->customer_id)
                ->select('cyo__work_order__corrections.correction', 'cyo__work_order__corrections.email', 'users.name')
                ->where('cyo__work_order__corrections.id', '=', $result->id)
                ->first();

        // $correction->doc_id = $request->doc_id;
        if ($request->email != NULL) {
            Mail::to($request->email)->send(new \App\Mail\WorkOrderCorrection($result));
        }
        if ($result) {
            return response()->json(['success' => 'Correction submitted successfully', 'corrections' => $corrections]);
        }
    }

    public function proceed($id, $notice_id) {
        $result = \App\Models\Cyo_WorkOrders::find($id);
        $result->update(['status' => 2]);
        $result->save();
        $is_rescind = $result->is_rescind;
        /* if ($result) {
          Session::flash('success', 'Record updated Successfully');
          } else {
          Session::flash('success', 'Problem in Record saving');
          } */
        return redirect('customer/create-your-own/edit/' . $id . '/' . $notice_id . '/' . $is_rescind);
    }

    public function store_attachments(Request $request) {
        $file = $request->file('file');
        if (!empty($file)) {
            $extension = $request->file('file')->getClientOriginalExtension(); // getting excel extension
            $original_file_name = $request->file('file')->getClientOriginalName();
            if (!file_exists(public_path() . '/attachment/cyo_work_order_document')) {
                $dir = mkdir(public_path() . '/attachment/cyo_work_order_document', 0777, true);
            } else {
                $dir = public_path() . '/attachment/cyo_work_order_document/';
            }
            $dir = public_path() . '/attachment/cyo_work_order_document/';
            $filename = uniqid() . '_' . time() . '_' . date('Ymd') . '.' . $extension;
            $request->file('file')->move($dir, $filename);
        } else {
            $filename = "";
            $original_file_name = "";
            $extension = "";
        }
        $doc_val = "";
        if ($request->doc_value != "") {
            $doc_val = $request->doc_value;
        }
        $data = [];
        $data['db_arr'] = [
            $request->document_type,
            $doc_val,
            $filename,
            $original_file_name,
            $extension
        ];
        if ($request->document_type == 1) {
            $request->document_type = 'Bond';
        } elseif ($request->document_type == 2) {
            $request->document_type = 'Notice to Commencement';
        } elseif ($request->document_type == 3) {
            $request->document_type = 'Permit';
        } elseif ($request->document_type == 4) {
            $request->document_type = 'Contract';
        } elseif ($request->document_type == 5) {
            $request->document_type = 'Invoice';
        } elseif ($request->document_type == 6) {
            $request->document_type = 'Final Release';
        } elseif ($request->document_type == 7) {
            $request->document_type = 'Partial Release';
        } elseif ($request->document_type == 8) {
            $request->document_type = 'Misc Document';
        } elseif ($request->document_type == 9) {
            $request->document_type = 'Signed Document';
        } elseif ($request->document_type == 10) {
            $request->document_type = 'Folio';
        }elseif ($request->document_type == 11) {
            $request->document_type = 'COL Signed';
        }elseif ($request->document_type == 12) {
            $request->document_type = 'Recorded COL/SCOL';
        }elseif ($request->document_type == 13) {
            $request->document_type = 'Pending Signature';
        }

        $data['ajax_arr'] = [
            $request->document_type,
            $doc_val,
            $original_file_name,
            date('d M y h:i A'),
            $extension
        ];


        return json_encode($data);
    }

    //edit attachment
    public function edit_attachments(Request $request) {
       //d($request->all());

        $file = $request->file('file');

        if (!empty($file)) {
            $extension = $request->file('file')->getClientOriginalExtension(); // getting excel extension
            $original_file_name = $request->file('file')->getClientOriginalName();
            if (!file_exists(public_path() . '/attachment/cyo_work_order_document')) {
                $dir = mkdir(public_path() . '/attachment/cyo_work_order_document', 0777, true);
            } else {
                $dir = public_path() . '/attachment/cyo_work_order_document/';
            }

            $dir = public_path() . '/attachment/cyo_work_order_document/';
            $filename = uniqid() . '_' . time() . '_' . date('Ymd') . '.' . $extension;
            $request->file('file')->move($dir, $filename);
        } else { 
            if(!empty($request->file_name) && $request->file_original_name!='undefined'  && $request->extension!='undefined'){
            $filename = $request->file_name;
            $original_file_name = $request->file_original_name;
            $extension = $request->extension;
            }else{
            $filename = "";
            $original_file_name = "";
            $extension = "";
            }
        }
        $data = [];
        $doc_val = "";
        if ($request->doc_value != "") {
            $doc_val = $request->doc_value;
        }
        $document_type_id = $request->document_type;
        if (Auth::user()->hasRole('account-manager')) {
            $data['db_arr'] = [
                $request->document_type,
                $doc_val,
                $filename,
                $original_file_name,
                $request->visibility
            ];
        } else {
            $data['db_arr'] = [
                $request->document_type,
                $doc_val,
                $filename,
                $original_file_name
            ];
        }
        if ($request->document_type == 1) {
            $request->document_type = 'Bond';
        } elseif ($request->document_type == 2) {
            $request->document_type = 'Notice to Commencement';
        } elseif ($request->document_type == 3) {
            $request->document_type = 'Permit';
        } elseif ($request->document_type == 4) {
            $request->document_type = 'Contract';
        } elseif ($request->document_type == 5) {
            $request->document_type = 'Invoice';
        } elseif ($request->document_type == 6) {
            $request->document_type = 'Final Release';
        } elseif ($request->document_type == 7) {
            $request->document_type = 'Partial Release';
        } elseif ($request->document_type == 8) {
            $request->document_type = 'Misc Document';
        } elseif ($request->document_type == 9) {
            $request->document_type = 'Signed Document';
        } elseif ($request->document_type == 10) {
            $request->document_type = 'Folio';
        }elseif ($request->document_type == 11) {
            $request->document_type = 'COL Signed';
        }elseif ($request->document_type == 12) {
            $request->document_type = 'Recorded COL/SCOL';
        }elseif ($request->document_type == 13) {
            $request->document_type = 'Pending Signature';
        }

        $data['ajax_arr'] = [
            $request->document_type,
            $doc_val,
            $original_file_name,
            date('d M y h:i A'),
            $extension
        ];
//dd($file_name);
        $remove_file = $request->remove_file;
         if($request->work_order_id == 'null'){
                $request->request->set('work_order_id',NULL);
            }
        if($request->doc_id==0){
            $attachments = new \App\Models\Cyo_WorkOrder_Attachment;
            $attachments->work_order_id = $request->work_order_id;
            $attachments->type = $data['db_arr']['0'];
            $attachments->title =  $data['db_arr']['1'];
            //if (!empty($file)) {
            $attachments->file_name = $data['db_arr']['2'];
            $attachments->original_file_name = $data['db_arr']['3'];
            //}
            if($remove_file==1 && empty($file)){
                $attachments->file_name = "";
                $attachments->original_file_name = "";
            }
            $attachments->visibility = $request->visibility;
            $attachments->save();

            /*$previous_attachments = \App\Models\WorkOrderAttachment::where('work_order_id', '=', $parent_work_order)->get();*/
        }else{
            $attachments = \App\Models\Cyo_WorkOrder_Attachment::find($request->doc_id);
            if(isset($request->work_order_id) && !empty($request->work_order_id)){
                $attachments->work_order_id = $request->work_order_id;
            }
            $attachments->type = $data['db_arr']['0'];
            $attachments->title =  $data['db_arr']['1'];
            if (!empty($file)) {
                $attachments->file_name = $data['db_arr']['2'];
                $attachments->original_file_name = $data['db_arr']['3'];
            }else if(!empty($request->file_name) && $request->file_original_name!='undefined'  && $request->extension!='undefined'){
                $attachments->file_name = $data['db_arr']['2'];
                $attachments->original_file_name = $data['db_arr']['3'];
            }
            if($remove_file==1 && empty($file)){
                $attachments->file_name = "";
                $attachments->original_file_name = "";
            }
            $attachments->visibility = $request->visibility;
//            dd($attachments);

            $attachments->save();
        }
        if(!empty($attachments->original_file_name)){
            $extension = explode('.', $attachments->original_file_name);
            $extension = $extension[1];
        }else{
          $extension = "";
        }
        $data['ajax_arr'] = [
            $request->document_type,
            $doc_val,
           // $attachments->file_name,
            $attachments->original_file_name,
            date('d M y h:i A'),
            $extension
        ];
        if (Auth::user()->hasRole('account-manager')) {
            $data['db_arr'] = [
                $attachments->document_type,
                $doc_val,
                $attachments->file_name,
                $attachments->original_file_name,
                $request->visibility,
                $attachments->id,
                $document_type_id,
                $extension
            ];
        } else {
            $data['db_arr'] = [
                $attachments->document_type,
                $doc_val,
                $attachments->file_name,
                $attachments->original_file_name,
                $attachments->id,
                $document_type_id,
                $extension
            ];
        }

        return json_encode($data);
    }

    /**
     * Print Work Order
     */
    /* public function printWorkOrder($work_order_id) {
      $work_order = \App\Models\Cyo_WorkOrders::find($work_order_id);
      $notice_id = $work_order->notice_id;


      //dd($request->all());

      $WorkOrderFields = \App\Models\Cyo_WorkOrder_fields::select('cyo__work_orders.parent_id', 'cyo__work_orders.created_at', 'cyo__work_orders.user_id', 'cyo__work_orders.customer_id', 'cyo__work_orders.notice_id as notice_id', 'cyo__work_orders.status', 'cyo__work_orders.user_id', 'cyo__work_orders.account_manager_id', 'cyo__work_order_fields.workorder_id', 'cyo__work_order_fields.notice_id', DB::raw("(GROUP_CONCAT(name SEPARATOR '||')) as `field_names`"), DB::raw("(GROUP_CONCAT(value SEPARATOR '||')) as `field_values`"))->join('notice_fields', 'notice_fields.id', '=', 'cyo__work_order_fields.notice_field_id')
      ->join('cyo__work_orders', 'cyo__work_orders.id', '=', 'cyo__work_order_fields.workorder_id')->groupBy('cyo__work_order_fields.workorder_id', 'cyo__work_order_fields.notice_id')
      ->where('cyo__work_orders.id', $work_order_id)
      ->get()->toArray();


      $result = [];
      if (isset($WorkOrderFields) && !empty($WorkOrderFields)) {
      foreach ($WorkOrderFields as $fields_data) {
      $field_names = explode('||', $fields_data['field_names']);
      $field_values = explode('||', $fields_data['field_values']);
      $field_names_values = array_combine($field_names, $field_values);
      $field_names_values['default'] = '';
      $result[] = array_merge($fields_data, $field_names_values);
      }
      }
      //Work Order Recipients
      $recipients = \App\Models\Recipient::where('work_order_id', '=', $work_order_id)
      ->get();
      $data = $result[0];


      $project_address = $data['project_address'];
      $project_state_name = \App\Models\State::find($data['state']);
      $project_state = $project_state_name->name;
      $project_city_name = \App\Models\City::find($data['city']);
      $project_city = $project_city_name->name;


      //Noc ,folio no and bond no
      $document = \App\Models\Cyo_WorkOrder_Attachment::where('work_order_id', '=', $work_order_id)->get();

      $bond = '';
      $noc = '';
      $folio = '';
      foreach ($document As $key => $each_document) {
      if ($each_document->type == 1 || $each_document->type == 2 || $each_document->type == 10) {

      if ($each_document->type == 1) {
      $bond = 'Bond:' . $each_document->title;
      }
      //Noc
      if ($each_document->type == 2) {
      $noc = 'NOC BK:' . $each_document->title;
      }

      //Folio
      if ($each_document->type == 10) {
      $folio = 'Folio No: ' . $each_document->title;
      }
      }
      }
      //Work Order No
      $work_order_no = 'Work Order No:' . $work_order_id;
      //Parent Work Order
      $parent_work_order = (isset($data['parent_work_order']) && !empty($data['parent_work_order'])) ? 'PARENT WORK ORDER:' . $data['parent_work_order'] : ' ';

      //Printing Date
      $printing_date = date('F d,Y');

      //Printing Day
      $printing_day = date('d'); //October 2018
      //Printing Month
      $printing_month = date('F Y');
      //Amount Text
      $amount_text = (isset($data['amount_text']) && !empty($data['amount_text'])) ? $data['amount_text'] : '';
      //Amount
      $amount = (isset($data['amount_due']) && !empty($data['amount_due'])) ? $data['amount_due'] : ' ';
      //Project Name
      $project_name = (isset($data['project_name']) && !empty($data['project_name'])) ? 'Project:' . $data['project_name'] : '';
      //Project Address
      $project_address = $project_address . ' ' . $project_city . ' ' . $project_state;

      //Legal Descritpion
      $legal_description = (isset($data['project_legal_description']) && !empty($data['project_legal_description'])) ? $data['project_legal_description'] : '';

      //Service Labour
      $labour_service = (isset($data['labor_service_furnished']) && !empty($data['labor_service_furnished'])) ? $data['labor_service_furnished'] : '';

      //Last Date of labour service furnished
      $last_date_of_labour_service = (isset($data['last_date_of_labour_service_furnished']) && !empty($data['last_date_of_laour_service'])) ? date("F d,Y", strtotime($data['last_date_of_labour_service_furnished'])) : '';

      //Agreement Date
      $agreement_date = (isset($data['enter_into_agreement']) && !empty($data['enter_into_agreement'])) ? date("F d,Y", strtotime($data['enter_into_agreement'])) : '';

      //Contracted By
      $contracted_by = (isset($data['contracted_by']) && !empty($data['contracted_by'])) ? $data['contracted_by'] : '';

      //Company Name
      $customer_company_name = \App\Models\Customer::find($work_order->customer_id);
      $company_name = $customer_company_name->company_name;
      $company_address = $customer_company_name->mailing_address;
      $company_state_name = \App\Models\State::find($customer_company_name->mailing_state_id);
      $company_state = $company_state_name->name;
      $company_city_name = \App\Models\City::find($customer_company_name->mailing_city_id);
      $company_city = $company_city_name->name;
      $company_city_state_zip = $company_city . ' ' . $company_state . ' ' . $customer_company_name->mailing_zip;

      //Job reference no
      $your_job_reference_no = (isset($data['your_job_reference_no']) && !empty($data['your_job_reference_no'])) ? $data['your_job_reference_no'] : '';

      $logo = public_path() . '/img/logo.png';
      $notice_state = \App\Models\Notice::where('id', '=', $notice_id)
      ->first();
      $notice_state = $notice_state->state_id;
      if ($notice_id == 1) {

      //Intent to lien notice
      $template_content = \App\Models\NoticeTemplate::where('state_id', '=', $notice_state)
      ->where('notice_id', '=', $notice_id)
      ->first();
      $content = $template_content->content;

      if (isset($content) && $content != NULL) {

      $owner_firm_mailing1 = '';
      $owner_name1 = '';
      $owner_address1 = '';
      $owner_city_state_zip1 = '';
      $owner_firm_mailing2 = '';
      $owner_name2 = '';
      $owner_address2 = '';
      $owner_city_state_zip2 = '';
      //owners
      if (isset($recipients) && count($recipients) > 0) {
      foreach ($recipients AS $k => $each_recipient) {
      if ($each_recipient->category_id == 1) {
      if ($k == 0) {
      $owner_firm_mailing1 = 'Firm Mailing';

      $owner_name1 = $each_recipient->name;
      $owner_address1 = $each_recipient->address;

      $state_name = \App\Models\State::find($each_recipient->state_id);
      $state = $state_name->name;
      $city_name = \App\Models\City::find($each_recipient->city_id);
      $city = $city_name->name;

      $owner_city_state_zip1 = $city . ' ' . $state . ' ' . $each_recipient->zip;
      }
      if ($k == 1) {
      $owner_firm_mailing2 = 'Firm Mailing';

      $owner_name2 = $each_recipient->name;
      $owner_address2 = $each_recipient->address;

      $state_name = \App\Models\State::find($each_recipient->state_id);
      $state = $state_name->name;
      $city_name = \App\Models\City::find($each_recipient->city_id);
      $city = $city_name->name;

      $owner_city_state_zip2 = $city . ' ' . $state . ' ' . $each_recipient->zip;
      }
      }
      }
      }

      $arr_variable = ['{{work_order_no}}', '{{parent_work_order}}', '{{printing_date}}', '{{owner_firm_mailing1}}', '{{owner_name1}}', '{{owner_address1}}', '{{owner_city_state_zip1}}',
      '{{owner_firm_mailing2}}', '{{owner_name2}}', '{{owner_address2}}', '{{owner_city_state_zip2}}', '{{company_name}}', '{{amount_text}}', '{{amount}}', '{{project_address}}', '{{project_name}}', '{{folio}}', '{{bond}}',
      '{{noc}}', '{{legal_description}}', '{{service_labour}}', '{{last_date_of_labour_service}}', '{{agreement_date}}', '{{contracted_by}}', '{{company_address}}',
      '{{company_city_state_zip}}', '{{your_job_reference_no}}', '{{state}}'];

      $arr_replace = [$work_order_no, $parent_work_order, $printing_date, $owner_firm_mailing1, $owner_name1, $owner_address1, $owner_city_state_zip1,
      $owner_firm_mailing2, $owner_name2, $owner_address2, $owner_city_state_zip2, $company_name, $amount_text, $amount, $project_address, $project_name, $folio, $bond, $noc,
      $legal_description, $labour_service, $last_date_of_labour_service, $agreement_date, $contracted_by, $company_address, $company_city_state_zip, $your_job_reference_no, $project_state];



      $content = str_replace($arr_variable, $arr_replace, $content);
      }
      } elseif ($notice_id == 9) {

      //NTO notice


      $template_content = \App\Models\NoticeTemplate::where('state_id', '=', $notice_state)
      ->where('notice_id', '=', $notice_id)
      ->first();
      $content = $template_content->content;

      if (isset($content) && $content != NULL) {
      $owner_firm_mailing1 = '';
      $owner_name1 = '';
      $owner_address1 = '';
      $owner_city_state_zip1 = '';
      $owner_firm_mailing2 = '';
      $owner_name2 = '';
      $owner_address2 = '';
      $owner_city_state_zip2 = '';
      $owner_html2 = '';
      if (isset($recipients) && count($recipients) > 0) {
      foreach ($recipients AS $k => $each_recipient) {
      if ($each_recipient->category_id == 1) {
      if ($k == 0) {
      $owner_firm_mailing1 = 'Firm Mailing';

      $owner_name1 = $each_recipient->name;
      $owner_address1 = $each_recipient->address;

      $state_name = \App\Models\State::find($each_recipient->state_id);
      $state = $state_name->name;
      $city_name = \App\Models\City::find($each_recipient->city_id);
      $city = $city_name->name;

      $owner_city_state_zip1 = $city . ' ' . $state . ' ' . $each_recipient->zip;
      }
      if ($k == 1) {
      $owner_firm_mailing2 = 'Firm Mailing';

      $owner_name2 = $each_recipient->name;
      $owner_address2 = $each_recipient->address;

      $state_name = \App\Models\State::find($each_recipient->state_id);
      $state = $state_name->name;
      $city_name = \App\Models\City::find($each_recipient->city_id);
      $city = $city_name->name;

      $owner_city_state_zip2 = $city . ' ' . $state . ' ' . $each_recipient->zip;

      $owner_html2 = '<td style="vertical-align: top;padding: 10px;">TO:</td>
      <td style="padding: 10px;">' . $owner_firm_mailing2 . '
      ' . $owner_name2 . '
      ' . $owner_address2 . '
      ' . $owner_city_state_zip2 . '</td>
      ';
      }
      }
      }
      }

      $arr_variable = ['{{work_order_no}}', '{{printing_date}}', '{{owner_firm_mailing1}}', '{{owner_name1}}', '{{owner_address1}}', '{{owner_city_state_zip1}}', '{{owner_html2}}',
      '{{owner_firm_mailing2}}', '{{owner_name2}}', '{{owner_address2}}', '{{owner_city_state_zip2}}',
      '{{service_labour}}', '{{project_address}}', '{{legal_description}}', '{{folio}}', '{{bond}}', '{{noc}}', '{{contracted_by}}',
      '{{company_name}}', '{{company_address}}', '{{company_city_state_zip}}', '{{logo}}', '{{state}}'];
      $arr_replace = [$work_order_no, $printing_date, $owner_firm_mailing1, $owner_name1, $owner_address1, $owner_city_state_zip1, $owner_html2,
      $owner_firm_mailing2, $owner_name2, $owner_address2, $owner_city_state_zip2,
      $labour_service, $project_address, $legal_description, $folio, $bond, $noc,
      $contracted_by, $company_name, $company_address, $company_city_state_zip, $logo, $project_state];

      $content = str_replace($arr_variable, $arr_replace, $content);
      }
      } elseif ($notice_id == 2) {
      //Non Payment notice


      $template_content = \App\Models\NoticeTemplate::where('state_id', '=', $notice_state)
      ->where('notice_id', '=', $notice_id)
      ->first();
      $content = $template_content->content;

      $owner_firm_mailing1 = '';
      $owner_name1 = '';
      $owner_address1 = '';
      $owner_city_state_zip1 = '';
      if (isset($content) && $content != NULL) {

      $owner_firm_mailing1 = '';
      $owner_name1 = '';
      $owner_address1 = '';
      $owner_city_state_zip1 = '';
      $owner_firm_mailing2 = '';
      $owner_name2 = '';
      $owner_address2 = '';
      $owner_city_state_zip2 = '';

      if (isset($recipients) && count($recipients) > 0) {
      foreach ($recipients AS $k => $each_recipient) {
      if ($each_recipient->category_id == 1) {
      if ($k == 0) {
      $owner_firm_mailing1 = 'Firm Mailing';

      $owner_name1 = $each_recipient->name;
      $owner_address1 = $each_recipient->address;

      $state_name = \App\Models\State::find($each_recipient->state_id);
      $state = $state_name->name;
      $city_name = \App\Models\City::find($each_recipient->city_id);
      $city = $city_name->name;

      $owner_city_state_zip1 = $city . ' ' . $state . ' ' . $each_recipient->zip;
      }
      }
      }
      }

      $arr_variable = ['{{work_order_no}}', '{{parent_work_order}}', '{{printing_date}}', '{{owner_firm_mailing1}}', '{{owner_name1}}', '{{owner_address1}}', '{{owner_city_state_zip1}}',
      '{{service_labour}}', '{{project_address}}', '{{legal_description}}', '{{folio}}', '{{bond}}', '{{noc}}', '{{contracted_by}}', '{{last_date_service_labour}}', '{{amount}}',
      '{{company_name}}', '{{company_address}}', '{{company_city_state_zip}}', '{{state}}'];
      $arr_replace = [$work_order_no, $parent_work_order, $printing_date, $owner_firm_mailing1, $owner_name1, $owner_address1, $owner_city_state_zip1,
      $labour_service, $project_address, $legal_description, $folio, $bond, $noc,
      $contracted_by, $last_date_of_labour_service, $amount, $company_name, $company_address, $company_city_state_zip, $project_state];

      $content = str_replace($arr_variable, $arr_replace, $content);
      }
      } elseif ($notice_id == 3) {
      //Claim of lien notice
      //dd($data);
      $template_content = \App\Models\NoticeTemplate::where('state_id', '=', $notice_state)
      ->where('notice_id', '=', $notice_id)
      ->first();

      $content = $template_content->content;
      if (isset($content) && $content != NULL) {


      if (isset($data['notary_state']) && !empty($data['notary_state'])) {
      $notary_state_name = \App\Models\State::find($data['notary_state']);
      $notary_state = $notary_state_name->name;
      } else {
      $notary_state = '';
      }


      if (isset($data['notary_county']) && !empty($data['notary_county'])) {
      $notary_county_name = \App\Models\Country::find($data['notary_county']);
      $notary_county = $notary_county_name->name;
      } else {
      $notary_county = '';
      }
      //Authorise Signture
      if (isset($data['authorise_agent']) && $data['authorise_agent'] != NULL) {
      $signature = \App\Models\CustomerAgent::find($data['authorise_agent']);
      $office_director = $signature->first_name . ' ' . $signature->last_name;
      } else {
      $office_director = '';
      }

      //Recipients
      $owner_name = '';
      $contractor_name = '';
      $sub_contractor_name = '';
      if (isset($recipients) && count($recipients) > 0) {
      foreach ($recipients AS $k => $each_recipient) {
      if ($each_recipient->category_id == 1) {

      $owner_name = $each_recipient->name;
      }
      if ($each_recipient->category_id == 2) {

      $contractor_name = $each_recipient->name;
      }
      if ($each_recipient->category_id == 3) {

      $sub_contractor_name = $each_recipient->name;
      }
      }
      }
      //Toal value in text
      $total_value_in_text = (isset($data['total_value_in_text']) && ($data['total_value_in_text'] != NULL)) ? $data['total_value_in_text'] : '';
      //Toal value in text
      $total_value = (isset($data['total_value']) && ($data['total_value'] != NULL)) ? $data['total_value'] : '';
      //Unpaid amount text value
      $unpaid_amount_text = (isset($data['unpaid_amount_text_value']) && ($data['unpaid_amount_text_value'] != NULL)) ? $data['unpaid_amount_text_value'] : '';

      //Unpaid amount
      $unpaid_amount = (isset($data['unpaid_amount']) && ($data['unpaid_amount'] != NULL)) ? $data['unpaid_amount'] : '';

      //Attorney Fees
      $attorneys_fees = (isset($data['attorneys_fees']) && ($data['attorneys_fees'] != NULL)) ? 'these charges along with interest accruing at the rate of ' . $data['attorneys_fees'] . ' monthly and attorneys fees,' : '';

      //Job Start date
      $job_start_date = (isset($data['job_start_date']) && ($data['job_start_date'] != NULL)) ? (date('F d,Y', strtotime($data['job_start_date']))) : '';

      //Job end date
      $job_end_date = (isset($data['last_date_on_the_job']) && ($data['last_date_on_the_job'] != NULL)) ? (date('F d,Y', strtotime($data['last_date_on_the_job']))) : '';

      //
      $owner_received_nto_date = (isset($data['owner_received_nto_date']) && ($data['owner_received_nto_date'] != NULL)) ? (date('F d,Y', strtotime($data['owner_received_nto_date']))) : '';
      $owner_received_type_mailing = (isset($data['owner_received_type_mailing']) && ($data['owner_received_type_mailing'] != NULL)) ? ('by ' . $data['owner_received_type_mailing']) : '';
      if (isset($contractor_name) && !empty($contractor_name)) {
      $contractor_name = ',that the lienor served copies of the notice on the contractor,' . $contractor_name;
      }
      $contractor_received_nto_date = (isset($data['contractor_received_nto_date']) && ($data['contractor_received_nto_date'] != NULL)) ? ('on' . date('F d,Y', strtotime($data['contractor_received_nto_date']))) : '';
      $contractor_received_type_mailing = (isset($data['contractor_received_type_mailing']) && ($data['contractor_received_type_mailing'] != NULL)) ? (',by ' . $data['contractor_received_type_mailing']) : '';
      if (isset($sub_contractor_name) && !empty($sub_contractor_name)) {
      $sub_contractor_name = 'and on the Sub Contractor';
      }
      $sub_contractor_received_nto_date = (isset($data['sub_contractor_received_nto_date']) && ($data['sub_contractor_received_nto_date'] != NULL)) ? (date('F d,Y', strtotime($data['sub_contractor_received_nto_date']))) : '';
      $sub_contractor_received_type_mailing = (isset($data['sub_contractor_received_type_mailing']) && ($data['sub_contractor_received_type_mailing'] != NULL)) ? $data['sub_contractor_received_type_mailing'] : '';

      $arr_variable = ['{{your_job_reference_no}}', '{{work_order_no}}', '{{parent_work_order}}', '{{notary_state}}', '{{notary_county}}', '{{printing_date}}', '{{office_director}}', '{{company_name}}',
      '{{company_address}}', '{{company_city_state_zip}}', '{{contracted_by}}',
      '{{project_address}}', '{{project_name}}', '{{legal_description}}', '{{folio}}', '{{bond}}',
      '{{noc}}', '{{owner_name}}', '{{amount_text}}', '{{amount}}', '{{unpaid_amount_text}}', '{{unpaid_amount}}',
      '{{attorneys_fees}}', '{{job_start_date}}', '{{job_end_date}}', '{{printing_day}}', '{{printing_month}}', '{{owner_received_nto_date}}', '{{owner_received_type_mailing}}',
      '{{contractor_name}}', '{{contractor_received_nto_date}}', '{{contractor_received_type_mailing}}',
      '{{sub_contractor_name}}', '{{sub_contractor_received_nto_date}}', '{{sub_contractor_received_type_mailing}}', '{{state}}'];

      $arr_replace = [$your_job_reference_no, $work_order_no, $parent_work_order, $notary_state, $notary_county, $printing_date, $office_director, $company_name,
      $company_address, $company_city_state_zip, $contracted_by, $project_address, $project_name, $legal_description, $folio, $bond, $noc,
      $owner_name, $total_value_in_text, $total_value, $unpaid_amount_text, $unpaid_amount, $attorneys_fees, $job_start_date, $job_end_date, $printing_day, $printing_month,
      $owner_received_nto_date, $owner_received_type_mailing, $contractor_name, $contractor_received_nto_date, $contractor_received_type_mailing,
      $sub_contractor_name, $sub_contractor_received_nto_date, $sub_contractor_received_type_mailing, $project_state];

      $content = str_replace($arr_variable, $arr_replace, $content);
      }
      } elseif ($notice_id == 7) {
      //Satisafaction bond claim notice


      $template_content = \App\Models\NoticeTemplate::where('state_id', '=', $notice_state)
      ->where('notice_id', '=', $notice_id)
      ->first();
      $content = $template_content->content;
      if (isset($content) && $content != NULL) {

      $bond_claim_number = (isset($data['bond_claim_number']) && $data['bond_claim_number'] != NULL) ? $data['bond_claim_number'] : '';

      $bond_number = (isset($data['bond_number']) && $data['bond_number'] != NULL) ? $data['bond_number'] : '';

      $bond_company = (isset($data['bond_company']) && $data['bond_company'] != NULL) ? $data['bond_company'] : '';

      //Recipients
      $owner_name = '';
      $contractor_name = '';
      $sub_contractor_name = '';
      if (isset($recipients) && count($recipients) > 0) {
      foreach ($recipients AS $k => $each_recipient) {
      if ($each_recipient->category_id == 1) {

      $owner_name = $each_recipient->name;
      }
      if ($each_recipient->category_id == 2) {

      $contractor_name = $each_recipient->name;
      }
      if ($each_recipient->category_id == 3) {

      $sub_contractor_name = $each_recipient->name;
      }
      }
      }

      //Authorise Signture
      if (isset($data['authorise_agent']) && $data['authorise_agent'] != NULL) {
      $signature = \App\Models\CustomerAgent::find($data['authorise_agent']);
      $office_director = $signature->first_name . ' ' . $signature->last_name;
      $office_director_title = $signature->title;
      } else {
      $office_director = '';
      $office_director_title = '';
      }

      $arr_variable = ['{{company_name}}', '{{company_address}}', '{{company_city_state_zip}}',
      '{{work_order_no}}', '{{parent_work_order}}', '{{bond_claim_number}}', '{{bond_number}}', '{{bond_company}}',
      '{{printing_date}}', '{{project_address}}', '{{project_name}}', '{{legal_description}}', '{{folio}}', '{{bond}}',
      '{{noc}}', '{{contracted_by}}', '{{owner_name}}', '{{office_director}}', '{{office_director_title}}',
      '{{printing_day}}', '{{printing_month}}', '{{state}}'];

      $arr_replace = [$company_name, $company_address, $company_city_state_zip,
      $work_order_no, $parent_work_order, $bond_claim_number, $bond_number, $bond_company, $printing_date,
      $project_address, $project_name, $legal_description, $folio, $bond, $noc, $contracted_by,
      $owner_name, $office_director, $office_director_title, $printing_day, $printing_month, $project_state];

      $content = str_replace($arr_variable, $arr_replace, $content);
      }
      } elseif ($notice_id == 5) {
      //Satisafaction  claim notice

      $template_content = \App\Models\NoticeTemplate::where('state_id', '=', $notice_state)
      ->where('notice_id', '=', $notice_id)
      ->first();
      $content = $template_content->content;
      if (isset($content) && $content != NULL) {

      if (isset($data['recorded_date']) && !empty($data['recorded_date'])) {
      //Recorded Day
      $recorded_day = date('d', strtotime($data['recorded_date'])); //October 2018
      //Recorded Month
      $recorded_month = date('F Y', strtotime($data['recorded_date']));
      } else {
      $recorded_day = '';
      $recorded_month = '';
      }

      if (isset($data['recorded_county']) && !empty($data['recorded_county'])) {
      $recorded_county_name = \App\Models\Country::find($data['recorded_county']);
      $recorded_county = $data['recorded_county'];
      } else {
      $recorded_county = '';
      }

      if (isset($data['recorded_state']) && !empty($data['recorded_state'])) {
      $recorded_state_name = \App\Models\State::find($data['recorded_state']);
      $recorded_state = $data['recorded_state'];
      } else {
      $recorded_state = '';
      }

      if (isset($data['book_no']) && !empty($data['book_no'])) {
      $book_no = $data['book_no'];
      } else {
      $book_no = '';
      }

      if (isset($data['page_no']) && !empty($data['page_no'])) {
      $page_no = $data['page_no'];
      } else {
      $page_no = '';
      }

      //Authorise Signture
      if (isset($data['authorise_agent']) && $data['authorise_agent'] != NULL) {
      $signature = \App\Models\CustomerAgent::find($data['authorise_agent']);
      $office_director = $signature->first_name . ' ' . $signature->last_name;
      $office_director_title = $signature->title;
      } else {
      $office_director = '';
      $office_director_title = '';
      }

      //Recipients
      $owner_name = '';
      if (isset($recipients) && count($recipients) > 0) {
      foreach ($recipients AS $k => $each_recipient) {
      if ($each_recipient->category_id == 1) {

      $owner_name = $each_recipient->name;
      }
      }
      }

      $arr_variable = ['{{company_name}}', '{{company_address}}', '{{company_city_state_zip}}',
      '{{work_order_no}}', '{{parent_work_order}}', '{{printing_date}}',
      '{{recorded_day}}', '{{recorded_month}}', '{{recorded_county}}', '{{recorded_state}}',
      '{{book_no}}', '{{page_no}}',
      '{{project_address}}', '{{project_name}}', '{{legal_description}}', '{{folio}}', '{{bond}}',
      '{{noc}}', '{{contracted_by}}', '{{owner_name}}', '{{office_director}}', '{{office_director_title}}',
      '{{printing_day}}', '{{printing_month}}', '{{state}}'];

      $arr_replace = [$company_name, $company_address, $company_city_state_zip,
      $work_order_no, $parent_work_order, $printing_date,
      $recorded_day, $recorded_month, $recorded_county, $recorded_state,
      $book_no, $page_no,
      $project_address, $project_name, $legal_description, $folio, $bond,
      $noc, $contracted_by, $owner_name, $office_director, $office_director_title,
      $printing_day, $printing_month, $project_state];

      $content = str_replace($arr_variable, $arr_replace, $content);
      }
      }
      $final_content = '';
      if (isset($recipients) && count($recipients) > 0) {
      foreach ($recipients AS $each) {

      $each_sub_state_name = \App\Models\State::find($each->state_id);
      $each_sub_state = $each_sub_state_name->name;
      $each_sub_city_name = \App\Models\City::find($each->city_id);
      $each_sub_city = $each_sub_city_name->name;

      $each_city_state_zip = $each_sub_city . ' ' . $each_sub_state . ' ' . $each_recipient->zip;



      $recipients_html = $content . '<div style="page-break-after: always;"></div>
      <table style="width: 600px;font-size: 12px;margin: 0 auto">
      <tr>
      <td style="text-align: left;padding: 10px;">
      <table>
      <tr>
      <td style="text-align: center;"><img src="' . $logo . '" style="height: 120px;width: 120px;" /></td>
      </tr>
      <tr>
      <td>AAA BUSINESS ASSOC.<br />
      P.O. Box 22821<br />
      Hialeah Florida 33002</td>
      </tr>
      </table>
      </td>
      </tr>
      <tr>
      <td align="right" style="text-align: left;padding: 10px;">
      <table align="right">
      <tr>
      <td>
      7017 2620 0000 2624 2089-83118<br/>
      ' . $each->name . '<br/>
      ' . $each->address . '<br/>
      ' . $each_city_state_zip . '<br/>
      </td>

      </tr>
      </table>
      </td>
      </tr></table>   <div style="page-break-after: always;"></div>                                             ';
      $final_content = $final_content . $recipients_html;
      }
      }
      if ($final_content != '') {
      $content = $final_content;
      } else {
      $content = $content;
      }
      $pdf = PDF::loadHTML($content);


      if ($work_order->file_name == NULL) {
      $file_name = $work_order_id . '_' . uniqid() . '.pdf';
      } else {
      $file_name = $work_order->file_name;
      }
      if (!file_exists(public_path() . '/pdf/work_order_document')) {
      mkdir(public_path() . '/pdf/work_order_document', 0777, true);
      }
      //update file name
      $work_order->file_name = $file_name;
      $work_order->status = 5;
      $work_order->save();

      if (!file_exists(public_path() . '/pdf/work_order_document/' . $file_name)) {
      $pdf->save(public_path() . '/pdf/work_order_document/' . $file_name);
      }

      return $pdf->stream($file_name);
      } */

    function updateStatus($id) {
        $result = \App\Models\Cyo_WorkOrders::find($id);
        if ($result->status == 0 || $result->status == 1) {
            $result->status = 0;
            $result->save();
        }

        Session::flash('success', 'Your work # ' . $id . ' Work Order Saved Successfully.<a href="' . url('customer/create-your-own/view/' . $id) . '">Click here to print.</a>');
        return redirect('customer/create-your-own/work-order-history');
    }

    function CheckFields(Request $request) {

        $Cyo_Fields = \App\Models\Cyo_WorkOrders::find($request->id);
        $Cyo_Fields_Data = \App\Models\Cyo_WorkOrder_fields::select()->where('workorder_id', $request->id)->get()->toArray();
        $notice_Field_ids = \App\Models\NoticeField::select()->where('notice_id', $Cyo_Fields->notice_id)->where('is_required', 1)->get()->toArray();
        $count = 0;
        foreach ($notice_Field_ids as $notice_field_id) {
            // return $notice_field_id;
            $result = \App\Models\Cyo_WorkOrder_fields::select('value')->where('workorder_id', $request->id)->where('notice_field_id', $notice_field_id['id'])->get()->toArray();

            if (isset($result[0]) && $result[0]['value'] == '') {
                $count = $count + 1;
            }
        }
        if ($count != 0) {
            return 0;
        } else
            return 1;
    }

    /*     * Function is used to delete attachment
     */

    public function deleteAttachment(Request $request) {
        if ($request->file != "" && file_exists(public_path('attachment/cyo_work_order_document/' . $request->file))) {
            unlink(public_path('attachment/cyo_work_order_document/' . $request->file));
        }
//        return redirect::back();
        return response()->json(['success' => 1]);
    }

    public function deleteParentAttachment(Request $request) {

        if ($request->file != "" && file_exists(public_path('attachment/cyo_work_order_document/parent_document/' . $request->file))) {
            unlink(public_path('attachment/cyo_work_order_document/parent_document/' . $request->file));
        }

        return response()->json(['success' => 1]);
    }

    public function deleteEditCyoDocAttachment(Request $request) {

        $workorderAttachment = \DB::table('cyo__work_order__attachments')->find($request->id);

        $workorderAttachment = \DB::table('cyo__work_order__attachments')
                ->where('id', '=', $request->id);
        if ($request->file != "" && file_exists(public_path('attachment/cyo_work_order_document/' . $request->file))) {
            unlink(public_path('attachment/cyo_work_order_document/' . $request->file));
        }
        $result = $workorderAttachment->delete();

//        return redirect::back();
        return response()->json(['success' => $result]);
    }

    /**
     * Function is used to when click on edit recipient get data on recipient page
     */
    public function getRecipientData(Request $request) {
        $recipient_id = $request->id;

        $recipient = \App\Models\Cyo_WorkOrder_Recipient::with('CyoUspsAddress')->find($recipient_id);
        
        if (isset($recipient) && !empty($recipient)) {
            if (!empty($recipient->city_id)) {
                $city_name = \App\Models\City::where('id', '=', $recipient->city_id)
                        ->select('name')
                        ->first();
                $recipient->city_name = $city_name->name;
            } else {
                $recipient->city_name = "";
            }

            if(!empty($recipient->CyoUspsAddress)) {
                $recipient->address2 = $recipient->CyoUspsAddress->address2;
                $recipient->address3 = $recipient->CyoUspsAddress->address3;
            }
        }
        return \Response::json(['success' => true, 'recipient' => $recipient
        ]);
    }

    /*
      firm mail for stamps
      recipient return receipt
      priority mail
      from address is project address added for work order
     */

    public function createLabel(Request $request) {        
        $flash = ['', ''];
        $error_flash_messages = 'Could not save data for recipient ids ';
        $success_flash_messages = 'Saved data for recipient ids ';
        $user = \Auth::user();
        $now = date('Y-m-d H:i:s');
        $recipient_ids = $request->recipient_ids;

        //Get firm mail price from quickbooks
        $qbo = new \App\Http\Controllers\QuickBooksController();
        $qbo_response = $qbo->generateTokens(config('constants.QBO.SCOPE'), 'getProductDetails', ['customer_id' => $user->id, 'product_id' => env('FirmMail')]);

        $qbo_response['response'] = 'error';
        if ($qbo_response['response'] == 'error') {
            $firm_mail = 1;
        } else {
            $firm_mail = $qbo_response['item_details']['Item']['UnitPrice'];
        }


        //Get next day delivery price from quickbooks
        $qbo_response = $qbo->generateTokens(config('constants.QBO.SCOPE'), 'getProductDetails', ['customer_id' => $user->id, 'product_id' => env('NextDayDelivery')]);
        $qbo_response['response'] = 'error';
        if ($qbo_response['response'] == 'error') {
            $next_day_delivery = 2;
        } else {
            $next_day_delivery = $qbo_response['item_details']['Item']['UnitPrice'];
        }

         $qbo_response = $qbo->generateTokens(config('constants.QBO.SCOPE'), 'getProductDetails', ['customer_id' => $user->id, 'product_id' => env('ReturnReceiptRequested')]);

        if ($qbo_response['response'] == 'error') {
            $cert_rr = 1;
        } else {
            $cert_rr = $qbo_response['item_details']['Item']['UnitPrice'];
        }
        $cert_rr += config('constants.QBO.MARGIN_CERTIFIED_RR');

        $qbo_response = $qbo->generateTokens(config('constants.QBO.SCOPE'), 'getProductDetails', ['customer_id' => $user->id, 'product_id' => env('ElectronicReturnReceipt')]);
        if ($qbo_response['response'] == 'error') {
            $cert_er = 1;
        } else {
            $cert_er = $qbo_response['item_details']['Item']['UnitPrice'];
        }
        $cert_er += config('constants.QBO.MARGIN_CERTIFIED_ERR');

        $qbo_response = $qbo->generateTokens(config('constants.QBO.SCOPE'), 'getProductDetails', ['customer_id' => $user->id, 'product_id' => env('CertifiedMail')]);
        if ($qbo_response['response'] == 'error') {
            $cert_usps = 1;
        } else {
            $cert_usps = $qbo_response['item_details']['Item']['UnitPrice'];
        }
        $cert_usps += config('constants.QBO.MARGIN_CERTIFIED');

       /* if (isset($request->rush_hour)) {
            $rush_hour = 'yes';
            //Get rush hour charge from quickbooks
            $qbo_response = $qbo->generateTokens(config('constants.QBO.SCOPE'), 'getProductDetails', ['customer_id' => $user->id, 'product_id' => env('RushHourCharges')]);
            $qbo_response['response'] = 'error';
            if ($qbo_response['response'] == 'error') {
                $rush_hour_amount = 10;
            } else {
                $rush_hour_amount = $qbo_response['item_details']['Item']['UnitPrice'];
            }
        } else {
            $rush_hour = 'no';
            $rush_hour_amount = 0;
        }*/
         if (isset($request->rush_hour)) {
          $rush_hour = 'yes';
          $rush_hour_amount = $request->rush_hour_charges;
         }else{
          $rush_hour = 'no';
          $rush_hour_amount = 0;
         }

        //Add the rush hour condition for work order
        \App\Models\Cyo_WorkOrders::where(['id' => $request->workorder_id])->update(['rush_hour_charges' => $rush_hour, 'rush_hour_amount' => $rush_hour_amount]);
        
        if(!empty($recipient_ids)) {
            for ($cnt = 1; $cnt <= count($recipient_ids); $cnt++) {
                $create_flag = 0;
                if (isset($request['type_of_mailing_' . $cnt]) && $request['type_of_mailing_' . $cnt] != '') {

                    $type_of_label = isset($request['type_of_mailing_' . $cnt]) ? $request['type_of_mailing_' . $cnt] : 'stamps';

                    if ($type_of_label == 'firm mail' || $type_of_label == 'next day delivery' || $type_of_label == 'Cert. USPS' || $type_of_label == 'Cert. RR' || $type_of_label == 'Cert. ER') {
                        if (isset($request['manual_inputs' . $cnt]) && $request['manual_inputs' . $cnt] != '') {
                            $tracking_number = $request['manual_inputs' . $cnt];
                            $generated_label = 'yes';
                            $create_flag = 1;
                        }
                    }else if ($type_of_label == 'manual') {
                        if (isset($request['manual_inputs' . $cnt]) && $request['manual_inputs' . $cnt] != '') {
                            $manual_charges = $request['manual_amount' . $cnt];
                            $tracking_number = $request['manual_inputs' . $cnt];
                            $generated_label = 'yes';
                            $create_flag = 1;
                        }
                    } else {
                        $tracking_number = '';
                        $generated_label = 'no';
                        $create_flag = 1;
                    } 
                    if ($create_flag == 1) {
                        $rate = ($type_of_label == 'firm mail') ? $firm_mail : $next_day_delivery;
                        if($type_of_label=="firm mail"){
                            $rate = $firm_mail;
                        }else if($type_of_label=="next day delivery"){
                            $rate = $next_day_delivery;
                        }else if($type_of_label=="Cert. USPS"){
                            $rate = $cert_usps;
                        }else if($type_of_label=="Cert. RR"){
                            $rate = $cert_rr;
                        }else if($type_of_label=="Cert. ER"){
                            $rate = $cert_er;
                        }else if($type_of_label=="manual"){
                            $rate = $manual_charges;
                        }
                        $label_insert_array = [
                            'Amount' => $rate,
                            'tracking_number' => $tracking_number,
                            'recipient_id' => $recipient_ids[$cnt - 1],
                            'created_at' => $now,
                            'updated_at' => $now,
                            'type_of_label' => $type_of_label,
                            'generated_label' => $generated_label,
                            'add_ons' => "",
                            'ShipDate' => "",
                            'rates_data_id' => null, 
                            'type_of_mailing' => null
                        ];
                        //Check if label record present for the recipient
                        $chk_label_record = \App\Models\Cyo_StampsLabel::where(['recipient_id' => $recipient_ids[$cnt - 1]])->orderBy('id', 'desc')->limit(1)->get()->toArray();

                        if (count($chk_label_record) > 0) {
                            //Delete previous record if present
                            if ($chk_label_record[0]['rates_data_id'] != NULL) {
                            /* $rates_data_id = \App\Models\Cyo_StampsRatesData::where(['id' => $chk_label_record[0]['rates_data_id']])->delete();*/
                            }
                            \App\Models\Cyo_StampsLabel::where(['recipient_id' => $recipient_ids[$cnt - 1]])->update($label_insert_array);
                            $label_id = $chk_label_record[0]['id'];
                        } else {
                            $label_id = \App\Models\Cyo_StampsLabel::create($label_insert_array)->id;
                        }
                    }

                    //Storing data for stamps shipping labels
                    if ($type_of_label == 'stamps') {
                        $today = date('Y-m-d');
                        //$shipping_date = (isset($request['shipping_date' . ($cnt)])) ? $request['shipping_date' . ($cnt)] : $today;
                        $shipping_date = (isset($request['shipping_date' . ($cnt)]) && $request['shipping_date' . ($cnt)] != '') ? date('Y-m-d', strtotime($request['shipping_date' . ($cnt)])) : $today;

                        //Stamps.com get token
                        $wsdl           = config('constants.STAMPS.WSDL');
                        $integrationID  = config('constants.STAMPS.INTEGRATION_ID');
                        $username       = config('constants.STAMPS.USERNAME');
                        $password       = config('constants.STAMPS.PASSWORD');
                        
                        $stamps = new \App\Helpers\StampsHelper($wsdl, $integrationID, $username, $password);

                        //Setting from address for creating label
                        $from_addr = [
                            'FullName' => env('FROM_NAME'),
                            'Address1' => env('FROM_ADDRESS'),
                            'City' => env('FROM_CITY'),
                            'State' => env('FROM_STATE'),
                            'ZIPCode' => env('FROM_ZIPCODE')
                        ];

                        $wsdl           = config('constants.STAMPS.WSDL');
                        $integrationID  = config('constants.STAMPS.INTEGRATION_ID');
                        $username       = config('constants.STAMPS.USERNAME');
                        $password       = config('constants.STAMPS.PASSWORD');
                        
                        $stamps = new \App\Helpers\StampsHelper($wsdl, $integrationID, $username, $password);

                        // $packages = $request['package_type' . ($cnt)];
                        // $service_type = $request['service_types' . ($cnt)];
                        $packages = 'Letter';
                        $service_type = 'US-FC';
                        $add_on_ids = $request['add_ons_' . ($cnt)];
                        $package_type_id = \App\Models\StampsPackages::where('name', $packages)->pluck('id')->toArray();
                        $service_type_id = \App\Models\StampsServices::where('abbr', $service_type)->pluck('id')->toArray();

                        if (count($add_on_ids) > 0) {
                            $addons = \App\Models\StampsAddOns::whereIn('id', $add_on_ids)->pluck('abbr')->toArray();
                            $addons_description = \App\Models\StampsAddOns::whereIn('id', $add_on_ids)->pluck('description')->toArray();
                        } else {
                            $addons = $addons_description = [];
                        }
                        $addons_description = !empty($addons_description) ? implode(',', $addons_description) : '';
                        $add_on_ids = !empty($add_on_ids) ? implode(',', $add_on_ids) : '';
                        $usps_addr = json_decode($request['usps_addr_' . $cnt], true);
                        $get_rates_values = [
                            "FromZIPCode" => $from_addr['ZIPCode'],
                            "ToZIPCode" => $usps_addr['zipcode'],
                            // "WeightLb" => 1,
                            // "InsuredValue" => 100,
                            "PackageType" => $packages,
                            "ShipDate" => $shipping_date,
                            "ServiceType" => $service_type
                        ];
                        if (!empty($addons)) {
                            $get_rates_values["AddOns"] = $addons;

                            //add print layout for label
                            $get_rates_values["PrintLayout"] = 'SDC3820';
                            if(in_array('US-A-RRE', $addons)) {
                                $get_rates_values["PrintLayout"] = 'SDC3820';
                            } else if(in_array('US-A-RR', $addons)) {
                                $get_rates_values["PrintLayout"] = 'SDC3810';
                            }
                            //for future: //Certified - SDC3810
                        }

                        try {
                            $get_rates = $stamps->GetRates($get_rates_values);
                            if ($get_rates['status'] == 'success') {
                                $get_rates = $get_rates['result'];
                                //Store the rates data in stamps_rates_data table, and delete the record once label is generated successfully
                                $rates_data_id = \App\Models\Cyo_StampsRatesData::create(['rates_data' => json_encode($get_rates)])->id;
                            
                                //Store rates_data_id in stamps_label table for generating label later
                                $update_array = ['ShipDate' => $get_rates_values['ShipDate'], 'rates_data_id' => $rates_data_id, 'add_ons' => $add_on_ids, 'package_type' => $package_type_id[0], 'service_type' => $service_type_id[0],'type_of_mailing'=>$addons_description];
                                
                                \App\Models\Cyo_StampsLabel::where(['id' => $label_id])->update($update_array);
                                $success_ids[] = $recipient_ids[$cnt - 1]; 
                            } else {
                                \Log::error(json_encode($get_rates) . ' for recipient id' . $recipient_ids[$cnt - 1]);
                                $error_ids[] = $recipient_ids[$cnt - 1];
                            }
                        } catch (Exception $e) {dd($e);
                            $error_ids[] = $recipient_ids[$cnt - 1];
                            \Log::error('Issue generating rates using stamps.com for recipient id ' . $recipient_ids[$cnt - 1]);
                        }
                    }//inner if for type of mailing stamps
                }//if
            }//for
        }

        if (!empty($error_ids)) {
            $error_ids = implode(', ', $error_ids);
            $error_flash_messages .= $error_ids;
            $flash = ['error', $error_flash_messages];
        }

        if (!empty($success_ids)) {
            $success_ids = implode(', ', $success_ids);
            $success_flash_messages .= $success_ids;
            $flash = ['success', $success_flash_messages];
        }

        if ($request->saveLabelData == 'no') {
            $mailing_status = \App\Models\Cyo_WorkOrders::find($request->workorder_id);
            $mailing_status->status = 9;
            $mailing_status->save();
            $flash = ['success', 'Work order has been successfully proceed to mailing.'];
        }
        if ($request->recording_pending_signature != 0) {
            $mailing_status = \App\Models\Cyo_WorkOrders::find($request->workorder_id);
            $mailing_status->status = $request->recording_pending_signature;
            $mailing_status->save();
            return json_encode(['status' => 'success', 'message' => 'Your work order number #'.$request->workorder_id.' successfully submitted']);
        }
        if ($request->generate_pdf_flag != 0) {
            return "true";
        }
       // echo "<script>window.close();</script>";
        return redirect::to('customer/create-your-own/work-order-history')->with($flash[0], $flash[1]);
    }

    /**
     * print work order pdf
     */
    public function printWO($work_order_id) {

        $file_name = \App\Models\Cyo_WorkOrders::find($work_order_id);
        if (file_exists(public_path() . '/pdf/cyo_work_order_document/' . $file_name->file_name)) {

            $mypdf = asset('/') . 'pdf/cyo_work_order_document/' . $file_name->file_name;
            echo "<script>window.open('$mypdf', '_blank');</script>";
        }

        $customer_id = Auth::User()->customer->id;

        $data['account_managers'] = \App\Models\User::select('id', 'name')->join('role_users', 'role_users.user_id', 'users.id')->where('role_users.role_id', 3)->get();
// dd($data['account_managers']);
        $data['categories'] = \App\Models\Category::select()->orderBy('name', 'ASC')->get();
        $data['project_roles'] = \App\Models\ProjectRoles::select()->orderBy('name', 'ASC')->get();
        $data['customers'] = \App\Models\Customer::paginate();
        $data['WorkOrders'] = \App\Models\Cyo_WorkOrders::select()->where('customer_id', $customer_id)->paginate(10);
        $data['WorkOrderFields'] = \App\Models\Cyo_WorkOrder_fields::select()->join('notice_fields', 'notice_fields.id', '=', 'cyo__work_order_fields.notice_field_id')->join('cyo__work_orders', 'cyo__work_orders.id', '=', 'cyo__work_order_fields.workorder_id')->where('notice_fields.name', 'project_address')->where('cyo__work_orders.customer_id', $customer_id)->get();
        $data['contracted_by'] = \App\Models\Contact::select()->where('customer_id', Auth::user()->customer->id)->orderBy('company_name', 'ASC')->get();
        $data['states'] = \App\Models\State::select()->orderBy('name')->get();
        $data['extraTitle'] = ' - Create Your Own';
        return view('customer.create_your_own.work_order_history', $data);
    }

    public function printrequesteworkorder($work_order_id) {
        $workorderdetails = \App\Models\Cyo_WorkOrders::find($work_order_id);
        if ($workorderdetails->status == 0) {
            $status = 'Draft';
        } elseif ($workorderdetails->status == 1) {
            $status = 'Request';
        } elseif ($workorderdetails->status == 2) {
            $status = 'Processing';
        } elseif ($workorderdetails->status == 3) {
            $status = 'Recording';
        } elseif ($workorderdetails->status == 4) {
            $status = 'Pending Signature';
        } elseif ($workorderdetails->status == 5) {
            $status = 'Mailed';
        } elseif ($workorderdetails->status == 6) {
            $status = 'Completed';
        } elseif ($workorderdetails->status == 7) {
            $status = 'Cancelled';
        } 
        $customer_company_name = \App\Models\Customer::find($workorderdetails->customer_id);
        $company_name = $customer_company_name->company_name;
        $contact_name = $customer_company_name->contact_person;
        $company_address = $customer_company_name->mailing_address;
        $company_state_name = \App\Models\State::find($customer_company_name->mailing_state_id);
        $company_state = $company_state_name->name;
        $company_city_name = \App\Models\City::find($customer_company_name->mailing_city_id);
        $company_city = !empty($company_city_name) ? $company_city_name->name : '';
        $full_address = $company_address . '<br>' . $company_city . ' ' . $company_state . ' ' . $customer_company_name->mailing_zip;

        $WorkOrderFields = \App\Models\Cyo_WorkOrder_fields::select('cyo__work_orders.parent_id', 'cyo__work_orders.created_at', 'cyo__work_orders.user_id', 'cyo__work_orders.customer_id', 'cyo__work_orders.notice_id as notice_id', 'cyo__work_orders.status', 'cyo__work_orders.user_id', 'cyo__work_orders.account_manager_id', 'cyo__work_order_fields.workorder_id', 'cyo__work_order_fields.notice_id', DB::raw("(GROUP_CONCAT(name SEPARATOR '||')) as `field_names`"), DB::raw("(GROUP_CONCAT(value SEPARATOR '||')) as `field_values`"))->join('notice_fields', 'notice_fields.id', '=', 'cyo__work_order_fields.notice_field_id')
                        ->join('cyo__work_orders', 'cyo__work_orders.id', '=', 'cyo__work_order_fields.workorder_id')->groupBy('cyo__work_order_fields.workorder_id', 'cyo__work_order_fields.notice_id')
                        ->where('cyo__work_orders.id', $work_order_id)
                        ->get()->toArray();
        $result = [];
        if (isset($WorkOrderFields) && !empty($WorkOrderFields)) {
            foreach ($WorkOrderFields as $fields_data) {
                $field_names = explode('||', $fields_data['field_names']);
                $field_values = explode('||', $fields_data['field_values']);
                $field_names_values = array_combine($field_names, $field_values);
                $field_names_values['default'] = '';
                $result[] = array_merge($fields_data, $field_names_values);
            }
        }
        $data = $result[0];
        if (isset($data['contracted_by']) && !empty($data['contracted_by'])) {
            $contracted_by = \App\Models\Contact::where('company_name', '=', $data['contracted_by'])
                    ->where('customer_id', '=', $workorderdetails->customer_id)
                    ->first();
            $contracted_by_name = $contracted_by->company_name;
            $contracted_by_address = $contracted_by->mailing_address;
            $contracted_by_phone = $contracted_by->phone;
        } else {
            $contracted_by_name = '';
            $contracted_by_address = '';
            $contracted_by_phone = '';
        }
        $notice = \App\Models\Notice::select('name', 'id', 'type', 'is_claim_of_lien','master_notice_id')->where('id', $workorderdetails->notice_id)->get()->toArray();
        $data['notice'] = $notice[0];
        $data['is_rescind'] = $workorderdetails->is_rescind;
        $notice_fields = \App\Models\NoticeField::select()
                        ->join('cyo__work_order_fields', 'cyo__work_order_fields.notice_field_id', '=', 'notice_fields.id')
                        ->where('cyo__work_order_fields.notice_id', $workorderdetails->notice_id)
                        ->where('cyo__work_order_fields.workorder_id', $work_order_id)
                        ->orderBy('sort_order', 'asc')->get();
        if (isset($notice_fields) && !empty($notice_fields)) {
            foreach ($notice_fields AS $key => $value) {
                if ($value->name == 'date_request' || $value->name == 'enter_into_agreement' ||
                        $value->name == 'last_date_of_labor_service_furnished' || $value->name == 'job_start_date') {
                    if (isset($value) && $value != NULL && $value != '') {
                        $value->value = date('m-d-Y', strtotime($value->value));
//                          $date = \DateTime::createFromFormat("m-d-Y", $value->value);
//                           
//                            $date = $date->format('m-d-Y'); 
//                            $value->value = $date;
                    }
                } elseif ($value->name == 'county' || $value->name == 'notary_county' || $value->name == 'recorded_county' ||
                        $value->name == 'project_county') {
                    if (isset($value) && $value != NULL) {
                        $county = \App\Models\City::find($value->value);
                        if (isset($county) && !empty($county)) {
                            $value->value = $county->county;
                        }
                    }
                } elseif ($value->name == 'notary_state' || $value->name == 'recorded_state') {
                    if (isset($value) && $value != NULL || $value != '') {
                        $state = \App\Models\State::find($value->value);
                        if (isset($state) && !empty($state)) {
                            $value->value = $state->name;
                        }
                    }
                } elseif ($value->name == 'your_role') {
                    if (isset($value) && $value != NULL || $value != '') {
                        $category = \App\Models\ProjectRoles::find($value->value);
                        $value->value = $category->name;
                    }
                } elseif ($value->name == 'project_type') {
                    if (isset($value) && $value != NULL || $value != '') {
                        $project_type = \App\Models\ProjectType::find($value->value);
                        $value->value = $project_type->type;
                    }
                }
            }
        }
        $recipients = \App\Models\Cyo_WorkOrder_Recipient::select()->where('work_order_id', $work_order_id)->get();
        if (Auth::user()->hasRole('customer')) {
            $notes = \App\Models\Cyo_WorkOrder_Notes::where('work_order_id', $work_order_id)->where('visibility', '!=', '1')->get();
        } else {
            $notes = \App\Models\Cyo_WorkOrder_Notes::where('work_order_id', $work_order_id)->get();
        }


        if (Auth::user()->hasRole('customer')) {
            $correction = \App\Models\Cyo_WorkOrder_Corrections::where('work_order_id', $work_order_id)->where('visibility', '!=', '1')->get();
        } else {
            $correction = \App\Models\Cyo_WorkOrder_Corrections::where('work_order_id', $work_order_id)->get();
        }

        $account_manager = \App\User::where('id', $workorderdetails->account_manager_id)->first();
        if (isset($account_manager) && $account_manager != NULL) {
            $account_manager_name = $account_manager->name;
        } else {
            $account_manager_name = '';
        }
        $attachement = \App\Models\Cyo_WorkOrder_Attachment::where('work_order_id', $work_order_id)->get();
        $content = '<!DOCTYPE html>
                    <html>
                    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                            <title>Work Order Info</title>
                    </head>
                    <body>
                            <table style="width:700px;font-size: 10px;margin: 0 auto;table-layout: fixed; word-wrap:break-word;" cellspacing="0" cellpadding="0">
                                    <tr>
                                            <td valign="top" colspan="4" style="padding:0 10px;"><h5 style="text-transform: uppercase;border-top: 1px solid #000;border-bottom: 1px solid #000;padding: 5px 0;margin:0;">order information</h5></td>
                                    </tr>

                                    <tr>
                                             <td valign="top" style="padding:0 5px;" width="20%"> Work Order No #' . $work_order_id . '</td>
                                            <td valign="top" style="padding:0 5px;font-weight: bold" width="65%">(Status:' . $status . ') ' . $data['notice']['name'] .(($data['is_rescind'] == 1) ? ' Amendment ' : ''). '</td>
                                           <td valign="top" style="padding:0 5px;text-align:right" align="right" width="15%">Submitted Date</td>
                                            <td valign="top" style="padding:0 5px;font-weight: bold" width="15%">' . date('d M Y', strtotime($workorderdetails->created_at)) . '</td>
                                    </tr>
                                    <tr>
			<td valign="top" colspan="4" style="padding:0 5px;"><h5 style="text-transform: uppercase;border-top: 1px solid #000;border-bottom: 1px solid #000;padding:5px 0;margin:0;">your information</h5></td>
		</tr>
                <tr>
		<td colspan="4" valign="top">
                        <table cellspacing="0" cellpadding="0">
                             <tr>
                                <td valign="top" width="100px">
                                        <table  cellspacing="0" cellpadding="0">
                                                <tr>
                                                        <td valign="top" style="padding:0 5px;">Company</td>
                                                </tr>
                                        </table>
                                </td>
                                <td valign="top" width="120px">
                                        <table cellspacing="0" cellpadding="0">
                                                <tr>
                                                        <td valign="top" style="padding:0 5px;">Contact Name</td>
                                                </tr>
                                        </table>
                                </td >
                                <td valign="top" width="150px">
                                 <table cellspacing="0" cellpadding="0">
                                        <tr>
                                                <td valign="top" style="padding:0 5px;">Address</td>
                                        </tr>
                                </table>
                               </td>
                         <td valign="top" width="100px">
                                    <table cellspacing="0" cellpadding="0">
                                            <tr>
                                                    <td valign="top" style="padding:0 5px;">Phone</td>
                                            </tr>
                                    </table>
                               </td>
                        <td valign="top" width="100px">
                                    <table cellspacing="0" cellpadding="0">
                                            <tr>
                                                    <td valign="top" style="padding:0 5px;">Fax</td>
                                            </tr>
                                    </table>
                                </td>
                          <td valign="top" width="100px">
                                    <table  cellspacing="0" cellpadding="0">
                                            <tr>
                                                    <td valign="top" style="padding:0 5px;">Email</td>
                                            </tr>
                                    </table>
                                </td>
                </tr>
                <tr>
                        <td valign="top" >
                                        <table cellspacing="0" cellpadding="0">
                                                <tr>
                                                        <td valign="top" style="padding:0 5px;font-weight: bold;">' . $company_name . '</td>
                                                </tr>
                                        </table>
                                </td>
                        <td valign="top">
                                    <table  cellspacing="0" cellpadding="0">
                                            <tr>
                                                    <td valign="top" style="padding:0 5px;font-weight: bold;">' . $contact_name . '</td>
                                            </tr>
                                    </table>
                                </td>
                         <td align="top">
                                <table style="width: 280px;" cellspacing="0" cellpadding="0">
                                        <tr>
                                                                <td valign="top" style="padding:0 5px;font-weight: bold;">' . $full_address . '</td>
                                                        </tr>
                                                </table>
                                        </td>
                                       <td valign="top">
                                                <table  cellspacing="0" cellpadding="0">
                                                        <tr>
                                                                <td style="padding:0 5px;font-weight: bold;">' . $customer_company_name->office_number . '</td>
                                                        </tr>
                                                </table>
                                        </td>
                                        <td valign="top">
                                                <table  cellspacing="0" cellpadding="0">
                                                        <tr>
                                                                <td style="padding:0 5px;font-weight: bold;">' . $customer_company_name->fax_number . '</td>
                                                        </tr>
                                                </table>
                                        </td>
                               
                                       <td valign="top" colspan="4"  style="line-height:10px;">
                                                <table  cellspacing="0" cellpadding="0">
                                                        <tr>
                                                                <td style="padding:0 5px;font-weight: bold;">' . $customer_company_name->company_email . '</td>
                                                        </tr>
                                                </table>
                                        </td>
                                </tr>
                        </table>
                </td>
        </tr>
        <tr>
			<td valign="top" colspan="4" style="padding:0 5px;"><h5 style="text-transform: uppercase;border-top: 1px solid #000;border-bottom: 1px solid #000;padding: 5px 0;margin:0;">your customers information</h5></td>
		</tr>

		<tr>
			<td colspan="4" valign="top">
				<table cellspacing="0" cellpadding="0">
					<tr>
						<td valign="top" width="100px">
							<table  cellspacing="0" cellpadding="0" style="width:100px">
								<tr>
									<td style="padding:0 5px;font-weight: bold;">' . $contracted_by_name . '</td>
								</tr>
							</table>
						</td>
						<td valign="top" width="120px">
							<table cellspacing="0" cellpadding="0" style="width:120px">
								<tr>
									<td style="padding:0 5px;font-weight: bold;">-</td>
								</tr>
							</table>
						</td>
						<td valign="top" width="100px">
							<table  cellspacing="0" cellpadding="0" style="width:100px;">
								<tr>
									<td style="padding:0 5px;font-weight: bold;">' . $contracted_by_address . '</td>
								</tr>
							</table>
						</td>
						<td valign="top" width="80px">
							<table  cellspacing="0" cellpadding="0" style="width:80px">
								<tr>
									<td style="padding:0 5px;font-weight: bold;">' . $contracted_by_phone . '</td>
								</tr>
							</table>
						</td>
						<<td valign="top" width="100px">
							<table cellspacing="0" cellpadding="0" style="width:100px">
								<tr>
									<td style="padding:0 5px;font-weight: bold;"></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
                        
		</tr>
                <tr>
			<td valign="top" colspan="4" style="padding:0 5px;"><h5 style="text-transform: uppercase;border-top: 1px solid #000;border-bottom: 1px solid #000;padding: 5px 0;margin:0;">project information</h5></td>
		</tr>	<tr>
			<td colspan="4" valign="top">
				<table>';
        $projectinfo = '<tr>';

        if (isset($notice_fields) && !empty($notice_fields)) {

            foreach ($notice_fields as $fields) {
                if ($fields->name != 'date_request') {
                    $label = str_replace("_", " ", $fields->name);
                    if ($fields->name == 'your_job_reference_no') {
                        $fields->value = '#' . str_replace("_", " ", $fields->value);
                    } else {
                        $fields->value = str_replace("_", " ", $fields->value);
                    }
                    if ($fields->name == 'parent_work_order' || $fields->name == 'your_job_reference_no') {
                        $projectinfo = $projectinfo . '
						<td >
							<table cellspacing="0" cellpadding="0">
								<tr>
									<td valign="top" width="30%">
										<table style="width: 120px" cellspacing="0" cellpadding="0">
											<tr>
												<td style="padding:0px 1px;width:120px;">' . ucfirst(trans($label)) . '</td>
											</tr>
										</table>
									</td>
									<td valign="top" width="70%">
										<table style="width: 200px" cellspacing="0" cellpadding="0">
											<tr>
												<td style="padding:0px 1px;font-weight: bold;width:200px;">' . $fields->value . '</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					';
                    }
                    /* check for check clear and notary seal option */
                    if ($fields->name == 'check_clear' || $fields->name == 'notary_seal') {
                        if ($fields->value)
                            $fields->value = "Optional Yes";
                        else
                            $fields->value = "Optional No";
                    }
                }
            }
            $projectinfo = $projectinfo . '</tr>';
            $i = 1;
            $fieldshtml = '';
            foreach ($notice_fields as $fields) {

                if ($fields->name != 'date_request' && $fields->name != 'parent_work_order' && $fields->name != 'your_job_reference_no') {
                    if ($i == 1) {
                        $fieldshtml = $fieldshtml . '<tr>';
                    }

                    $label = str_replace("_", " ", $fields->name);
                    $fieldshtml = $fieldshtml . '<td>
							<table>
								<tr>
									<td valign="top" width="30%">
										<table style="width: 120px" cellspacing="0" cellpadding="0">
											<tr>
												<td style="padding:0px 1px;width:120px;">' . ucfirst(trans($label)) . '</td>
											</tr>
										</table>
									</td>
									<td valign="top" width="70%">
										<table style="width: 200px;" cellspacing="0" cellpadding="0">
											<tr>
												<td style="padding:0px 1px;font-weight: bold;width:200px;">' . $fields->value . '</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>';
                    if ($i == 2) {
                        $fieldshtml = $fieldshtml . '</tr>';
                        $i = 0;
                    }
                    $i++;
                }
            }

            $projectinfo = $projectinfo . $fieldshtml;
        }

        $content = $content . $projectinfo;
        $content = $content . '	</table>
						</td>
					</tr><tr>
		<td colspan="4" valign="top" style="padding:0 5px;"><h5 style="text-transform: uppercase;border-top: 1px solid #000;border-bottom: 1px solid #000;padding: 2px 0;margin:0;">receipent information</h5></td>
		</tr>
                <tr>
			<td colspan="4" valign="top">
				<table cellspacing="0" cellpadding="0">
					<tr>
						<td valign="top" width="100px">
							<table style="width: 100px;" cellspacing="0" cellpadding="0">
								<tr>
									<td style="padding:0 5px;">Type</td>
								</tr>
							</table>
						</td>
						<td valign="top" width="150px">
							<table style="width: 150px;" cellspacing="0" cellpadding="0">
								<tr>
									<td style="padding:0 5px;">Company</td>
								</tr>
							</table>
						</td>
						<td valign="top" width="170px">
							<table style="width: 170px;" cellspacing="0" cellpadding="0">
								<tr>
									<td style="padding:0 5px;">Address</td>
								</tr>
							</table>
						</td>
						<td valign="top" width="60px">
							<table style="width: 60px;" cellspacing="0" cellpadding="0">
								<tr>
									<td style="padding:0 5px;">Phone</td>
								</tr>
							</table>
						</td>
						<td valign="top" width="80px">
							<table style="width: 80px;" cellspacing="0" cellpadding="0">
								<tr>
									<td style="padding:0 5px;">Fax</td>
								</tr>
							</table>
						</td>
					</tr>';

        $recipient_html = '';
        if (isset($recipients) && !empty($recipients)) {
            foreach ($recipients AS $k => $val) {
                $recipient_html = $recipient_html . '<tr>
						<td valign="top">
							<table style="width: 160px;" cellspacing="0" cellpadding="0">
								<tr>
									<td style="padding:0 5px;font-weight: bold;">' . $val->category->name . '</td>
								</tr>
							</table>
						</td>
						<td valign="top">
							<table style="width: 180px;" cellspacing="0" cellpadding="0">
								<tr>
									<td style="padding:0 5px;font-weight: bold;">' . $val->name . '</td>
								</tr>
							</table>
						</td>
						<td valign="top">
							<table style="width: 200px;" cellspacing="0" cellpadding="0">
								<tr>
									<td style="padding:0 5px;font-weight: bold;;word-wrap: break-word;">' . $val->address . ' ' . $val->city->name . ' ' . $val->state->name . ' ' . $val->zip . '</td>
								</tr>
							</table>
						</td>
						<td valign="top">
							<table style="width: 125px;" cellspacing="0" cellpadding="0">
								<tr>
									<td style="padding:0 5px;font-weight: bold;">' . $val->contact . '</td>
								</tr>
							</table>
						</td>
						<td valign="top">
							<table style="width: 125px;" cellspacing="0" cellpadding="0">
								<tr>
									<td style="padding:0 5px;font-weight: bold;">-</td>
								</tr>
							</table>
						</td>
					</tr>';
            }
        }

        $content = $content . $recipient_html;
        $content = $content . '</table>
                                            </td>
                                           </tr>
                                           <tr>
		<td colspan="4" valign="top" style="padding:0 5px;"><h5 style="text-transform: uppercase;border-top: 1px solid #000;border-bottom: 1px solid #000;padding: 5px 0;margin:0;">order notes</h5></td>
	</tr>';

        $notes_html = '';
        if (isset($notes) && !empty($notes)) {
            foreach ($notes AS $k => $val) {
                $notes_html = $notes_html . '<tr>
			<td colspan="4" valign="top">
				<table cellspacing="0" cellpadding="0">
					<tr>
						<td valign="top" style="width: 120px">
							<table cellspacing="0" cellpadding="0">
								<tr>
									<td valign="top" style="padding:0 5px;font-weight: bold;">' . date('m-d-Y', strtotime($val->created_at)) . '</td>
								</tr>
							</table>
						</td>
						<td valign="top" style="width: 120px">
							<table cellspacing="0" cellpadding="0">
								<tr>
									<td valign="top" style="padding:0 5px;font-weight: bold;">' . date('H:i A', strtotime($val->created_at)) . '</td>
								</tr>
							</table>
						</td>
						<td valign="top" style="width: 280px">
							<table cellspacing="0" cellpadding="0">
								<tr>
									<td valign="top" style="padding:0 5px;font-weight: bold;;text-align:justify">' . $val->note . '</td>
								</tr>
							</table>
						</td>
						<td valign="top"  style="width: 120px">
							<table cellspacing="0" cellpadding="0">
								<tr>

									<td valign="top" style="padding:0 5px;font-weight: bold;;">' . $account_manager_name . '</td>

								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>';
            }
        }
        $content = $content . $notes_html;

        $content = $content . '<tr>
		<td colspan="4" valign="top" style="padding:0 5px;"><h5 style="text-transform: uppercase;border-top: 1px solid #000;border-bottom: 1px solid #000;padding: 5px 0;margin:0;">order Correction</h5></td>
	</tr>';

        $correction_html = '';
        if (isset($correction) && !empty($correction)) {
            foreach ($correction AS $k => $val) {
                $correction_html = $correction_html . '<tr>
			<td colspan="4" valign="top">
				<table cellspacing="0" cellpadding="0">
					<tr>
						<td valign="top" style="width: 120px">
							<table cellspacing="0" cellpadding="0">
								<tr>
									<td valign="top" style="padding:0 5px;font-weight: bold;">' . date('m-d-Y', strtotime($val->created_at)) . '</td>
								</tr>
							</table>
						</td>
						<td valign="top" style="width: 120px">
							<table cellspacing="0" cellpadding="0">
								<tr>
									<td valign="top" style="padding:0 5px;font-weight: bold;">' . date('H:i A', strtotime($val->created_at)) . '</td>
								</tr>
							</table>
						</td>
						<td valign="top" style="width: 280px">
							<table cellspacing="0" cellpadding="0">
								<tr>
									<td valign="top" style="padding:0 5px;font-weight: bold;;text-align:justify">' . $val->correction . '</td>
								</tr>
							</table>
						</td>
						<td valign="top"  style="width: 120px">
							<table cellspacing="0" cellpadding="0">
								<tr>

									<td valign="top" style="padding:0 5px;font-weight: bold;;">' . $account_manager_name . '</td>

								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>';
            }
        }
        $content = $content . $correction_html;




        $content = $content . '<tr>
			<td colspan="4" valign="top" style="padding:0 5px;"><h5 style="text-transform: uppercase;border-top: 1px solid #000;border-bottom: 1px solid #000;padding: 5px 0;margin:0;">attached documents</h5></td>
		</tr>';
        $attachment_html = '';

        if (isset($attachement) && !empty($attachement)) {
            foreach ($attachement AS $k => $val) {
                $attachment_html = $attachment_html . '	<tr>
			<td colspan="4"  valign="top">
				<table>
					<tr>
						<td valign="top" style="width: 115px">
							<table cellspacing="0" cellpadding="0">
								<tr>
									<td valign="top" style="padding:0 5px;font-weight: bold;">' . date('m-d-Y', strtotime($val->created_at)) . '</td>
								</tr>
							</table>
						</td>
						<td valign="top" style="width: 115px">
							<table cellspacing="0" cellpadding="0">
								<tr>
									<td valign="top" style="padding:0 5px;font-weight: bold;">' . date('H:i A', strtotime($val->created_at)) . '</td>
								</tr>
							</table>
						</td>
						<td valign="top" style="width: 275px">
							<table cellspacing="0" cellpadding="0">
								<tr>
									<td valign="top" style="padding:0 5px;font-weight: bold;">' . $val->original_file_name . '</td>
								</tr>
							</table>
						</td>
						<td valign="top" style="width: 110px">
							<table cellspacing="0" cellpadding="0">
								<tr>
									<td valign="top" style="padding:0 5px;font-weight: bold;">' . $val->title . '</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>';
            }
        }

        $content = $content . $attachment_html;
        $content = $content . '</table>
                                      </body>
                                      </html>';
        //echo $content;exit;
        $pdf = PDF::loadHTML($content);

        $file_name = 'VW' . $work_order_id . '_' . uniqid() . '.pdf';


        return $pdf->stream($file_name);
    }

    function updateRecordingPendingSignStatus($status, $work_order_id) {
        try {
            $update_status = \App\Models\Cyo_WorkOrders::find($work_order_id);
            $update_status->status = $status;
            $update_status->save();
            return json_encode(['status' => 'success', 'message' => 'Status successfully updated']);
        } catch (Exception $e) {
            return json_encode(['status' => 'error', 'message' => 'Status not updated']);
        }
    }

    public function postViewWorkOrder(Request $request) {
        $fields = array_merge($request->except(['_token', 'work_order_id', 'notice_id', 'visibility',
                    'doc_input', 'file_name', 'attachments', 'remove_doc', 'continue']));

        //dd($fields);
        $fieldsData = [];
        $values = [];
        $i = 0;
        foreach ($fields as $key => $value) {
            $field = explode("_", $key);
            $fieldsData[$i] = $field['0'];
            $values[$i] = $value;
            $i++;
        }
        $work_order_id = $request->work_order_id;
        foreach ($fieldsData as $key => $value) {
            if ($values[$key] != NULL) {
                $workorderFieldsObj = \App\Models\Cyo_WorkOrder_fields::where('workorder_id', '=', $work_order_id)
                        ->where('notice_field_id', '=', $value)
                        ->first();

                if (isset($workorderFieldsObj) && !empty($workorderFieldsObj)) {

                    $workorderFieldsObj->value = $values[$key];
                    $workorderFieldsObj->save();
                }
            }
        }
       /* Session::flash('success', 'Your work # ' . $work_order_id . '  has been successfully submitted.<a href="' . url('customer/create-your-own/view/' . $work_order_id) . '">Click here to print.</a>');*/
        $notice = \App\Models\Notice::select('name', 'id', 'type','master_notice_id')->where('id', $request->notice_id)->get()->toArray();
        $data['recipients'] = \App\Models\Cyo_WorkOrder_Recipient::select()->where('work_order_id', $work_order_id)->get();
        $data['notice'] = $notice[0];
        $data['id'] = $request->work_order_id;
        $data['customer_details'] = Auth::user()->customer;

        $work_order = \App\Models\Cyo_WorkOrders::find($work_order_id);
        $data['work_order'] = $work_order;

        $account_manager_email = \App\Models\SecondaryDocument::getAccountManagerEmail(Auth::user()->customer->id);
        $admin_email = \App\Models\SecondaryDocument::getAdminEmail();
        /* if (isset($account_manager_email) && !empty($account_manager_email)) {
          $data['note_emails'] = [
          $admin_email->email,
          $account_manager_email->email
          ];
          } else {
          $data['note_emails'] = [
          $admin_email->email
          ];
          } */
        $result = \App\Models\Cyo_WorkOrders::select('state_id', 'customer_id', 'is_rescind')->join('notice_fields', 'notice_fields.notice_id', '=', 'cyo__work_orders.notice_id')->where('cyo__work_orders.id', $request->work_order_id)->limit(1)->get()->toArray(); //dd($result);
        $data['is_rescind'] = $result[0]['is_rescind'];
        $data['note_emails'] = get_user_notice_email($result[0]['customer_id']);
        // if (Auth::user()->hasRole('account-manager')) {
        $attachment = explode(',|,', trim($request->attachments[0],'|,'));
//            if (isset($attachment) && !empty($attachment)) {
        foreach ($attachment AS $k => $v) {
          $v = trim($v,'|,');
            if (isset($request->remove_doc) && !empty($request->remove_doc) && $request->remove_doc[0] != '') {
                $remove_doc = explode(',', $request->remove_doc[0]);

                $each_attachment = explode(',', $v);
                if ($each_attachment[0] != '') {
                    $true = !in_array($each_attachment[2], $remove_doc);
                } else {
                    $true = '';
                }
                if (isset($true) && $true != '') {
                    if ($v != '') {

                        $work_order_attachment_obj = new \App\Models\Cyo_WorkOrder_Attachment();
                        $work_order_attachment_obj->work_order_id = $request->work_order_id;
                        $work_order_attachment_obj->type = $each_attachment[0];
                        $work_order_attachment_obj->title = $each_attachment[1];
                        $work_order_attachment_obj->file_name = $each_attachment[2];
                        $work_order_attachment_obj->original_file_name = $each_attachment[3];
                        if (Auth::user()->hasRole('customer')) {
                            $work_order_attachment_obj->visibility = 0;
                        } else {

                            $work_order_attachment_obj->visibility = $each_attachment[4];
                        }
                        $work_order_attachment_obj->save();
                    }
                }
            } else {
                if ($v != '') {
                    $each_attachment = explode(',', $v);
                    $work_order_attachment_obj = new \App\Models\Cyo_WorkOrder_Attachment();
                    $work_order_attachment_obj->work_order_id = $request->work_order_id;
                    $work_order_attachment_obj->type = $each_attachment[0];
                    $work_order_attachment_obj->title = $each_attachment[1];
                    $work_order_attachment_obj->file_name = $each_attachment[2];
                    $work_order_attachment_obj->original_file_name = $each_attachment[3];
                    if (Auth::user()->hasRole('customer')) {
                        $work_order_attachment_obj->visibility = 0;
                    } else {

                        $work_order_attachment_obj->visibility = $each_attachment[4];
                    }
                    $work_order_attachment_obj->save();
                }
            }
        }
//            }
        //   }
        if (Auth::user()->hasRole('customer')) {
            $data['attachment'] = \App\Models\Cyo_WorkOrder_Attachment::where('work_order_id', '=', $request->work_order_id)
                    ->where('visibility', '=', 0)
                    ->get();

            $data['notes'] = \App\Models\Cyo_WorkOrder_Notes::leftjoin('users', 'users.id', '=', 'cyo__work_order__notes.user_id')->where('customer_id', '=', Auth::user()->customer->id)
                    ->where('work_order_id', '=', $request->work_order_id)
                    ->where('visibility', '=', 0)
                    ->orderBy('cyo__work_order__notes.id', 'DESC')
                    ->get();
            $data['corrections'] = \App\Models\Cyo_WorkOrder_Corrections::leftjoin('users', 'users.id', '=', 'cyo__work_order__corrections.user_id')->where('customer_id', '=', Auth::user()->customer->id)
                    ->where('cyo__work_order__corrections.work_order_id', '=', $request->work_order_id)
                    ->where('cyo__work_order__corrections.visibility', '=', 0)
                    ->orderBy('cyo__work_order__corrections.id', 'DESC')
                    ->get();
        } else {
            $data['attachment'] = \App\Models\Cyo_WorkOrder_Attachment::where('work_order_id', '=', $request->work_order_id)
                    ->get();

            $data['notes'] = \App\Models\Cyo_WorkOrder_Notes::leftjoin('users', 'users.id', '=', 'cyo__work_order__notes.user_id')->where('customer_id', '=', Auth::user()->customer->id)
                    ->where('work_order_id', '=', $request->work_order_id)
                    ->orderBy('cyo__work_order__notes.id', 'DESC')
                    ->get();
            $data['corrections'] = \App\Models\Cyo_WorkOrder_Corrections::leftjoin('users', 'users.id', '=', 'cyo__work_order__corrections.user_id')->where('customer_id', '=', Auth::user()->customer->id)
                    ->where('cyo__work_order__corrections.work_order_id', '=', $request->work_order_id)
                    ->orderBy('cyo__work_order__corrections.id', 'DESC')
                    ->get();
        }
        if ($notice[0]['type'] == 2) {
            if ($request->continue == 'Continue') {
                $data['tab'] = 'recipients';
                $data['notice_fields_section1'] = \App\Models\NoticeField::select()->join('cyo__work_order_fields', 'cyo__work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('cyo__work_order_fields.workorder_id', $request->work_order_id)->where('cyo__work_order_fields.notice_id', $request->notice_id)->where('notice_fields.section', 1)->orderBy('sort_order', 'asc')->get();
                if (isset($data['notice_fields_section1']) && !empty($data['notice_fields_section1'])) {
                    foreach ($data['notice_fields_section1'] AS $key => $value) {
                        if ($value->name == 'enter_into_agreement' ||
                                $value->name == 'last_date_of_labor_service_furnished' || $value->name == 'job_start_date') {
                            if (isset($value) && $value != NULL && $value != '') {
                                $value->value = date('m-d-Y', strtotime($value->value));
//                          $date = \DateTime::createFromFormat("m-d-Y", $value->value);
//                           
//                            $date = $date->format('m-d-Y'); 
//                            $value->value = $date;
                            }
                        } elseif ($value->name == 'county' || $value->name == 'notary_county' || $value->name == 'recorded_county' ||
                                $value->name == 'project_county') {
                            if (isset($value) && $value != NULL) {
                                $county = \App\Models\City::find($value->value);
                                if (isset($county) && !empty($county)) {
                                    $value->value = $county->county;
                                }
                            }
                        } elseif ($value->name == 'notary_state' || $value->name == 'recorded_state') {
                            if (isset($value) && $value != NULL) {
                                $state = \App\Models\State::find($value->value);
                                if (isset($state) && !empty($state)) {
                                    $value->value = $county->name;
                                }
                            }
                        } elseif ($value->name == 'your_role') {
                            if (isset($value) && $value != NULL) {
                                $category = \App\Models\ProjectRoles::find($value->value);
                                $value->value = $category->name;
                            }
                        } elseif ($value->name == 'project_type') {
                            if (isset($value) && $value != NULL) {
                                $project_type = \App\Models\ProjectType::find($value->value);
                                $value->value = $project_type->type;
                            }
                        }
                        if ($value->name == 'parent_work_order') {
                            $parent_work_order = $value->value;
                            $data['parent_work_order'] = $parent_work_order;
                        }
                    }
                }

                $data['notice_fields_section2'] = \App\Models\NoticeField::select()->join('cyo__work_order_fields', 'cyo__work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('cyo__work_order_fields.notice_id', $request->notice_id)->where('cyo__work_order_fields.workorder_id', $request->work_order_id)->where('notice_fields.section', 2)->orderBy('sort_order', 'asc')->get();
                return view('customer.create_your_own.view_soft_notices', $data);
            }
        } else {
            if ($request->continue == 'Continue') {
                $data['tab'] = 'recipients';
                $data['notice_fields'] = \App\Models\NoticeField::select()->join('cyo__work_order_fields', 'cyo__work_order_fields.notice_field_id', '=', 'notice_fields.id')->where('state_id', $result[0]['state_id'])->where('cyo__work_order_fields.notice_id', $request->notice_id)->where('cyo__work_order_fields.workorder_id', $request->work_order_id)->orderBy('sort_order', 'asc')->get();

                if (isset($data['notice_fields']) && !empty($data['notice_fields'])) {
                    foreach ($data['notice_fields'] AS $key => $value) {
                        if ($value->name == 'enter_into_agreement' ||
                                $value->name == 'last_date_of_labor_service_furnished' || $value->name == 'job_start_date') {
                            if (isset($value) && $value != NULL && $value != '') {
                                $value->value = date('m-d-Y', strtotime($value->value));
//                          $date = \DateTime::createFromFormat("m-d-Y", $value->value);
//                           
//                            $date = $date->format('m-d-Y'); 
//                            $value->value = $date;
                            }
                        } elseif ($value->name == 'county' || $value->name == 'notary_county' || $value->name == 'recorded_county' ||
                                $value->name == 'project_county') {
                            if (isset($value) && $value != NULL) {
                                $county = \App\Models\City::find($value->value);
                                if (isset($county) && !empty($county)) {
                                    $value->value = $county->county;
                                }
                            }
                        } elseif ($value->name == 'notary_state' || $value->name == 'recorded_state') {
                            if (isset($value) && $value != NULL) {
                                $state = \App\Models\State::find($value->value);
                                if (isset($state) && !empty($state)) {
                                    $value->value = $county->name;
                                }
                            }
                        } elseif ($value->name == 'your_role') {
                            if (isset($value) && $value != NULL) {
                                $category = \App\Models\ProjectRoles::find($value->value);
                                $value->value = $category->name;
                            }
                        } elseif ($value->name == 'project_type') {
                            if (isset($value) && $value != NULL) {
                                $project_type = \App\Models\ProjectType::find($value->value);
                                $value->value = $project_type->type;
                            }
                        }
                        if ($value->name == 'parent_work_order') {
                            $parent_work_order = $value->value;
                            $data['parent_work_order'] = $parent_work_order;
                        }
                    }
                }
                return view('customer.create_your_own.view_hard_notices', $data);
            }
        }
    }

    public function  creditCard($id,$work_order_id) {  
        $data['id'] = $id;
        $data['work_order_id'] = $work_order_id;
        $data['user_cards'] = [];
        $data['amount'] = '';  
        $user = Auth::user();


        if($work_order_id != '')
        {  
             
            $result = \App\Models\Cyo_WorkOrders::where('id',$work_order_id)->get();
            
            $qbo_customer_id = $user->qbo_customer_id; ////$result[0]->customer_id;           
            $customer_id = $user->id; //$result[0]->customer_id;
            $notice_id = $result[0]->notice_id;            

            $package = \App\Models\Customer_subscription::where(['customer_id'=>$result[0]->customer_id,'status'=>1])->get();
                
            $notice = \App\Models\Notice::join('cyo__work_orders', 'cyo__work_orders.notice_id', '=', 'notices.id')
                            ->join('notice_fields', 'notice_fields.notice_id', '=', 'notices.id')
                            ->where(['cyo__work_orders.id' => $work_order_id])
                            ->get([\DB::raw('notice_fields.id as notice_field_id'), \DB::raw('cyo__work_orders.id as workorder_id'), \DB::raw('notice_fields.name as notice_field_name'), \DB::raw('notices.name as notice_name'), \DB::raw('notices.id as notice_id1'),\DB::raw('cyo__work_orders.order_no as order_no'),\DB::raw('notices.master_notice_id as master_notice_id')])
                            ->toArray();                                
                             
            foreach ($notice as $key => $val) {                   
                    
                if($val['master_notice_id'] == config('constants.MASTER_NOTICE')['COL']['ID'] || $val['master_notice_id'] == config('constants.MASTER_NOTICE')['NPN']['ID'] || $val['master_notice_id'] == config('constants.MASTER_NOTICE')['SOL']['ID'])
                {  
                    $field_value = \App\Models\Cyo_WorkOrder_fields::where(['notice_field_id' => $val['notice_field_id'], 'workorder_id' => $val['workorder_id']])->pluck('value')->toArray();
                    $notice_fields[$val['notice_field_name']] = (isset($field_value[0])) ? $field_value[0] : '';
                    $unpaid_amount = '00';
                    if(isset($notice_fields['unpaid_amount'])) {
                        $unpaid_amount = $notice_fields['unpaid_amount'];
                    }

                    $notice_id_info = \App\Models\Pricing::join('notices','notices.id','pricings.notice_id')->where('pricings.lower_limit', '<=', $unpaid_amount)
                            ->where('pricings.upper_limit', '>=',$unpaid_amount)->where('notices.master_notice_id', $val['master_notice_id'])->where('package_id',$package[0]->package_id)->select('pricings.notice_id as customer_notice_id')->first();
                        
                    $customer_package_notice_id = $notice_id;
                    if (!empty($notice_id_info)){
                        $customer_package_notice_id = $notice_id_info['customer_notice_id'];
                    }
                    $customer_package = \App\Models\Customer_package::where(['customer_id' => $result[0]->customer_id, 'notice_id' => $customer_package_notice_id])->where('package_id',$package[0]->package_id)
                                        ->where('lower_limit', '<=', $unpaid_amount)
                                        ->where('upper_limit', '>=', $unpaid_amount)
                                        ->first();

                    if (empty($customer_package)) {
                        $customer_package = \App\Models\Pricing::where('notice_id',$customer_package_notice_id)->where('package_id',$package[0]->package_id)
                                            ->where('lower_limit', '<=', $unpaid_amount)
                                            ->where('upper_limit', '>=', $unpaid_amount)
                                            ->first();
                    }
                    $charges = $customer_package['charge']; 
                }                
            }
            $data['amount'] = ($charges)? $charges: 0;
        }  
        /*$user = Auth::user();
        $cards = \App\Models\UserCard::where(['user_id'=>$user->id])->get()->toArray();
            
        if($cards)
        {
            $qbo = new QuickBooksController();       
            $user_cards = $qbo->generateTokens(config('constants.QBO.PAYMENT_SCOPE'),'getDocSavedCards',['customer_id'=>$user->id,'qbo_customer_id'=>$user->qbo_customer_id,'card_id'=>$cards[0]['card_id']]);   
            if($user_cards['response'] == 'success' && !empty($user_cards['cards']))
            {
                $data['user_cards'] = $user_cards['cards'];
            }
        }  */      
        return view('customer.create_your_own.credit_card',$data);
    }    
    
    public function checkCreditCard(Request $request)
    {  
        //dd($request->input());
        $user = Auth::user();       
        if(isset($request->card_id) && $request->card_id != '')
        {               
            $card_details = [
                "amount" => 0,//$request->amount,
                "currency" => "USD",
                "context" => [
                    "mobile" => "false",
                    "isEcommerce" => "true"
                ]
            ];
            $card_details["cardOnFile"] = $request->card_id;                 
        }else{

            $validator = \Validator::make($request->all(),[
                'card_number' => 'required|ccn',
                'credit_card_date' => 'required|ccd',
                'security_code' => 'required|cvc',
            ],
            [
                'card_number.ccn' => 'Invalid card number',
                'credit_card_date.ccd' => 'Invalid card date',
                'security_code.cvc' =>'Invalid security code'
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput();
            }
            else
            {   
                $expiry_date = explode('/',$request->credit_card_date);
                $expiry_month = $expiry_date[0];
                $expiry_year = $expiry_date[1];
                $card_number = str_replace(' ','',$request->card_number);

                $card_details = [
                    "amount" => 0,//$charges
                    "currency" => "USD",
                    "context" => [
                        "mobile" => "false",
                        "isEcommerce" => "true"
                    ],
                    "card" => [ 
                        "number" => $card_number,
                        "expMonth" => $expiry_month,
                        "expYear" => $expiry_year,
                        "cvc" => $request->security_code,
                    ]
                ];
            }
        }               

        if($request->work_order_id != '')
        {  
             
            $result = \App\Models\Cyo_WorkOrders::where('id',$request->work_order_id)->get();
            
            $qbo_customer_id = $user->qbo_customer_id; ////$result[0]->customer_id;           
            $customer_id = $user->id; //$result[0]->customer_id;
            $notice_id = $result[0]->notice_id;            

            $package = \App\Models\Customer_subscription::where(['customer_id'=>$result[0]->customer_id,'status'=>1])->get();
                
            $notice = \App\Models\Notice::join('cyo__work_orders', 'cyo__work_orders.notice_id', '=', 'notices.id')
                            ->join('notice_fields', 'notice_fields.notice_id', '=', 'notices.id')
                            ->where(['cyo__work_orders.id' => $request->work_order_id])
                            ->get([\DB::raw('notice_fields.id as notice_field_id'), \DB::raw('cyo__work_orders.id as workorder_id'), \DB::raw('notice_fields.name as notice_field_name'), \DB::raw('notices.name as notice_name'), \DB::raw('notices.id as notice_id1'),\DB::raw('cyo__work_orders.order_no as order_no'),\DB::raw('notices.master_notice_id as master_notice_id')])
                            ->toArray();                                
            //dd($notice);                 
            foreach ($notice as $key => $val) {                   
                    
                if($val['master_notice_id'] == config('constants.MASTER_NOTICE')['COL']['ID'] || $val['master_notice_id'] == config('constants.MASTER_NOTICE')['NPN']['ID'] || $val['master_notice_id'] == config('constants.MASTER_NOTICE')['SOL']['ID'])
                {  
                    $field_value = \App\Models\Cyo_WorkOrder_fields::where(['notice_field_id' => $val['notice_field_id'], 'workorder_id' => $val['workorder_id']])->pluck('value')->toArray();
                    $notice_fields[$val['notice_field_name']] = (isset($field_value[0])) ? $field_value[0] : '';
                    $unpaid_amount = '00';
                    if(isset($notice_fields['unpaid_amount'])) {
                        $unpaid_amount = $notice_fields['unpaid_amount'];
                    }
                    
                    if(isset($notice_fields['unpaid_retainage_if_none_enter_zero'])) {
                        $unpaid_amount = $notice_fields['unpaid_retainage_if_none_enter_zero'];
                    }

                    $notice_id_info = \App\Models\Pricing::join('notices','notices.id','pricings.notice_id')->where('pricings.lower_limit', '<=', $unpaid_amount)
                            ->where('pricings.upper_limit', '>=',$unpaid_amount)->where('notices.master_notice_id', $val['master_notice_id'])->where('package_id',$package[0]->package_id)->select('pricings.notice_id as customer_notice_id')->first();
                        
                    $customer_package_notice_id = $notice_id;
                    if (!empty($notice_id_info)){
                        $customer_package_notice_id = $notice_id_info['customer_notice_id'];
                    }
                    $customer_package = \App\Models\Customer_package::where(['customer_id' => $result[0]->customer_id, 'notice_id' => $customer_package_notice_id])->where('package_id',$package[0]->package_id)
                                        ->where('lower_limit', '<=', $unpaid_amount)
                                        ->where('upper_limit', '>=', $unpaid_amount)
                                        ->first();

                    if (empty($customer_package)) {
                        $customer_package = \App\Models\Pricing::where('notice_id',$customer_package_notice_id)->where('package_id',$package[0]->package_id)
                                            ->where('lower_limit', '<=', $unpaid_amount)
                                            ->where('upper_limit', '>=', $unpaid_amount)
                                            ->first();
                    }                    
                    $charges = $customer_package['charge']; 
                   
                }                
            }

            if(empty($customer_package)){
               return redirect()->back()->with('errorMessage','Customer package can not found');
            }     

            $card_details['amount'] = $charges;
            if($card_details['amount']==0){
                $card_details['amount'] = '00';
            }            
            //dd($qbo_customer_id,'hiiiiiiiiiiiiiiii');

            if($qbo_customer_id != 0)
            {                 
                //Qbo addCard                
                $card_id = ($request->card_id)?$request->card_id:'';
                if($card_id == '')
                {   
                    //dd('Card id empty');
                    $card_details_arr = ['name'=>$request->credit_card_name,'number'=>$card_number,'expMonth'=>$expiry_month,'expYear'=>$expiry_year];
                    $qbo_card = new \App\Http\Controllers\QuickBooksController();       
                    $qbo_card_response = $qbo_card->generateTokens(config('constants.QBO.PAYMENT_SCOPE'),'addDocCard',['customer_id'=>$customer_id,'qbo_customer_id'=>$qbo_customer_id,'card_details'=>$card_details_arr]);
                }
                //dd('Card id',$qbo_card_response);

                //Qbo CreateCard                
                $qbo = new \App\Http\Controllers\QuickBooksController();       
                $response = $qbo->generateTokens(config('constants.QBO.PAYMENT_SCOPE'),'createDocPayment',['customer_id'=>$customer_id,'card_details'=>$card_details,'user_id'=>$customer_id,'card_id'=>$card_id,'scope'=>config('constants.QBO.PAYMENT_SCOPE'),'package_id'=>$package[0]->package_id]);
                if($response['response'] == 'success')                       
                {                            
                    if($package[0]->package_id == 2)
                    {
                        $start = date('Y-m-d');
                        $end = date('Y-m-d',strtotime('+1 months'));
                    }
                    else
                    {
                        $start = date('Y-m-d');
                        $end = date('Y-m-d',strtotime('+1 years'));
                    }
                    //$charges
                    $insert_array = ['work_order_id'=>$request->work_order_id,'customer_id'=>$result[0]->customer_id,'package_id'=>$package[0]->package_id,'charges'=>$card_details['amount'],'type'=>1,'status'=>'1','start_date'=>$start,'end_date'=>$end];
                        \App\Models\Cyo_Doc_Payment_History::create($insert_array);
                    
                    $this->updateRecordingPendingSignStatus(4,$request->work_order_id); 
                    $this->genratePdfInvoice($request->work_order_id,$customer_package,$result[0]->customer_id);
                   
                    return redirect()->back()->withSuccess('success');

                }else{
                    return redirect()->back()->with('errorMessage',$response['message']);                        
                }               
            }
            return redirect()->back(); 
        }
        return redirect()->back();        
    }

    public function genratePdfInvoice($workOrderId, $customer_package, $customer_id){       
        
        if(!empty($customer_package)){
            $customers = \App\Models\Customer::where('id',$customer_id)->get();
            $data['data'] = ['charge'=>$customer_package->charge, 'description'=>$customer_package->description, 'company_name'=>$customers[0]->company_name];
            $pdf = PDF::loadView('customer.create_your_own.invoice',$data);
            $fileName = 'invoice-'.$workOrderId.'-'.Str::random(8).'.pdf';
            Storage::disk('public_uploads')->put('pdf/invoice-pdf/'.$fileName, $pdf->output());
            $this->sendMailToCustomers($fileName,$customer_package->description);
            return $pdf->download($fileName); 
        }else{
            return false;
        }
    }    

    public function sendMailToCustomers($fileName,$description){        
        $user = Auth::User();      
        $email = $user->email;
        $data["title"] = "Invoice";
        $data["body"] = 'Invoice for generating '. $description .' document is attached with this email.';
        $data["file"] = $fileName;        
        Mail::to($email)->send(new CyoDocPaymentInvoiceMail($data));
    }
}
