<?php

namespace App\Http\Controllers\Auth;

use Alert;
use Auth;
use App\Models\City;
use App\Models\State;
use App\Models\Customer;
use Backpack\Base\app\Http\Controllers\Auth\MyAccountController as NewMyAccountController;
use Backpack\Base\app\Http\Requests\AccountInfoRequest;
use Backpack\Base\app\Http\Requests\ChangePasswordRequest;
use Illuminate\Support\Facades\Hash;
use App\Models\CustomerAgent;
use App\Models\CustomerRepresentative;
use App\Models\Branch;
use App\Models\Customer_subscription;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\QuickBooksController;

class MyAccountController extends NewMyAccountController {

    protected $data = [];

    /**
     * Show the user a form to change his personal information.
     */
    public function getAccountInfoForm() {
        $data['user_cards'] = [];
        $this->data['title'] = trans('backpack::base.my_account');
        $this->data['user'] = $this->guard()->user();
        $user_id = $this->data['user']['id'];
        $qbo_customer_id = $this->data['user']['qbo_customer_id'];
        
        $this->data['customer_subscription'] = [];
        if (Auth::user()->hasRole('customer')) {
            $data['customer'] = Auth::user()->customer;
            /* check  customer_subscriptions */
            $this->data['customer_subscription'] = \App\Models\Customer_subscription::
            with('package')->where('customer_id', Auth::user()->customer->id)->where('end_date', '>=', date('Y-m-d'))->orderBy('customer_subscriptions.id', 'desc')->first();
            //dd( $this->data['customer_subscription']->package->name);
            if (empty($this->data['customer_subscription'])) {
                /* redirect to pricing */
                Session::flash('warning', 'Your subscription for Create your own has expired, please renew ');
            } else {
                Session::flash('warning', '');
            }
            $mailing_city_name = \App\Models\City::where('id', '=', $data['customer']->mailing_city_id)->select('name')->first();
            $data['customer']->mailing_city_name = !empty($mailing_city_name) ? $mailing_city_name->name : '';
            $physical_city_name = \App\Models\City::where('id', '=', $data['customer']->physical_city_id)->select('name')->first();
            $data['customer']->physical_city_name = !empty($physical_city_name) ? $physical_city_name->name : '';
            $this->data['customer'] = $data['customer'];
            $this->data['cities'] = City::select('id', 'name')->get();
            $this->data['states'] = State::select('id', 'name')->get();
            $this->data['customer_agents'] = CustomerAgent::where('customer_id', '=', Auth::user()->customer->id)->get();
            $this->data['customer_repres'] = CustomerRepresentative::where('customer_id', '=', Auth::user()->customer->id)->get();
            $branches = Branch::where('customer_id', '=', Auth::user()->customer->id)->get();
            if (isset($branches) && count($branches) > 0) {
                foreach ($branches AS $each_branch) {
                    $city_name = \App\Models\City::where('id', '=', $each_branch->city_id)->select('name')->first();
                    $each_branch->branch_city_name = ($city_name) ? $city_name->name : NULL;
                    $county_name = \App\Models\City::where('id', '=', $each_branch->country)->select('county')->first();
                    $each_branch->branch_county_name = ($county_name) ? $county_name->county : NULL;
                }
            }

            $this->data['branches'] = $branches;
            $this->data['pricing'] = \App\Models\Customer_package::join('notices','notices.id','customer_packages.notice_id')->whereHas('notice', function ($query) {
                        $query->where('customer_packages.status', 1);
                    })->whereHas('package', function ($query2) {
                        $query2->where('customer_packages.status', 1);
                    })->where(['customer_id' => Auth::user()->customer->id, 'package_id' => 1, 'customer_packages.status' => 1])->get();

            $this->data['main_pricing'] = \App\Models\Pricing::join('notices','notices.id','pricings.notice_id')->whereHas('notice', function ($query) {
                        $query->where('pricings.status', 1);
                    })->whereHas('package', function ($query2) {
                        $query2->where('pricings.status', 1);
                    })->where(['package_id' => 1, 'pricings.status' => 1, 'pricing_copied_id' => 0])->get();
            //dd($this->data['main_pricing']);
            $cards = \App\Models\UserCard::where(['user_id'=>$user_id])->get()->toArray();
            
            if($cards)
            {
                $qbo = new QuickBooksController();       
                $user_cards = $qbo->generateTokens(config('constants.QBO.PAYMENT_SCOPE'),'getSavedCards',['customer_id'=>$user_id,'qbo_customer_id'=>$qbo_customer_id,'card_id'=>$cards[0]['card_id']]); 
                if($user_cards['response'] == 'success' && !empty($user_cards['cards']))
                {
                    $this->data['user_cards'] = $user_cards['cards'];

                }
            }
        }
        
        return view('auth.customer_update_info', $this->data);
    }

    /**
     * Save the modified personal information for a user.
     */
    public function postAccountInfoForm(AccountInfoRequest $request) {

        //dd($request->all());
        $validation = Validator::make($request->all(), [
                    'name' => 'required|string|max:255',
                    'company_name' => 'required|regex:/^[a-zA-Z ]+$/u',
                    'contact_person' => 'required|regex:/^[a-zA-Z ]+$/u',
                    'mailing_address' => 'required|regex:/^[a-zA-Z ]+$/u',
                    'mailing_city' => 'required|exists:cities,id',
                    'mailing_state' => 'required|exists:states,id',
                    'mailing_zip' => 'required|numeric',
                    'physical_address' => 'required',
                    'physical_city' => 'required|exists:cities,id',
                    'physical_state' => 'required|exists:states,id',
                    'physical_zip' => 'required|numeric',
                    'office_number' => 'required|numeric',
                    'fax' => '',
                    'mobile_number' => 'numeric',
                    'no_of_offices' => 'required|numeric',
                    'company_email.*' => 'required|email',
                    'agent_first_name.*' => 'required|regex:/^[a-zA-Z ]+$/u',
                    'agent_last_name.*' => 'required|regex:/^[a-zA-Z ]+$/u',
                    'agent_title.*' => 'required|regex:/^[a-zA-Z ]+$/u',
                    'repres_contact_person.*' => 'required|regex:/^[a-zA-Z ]+$/u',
                    'repres_company_branch_name.*' => 'required|regex:/^[a-zA-Z ]+$/u',
                    'repres_email.*' => 'required|email'
                        ], [
                    'company_email.*' => 'Company email is invalid',
                    'agent_first_name.*' => 'Officer first name format is invalid',
                    'agent_last_name.*' => 'Officer last name format is invalid',
                    'agent_title.*' => 'Officer title format is invalid',
                    'repres_contact_person.*' => 'Representative contact person format is invalid',
                    'repres_company_branch_name.*' => 'Representative branch name format is invalid',
                    'repres_email.*' => 'Representative email format is invalid',
        ]);

        if ($validation->fails()) {//dd($validation);
            return redirect()->back()->withErrors($validation)->withInput($request->all());
        }

        $result1 = $this->guard()->user()->update($request->except(['_token']));
        if (Auth::user()->hasRole('customer')) {
            $input = $request->all();
            $emailArray = $request->company_email;
            $email = implode(",", $emailArray);
            $request->request->set('company_email', $email);
            $result2 = Auth::user()->customer()->update([
                'company_name' => $request->company_name,
                'contact_person' => $request->contact_person,
                'mailing_address' => $request->mailing_address,
                'mailing_city_id' => $request->mailing_city,
                'mailing_state_id' => $request->mailing_state,
                'mailing_zip' => $request->mailing_zip,
                'physical_address' => $request->physical_address,
                'physical_city_id' => $request->physical_city,
                'physical_state_id' => $request->physical_state,
                'physical_zip' => $request->physical_zip,
                'hear_about' => $request->hear_about,
                'office_number' => $request->office_number,
                'fax_number' => $request->fax,
                'mobile_number' => $request->mobile_number,
                'company_email' => $request->company_email,
                'no_of_offices' => $request->no_of_offices,
            ]);

            //Customer Agents
            $customerAgents = CustomerAgent::where('customer_id', '=', Auth::user()->id)->delete();

            if ((isset($input['agent_first_name'])) && $input['agent_first_name'][0] != NULL) {
                foreach ($input['agent_first_name'] as $key => $val) {
                    CustomerAgent::create([
                        'customer_id' => Auth::user()->id,
                        'first_name' => $input['agent_first_name'][$key],
                        'last_name' => $input['agent_last_name'][$key],
                        'title' => $input['agent_title'][$key]
                    ]);
                }
            }

            //Customer repreentative
            $customerRepresentatives = CustomerRepresentative::where('customer_id', '=', Auth::user()->id)->delete();

            //Representative Details
            if ((isset($input['repres_contact_person'])) && $input['repres_contact_person'][0] != NULL) {
                foreach ($input['repres_contact_person'] as $key => $val) {
                    if(!empty($input['repres_contact_person'][$key]) || !empty($input['repres_company_branch_name'][$key]) || !empty($input['repres_email'][$key]) || !empty($input['repres_mobile_number'][$key])) {
                        CustomerRepresentative::create([
                            'customer_id' => Auth::user()->id,
                            'contact_person' => $input['repres_contact_person'][$key],
                            'branch_name' => $input['repres_company_branch_name'][$key],
                            'email' => $input['repres_email'][$key],
                            'mobile_number' => $input['repres_mobile_number'][$key]
                        ]);
                    }
                }
            }
            //Customer branches
            $customerBranches = Branch::where('customer_id', '=', Auth::user()->id)->delete();

            //Branch Details
            if ((isset($input['branch_name'])) && $input['branch_name'][0] != NULL) {
                foreach ($input['branch_name'] as $key => $val) {
                    Branch::create([
                        'customer_id' => Auth::user()->id,
                        'name' => $input['branch_name'][$key],
                        'contact_person' => $input['branch_contact_person'][$key],
                        'phone' => $input['branch_phone'][$key],
                        'email' => NULL,
                        'title' => $input['branch_title'][$key],
                        'first_name' => $input['branch_first_name'][$key],
                        'last_name' => $input['branch_last_name'][$key],
                        'address' => $input['branch_address'][$key],
                        'city_id' => $input['branch_city'][$key],
                        'state_id' => $input['branch_state'][$key],
                        'zip' => $input['branch_zip'][$key]
                    ]);
                }
            }
        }

        if ($result1 || $result2) {
            // Session::set('success',trans('backpack::base.account_updated'));
            return redirect()->back()->with('success', trans('backpack::base.account_updated'));
        } else {

            return redirect()->back()->with('error', trans('backpack::base.error_saving'));
        }
    }

    /**
     * Show the user a form to change his login password.
     */
    public function getChangePasswordForm() {
        $this->data['title'] = trans('backpack::base.my_account');
        $this->data['user'] = $this->guard()->user();

        return view('backpack::auth.account.change_password', $this->data);
    }

    /**
     * Save the new password for a user.
     */
    public function postChangePasswordForm(ChangePasswordRequest $request) {
//        dd($request);
        $user = $this->guard()->user();
        $user->password = Hash::make($request->new_password);

        if ($user->save()) {
//            redirect()->back()->with('success', 'Password successfully changed');  
            Alert::success(trans('backpack::base.account_updated'))->flash();
//         return \Redirect::back()->with('success','You have registered successfully');
//         \Session::flash('success','You have registered successfully');
//         return \Redirect::to('admin/edit-account-info');
        } else {
            return redirect()->back()->with('error', trans('backpack::base.error_saving'));
        }
//                return redirect()->back();
    }

    /**
     * Get the guard to be used for account manipulation.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard() {
        return Auth::guard();
    }

}
