<?php
namespace App\Http\Controllers\Auth;

use Alert;
use App\Models\User;
use App\Models\Customer;
use App\Models\City;
use App\Models\State;
use App\Models\CustomerAgent;
use App\Models\CustomerRepresentative;
use App\Models\Branch;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use App\Helpers\QuickBooksHelper;
use Illuminate\Support\Facades\Mail;
use App\Mail\RegistrationMailed;
use App\Mail\AdminRegistartionMail;
use LVR\CreditCard\CardCvc;
use LVR\CreditCard\Cards\Card;
use LVR\CreditCard\CardNumber;
use LVR\CreditCard\CardExpirationYear;
use LVR\CreditCard\CardExpirationMonth;
use LVR\CreditCard\Cards\ExpirationDateValidator;
use App\Http\Controllers\QuickBooksController;

class RegisterController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Register Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles the registration of new users as well as their
      | validation and creation. By default this controller uses a trait to
      | provide this functionality without requiring any additional code.
      |
     */

use RegistersUsers {
        register as traitRegister;
    }

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest');
        
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data) {
//        return Validator::make($data, [
//                    'name' => 'required|string|max:255',
//                    'email' => 'required|email|max:255|unique:users',
//                    'password' => 'required|string|min:6|confirmed',
//                    'company_name' => 'required|string',
//                    'contact_person' => 'required|string',
//                    'mailing_address' => 'required',
//                    'mailing_city' => 'required|exists:cities,id',
//                    'mailing_state' => 'required|exists:states,id',
//                    'mailing_zip' => 'required',
//                    'company_physical_address' => 'required',
//                    'physical_city' => 'required|exists:cities,id',
//                    'physical_state' => 'required|exists:states,id',
//                    'physical_zip' => 'required',
//                    'office_number' => 'required|numeric',
//                  //  'company_email' => 'required|emails|max:255',
//                    'agent_first_name' => 'required',
//                    'agent_last_name' => 'required',
//                    'agent_title' => 'required',
//                    'repres_contact_person' => 'required',
//                    'repres_company_branch_name' => 'required',
//                    'repres_email' => 'required',
//                    'no_of_offices' => 'required'
//        ]);
          return Validator::make($data,[
            'name' => 'required|string|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'company_name' => 'required|regex:/^[a-zA-Z ]+$/u',
            'contact_person' => 'required|regex:/^[a-zA-Z ]+$/u',
            'mailing_address' => 'required',
            'mailing_city' => 'required|exists:cities,id',
            'mailing_state' => 'required|exists:states,id',
            'mailing_zip' => 'required|numeric',
            'company_physical_address' => 'required',
            'physical_city' => 'required|exists:cities,id',
            'physical_state' => 'required|exists:states,id',
            'physical_zip' => 'required|numeric',
            'office_number' => 'required',
            'fax' => '',
//            'mobile_number' => 'numeric',
            'no_of_offices'=>'numeric',
            'company_email.*' => 'required|email',
            'agent_first_name.*' => 'required|regex:/^[a-zA-Z ]+$/u',
            'agent_last_name.*' => 'required|regex:/^[a-zA-Z ]+$/u',
            'agent_title.*' => 'required|regex:/^[a-zA-Z ]+$/u',
            'repres_contact_person.*' => 'required|regex:/^[a-zA-Z ]+$/u',
            'repres_company_branch_name.*'=> 'required|regex:/^[a-zA-Z ]+$/u',
            'repres_email.*' => 'required|email'
        ],[
            'company_email.*' => 'Company email is invalid',
            'agent_first_name.*' => 'Officer first name format is invalid',
            'agent_last_name.*' => 'Officer last name format is invalid',  
            'agent_title.*' => 'Officer title format is invalid', 
            'repres_contact_person.*' => 'Representative contact person format is invalid',
            'repres_company_branch_name.*' => 'Representative branch name format is invalid',  
            'repres_email.*' => 'Representative email format is invalid', 

        ]);
         
//        if($validation->fails()){//dd($validation);
//            return redirect()->back()->withErrors($validation)->withInput();
//        }
      
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data) {
        return User::create([
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'password' => bcrypt($data['password']),
                    'password_text' => $data['password']
        ]);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm() {

        $data['cities'] = City::select('id', 'name')->get();
        $data['states'] = State::select('id', 'name')->orderBy('name')->get();
        
        $data['countries'] = \App\Models\Country::select('id', 'name')->get();
        return view('auth.register', $data);
    }

    public function  creditCard() {
        //Create customer in quickbooks
        // $qbo = new QuickBooksController();       
        // $response = $qbo->generateTokens(config('constants.QBO.SCOPE'),'createInvoice',['customer_id'=>1]);
        // dd($response);
        return view('auth.credit_card');
    }   

    public function checkCreditCard(Request $request)
    {      
        $validator = Validator::make($request->all(),[
            'card_number' => 'required|ccn',
            'credit_card_date' => 'required|ccd',
            'security_code' => 'required|cvc',
        ],
        [
            'card_number.ccn' => 'Invalid card number',
            'credit_card_date.ccd' => 'Invalid card date',
            'security_code.cvc' =>'Invalid security code'
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        else
        {
            // store card for customer in quickbooks
            if(session('qbo_customer_id') != '')
            {                
                $qbo_customer_id = session('qbo_customer_id');
                $customer_id = session('customer_id');
                // $qbo_customer_id = 59;
                if($qbo_customer_id != 0)
                {
                    $expiry_date = explode('/',$request->credit_card_date);
                    $expiry_month = $expiry_date[0];
                    $expiry_year = $expiry_date[1];
                    $card_number = str_replace(' ','',$request->card_number);
                    $card_details = ['name'=>$request->credit_card_name,'number'=>$card_number,'expMonth'=>$expiry_month,'expYear'=>$expiry_year];
                    
                    $qbo = new QuickBooksController();       
                    $qbo_response = $qbo->generateTokens(config('constants.QBO.PAYMENT_SCOPE'),'addCard',['customer_id'=>$customer_id,'qbo_customer_id'=>$qbo_customer_id,'card_details'=>$card_details]);
                    return redirect('login');
                }
            }
            else
            {
                return redirect('login');
            }
        }
    }

    public function register(Request $request) {
    
         $validator = Validator::make($request->all(),[
            'name' => 'required|string|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'company_name' => 'required',
            'contact_person' => 'required',
            'mailing_address' => 'required',
            'mailing_city_value_id' => 'required',
            'state' => 'required|exists:states,id',
            'mailing_zip' => 'required|numeric',
            'company_physical_address' => 'required',
            'physical_city_value_id' => 'required',
            'physical_state' => 'required|exists:states,id',
            'physical_zip' => 'required|numeric',
            'office_number' => 'required',
            'fax' => '',
            'mobile_number' => '',
            'no_of_offices'=>'nullable|numeric',
            'company_email.0' => 'required|email',
            'agent_first_name.*' => 'required',
            'agent_last_name.*' => 'required',
            'agent_title.*' => 'required',
            'repres_contact_person.*' => 'nullable',
            'repres_company_branch_name.*'=> 'nullable',
            'repres_email.*' => 'nullable|email',
        ],[
            'company_email.*' => 'Company email is invalid',
            'agent_first_name.*' => 'Officer first name format is invalid',
            'agent_last_name.*' => 'Officer last name format is invalid',  
            'agent_title.*' => 'Officer title format is invalid', 
            'repres_contact_person.*' => 'Representative contact person format is invalid',
            'repres_email.*' => 'Representative email format is invalid', 

        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
        //dd($request->all());
        if(!is_numeric($request->mailing_city_value_id)){
            $city = \App\Models\City::firstOrNew(array('name' => $request->mailing_city_value_id));
            //$city = new \App\Models\City();
            $city->name = $request->mailing_city_value_id;
            $city->state_id = $request->state;
            $city->zip_code = $request->mailing_zip;
            $city->save();
            $request->request->set('city',$city->id);
        }
         if(!is_numeric($request->physical_city_value_id)){
            $city = \App\Models\City::firstOrNew(array('name' => $request->physical_city_value_id));
            //$city = new \App\Models\City();
            $city->name = $request->physical_city_value_id;
            $city->state_id = $request->physical_state;
            $city->zip_code = $request->physical_zip;
            $city->save();
            $request->request->set('physical_city',$city->id);
        }
        /*$request->request->set('city', $request->mailing_city_value_id);*/
       /* $request->request->set('physical_city', $request->physical_city_value_id);*/
        $request->request->set('branch_city', $request->branch_city_ids[0]);
        $request->request->set('branch_country', $request->branch_county_ids[0]);
  
        event(new Registered($user = $this->create($request->all())));
        $input = $request->all();
        $emailArray = $input['company_email'];
        $email = implode(",", $emailArray);
        // Assign Role and Mke entry in customer 
       
        $user->assignRole('customer');
        $customer = $user->customer()->create([
            'company_name' => $request->company_name,
            'contact_person' => $request->contact_person,
            'mailing_address' => $request->mailing_address,
            'mailing_city_id' => $request->mailing_city_value_id,
            'mailing_state_id' => $request->state,
            'mailing_zip' => $request->mailing_zip,
            'physical_address' => $request->company_physical_address,
            'physical_city_id' => $request->physical_city_value_id,
            'physical_state_id' => $request->physical_state,
            'physical_zip' => $request->physical_zip,
            'hear_about' => $request->hear_about,
            'office_number' => $request->office_number,
            'fax_number' => $request->fax,
//            'mobile_number' => $request->mobile_number,
            'company_email' => $email,
            'no_of_offices' => $request->no_of_offices,
        ]);
        //Agent Details
        if ((isset($input['agent_first_name'])) && $input['agent_first_name'][0] != NULL) {
            foreach ($input['agent_first_name'] as $key => $val) {
                CustomerAgent::create([
                    'customer_id' => $customer->id,
                    'first_name' => $input['agent_first_name'][$key],
                    'last_name' => $input['agent_last_name'][$key],
                    'title' => $input['agent_title'][$key]
                ]);
            }
        }


        //Representative Details
        if ((isset($input['repres_contact_person'])) && $input['repres_contact_person'][0] != NULL) {
            foreach ($input['repres_contact_person'] as $key => $val) {
                CustomerRepresentative::create([
                    'customer_id' => $customer->id,
                    'contact_person' => $input['repres_contact_person'][$key],
                    'branch_name' => $input['repres_company_branch_name'][$key],
                    'email' => $input['repres_email'][$key],
                    'mobile_number' => $input['repres_mobile_number'][$key]
                ]);
            }
        }
        if(count($request->branch_city_ids)> 1){
        $branch_city = explode(',',$request->branch_city);
        }
        if(count($request->branch_county_ids)> 1){
        $branch_country = explode(',',$request->branch_country);
        }
            
        //Branch Details
         if ((isset($input['branch_name'])) && $input['branch_name'][0] != NULL) {
            foreach ($input['branch_name'] as $key => $val) { ;
                Branch::create([
                    'customer_id' => $customer->id,
                    'name' => $input['branch_name'][$key],
                    'contact_person' => $input['branch_contact_person'][$key],
                    'phone' => $input['branch_phone'][$key],
                    'email' => NULL,
                    'title' => $input['branch_title'][$key],
                    'first_name' =>$input['branch_first_name'][$key],
                    'last_name'=>$input['branch_last_name'][$key],
                    'address'=>$input['branch_address'][$key],
                    'city_id'=>(isset($branch_city))?$branch_city[$key]:$request->branch_city,
                    'state_id'=>$input['branch_state'][$key],
                    'country' => (isset($branch_country))?$branch_country[$key]:$request->branch_country,
                    'zip'=>$input['branch_zip'][$key]
                ]);
            }
        }
        $admin_email = \App\Models\Role::select('users.email','users.name')->join('role_users','role_users.role_id','roles.id')->join('users','users.id','role_users.user_id')->where('roles.name','admin')->get();
        $admin_email['thank_msg'] = '';
        $admin_email_id = $admin_email[0]['email'];
        Mail::to($admin_email_id)->send(new AdminRegistartionMail($customer,$admin_email));
        Mail::to($request->email)->send(new RegistrationMailed($customer));
        Mail::to(config('constants.TRANSACTION_EMAILS.SALES_EMAIL'))->send(new AdminRegistartionMail($customer,$admin_email));

        $admin_email['thank_msg'] = "Thank you for your business.";
        //Mail::to(config('constants.TRANSACTION_EMAILS.SALES_EMAIL'))->cc(config('constants.TRANSACTION_EMAILS.CLIENT_MAIL'))->send(new AdminRegistartionMail($customer,$admin_email,$thank_msg));
        Mail::to(config('constants.TRANSACTION_EMAILS.CLIENT_MAIL'))->send(new AdminRegistartionMail($customer,$admin_email));
        
       
        //Create customer in quickbooks
        $qbo = new QuickBooksController();       
        $qbo->generateTokens(config('constants.QBO.SCOPE'),'createCustomer',['customer_id'=>$user->id]);
        return \Redirect::to('credit-card')->with('customer_name',$request->name);
    }
    
    public function getCounties(Request $request){
        $query = $request->get('term','');
        
      
        $names = \DB::table('cities')->distinct('county')->where('county','LIKE',$query.'%')->groupBy('county')->get();
        $data=array();
        foreach ($names as $name) {
                $data[]=array('value'=>$name->county,'id'=>$name->id);
        }
        if(count($data))
             return $data;
        else
            return ['value'=>'No Result Found','id'=>''];
    }
}
