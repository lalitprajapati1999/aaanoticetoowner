<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use DB;
use Alert;
use Auth;
use Session;
use Hash;
use Illuminate\Support\Facades\Redirect;
class LoginController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | redirecting them to your home screen. The controller uses a trait
      | to conveniently provide its functionality to your applications.
      |
     */

use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('guest')->except('logout');
    }

    protected function redirectTo() {
        $user = \Auth::user();

        /*if ($user->can('customer-dashboard'))
            return redirect('customer/dashboard');*/
        if ($user->can('admin-dashboard'))
            return config('backpack.base.route_prefix', 'admin') . '/dashboard';
        else
            return redirect('/dashboard');
            // return property_exists($this, 'redirectTo') ? $this->redirectTo : '/dashboard';
       /* return property_exists($this, 'redirectTo') ? $this->redirectTo : '/customer/dashboard';*/
    }
    
    public function login(Request $request) {   
        $password = \App\Models\User::select('password')->where('email',$request->email)->get();
        if($password->isEmpty()){
           Session::flash('error','Invalid email-ID');
                        return redirect('login');
        }
        else  if(Hash::check($request->password, $password['0']->password))
            {
                $user = \App\Models\User::select('id','allow_secondary_network')->where('email', $request->email)->get();
                if (count($user) != 0) {
                    $user_id = $user->toArray()[0];
                    $role = DB::table('role_users')->where('user_id', $user_id['id'])->select('role_id')->get()->toArray();
                    $role_id = $role[0]->role_id;
                    if ($role_id == 4) {
                        $status = \App\Models\Customer::select('status')->where('user_id', $user_id['id'])->first()->toArray();
                        if ($status['status'] == 1) {     
                            $this->validateLogin($request);
                            if ($this->hasTooManyLoginAttempts($request)) {
                                $this->fireLockoutEvent($request);
                                return $this->sendLockoutResponse($request);
                            }

                            if ($this->attemptLogin($request)) {
                                if(\Session::get('subscription_package_selected') != '')
                                {
                                    $package_id = \Session::get('subscription_package_selected');
                                    $data['subscription_package'] = \App\Models\Customer_subscription::select('package_id')->where('status','1')->where('start_date', '<=', date('Y-m-d'))->where('end_date', '>=', date('Y-m-d'))->where('customer_id',Auth::user()->customer->id)->where('package_id',$package_id)->get();
                                    if($data['subscription_package']->isEmpty()){
                                      return redirect('/customer/subscription/'.$package_id.'/make-payment');  
                                      }else{
                                         return redirect::to('/home')->with('success', 'Already subscribed this package');
                                      }
                                    
                                }
                                else
                                {
                                    \Session::put('error','');
                                    return redirect('/home');
                                }                                
                            }
                            \Session::put('subscription_package_selected','');
                            $this->incrementLoginAttempts($request);

                            return $this->sendFailedLoginResponse($request);
                             }
                             else 
                             {
                                Session::flash('error','Your account need admin approval');
                                return redirect('login');
                             }
                    } else {
                        //To check admin and account manager account approved or not
                         $status = \App\Models\User::select('status')->where('id', $user_id['id'])->first()->toArray();
                        if($status['status'] == 0){
                             Session::flash('error','Your account need admin approval.');
                                return redirect('login');
                        }
                        //For account manager only
                        if ($role_id == 3) {
                           $user_id = $user->toArray()[0];
                           $allow_secondary_network_status = $user_id['allow_secondary_network'];
                          
                           //if want to access site from other location 
                           
                           if(env('NETWORK_IP_ADDRESS') != $_SERVER['REMOTE_ADDR'] ){
                               if($allow_secondary_network_status != 1 ){
                                 return redirect('login')->with('success', "You are not allowed to access site outside the office network. Please contact admin for approval.");  
                               }
                           }
                        }
                        $this->validateLogin($request);
                        if ($this->hasTooManyLoginAttempts($request)) {
                            $this->fireLockoutEvent($request);
                            return $this->sendLockoutResponse($request);
                        }

                        if ($this->attemptLogin($request)) {
                            return $this->sendLoginResponse($request)->with('message', 'You are successfully login');
                        }
                        $this->incrementLoginAttempts($request);

                        return $this->sendFailedLoginResponse($request);
                    }
                } else {
                    return redirect('login')->with('message', 'This email id is invalid');
                }
            } 
                    else
                      Session::flash('error','Invalid Password');
                                return redirect('login');

            
       
       
    }

}
