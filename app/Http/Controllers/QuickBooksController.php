<?php

namespace App\Http\Controllers;

use QuickBooksOnline\API\Facades\Customer;
use QuickBooksOnline\API\Facades\Invoice;
use Illuminate\Http\Request;
use App\Helpers\Client;
use App\Helpers\QuickBooksHelper;
use App\Models\QuickbooksToken;
use QuickBooksOnline\API\DataService\DataService;
use QuickBooksOnline\API\Core\Http\Serialization\XmlObjectSerializer;
use App\Models\Customer as Customer1;

class QuickBooksController {

    public function generateTokens($scope, $task = '', $data = []) {
        if ($task) {
            \DB::table('quickbooks_temp_data')->where(['customer_id' => $data['customer_id']])->delete();
            \DB::table('quickbooks_temp_data')->insert(['quickbooks_data' => json_encode($data), 'quickbooks_task' => $task, 'scope' => $scope, 'customer_id' => $data['customer_id']]);
            session(['customer_id' => $data['customer_id']]);
        }
        $auth_tokens = NULL;
        $saved_data = \DB::table('quickbooks_temp_data')->select()->where(['customer_id' => session('customer_id')])->first();
        //dd($saved_data);
        if(!empty($saved_data)){
            $quickbooks_data = json_decode($saved_data->quickbooks_data, true);
            //Check if token saved for the required scope
            $auth_tokens = QuickbooksToken::where(['scope' => $saved_data->scope])->first();
        }

        if (!empty($auth_tokens)) {
            $now = time();
            //check if already expired, then refresh token 
            if ((strtotime($auth_tokens->token_expires_at) > $now) === false) {
                $helper = new QuickBooksHelper();
                $refresh_data = ['refresh_token' => $auth_tokens->refresh_token, 'company_id' => $auth_tokens->company_id, 'customer_id' => 1, 'scope' => $saved_data->scope];
                $helper->refreshToken($refresh_data);
            }
            return $this->task($saved_data, $auth_tokens);
        } else {
            $authorizationRequestUrl = config('constants.QBO.AUTH_REQ_URL');
            $tokenEndPointUrl = config('constants.QBO.TOK_ENDPOINT_URL');
            $client_id = config('constants.QBO.CONSUMER_KEY');
            $client_secret = config('constants.QBO.CONSUMER_SECRET');
            $redirect_uri = config('constants.QBO.CALLBACK_URL');


            $response_type = 'code';
            $state = 'RandomState';
            $include_granted_scope = 'false';

            $grant_type = 'authorization_code';
            $base_url = url('/');

            $certFilePath = public_path().'/Certificate/cacert.pem';
            /*if(strpos($base_url,'localhost') || strpos($base_url,'http://127.0.0.1'))
            {
                $certFilePath = './public/Certificate/cacert.pem';
            }
            else
            {dd('hidf');
                $certFilePath = $_SERVER['DOCUMENT_ROOT'];
                $certFilePath .= "/public/Certificate/cacert.pem";
            }*/

            $client = new Client($client_id, $client_secret, $certFilePath);

            if (!isset($_GET["code"])) {
                /* Step 1
                  /*Do not use Curl, use header so it can redirect. Curl just download the content it does not redirect */
                $authUrl = $client->getAuthorizationURL($authorizationRequestUrl, $scope, $redirect_uri, $response_type, $state);
                \Session::save(); //this is important because without saving like this session data will be lost after redirection
                header("Location: " . $authUrl);
                exit();
            } else {
                $code = $_GET["code"];
                $responseState = $_GET['state'];
                $company_id = $_GET['realmId'];
                if (strcmp($state, $responseState) != 0) {
                    throw new Exception("The state is not correct from Intuit Server. Consider your app is hacked.");
                }
                $result = $client->getAccessToken($tokenEndPointUrl, $code, $redirect_uri, $grant_type);

                if (isset($result['error']) && $result['error'] != '') {
                    return redirect('register')->with('error', $result['error']);
                    ;
                } else {
                    $now = date('Y-m-d H:i:s');
                    $token_expiry = date('Y-m-d H:i:s', strtotime('+ ' . $result['expires_in'] . ' seconds'));
                    $refresh_expiry = date('Y-m-d H:i:s', strtotime('+ ' . $result['x_refresh_token_expires_in'] . ' seconds'));

                    $token_data = [
                        'access_token' => $result['access_token'],
                        'refresh_token' => $result['refresh_token'],
                        'token_expires_at' => $token_expiry,
                        'refresh_token_expires_at' => $refresh_expiry,
                        'updated_at' => $now,
                        'company_id' => $company_id,
                        'customer_id' => 1,
                        'scope' => $saved_data->scope
                    ];
                    QuickbooksToken::insert($token_data);
                    $auth_tokens = QuickbooksToken::where(['scope' => $saved_data->scope])->first();
                    return $this->task($saved_data, $auth_tokens);
                }
            }
        }
    }

    public function task($saved_data, $auth_tokens) {

        $task = $saved_data->quickbooks_task;
        $data = json_decode($saved_data->quickbooks_data, true);
        switch ($task) {
            case 'createCustomer':
                $customer_id = $data['customer_id'];
                $helper = new QuickBooksHelper();
                $response = $helper->createCustomer($data['customer_id'], $auth_tokens->access_token, $auth_tokens->refresh_token);
                return redirect('credit-card')->with($response['response'], $response['message']);
                break;
            case 'addCard':
                $helper = new QuickBooksHelper();
                $response = $helper->addCard($data['customer_id'], $data['card_details'], $auth_tokens->access_token);
                return redirect('login')->with($response['response'], $response['message']);
                break;
            case 'getSavedCards':
                $helper = new QuickBooksHelper();
                $data1 = ['card_id' => $data['card_id'], 'qbo_customer_id' => $data['qbo_customer_id'], 'access_token' => $auth_tokens->access_token, 'card_id' => $data['card_id'], 'customer_id' => $data['customer_id'], 'scope' => $saved_data->scope];
                $response = $helper->getSavedCards($data1);
                return $response;
                break;
            case 'createInvoice':
                $helper = new QuickBooksHelper();
                $response = $helper->createInvoice(['access_token' => $auth_tokens->access_token, 'refresh_token' => $auth_tokens->refresh_token, 'invoice_details' => $data]);
                return $response;
                break;
            case 'createCharge':
                $package_id = $data['package_id'];
                $helper = new QuickBooksHelper();
                $response = $helper->createCharge($data, $auth_tokens->access_token);
                return $response;
                break;
            case 'getProductDetails':
                $helper = new QuickBooksHelper();
                $data1 = ['product_id' => $data['product_id'], 'access_token' => $auth_tokens->access_token, 'customer_id' => $data['customer_id'], 'scope' => $saved_data->scope];
                $response = $helper->getProductDetails($data1);
                return $response;
                break;
            case 'deleteCard':
                $helper = new QuickBooksHelper();
                $response = $helper->deleteCard($data['qbo_customer_id'],$data['card_id'],$auth_tokens->access_token,$data['customer_id']);
            //$response = $helper->deleteCard($data1);
            return $response;

                break;
            case 'queryItem': 
                $helper = new QuickBooksHelper();
                $data1 = ['access_token' => $auth_tokens->access_token,  'scope' => $saved_data->scope, 'query' => $data['query']];
                $response = $helper->queryItem($data1);
                return $response;
                break;
            case 'addDocCard':
                $helper = new QuickBooksHelper();
                $response = $helper->addDocCard($data['customer_id'], $data['card_details'], $auth_tokens->access_token);
                return redirect('login')->with($response['response'], $response['message']);
                break;
            case 'getDocSavedCards':
                $helper = new QuickBooksHelper();
                $data1 = ['card_id' => $data['card_id'], 'qbo_customer_id' => $data['qbo_customer_id'], 'access_token' => $auth_tokens->access_token, 'card_id' => $data['card_id'], 'customer_id' => $data['customer_id'], 'scope' => $saved_data->scope];
                $response = $helper->getDocSavedCards($data1);
                return $response;
                break;        
            case 'createDocPayment':
                $package_id = $data['package_id'];
                $helper = new QuickBooksHelper();
                $response = $helper->createDocPayment($data, $auth_tokens->access_token);
                return $response;
                break;        
            default:
                return;
        }
        return;
    }

}
