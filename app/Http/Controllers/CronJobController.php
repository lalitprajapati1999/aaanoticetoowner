<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use DB;
use App\Mail\DuedateNotificationMail;
use Illuminate\Support\Facades\Storage;
class CronJobController extends Controller {

    /**
     * Email Notification of due date
     *
     * @return \Illuminate\Http\Response
     */
    public function dueDateNotification() {
        $WorkOrderFields = \App\Models\WorkOrderFields::select('work_orders.parent_id', 'work_orders.created_at', 'work_orders.user_id', 'work_orders.customer_id', 'work_orders.notice_id as notice_id', 'work_orders.status', 'work_orders.user_id', 'work_orders.account_manager_id', 'work_order_fields.workorder_id', 'work_order_fields.notice_id', DB::raw("(GROUP_CONCAT(notice_fields.name SEPARATOR '||')) as `field_names`"), DB::raw("(GROUP_CONCAT(value SEPARATOR '||')) as `field_values`"), 'users.email','work_orders.discontinue_reminder','users.name')
                        ->leftjoin('notice_fields', 'notice_fields.id', '=', 'work_order_fields.notice_field_id')
                        ->leftjoin('work_orders', 'work_orders.id', '=', 'work_order_fields.workorder_id')
                        ->leftjoin('users', 'users.id', '=', 'work_orders.user_id')
                        ->groupBy('work_order_fields.workorder_id', 'work_order_fields.notice_id')
                        ->get()->toArray();
        $result = [];
        if (isset($WorkOrderFields) && !empty($WorkOrderFields)) {
            foreach ($WorkOrderFields as $fields_data) {
                $field_names = explode('||', $fields_data['field_names']);
                $field_values = explode('||', $fields_data['field_values']);
                $field_names_values = array_combine($field_names, $field_values);
                $field_names_values['default'] = '';
                $result[] = array_merge($fields_data, $field_names_values);
            }
        }

      
        if (isset($result) && !empty($result)) {
            foreach ($result AS $k => $each_val) {
                
                 $notice_type = \App\Models\Notice::find($each_val['notice_id']);
                
                if ($notice_type->type == 2) {
                   
                    if (isset($each_val['job_start_date']) && $each_val['job_start_date'] != '') {
                       

                        $todays_date = date('m/d/Y');
                      
                        $days65 = 65 . 'days';
                        $days75 = 75 . 'days';
                        $days80 = 80 . 'days';
                        $days60 = 60 . 'days';
                  
                        $days65_date = date('m/d/Y', strtotime('+ ' . $days65, strtotime($each_val['job_start_date'])));
                        $days75_date = date('m/d/Y', strtotime('+ ' . $days75, strtotime($each_val['job_start_date'])));
                        $days80_date = date('m/d/Y', strtotime('+ ' . $days80, strtotime($each_val['job_start_date'])));
                        $days60_date = date('m/d/Y', strtotime('+ ' . $days60, strtotime($each_val['job_start_date'])));
                         
                         $work_order_id = $each_val['workorder_id'];
                        $each_val['stopnotificationurl']= url("dueDate/discontinueReminder/$work_order_id");
                        
                        if (!$each_val['discontinue_reminder']) {


                            if (isset($each_val['last_date_on_the_job']) && $each_val['last_date_on_the_job'] != '') {
                                //if last job entered
                                
                                if ($todays_date == $days65_date || $todays_date == $days75_date || $todays_date == $days80_date) {
                                    
                                    if(isset($each_val['email']) && $each_val['email'] !=''){
                                   Mail::to($each_val['email'])->send(new DuedateNotificationMail($each_val));
                                    }
                                }
                            } else {
                                //if last job is not enetered
                               
                                $format_todays_date = date('Y-m-d',strtotime($todays_date));
                                $date1=date_create($format_todays_date);
                               
                                $format_60days_date = date('Y-m-d',strtotime($days60_date));
                                $date2=date_create($format_60days_date);
                                
                                //Calculate date diff between todays and 60 days form job start date
                                $diff=date_diff($date1,$date2);
                                $diff_days = $diff->format("%a");
                           
                                    if($todays_date == $days60_date){
                                         if(isset($each_val['email']) && $each_val['email'] !=''){
                                            Mail::to($each_val['email'])->send(new DuedateNotificationMail($each_val));
                                             }
                                    }else{
                                        if($todays_date > $days60_date && $diff_days % 30 == 0){
                                           if(isset($each_val['email']) && $each_val['email'] !=''){
                                            Mail::to($each_val['email'])->send(new DuedateNotificationMail($each_val));
                                             }
                                        }
                                    }
                                 
                                       
                            }
                        }
                    }
                }else{
                   if (isset($each_val['clerk_of_court_recorded_date']) && $each_val['clerk_of_court_recorded_date'] != '') {
                     
                       $todays_date = date('m/d/Y');
                        $work_order_id = $each_val['workorder_id'];
                        $each_val['stopnotificationurl']= url("dueDate/discontinueReminder/$work_order_id");
                        $tenmonth = date('m/d/Y', strtotime("+10 months", strtotime($each_val['clerk_of_court_recorded_date'])));
                        $elevenmonth =  date('m/d/Y', strtotime("+11 months", strtotime($each_val['clerk_of_court_recorded_date'])));
                         
                        if (!$each_val['discontinue_reminder']) {
                            if ($todays_date == $tenmonth || $todays_date == $elevenmonth) {
                                    
                                    if(isset($each_val['email']) && $each_val['email'] !=''){
                                   
                                      
                                   Mail::to($each_val['email'])->send(new DuedateClaimLienNotificationMail($each_val));
                                    }
                                }
                        }
                       
                   }
                }
            }
        }
    }
    
    public function discontinueReminder($work_order_id){
        
        $workorderObj = \App\Models\WorkOrder::find($work_order_id);
        $workorderObj->discontinue_reminder = 1;
        $workorderObj->save();
        return view('stop_notification_template');
       
    }
    /**
     * Email Notification of due date
     *
     * @return \Illuminate\Http\Response
     */
    public function createyourowndueDateNotification() {
        
         $WorkOrderFields = \App\Models\Cyo_WorkOrder_fields::select('cyo__work_orders.parent_id', 'cyo__work_orders.created_at', 'cyo__work_orders.user_id', 'cyo__work_orders.customer_id', 'cyo__work_orders.notice_id as notice_id', 'usr.name as account_manager_name', 'cyo__work_orders.status', 'cyo__work_orders.user_id', 'cyo__work_orders.account_manager_id', 'cyo__work_order_fields.workorder_id', 'cyo__work_order_fields.notice_id', DB::raw("(GROUP_CONCAT(notice_fields.name SEPARATOR '||')) as `field_names`"), DB::raw("(GROUP_CONCAT(value SEPARATOR '||')) as `field_values`"), 'users.email','cyo__work_orders.discontinue_reminder','users.name')
                                            ->leftjoin('notice_fields', 'notice_fields.id', '=', 'cyo__work_order_fields.notice_field_id')
                                            ->leftjoin('cyo__work_orders', 'cyo__work_orders.id', '=', 'cyo__work_order_fields.workorder_id')
                                            ->leftjoin('users', 'users.id', '=', 'cyo__work_orders.user_id')
                                            ->leftjoin('users as usr', 'usr.id', '=', 'cyo__work_orders.account_manager_id')
                                            ->groupBy('cyo__work_order_fields.workorder_id', 'cyo__work_order_fields.notice_id')
                                             ->get()->toArray();

        
        
        $result = [];
        if (isset($WorkOrderFields) && !empty($WorkOrderFields)) {
            foreach ($WorkOrderFields as $fields_data) {
                $field_names = explode('||', $fields_data['field_names']);
                $field_values = explode('||', $fields_data['field_values']);
                $field_names_values = array_combine($field_names, $field_values);
                $field_names_values['default'] = '';
                $result[] = array_merge($fields_data, $field_names_values);
            }
        }

      
        if (isset($result) && !empty($result)) {
            foreach ($result AS $k => $each_val) {
                
                 $notice_type = \App\Models\Notice::find($each_val['notice_id']);
                
                if ($notice_type->type == 2) {
                   
                    if (isset($each_val['job_start_date']) && $each_val['job_start_date'] != '') {
                       

                        $todays_date = date('m/d/Y');
                      
                        $days65 = 65 . 'days';
                        $days75 = 75 . 'days';
                        $days80 = 80 . 'days';
                        $days60 = 60 . 'days';
                  
                        $days65_date = date('m/d/Y', strtotime('+ ' . $days65, strtotime($each_val['job_start_date'])));
                        $days65_date = $todays_date;
                        $days75_date = date('m/d/Y', strtotime('+ ' . $days75, strtotime($each_val['job_start_date'])));
                        $days80_date = date('m/d/Y', strtotime('+ ' . $days80, strtotime($each_val['job_start_date'])));
                        $days60_date = date('m/d/Y', strtotime('+ ' . $days60, strtotime($each_val['job_start_date'])));
                        $work_order_id = $each_val['workorder_id'];
                        $each_val['stopnotificationurl']= url("create_your_own/dueDate/discontinueReminder/$work_order_id");
                        
                        if (!$each_val['discontinue_reminder']) {


                            if (isset($each_val['last_date_on_the_job']) && $each_val['last_date_on_the_job'] != '') {
                                //if last job entered
                                
                                if ($todays_date == $days65_date || $todays_date == $days75_date || $todays_date == $days80_date) {
                                    
                                    if(isset($each_val['email']) && $each_val['email'] !=''){
                                   Mail::to($each_val['email'])->send(new DuedateNotificationMail($each_val));
                                    }
                                }
                            } else {
                                //if last job is not enetered
                               
                                $format_todays_date = date('Y-m-d',strtotime($todays_date));
                                $date1=date_create($format_todays_date);
                               
                                $format_60days_date = date('Y-m-d',strtotime($days60_date));
                                $date2=date_create($format_60days_date);
                                
                                //Calculate date diff between todays and 60 days form job start date
                                $diff=date_diff($date1,$date2);
                                $diff_days = $diff->format("%a");
                           
                                    if($todays_date == $days60_date){
                                         if(isset($each_val['email']) && $each_val['email'] !=''){
                                            Mail::to($each_val['email'])->send(new DuedateNotificationMail($each_val));
                                             }
                                    }else{
                                        if($todays_date > $days60_date && $diff_days % 30 == 0){
                                           if(isset($each_val['email']) && $each_val['email'] !=''){
                                            Mail::to($each_val['email'])->send(new DuedateNotificationMail($each_val));
                                             }
                                        }
                                    }
                                 
                                       
                            }
                        }
                    }
                }
            }
        }
    }
    
    public function createyourowndiscontinueReminder($work_order_id){
        
        $workorderObj = \App\Models\Cyo_WorkOrders::find($work_order_id);
        $workorderObj->discontinue_reminder = 1;
        $workorderObj->save();
        return view('stop_notification_template');
       
    }
    
//    public function saveLabel(){
//       
//        $url = 'https://swsim.testing.stamps.com/Label/Label2.ashx/label-200.png?AQAAAJfw6rbOUdYI0WHJnP-H2jeEkrx94bIgMPSmYJV4nG1Sz2sTQRR-s91NQtGkWozVKkx_IAka2c1uNtkKhnWTpqltErOpnrStyaYupNmYRuxFmA0VpF4E8SAUtAheelNQsIhCxBxU9JQ_QVAvuXnSddJcBH3M8OZ773vzzXtMzJiCsbiLDCL4yxg49m8IEFnvPN2e-fQu-dgqjMze_1hAiHy4uu_u19_b2sbPqVYx7lTdYPv0a2atZlZXEtbqorQuH_Y1J6jMsKqqOGM1zKKBGxbO3qwadcdxJiVewJpRbRh1nLNM6rFm1osV4xTWb5gUCRFJobwDaqWxvGr18nqtTm9fc09XrLpZWubEcJQXHGdkqHmG6rjWSmW6aAkO5NVMKonDEs_jEHVKLIhT-WQyM5Oem8P5BOW4LqX1RHaemS9wESUqCyxPDYBmnCMPSL_3bvzoAEEwCqQvxTI2e4Xu4x6yyCHHAeDlsN5NfUb3mm8ASoOw0RXR7SUEl3-FaD3bm97FNgeSGOZfrrz2dl4NlUMHUf60f-n77k75WfvHtx32yfOT118EtypaZDLANUcRfHl_axgD8SuSpPCCEFOUiCyJskjFZCWWBDLe2pTC_N6zW5v_Z00D8S3oOR0X8qp2Pp1J4YkxL4G3TPvRhY5nnB7h4VYn17mjuwmyDtloAFyM7Z6lKMDYniBjM1nG3p8DgqJngfSmE9Y40u8L4AQQWGD3IMeSczTiB-LtTYPnRV6UhUhMYljS-0l_ADfXsLI=';
//        $contents = file_get_contents($url);
//        $name = substr($url, strrpos($url, '/') + 1);
//        $name = 'demo';
//        Storage::put($name, $contents);
//    }
}
