<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use QuickBooksOnline\API\DataService\DataService;
use App\Models\QuickbooksToken;
use App\Models\OAuth2InvoiceResponse;

class WebhookQbController extends Controller
{
    public function __construct(OAuth2InvoiceResponse $OAuth2InvoiceResponse)
    {    
        $this->OAuth2InvoiceResponse = $OAuth2InvoiceResponse;
    }

    public function index(Request $request)
    {   
        $file_name = './new_test_webhook.txt';
        $this->fp = fopen($file_name,'a');
        
        $resultEncodedData = json_encode($request->all());

        fwrite($this->fp, "\n".$resultEncodedData);

        // $data = $request->input('data');
        $data_arr = $request->input('eventNotifications');
        
        // $data_arr = $this->json_split_objects($data);

        $qbo_oauth_arr = [];
        $qbo_oauth_obj = QuickbooksToken::where('scope','com.intuit.quickbooks.accounting')->first();
            
        if($qbo_oauth_obj)
        {
            $qbo_oauth_arr = $qbo_oauth_obj->toArray();
        }       

        //Fetch Invoice Details
        $dataService = DataService::Configure(array(                
                'auth_mode' => 'oauth2',
                'ClientID' => config('constants.QBO.CONSUMER_KEY'),
                'ClientSecret' => config('constants.QBO.CONSUMER_SECRET'),
                'accessTokenKey' => $qbo_oauth_arr['access_token'],
                'refreshTokenKey' => $qbo_oauth_arr['refresh_token'],
                'QBORealmID' => config('constants.QBO.COMPANY_ID'),
                'baseUrl' => config('constants.QBO.ENVIRONMENT')
        ));
        if(isset($data_arr) && count($data_arr)>0)
        {
            foreach ($data_arr as $key => $entries_arr) 
            {
                // $entries_arr = json_decode($value,true);

                $entries_arr = isset($entries_arr['dataChangeEvent']['entities'])?
                                $entries_arr['dataChangeEvent']['entities']:[];
                
                if(isset($entries_arr) && count($entries_arr)>0)
                {
                    foreach ($entries_arr as $key => $value) 
                    {
                        if($value['name'] == 'Invoice')
                        {
                            $this->storeInvoiceRecords($value['id'],$dataService,$value['operation']);
                        }
                    }
                }            
            }
        }

        return;                                   
        
  //    $resultEncodedData = json_encode($request->all());
  //    $resultData = $request->all();      
  //    $file_name = './new_test_webhook.txt';
  //    $this->fp = fopen($file_name,'a');

        // fwrite($this->fp, "\n".$resultEncodedData);
        // if(!empty($resultData->eventNotifications[0]->dataChangeEvent->entities[1]->id)) {
        //  $invoiceId = $resultData->eventNotifications[0]->dataChangeEvent->entities[1]->id;
        //  fwrite($this->fp, "\n"."Invoice ID:".$invoiceId);


        //  //Fetch Invoice Details
        //  $dataService = DataService::Configure(array(
  //                   'auth_mode' => 'oauth2',
  //                   'ClientID' => 'L0IHReKjbeVZ1m7KF4k9m1cVu3BRMZBqUnvBc23HHkjswvfEpW',
  //                   'ClientSecret' => 'lkj323HlcWcpSWEEVi429KYUgpyfr6C9XjXp0y8M',
  //                   'accessTokenKey' => 'eyJlbmMiOiJBMTI4Q0JDLUhTMjU2IiwiYWxnIjoiZGlyIn0..eqEMCO3ZdmngUCzZViIpLg.pDVeNXmm0b3GLV5HUXAhUsbd1qeEhnxubLxHX7m1XOBUL0xlrBsleAvHdbX3yu-Qoip0iajNqbaNvvUs2G0_5wS4eFc6rHrgF29K5RHnS9OyLWyL3wmNhLV5KG0PECKhbduMNpqsBmJG9wZRZ_LDQqWuNfv8OWwYAoTuXZspv8dy5D3q4R4aZ7oFKkXZif0KcocVUUjZYuASivMcl7jUVdDWA7g_SS7BeL_VUGmQOew0zxSgf6iFbblzjYzXRloVYs6ybYQkd_yP9So7OfJ9YuaeVf38v7WbHLJslwLC2jFEvUkj2YBhdxo1qXjZ2bzK-xL7bWuRvLVspAy8ZpDFKylnvnwN0wf217V7o6V3FeEBLyKfxvqqvN5DauQ1J_jLaU5CoNdMVF_Eg4Zgc8xDkcTR04pkbs51XhxoFlsYzloHXzy-y-nmv9vwB8vSFXlN2FsQEjDzzt4vz51uyiNqumcL80ApllEqa7grpKx5Bx_1p-M_D9jgdvDu3XcS9Sc7PZTZ58NPceamhmNeyMcR1iW4cz0nJNb4cm_LIRAMTArPi09vSq92QXsFlInFz8JBAyOUV-hE7yj-ggn0XvrCTOtaRyiYpai73fdImqOYLz1ltqvE1uQI84QVbRaIf2PR3Qm00qCt82lL0Rt2qBg9BlVKEViDHKFAwgkgQTsKtr716sWSS77s4raPznY86VUk4Qtwwy8kbV7JTJs3889pNg.6L3L84ytCxliTwMWCV7Dgw',
  //                   'refreshTokenKey' => 'AB11572778806e9Bbvx3iQhelkQbFS5GsQyHtfzcaa6Nh8TVoM',
  //                   'QBORealmID' => 123146130051669,
  //                   'baseUrl' => 'Development'
  //        ));
  //        $theInvoice = $dataService->Query("select * from Invoice where Id = '".$invoiceId."'");
  //        fwrite($this->fp, "\n"."Invoice Details:".json_encode($invoiceId));
        // }    
        
  //       //dd($theInvoice);
        // exit();
            

        //var_dump(file_exists($file_name));die;
    }

    private function storeInvoiceRecords($invoiceId,$dataService,$operation)
    {
        $theInvoice = $dataService->Query("select * from Invoice where Id = '".$invoiceId."'");
        fwrite($this->fp, "\n"."Invoice Details:".json_encode($invoiceId));

        if($theInvoice)
        {
            $invoice_details = $theInvoice[0];
            $data_arr = [];
            $data_arr['invoice_id'] = $invoice_details->Id or 0;
            $data_arr['qbo_user_id'] = $invoice_details->CustomerRef or 0;
            $data_arr['bill_addr']  = isset($invoice_details->BillAddr)?json_encode($invoice_details->BillAddr):'';
            $data_arr['sales_term_ref']  = $invoice_details->SalesTermRef or '';
            $data_arr['tranx_date']  = $invoice_details->TxnDate or '';
            $data_arr['due_date']   = $invoice_details->DueDate or '';
            $data_arr['invoice_status']   = $invoice_details->EInvoiceStatus or '';
            $data_arr['job_ref_no']   = isset($invoice_details->CustomField[0]->StringValue)?$invoice_details->CustomField[0]->StringValue:'';
            $data_arr['notice_type']  = isset($invoice_details->CustomField[1]->StringValue)?$invoice_details->CustomField[1]->StringValue:'';
            $data_arr['job_location'] = isset($invoice_details->CustomField[2]->StringValue)?$invoice_details->CustomField[2]->StringValue:'';
            $data_arr['total_amt']    = $invoice_details->TotalAmt or '';
            $data_arr['balance']      = $invoice_details->Balance or '';      
            $data_arr['doc_number']   = $invoice_details->DocNumber or '';      
            $data_arr['other_details'] = isset($invoice_details->Line)?json_encode($invoice_details->Line):'';

            
            /* check is invoice already available or not*/
            $is_exist = $this->OAuth2InvoiceResponse->where('invoice_id',$invoice_details->Id)
                                                    ->count()>0;             

            if($is_exist && $operation == 'Update')
            {
                $is_store = $this->OAuth2InvoiceResponse->where('invoice_id',$invoice_details->Id)
                                            ->update($data_arr);                
            }
            else
            {
                $is_store = $this->OAuth2InvoiceResponse->insert($data_arr);
            }
            
            if($is_store)
            {
                return true;               
            }
            else
            {
                return false;
            }                              
        }

        return false;
    }

    private function json_split_objects($json)
    {
        $q = FALSE;
        $len = strlen($json);
        for($l=$c=$i=0;$i<$len;$i++)
        {   
            $json[$i] == '"' && ($i>0?$json[$i-1]:'') != '\\' && $q = !$q;
            if(!$q && in_array($json[$i], array(" ", "\r", "\n", "\t"))){continue;}
            in_array($json[$i], array('{', '[')) && !$q && $l++;
            in_array($json[$i], array('}', ']')) && !$q && $l--;
            (isset($objects[$c]) && $objects[$c] .= $json[$i]) || $objects[$c] = $json[$i];
            $c += ($l == 0);
        }   
        return $objects;
    }
}