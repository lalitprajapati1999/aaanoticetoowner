<?php

use App\Models\City;
use App\Models\CustomerRepresentative;

/**
 * Get County from id
 */
function getCounty($county_id) {
    $names = City::select(['id', \DB::raw('concat(county) as name')])->find($county_id);

    return $names['name'];
}
function getCountyName($county_id) {
    $names = City::select(['id', \DB::raw('concat(name) as name')])->find($county_id);

    return $names['name'];
}

function getSecondaryDocumentCustomerId($secondary_doc_id) {
    $secondary_doc = App\Models\SecondaryDocument::find($secondary_doc_id);

    return $secondary_doc->customer_id;
}

/**
 * Get Pricing info
 */
function getPricings($notice_id, $package_id, $id) {
    //echo $notice_id . "=" . $package_id . "=" . $lower_limit . "=" . $upper_limit;
    $data = App\Models\Pricing::where(['notice_id' => $notice_id, 'package_id' => $package_id, 'id' => $id])->first();
     
    return $data;
}
/**
 * Get Pricing info
 */
function getOtherPricings($notice_id, $package_id, $lower_limit, $upper_limit) {
    //echo $notice_id . "=" . $package_id . "=" . $lower_limit . "=" . $upper_limit;
    $data = App\Models\Pricing::where(['notice_id' => $notice_id, 'package_id' => $package_id, 'lower_limit' => $lower_limit, 'upper_limit' => $upper_limit])->first();
    // dd($data);
    return $data;
}

/**
 * Get customer Pricing info
 */
function getCustomerPricings($customer_id, $notice_id, $package_id, $lower_limit, $upper_limit) {
    //echo $notice_id . "=" . $package_id . "=" . $lower_limit . "=" . $upper_limit;
    $data = App\Models\Customer_package::where(['customer_id' => $customer_id, 'notice_id' => $notice_id, 'package_id' => $package_id, 'lower_limit' => $lower_limit, 'upper_limit' => $upper_limit])->first();
    //  dd($data);
    return $data;
}

/**
 * Get County from id
 */
function getNewApplicantsCount() {
    $data_count = App\Models\Customer::where('account_manager_id', '=', NULL)
            ->orderBy('id', 'DESC')
            ->count();
    return $data_count;
}

function getDocumentTypes() {
    $types = ['10' => 'Folio', '1' => 'Bond', '2' => 'Notice of Commencement', '3' => 'Permit', '4' => 'Contract',
        '5' => 'Invoice', '6' => 'Final Release', '7' => 'Partial Release', '8' => 'Misc Document',
        '9' => 'Signed Document','11' => 'COL Signed','12'=>'Recorded COL/SCOL', '13'=>'Pending Signature'];
    return $types;
}

function get_user_notice_email($customer_id = 0) {
    $data = [];

    /* get all admin list */
    $admin_email = \App\User::leftjoin('role_users', 'role_users.user_id', '=', 'users.id')
            ->where('role_users.role_id', '=', 2)
            ->select('users.email')
            ->orderBy('users.email', 'asc')
            ->get();
    $admin_email_val = [];
    foreach ($admin_email as $admin_val) {
        if($admin_val->email == "info@aaanoticetoowner.com"){
            $admin_email_val[] = $admin_val->email;
        }
    }

    /* get all account manager list */
    $accountManager = \App\User::leftjoin('role_users', 'role_users.user_id', '=', 'users.id')
            ->where('role_users.role_id', '=', 3)
            ->select('users.email')
            ->orderBy('users.email', 'asc')
            ->get();
    $acc_email_val = [];
    foreach ($accountManager as $acc_manager) {
        $acc_email_val[] = $acc_manager->email;
    }

   

    /* get Cutomer assigned account manager */
    $custAccountManager = \App\Models\Customer::leftjoin('users', 'users.id', '=', 'customers.account_manager_id')
            ->select('users.email')
            ->find($customer_id);
    $cust_acc_email = "";
    if (!empty($custAccountManager)) {
        $cust_acc_email = $custAccountManager->email;
    }
    /* get customer email id */
    $getCustomer = \App\Models\Customer::leftjoin('users', 'users.id', '=', 'customers.user_id')
            ->select('users.email')
            ->orderBy('users.email', 'asc')
            ->find($customer_id);
    $cust_email = [];
    if (!empty($getCustomer)) {
        $cust_email = $getCustomer->email;
    }
    foreach ($admin_email_val as $data_email) {
        $data[] = $data_email;
    }

    if (Auth::user()->hasRole('customer')) {
        $data[] = $cust_acc_email;
    } else {
        foreach ($acc_email_val as $dataemail) {
            $data[] = $dataemail;

        }
        $data[] = $cust_email;

    }

     /* get Cutomer assigned Representative */
    $custrepresentative = CustomerRepresentative::where('customer_id','=',$customer_id)->orderBy('email', 'asc')->get();
    $cust_rep_email_val = "";
    foreach ($custrepresentative as $rep) {
        $data[] = $rep->email;
    }
    /* get all Representative list */
    $representative = CustomerRepresentative::get();
        
    $rep_email_val = [];
    foreach ($representative as $rep) {
        $rep_email_val[] = $rep->email;
    }


    /** Customer representative **/
    /*if (Auth::user()->hasRole('account-manager')) {
        $data[] = $cust_rep_email_val;
    } else {
        foreach ($rep_email_val as $dataemail) {
            $data[] = $dataemail;
            
        }
    }*/
    $data = array_merge($data,Config('constants.SIGNED_DOC_EMAILS'));
   // dd($data);
    return $data;

}
function get_customer_user_notice_email($customer_id){
    $data = [];

    /* get all admin list */
    $admin_email = \App\User::leftjoin('role_users', 'role_users.user_id', '=', 'users.id')
            ->where('role_users.role_id', '=', 2)
            ->select('users.email')
            ->orderBy('users.email', 'asc')
            ->get();
    $admin_email_val = [];
    foreach ($admin_email as $admin_val) {
        if($admin_val->email == "info@aaanoticetoowner.com"){
            $admin_email_val[] = $admin_val->email;
        }
    }
    /* get all account manager list */
    $accountManager = \App\User::leftjoin('role_users', 'role_users.user_id', '=', 'users.id')
            ->where('role_users.role_id', '=', 3)
            ->select('users.email')
            ->orderBy('users.email', 'asc')
            ->get();
    $acc_email_val = [];
    foreach ($accountManager as $acc_manager) {
        $acc_email_val[] = $acc_manager->email;
    }

   

    /* get Cutomer assigned account manager */
    $custAccountManager = \App\Models\Customer::leftjoin('users', 'users.id', '=', 'customers.account_manager_id')
            ->select('users.email')
            ->find($customer_id);
    $cust_acc_email = "";
    if (!empty($custAccountManager)) {
        $cust_acc_email = $custAccountManager->email;
    }
    /* get customer email id */
    $getCustomer = \App\Models\Customer::leftjoin('users', 'users.id', '=', 'customers.user_id')
            ->select('users.email')
            ->orderBy('users.email', 'asc')
            ->find($customer_id);
    $cust_email = [];
    if (!empty($getCustomer)) {
        $cust_email = $getCustomer->email;
    }
    foreach ($admin_email_val as $data_email) {
        $data[] = $data_email;
    }

    if (Auth::user()->hasRole('customer')) {
        $data[] = $cust_acc_email;
    } else {
        foreach ($acc_email_val as $dataemail) {
            $data[] = $dataemail;

        }
        $data[] = $cust_email;

    }

     /* get Cutomer assigned Representative */
    $custrepresentative = CustomerRepresentative::where('customer_id','=',$customer_id)->orderBy('email', 'asc')->get();
    $cust_rep_email_val = "";
    foreach ($custrepresentative as $rep) {
        $data[] = $rep->email;
    }
    /* get all Representative list */
    $representative = CustomerRepresentative::get();
        
    $rep_email_val = [];
    foreach ($representative as $rep) {
        $rep_email_val[] = $rep->email;
    }

    return $data;
}

function get_user_private_notice_email($customer_id = 0) {

  $data = [];

  /* get customer email id */
    $getCustomer = \App\Models\Customer::leftjoin('users', 'users.id', '=', 'customers.user_id')
            ->select('users.email')
            ->orderBy('users.email', 'asc')
            ->find($customer_id);
    $cust_email = [];
    if (!empty($getCustomer)) {
        $cust_email = $getCustomer->email;
    }

    /* get all admin list */
    $admin_email = \App\User::leftjoin('role_users', 'role_users.user_id', '=', 'users.id')
            ->where('role_users.role_id', '=', 2)
            ->orderBy('users.email', 'asc')
            ->select('users.email')
            ->get();
    $admin_email_val = [];
    foreach ($admin_email as $admin_val) {
        $admin_email_val[] = $admin_val->email;
    }

    /* get all account manager list */
    $accountManager = \App\User::leftjoin('role_users', 'role_users.user_id', '=', 'users.id')
            ->where('role_users.role_id', '=', 3)
            ->orderBy('users.email', 'asc')
            ->select('users.email')
            ->get();
    $acc_email_val = [];
    foreach ($accountManager as $acc_manager) {
        $acc_email_val[] = $acc_manager->email;
    }

    foreach ($admin_email_val as $data_email) {
        $data[] = $data_email;
    }

    foreach ($acc_email_val as $dataemail) {
        $data[] = $dataemail;

    }
    $data[] = $cust_email;
    $data = array_merge($data,Config('constants.SIGNED_DOC_EMAILS'));
    return $data;

}

function str_lreplace($str)
{
    $str= str_replace("<p>", "<br/>", $str);
    $str= html_entity_decode($str);
    $patterns = '/<br\/>/i';
    $search = "<br/>";
    $replace = " ";
    $tab = explode("</p>",$str);
    $len = sizeof($tab);
    $str = preg_replace($patterns, $replace , $str,1);
    $str = strtoupper($str);
    return $str;
}

