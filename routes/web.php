<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test', function () {
    return view('test');
});

// Auth::routes();
require (__DIR__ . '/auth.php');

Route::get('/home', 'HomeController@index')->name('home');


//contact routes
require (__DIR__ . '/customer.php');
require (__DIR__ . '/admin.php');
require (__DIR__ . '/account_manager.php');

//Customer account
Route::get('/deadline-calculator', 'PagesController@index');
Route::get('/lien-laws', 'PagesController@lien_laws');
Route::get('/pricing', 'PagesController@pricing');
Route::get('/pricing/{package_id}/subscribe', 'PagesController@subscription_redirection');
Route::get('/contact-us', 'PagesController@contactus');
Route::post('store/contact-us', 'PagesController@storeContactus');
Route::post('get-add-ons', 'AccountManager\ResearchController@getAddons');
Route::post('get-package-types', 'AccountManager\ResearchController@getPackageTypes');
Route::post('get-prohibited', 'AccountManager\ResearchController@getProhibitedRequiredAddOns');
Route::post('store/deadline/contact-us', 'PagesController@storeDeadlineContactus');
Route::get('dueDate/emailNotification','CronJobController@dueDateNotification');
Route::get('dueDate/discontinueReminder/{work_order_id}','CronJobController@discontinueReminder');
Route::get('create_your_own/dueDate/emailNotification','CronJobController@createyourowndueDateNotification');
Route::get('create_your_own/dueDate/discontinueReminder/{work_order_id}','CronJobController@createyourowndiscontinueReminder');
Route::get('saveLabel','CronJobController@saveLabel');

//Quickbook webhook callback url
Route::any('qb_webhook','WebhookQbController@index');
Route::any('test-controller','TestController@index');

