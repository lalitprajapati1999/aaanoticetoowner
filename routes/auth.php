<?php

if (config('adminlte.login_url', 'login')):
// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::any('logout', 'Auth\LoginController@logout')->name('logout');
endif;

if (config('adminlte.register_url', 'register')):
// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::get('credit-card', 'Auth\RegisterController@creditCard')->name('credit-card');
Route::post('check-credit-card', 'Auth\RegisterController@checkCreditCard');
Route::post('register', 'Auth\RegisterController@register');
Route::get('county','Auth\RegisterController@getCounties');
endif;

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');
Route::get('cities','PagesController@getCities');
Route::get('states','PagesController@getStates');


Route::get('qbo/oauth', 'QuickBooksController@generateTokens');
Route::get('qbo/ouath_url', 'QuickBooksController@getAuthorisationUrl');
Route::get('qbo/task', 'QuickBooksController@task');