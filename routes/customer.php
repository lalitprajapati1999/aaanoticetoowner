<?php

/*
  |--------------------------------------------------------------------------
  | Admin Section Routes
  |--------------------------------------------------------------------------
  |
  | Below route group is where you may define all of the routes that are
  | handled at admin and superadmin login.
  |
 */
Route::group(
        [
    'namespace' => 'Auth',
    'middleware' => 'auth',
    'prefix' => config('backpack.base.route_prefix'),
        ], function () {

    // if not otherwise configured, setup the "my account" routes
    if (config('backpack.base.setup_my_account_routes')) {
        Route::get('edit-account-info', 'MyAccountController@getAccountInfoForm')->name('backpack.account.info')->middleware('can:admin-edit-account-info');
        Route::post('edit-account-info', 'MyAccountController@postAccountInfoForm')->name('backpack.account.info')->middleware('can:admin-edit-account-info');

        
    }
});
Route::group(
        [
    'namespace' => 'Customer',
    'middleware' => 'auth',
    'prefix' => 'customer',
    'as' => 'customer.'
        ], function () {
    Route::post('check-credit-card', 'DashboardController@checkCreditCard');
    Route::get('customer/dashboard', 'HomeController@customerDashboard')->name('customer.dashboard')->middleware('can:customer-dashboard');
    ;
    Route::get('contacts', 'ContactController@index')->name('address-book')->middleware('can:customer-contacts');
    Route::get('contacts/custom-filter', 'ContactController@getAddressBookFilterData');
    Route::get('contact/create', 'ContactController@create')->name('contact.create')->middleware('can:customer-contacts-create');
    Route::post('contact/create', 'ContactController@store')->name('contact.store')->middleware('can:customer-contacts-create');
    Route::get('contact/{id}/edit', 'ContactController@edit')->name('contact.edit')->middleware('can:customer-contacts-edit');
    Route::post('contact/edit/{id}', 'ContactController@update')->name('contact.update')->middleware('can:customer-contacts-edit');
    Route::post('contact/delete', 'ContactController@destroy')->name('contact.delete')->middleware('can:customer-contacts-delete');
    Route::post('contact/secondary-document/update_recipient_contact', 'ContactController@updateSecondaryRecipientContact');
    Route::post('wo_update_recipient_contact', 'ContactController@updateWorkOrderRecipientContact');
//        ->middleware('can:customer-contacts');
    Route::post('contact/cyowork-order/update_recipient_contact', 'ContactController@updateCyoWorkOrderRecipientContact')->middleware('can:customer-contacts');

//workorder

    Route::get('new-work-order', 'WorkOrderController@index')->middleware('can:new-work-order');
    Route::get('work-order/create/', 'WorkOrderController@create')->middleware('can:new-work-order-create');
    Route::post('new-work-order/create/', 'WorkOrderController@store')->middleware('can:new-work-order-create');
    Route::get('new-work-order/create/{id}/{notice_id}', 'WorkOrderController@workorderStoreNextStep')->middleware('can:new-work-order-create');
    Route::post('work-order/attachments', 'WorkOrderController@store_attachments');
    Route::post('work-order/edit/attachments', 'WorkOrderController@edit_attachments');
    /* This routes for customer and account manager login */
    Route::post('work-order/store_note', 'WorkOrderController@storeNote');
    Route::post('work-order/store_correction', 'WorkOrderController@storeCorrection');
    /**/
    Route::get('work-order/updateStatus/{work_order_id}/{status}', 'WorkOrderController@updateStatus');
    Route::get('view-work-order/updateStatus/{work_order_id}/{status}', 'ViewWorkOrderController@updateStatus');

    //last-date-on-job
    Route::get('modal-add-last-date-on-job/{work_order_id}/{cyo}', 'ViewWorkOrderController@loadModalAddLastDateOnJob');
    Route::post('save-last-date-on-job/{cyo}', 'ViewWorkOrderController@saveLastDateOnJob');

    //clerk-court-recorded-date
    Route::get('modal-add-clerk-court-recorded-date/{work_order_id}/{cyo}', 'ViewWorkOrderController@loadModalClerkCourtRecordedDate');
    Route::post('save-clerk-court-recorded-date/{cyo}', 'ViewWorkOrderController@saveClerkCourtRecordedDate');
//Secondary Document


    Route::get('secondary-document', 'SecondaryDocumentController@index')->middleware('can:secondary-document');
    Route::get('secondary-document/custom-filter', 'SecondaryDocumentController@getCustomeFilterData')->middleware('can:secondary-document');
    Route::get('partial-document/{partial_final_type}', 'SecondaryDocumentController@create');
    Route::post('partial-document', 'SecondaryDocumentController@store');
    Route::post('secondary-recipients/{secondary_doc_id}', 'SecondaryDocumentController@store_recipients');
    Route::post('get-contacts', 'SecondaryDocumentController@get_contacts');
    Route::get('secondary-recipient/delete/{id}', 'SecondaryDocumentController@delete');
    Route::post('secondary-document/attachments', 'SecondaryDocumentController@store_attachments');
    Route::post('secondary-document/edit/attachments', 'SecondaryDocumentController@edit_attachments');
    Route::get('secondary-document/autocomplete', 'SecondaryDocumentController@getWorkOrdeNumber');
    Route::get('secondary-document/autocomplete_V1', 'SecondaryDocumentController@getWorkOrdeNumberV1');
    Route::post('account-manager/secondary-document/autocomplete', 'SecondaryDocumentController@getAccountManagerWorkOrdeNumber');
    Route::post('account-manager/secondary-document/authoriseagent', 'SecondaryDocumentController@getAccountManagerAuthoriseAgent');
    Route::post('work-order/recipient/create/{id}/{notice_id}', 'WorkOrderController@store_recipients');
    Route::post('recipients/{id}/{notice_id}', 'ViewWorkOrderController@store_recipients');
    Route::post('contact-recipient/store', 'ContactController@contactAddFromRecipients');
    Route::post('getAllContacts', 'ContactController@getAllContacts');
    Route::get('secondary-document/updateStatus/{secondary_doc_id}', 'SecondaryDocumentController@updateStatus');
    Route::post('secondary-document/get_recipient_data', 'SecondaryDocumentController@getRecipientData')->middleware('can:secondary-document');

    Route::get('recipient/delete/{id}/{notice_id}/{work_order_id}', 'ViewWorkOrderController@destroy');
    Route::get('recipient/deleteCreated/{id}/{notice_id}/{work_order_id}', 'WorkOrderController@destroy');
    Route::get('get-projectaddress', 'SecondaryDocumentController@getProjectAddress');
    Route::get('get-secondarydocids', 'SecondaryDocumentController@getSecondaryDocIds');
    Route::get('get-secondarydocids_V1', 'SecondaryDocumentController@getSecondaryDocIds_V1');
    Route::get('get-secondaryparentworkorderids', 'SecondaryDocumentController@getSecondaryParentWorkOrder');
    Route::get('get-secondaryparentworkorderids_V1', 'SecondaryDocumentController@getSecondaryParentWorkOrder_V1');
    Route::post('secondary-document/work_order_details', 'SecondaryDocumentController@getWorkOrderDetails');
    Route::post('secondary-document/store_note', 'SecondaryDocumentController@storeNote');
    Route::get('secondary-document/printSecondaryDocument/{id}/{print_status}', 'SecondaryDocumentController@printSecondaryDocument');
    Route::post('secondary-document/sendMailPdf', 'SecondaryDocumentController@sendMailPdf');
    Route::get('secondary-document/edit/{secondary_doc_id}/{edit_flag}', 'SecondaryDocumentController@editSecondaryDocument')->middleware('can:secondary-document');
    Route::post('secondary-document/edit/{secondary_doc_id}', 'SecondaryDocumentController@updateSecondaryDocument');
    Route::get('secondary-document/printpdf/{secondary_doc_id}/{partial_final_release}', 'SecondaryDocumentController@printCreatedSecondaryDocument');
    Route::post('removeattachment', 'SecondaryDocumentController@deleteAttachment');
    Route::post('parent_document/removeattachment', 'SecondaryDocumentController@deleteParentAttachment');
    Route::post('edit/removeattachment', 'SecondaryDocumentController@deleteEditDocAttachment');
    Route::post('secondary-document/store_correction', 'SecondaryDocumentController@storeCorrection')->middleware('can:secondary-document');
//view work order
    Route::get('view-work-order', 'ViewWorkOrderController@index')->middleware('can:customer-view-work-order');
//Route::get('recipients/edit/{id}/{notice_id}','ViewWorkOrderController@edit_recipient');
    Route::get('work-order/cancel/{id}', 'ViewWorkOrderController@cancel');
    Route::get('work-order/duplicate/{id}/{notice_id}/{is_rescind}', 'ViewWorkOrderController@duplicate');
    Route::get('work-order/rescind/{id}/{notice_id}/{is_rescind}', 'ViewWorkOrderController@duplicate');
    Route::get('work-order/proceed/{id}', 'ViewWorkOrderController@proceed');
    Route::get('work-order/edit/{id}/{notice_id}/{is_rescind}', 'ViewWorkOrderController@edit');
    Route::post('work-order/edit/{id}/{notice_id}/{is_rescind}', 'ViewWorkOrderController@update');
    Route::get('work-order/edit-new/{id}/{notice_id}/{is_rescind}', 'ViewWorkOrderController@researchEditNextStep');
    Route::post('work-order/create/{id}/{notice_id}', 'WorkOrderController@update');
    Route::get('work-order/create/{id}/{notice_id}', 'WorkOrderController@researchEditNextStep');
    Route::get('get-work-order', 'ViewWorkOrderController@get_workorders');
    Route::post('work-order/add-note', 'ViewWorkOrderController@add_note');
    Route::get('get-selectedCustomer-workOrders', 'ViewWorkOrderController@selectedCustomer');
    Route::get('get-project_Info-workOrders', 'ViewWorkOrderController@filterWithProjectInfo');
   
    Route::get('work-order/view/{id}', 'ViewWorkOrderController@viewWorkOrder');
    Route::post('work-order/view/uploadreceipt', 'ViewWorkOrderController@postViewWorkOrder');
    Route::get('work-order/view/printworkorder/{workorderid}', 'ViewWorkOrderController@printrequesteworkorder');
    Route::post('parent_work_order', 'WorkOrderController@getParentOrderId');
    Route::get('work-order/printpdf/{work_order_id}', 'ViewWorkOrderController@printWorkOrder');
    Route::get('printpdf/{work_order_id}', 'ViewWorkOrderController@printWO');
    Route::post('contact_workorderrecipient/store', 'ContactController@contactAddFromWorkOrderRecipients');
    Route::post('getAllWorkOrderContacts', 'ContactController@getAllWorkOrderContacts');
    Route::post('new-work-order/get-hard-notices', 'WorkOrderController@getHardNotices');
    Route::post('new-work-order/get-soft-notices', 'WorkOrderController@getSoftNotices');
    Route::post('work-order/removeattachment', 'WorkOrderController@deleteAttachment');
    Route::post('edit/work-ordwer/removeattachment', 'WorkOrderController@deleteEditDocAttachment');
    Route::post('edit/cyo-work-ordwer/removeattachment', 'OwnWorkOrderController@deleteEditCyoDocAttachment');
    Route::post('work-order-document/work_order_details', 'SecondaryDocumentController@getWorkOrderDocumentDetails');
    Route::post('work-order-document/parent_document/removeattachment', 'WorkOrderController@deleteParentAttachment');

    Route::post('work-order/get_recipient_data', 'WorkOrderController@getRecipientData');
    Route::post('cyo-work-order-document/parent_document/removeattachment', 'WorkOrderController@deleteParentAttachment');

    Route::get('secondary-recipient/delete/{id}/{secondary_document_id}/{partial_final_type}', 'SecondaryDocumentController@destroy');
    Route::post('secondary-recipients/update/{secondary_doc_id}', 'SecondaryDocumentController@update_recipients');
    Route::post('contact-cyoworkorderrecipient/store', 'ContactController@contactAddFromCyoWorkOrderRecipients');
    Route::post('getAllCyoWorkOrderContacts', 'ContactController@getAllCyoWorkOrderContacts');

//Route::get('date-range/custom-filter','ViewWorkOrderController@getCustomeFilterData');

    Route::get('subscription/{id}/make-payment', 'SubscriptionController@makePayment')->middleware('auth');
    Route::post('subscription/get-paid', 'SubscriptionController@getpaid')->middleware('auth');
    Route::post('subscription/save_cheque_details', 'SubscriptionController@saveChequeDetails')->middleware('auth');
    Route::post('subscription/save_creditcard_details', 'SubscriptionController@saveCreditCardDetails')->middleware('auth');
    Route::post('subscription/delete-card', 'SubscriptionController@deleteCardDetails')->middleware('auth');
//create your own section
    Route::get('create-your-own', 'OwnWorkOrderController@index')->name('create-your-own')->middleware('can:customer-create-your-own');
    Route::get('create-your-own/notice-list', 'OwnWorkOrderController@notice_list');
//Route::get('create-your-own/notice-list', 'OwnWorkOrderController@notice_list');
    Route::get('create-your-own/work-order-history', 'OwnWorkOrderController@WorkOrderHitory');
    Route::get('create-your-own/create/{notice_type}/{notice_id}/{state_id}', 'OwnWorkOrderController@create');
    Route::post('create-your-own/create', 'OwnWorkOrderController@store');
    Route::get('create-your-own/create-new/{id}/{notice_id}', 'OwnWorkOrderController@cyoStoreNextStep');
    Route::post('create-your-own/recipients/{id}/{notice_id}', 'OwnWorkOrderController@store_recipients');
    Route::post('create-your-own/recipients/edit/{id}/{notice_id}', 'OwnWorkOrderController@edit_recipients');
    Route::get('create-your-own/recipient/delete/{id}/{notice_id}/{work_order_id}', 'OwnWorkOrderController@destroy');
    Route::post('create-your-own/store_note', 'OwnWorkOrderController@storeNote');
    Route::post('create-your-own/store_correction', 'OwnWorkOrderController@storeCorrection');
    Route::get('create-your-own/get-work-order', 'OwnWorkOrderController@get_workorders');
    Route::get('create-your-own/cancel/{id}', 'OwnWorkOrderController@cancel');
    Route::post('create-your-own/add-note', 'OwnWorkOrderController@add_note');
    Route::get('create-your-own/edit/{id}/{notice_id}/{is_rescind}', 'OwnWorkOrderController@edit');
    Route::post('create-your-own/edit/{id}/{notice_id}/{is_rescind}', 'OwnWorkOrderController@update');
     Route::get('create-your-own/edit-new/{id}/{notice_id}/{is_rescind}', 'OwnWorkOrderController@researchEditNextStep');
    Route::post('create-your-own/create/{id}/{notice_id}', 'OwnWorkOrderController@store_own_work_order');
    Route::get('create-your-own/updateStatus/{id}', 'OwnWorkOrderController@updateStatus');
    Route::Post('create-your-own/checkFields', 'OwnWorkOrderController@CheckFields');
    Route::get('create-your-own/view/printworkorder/{workorderid}', 'OwnWorkOrderController@printrequesteworkorder');

    Route::get('create-your-own/proceed/{id}/{notice_id}', 'OwnWorkOrderController@proceed');
    Route::get('create-your-own/autocomplete', 'OwnWorkOrderController@getWorkOrdeNumber');
    Route::get('create-your-own/autocomplete_V1','OwnWorkOrderController@getWorkOrdeNumberV1');
    Route::post('create-your-own/work_order_details', 'OwnWorkOrderController@getWorkOrderDetails');
    Route::post('create-your-own/attachments', 'OwnWorkOrderController@store_attachments');

    Route::post('create-your-own/edit/attachments', 'OwnWorkOrderController@edit_attachments');

    Route::get('create-your-own/printpdf/{work_order_id}', 'OwnWorkOrderController@printWO');
    Route::post('create-your-own/removeattachment', 'OwnWorkOrderController@deleteAttachment');
    Route::post('create-your-own/get_recipient_data', 'OwnWorkOrderController@getRecipientData');
    Route::get('create-your-own/duplicate/{id}/{notice_id}/{is_rescind}', 'OwnWorkOrderController@duplicate');
    Route::get('create-your-own/rescind/{id}/{notice_id}/{is_rescind}', 'OwnWorkOrderController@duplicate');
    Route::get('create-your-own/view/{id}', 'OwnWorkOrderController@viewWorkOrder');
    Route::post('create-your-own/createLabel', 'OwnWorkOrderController@createLabel');

    Route::get('create-your-own/recording/pending_signature/updateStatus/{status}/{work_order_id}', 'OwnWorkOrderController@updateRecordingPendingSignStatus');
    Route::post('create-your-own/view/uploadreceipt', 'OwnWorkOrderController@postViewWorkOrder');

    Route::get('deadline-calculator', 'DashboardController@index')->name('customer.deadline_calculator');/*->middleware('can:customer-dashboard');*/
//        ->middleware('can:customer-deadline-calculator');
    Route::post('deadline-calculator/notices', 'DashboardController@getNotices')->name('customer.deadline.notices');

//        ->middleware('can:customer-deadline-calculator');
    Route::post('calculate-date', 'DashboardController@calculate_date')->name('customer.deadline.calculate_date');
//        ->middleware('can:customer-deadline-calculator');
    Route::get('lien_blog', 'DashboardController@getlienblogs');/*->middleware('can:customer-dashboard');
    ;*/
    Route::get('contact-us', 'DashboardController@viewcontactus');/*->middleware('can:customer-dashboard');*/
    Route::post('store/contact-us', 'DashboardController@storeContactUs');
//create your own verifies address
    Route::post('create-your-own/getVerifiedAddress', 'OwnWorkOrderController@getVerifiedAddress');
    Route::get('statement-invoices', 'StatementInvoicesController@getStatementInvoices')->name('customer.statement_invoices')->middleware('can:customer-statement-invoices');
    Route::get('statement-invoices/custom-filter', 'StatementInvoicesController@getStatementInvoicesFilterData');
    Route::post('statement-invoice/print/', 'StatementInvoicesController@printStatementInvoice');
    Route::get('statement-invoices/generateInvoice/{work_order_id}', 'StatementInvoicesController@generateInvoice');

    //Invoice routes
    Route::get('invoice', 'InvoicesController@listing')->name('invoice');
    Route::get('invoice/invoice-filter', 'InvoicesController@getInvoices');
    Route::get('invoice/invoice-details/{invoice_id}', 'InvoicesController@view');
    Route::get('searchajaxcustomername', 'SecondaryDocumentController@autoCompleteCustomer');

    //cradit-card
    Route::get('credit-card/{id}/{work_order_id}', 'OwnWorkOrderController@creditCard')->name('credit-card');
    Route::post('check-credit-card', 'OwnWorkOrderController@checkCreditCard');

});

Route::get('qbo/success', 'Auth\RegisterController@qbo_success');

Route::group(
        [
    'namespace' => 'Customer',
    'middleware' => 'web',
    'prefix' => 'customer',
    'as' => 'customer.'
        ], function () {

    Route::get('contact/searchajaxcompanyname', 'ContactController@autoCompleteCompanyName');
    Route::get('city/autocompletecityname', 'SecondaryDocumentController@autoCompleteCity');
    Route::get('city/autocompletecountyname/{notice_id?}', 'SecondaryDocumentController@autoCompleteCounty');
    Route::get('city/autocompletecityzip', 'SecondaryDocumentController@autocompleteCityZip');
    Route::get('city-list', 'SecondaryDocumentController@cityList');
    Route::get('city-list', 'SecondaryDocumentController@cityList');
    Route::get('notices/{state_id}', 'SecondaryDocumentController@getNotices');

});
