<?php

/*
  |--------------------------------------------------------------------------
  | Admin Section Routes
  |--------------------------------------------------------------------------
  |
  | Below route group is where you may define all of the routes that are
  | handled at admin and superadmin login.
  |
 */

Route::group(
        [
    'namespace' => 'Admin',
    'middleware' => 'auth',
    'prefix' => 'admin',
    'as' => 'admin.'
        ], function () {
    Route::get('notices', 'NoticesController@index')->name('notices')->middleware('can:admin-notices');
    Route::get('notices/create', 'NoticesController@create')->name('notices.create')->middleware('can:admin-notices-create');
    Route::post('notices', 'NoticesController@store')->name('notices.store')->middleware('can:admin-notices-create');
    Route::get('notices/{notice}/edit', 'NoticesController@edit')->name('notices.edit')->middleware('can:admin-notices-edit');
    Route::put('notices/{notice}', 'NoticesController@update')->name('notices.update')->middleware('can:admin-notices-edit');
    Route::post('notices/status', 'NoticesController@changeStatus')->name('notices.status')->middleware('can:admin-notices-status');
    Route::get('notices/get_all_notices/{hard_soft_type}', 'NoticesController@getAllNotices')->name('notices')->middleware('can:admin-notices');
    // Notice Fields
    Route::get('notices/{notice}/{state_id}', 'NoticeFieldsController@index')->name('notices.states.fields')->middleware('can:admin-notices-states-fields');
    /* Route::get('notices/{notice}/states/{state}', 'NoticeFieldsController@index')->name('notices.states.fields')->middleware('can:admin-notices-states-fields'); */
    Route::put('notices/{notice}/states/{state}', 'NoticeFieldsController@store')->name('notices.states.fields.store')->middleware('can:admin-notices-states-fields');
    Route::put('notices/{notice}/states/{state}/order', 'NoticeFieldsController@changeOrder')->name('notices.states.fields.order')->middleware('can:admin-notices-states-fields');
    Route::get('notices/{notice}/states/{state}/fields/{noticeField}', 'NoticeFieldsController@edit')->name('notices.states.fields.order')->middleware('can:admin-notices-states-fields');
    Route::get('notice-field/destroy', 'NoticeFieldsController@destroy')->name('notices.fields.delete')->middleware('can:admin-notices-states-fields');

    //Customer
    Route::get('customer', 'CustomerController@index')->name('admin.customer')->middleware('can:admin-customer');
    Route::get('customer/custom-filter', 'CustomerController@getCustomerFilter')->name('admin.customer.filter');
    Route::get('customer/{customer}/edit', 'CustomerController@edit')->name('customer.edit')->middleware('can:admin-customer-edit');
    Route::post('customer/edit/{customer}', 'CustomerController@update')->name('customer.update')->middleware('can:admin-customer-edit');
    Route::post('customer/changestatus', 'CustomerController@changestatus')->name('customer.status')->middleware('can:admin-customer-status');
    Route::post('customer/activate-subscription', 'CustomerController@activateSubscription')->name('customer.activate-subscription')->middleware('can:admin-customer-subscription');
    Route::get('customer/view/{customer_id}', 'CustomerController@viewCustomer')->name('admin.customer.view');
    Route::get('customer/searchajaxcustomername', 'CustomerController@autoCompleteCustomer');
    //Route Network
    Route::get('network', 'NetworkController@index')->name('admin.network')->middleware('can:admin-network');
    Route::post('network/allowsecondarynetwork', 'NetworkController@allowSecondaryNetwork')->name('network.allowsecondarynetwork')->middleware('can:admin-network-allow-secondary');
    Route::post('network/removesecondarynetwork', 'NetworkController@removeSecondaryNetwork')->name('network.allowsecondarynetwork')->middleware('can:admin-network-allow-secondary');
    Route::get('network/searchajaxaccountmanager', 'NetworkController@autoCompleteAccountManager')->middleware('can:admin-network-allow-secondary');

    //Route New Applicants
    Route::get('new-applicants', 'NewApplicantsController@index')->name('admin.new.applicants')->middleware('can:admin-new-applicants');
    Route::post('new-applicants/assign/accountmanager', 'NewApplicantsController@assignAccountManager')->name('admin.new.applicants.assign.accmanager')->middleware('can:admin-new-applicants');

    //Route for Notice Template
    Route::get('notice_templates', 'NoticeTemplateController@index')->name('admin.notice_templates')->middleware('can:admin-notice-templates');
    Route::get('notice_templates/datatable', 'NoticeTemplateController@noticeTemplateDatatable')->name('admin.notice_templates')->middleware('can:admin-notice-templates');
    Route::get('notice_template/create', 'NoticeTemplateController@createTemplate')->name('admin.notice_template.create')->middleware('can:admin-notice-template-create');
    Route::post('notice_template/store', 'NoticeTemplateController@storeNoticeTemplate')->name('admin.notice.store.template')->middleware('can:admin-notice-template-create');
    Route::get('notice_template/destroy', 'NoticeTemplateController@destroyNoticeTemplate')->name('admin.notice.destroy.template')->middleware('can:admin-notice-template-destroy');
    Route::get('notice_template/edit/{template_id}', 'NoticeTemplateController@editNoticeTemplate')->name('admin.notice.template.edit')->middleware('can:admin-notice-template-edit');
    Route::post('notice_template/update', 'NoticeTemplateController@updateNoticeTemplateDetails')->name('admin.notice.template.edit')->middleware('can:admin-notice-template-edit');
    Route::post('notice_template/notices', 'NoticeTemplateController@getNotices')->name('admin.notice.template.notices')->middleware('can:admin-notice-template-create');

    //Employees Routes
    Route::get('employees', 'EmployeesController@index')->name('admin.employees')->middleware('can:admin-employees');
    Route::get('employees/custom-filter', 'EmployeesController@getEmployeesFilter')->name('admin.employees.filter');
    Route::get('employee/create', 'EmployeesController@createEmployee')->name('admin.employee.create')->middleware('can:admin-employee-create');
    Route::post('employee/store', 'EmployeesController@storeEmployee')->name('admin.employee.store')->middleware('can:admin-employee-create');
    Route::post('employee/changestatus', 'EmployeesController@changestatus');
    Route::post('employee/add-note', 'EmployeesController@addNote')->name('admin.employee.note')->middleware('can:admin-employees-note');
    Route::post('employee/add-attachment', 'EmployeesController@addAttachment')->name('admin.employee.attachment')->middleware('can:admin-employees-attachment');
    Route::get('employee/edit/{employee_id}', 'EmployeesController@editEmployee')->name('admin.employee.edit')->middleware('can:admin-employee-edit');
    Route::post('employee/update', 'EmployeesController@updateEmployee')->name('admin.employee.edit')->middleware('can:admin-employee-edit');
    Route::get('employee/view/{employee_id}', 'EmployeesController@viewEmployee')->name('admin.employee.view')->middleware('can:admin-employee-view');
    Route::post('employee/getNotes', 'EmployeesController@getNotes')->name('admin.employee.getnote')->middleware('can:admin-employees-note');
    Route::post('employee/note/delete/{note_id}', 'EmployeesController@destroy');
    Route::post('employee/getAttachments', 'EmployeesController@getAttachments')->name('admin.employee.getattachment')->middleware('can:admin-employees-attachment');
    Route::post('employee/attachment/delete/{attachment_id}', 'EmployeesController@deleteAttachment');
    Route::get('customer/searchajaxemployeename', 'EmployeesController@autoCompleteEmployee');


    // Testimonial
    Route::get('testimonial', 'TestimonialController@index')->name('admin.testimonial')->middleware('can:admin-testimonial');
    Route::get('testimonial/view/{testimonial_id}', 'TestimonialController@viewTestimonial');
    Route::get('testimonial/custom-filter', 'TestimonialController@getTestimonialFilter')->name('admin.testimonial.filter');
    Route::get('testimonial/edit/{testimonial_id}', 'TestimonialController@editTestimonial')->name('admin.testimonial.edit');
    Route::post('testimonial/update', 'TestimonialController@updateTestimonial')->name('admin.testimonial.edit');
    Route::get('testimonial/create', 'TestimonialController@createTestimonial')->name('admin.testimonial.create');
    Route::post('testimonial/store', 'TestimonialController@storeTestimonial')->name('admin.testimonial.store');
    Route::post('testimonial/changestatus', 'TestimonialController@changeStatus');
    Route::get('testimonial/delete', 'TestimonialController@deleteTestimonial');
    // Lien Blog
    Route::get('lien_blog', 'LienBlogController@index')->name('admin.lien_blog')->middleware('can:admin-lien_blog');
    Route::get('lien_blog/view/{lien_blog}', 'LienBlogController@viewLienBlog');
    Route::get('lien_blog/custom-filter', 'LienBlogController@getLienBlogFilter')->name('admin.lien_blog.filter');
    Route::get('lien_blog/edit/{lien_blog}', 'LienBlogController@editLienBlog')->name('admin.lien_blog.edit');
    Route::post('lien_blog/update', 'LienBlogController@updateLienBlog')->name('admin.lien_blog.edit');
    Route::get('lien_blog/create', 'LienBlogController@createLienBlog')->name('admin.lien_blog.create');
    Route::post('lien_blog/store', 'LienBlogController@storeLienBlog')->name('admin.lien_blog.store');
    Route::get('lien_blog/delete', 'LienBlogController@deleteLienBlog');

    Route::get('states', 'StateController@index')->name('admin-states')->middleware('can:admin-states');

    Route::get('states/list', 'StateController@Statelist')->middleware('can:admin-states-list');
    Route::post('state/changestatus', 'StateController@changestatus')->middleware('can:admin-states-changestatus');
    Route::get('states/create', 'StateController@create');
    Route::post('states/store', 'StateController@store');
    Route::post('ZipCodeLookups', 'CitiesController@ZipCodeLookup');
    Route::post('getStateName', 'CitiesController@getStateName');

    Route::get('cities', 'CitiesController@index');
    Route::get('get_city', 'CitiesController@getCity');
    Route::get('cities/create', 'CitiesController@create');
    Route::post('cities/store', 'CitiesController@store');
    Route::get('cities/edit/{id}', 'CitiesController@edit');
    Route::post('cities/update/{id}', 'CitiesController@update');


    Route::post('lien_blog/changestatus', 'LienBlogController@changeStatus');

    //Manage Subscriptions
    Route::get('manage-subscriptions', 'SubscriptionController@index')->name('subscription.index')->middleware('can:admin-manage-subscriptions');
    Route::get('manage-subscription/datatable', 'SubscriptionController@datatableindex')->name('subscription.datatable.index')->middleware('can:admin-manage-subscriptions');
    Route::get('manage-subscriptions/add-package/{id}', 'SubscriptionController@addPackage')->name('subscription.add.package')->middleware('can:admin-manage-subscriptions');
    Route::post('manage-subscriptions/save-package', 'SubscriptionController@savePackage')->name('managesubscription.save')->middleware('can:admin-manage-subscriptions');
    Route::get('manage-subscriptions/load-package', 'SubscriptionController@loadPackage')->name('loadpackage')->middleware('can:admin-manage-subscriptions');
    // Manage Customer Pricing Package
    Route::get('customer/{customer}/manage-package-pricing', 'CustomerController@managePackagePricing')->name('customer.managePackagePricing')->middleware('can:admin-customer-edit');
    Route::post('customer/save-package-pricing', 'CustomerController@savePackagePricing')->name('customer.savePackagePricing')->middleware('can:admin-customer-edit');
    Route::get('customer/load-package-pricing', 'CustomerController@loadPackagePricing')->name('customer.loadpackagepricing')->middleware('can:admin-customer-edit');
    //Approve Cheque Subscriptions
    Route::get('cheque-subscriptions', 'SubscriptionController@getChequeSubscriptionList')->name('cheque.subscription')->middleware('can:admin-manage-subscriptions');
    Route::post('approve-cheque-subscriptions', 'SubscriptionController@approveChequeSubscriptionList')->name('approve.cheque.subscription')->middleware('can:admin-manage-subscriptions');

    //your role 
    Route::get('category', 'RolesController@index')->name('admin.category')->middleware('can:admin-category');
    Route::get('category/create', 'RolesController@create');
    Route::post('category/create', 'RolesController@store');
    Route::get('category/edit/{category_id}', 'RolesController@edit');
    Route::post('category/update', 'RolesController@update');
    Route::get('category/destroy', 'RolesController@destroy');
    //project type
    Route::get('project-type', 'ProjectTypeController@index')->name('admin.project.type')->middleware('can:admin-project-type');
    Route::get('project-type/create', 'ProjectTypeController@create');
    Route::post('project-type/create', 'ProjectTypeController@store');
    Route::get('project-type/edit/{type_id}', 'ProjectTypeController@edit');
    Route::post('project-type/update', 'ProjectTypeController@update');
    Route::get('project-type/destroy', 'ProjectTypeController@destroy');

    //ROles
    Route::get('project-roles', 'ProjectRoles@index')->name('admin.project.roles')->middleware('can:admin-project-roles');
    Route::get('project-role/create', 'ProjectRoles@create');
    Route::post('project-role/create', 'ProjectRoles@store');
    Route::get('project-role/edit/{type_id}', 'ProjectRoles@edit');
    Route::post('project-role/update', 'ProjectRoles@update');
    Route::get('project-role/destroy', 'ProjectRoles@destroy');



    //reports
    Route::get('reports', 'ReportsController@getAccountManagerReports')->name('admin.reports')->middleware('can:admin-reports');
    Route::get('reports/custom-filter', 'ReportsController@getReportsCustomFilter');
    Route::post('reports/sendMailExcel', 'ReportsController@sendMailExcel');
    Route::post('reports/exportExcel', 'ReportsController@exportExcel');
    Route::post('change-password', 'CustomerController@postChangePasswordForm');
    Route::post('customer/change-password', 'CustomerController@postCustomerChangePasswordForm');
    Route::post('change_account_setting', 'CustomerController@postChangeAccountSetting');


    Route::get('remove_agent/{id}', 'CustomerController@removeAgent');
    Route::get('remove_representative/{id}', 'CustomerController@removeRepresentative');
    
});

Route::get('contacts/{id?}', 'Customer\ContactController@index')->name('address-book');
Route::get('contacts/custom-filter/{id?}', 'Customer\ContactController@getAddressBookFilterData');
