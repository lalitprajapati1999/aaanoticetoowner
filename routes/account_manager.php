<?php

/*
|--------------------------------------------------------------------------
| Admin Section Routes
|--------------------------------------------------------------------------
|
| Below route group is where you may define all of the routes that are
| handled at admin and superadmin login.
|
*/
Route::group(
[
    'namespace'  => 'Auth',
    'middleware' => 'auth',
    'prefix'     => config('backpack.base.route_prefix'),
],
function () {

    // if not otherwise configured, setup the "my account" routes
     if (config('backpack.base.setup_my_account_routes')) {
        Route::get('edit-account-info', 'MyAccountController@getAccountInfoForm')->name('backpack.account.info')->middleware('can:admin-edit-account-info');
        Route::post('edit-account-info', 'MyAccountController@postAccountInfoForm')->middleware('can:admin-edit-account-info');
    }
}); 
Route::group(
[
    'namespace'  => 'AccountManager',
    'middleware' => 'auth',
    'prefix'     => 'account-manager',
    'as' => 'account-manager.'
],
function () {
    Route::get('create-your-own','ResearchController@createYourOwn');
Route::get('create-your-own/work-order-history', 'ViewWorkOrderController@WorkOrderHitory');
Route::get('create-your-own/get-work-order', 'ViewWorkOrderController@get_workorders');
Route::get('create-your-own/edit/{id}/{notice_id}/{is_rescind}', 'ViewWorkOrderController@edit');
Route::post('create-your-own/edit/{id}/{notice_id}/{is_rescind}', 'ViewWorkOrderController@update');
Route::get('create-your-own/updateStatus/{id}', 'ViewWorkOrderController@updateStatus');
Route::post('create-your-own/recipients/{id}/{notice_id}', 'ViewWorkOrderController@store_recipients');
Route::post('create-your-own/recipients/edit/{id}/{notice_id}', 'ViewWorkOrderController@edit_recipients');
Route::get('create-your-own/recipient/delete/{id}/{notice_id}/{work_order_id}', 'ViewWorkOrderController@destroy');
Route::post('create-your-own/createLabel', 'ViewWorkOrderController@createLabel');
Route::get('create-your-own/proceed/{id}/{notice_id}', 'ViewWorkOrderController@proceed');
Route::get('create-your-own/duplicate/{id}/{notice_id}/{is_rescind}', 'ViewWorkOrderController@duplicate');
Route::get('create-your-own/cancel/{id}', 'ViewWorkOrderController@cancel');
Route::get('create-your-own/view/{id}', 'ViewWorkOrderController@viewWorkOrder');
Route::get('create-your-own/rescind/{id}/{notice_id}/{is_rescind}', 'ViewWorkOrderController@duplicate');
Route::get('create-your-own/view/printworkorder/{workorderid}', 'ViewWorkOrderController@printrequesteworkorder');
Route::get('create-your-own/get_note_emails/{work_order_id}','ViewWorkOrderController@get_note_emails');
Route::post('create-your-own/view/uploadreceipt', 'ViewWorkOrderController@postViewWorkOrder');
Route::post('create-your-own/add-note', 'ViewWorkOrderController@add_note');
Route::get('create-your-own/edit-new/{id}/{notice_id}/{is_rescind}', 'ViewWorkOrderController@researchEditNextStep');
Route::get('research', 'ResearchController@index')->name('research')->middleware('can:account-manager-research');
Route::get('research/cancel/{id}','ResearchController@cancel');
Route::get('research/duplicate/{id}/{notice_id}/{is_rescind}','ResearchController@duplicate');
Route::get('research/rescind/{id}/{notice_id}/{is_rescind}', 'ResearchController@duplicate');
Route::get('research/proceed/{id}/{notice_id}','ResearchController@proceed');
Route::get('research/tabselection','ResearchController@filterSelect');
Route::get('research/resetselection','ResearchController@resetFilter');
Route::get('research/edit/{id}/{notice_id}/{is_rescind}','ResearchController@edit');
Route::post('research/edit/{id}/{notice_id}/{customer_id}/{is_rescind}','ResearchController@update');
Route::post('research/email-notes','ResearchController@notesEmail');

Route::get('research/edit/{id}/{notice_id}/{customer_id}/{is_rescind}','ResearchController@researchEditNextStep');
Route::get('get-work-order','ResearchController@get_workorders');
Route::post('research/add-note','ResearchController@add_note');
Route::get('get-selectedCustomer-workOrders','ResearchController@selectedCustomer');
Route::post('recipients/{id}/{notice_id}/{customer_id}','ResearchController@store_recipients');
Route::get('work-order/add-note','ResearchController@add_note');
Route::post('change-account-manager','ResearchController@changeAccountManager');
Route::post('get-account-managers','ResearchController@getAccountManagerList');
Route::post('research/attachments','ResearchController@store_attachments');
Route::get('research/autocomplete','ResearchController@getWorkOrdeNumber');
Route::get('research/autocomplete_V1','ResearchController@getWorkOrdeNumberV1');
Route::Post('research/checkFields','ResearchController@CheckFields');
Route::get('recipient/delete/{id}/{notice_id}/{work_order_id}/{customer_id}','ResearchController@destroy');
Route::get('research/restrict-work-order/{work_order_id}','ResearchController@restrictWorkOrder');
Route::get('research/send-back-to-proccesing/{work_order_id}','ResearchController@BackToProccessing');
Route::post('research/createLabel','ResearchController@createLabel');
Route::post('research/getVerifiedAddress','ResearchController@getVerifiedAddress');
Route::post('research/saveStampsLabelData','ResearchController@saveStampsLabelData');
Route::post('get-contacts','ResearchController@get_contacts');
Route::post('recipients/{id}/{notice_id}/{customer_id}','ResearchController@store_recipients');

Route::post('work-order/store_note','ResearchController@storeNote');

Route::get('printpdf/{work_order_id}/{type_work_order}/{Notice}','ResearchController@printWorkOrder');
Route::get('proceed_mailing/{work_order_id}','ResearchController@proceedtoMailing');
Route::get('research/printpdf/{work_order_id}','ResearchController@printWO');
Route::get('updateStatus/{work_order_id}','ResearchController@updateStatus');
Route::get('get_note_email/{work_order_id}','ResearchController@get_note_email');


//mailing routes
Route::get('mailing/{flag}', 'MailingController@index')->name('mailing')->middleware('can:account-manager-mailing');
Route::post('mailing/printpdf','MailingController@printWorkOrder');
Route::post('mailing/completedwoprintpdf','MailingController@completewoprintpdf');
Route::post('mailing/createShippingLabels','MailingController@generateShippinglabels');
Route::post('mailing/printfirmmail/{print_type?}','MailingController@printFirmMails');
Route::post('complete-wo','MailingController@completeWo');
Route::get('get-mailing-workorders','MailingController@get_workorders');
Route::post('print-work-order','MailingController@printWO');
//cyo mailing
Route::get('cyo-mailing/{flag}', 'MailingController@cyoMailingList')->name('cyo-mailing')->middleware('can:account-manager-cyo-mailing');
Route::get('get-cyo-mailing-workorders','MailingController@getCyoMailing');
Route::post('cyo-mailing/createShippingLabels','MailingController@generateCyoShippinglabels');

Route::post('mailing/checkpin','MailingController@checkMailingPin');
Route::get('recording/pending_signature/updateStatus/{status}/{work_order_id}','ResearchController@updateRecordingPendingSignStatus');

//Cyo research
Route::get('cyo-research/send-back-to-proccesing/{work_order_id}','ResearchController@CyoBackToProccessing');
});
